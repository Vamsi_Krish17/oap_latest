﻿using OAP.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;
//using OAP.Domain.ViewModel;
using OAP.Domain;
using System.IO;
using OAP.Domain.ViewModel;
using System.Data.Entity;

namespace OAP.Controllers
{
    //[Authorize]
    [InitializeSimpleMembership]
    public class BaseController : Controller
    {

        public OAPEntities db = new OAPEntities();
        public DataTableRequest datatableRequest { get; set; }

        public class DataTableRequest
        {
            HttpRequestBase Request;
            private string[] Columns;

            public int DrawRequestNumber
            {
                get
                {
                    return Convert.ToInt32(Request["draw"]);
                }
            }
            public int ItemsToSkip
            {
                get
                {
                    return Convert.ToInt32(Request["start"]);
                }
            }
            public int LengthRequired
            {
                get
                {
                    return Convert.ToInt32(Request["length"]);
                }
            }
            public string SearchString
            {
                get
                {
                    return Request["search[value]"] != null ? Request["search[value]"].ToLower() : "";
                }
            }
            public bool ShouldOrder
            {
                get
                {
                    return !string.IsNullOrEmpty(OrderByColumn);
                }
            }
            public string OrderByColumn
            {
                get
                {
                    string sortColumn = Request["order[0][column]"];
                    if (string.IsNullOrEmpty(sortColumn))
                    {
                        return "";
                    }
                    else
                    {
                        int colIndex = Convert.ToInt32(sortColumn);
                        return Columns[colIndex];
                    }
                }
            }
            public bool IsOrderAsccending
            {
                get
                {
                    string sortDirection = Request["order[0][dir]"];
                    if (string.IsNullOrEmpty(sortDirection))
                    {
                        return true;
                    }
                    else
                    {
                        return sortDirection.Equals("asc") ? true : false;
                    }
                }
            }
            public DataTableRequest(HttpRequestBase _request, string[] columns)
            {
                this.Request = _request;
                this.Columns = columns;
            }
        }

        public int GetCurrentUserID()
        {
            return WebSecurity.GetUserId(User.Identity.Name);
        }

        public DateTime GetDefaultDateTime()
        {
            return new DateTime(1900, 01, 01);
        }

        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            base.Initialize(requestContext);
            InitializeNotifications();
            InitializeProfileImage(requestContext);
            InitializeTheme();
            InitializeLogo();
            SetViewBagDetails();
        }


        public string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }

        #region Initialize the notification viewbag and ImageUrl

        public void InitializeProfileImage(System.Web.Routing.RequestContext requestContext)
        {
            if (requestContext.HttpContext.User.Identity.IsAuthenticated)
            {
                string userName = requestContext.HttpContext.User.Identity.Name;
                UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == userName.ToLower()).FirstOrDefault();
                ViewBag.ProfileImage = userProfile.ImageUrl;
            }
        }

        [InitializeSimpleMembership]
        public void InitializeNotifications()
        {
            int userId = GetCurrentUserID();
            UserNotification lastUserNotification = db.UserNotification.Where(x => x.UserID == userId).FirstOrDefault();
            DateTime lastVisitedDate = lastUserNotification != null ? lastUserNotification.LastVisitedDate ?? GetDefaultDateTime() :  DateTime.Now;

            List<NotificationViewModel> notificationViewModelList = new List<NotificationViewModel>();
            int notificationCount = 0;
            if (this.User != null && User.Identity.IsAuthenticated && (User.IsInRole("CollegeAdmin") || (User.IsInRole("SuperAdmin"))))
            {

                int completedTestCount = db.UserAssessment.ToList().Where(ass => (ass.FinishTime == null ? false : (DateTime.Compare(ass.FinishTime.Value.Date, lastVisitedDate.Date) == 0))).Count();

                if (completedTestCount > 0)
                {
                    notificationCount++;
                    NotificationViewModel retestRequestNotificationViewModel = new NotificationViewModel();
                    retestRequestNotificationViewModel.Count = completedTestCount;
                    retestRequestNotificationViewModel.Name = "Completed Tests";
                    retestRequestNotificationViewModel.Url = "Batch/BatchQuestionPaper";
                    notificationViewModelList.Add(retestRequestNotificationViewModel);
                    ViewBag.RetestRequestCount = completedTestCount;
                }

            }
            else if (this.User != null && User.Identity.IsAuthenticated && User.IsInRole("Student"))
            {
                UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower().Equals(this.User.Identity.Name.ToLower())).FirstOrDefault();
                if (userProfile != null)
                {
                    int scheduledTestCount = userProfile.Student.BatchStudent.SelectMany(ba => ba.Batch.BatchAssessment).Where(ga => (ga.StartDateTime <= lastVisitedDate) && ga.EndDateTime >= lastVisitedDate).Distinct().Count();
                    if (scheduledTestCount > 0)
                    {
                        notificationCount++;
                        NotificationViewModel retestRequestNotificationViewModel = new NotificationViewModel();
                        retestRequestNotificationViewModel.Count = scheduledTestCount;
                        retestRequestNotificationViewModel.Name = "Scheduled Test";
                        retestRequestNotificationViewModel.Url = "Dashboard/TestList";
                        notificationViewModelList.Add(retestRequestNotificationViewModel);
                    }

                }
            }
            ViewBag.NotificationCount = notificationCount;
            ViewBag.Notifications = notificationViewModelList;

        }

        public void InitializeTheme()
        {
            Setting setting = db.Setting.Where(s => s.Name.ToLower() == "theme").FirstOrDefault();
            if (setting != null && !string.IsNullOrEmpty(setting.Value))
                ViewBag.Theme = setting.Value;
        }

        public void InitializeLogo()
        {
            int userId = GetCurrentUserID();
            string logo = "";
             if (User.IsInRole("CollegeAdmin"))
            {
               // ViewBag.Layout = "~/Views/Shared/_CollegeAdminLayout.cshtml";
                //ViewBag.CancelLink = "/Dashboard/Index";
                 logo = db.UserProfile.Find(userId).UserOrganisation.FirstOrDefault() != null ? db.UserProfile.Find(userId).UserOrganisation.FirstOrDefault().OrgLogo : "";

            }
            ViewBag.Logo = logo;
        }

        public void SetViewBagDetails()
        {
            //if (User.IsInRole("Admin"))
            if (User.IsInRole("SuperAdmin"))
            {
                ViewBag.Layout = "~/Views/Shared/_SuperAdminLayout.cshtml";
                //ViewBag.CancelLink = "/College/Index";
            }
            else if (User.IsInRole("CollegeAdmin"))
            {
                ViewBag.Layout = "~/Views/Shared/_CollegeAdminLayout.cshtml";
                //ViewBag.CancelLink = "/Dashboard/Index";
            }
            else if (User.IsInRole("Student"))
            {
                ViewBag.Layout = "~/Views/Shared/_StudentLayout.cshtml";
            }
            //else if (User.IsInRole("Staff"))
            //{
            //    ViewBag.Layout = "~/Views/Shared/_StaffLayout.cshtml";
            //}
        }

        public void UpdateNotificationVisitedDate()
        {
            int userId = GetCurrentUserID();
            UserNotification lastUserNotification = db.UserNotification.Where(x => x.UserID == userId).FirstOrDefault();
            if (lastUserNotification == null)
            {
                lastUserNotification = new UserNotification();
                lastUserNotification.UserID = GetCurrentUserID();
                lastUserNotification.LastVisitedDate = DateTime.Now;
                db.UserNotification.Add(lastUserNotification);
            }
            else
            {
                lastUserNotification.LastVisitedDate = DateTime.Now;
                db.Entry(lastUserNotification).State = EntityState.Modified;
            }
            db.SaveChanges();
            
        }

        #endregion

    }
}
