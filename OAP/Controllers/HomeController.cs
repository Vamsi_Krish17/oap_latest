﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OAP.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
       
    }
    public class MiniStatisticsModel
    {

        public int TotalStudents { get; set; }

        public int CreatedTests { get; set; }

        public int ActiveTests { get; set; }

        public int ActiveUsers { get; set; }

        public int CompletedTestsToday { get; set; }

        public int CompletedTestsLastSeven { get; set; }

        public int CompletedTestsLastThirty { get; set; }

        public int CompletedTestsAll { get; set; }

    }
}
