﻿using Newtonsoft.Json;
using OAP.Domain;
using OAP.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace OAP.Controllers
{
    public class PracticeSchoolController : Controller
    {
        private OAPEntities db = new OAPEntities();

        public ActionResult Index()
        {
            var practiceSchoolViewModelList = new List<PracticeSchoolViewModel>();
            var practiceSchoolModelList = db.PracticeSchool.OrderBy(x => x.SessionName).ToList();

            if (practiceSchoolModelList != null && practiceSchoolModelList.Any())
            {
                foreach (var practiceSchoolModel in practiceSchoolModelList)
                {
                    var practiceSchoolViewModel = new PracticeSchoolViewModel
                    {
                        SessionName = practiceSchoolModel.SessionName,
                        ID = practiceSchoolModel.ID,
                        IsTechnicalSession = practiceSchoolModel.IsTechnicalSession,
                        Section1 = practiceSchoolModel.Section1,
                        Section2 = practiceSchoolModel.Section2,
                        Section3 = practiceSchoolModel.Section3,
                        Section4 = practiceSchoolModel.Section4,
                        CreatedBy = practiceSchoolModel.CreatedBy,
                        ModifiedBy = practiceSchoolModel.ModifiedBy,
                        CreatedDate = practiceSchoolModel.CreatedDate,
                        ModifiedDate = practiceSchoolModel.ModifiedDate,
                    };

                    practiceSchoolViewModelList.Add(practiceSchoolViewModel);
                }

                return View(practiceSchoolViewModelList);
            }

            return View(practiceSchoolViewModelList);
        }

        public ActionResult PracticeLevel(int id)
        {
            var levelModels = new List<PracticeSchoolLevelModel>();
            var dbLevelModels = db.PracticeSchoolLevel.Where(x => x.PracticeSchoolId == id).ToList();

            foreach (var level in dbLevelModels)
            {
                var levelModel = new PracticeSchoolLevelModel()
                {
                    ID = level.ID,
                    LevelName = level.LevelName,
                    PracticeSchoolID = level.PracticeSchoolId
                };

                levelModels.Add(levelModel);
            }

            return View(levelModels);
        }

        public ActionResult PracticeQuestionDisplay(int levelId)
        {

            var technicalModelList = new List<TechnicalQuestionViewModel>();

            var technicalQuestion = db.PracticeSchoolLevelQuestion.Where(x => x.PracticeSchoolLevelId == levelId).Select(s => s).ToList();

            foreach (var question in technicalQuestion)
            {
                var questionDbModel = db.TechnicalQuestion.Where(x => x.ID == question.TechnicalQuestionId).FirstOrDefault();

                if (questionDbModel != null)
                {
                    var technicalModel = new TechnicalQuestionViewModel()
                    {
                        ID = questionDbModel.ID,
                        Title = questionDbModel.Title,
                        Description = questionDbModel.Description,
                        LanguagesString = questionDbModel.Languages,
                        PracticeSchoolLevelQuestionId = question.ID
                    };

                    technicalModelList.Add(technicalModel);
                }
            }

            return View(technicalModelList);
        }

        public ActionResult SolveQuestion(int questionID, int practiceSchoolLevelQuestionId)
        {
            var questionDbModel = db.TechnicalQuestion.Where(x => x.ID == questionID).FirstOrDefault();
            var answerDbModels = db.TechnicalAnswer.Where(x => x.QuestionID == questionID).ToList();

            var answerModelList = new List<TechnicalAnswerViewModel>();

            foreach (var answerDbModel in answerDbModels)
            {
                var answerModel = new TechnicalAnswerViewModel()
                {
                    ID = answerDbModel.ID,
                    InputParam = answerDbModel.InputParam,
                    OutputParam = answerDbModel.OutputParam,
                    QuestionID = answerDbModel.QuestionID
                };

                answerModelList.Add(answerModel);
            }

            var technicalQuestion = new TechnicalQuestionViewModel()
            {
                QuestionID = questionID,
                Title = questionDbModel.Title,
                Description = questionDbModel.Description,
                LanguagesString = questionDbModel.Languages,
                Languages = GetAllLanguage(),
                AnswerViewModelList = answerModelList,
                PracticeSchoolLevelQuestionId = practiceSchoolLevelQuestionId
            };

            return View(technicalQuestion);
        }
        private List<CodingLanguageModel> GetAllLanguage()
        {
            var url = "https://api.judge0.com";
            var urlParameters = "/languages";

            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(url),
            };

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync(urlParameters).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                var rootResult = JsonConvert.DeserializeObject<List<CodingLanguageModel>>(result);

                client.Dispose();
                return rootResult;
                // Parse the response body.

            }
            else
            {
                client.Dispose();
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                return new List<CodingLanguageModel>();
            }
        }

        public bool MarkComplete(bool isCompleted, int practiceSchoolLevelQuestionId)
        {
            var user = User.Identity.Name;
            int userId = WebSecurity.GetUserId(user);
            //Restrict Multiple Entries
            var markComplete = db.PracticeSchoolStudentAnalytics.Where(x => x.UserId == userId && x.PracticeSchoolQuestionId == practiceSchoolLevelQuestionId)
                                                                .FirstOrDefault() ?? new PracticeSchoolStudentAnalytics();
            //Create
            if (markComplete.Id == 0)
            {
                markComplete.IsCompleted = isCompleted;
                markComplete.PracticeSchoolQuestionId = practiceSchoolLevelQuestionId;
                markComplete.UserId = userId;
                db.PracticeSchoolStudentAnalytics.Add(markComplete);
            }
            //Update
            else
            {
                markComplete.IsCompleted = isCompleted;
            }
            return db.SaveChanges() > 0;
        }

    }
}
