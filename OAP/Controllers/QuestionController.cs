﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OAP.Domain;
using HelperClasses;
using OAP.Domain.ViewModel;
using System.Data.Entity;
using Newtonsoft.Json;
using OAP.Domain.ViewModel.DataTableViewModel;
using System.Data.SqlClient;
using System.Web.Routing;
using System.IO;
using System.Data;
using System.Net.Http;
using System.Net.Http.Headers;
using OAP.Models.ViewModels;

namespace OAP.Controllers
{
    [Authorize(Roles = "SuperAdmin,CollegeAdmin,Admin,Staff")]
    public class QuestionController : BaseController
    {
        private OAPEntities db = new OAPEntities();
        string[] columns = { "Description", "LastUpdated" };
        //public SorterHelper sh = new SorterHelper();
        HtmlFormatter htmlConverter = new HtmlFormatter();

        //
        // GET: /Question/

        //#region Question Index

        //public ActionResult Index()
        //{
        //    return View();
        //}

        //public void List()
        //{
        //    datatableRequest = new DataTableRequest(Request, columns);

        //    // IQueryable<Question> questions = db.Question;

        //    IQueryable<Question> questions = (from q in db.Question
        //                                      group q by new
        //                                      {
        //                                          q.Description
        //                                      } into qs
        //                                      select qs.FirstOrDefault());


        //    int totalRecords = questions.Count();
        //    int filteredCount = totalRecords;
        //    if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
        //    {
        //        questions = (from q in questions where q.Description.Contains(datatableRequest.SearchString) select q);
        //        filteredCount = questions.Count();
        //    }
        //    questions = questions.OrderBy(x => x.ID);
        //    if (datatableRequest.ShouldOrder)
        //    {
        //        questions = questions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
        //    }
        //    questions = questions.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
        //    List<QuestionViewModel> questionViewModelList = MapperHelper.MapToViewModelList(questions);
        //    //foreach (QuestionViewModel questionViewModel in questionViewModelList)
        //    //{
        //    //    questionViewModel.LastUpdated = questionViewModel.LastUpdatedTime.ToString("dd-MM-yyyy");
        //    //}
        //  //  questionViewModelList.Select(q=>q.LastUpdated=q.LastUpdatedTime)
        //    HtmlFormatter htmlFormatter = new HtmlFormatter();
        //    questionViewModelList = questionViewModelList.Select(q => { q.Description = htmlFormatter.GetContentFromHtmlWithOutReplaceSpace(q.Description); return q; }).ToList();
        //    QuestionDataTableViewModel dataTableViewModel = new QuestionDataTableViewModel(questionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

        //    string result = JsonConvert.SerializeObject(dataTableViewModel);
        //    Response.Write(result);
        //}

        //#endregion
        #region Question Index

        public ActionResult Index()
        {
            Models.ViewModels.CommonQuestionViewModel questionViewModel = new Models.ViewModels.CommonQuestionViewModel();
            IQueryable<Tag> tagsList = db.Tag.GroupBy(x => x.Name).Select(x => x.FirstOrDefault()).OrderBy(x => x.Name);
            List<TagViewModel> tagViewModelList = MapperHelper.MapToViewModelList(tagsList);
            questionViewModel.TagViewModelList = tagViewModelList;

            return View(questionViewModel);
        }

        public void List(string tags = "")
       {
            datatableRequest = new DataTableRequest(Request, columns);

            //Take Normal questions
            List<Question> questionList = new List<Question>();
            IQueryable<Question> questions = (from q in db.Question
                                              group q by new
                                              {
                                                  q.Description
                                              } into qs
                                              select qs.FirstOrDefault());

            //Take Technical questions
            List<TechnicalQuestion> technicalQuestionList = new List<TechnicalQuestion>();
            IQueryable<TechnicalQuestion> technicalQuestions = (from q in db.TechnicalQuestion
                                                                group q by new
                                                                {
                                                                    q.Description
                                                                } into qs
                                                                select qs.FirstOrDefault());

            if (tags == "")
            {
                questions = (from q in db.Question
                             group q by new
                             {
                                 q.Description
                             } into qs
                             select qs.FirstOrDefault());

                technicalQuestions = (from q in db.TechnicalQuestion
                                      group q by new
                                      {
                                          q.Description
                                      } into qs
                                      select qs.FirstOrDefault());
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(tags))
                {
                    List<long> tagIDList = tags.Split(',').ToList().Select(long.Parse).ToList();
                    questions = questions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && (q.QuestionTags.Where(qt => tagIDList.Contains(qt.TagID)).Count() > 0));
                    technicalQuestions = technicalQuestions.Where(a => a.TechnicalQuestionTags.Where(x => tagIDList.Contains(x.TagID)).Count() > 0);
                }
                else
                {
                    questions = questions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0);
                }
            }



            int totalRecords = 0;
            IQueryable<Domain.ViewModel.QuestionViewModel> questionViewModels = MapperHelper.MapToViewModelList(questions).AsQueryable();
            IQueryable<Domain.ViewModel.TechnicalQuestionViewModel> technicalQuestionViewModels = MapperHelper.MapToTechnicalViewModelList(technicalQuestions).AsQueryable();

            try
            {
                if (questionViewModels != null && questionViewModels.ToList().Count() != 0)
                {
                    totalRecords = questionViewModels.ToList().Count();
                }

                if (technicalQuestionViewModels != null && technicalQuestionViewModels.ToList().Count() != 0)
                {
                    totalRecords = totalRecords + technicalQuestionViewModels.ToList().Count();
                }
            }
            catch (NullReferenceException)
            {
                totalRecords = 0;
            }

            int filteredCount = totalRecords;

            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                questionViewModels = (from q in questionViewModels where q.Description.Contains(datatableRequest.SearchString) select q);
                technicalQuestionViewModels = (from q in technicalQuestionViewModels where q.Description.Contains(datatableRequest.SearchString) select q);

                filteredCount = questionViewModels.Count() + technicalQuestionViewModels.Count();
            }

            questionViewModels = questionViewModels.OrderBy(x => x.ID);
            technicalQuestionViewModels = technicalQuestionViewModels.OrderBy(x => x.ID);

            if (datatableRequest.ShouldOrder)
            {
                if (datatableRequest.OrderByColumn == "LastUpdated")
                {
                    if (datatableRequest.IsOrderAsccending == true)
                    {
                        questionViewModels = questionViewModels.OrderBy(p => p.LastUpdatedTime);
                        technicalQuestionViewModels = technicalQuestionViewModels.OrderBy(p => p.LastUpdatedTime);
                    }
                    else
                    {
                        questionViewModels = questionViewModels.OrderByDescending(p => p.LastUpdatedTime);
                        technicalQuestionViewModels = technicalQuestionViewModels.OrderByDescending(p => p.LastUpdatedTime);
                    }
                }
            }

            questionViewModels = questionViewModels.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            technicalQuestionViewModels = technicalQuestionViewModels.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);

            List<Domain.ViewModel.QuestionViewModel> questionViewModelList = questionViewModels.ToList();
            List<Domain.ViewModel.TechnicalQuestionViewModel> technicalQuestionViewModelList = technicalQuestionViewModels.ToList();

            var htmlFormatter = new HtmlFormatter();

            questionViewModelList = questionViewModelList.Select(q => { q.Description = htmlFormatter.GetContentFromHtmlWithOutReplaceSpace(q.Description); return q; }).ToList();
            technicalQuestionViewModelList = technicalQuestionViewModelList.Select(q => { q.Description = htmlFormatter.GetContentFromHtmlWithOutReplaceSpace(q.Description); return q;}).ToList();

            QuestionDataTableViewModel dataTableViewModel = new QuestionDataTableViewModel(questionViewModelList, technicalQuestionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }


        public void GetOtherQuestions(string tags, string subjects, int complexity = 0, int id = 0)
        {
            string[] columns = { "Description" };
            datatableRequest = new DataTableRequest(Request, columns);
            IQueryable<Question> allquestions = db.Question;
            IQueryable<Question> questions = allquestions;
            //  QuestionPaperSection questionPaperSection = db.QuestionPaperSection.Find(id);
            int totalRecords = 0;
            int filteredCount = 0;

            //if (questionPaperSection != null)
            if (true)
            {
                totalRecords = questions.Count();
                filteredCount = totalRecords;

                if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
                {
                    //questions = (from q in db.Question where q.Description.Contains(datatableRequest.SearchString) && q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 select q);
                    filteredCount = questions.Count();
                }

                questions = questions.OrderBy(x => x.Description);
                if (datatableRequest.ShouldOrder)
                {
                    questions = questions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
                }
                questions = questions.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            }
            List<Domain.ViewModel.QuestionViewModel> questionViewModelList = MapperHelper.MapToViewModelList(questions);

            var technicalQuestionViewModelList = new List<Domain.ViewModel.TechnicalQuestionViewModel>();

            QuestionDataTableViewModel dataTableViewModel = new QuestionDataTableViewModel(questionViewModelList, technicalQuestionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }


        #endregion

        #region Create

        public ActionResult Create(int passageID = 0)
        {
            PrepareQuestionViewBag();
            var questionViewModel = new Domain.ViewModel.QuestionViewModel();
            List<TagViewModel> tagsNotAssociated = MapperHelper.MapToViewModelList(db.Tag.OrderBy(x => x.Name));
            //   tagsNotAssociated = tagsNotAssociated);
            questionViewModel.TagsNotAssociated = tagsNotAssociated;

            questionViewModel.PassageID = passageID;
            return View(questionViewModel);
        }

        public ActionResult CreateTechnical(int passageID = 0)
        {
            PrepareTechnicalQuestionViewBag();
            var questionViewModel = new Domain.ViewModel.TechnicalQuestionViewModel();
            List<TagViewModel> tagsNotAssociated = MapperHelper.MapToViewModelList(db.Tag.OrderBy(x => x.Name));
            //   tagsNotAssociated = tagsNotAssociated);
            questionViewModel.TagsNotAssociated = tagsNotAssociated;
            questionViewModel.Languages = ((ViewBag.AllLanguages == null) || (!ViewBag.AllLanguages.Any())) ? GetAllLanguage() : ViewBag.AllLanguages;
            questionViewModel.PassageID = passageID;
            return View(questionViewModel);
        }

        private List<Domain.ViewModel.CodingLanguageModel> GetAllLanguage()
        {
            var url = "https://api.judge0.com";
            var urlParameters = "/languages";

            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(url),
            };

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync(urlParameters).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                var rootResult = JsonConvert.DeserializeObject<List<Domain.ViewModel.CodingLanguageModel>>(result);
                ViewBag.AllLanguages = rootResult;
                client.Dispose();
                return rootResult;
                // Parse the response body.

            }
            else
            {
                client.Dispose();
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                return new List<Domain.ViewModel.CodingLanguageModel>();
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(Domain.ViewModel.QuestionViewModel questionViewModel)
        {
            Question question = MapperHelper.MapToDomainObject(db, questionViewModel);
            if (ModelState.IsValid)
            {
                if (questionViewModel.PassageID > 0)
                    question.PassageID = questionViewModel.PassageID;
                db.Question.Add(question);
                db.SaveChanges();

                AddTagsToQuestions(questionViewModel.TagIDString, question);
                UpdateQuestionIDToAnswers(question, questionViewModel.AnswerIDString);

                if (questionViewModel.PassageID > 0)
                    return RedirectToAction("Edit", "Passage", new RouteValueDictionary(new { id = questionViewModel.PassageID }));

                return RedirectToAction("Index");
            }
            DeleteAllUnsavedAnswers();                      //Delete the answers with no questions
            PrepareQuestionViewBag();
            List<TagViewModel> tagsNotAssociated = MapperHelper.MapToViewModelList(db.Tag);
            questionViewModel.TagsNotAssociated = tagsNotAssociated;
            return View(questionViewModel);
        }


        [HttpPost]
        //[ValidateInput(false)]
        public ActionResult CreateTechnical(Domain.ViewModel.TechnicalQuestionViewModel questionViewModel)
        {

            string languagesString = string.Empty;
            TechnicalQuestion question = MapperHelper.MapToDomainObject(db, questionViewModel);

            if (ModelState.IsValid)
            {
                if (questionViewModel.PassageID > 0)
                    question.PassageID = questionViewModel.PassageID;

                db.TechnicalQuestion.Add(question);
                db.SaveChanges();

                AddTagsToTechnicalQuestions(questionViewModel.TagIDString, question);
                UpdateTechnicalQuestionIDToAnswers(question, questionViewModel.AnswerIDString);

                if (questionViewModel.PassageID > 0)
                    return RedirectToAction("Edit", "Passage", new RouteValueDictionary(new { id = questionViewModel.PassageID }));

                return RedirectToAction("Index");
            }
            DeleteAllUnsavedAnswers();                      //Delete the answers with no questions
            PrepareQuestionViewBag();
            List<TagViewModel> tagsNotAssociated = MapperHelper.MapToViewModelList(db.Tag);
            questionViewModel.TagsNotAssociated = tagsNotAssociated;
            return View(questionViewModel);
        }

        public ActionResult ImageUpload(HttpPostedFileBase file)
        {
            if (file != null)
            {
                string pic = System.IO.Path.GetFileName(file.FileName);
                string path = System.IO.Path.Combine(
                                       Server.MapPath("~/images/profile"), pic);
                // file is uploaded
                file.SaveAs(path);

                // save the image path path to the database or you can send image 
                // directly to database
                // in-case if you want to store byte[] ie. for DB
                using (MemoryStream ms = new MemoryStream())
                {
                    file.InputStream.CopyTo(ms);
                    byte[] array = ms.GetBuffer();
                }

            }
            // after successfully uploading redirect the user
            return RedirectToAction("ImageUpload", "Question");
        }

        #endregion

        #region Edit

        public ActionResult Edit(long id = 0, int passageID = 0)
        {
            Session["ID"] = id;
            PrepareQuestionViewBag();
            Question question = db.Question.Find(id);
            if (question == null)
            {
                return HttpNotFound();
            }
            var questionViewModel = new Domain.ViewModel.QuestionViewModel();
            questionViewModel = MapperHelper.MapToViewModel(question);
            questionViewModel.TagsAssociated = MapperHelper.MapToViewModelList(db.QuestionTags.Where(qt => qt.Question.ID == question.ID).Select(x => x.Tag).OrderBy(x => x.Name));
            List<TagViewModel> tagsNotAssociated = MapperHelper.MapToViewModelList(db.Tag.Except(db.QuestionTags.Where(qt => qt.Question.ID == question.ID).Select(x => x.Tag)));
            questionViewModel.TagsNotAssociated = tagsNotAssociated;
            var answerViewModelList = new List<Domain.ViewModel.AnswerViewModel>();
            //answerViewModelList = MapperHelper.MapToViewModelList(db.Answer.Where(a => a.Question.ID == question.ID));
            answerViewModelList = MapperHelper.MapToViewModelList(question.Answer.AsQueryable().OrderBy(x => x.Description));
            questionViewModel.AnswerViewModelList = answerViewModelList;
            questionViewModel.PassageID = passageID;
            return View(questionViewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(Domain.ViewModel.QuestionViewModel questionViewModel)
        {
            if (questionViewModel.ID == 0)
            {
                questionViewModel.ID = Convert.ToInt32(Session["ID"]);
            }
            Question question = db.Question.Find(questionViewModel.ID);
            question = MapperHelper.MapToDomainObject(db, questionViewModel, question);
            if (question.ComplexityID == 0)
            {
                question.ComplexityID = null;
            }
            if (question.SubjectID == 0)
            {
                question.SubjectID = null;
            }
            if (ModelState.IsValid)
            {
                // question.Description = htmlConverter.FormatFileContent(question.Description);
                question.Description = question.Description;


                db.Entry(question).State = EntityState.Modified;
                db.SaveChanges();
                AddTagsToQuestions(questionViewModel.TagIDString, question);
                UpdateQuestionIDToAnswers(question, questionViewModel.AnswerIDString);
                if (questionViewModel.PassageID > 0)
                    return RedirectToAction("Edit", "Passage", new RouteValueDictionary(new { id = questionViewModel.PassageID }));
                return RedirectToAction("Index");
            }
            PrepareQuestionViewBag();
            DeleteAllUnsavedAnswers();                      //Delete the answers with no questions
            questionViewModel.TagsAssociated = MapperHelper.MapToViewModelList(db.QuestionTags.Where(qt => qt.Question.ID == questionViewModel.ID).Select(x => x.Tag));
            // questionViewModel.TagsAssociated = questionViewModel.TagsAssociated);
            List<TagViewModel> tagsNotAssociated = MapperHelper.MapToViewModelList(db.Tag.Except(db.QuestionTags.Where(qt => qt.Question.ID == question.ID).Select(x => x.Tag)));
            // tagsNotAssociated = tagsNotAssociated);
            questionViewModel.TagsNotAssociated = tagsNotAssociated;
            questionViewModel.TagsAssociated = new List<TagViewModel>();
            questionViewModel.AnswerViewModelList = MapperHelper.MapToViewModelList(question.Answer.AsQueryable());
            return View(questionViewModel);
        }

        #endregion

        public void CreateTag(string tagName)
        {
            string result;
            Tag tag = new Tag();
            tag.Name = tagName;
            try
            {
                db.Tag.Add(tag);
                db.SaveChanges();
                result = tag.ID.ToString() + "," + tag.Name;
            }
            catch (SqlException ex)
            {
                result = "False";
            }

            Response.Write(result);
        }

        public void DeleteAllUnsavedAnswers()
        {
            //List<Answer> answerList = db.Answer.Where(a => (a.Question == null || a.Question.ID == 0) && a.LastUpdated < DateTime.Now).ToList();
            //db.Answer.RemoveRange(answerList);
            //db.SaveChanges();
        }

        public void UpdateQuestionIDToAnswers(Question question, string answerIDString)
        {
            if (!String.IsNullOrEmpty(answerIDString))
            {
                List<string> answerIDListString = answerIDString.Split(',').ToList();
                List<long> answerIDList = answerIDListString.Select(long.Parse).ToList();
                foreach (long answerID in answerIDList)
                {
                    Answer answer = db.Answer.Find(answerID);
                    if (answer != null)
                    {
                        answer.Question = question;
                        //   answer.Description = htmlConverter.FormatFileContent(answer.Description);
                        answer.Description = answer.Description;


                        db.Entry(answer).State = EntityState.Modified;
                    }
                }
                db.SaveChanges();
            }
        }

        public void UpdateTechnicalQuestionIDToAnswers(TechnicalQuestion question, string answerIDString)
        {
            if (!String.IsNullOrEmpty(answerIDString))
            {
                List<string> answerIDListString = answerIDString.Split(',').ToList();
                List<long> answerIDList = answerIDListString.Select(long.Parse).ToList();
                foreach (long answerID in answerIDList)
                {
                    TechnicalAnswer answer = db.TechnicalAnswer.Find(answerID);
                    if (answer != null)
                    {
                        answer.TechnicalQuestion = question;
                        //   answer.Description = htmlConverter.FormatFileContent(answer.Description);
                        answer.InputParam = answer.InputParam;

                        db.Entry(answer).State = EntityState.Modified;
                    }
                }
                db.SaveChanges();
            }
        }

        public void PrepareQuestionViewBag()
        {
            ViewBag.QuestionCategory = new SelectList(db.QuestionCategory.OrderBy(qc => qc.Name), "ID", "Name");
            ViewBag.QuestionType = new SelectList(db.QuestionType.OrderBy(qt => qt.Name), "ID", "Name");
            ViewBag.Subject = new SelectList(db.Subject.OrderBy(qt => qt.Name), "ID", "Name");
            ViewBag.Complexity = new SelectList(db.Complexity.OrderBy(qt => qt.ComplexityLevel), "ID", "ComplexityLevel");
        }

        public void PrepareTechnicalQuestionViewBag()
        {
            ViewBag.QuestionType = new SelectList(new List<SelectListItem>
            {
               new SelectListItem { Selected = true, Text = "Tech", Value = "4"}
            });

            ViewBag.SolutionType = new SelectList(new List<SelectListItem>
            {
               new SelectListItem { Selected = true, Text = "Text", Value = "Text"},
               new SelectListItem { Selected = true, Text = "File", Value = "File"}
            });

            ViewBag.Complexity = new SelectList(db.Complexity.OrderBy(qt => qt.ComplexityLevel), "ID", "ComplexityLevel");
        }

        public void AddTagsToQuestions(string tagIDString, Question question)
        {
            IEnumerable<QuestionTags> associatedTags = question.QuestionTags;
            db.QuestionTags.RemoveRange(associatedTags);
            db.SaveChanges();
            QuestionTags questionTags;
            if (!String.IsNullOrEmpty(tagIDString))
            {
                List<string> tagIDList = tagIDString.Split(',').ToList();
                foreach (string tagID in tagIDList)
                {
                    long id = Convert.ToInt64(tagID);
                    questionTags = new QuestionTags();
                    Tag tags = db.Tag.Find(id);
                    questionTags.Tag = tags;
                    questionTags.Question = question;
                    db.QuestionTags.Add(questionTags);
                    db.SaveChanges();
                }
            }
        }

        public void AddTagsToTechnicalQuestions(string tagIDString, TechnicalQuestion question)
        {
            IEnumerable<TechnicalQuestionTags> associatedTags = question.TechnicalQuestionTags;
            db.TechnicalQuestionTags.RemoveRange(associatedTags);
            db.SaveChanges();
            TechnicalQuestionTags questionTags;
            if (!String.IsNullOrEmpty(tagIDString))
            {
                List<string> tagIDList = tagIDString.Split(',').ToList();
                foreach (string tagID in tagIDList)
                {
                    long id = Convert.ToInt64(tagID);
                    questionTags = new TechnicalQuestionTags();
                    Tag tags = db.Tag.Find(id);
                    questionTags.Tag = tags;
                    questionTags.TechnicalQuestion = question;
                    db.TechnicalQuestionTags.Add(questionTags);
                    db.SaveChanges();
                }
            }
        }

        #region SaveAnswer

        [ValidateInput(false)]
        public void SaveAnswer(string answerContent, string isCorrectString, string questionIDString)
        {
            int questionID = 0;
            if (!string.IsNullOrEmpty(questionIDString))
                questionID = Convert.ToInt16(questionIDString);
            Answer answer = new Answer();
            answer.Description = answerContent;
            answer.IsCorrect = isCorrectString == "true" ? true : false;
            answer.LastUpdated = DateTime.Now;
            if (!string.IsNullOrEmpty(questionIDString))
                answer.Question = db.Question.Find(questionID);
            //answer.Description = htmlConverter.FormatFileContent(answer.Description);
            answer.Description = answer.Description;

            db.Answer.Add(answer);
            db.SaveChanges();
            SaveAnswerModel saveAnswerModel = new SaveAnswerModel();
            saveAnswerModel.AnswerID = answer.ID;
            saveAnswerModel.IsCorrectAnswer = isCorrectString == "true" ? true : false;
            int answerDescriptionCount = answer == null ? 0 : (answer.Description.Count() > 200 ? 200 : answer.Description.Count());
            saveAnswerModel.AnswerDescription = answer.Description.Substring(0, answerDescriptionCount);
            Response.Write(JsonConvert.SerializeObject(saveAnswerModel));
        }

        //[ValidateInput(false)]
        public void SaveTechnicalAnswer(string inputParam, string outputParam, string questionIDString)
        {
            int questionID = 0;
            if (!string.IsNullOrEmpty(questionIDString))
                questionID = Convert.ToInt16(questionIDString);
            TechnicalAnswer answer = new TechnicalAnswer();
            answer.InputParam = inputParam;
            answer.OutputParam = outputParam;
            answer.LastUpdated = DateTime.Now;
            if (!string.IsNullOrEmpty(questionIDString))
                answer.TechnicalQuestion = db.TechnicalQuestion.Find(questionID);

            db.TechnicalAnswer.Add(answer);
            db.SaveChanges();
            SaveTechnicalAnswerModel saveAnswerModel = new SaveTechnicalAnswerModel();
            saveAnswerModel.AnswerID = answer.ID;
            saveAnswerModel.InputParam = inputParam;
            saveAnswerModel.OutputParam = outputParam;
            Response.Write(JsonConvert.SerializeObject(saveAnswerModel));
        }


        //public void SaveQuestion(string answerContent, string isCorrectString, string questionIDString)
        //{
        //    int questionID = 0;
        //    if (!string.IsNullOrEmpty(questionIDString))
        //        questionID = Convert.ToInt16(questionIDString);
        //    Answer answer = new Answer();
        //    Question question = new Question();
        //    answer.Description = answerContent;     question.Description = questionIDString;        
        //    answer.IsCorrect = isCorrectString == "true" ? true : false;
        //    answer.LastUpdated = DateTime.Now;
        //    if (!string.IsNullOrEmpty(questionIDString))
        //        answer.Question = db.Question.Find(questionID);
        //    db.Answer.Add(answer);
        //    db.Question.Add(question);
        //    db.SaveChanges();
        //    SaveAnswerModel saveAnswerModel = new SaveAnswerModel();
        //    saveAnswerModel.AnswerID = answer.ID;
        //    saveAnswerModel.IsCorrectAnswer = isCorrectString == "true" ? true : false;



        //    int answerDescriptionCount = answer == null ? 0 : (answer.Description.Count() > 50 ? 50 : answer.Description.Count());
        //    saveAnswerModel.AnswerDescription = answer.Description.Substring(0, answerDescriptionCount);
        //    Response.Write(JsonConvert.SerializeObject(saveAnswerModel));
        //}

        #endregion

        public void DeleteAnswerByID(string answerIDString)
        {
            //long id = Convert.ToInt32(answerIDString);
            //Answer answer = db.Answer.Find(id);

            //if (answer != null)
            //{
            //    List<QuestionPaperSnapshot> assessmentSnapshotList = answer.QuestionPaperSnapshot.ToList();
            //    if (assessmentSnapshotList != null)
            //    {
            //        db.QuestionPaperSnapshot.RemoveRange(assessmentSnapshotList);
            //        db.SaveChanges();
            //    }

            //    db.Answer.Remove(answer);
            //    db.SaveChanges();
            //}
            //Response.Write(answer != null ? (answer.IsCorrect != null ? (answer.IsCorrect.Value == true ? "CorrectAnswerDeleted" : "NotCorrectAnswer") : "NotCorrectAnswer") : "NotCorrectAnswer");
        }

        public void GetAnswerByID(string answerIDString)
        {
            long id = Convert.ToInt32(answerIDString);
            Answer answer = db.Answer.Find(id);

            SaveAnswerModel saveAnswerModel = new SaveAnswerModel();
            if (answer != null)
            {
                saveAnswerModel.AnswerID = answer.ID;
                saveAnswerModel.IsCorrectAnswer = answer.IsCorrect == null ? false : answer.IsCorrect.Value;
                saveAnswerModel.AnswerDescription = answer.Description;
            }
            // Response.Write(JsonConvert.SerializeObject(""));
            Response.Write(JsonConvert.SerializeObject(saveAnswerModel));

        }

        [ValidateInput(false)]
        public void UpdateAnswer(string answerIDString, string answerDescription, string isCorrectString)
        {
            long id = Convert.ToInt32(answerIDString);
            Answer answer = db.Answer.Find(id);
            answer.Description = answerDescription;
            answer.IsCorrect = isCorrectString == "true" ? true : false;
            // answer.Description = htmlConverter.FormatFileContent(answer.Description);
            answer.Description = answer.Description;

            db.Entry(answer).State = EntityState.Modified;
            db.SaveChanges();

            SaveAnswerModel saveAnswerModel = new SaveAnswerModel();
            if (answer != null)
            {
                saveAnswerModel.AnswerID = answer.ID;
                saveAnswerModel.AnswerDescription = answer.Description;
                saveAnswerModel.IsCorrectAnswer = answer.IsCorrect == null ? false : answer.IsCorrect.Value;
            }
            Response.Write(JsonConvert.SerializeObject(saveAnswerModel));
        }

        [ValidateInput(false)]
        public void AddAnswer(string questionIDString, string answerContent, string isCorrectString)
        {
            long questionID = Convert.ToInt32(questionIDString);
            Question question = db.Question.Find(questionID);
            SaveAnswerModel saveAnswerModel = new SaveAnswerModel();
            if (question != null)
            {
                Answer answer = new Answer();
                answer.Description = answerContent;
                answer.IsCorrect = isCorrectString == "true" ? true : false;
                answer.Question = question;
                // answer.Description = htmlConverter.FormatFileContent(answer.Description);
                answer.Description = answer.Description;

                db.Answer.Add(answer);
                db.SaveChanges();

                saveAnswerModel.AnswerID = answer.ID;
                saveAnswerModel.AnswerDescription = answer.Description;
                saveAnswerModel.IsCorrectAnswer = answer.IsCorrect == null ? false : answer.IsCorrect.Value;
            }
            Response.Write(JsonConvert.SerializeObject(saveAnswerModel));
        }

        #region Detail

        public void GetQuestionDetail(int id)
        {
            //Question question = db.Question.Find(id);
            //QuestionViewModel questionViewModel = MapperHelper.MapToViewModel(question);
            //questionViewModel.AnswerViewModelList = MapperHelper.MapToViewModelList(question.Answer.AsQueryable());
            //string result = RenderPartialViewToString("~/Views/Question/_QuestionDetail.cshtml", questionViewModel);
            //Response.Write(result);
        }

        public string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");
            ViewData.Model = model;
            using (StringWriter sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                return sw.GetStringBuilder().ToString();
            }
        }

        #endregion

        #region Delete

        public void Delete(string questionIDString)
        {
            bool result = true;
            try
            {
                if (!string.IsNullOrEmpty(questionIDString))
                {
                    List<int> questionIDList = questionIDString.Split(',').Select(int.Parse).ToList();
                    List<Question> questionList = new List<Question>();
                    foreach (int questionID in questionIDList)
                    {
                        Question question = db.Question.Find(questionID);
                        List<QuestionPaperSectionQuestion> assessmentSectionQuestionList = question.QuestionPaperSectionQuestion.ToList();
                        List<QuestionPaperQuestion> assessmentQuestionList = question.QuestionPaperQuestion.ToList();
                        db.QuestionPaperSectionQuestion.RemoveRange(assessmentSectionQuestionList);
                        db.QuestionPaperQuestion.RemoveRange(assessmentQuestionList);

                        List<PassageQuestionOrder> passageQuestionOrders = question.PassageQuestionOrder.ToList();
                        db.PassageQuestionOrder.RemoveRange(passageQuestionOrders);
                        db.SaveChanges();

                        List<AssessmentSnapshot> assessmentSnapshotList = new List<AssessmentSnapshot>();
                        assessmentSnapshotList.AddRange(question.AssessmentSnapshot.ToList());
                        //db.SaveChanges();
                        db.QuestionTags.RemoveRange(question.QuestionTags);
                        db.SaveChanges();
                        questionList.Add(question);

                        if (question.Answer.Count() > 0)
                        {                                       //Question having answer
                            List<Answer> answerList = question.Answer.ToList();
                            if (answerList != null)
                            {
                                assessmentSnapshotList = new List<AssessmentSnapshot>();
                                assessmentSnapshotList.AddRange(question.Answer.SelectMany(x => x.AssessmentSnapshot).ToList());

                                List<UserAssessmentSnapshot> userQuestionPaperSnapShotList = assessmentSnapshotList.SelectMany(x => x.UserAssessmentSnapshot).ToList();

                                db.UserAssessmentSnapshot.RemoveRange(userQuestionPaperSnapShotList);
                                db.SaveChanges();
                                db.AssessmentSnapshot.RemoveRange(assessmentSnapshotList);
                                db.SaveChanges();
                            }
                            db.Answer.RemoveRange(question.Answer);
                            db.SaveChanges();

                        }
                        db.Question.Remove(question);
                        db.SaveChanges();
                    }
                    //  db.Question.RemoveRange(questionList);
                    db.SaveChanges();
                }
            }
            catch
            {
                result = false;
            }
            Response.Write(result);
        }

        #endregion

        #region Correcting Answers

        //public ActionResult Corrections(long ID=0)
        //{
        //    BatchQuestionPaper batchQuestionPaper = db.BatchQuestionPaper.Find(ID);
        //    QuestionViewModel questionViewModel = new QuestionViewModel();
        //    if (batchQuestionPaper != null)
        //    {
        //        QuestionPaper questionPapers = batchQuestionPaper.QuestionPaper;
        //        Batch batch = batchQuestionPaper.Batch;
        //        if (batch != null)
        //        {
        //            College college = batch.College;
        //            ViewBag.Colleges = db.College);
        //            ViewBag.QuestionPapers = db.QuestionPaper);
        //            questionViewModel.questionPaperID = questionPapers.ID;
        //            questionViewModel.CollegeID = college.ID;
        //            questionViewModel.batchID = batch.ID;
        //            ViewBag.Batches = db.Batch.Where(c => c.CollegeID == college.ID).OrderBy(c => c.Name);
        //        }
        //        else
        //        {
        //            ViewBag.Colleges = db.College);
        //            ViewBag.QuestionPapers = db.QuestionPaper);
        //            ViewBag.Batches = new List<Batch>();
        //        }
        //    }
        //    else
        //    {
        //        ViewBag.Colleges = db.College);
        //        ViewBag.QuestionPapers = db.QuestionPaper);
        //        ViewBag.Batches = new List<Batch>();
        //    }
        //    ViewBag.Staffs = sh.SortUserProfile(db.UserProfile.Where(up => up.webpages_Roles.Where(wpr => wpr.RoleName.ToLower().Equals("staff")).Count() > 0));

        //    return View(questionViewModel);
        //}

        //public void CorrectionList(string selectedColleges, string selectedQuestionPapers, string selectedBatches)
        //{
        //    List<int> selectedBatchIDList = new List<int>();
        //    if (!string.IsNullOrEmpty(selectedBatches) && !selectedBatches.ToLower().Equals("undefined") && (!selectedBatches.ToLower().Equals("null")))
        //        selectedBatchIDList = selectedBatches.Split(',').Select(int.Parse).ToList();

        //    string[] correctionColumns = { "Question", "RegistrationNumber" };

        //    datatableRequest = new DataTableRequest(Request, correctionColumns);

        //    List<QuestionPaperSnapshot> emptyQuestionPaperSnapshot = new List<QuestionPaperSnapshot>();

        //    IQueryable<QuestionPaperSnapshot> assessmentSnapShots = db.QuestionPaperSnapshot.Where(asn => asn.Question.QuestionType.Name.ToLower().Equals("open ended") && asn.Marks == null && asn.UserQuestionPaperSnapshot.Count() <= 0);
        //    int totalRecords = assessmentSnapShots.Count();
        //    int filteredCount = totalRecords;

        //    if (selectedBatchIDList.Count() > 0)
        //        assessmentSnapShots = assessmentSnapShots.Where(asn => asn.UserQuestionPaper.QuestionPaper.BatchQuestionPaper.Where(ba => (selectedBatchIDList.Contains(ba.BatchID))).Count() > 0);
        //    else
        //    {                                                       //Batches not selected
        //        //assessmentSnapShots = emptyQuestionPaperSnapshot.AsQueryable();
        //        if ((string.IsNullOrEmpty(selectedColleges) && string.IsNullOrEmpty(selectedQuestionPapers)) || (selectedColleges.ToLower().Equals("undefined") && selectedQuestionPapers.ToLower() == "undefined"))
        //        {                                                   //Collges and questionpapers not selected show all assessment snapshots
        //        }
        //        else if (string.IsNullOrEmpty(selectedColleges) || string.IsNullOrEmpty(selectedQuestionPapers))
        //        {                                                   //Colleges or questionpapers selected
        //            assessmentSnapShots = emptyQuestionPaperSnapshot.AsQueryable();
        //        }
        //        else
        //        {
        //            assessmentSnapShots = emptyQuestionPaperSnapshot.AsQueryable();
        //        }
        //    }

        //    if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
        //    {
        //        assessmentSnapShots = (from asn in assessmentSnapShots
        //                               where asn.Question.Description.Contains(datatableRequest.SearchString)
        //                                   || asn.UserQuestionPaper.UserProfile.Student.Name.Contains(datatableRequest.SearchString)
        //                                   || asn.UserQuestionPaper.UserProfile.Student.UniversityRegisterNumber.Contains(datatableRequest.SearchString)
        //                               select asn);
        //        filteredCount = assessmentSnapShots.Count();
        //    }
        //    assessmentSnapShots = assessmentSnapShots.OrderBy(x => x.ID);



        //    assessmentSnapShots = assessmentSnapShots.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
        //    List<QuestionPaperSnapShotViewModel> assessmentSnapShotViewModelList = MapperHelper.MapToViewModelList(assessmentSnapShots);
        //    IQueryable<QuestionPaperSnapShotViewModel> assessmentSnapShotViewModels = assessmentSnapShotViewModelList.AsQueryable();
        //    if (datatableRequest.ShouldOrder)
        //    {
        //        assessmentSnapShotViewModelList = assessmentSnapShotViewModels.AsQueryable().OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending).ToList();
        //    }

        //    QuestionPaperSnapShotDataTableViewModel dataTableViewModel = new QuestionPaperSnapShotDataTableViewModel(assessmentSnapShotViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

        //    string result = JsonConvert.SerializeObject(dataTableViewModel);
        //    Response.Write(result);
        //}

        //public void GetBatchUnderCollegeAndQuestionPaper(string collegeIDString, string assessmentIDString)
        //{
        //    IQueryable<Batch> batchList = null;
        //    if (!string.IsNullOrEmpty(collegeIDString) && !string.IsNullOrEmpty(assessmentIDString))
        //    {                                                                   //Colleges and batches selected
        //        List<long> selectedCollegeIDList = collegeIDString.Split(',').Select(long.Parse).ToList();
        //        List<long> selectedQuestionPaperIDList = assessmentIDString.Split(',').Select(long.Parse).ToList();

        //        batchList = db.Batch.Where(b => b.BatchQuestionPaper.Where(ba => (selectedQuestionPaperIDList.Contains(ba.QuestionPaperID))).Count() > 0 && selectedCollegeIDList.Contains(b.CollegeID != null ? b.CollegeID.Value : 0));
        //    }
        //    else
        //    {                                                                   //College or batch not selected

        //    }
        //    List<BatchViewModel> batchViewModelList;
        //    if (batchList != null)
        //    {
        //        batchViewModelList = MapperHelper.MapToViewModelList(batchList);
        //        batchViewModelList = sh.SortBatchViewModelList(batchViewModelList);
        //    }
        //    else
        //        batchViewModelList = new List<BatchViewModel>();

        //    Response.Write(JsonConvert.SerializeObject(batchViewModelList));
        //}

        //public void AssignQuestionPaperSnapshotToStaff(string selectedStaffIDString, string selectedQuestionPaperSnapshots)
        //{
        //    if (!string.IsNullOrEmpty(selectedStaffIDString) && !string.IsNullOrEmpty(selectedQuestionPaperSnapshots))
        //    {
        //        int selectedUserID = Convert.ToInt32(selectedStaffIDString);
        //        UserProfile selectedUserProfile = db.UserProfile.Find(selectedUserID);
        //        UserProfile createdUserProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
        //        List<int> selectedQuestionPaperSnapshotIDList = selectedQuestionPaperSnapshots.Split(',').Select(int.Parse).ToList();
        //        foreach (int assessmentSnapShotID in selectedQuestionPaperSnapshotIDList)
        //        {
        //            QuestionPaperSnapshot assessmentSnapshot = db.QuestionPaperSnapshot.Find(assessmentSnapShotID);
        //            UserQuestionPaperSnapshot userQuestionPaperSnapshot = new UserQuestionPaperSnapshot();
        //            userQuestionPaperSnapshot.UserProfile = selectedUserProfile;
        //            userQuestionPaperSnapshot.QuestionPaperSnapshot = assessmentSnapshot;
        //            userQuestionPaperSnapshot.CreatedUserID = createdUserProfile.UserId;
        //            userQuestionPaperSnapshot.CreatedDate = DateTime.Now;
        //            db.UserQuestionPaperSnapshot.Add(userQuestionPaperSnapshot);
        //        }
        //        db.SaveChanges();
        //    }

        //}

        #endregion

        #region EvaluateAnswer for Open ended questions

        //public ActionResult EvaluateAnswerList()
        //{
        //    UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower().Equals(User.Identity.Name.ToLower())).FirstOrDefault();
        //    List<UserQuestionPaperSnapshotViewModel> userQuestionPaperSnapshotViewModelList = new List<UserQuestionPaperSnapshotViewModel>();
        //    userQuestionPaperSnapshotViewModelList = MapperHelper.MapToViewModelList(userProfile.UserQuestionPaperSnapshot.AsQueryable());

        //    ViewBag.Questions = sh.SortQuestion(userProfile.UserQuestionPaperSnapshot.Where(uan => uan.QuestionPaperSnapshot.Question.QuestionType.Name.ToLower().Equals("open ended")).Select(x => x.QuestionPaperSnapshot.Question).ToList());
        //    ViewBag.UserProfile = sh.SortUserProfile(userProfile.UserQuestionPaperSnapshot.Where(uan => uan.QuestionPaperSnapshot.Question.QuestionType.Name.ToLower().Equals("open ended")).Select(x => x.QuestionPaperSnapshot.UserQuestionPaper.UserProfile).Distinct().AsQueryable());
        //    return View(userQuestionPaperSnapshotViewModelList);
        //}

        //public ActionResult EvaluateAnswer(int id = 0, string sort = "", int sortID = 0)
        //{
        //    int currentIndex = 0;
        //    int totalRecords = 0;
        //    UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower().Equals(User.Identity.Name.ToLower())).FirstOrDefault();
        //    UserQuestionPaperSnapshotViewModel userQuestionPaperSnapshotViewModel = new UserQuestionPaperSnapshotViewModel();
        //    if (userProfile != null)
        //    {
        //        //totalRecords = userProfile.UserQuestionPaperSnapshot.Count();

        //        if (id > 0)
        //            currentIndex = userProfile.UserQuestionPaperSnapshot.ToList().FindIndex(x => x.ID == id);
        //        //UserQuestionPaperSnapshot userQuestionPaperSnapshot = userProfile.UserQuestionPaperSnapshot.ToList()[currentIndex];
        //        //userQuestionPaperSnapshotViewModel = MapperHelper.MapToViewModel(userQuestionPaperSnapshot);
        //        bool isCorrectByQuestion = false;
        //        bool isCorectByStudent = false;
        //        if (sort.ToLower().Equals("question"))
        //            isCorrectByQuestion = true;
        //        else if (sort.ToLower().Equals("student"))
        //            isCorectByStudent = true;

        //        userQuestionPaperSnapshotViewModel = GetUserQuestionPaperSnapshotViewModelByCriteria(userProfile, currentIndex, isCorrectByQuestion, isCorectByStudent, ref totalRecords, sortID);
        //        if (totalRecords > 0)
        //        {
        //            HelperClasses.HtmlFormatter htmlFormatter = new HtmlFormatter();

        //            string answerSolution = "";
        //            if (!string.IsNullOrEmpty(userQuestionPaperSnapshotViewModel.AnswerSolution))
        //                answerSolution = htmlFormatter.GetContentFromHtml(userQuestionPaperSnapshotViewModel.AnswerSolution);

        //            string[] keyList = answerSolution.Split(',').Select(x => x.ToLower()).ToArray();

        //            string[] userAnswered = userQuestionPaperSnapshotViewModel.UserAnswered.Split(' ').Select(x => x.ToLower()).ToArray();

        //            int correctKeyCount = keyList.Intersect(userAnswered).Count();

        //            userQuestionPaperSnapshotViewModel.KeywordCount = keyList.Count();
        //            userQuestionPaperSnapshotViewModel.MatchedKeywordCount = correctKeyCount;

        //            userQuestionPaperSnapshotViewModel.TotalRecords = totalRecords;
        //            userQuestionPaperSnapshotViewModel.CurrentIndex = currentIndex;

        //           // userQuestionPaperSnapshotViewModel.ScoredAssumptionMarks = CalculateMarkForOpenEndedQuestion(userQuestionPaperSnapshotViewModel.Marks, keyList.Count(), correctKeyCount);

        //            //userQuestionPaperSnapshotViewModel.UserAnswered = htmlFormatter.GetContentFromHtmlWithOutReplaceSpace(userQuestionPaperSnapshotViewModel.UserAnswered);
        //            //userQuestionPaperSnapshotViewModel.AnswerSolution = htmlFormatter.GetContentFromHtmlWithOutReplaceSpace(userQuestionPaperSnapshotViewModel.AnswerSolution);

        //            return View(userQuestionPaperSnapshotViewModel);
        //        }
        //        else
        //        {
        //            return RedirectToAction("EvaluateAnswerList");
        //        }
        //    }
        //    return RedirectToAction("EvaluateAnswerList");

        //}

        //        [HttpPost]
        //        [ValidateInput(false)]
        //        public ActionResult EvaluateAnswer(UserQuestionPaperSnapshotViewModel userQuestionPaperSnapshotViewModel, string save)
        //        {
        //            UserQuestionPaperSnapshot userQuestionPaperSnapshot = db.UserQuestionPaperSnapshot.Find(userQuestionPaperSnapshotViewModel.ID);
        //            userQuestionPaperSnapshot.QuestionPaperSnapshot.Marks = userQuestionPaperSnapshotViewModel.MarksScored;
        //            db.Entry(userQuestionPaperSnapshot.QuestionPaperSnapshot).State = EntityState.Modified;

        //            userQuestionPaperSnapshot.QuestionPaperSnapshot.UserQuestionPaper.Marks = userQuestionPaperSnapshot.QuestionPaperSnapshot.UserQuestionPaper.Marks + userQuestionPaperSnapshotViewModel.MarksScored;
        //            db.Entry(userQuestionPaperSnapshot.QuestionPaperSnapshot.UserQuestionPaper).State = EntityState.Modified;

        //            db.SaveChanges();

        //            if (save.ToLower().Equals("finish"))
        //            {                                                   //Last record 
        //                return RedirectToAction("EvaluateAnswerList");
        //            }
        //            else
        //            {
        //                int currentIndex = userQuestionPaperSnapshotViewModel.CurrentIndex;

        //                if (save.ToLower().Equals("prev"))
        //                {                                   //Prev button clicked
        //                    currentIndex--;
        //                }
        //                else if (save.ToLower().Equals("next"))
        //                {
        //                    currentIndex++;
        //                }
        //                int totalRecords = 0;
        //                int sortID = userQuestionPaperSnapshotViewModel.SortID;
        //                UserProfile userProfile = userQuestionPaperSnapshot.UserProfile;
        //                //if (userQuestionPaperSnapshotViewModel.TotalRecords == 0)
        //                //{
        //                //    totalRecords = userProfile.UserQuestionPaperSnapshot.Count();
        //                //}
        //                //else
        //                //{
        //                //    totalRecords = userQuestionPaperSnapshotViewModel.TotalRecords;
        //                //}

        //                ModelState.Clear();

        //                //if (currentIndex < totalRecords)
        //                //{
        //                //UserQuestionPaperSnapshot currentUserQuestionPaperSnapshot = new UserQuestionPaperSnapshot();
        //                //currentUserQuestionPaperSnapshot = userProfile.UserQuestionPaperSnapshot.ToList()[currentIndex];
        //                //userQuestionPaperSnapshotViewModel = MapperHelper.MapToViewModel(currentUserQuestionPaperSnapshot);
        //decimal marks = userQuestionPaperSnapshotViewModel.Marks;
        //                userQuestionPaperSnapshotViewModel = GetUserQuestionPaperSnapshotViewModelByCriteria(userProfile, currentIndex, userQuestionPaperSnapshotViewModel.IsCorrectByQuestion, userQuestionPaperSnapshotViewModel.IsCorrectByStudent, ref totalRecords);
        //                if (userQuestionPaperSnapshotViewModel.Marks == 0)
        //                {
        //                    userQuestionPaperSnapshotViewModel.Marks = marks;
        //                }
        //                //}

        //                HelperClasses.HtmlFormatter htmlFormatter = new HtmlFormatter();

        //                string answerSolution = htmlFormatter.GetContentFromHtml(userQuestionPaperSnapshotViewModel.AnswerSolution);

        //                string[] keyList = answerSolution.Split(',').Select(x => x.ToLower()).ToArray();

        //                string[] userAnswered = userQuestionPaperSnapshotViewModel.UserAnswered.Split(' ').Select(x => x.ToLower()).ToArray();

        //                int correctKeyCount = keyList.Intersect(userAnswered).Count();

        //                userQuestionPaperSnapshotViewModel.KeywordCount = keyList.Count();
        //                userQuestionPaperSnapshotViewModel.MatchedKeywordCount = correctKeyCount;

        //                userQuestionPaperSnapshotViewModel.TotalRecords = totalRecords;
        //                userQuestionPaperSnapshotViewModel.CurrentIndex = currentIndex;

        //                userQuestionPaperSnapshotViewModel.ScoredAssumptionMarks = CalculateMarkForOpenEndedQuestion(userQuestionPaperSnapshotViewModel.Marks, keyList.Count(), correctKeyCount);

        //                //userQuestionPaperSnapshotViewModel.AnswerSolution = htmlFormatter.GetContentFromHtmlWithOutReplaceSpace(userQuestionPaperSnapshotViewModel.AnswerSolution);
        //                return View(userQuestionPaperSnapshotViewModel);
        //            }
        //        }

        //        public UserQuestionPaperSnapshotViewModel GetUserQuestionPaperSnapshotViewModelByCriteria(UserProfile userProfile, int currentIndex, bool isCorrectByQuestions, bool isCorrectByStudent, ref int totalRecords, int sortID = 0)
        //        {
        //            UserQuestionPaperSnapshot currentUserQuestionPaperSnapshot;
        //            List<UserQuestionPaperSnapshot> userQuestionPaperSnapshotList = new List<UserQuestionPaperSnapshot>();

        //            if (isCorrectByQuestions)
        //            {                                                   //User selected correct by question
        //                if (sortID > 0)
        //                {                                           //User selected a particular question
        //                    currentUserQuestionPaperSnapshot = userProfile.UserQuestionPaperSnapshot.Where(x => x.QuestionPaperSnapshot.Question.ID == sortID).OrderBy(x => x.QuestionPaperSnapshot.Question.ID).ToList()[currentIndex];
        //                    totalRecords = userProfile.UserQuestionPaperSnapshot.Where(x => x.QuestionPaperSnapshot.Question.ID == sortID).OrderBy(x => x.QuestionPaperSnapshot.Question.ID).Count();
        //                }
        //                else
        //                {                                           //User selected all question
        //                    currentUserQuestionPaperSnapshot = userProfile.UserQuestionPaperSnapshot.OrderBy(x => x.QuestionPaperSnapshot.Question.ID).ToList()[currentIndex];
        //                    totalRecords = userProfile.UserQuestionPaperSnapshot.OrderBy(x => x.QuestionPaperSnapshot.Question.ID).Count();
        //                }
        //            }
        //            else if (isCorrectByStudent)
        //            {                                                   //User selected correct by student
        //                if (sortID > 0)
        //                {                                           //User selected particular student
        //                    currentUserQuestionPaperSnapshot = userProfile.UserQuestionPaperSnapshot.Where(uan => uan.QuestionPaperSnapshot.UserQuestionPaper.UserProfile.UserId == sortID).OrderBy(x => x.QuestionPaperSnapshot.UserQuestionPaper.UserProfile.UserId).ToList()[currentIndex];
        //                    totalRecords = userProfile.UserQuestionPaperSnapshot.Where(uan => uan.QuestionPaperSnapshot.UserQuestionPaper.UserProfile.UserId == sortID).OrderBy(x => x.QuestionPaperSnapshot.UserQuestionPaper.UserProfile.UserId).Count();
        //                }
        //                else
        //                {                                           //user selected all student
        //                    currentUserQuestionPaperSnapshot = userProfile.UserQuestionPaperSnapshot.OrderBy(x => x.QuestionPaperSnapshot.UserQuestionPaper.UserProfile.UserId).ToList()[currentIndex];
        //                    totalRecords = userProfile.UserQuestionPaperSnapshot.OrderBy(x => x.QuestionPaperSnapshot.UserQuestionPaper.UserProfile.UserId).Count();
        //                }

        //            }
        //            else
        //            {
        //                currentUserQuestionPaperSnapshot = userProfile.UserQuestionPaperSnapshot.OrderBy(x => x.ID).ToList()[currentIndex];
        //                totalRecords = userProfile.UserQuestionPaperSnapshot.Count();
        //            }

        //            //UserQuestionPaperSnapshot userQuestionPaperSnapshot = userProfile.UserQuestionPaperSnapshot.ToList()[currentIndex];
        //            int currentQuestionID = currentUserQuestionPaperSnapshot.QuestionPaperSnapshot.QuestionID ?? 0;


        //            decimal marks = 0;
        //            QuestionPaper currentQuestionPaper = currentUserQuestionPaperSnapshot.QuestionPaperSnapshot.UserQuestionPaper.QuestionPaper;

        //            QuestionPaperSectionQuestion currentQuestionPaperSectionQuestion = currentQuestionPaper.QuestionPaperSection.Where(ass => ass.QuestionPaperSectionQuestion.Where(asq => asq.QuestionID == currentQuestionID).Count() > 0).SelectMany(x => x.QuestionPaperSectionQuestion).FirstOrDefault();

        //            List<QuestionPaperSectionQuestion> currentQuestionPaperSectionQuestionList = currentQuestionPaper.QuestionPaperSection.Where(ass => ass.QuestionPaperSectionQuestion.Where(asq => asq.QuestionID == currentQuestionID && asq.QuestionPaperSection.QuestionPaperID == currentQuestionPaper.ID).Count() > 0).SelectMany(x => x.QuestionPaperSectionQuestion).ToList();
        //            List<QuestionPaperSection> currentQuestionPaperSectionList = currentQuestionPaperSectionQuestionList.Where(asq => asq.QuestionPaperSection.QuestionPaperID == currentQuestionPaper.ID).Select(x => x.QuestionPaperSection).ToList();
        //            //QuestionPaperSectionQuestion test=currentQuestionPaperSectionList.Where()
        //            //QuestionPaperSectionQuestion currentQuestionPaperSectionQuestion=currentQuestionPaperSectionQuestionList.Where(asq=>asq.QuestionPaperSection.QuestionPaperID==currentQuestionPaper.ID && asq.QuestionID==currentQuestionID).FirstOrDefault();

        //            if (currentQuestionPaperSectionQuestion != null && currentQuestionPaperSectionQuestion.Mark != null)
        //                marks = currentQuestionPaperSectionQuestion.Mark ?? 0;
        //            else if (currentUserQuestionPaperSnapshot.QuestionPaperSnapshot.Question.Marks != null)
        //                marks = currentUserQuestionPaperSnapshot.QuestionPaperSnapshot.Question.Marks ?? 0;

        //            UserQuestionPaperSnapshotViewModel userQuestionPaperSnapshotViewModel = MapperHelper.MapToViewModel(currentUserQuestionPaperSnapshot);
        //            userQuestionPaperSnapshotViewModel.IsCorrectByQuestion = isCorrectByQuestions;
        //            userQuestionPaperSnapshotViewModel.IsCorrectByStudent = isCorrectByStudent;
        //            userQuestionPaperSnapshotViewModel.SortID = sortID;
        //            //userQuestionPaperSnapshotViewModel.Marks = marks;
        //if (marks == 0)
        //            {
        //                userQuestionPaperSnapshotViewModel.Marks = currentUserQuestionPaperSnapshot.QuestionPaperSnapshot.Marks??0;
        //            }
        //            else
        //            {
        //                userQuestionPaperSnapshotViewModel.Marks = marks;
        //            }

        //            HtmlFormatter htmlFormatter = new HtmlFormatter();
        //            //userQuestionPaperSnapshotViewModel.UserAnswered = htmlFormatter.GetContentFromHtmlWithOutReplaceSpace(userQuestionPaperSnapshotViewModel.UserAnswered);
        //            userQuestionPaperSnapshotViewModel.AnswerSolution = htmlFormatter.GetContentFromHtmlWithOutReplaceSpace(userQuestionPaperSnapshotViewModel.AnswerSolution);

        //            return userQuestionPaperSnapshotViewModel;
        //        }

        //        public decimal CalculateMarkForOpenEndedQuestion(decimal marks, int totalKeywords, int matchedKeywords)
        //        {
        //            decimal scoredMarks = 0;
        //            scoredMarks = (marks / totalKeywords) * matchedKeywords;
        //            return scoredMarks;
        //        }

        #endregion

    }

    public class SaveAnswerModel
    {

        public long QuestionID { get; set; }

        public long AnswerID { get; set; }

        public bool IsCorrectAnswer { get; set; }

        public string AnswerDescription { get; set; }

    }

    public class SaveTechnicalAnswerModel
    {

        public long QuestionID { get; set; }

        public long AnswerID { get; set; }

        public string InputParam { get; set; }

        public string OutputParam { get; set; }

    }
    //public class SaveQuestionModel
    //{

    //    public long QuestionID { get; set; }

    //    public long AnswerID { get; set; }

    //    public bool IsCorrectAnswer { get; set; }

    //    public string AnswerDescription { get; set; }

    //}

}
