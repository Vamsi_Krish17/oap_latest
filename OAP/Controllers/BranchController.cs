﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OAP.Domain;
using OAP.Domain.ViewModel;
using HelperClasses;
using Newtonsoft.Json;
using OAP.Domain.ViewModel.DataTableViewModel;



namespace OAP.Controllers
{
    [Authorize(Roles = "SuperAdmin, CollegeAdmin, Admin")]
    public class BranchController : BaseController
    {
        private OAPEntities db = new OAPEntities();
        public string pageLimitString = System.Configuration.ConfigurationManager.AppSettings["pageLimit"];

        #region Index

        [Authorize]
        public ActionResult Index()
        {
            //IEnumerable<Branch> branches = db.Branch;
            //List<BranchViewModel> branchViewModelList = MapperHelper.MapToViewModelList(branches,1);
            //return View(branchViewModelList);
            return View();
        }

        public void List()
        {
            string[] columns = { "Name" };
            datatableRequest = new DataTableRequest(Request, columns);

           // IQueryable<Branch> branches = db.Branch;
            IQueryable<Branch> branches = (from b in db.Branch
                                         group b by new
                                         {
                                             b.Name
                                         } into bt
                                         select bt.FirstOrDefault());

            int totalRecords = branches.Count();
            int filteredCount = totalRecords;
            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                branches = (from q in db.Branch where q.Name.Contains(datatableRequest.SearchString) select q);
                filteredCount = branches.Count();
            }
            branches = branches.OrderBy(x => x.Name);
            if (datatableRequest.ShouldOrder)
            {
                branches = branches.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            }
            branches = branches.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            List<BranchViewModel> branchViewModelList = MapperHelper.MapToViewModelList(branches);
            BranchDataTableViewModel dataTableViewModel = new BranchDataTableViewModel(branchViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        #endregion

        #region Details

        //
        // GET: /Branch/Details/5
        [Authorize]
        public ActionResult Details(long id = 0)
        {
            Branch branch = db.Branch.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            BranchViewModel branchViewModel = MapperHelper.MapToViewModel(branch);
            return View(branchViewModel);
        }

        #endregion

        #region Create

        [Authorize]
        public ActionResult Create()
        {
            BranchViewModel branchViewModel = new BranchViewModel();
            ViewBag.Years = GetYears(0);
            //branchViewModel.CollegeID = id;
            return View(branchViewModel);
        }

        public List<SelectListItem> GetYears(int selectedYear)
        {
            List<SelectListItem> yearListItem = new List<SelectListItem>();
            for (int i = 1; i < 10; i++)
            {
                SelectListItem selectListItem = new SelectListItem();
                selectListItem.Text = i.ToString();
                selectListItem.Value= i.ToString();
                if (i == selectedYear)
                    selectListItem.Selected = true;
                yearListItem.Add(selectListItem);
            }
            return yearListItem;
        }

        [HttpPost]
        public ActionResult Create(BranchViewModel branchViewModel)
        {
            Branch branch = MapperHelper.MapToDomainObject(db, branchViewModel);
            if (ModelState.IsValid)
            {
                branch.CreatedDate = DateTime.Now;
                branch.CreatedBy = GetCurrentUserID();
                branch.ModifiedDate = DateTime.Now;
                branch.ModifiedBy = GetCurrentUserID();
                db.Branch.Add(branch);
                db.SaveChanges();
                return RedirectToAction("Index");
                //return RedirectToAction("AddDetails", "College", new { id = branchViewModel.CollegeID});
            }
            ViewBag.Years = GetYears(branchViewModel.Years);
            return View(branchViewModel);
        }

        #endregion

        #region Edit

        
        [Authorize]
        public ActionResult Edit(long id = 0)
        {
            Branch branch = db.Branch.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            BranchViewModel branchViewModel = MapperHelper.MapToViewModel(branch);
            ViewBag.Years = GetYears(branch.Years??0);
            return View(branchViewModel);
        }

        [HttpPost]
        public ActionResult Edit(BranchViewModel branchViewModel)
        {
            Branch branch = db.Branch.Find(branchViewModel.ID);
            branch = MapperHelper.MapToDomainObject(db, branchViewModel, branch);
            if (ModelState.IsValid)
            {
                branch.ModifiedDate = DateTime.Now;
                branch.ModifiedBy = GetCurrentUserID();
                db.Entry(branch).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Years = GetYears(branch.Years??0);
            return View(branchViewModel);
        }

        #endregion

        #region Delete

        [Authorize]
        public ActionResult Delete(long id = 0)
        {
            Branch branch = db.Branch.Find(id);
            if (branch == null)
            {
                return HttpNotFound();
            }
            BranchViewModel branchViewModel = MapperHelper.MapToViewModel(branch);
            return View(branchViewModel);
        }


        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(long id)
        {
            Branch branch = db.Branch.Find(id);
            db.Branch.Remove(branch);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public void DeleteBranches(string branchIDString)
        {
            bool result = true;
            if (!string.IsNullOrEmpty(branchIDString))
            {
                try {
                    List<long> branchIDList = branchIDString.Split(',').Select(long.Parse).ToList();
                    foreach (long branchID in branchIDList )
                    {
                        Branch branch = db.Branch.Find(branchID);

                        db.CollegeBranch.RemoveRange(branch.CollegeBranch);
                        List<Student> studentList = branch.Student.ToList();
                        foreach (Student student in studentList)
                        {
                            student.Branch = null;
                            db.Entry(student).State = EntityState.Modified;
                        }
                        db.Branch.Remove(branch);
                    }

                    db.SaveChanges();
                }
                catch { result = false; }
            }
            else
            { result = false; }
            Response.Write(result);
        }

        #endregion

        #region Reference

        //public ActionResult UpdateTransactionLimit(string sortOrder, int totalPages, int pageNo = 1)
        //{
        //    ViewBag.NameSortParam = String.IsNullOrEmpty(sortOrder) ? sortOrder : "Name_Asc";
        //    ViewBag.NameSortParam = sortOrder == "Name_Asc" ? "Name_Desc" : "Name_Asc";

        //    int pageLimit = Convert.ToInt16(pageLimitString);

        //    IEnumerable<Branch> branches = (from mtl in db.Branch
        //                                    select mtl).OrderBy(x => x.ID).Skip(pageLimit * pageNo).Take(pageLimit);

        //    totalPages = (totalPages == null || totalPages == 0) ? (db.Branch.Count() / pageLimit) - 1 : totalPages;
        //    //ViewBag.TotalPages = totalPages;
        //    //ViewBag.CurrentPageNo = pageNo;

        //    List<Branch> merchantTransactionLimitList = branches.ToList();

        //    int rowNumber = ((pageNo - 1) * 10) + 1;

        //    List<BranchViewModel> branchViewModelList = MapperHelper.MapToViewModelList(branches, rowNumber);

        //    string performSort = ViewBag.NameSortParam;

        //    if (!String.IsNullOrEmpty(sortOrder))
        //    {
        //        switch (performSort)
        //        {
        //            case "Name_Asc":
        //                branchViewModelList = branchViewModelList.OrderBy(x => x.Name).ToList();
        //                break;
        //            case "Name_Desc":
        //                branchViewModelList = branchViewModelList.OrderByDescending(x => x.Name).ToList();
        //                break;
        //        }
        //    }


        //    return View(branchViewModelList);
        //}

        #endregion

        public void GetEndYearRangeByBranch(string branchIDString, string startYearString)
        {
            //long branchID = Convert.ToInt32(branchIDString);
            int startYear = Convert.ToInt32(startYearString);
            //BranchRepository _branchRepository = new BranchRepository();
            //Branch branch = _branchRepository.GetBranchByID(branchID);
            //int branchYear = branch!=null?branch.Years != null ? branch.Years.Value : 0:0;
            int branchYear = 4;
            List<SelectListItem> endYearListItem = GetYearRangeByYear(startYear + branchYear);

            var result1 = JsonConvert.SerializeObject(endYearListItem);
            Response.Write(result1);
        }

        public List<SelectListItem> GetYearRangeByYear(int year)
        {
            List<SelectListItem> yearListItem = new List<SelectListItem>();
            for (int i = year - 5; i <= year + 5; i++)
            {
                SelectListItem yearItem = new SelectListItem();
                yearItem.Text = i.ToString();
                yearItem.Value = i.ToString();
                if (i == year)
                {
                    yearItem.Selected = true;
                }
                yearListItem.Add(yearItem);
            }
            return yearListItem;
        }
      
        #region Get Branch Under Assessment

        public void GetBranchUnderAssessment(int assessmentID)
        {
            QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentID);
            List<BranchViewModel> branchViewModelList = new List<BranchViewModel>();
            UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            List<long> collegeIdList = userProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();

            List<Branch> userProfileBranchList = new List<Branch>();

            if (questionPaper != null && userProfile != null)
            {
                if (User.IsInRole("Admin"))
                {
                    userProfileBranchList = questionPaper.UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Select(x => x.UserProfile.Student.Branch).Where(b=>b!=null).Distinct().ToList();
                }
                else if (User.IsInRole("InstituteAdmin"))
                {
                    List<College> userProfileCollegeList = userProfile.UserProfileCollege.Select(x => x.College).Distinct().ToList();
                    userProfileBranchList =questionPaper.UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID && (collegeIdList.Contains(ua.UserProfile.Student.CollegeID ?? 0))).Select(x => x.UserProfile.Student.Branch).Distinct().ToList();
                    //userProfileBranchList = userProfileCollegeList.SelectMany(upc => upc.CollegeBranch).Distinct().Select(x => x.Branch).Where(b => b.Student.Where(s => s.UserProfile.Where(up => up.UserAssessment.Where(ua => ua.AssessmentID == assessment.ID).Count() > 0).Count() > 0).Count() > 0).ToList();
                }


                branchViewModelList = MapperHelper.MapToViewModelList(userProfileBranchList.AsQueryable());
                branchViewModelList = branchViewModelList.OrderBy(x => x.Name).ToList();
            }
            Response.Write(JsonConvert.SerializeObject(branchViewModelList));
        }



        #endregion

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}