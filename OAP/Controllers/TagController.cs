﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OAP.Domain;
using OAP.Domain.ViewModel;
using OAP.Domain.ViewModel.DataTableViewModel;
using Newtonsoft.Json;
using HelperClasses;
using System.Data.Entity;
using System.Data;

namespace OAP.Controllers
{
    [Authorize(Roles = "SuperAdmin, CollegeAdmin, Admin")]
    public class TagController : BaseController
    {
        //
        // GET: /Tag/

        OAPEntities db = new OAPEntities();
        string[] columns = { "Name" };

        public ActionResult Index()
        {
            return View();
        }

        public void List()
        {
            datatableRequest = new DataTableRequest(Request, columns);

            // IQueryable<Tag> tags = db.Tag;
            IQueryable<Tag> tags = (from t in db.Tag
                                     group t by new
                                     {
                                         t.Name
                                     } into tg
                                     select tg.FirstOrDefault());


            int totalRecords = tags.Count();
            int filteredCount = totalRecords;
            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                tags = (from t in db.Tag where t.Name.Contains(datatableRequest.SearchString) select t);
                filteredCount = tags.Count();
            }
            tags = tags.OrderBy(x => x.Name);
            if (datatableRequest.ShouldOrder)
            {
                tags = tags.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            }
            tags = tags.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            List<TagViewModel> tagViewModelList = MapperHelper.MapToViewModelList(tags);

            TagDataTableViewModel dataTableViewModel = new TagDataTableViewModel(tagViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        #region Create

        public ActionResult Create()
        {
            TagViewModel tagViewModel = new TagViewModel();
            ViewBag.ParentTag = new SelectList(db.Tag.OrderBy(qc => qc.Name), "ID", "Name");
            return View(tagViewModel);
        }

        [HttpPost]
        public ActionResult Create(TagViewModel tagViewModel)
        {
            //if (ModelState.IsValid)
            //{                                               //Valid model state
                Tag tags = MapperHelper.MapToDomainObject(db, tagViewModel);
                db.Tag.Add(tags);
                db.SaveChanges();
                return RedirectToAction("Index");
            //}
            //return View(tagViewModel);

        }

        #endregion
        public void SaveTag(string TagName)
        {
            try
            {
                Tag tags = new Tag();
                tags.Name = TagName;
                db.Tag.Add(tags);
                db.SaveChanges();
                Response.Write("True");
            }
            catch
            {
                Response.Write("False");
            }
        }
        #region Edit

        public ActionResult Edit(long id)
        {
            Tag tags = db.Tag.Find(id);
            TagViewModel tagViewModel;
            if (tags != null)
            {
                tagViewModel = MapperHelper.MapToViewModel(tags);
            }
            else
            {
                tagViewModel = new TagViewModel();
            }
ViewBag.ParentTag = new SelectList(db.Tag.Where(t => t.ID != id).OrderBy(qc => qc.Name), "ID", "Name",tagViewModel.ParentTagID);
            return View(tagViewModel);
        }

        [HttpPost]
        public ActionResult Edit(TagViewModel tagViewModel)
        {
            //if (ModelState.IsValid)
            //{                                   //Valid model state
                Tag tag = MapperHelper.MapToDomainObject(db, tagViewModel);
                db.Entry(tag).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            //}
            //return View(tagViewModel);

        }

        #endregion

        #region Delete

        //public ActionResult Delete(long id)
        //{
        //    Tag tag = db.Tag.Find(id);
        //    TagViewModel tagViewModel;
        //    if (tag != null)
        //    {
        //        tagViewModel = MapperHelper.MapToViewModel(tag);
        //    }
        //    else
        //    {
        //        tagViewModel = new TagViewModel();
        //    }
        //    return View(tagViewModel);
        //}

        //[HttpPost]
        //public ActionResult Delete(TagViewModel tagViewModel)
        //{
        //    Tag tag = db.Tag.Find(tagViewModel.ID);
        //    if (tag != null)
        //    {
        //        db.Tag.Remove(tag);
        //        db.SaveChanges();
        //    }
        //    return RedirectToAction("Index");
        //}

        public void Delete(string idString)
        {
            string result = "";
            try
            {
                if (!string.IsNullOrEmpty(idString))
                {
                    List<int> idList = idString.Split(',').Select(int.Parse).ToList();
                    List<Tag> tagList = new List<Tag>();
                    foreach (int id in idList)
                    {
                        Tag tag = db.Tag.Find(id);
                        List<QuestionTags> questionTags = tag.QuestionTags.ToList();
                        db.QuestionTags.RemoveRange(questionTags);
                        tagList.Add(tag);
                    }
                    db.SaveChanges();
                    db.Tag.RemoveRange(tagList);
                    db.SaveChanges();
                    result = "Tag deleted";
                }
            }
            catch
            {
                result = "Cannot delete these tags. First delete these";
            }
        }

        #endregion

    }
}
