﻿using OAP.Domain;
using OAP.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OAP.Controllers
{
    public class ColorCodeController : BaseController
    {
        //
        // GET: /ColorCode/

        public ActionResult ColorCode()
        {
            ColorsViewModel colorsViewModel = new ColorsViewModel();           
            IQueryable<ColorCode> colorCode = db.ColorCode;
            List<ColorCodeViewModel> colorCodeList = (from c in colorCode
                                                             select new ColorCodeViewModel()
                                                           {
                                                               ID = c.ID,
                                                               Name = c.Name,
                                                               HexCode=c.HexCode,
                                                           }).ToList();
            colorsViewModel.colorCodeViewModelList = colorCodeList;
            return View(colorsViewModel);
        }

        public void SaveColorCode(string ActionColor)
        {
            List<string> colorCodeString = new List<string>();
            List<int> countStringsList = new List<int>();
            List<ColorCode> colorcodes = db.ColorCode.ToList();
            if (colorcodes != null)
            {
                if (!string.IsNullOrWhiteSpace(ActionColor))
                {
                    colorCodeString = ActionColor.Split(',').ToList();
                }
                for (int i = 0; i < colorCodeString.Count(); i++)
                {
                    ColorCode colorcode = colorcodes[i];
                    colorcode.HexCode = colorCodeString[i];
                    db.Entry(colorcode).State = EntityState.Modified;
                    db.SaveChanges();
                }

                Response.Write("True");
            }
        }

    }
}
