﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OAP.Domain;
using OAP.Domain.ViewModel;
using HelperClasses;
using OAP.Domain.ViewModel.DataTableViewModel;
using Newtonsoft.Json;
using System.Web.Routing;
using System.Data.Entity;
using System.Web.Script.Serialization;
using System.Data;

namespace OAP.Controllers
{
    [Authorize(Roles = "SuperAdmin, CollegeAdmin, Admin")]
    public class PassageController : BaseController
    {
        //
        // GET: /Passage/
        OAPEntities db = new OAPEntities();
        HtmlFormatter htmlConverter = new HtmlFormatter();

        string[] columns = { "Description" };

        #region Index

        public ActionResult Index()
        {
            return View();
        }

        public void List()
        {
            datatableRequest = new DataTableRequest(Request, columns);

            //IQueryable<Passage> passages = db.Passage;
            IQueryable<Passage> passages = (from p in db.Passage
                                           group p by new
                                           {
                                               p.Description
                                           } into ps
                                           select ps.FirstOrDefault());


            int totalRecords = passages.Count();
            int filteredCount = totalRecords;
            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                passages = (from c in db.Passage where c.Description.Contains(datatableRequest.SearchString) select c);
                filteredCount = passages.Count();
            }
            passages = passages.OrderBy(x => x.Description);
            if (datatableRequest.ShouldOrder)
            {
                passages = passages.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            }
            passages = passages.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            List<PassageViewModel> passageViewModelList = MapperHelper.MapToViewModelList(passages);

            HtmlFormatter htmlFormatter = new HtmlFormatter();
            passageViewModelList = passageViewModelList.Select(p => { p.Description = htmlFormatter.GetContentFromHtmlWithOutReplaceSpace(p.Description); return p; }).ToList();
            
            PassageDataTableViewModel dataTableViewModel = new PassageDataTableViewModel(passageViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        #endregion

        #region Create

        public ActionResult Create()
        {
            PassageViewModel passageViewModel = new PassageViewModel();
            passageViewModel.CanShuffle = true;
            return View(passageViewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Create(PassageViewModel passageViewModel, string save)
        {
            if (ModelState.IsValid)
            {
                Passage passage = MapperHelper.MapToDomainObject(db, passageViewModel);
                passage.CreatedDate = DateTime.Now;
                passage.CreatedBy = GetCurrentUserID();
                passage.ModifiedDate = DateTime.Now;
                passage.ModifiedBy = GetCurrentUserID();
                passage.LastUpdated = DateTime.Now;
                db.Passage.Add(passage);
                db.SaveChanges();
                if (save.Equals("SaveAddQuestion"))
                {                                                               //Add Existing Question\
                    return RedirectToAction("AddExistingQuestions", "Passage", new RouteValueDictionary(new { id = passage.ID }));

                }
                else if (save.Equals("SaveAddNewQuestion"))
                {                                                               //Add New Question
                    return RedirectToAction("Create", "Question", new RouteValueDictionary(new { PassageID = passage.ID }));
                }

                return RedirectToAction("Index");
            }
            else
            {
                return View(passageViewModel);
            }
        }


        #endregion

        #region Edit

        public ActionResult Edit(int id)
        {
            Passage passage = db.Passage.Find(id);
            PassageViewModel passageViewModel = MapperHelper.MapToViewModel(passage);

            IQueryable<Question> questions = (from q in db.Question where (q.PassageID == id) select q);
            List<QuestionViewModel> QuestionViewModelList = MapperHelper.MapToViewModelList(questions);
            QuestionViewModelList = QuestionViewModelList;
            passageViewModel.QuestionViewModelList = QuestionViewModelList;
            return View(passageViewModel);
        }
          public void PassageEdit(int passageid)
        {
            Passage passage = db.Passage.Find(passageid);
            PassageViewModel passageViewModel = MapperHelper.MapToViewModel(passage);

            IQueryable<Question> questions = (from q in db.Question where (q.PassageID == passageid) select q);
            List<QuestionViewModel> QuestionViewModelList = MapperHelper.MapToViewModelList(questions);
            QuestionViewModelList = QuestionViewModelList;
            passageViewModel.QuestionViewModelList = QuestionViewModelList;
            string result = RenderPartialViewToString("PassageEdit", passageViewModel);
            Response.Write(result);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(PassageViewModel passageViewModel, string save)
        {
            Passage passage = db.Passage.Find(passageViewModel.ID);
            if (ModelState.IsValid)
            {
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                List<QuestionOrder> questionOrderList = new List<QuestionOrder>();
                if (!String.IsNullOrEmpty(passageViewModel.AnswerOrder))
                    questionOrderList = JsonConvert.DeserializeObject<List<QuestionOrder>>(passageViewModel.AnswerOrder); //jsonSerializer.Deserialize<AnswerOrder[]>();
                if (passage != null)
                {
                    passage.ModifiedDate = DateTime.Now;
                    passage.ModifiedBy = GetCurrentUserID();
                    passage = MapperHelper.MapToDomainObject(db, passageViewModel, passage);
                    passage.LastUpdated = DateTime.Now;
                  //  passage.Description = htmlConverter.FormatFileContent(passage.Description);
                    passage.Description = passage.Description;


                    db.Entry(passage).State = EntityState.Modified;
                }
                OrderQuestionForPassage(questionOrderList, passageViewModel.CanShuffle, passage);

                db.SaveChanges();
                if (save.Equals("SaveAddNewQuestion"))
                {                                                           //Add new question
                    return RedirectToAction("Create", "Question", new RouteValueDictionary(new { PassageID = passage.ID }));
                }
                else if (save.Equals("SaveAddQuestion"))
                {                                                           //Add existing question
                    return RedirectToAction("AddExistingQuestions", "Passage", new RouteValueDictionary(new { id = passage.ID }));
                }

                return RedirectToAction("Index", "Question");
            }
            IQueryable<Question> questions = (from q in db.Question where (q.PassageID == passageViewModel.ID) select q);
            List<QuestionViewModel> QuestionViewModelList = MapperHelper.MapToViewModelList(questions);
            QuestionViewModelList = QuestionViewModelList;
            passageViewModel.QuestionViewModelList = QuestionViewModelList;
            return View(passageViewModel);
        }

        //public void PassageEditPost(long PassageID, string AnswerOrder, bool CanShuffle, string Description)
        //{
        //    Passage passage = db.Passage.Find(PassageID);
        //    if (ModelState.IsValid)
        //    {
        //        PassageViewModel passageViewModel = new PassageViewModel();
        //        passageViewModel.AnswerOrder = AnswerOrder;
        //        passageViewModel.CanShuffle = CanShuffle;
        //        passageViewModel.Description = AnswerOrder;

        //        JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
        //        List<QuestionOrder> questionOrderList = null;
        //        if (!String.IsNullOrEmpty(AnswerOrder))
        //            questionOrderList = JsonConvert.DeserializeObject<List<QuestionOrder>>(AnswerOrder); //jsonSerializer.Deserialize<AnswerOrder[]>();
        //        passage.ModifiedDate = DateTime.Now;
        //        passage.ModifiedBy = GetCurrentUserID();
        //        passage = MapperHelper.MapToDomainObject(db, passageViewModel, passage);
        //        passage.LastUpdated = DateTime.Now;
        //        db.Entry(passage).State = EntityState.Modified;

        //        OrderQuestionForPassage(questionOrderList, passageViewModel.CanShuffle, passage);

        //        db.SaveChanges();
        //        if (save.Equals("SaveAddNewQuestion"))
        //        {                                                           //Add new question
        //            //return RedirectToAction("Create", "Question", new RouteValueDictionary(new { PassageID = passage.ID }));
        //        }
        //        else if (save.Equals("SaveAddQuestion"))
        //        {                                                           //Add existing question
        //           // return RedirectToAction("AddExistingQuestions", "Passage", new RouteValueDictionary(new { id = passage.ID }));
        //        }

        //        //return RedirectToAction("Index");
        //    }
        //    IQueryable<Question> questions = (from q in db.Question where (q.PassageID == passageViewModel.ID) select q);
        //    List<QuestionViewModel> QuestionViewModelList = MapperHelper.MapToViewModelList(questions);
        //    QuestionViewModelList = sh.SortQuestionViewModel(QuestionViewModelList);
        //    passageViewModel.QuestionViewModelList = QuestionViewModelList;
        //    //return View(passageViewModel);
        //}

        public void OrderQuestionForPassage(List<QuestionOrder> questionOrderList, bool canShuffle, Passage passage)
        {
            //Passage passage = db.Passage.Find(passageID);
            if (passage != null)
            {
            List<PassageQuestionOrder> passageQuestionOrderList = passage.PassageQuestionOrder.Where(pqo => pqo.Passage.ID == passage.ID).ToList();
            db.PassageQuestionOrder.RemoveRange(passageQuestionOrderList);
            db.SaveChanges();
            if (!canShuffle)
            {
                
                    int i = 1;
                    foreach (QuestionOrder questionID in questionOrderList)
                    {
                        Question question = db.Question.Find(questionID.id);
                        if (question != null)
                        {
                            PassageQuestionOrder passageQuestionOrder = new PassageQuestionOrder();
                            passageQuestionOrder.Passage = passage;
                            passageQuestionOrder.Question = question;
                            passageQuestionOrder.Order = i;
                            db.PassageQuestionOrder.Add(passageQuestionOrder);
                            i++;
                        }
                    }
                }
            }
        }

        #endregion

        #region Delete

        public ActionResult Delete(int id)
        {
            Passage passage = db.Passage.Find(id);
            ViewBag.QuestionsCount = passage.PassageQuestionOrder.Count();
            PassageViewModel passageViewModel = MapperHelper.MapToViewModel(passage);
            return View(passageViewModel);
        }

        [HttpPost]
        public ActionResult Delete(PassageViewModel passageViewModel)
        {
            Passage passage = db.Passage.Find(passageViewModel.ID);
            bool isPassageQuestionsDeleted = DeletePassageQuestions(passage);
            if (isPassageQuestionsDeleted)
            {                                                   //Questions under the passages deleted successfully
                List<PassageQuestionOrder> passageQuestionOrderList = passage.PassageQuestionOrder.ToList();
                db.PassageQuestionOrder.RemoveRange(passageQuestionOrderList);
                db.SaveChanges();

                db.Passage.Remove(passage);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {                                                   //Questions are not deleted under the question.
                return View(passageViewModel);
            }
        }

        public void DeletePassages(string passageIDString)
        {
            bool result = true;
            try
            {
                if (!string.IsNullOrEmpty(passageIDString))
                {                                                       //Passage selected
                    List<int> passageIDList = passageIDString.Split(',').Select(int.Parse).ToList();
                    List<Passage> passageList = new List<Passage>();
                    foreach (int passageID in passageIDList)
                    {
                        Passage passage = db.Passage.Find(passageID);
                        if (passage != null)
                        {                                                   //Passage is not null. Valid passageid
                            passageList.Add(passage);
                            bool isQuestionsDeleted = DeletePassageQuestions(passage);
                            if (isQuestionsDeleted)
                            {                                                       //Questions under the passages are deleted successfully
                                List<PassageQuestionOrder> passageQuestionOrderList = passage.PassageQuestionOrder.ToList();
                                db.PassageQuestionOrder.RemoveRange(passageQuestionOrderList);
                                db.SaveChanges();
                                db.Passage.Remove(passage);
                                db.SaveChanges();
                            }
                            else
                            {                                                       //Questions under the passages are not deleted. some error occured
                                result = false;
                            }
                        }
                        else
                        {                                                    //Passage is null. Invalid passageid

                        }
                    }
                }
                else
                {                                                       //No passage selected

                }
            }
            catch
            {
                result = false;
            }
            Response.Write(result);
        }

        public bool DeletePassageQuestions(Passage passage)
        {
            bool result = true;
            try
            {
                if (passage != null)
                {
                    List<Question> questionList = passage.Question.ToList();
                    foreach (Question question in questionList)
                    {
                        List<QuestionPaperSectionQuestion> questionPaperSectionQuestionList = question.QuestionPaperSectionQuestion.ToList();
                        List<QuestionPaperQuestion> questionPaperQuestionList = question.QuestionPaperQuestion.ToList();
                        db.QuestionPaperSectionQuestion.RemoveRange(questionPaperSectionQuestionList);
                        db.QuestionPaperQuestion.RemoveRange(questionPaperQuestionList);

                        List<AssessmentSnapshot> assessmentSnapshotList = new List<AssessmentSnapshot>();
                        assessmentSnapshotList.AddRange(question.AssessmentSnapshot.ToList());
                        db.SaveChanges();

                        if (question != null)
                        {                                                   //Question is not null
                            if (question.Answer.Count() > 0)
                            {                                       //Question having answer
                                List<Answer> answerList = question.Answer.ToList();
                                if (answerList != null)
                                {
                                    assessmentSnapshotList = new List<AssessmentSnapshot>();
                                    assessmentSnapshotList.AddRange(question.Answer.SelectMany(x => x.AssessmentSnapshot).ToList());

                                    List<UserAssessmentSnapshot> userAssessmentSnapShotList = assessmentSnapshotList.SelectMany(x => x.UserAssessmentSnapshot).ToList();

                                    db.UserAssessmentSnapshot.RemoveRange(userAssessmentSnapShotList);
                                    db.SaveChanges();

                                    db.AssessmentSnapshot.RemoveRange(assessmentSnapshotList);
                                    db.SaveChanges();

                                }
                                db.Answer.RemoveRange(question.Answer);
                                db.SaveChanges();
                            }
                        }
                    }
                    db.SaveChanges();
                    db.Question.RemoveRange(questionList);
                    db.SaveChanges();
                }
            }
            catch { result = false; }
            return result;
        }

        #endregion

        #region AddExistingQuestion

        public ActionResult AddExistingQuestions(int id = 0)
        {
            PassageViewModel passageViewModel = new PassageViewModel();
            passageViewModel.ID = id;
            return View(passageViewModel);
        }

        public void AddExistingQuestionsView()
        {
            datatableRequest = new DataTableRequest(Request, columns);

            int id = Convert.ToInt32(Request.QueryString["id"]);
            IQueryable<Question> questions = db.Question.Where(q => q.PassageID != id && (q.PassageID == null || q.PassageID == 0));
            int totalRecords = questions.Count();
            int filteredCount = totalRecords;
            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                questions = (from q in db.Question where q.Description.Contains(datatableRequest.SearchString) select q);
                filteredCount = questions.Count();
            }
            questions = questions.OrderBy(x => x.Description);
            if (datatableRequest.ShouldOrder)
            {
                questions = questions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            }
            questions = questions.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            List<QuestionViewModel> questionViewModelList = MapperHelper.MapToViewModelList(questions);
            var technicalQuestionViewModelList = new List<Domain.ViewModel.TechnicalQuestionViewModel>();

            QuestionDataTableViewModel dataTableViewModel = new QuestionDataTableViewModel(questionViewModelList,technicalQuestionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        #endregion

        public void AssignQuestionsToPassage(string passageIDString, string selectedQuestionIDString)
        {
            int passageID = Convert.ToInt32(passageIDString);
            if (!String.IsNullOrEmpty(selectedQuestionIDString))
            {
                List<long> selectedQuestionIDList = selectedQuestionIDString.Split(',').ToList().Select(long.Parse).ToList();
                foreach (long questionID in selectedQuestionIDList)
                {
                    Question question = db.Question.Find(questionID);
                    if (question != null)
                    {
                        question.PassageID = passageID;
                        //question.Description = htmlConverter.FormatFileContent(question.Description);
                        question.Description = question.Description;

                        db.Entry(question).State = EntityState.Modified;
                    }
                }
                db.SaveChanges();
            }
        }

    }

    public class QuestionOrder
    {
        public int id { get; set; }
    }
}
