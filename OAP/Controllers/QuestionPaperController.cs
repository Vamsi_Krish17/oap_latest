﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OAP.Domain;
//using OAP.Models.ViewModels;
using WebMatrix.WebData;
using HelperClasses;
using Newtonsoft.Json;
//using OAP.Domain.ViewModel.DataTableViewModel;
using System.Globalization;
using System.IO;
//using Microsoft.Office.Interop.Word;
using System.Text;
using System.Xml.Linq;
using System.Web.Routing;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml.Drawing;
using System.Text.RegularExpressions;
using System.Net;
using System.Net.Http;
using System.Collections.Specialized;
using OAP.Domain.ViewModel;
using OAP.Models.ViewModels;
using OAP.Domain.ViewModel.DataTableViewModel;
using AutoMapper;

//using DocumentFormat.OpenXml.Wordprocessing;

namespace OAP.Controllers
{

    public class QuestionPaperController : BaseController
    {
        HtmlFormatter htmlConverter = new HtmlFormatter();
        private static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        //protected Microsoft.Office.Interop.Word.ApplicationClass objWord = new ApplicationClass();

        //private OAPEntities db = new OAPEntities();
        string[] columns = { "Name", "Code", "CreatedDateString", "Download" };
        string[] assessmentSectionColumns = { "Name", "Mark", "NegativeMark", "TotalQuestionCount", "TimeLimit" };

        public ActionResult Index()
        {
            //return View(db.QuestionPaper.ToList());
            ViewBag.Series = new SelectList(db.Series, "ID", "SeriesType");
            return View();
        }

        //public void List()
        //{
        //    datatableRequest = new DataTableRequest(Request, columns);

        //    IQueryable<QuestionPaper> questionpapers= db.QuestionPaper;
        //    int totalRecords = questionpapers.Count();
        //    int filteredCount = totalRecords;
        //    if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
        //    {
        //        questionpapers = (from a in db.QuestionPaper where a.Name.Contains(datatableRequest.SearchString) select a);
        //        filteredCount = questionpapers.Count();
        //    }
        //    questionpapers = questionpapers.OrderBy(x => x.ID);
        //    if (datatableRequest.ShouldOrder)
        //    {
        //        questionpapers = questionpapers.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
        //    }
        //    questionpapers = questionpapers.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
        //    List<OAP.Domain.ViewModel.QuestionPaperViewModel> assessmentViewModelList = MapperHelper.MapToViewModelList(questionpapers);

        //    QuestionPaperDataTableViewModel dataTableViewModel = new QuestionPaperDataTableViewModel(assessmentViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

        //    string result = JsonConvert.SerializeObject(dataTableViewModel);
        //    Response.Write(result);
        //}

        #region QuestionPaper List

        public ActionResult QuestionPaperList()
        {
            //CollegeViewModel collegeViewModel = new CollegeViewModel();
            //if (ID != 0)
            //{
            //    College college = db.College.Find(ID);
            //    collegeViewModel = MapperHelper.MapToViewModel(college);
            //    List<Program> programs = db.InstituteProgram.Where(i => i.InstituteID == ID).Select(i => i.Program).ToList();
            //    List<ProgramViewModel> ProgramList = MapperHelper.MapToViewModelList(programs);
            //    collegeViewModel.ProgramList = ProgramList;
            //}
            //else
            //{
            //    collegeViewModel.ProgramList = new List<ProgramViewModel>();
            //}
            //ViewBag.College = db.College.ToList();
            //ViewBag.Branches = new List<Section>();
            //ViewBag.Sections = db.Section.ToList();
            //ViewBag.Batches = new List<Section>();

            //ViewBag.College = db.College);
            //ViewBag.Question = db.QuestionPaper);
            ////ViewBag.Sections = db.Section.ToList();
            //ViewBag.Batches = db.Batch);
            ViewBag.QuestionSeries = db.Question;
            ViewBag.AnswerSeries = db.Answer;
            ViewBag.Upload = db.Uploads;

            return View();
        }

        public void List()
        {
            datatableRequest = new DataTableRequest(Request, columns);

            // IQueryable<QuestionPaper> questionpapers = db.QuestionPaper;
            IQueryable<QuestionPaper> questionpapers = (from a in db.QuestionPaper
                                                        group a by new
                                                        {
                                                            a.Name
                                                        } into asm
                                                        select asm.FirstOrDefault());

            if (Request.QueryString["college"] != null && Request.QueryString["college"] != "0")
            {
                long collegeID = Convert.ToInt64(Request.QueryString["college"]);
                questionpapers = questionpapers.Where(a => a.BatchAssessment.Where(b => b.Batch != null & b.Batch.CollegeID == collegeID).Count() > 0);
            }
            if (Request.QueryString["Batch"] != null && Request.QueryString["Batch"] != "0")
            {
                long batchID = Convert.ToInt64(Request.QueryString["Batch"]);
                questionpapers = questionpapers.Where(a => a.BatchAssessment.Where(b => b.BatchID == batchID).Count() > 0);
            }
            if (Request.QueryString["Question"] != null && Request.QueryString["Question"] != "0")
            {
                long assessmentID = Convert.ToInt64(Request.QueryString["Question"]);
                questionpapers = questionpapers.Where(a => a.ID == assessmentID);
            }
            int totalRecords = questionpapers.Count();
            int filteredCount = totalRecords;
            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                questionpapers = (from c in db.QuestionPaper where c.Code.Contains(datatableRequest.SearchString) || c.Name.Contains(datatableRequest.SearchString) select c);
                filteredCount = questionpapers.Count();
            }
            questionpapers = questionpapers.OrderBy(x => x.Name);
            if (datatableRequest.ShouldOrder)
            {
                if (datatableRequest.OrderByColumn == "CreatedDateString")
                {
                    if (datatableRequest.IsOrderAsccending == true)
                    {
                        questionpapers = questionpapers.OrderBy(p => p.CreatedDate);
                    }
                    else
                    {
                        questionpapers = questionpapers.OrderByDescending(p => p.CreatedDate);
                    }
                }
                else
                {
                    questionpapers = questionpapers.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
                }
            }
            //questionpapers = questionpapers.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            questionpapers = questionpapers.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);

            List<OAP.Domain.ViewModel.QuestionPaperViewModel> assessmentViewModelList = MapperHelper.MapToViewModelList(questionpapers);

            QuestionPaperDataTableViewModel dataTableViewModel = new QuestionPaperDataTableViewModel(assessmentViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        public ActionResult AssessmentList()
        {

            //QuestionPaperViewModel questionPaperViewModel = new QuestionPaperViewModel();
            //if (ID != 0)
            //{

            //    Series series = db.Series.Find(ID);
            //    questionPaperViewModel = MapperHelper.MapToViewModel(series);
            //    List<Program> programs = db.InstituteProgram.Where(i => i.InstituteID == ID).Select(i => i.Program).ToList();
            //    List<ProgramViewModel> ProgramList = MapperHelper.MapToViewModelList(programs);
            //    collegeViewModel.ProgramList = ProgramList;
            //}
            //else
            //{
            //    collegeViewModel.ProgramList = new List<ProgramViewModel>();
            //}
            //ViewBag.College = db.College.ToList();
            //ViewBag.Branches = new List<Section>();
            //ViewBag.Sections = db.Section.ToList();
            //ViewBag.Batches = new List<Section>();

            //ViewBag.College = db.College.ToList();
            //ViewBag.Question = db.QuestionPaper.ToList();
            ////ViewBag.Sections = db.Section.ToList();
            ViewBag.Series = new SelectList(db.Series, "ID", "SeriesType");
            ViewBag.Upload = new SelectList(db.DocumentTemplate, "ID", "TemplateName");


            return View();
        }

        public void GetfilterBatch(long selectedCollage)
        {
            College college = new College();
            IQueryable<Batch> batchStudents = db.Batch;
            BranchViewModel BranchInstituteViewModel = new BranchViewModel();
            if (selectedCollage != 0)
            {
                college = db.College.Find(selectedCollage);
                if (college != null)
                {
                    List<Batch> batch = batchStudents.Where(x => x.CollegeID == college.ID).ToList();
                    List<BatchViewModel> batchViewModelList = (from b in batch
                                                               select new BatchViewModel
                                                               {
                                                                   ID = b.ID,
                                                                   Name = b.Name,
                                                                   StudentCount = b.BatchStudent.Select(x => x.Student).Count(),
                                                               }).OrderBy(x => x.Name).ToList();

                    BranchInstituteViewModel.batchList = batchViewModelList;
                }
            }

            string result = JsonConvert.SerializeObject(BranchInstituteViewModel);
            Response.Write(result);
        }


        public void GetfilterQuestionPaper(long selectedBatch)
        {
            BatchAssessment assessment = new BatchAssessment();
            IQueryable<BatchAssessment> batchStudents = db.BatchAssessment;
            BatchQuestionPaperViewModel batchQuestionPaperViewModel = new BatchQuestionPaperViewModel();
            if (selectedBatch != 0)
            {
                if (assessment != null)
                {
                    List<BatchAssessment> assessmentlist = db.BatchAssessment.Where(x => x.BatchID == selectedBatch).ToList();
                    List<BatchQuestionPaperViewModel> batchViewModelList = (from b in assessmentlist
                                                                            select new BatchQuestionPaperViewModel
                                                                            {
                                                                                ID = b.AssessmentID,
                                                                                Name = b.QuestionPaper.Name,
                                                                                //StudentCount = b.BatchStudent.Select(x => x.Student).Count(),
                                                                            }).ToList();

                    batchQuestionPaperViewModel.batchList = batchViewModelList;
                }
            }
            string result = JsonConvert.SerializeObject(batchQuestionPaperViewModel);
            Response.Write(result);
        }

        #endregion

        #region Create QuestionPaper
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /QuestionPaper/Create

        [HttpPost]
        [ValidateInput(false)]
        //[ValidateAntiForgeryToken]
        public ActionResult Create(OAP.Domain.ViewModel.QuestionPaperViewModel questionPaperViewModel)
        {
            QuestionPaper questionPaper = MapperHelper.MapToDomainObject(db, questionPaperViewModel);
            //DateTime nullDate = Convert.ToDateTime("01-01-0001");
            //if (questionPaperViewModel.StartDate==nullDate)
            //    ModelState.AddModelError("StartDateString","");
            //if (questionPaperViewModel.EndDate == nullDate)
            //    ModelState.AddModelError("EndDateString", "");
            if (ModelState.IsValid)
            {
                questionPaper.CreatedDate = DateTime.Now;
                questionPaper.CreatedBy = GetCurrentUserID();
                questionPaper.ModifiedBy = GetCurrentUserID();
                db.QuestionPaper.Add(questionPaper);
                db.SaveChanges();
                questionPaperViewModel.ID = questionPaper.ID;
                if (questionPaperViewModel.TimeLimit == null || questionPaperViewModel.TimeLimit == 0)
                {                                                                                   //Time limit not given for assessment
                    questionPaperViewModel.IsShowSectionTimeLimit = true;
                }
                else
                {                                                                                   //Time limit given for whole assessment
                    questionPaperViewModel.IsShowSectionTimeLimit = false;
                }
                CreateSectionsforQuestionPaper(questionPaperViewModel, questionPaper);
                return RedirectToAction("EditQuestionPaperSections", "QuestionPaperSection", questionPaperViewModel);
            }

            return View(questionPaperViewModel);
        }

        public void CreateSectionsforQuestionPaper(OAP.Domain.ViewModel.QuestionPaperViewModel questionPaperViewModel, QuestionPaper questionPaper)
        {
            int i = 0;
            List<QuestionPaperSection> questionPaperSection = questionPaper.QuestionPaperSection.ToList();
            if (questionPaperSection.Count() > 0)
            {
                int extraSections = questionPaperViewModel.SectionCount - questionPaperSection.Count();
                if (extraSections > 0)
                {
                    i = questionPaperSection.Count();
                }
                else
                {
                    i = -1;
                }
            }
            if (i != -1)
            {
                for (; i < questionPaperViewModel.SectionCount; i++)
                {
                    QuestionPaperSection questionPaperSec = new QuestionPaperSection();
                    questionPaperSec.ModifiedDate = DateTime.Now;
                    questionPaperSec.CreatedDate = DateTime.Now;
                    questionPaperSec.CreatedBy = GetCurrentUserID();
                    questionPaperSec.ModifiedBy = GetCurrentUserID();
                    questionPaperSec.QuestionPaper = questionPaper;
                    questionPaperSec.Name = "Section " + (i + 1).ToString();
                    db.QuestionPaperSection.Add(questionPaperSec);
                    db.SaveChanges();
                }
            }


        }


        //public void CreateSectionforQuestionPaper(long assessmentId, string sectionName, string sectionDescription, string sectionMark, string sectionCutoffMark, string sectionNegativeMark, string sectionTimeLimit)
        //{
        //    bool result = true;
        //    QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentId);
        //    try
        //    {
        //        QuestionPaperSection questionPaperSection = new QuestionPaperSection();
        //        questionPaperSection.ModifiedDate = DateTime.Now;
        //        questionPaperSection.CreatedDate = DateTime.Now;
        //        questionPaperSection.CreatedBy = GetCurrentUserID();
        //        questionPaperSection.ModifiedBy = GetCurrentUserID();
        //        questionPaperSection.QuestionPaper = questionPaper;
        //        questionPaperSection.Name = sectionName;
        //        questionPaperSection.Description = sectionDescription;
        //        questionPaperSection.Mark = Convert.ToDecimal(sectionMark);
        //        questionPaperSection.NegativeMark = Convert.ToDecimal(sectionNegativeMark);
        //        questionPaperSection.CutOffMark = Convert.ToDecimal(sectionNegativeMark);
        //        if (!string.IsNullOrWhiteSpace(sectionTimeLimit))
        //        {
        //            questionPaperSection.TimeLimit = Convert.ToInt32(sectionTimeLimit);
        //        }
        //        db.QuestionPaperSection.Add(questionPaperSection);
        //        db.SaveChanges();
        //    }
        //    catch (Exception ex)
        //    {
        //        result = false;
        //    }
        //    Response.Write(result);
        //}


        public void CreateSectionforQuestionPaper(QuestionPaperSectionViewModel questionpaperSectionViewmodel)
        {
            bool result = true;
            QuestionPaper questionPaper = db.QuestionPaper.Find(questionpaperSectionViewmodel.QuestionPaperID);
            try
            {
                QuestionPaperSection questionPaperSection = new QuestionPaperSection();
                questionPaperSection.ModifiedDate = DateTime.Now;
                questionPaperSection.CreatedDate = DateTime.Now;
                questionPaperSection.CreatedBy = GetCurrentUserID();
                questionPaperSection.ModifiedBy = GetCurrentUserID();
                questionPaperSection.QuestionPaper = questionPaper;
                questionPaperSection.Name = questionpaperSectionViewmodel.SectionName;
                questionPaperSection.Description = questionpaperSectionViewmodel.Description;
                questionPaperSection.Mark = questionpaperSectionViewmodel.Mark;//Convert.ToDecimal(sectionMark);
                questionPaperSection.NegativeMark = questionpaperSectionViewmodel.NegativeMark;//Convert.ToDecimal(sectionNegativeMark);
                questionPaperSection.CutOffMark = questionpaperSectionViewmodel.CutOffMark;//Convert.ToDecimal(sectionNegativeMark);

                int minutes = ((questionpaperSectionViewmodel.TimeLimitTimeSpan.Hours == 0 ? questionpaperSectionViewmodel.TimeLimitTimeSpan.Days : questionpaperSectionViewmodel.TimeLimitTimeSpan.Hours * 60)) + (questionpaperSectionViewmodel.TimeLimitTimeSpan.Minutes);
                questionPaperSection.TimeLimit = minutes;

                //if (!string.IsNullOrWhiteSpace(questionpaperSectionViewmodel.TimeLimit))
                //{
                //    questionPaperSection.TimeLimit = Convert.ToInt32(sectionTimeLimit);
                //}
                db.QuestionPaperSection.Add(questionPaperSection);
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                result = false;
            }
            Response.Write(result);
        }


        #endregion

        #region Edit

        public ActionResult Edit(int id = 0)
        {
            QuestionPaper questionPaper = db.QuestionPaper.Find(id);
            if (questionPaper == null)
            {
                return HttpNotFound();
            }
            OAP.Domain.ViewModel.QuestionPaperViewModel questionPaperViewModel = MapperHelper.MapToViewModel(questionPaper);
            questionPaperViewModel.SectionCount = questionPaper.QuestionPaperSection.Count();
            if (questionPaperViewModel.TimeLimit == null || questionPaperViewModel.TimeLimit == 0)
            {                                                                                   //Time limit not given for assessment
                questionPaperViewModel.IsShowSectionTimeLimit = true;
            }
            else
            {                                                                                   //Time limit given for whole assessment
                questionPaperViewModel.IsShowSectionTimeLimit = false;
            }
            return View(questionPaperViewModel);
        }

        //[HttpPost]
        //[ValidateInput(false)]
        //public ActionResult Edit(OAP.Domain.ViewModel.QuestionPaperViewModel questionPaperViewModel)
        //{
        //    QuestionPaper questionPaper = db.QuestionPaper.Find(questionPaperViewModel.ID);

        //    CultureInfo cultureInfo = new CultureInfo("en-US");
        //    int minutes = ((questionPaperViewModel.TimeLimitTimeSpan.Hours * 60) + questionPaperViewModel.TimeLimitTimeSpan.Minutes);
        //    questionPaperViewModel.TimeLimit = minutes;

        //    //string selectedDate = questionPaperViewModel.StartDate.ToString("dd/MM/yyy");
        //    //DateTime cultureBasedDate = DateTime.Parse(selectedDate, cultureInfo);      //Converting all the given  date to US format
        //    //assessment.StatDateTime = cultureBasedDate;

        //    //selectedDate = questionPaperViewModel.EndDate.ToString("dd/MM/yyy");
        //    //cultureBasedDate = DateTime.Parse(selectedDate, cultureInfo);
        //    //assessment.EndDateTime = cultureBasedDate;
        //    questionPaper = MapperHelper.MapToDomainObject(db, questionPaperViewModel, questionPaper);
        //    if (ModelState.IsValid)
        //    {
        //        questionPaper.ModifiedBy = GetCurrentUserID();
        //        questionPaper.ModifiedDate = DateTime.Now;
        //        if (questionPaper.StatDateTime == new DateTime())
        //        {
        //            questionPaper.StatDateTime = GetDefaultDateTime();
        //        }
        //        if (questionPaper.EndDateTime == new DateTime())
        //        {
        //            questionPaper.EndDateTime = GetDefaultDateTime();
        //        }
        //        db.Entry(questionPaper).State = EntityState.Modified;
        //        questionPaper.ModifiedBy = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault().UserId;
        //        db.SaveChanges();
        //        questionPaperViewModel.ID = questionPaper.ID;
        //        CreateSectionsforQuestionPaper(questionPaperViewModel, questionPaper);
        //        return RedirectToAction("EditQuestionPaperSections", "QuestionPaperSection", questionPaperViewModel);
        //        //return RedirectToAction("Index");
        //    }
        //    questionPaperViewModel.questionPaperSectionViewModelList = MapperHelper.MapToViewModelList(questionPaper.QuestionPaperSection);
        //    questionPaperViewModel.SectionCount = questionPaperViewModel.questionPaperSectionViewModelList.Count();

        //    return View(questionPaperViewModel);
        //}
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(OAP.Domain.ViewModel.QuestionPaperViewModel questionPaperViewModel)
        {
            QuestionPaper questionPaper = db.QuestionPaper.Find(questionPaperViewModel.ID);

            CultureInfo cultureInfo = new CultureInfo("en-US");
            int minutes = ((questionPaperViewModel.TimeLimitTimeSpan.Hours * 60) + questionPaperViewModel.TimeLimitTimeSpan.Minutes);
            questionPaperViewModel.TimeLimit = minutes;

            //string selectedDate = questionPaperViewModel.StartDate.ToString("dd/MM/yyy");
            //DateTime cultureBasedDate = DateTime.Parse(selectedDate, cultureInfo);      //Converting all the given  date to US format
            //assessment.StatDateTime = cultureBasedDate;

            //selectedDate = questionPaperViewModel.EndDate.ToString("dd/MM/yyy");
            //cultureBasedDate = DateTime.Parse(selectedDate, cultureInfo);
            //assessment.EndDateTime = cultureBasedDate;
            questionPaper = MapperHelper.MapToDomainObject(db, questionPaperViewModel, questionPaper);
            ModelState.Remove("TimeLimitTimeSpan");
            //if (ModelState.ContainsKey("{TimeLimitTimeSpan}"))
            //    ModelState["{TimeLimitTimeSpan}"].Errors.Clear();

            if (ModelState.IsValid)
            {
                questionPaper.ModifiedBy = GetCurrentUserID();
                questionPaper.ModifiedDate = DateTime.Now;
                if (questionPaper.StatDateTime == new DateTime())
                {
                    questionPaper.StatDateTime = GetDefaultDateTime();
                }
                if (questionPaper.EndDateTime == new DateTime())
                {
                    questionPaper.EndDateTime = GetDefaultDateTime();
                }
                db.Entry(questionPaper).State = EntityState.Modified;
                questionPaper.ModifiedBy = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault().UserId;
                db.SaveChanges();
                questionPaperViewModel.ID = questionPaper.ID;
                CreateSectionsforQuestionPaper(questionPaperViewModel, questionPaper);

                if (minutes > 0)
                    questionPaperViewModel.IsShowSectionTimeLimit = false;
                else
                    questionPaperViewModel.IsShowSectionTimeLimit = true;


                return RedirectToAction("EditQuestionPaperSections", "QuestionPaperSection", questionPaperViewModel);
                //return RedirectToAction("Index");
            }
            questionPaperViewModel.questionPaperSectionViewModelList = MapperHelper.MapToViewModelList(questionPaper.QuestionPaperSection);
            questionPaperViewModel.SectionCount = questionPaperViewModel.questionPaperSectionViewModelList.Count();

            return View(questionPaperViewModel);
        }
        public void GetSectionList()
        {
            long assessmentId = (!string.IsNullOrWhiteSpace(Request.QueryString["QuestionPaperID"])) ? Convert.ToInt64(Request.QueryString["QuestionPaperID"]) : 0;
            QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentId);
            if (questionPaper != null)
            {

                datatableRequest = new DataTableRequest(Request, assessmentSectionColumns);

                IQueryable<QuestionPaperSection> questionPaperSection = questionPaper.QuestionPaperSection.AsQueryable();
                int totalRecords = questionPaperSection.Count();
                int filteredCount = totalRecords;
                if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
                {
                    questionPaperSection = (from a in questionPaperSection where a.Name.ToLower().Contains(datatableRequest.SearchString) select a);
                    filteredCount = questionPaperSection.Count();
                }
                questionPaperSection = questionPaperSection.OrderBy(x => x.Name);
                if (datatableRequest.ShouldOrder)
                {
                    questionPaperSection = questionPaperSection.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
                }
                questionPaperSection = questionPaperSection.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
                List<OAP.Domain.ViewModel.QuestionPaperSectionViewModel> assessmentSectionViewModelList = MapperHelper.MapToViewModelList(questionPaperSection);

                QuestionPaperSectionDataTableViewModel dataTableViewModel = new QuestionPaperSectionDataTableViewModel(assessmentSectionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

                string result = JsonConvert.SerializeObject(dataTableViewModel);
                Response.Write(result);
            }
        }

        #endregion

        #region Delete

        public ActionResult Delete(int id = 0)
        {
            QuestionPaper questionPaper = db.QuestionPaper.Find(id);
            if (questionPaper == null)
            {
                return HttpNotFound();
            }
            return View(questionPaper);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            QuestionPaper questionPaper = db.QuestionPaper.Find(id);
            db.QuestionPaper.Remove(questionPaper);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public void DeleteQuestionPapers(string assessmentIDString)
        {
            bool result = true;

            try
            {
                if (!string.IsNullOrEmpty(assessmentIDString))
                {
                    List<int> assessmentIDList = assessmentIDString.Split(',').Select(int.Parse).ToList();
                    foreach (int assessmentID in assessmentIDList)
                    {
                        QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentID);

                        db.GroupAssessment.RemoveRange(questionPaper.GroupAssessment);

                        db.RequestReTest.RemoveRange(questionPaper.RequestReTest);

                        db.QuestionPaperQuestion.RemoveRange(questionPaper.QuestionPaperQuestion);

                        List<QuestionPaperSection> questionPaperSectionList = questionPaper.QuestionPaperSection.ToList();


                        List<QuestionPaperSectionQuestion> questionPaperSectionQuestionList = questionPaperSectionList.SelectMany(x => x.QuestionPaperSectionQuestion).ToList();

                        List<UserSectionStatus> userSectionStatuses = questionPaper.QuestionPaperSection.SelectMany(x => x.UserSectionStatus).ToList();
                        db.UserSectionStatus.RemoveRange(userSectionStatuses);
                        db.SaveChanges();
                        db.QuestionPaperSectionQuestion.RemoveRange(questionPaperSectionQuestionList);

                        db.QuestionPaperSection.RemoveRange(questionPaperSectionList);
                        db.BatchAssessment.RemoveRange(questionPaper.BatchAssessment);


                        List<UserAssessment> userQuestionPaperList = questionPaper.UserAssessment.ToList();
                        List<AssessmentSnapshot> assessmentSnapShotList = userQuestionPaperList.SelectMany(x => x.AssessmentSnapshot).ToList();
                        db.AssessmentSnapshot.RemoveRange(assessmentSnapShotList);
                        db.UserAssessment.RemoveRange(userQuestionPaperList);

                        db.QuestionPaper.Remove(questionPaper);
                    }
                    db.SaveChanges();
                }
                else { result = false; }
            }
            catch { result = false; }
            Response.Write(result);
        }

        #endregion

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }

        public ActionResult Take(int id = 0)
        {
            //id = 28;
            UserProfile userProfile = db.UserProfile.First(u => u.UserName.Equals(User.Identity.Name));
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            //var assessmentsIdList = userProfile.Group.GroupQuestionPaper.Where(ga => (!ga.StartDate.HasValue || (ga.StartDate.HasValue && ga.StartDate.Value <= indianTime)) && (!ga.StartDate.HasValue || (ga.EndDate.HasValue && ga.EndDate.Value >= indianTime))).Select(ga => ga.QuestionPaper.ID).ToList();

            QuestionPaper questionPaper = db.QuestionPaper.Find(id);
            if (questionPaper.AutoCreatedFromTemplate ?? false == true)
            {
                //StudentQuestionPaper studentQuestionPaper = d
            }

            OAP.Models.ViewModels.QuestionPaperViewModel questionPaperViewModel = GetQuestionPaperViewModel(questionPaper);

            if (questionPaper.QuestionPaperSection.Any(x => x.TechnicalQuestionPaperSectionQuestion.Count() > 0) && questionPaperViewModel != null)
            {
                foreach (var sectionViewModel in questionPaperViewModel.SectionList)
                {
                    if (sectionViewModel.Name == "Tech" && sectionViewModel.QuestionList.Count == 0)
                    {
                        var questionListModel = GetTechnicalQuestionList(questionPaper);
                        if (questionListModel != null)
                        {
                            sectionViewModel.QuestionList = questionListModel;
                        }
                    }
                }
            }

            if (questionPaperViewModel.ShowInstruction)
            {
                UserAssessment userQuestionPaper = userProfile.UserAssessment.Where(ua => ua.AssessmentID == id).FirstOrDefault();
                if (userQuestionPaper == null)
                {                                                               //Instruction not accepted
                    return View("~/Views/QuestionPaper/Instruction.cshtml", questionPaperViewModel);
                }
            }

            var assessmentsIdList = userProfile.Student.BatchStudent.SelectMany(ba => ba.Batch.BatchAssessment).Where(ga => (ga.StartDateTime <= indianTime) && ga.EndDateTime >= indianTime).Select(ga => ga.QuestionPaper.ID).Distinct().ToList();

            assessmentsIdList.AddRange(userProfile.Student.StudentAssessment.Where(ga => (ga.StartTime <= indianTime) && ga.EndTime >= indianTime).Select(ga => ga.QuestionPaper.ID).Distinct().ToList());

            // assessmentsIdList.AddRange(db.FormQuestionPaper.Where(ga => (ga.StartDateTime <= indianTime) && ga.EndDateTime >= indianTime).Select(ga => ga.QuestionPaper.ID).Distinct().ToList());
            //var TestPackageQuestionPapersIdList = 

            if (!assessmentsIdList.Contains(id))
            {
                questionPaperViewModel.NoPermission = true;
                return View(questionPaperViewModel);
            }
            if (questionPaperViewModel == null)
            {
                return HttpNotFound();
            }

            DateTime startTime = SetUserQuestionPaper(id, ref questionPaperViewModel);

            DateTime actualStartTime = questionPaper.UserAssessment.Where(x => x.UserID == userProfile.UserId).FirstOrDefault().StartTime.Value;
            TimeSpan test = startTime.Subtract(actualStartTime);


            double secondsToBeAdded = DateTime.Now.Subtract(startTime).TotalSeconds;


            questionPaperViewModel.StartTime = startTime;
            questionPaperViewModel.CurrentSection = questionPaperViewModel.SectionList[questionPaperViewModel.CurrentSectionIndex];
            //questionPaperViewModel.CurrentSection.QuestionList = Shuffle(questionPaperViewModel.CurrentSection.QuestionList).OrderBy(q => q.Question.PassageID).ToList();

            //if (questionPaper.CanShuffle ?? false)
            //    questionPaperViewModel.CurrentSection.QuestionList = Shuffle(questionPaperViewModel.CurrentSection.QuestionList).OrderBy(q => q.Question.PassageID).ToList();
            //else
            //    questionPaperViewModel.CurrentSection.QuestionList = questionPaperViewModel.CurrentSection.QuestionList.OrderBy(q => q.Question.PassageID).ToList();
            //questionPaperViewModel.RemainingSeconds = (questionPaperViewModel.CurrentSection.TimeLimit * 60) - DateTime.Now.Subtract(startTime).TotalSeconds;

            if (questionPaperViewModel.TimeLimit != null && questionPaperViewModel.TimeLimit != 0)
            {                                                                                   //Time limit not given for assessment
                                                                                                // questionPaperViewModel.RemainingSeconds = (questionPaperViewModel.TimeLimit * 60) - DateTime.Now.Subtract(startTime).TotalSeconds;
                                                                                                //  questionPaperViewModel.RemainingSeconds = (questionPaperViewModel.TimeLimit * 60) - startTime.Subtract(actualStartTime).TotalSeconds;
                questionPaperViewModel.RemainingSeconds = (questionPaper.UserAssessment.Where(x => x.UserID == userProfile.UserId).FirstOrDefault().RemainingTime ?? (questionPaperViewModel.TimeLimit * 60));

                questionPaperViewModel.IsSectionJump = true;
                questionPaperViewModel.CurrentQuestionNumber = 1;
            }
            else
            {                                                                                   //Time limt given for section
                                                                                                //questionPaperViewModel.RemainingSeconds = ((questionPaperViewModel.CurrentSection.TimeLimit * 60)) - DateTime.Now.Subtract(startTime).TotalSeconds;
                                                                                                //    questionPaperViewModel.RemainingSeconds = ((questionPaperViewModel.CurrentSection.TimeLimit * 60)) - startTime.Subtract(actualStartTime).TotalSeconds;

                questionPaperViewModel.RemainingSeconds = (questionPaper.UserAssessment.Where(x => x.UserID == userProfile.UserId).FirstOrDefault().UserSectionStatus.Where(y => y.SectionID == questionPaperViewModel.CurrentSection.ID).FirstOrDefault().RemainingTime ?? (questionPaperViewModel.CurrentSection.TimeLimit * 60));

                questionPaperViewModel.IsSectionJump = false;
                questionPaperViewModel.CurrentQuestionNumber = 1;
            }


            IQueryable<ColorCode> colorCode = db.ColorCode.OrderBy(x => x.Name);
            List<ColorCodeViewModel> colorCodeList = (from c in colorCode
                                                      select new ColorCodeViewModel()
                                                      {
                                                          ID = c.ID,
                                                          Name = c.Name,
                                                          HexCode = c.HexCode,
                                                      }).ToList();
            questionPaperViewModel.ColorCodeViewModelList = colorCodeList;

            ViewBag.Active = colorCodeList.Where(c => c.Name == "Active").Select(x => x.HexCode).FirstOrDefault();
            ViewBag.NotVisited = colorCodeList.Where(c => c.Name == "Not Visited").Select(x => x.HexCode).FirstOrDefault();
            ViewBag.VisitedUnanswer = colorCodeList.Where(c => c.Name == "Visited but Unanswer").Select(x => x.HexCode).FirstOrDefault();
            ViewBag.VisitedAnswered = colorCodeList.Where(c => c.Name == "Visited and Answered").Select(x => x.HexCode).FirstOrDefault();
            ViewBag.Marked = colorCodeList.Where(c => c.Name == "Marked").Select(x => x.HexCode).FirstOrDefault();
            List<BatchStudent> batchList = userProfile.Student.BatchStudent.ToList();
            if (batchList != null)
            {
                foreach (BatchStudent batch in batchList)
                {
                    List<BatchAssessment> batchQuestionPaper = db.BatchAssessment.Where(b => b.BatchID == batch.BatchID && b.AssessmentID == id).ToList();
                    try
                    {
                        questionPaperViewModel.CanReview = batchQuestionPaper.LastOrDefault().CanReview ?? false;
                    }
                    catch
                    {
                    }
                }
            }
            return View(questionPaperViewModel);

        }

        public ActionResult ShowQuestionNumberShowSection(int id = 0)
        {
            List<String> status = new List<string>();
            status.Add("true");
            status.Add("false");

            ViewBag.ShowQuestionNumber = status;
            ViewBag.ShowSection = status;
            return View();
        }

        // public void UpdateTestType(bool QuestionNumber, bool SectionName)
        //{

        //     QuestionPaperControls assessmentControls2 = db.QuestionPaperControls.Where(a => a.Controls.ToLower() == "ShowQuestionNumber".ToLower()).FirstOrDefault();
        //     QuestionPaperControls assessmentControls1 = db.QuestionPaperControls.Where(a => a.Controls.ToLower() == "ShowSection".ToLower()).FirstOrDefault();
        //     assessmentControls2.Status = QuestionNumber;
        //     assessmentControls1.Status = SectionName;
        //     db.Entry(assessmentControls2).State=EntityState.Modified;
        //     db.Entry(assessmentControls1).State = EntityState.Modified;
        //     db.SaveChanges();
        //     Response.Write("true");
        // }

        private static Random rng = new Random();
        public List<QuestionAnswerViewModel> Shuffle(List<QuestionAnswerViewModel> c)
        {
            QuestionAnswerViewModel[] a = new QuestionAnswerViewModel[c.Count];
            c.CopyTo(a, 0);
            byte[] b = new byte[a.Length];
            rng.NextBytes(b);
            Array.Sort(b, a);
            List<QuestionAnswerViewModel> questions = new List<QuestionAnswerViewModel>(a);
            return questions;
        }

        private DateTime SetUserQuestionPaper(int id, ref OAP.Models.ViewModels.QuestionPaperViewModel questionPaperViewModel)
        {
            //DateTime startTime = DateTime.Now;
            UserProfile profile = db.UserProfile.Where(up => up.UserName == User.Identity.Name).FirstOrDefault();
            UserAssessment userQuestionPaper = db.UserAssessment.Where(ua => ua.QuestionPaper.ID == id && (ua.IsActive ?? false) && ua.UserProfile.UserId == profile.UserId).FirstOrDefault();

            if (db.UserAssessment.Where(ua => ua.QuestionPaper.ID == id && ua.FinishTime.HasValue && ua.UserProfile.UserId == profile.UserId).Count() > 0)
            {
                questionPaperViewModel.TestComplete = true;
                return DateTime.Now;
            }

            if (questionPaperViewModel.TimeLimit != null && questionPaperViewModel.TimeLimit != 0)
            {                                                                                   //Time limit not given for assessment
                //questionPaperViewModel.RemainingSeconds = (questionPaperViewModel.TimeLimit * 60) - DateTime.Now.Subtract(startTime).TotalSeconds;
                questionPaperViewModel.IsSectionJump = true;
                questionPaperViewModel.CurrentQuestionNumber = 1;
            }
            else
            {                                                                                   //Time limt given for section
                //questionPaperViewModel.RemainingSeconds = ((questionPaperViewModel.CurrentSection.TimeLimit * 60)) - DateTime.Now.Subtract(startTime).TotalSeconds;
                questionPaperViewModel.IsSectionJump = false;
                questionPaperViewModel.CurrentQuestionNumber = 1;
            }

            if (userQuestionPaper == null)
            {
                userQuestionPaper = new UserAssessment();
                userQuestionPaper.StartTime = DateTime.Now;
                userQuestionPaper.QuestionPaper = db.QuestionPaper.Find(id);
                userQuestionPaper.UserProfile = profile;
                userQuestionPaper.CurrentSectionIndex = 0;
                userQuestionPaper.IsActive = true;
                db.UserAssessment.Add(userQuestionPaper);

                if (questionPaperViewModel.IsSectionJump)
                {
                    for (int k = 0; k < userQuestionPaper.QuestionPaper.QuestionPaperSection.ToList().Count(); k++)
                    {
                        UserSectionStatus sectionStatus = new UserSectionStatus();
                        sectionStatus.StartTime = userQuestionPaper.StartTime;
                        sectionStatus.UserAssessment = userQuestionPaper;
                        sectionStatus.QuestionPaperSection = userQuestionPaper.QuestionPaper.QuestionPaperSection.ToList()[k];
                        db.UserSectionStatus.Add(sectionStatus);
                    }
                }
                else
                {
                    UserSectionStatus sectionStatus = new UserSectionStatus();
                    sectionStatus.StartTime = userQuestionPaper.StartTime;
                    sectionStatus.UserAssessment = userQuestionPaper;
                    sectionStatus.QuestionPaperSection = userQuestionPaper.QuestionPaper.QuestionPaperSection.ToList()[0];
                    db.UserSectionStatus.Add(sectionStatus);
                }
                db.SaveChanges();
                questionPaperViewModel.SavedAnswers = "[]";
                return userQuestionPaper.StartTime.Value;
                // return sectionStatus.InterruptedTime != null ? sectionStatus.InterruptedTime.Value : sectionStatus.StartTime.Value;

            }
            else
            {
                questionPaperViewModel.CurrentSectionIndex = userQuestionPaper.CurrentSectionIndex ?? 0;
                if (userQuestionPaper.UserSectionStatus.Count > userQuestionPaper.CurrentSectionIndex) // Refreshing/relaoding started section old
                {
                    if (userQuestionPaper.InterruptedTime != null)
                    {
                        userQuestionPaper.StartAfterInterruption = DateTime.Now;
                        db.Entry(userQuestionPaper).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    UserSectionStatus sectionStatus = userQuestionPaper.UserSectionStatus.ToList()[userQuestionPaper.CurrentSectionIndex ?? 0];
                    if (sectionStatus.InterruptedTime != null)
                    {
                        sectionStatus.StartAfterInterruption = DateTime.Now;
                        db.Entry(sectionStatus).State = EntityState.Modified;
                        db.SaveChanges();
                    }

                    List<AssessmentSnapshot> snapshotlist = sectionStatus.UserAssessment.AssessmentSnapshot.ToList();

                    Mapper.CreateMap<AssessmentSnapshot, SnapshotViewModel>().ForMember(dest => dest.AnswerID, opt => opt.MapFrom(src => src.Answer.ID)).ForMember(dest => dest.QuestionID, opt => opt.MapFrom(src => src.Question.ID));
                    List<SnapshotViewModel> snapshotViewmodelList = Mapper.Map<List<SnapshotViewModel>>(snapshotlist);

                    System.Web.Script.Serialization.JavaScriptSerializer oSerializer = new System.Web.Script.Serialization.JavaScriptSerializer();
                    string responseList = oSerializer.Serialize(snapshotViewmodelList);
                    questionPaperViewModel.SavedAnswers = string.IsNullOrWhiteSpace(responseList) ? "[]" : responseList;

                    if (userQuestionPaper.QuestionPaper.TimeLimit == null || userQuestionPaper.QuestionPaper.TimeLimit == 0)
                        return sectionStatus.StartAfterInterruption != null ? sectionStatus.StartAfterInterruption.Value : sectionStatus.StartTime.Value;
                    // return sectionStatus.InterruptedTime != null ? sectionStatus.InterruptedTime.Value : sectionStatus.StartTime.Value;

                    //  return userQuestionPaper.StartTime.Value;
                    else
                        return userQuestionPaper.StartAfterInterruption != null ? userQuestionPaper.StartAfterInterruption.Value : userQuestionPaper.StartTime.Value;
                    //  return userQuestionPaper.InterruptedTime != null ? userQuestionPaper.InterruptedTime.Value : userQuestionPaper.StartTime.Value;


                }
                else //New section
                {

                    UserSectionStatus sectionStatus = new UserSectionStatus();
                    sectionStatus.StartTime = DateTime.Now;
                    sectionStatus.UserAssessment = userQuestionPaper;
                    sectionStatus.QuestionPaperSection = userQuestionPaper.QuestionPaper.QuestionPaperSection.ToList()[userQuestionPaper.CurrentSectionIndex ?? 0];
                    db.UserSectionStatus.Add(sectionStatus);
                    db.SaveChanges();
                    questionPaperViewModel.SavedAnswers = "[]";


                    if (questionPaperViewModel.IsSectionJump)
                    {
                        for (int k = 1; k < userQuestionPaper.QuestionPaper.QuestionPaperSection.ToList().Count(); k++)
                        {
                            //UserSectionStatus sectionStatus = new UserSectionStatus();
                            sectionStatus.StartTime = userQuestionPaper.StartTime;
                            sectionStatus.UserAssessment = userQuestionPaper;
                            sectionStatus.QuestionPaperSection = userQuestionPaper.QuestionPaper.QuestionPaperSection.ToList()[k];
                            db.UserSectionStatus.Add(sectionStatus);
                        }
                    }

                    if (userQuestionPaper.QuestionPaper.TimeLimit == null || userQuestionPaper.QuestionPaper.TimeLimit == 0)
                        // return sectionStatus.InterruptedTime != null ? sectionStatus.InterruptedTime.Value : sectionStatus.StartTime.Value;
                        return sectionStatus.StartAfterInterruption != null ? sectionStatus.StartAfterInterruption.Value : sectionStatus.StartTime.Value;

                    //return userQuestionPaper.StartTime.Value;
                    else
                        //   return userQuestionPaper.InterruptedTime != null ? userQuestionPaper.InterruptedTime.Value : userQuestionPaper.StartTime.Value;
                        return userQuestionPaper.StartAfterInterruption != null ? userQuestionPaper.StartAfterInterruption.Value : userQuestionPaper.StartTime.Value;

                    //  return sectionStatus.InterruptedTime!=null?sectionStatus.InterruptedTime.Value:sectionStatus.StartTime.Value;

                }
            }
        }

        [OutputCache(Duration = 300)]
        private OAP.Models.ViewModels.QuestionPaperViewModel GetQuestionPaperViewModel(QuestionPaper questionPaper)
        {
            OAP.Models.ViewModels.QuestionPaperViewModel questionPaperViewModel;
            if (questionPaper == null)
            {
                return null;
            }
            Mapper.CreateMap<QuestionPaper, OAP.Models.ViewModels.QuestionPaperViewModel>().ForMember(dest => dest.SectionList, opt => opt.MapFrom(src => src.QuestionPaperSection));
            Mapper.CreateMap<QuestionPaperSection, OAP.Models.ViewModels.SectionViewModel>().ForMember(dest => dest.QuestionList, opt => opt.MapFrom(src => src.QuestionPaperSectionQuestion));
            Mapper.CreateMap<QuestionPaperSectionQuestion, QuestionAnswerViewModel>().ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Question.Description));
            Mapper.CreateMap<Passage, OAP.Models.ViewModels.PassageViewModel>();
            //Mapper.CreateMap<Question, OAP.Models.ViewModels.QuestionViewModel>().ForMember(dest => dest.AnswerList, opt => opt.MapFrom(src => src.Answer));
            Mapper.CreateMap<Question, OAP.Models.ViewModels.QuestionViewModel>().ForMember(dest => dest.AnswerList, opt => opt.MapFrom(src => src.Answer)).ForMember(dest => dest.PassageViewModel, opt => opt.MapFrom(src => src.Passage)).ForMember(dest => dest.QuestionType, opt => opt.MapFrom(src => src.QuestionType != null ? src.QuestionType.Name : ""));
            //Mapper.CreateMap<Question, OAP.Models.ViewModels.QuestionViewModel>().ForMember(dest => dest.PassageViewModel, opt => opt.MapFrom(src => src.Passage));
            Mapper.CreateMap<Answer, OAP.Models.ViewModels.AnswerViewModel>();
            questionPaperViewModel = Mapper.Map<OAP.Models.ViewModels.QuestionPaperViewModel>(questionPaper);
            questionPaperViewModel.IsSectionJump = questionPaper.CanJumpSections ?? false;
            return questionPaperViewModel;
        }

        private List<QuestionAnswerViewModel> GetTechnicalQuestionList(QuestionPaper questionPaper)
        {
            var questionPaperSectionList = questionPaper.QuestionPaperSection.Where(x => x.TechnicalQuestionPaperSectionQuestion.Count() > 0).FirstOrDefault();

            var sectionViewModelList = new List<QuestionAnswerViewModel>();

            foreach (var technicalQuestion in questionPaperSectionList.TechnicalQuestionPaperSectionQuestion)
            {
                var sectionViewModel = new QuestionAnswerViewModel()
                {
                    ID = technicalQuestion.TechnicalQuestionID,
                    Description = technicalQuestion.TechnicalQuestion.Description,
                    TechnicalQuestion = new Domain.ViewModel.TechnicalQuestionViewModel()
                    {
                        ID = technicalQuestion.TechnicalQuestion.ID,
                        AnswerViewModelList= MapperHelper.MapToTechnicalAnswerList(technicalQuestion.TechnicalQuestion.TechnicalAnswer.ToList()),
                        Description = technicalQuestion.TechnicalQuestion.Description,
                        LanguagesString = technicalQuestion.TechnicalQuestion.Languages,
                        SolutionType = technicalQuestion.TechnicalQuestion.SolutionType,
                        Title = technicalQuestion.TechnicalQuestion.Title
                    }
                };

                sectionViewModelList.Add(sectionViewModel);
            }

            return sectionViewModelList;
        }

        [HttpPost]
        public ActionResult Confirm(int questionId, int questionIndex, string answerIdString, string questionTiming, string questionType)
        {
            if (Request.IsAuthenticated)
            {
                UserProfile profile = db.UserProfile.Where(up => up.UserName == User.Identity.Name).FirstOrDefault();
                if (profile == null)
                {
                    var data1 = new { result = "-1" };
                    return Json(data1);
                }
                UserAssessment activeUserQuestionPaper = profile.UserAssessment.Where(ua => ua.IsActive ?? false).FirstOrDefault();

                if (questionType.ToLower().Contains("single select"))
                {                                                           //Question type is single select
                    int answerId = Convert.ToInt32(answerIdString);
                    AssessmentSnapshot snapShot = activeUserQuestionPaper.AssessmentSnapshot.Where(snap => snap.Question.ID == questionId).FirstOrDefault();
                    UpdateQuestionPaperSnapshot(snapShot, answerId, questionTiming, questionIndex, activeUserQuestionPaper, questionId, questionType);

                }
                else if (questionType.ToLower().Contains("multi select"))
                {                                                           //Question type is multi select
                    List<int> selectedAnswersList = answerIdString.Split(',').Select(int.Parse).ToList();

                    List<AssessmentSnapshot> assessmentSnapshotList = activeUserQuestionPaper.AssessmentSnapshot.Where(snap => snap.QuestionID == questionId && !selectedAnswersList.Contains(snap.AnswerID ?? 0)).ToList();
                    db.AssessmentSnapshot.RemoveRange(assessmentSnapshotList);
                    db.SaveChanges();

                    foreach (int answerID in selectedAnswersList)
                    {
                        AssessmentSnapshot snapShot = activeUserQuestionPaper.AssessmentSnapshot.Where(snap => snap.Question.ID == questionId && snap.AnswerID == answerID).FirstOrDefault();
                        if (snapShot == null)
                        {
                            UpdateQuestionPaperSnapshot(snapShot, answerID, questionTiming, questionIndex, activeUserQuestionPaper, questionId, questionType);
                        }
                    }
                }
                else if (questionType.ToLower().Contains("open ended"))
                {                                                       //Question is open ended. It has no answer
                    string answerContent = Request["answerContent"];
                    AssessmentSnapshot snapShot = activeUserQuestionPaper.AssessmentSnapshot.Where(snap => snap.Question.ID == questionId).FirstOrDefault();
                    if (snapShot != null)
                    {                                                               //Question already confirmed
                        snapShot.AnswerContent = answerContent;
                        if (snapShot.TimeTaken == null)
                        {
                            snapShot.TimeTaken = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(questionTiming.Split(',')[questionIndex - 1])));
                            //snapShot.TimeTaken = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(questionTiming)));

                        }
                        else
                        {
                            int extraTimeSpent = 0;
                            if (snapShot.TimeTaken.Value > Convert.ToInt32(Math.Ceiling(Convert.ToDouble(questionTiming.Split(',')[questionIndex - 1]))))
                            {
                                extraTimeSpent = snapShot.TimeTaken.Value - Convert.ToInt32(Math.Ceiling(Convert.ToDouble(questionTiming.Split(',')[questionIndex - 1])));
                            }
                            else
                            {
                                extraTimeSpent = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(questionTiming.Split(',')[questionIndex - 1]))) - snapShot.TimeTaken.Value;

                            }
                            snapShot.TimeTaken = snapShot.TimeTaken + extraTimeSpent;
                        }
                        db.Entry(snapShot).State = EntityState.Modified;
                        db.SaveChanges();
                    }
                    else
                    {                                                              //Newly confirmed the question
                        snapShot = new AssessmentSnapshot();
                        snapShot.Question = db.Question.Find(questionId);
                        snapShot.AnswerContent = answerContent;
                        snapShot.UserAssessment = activeUserQuestionPaper;
                        snapShot.TimeTaken = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(questionTiming.Split(',')[questionIndex - 1])));
                        db.AssessmentSnapshot.Add(snapShot);
                        db.SaveChanges();
                    }
                }

                var data2 = new { result = "1", questionIndex = questionIndex, questionId = questionId };
                return Json(data2);
            }
            var data3 = new { result = "-2" };
            return Json(data3);
        }

        public void UpdateQuestionPaperSnapshot(AssessmentSnapshot snapShot, int answerId, string questionTiming, int questionIndex, UserAssessment activeUserAssessment, int questionId, string questionType)
        {
            if (snapShot != null)
            {
                snapShot.Answer = db.Answer.Find(answerId);
                if (snapShot.TimeTaken == null)
                {
                    snapShot.TimeTaken = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(questionTiming.Split(',')[questionIndex - 1])));
                    //snapShot.TimeTaken = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(questionTiming)));

                }
                else
                {
                    int extraTimeSpent = 0;
                    if (snapShot.TimeTaken.Value > Convert.ToInt32(Math.Ceiling(Convert.ToDouble(questionTiming.Split(',')[questionIndex - 1]))))
                    {
                        extraTimeSpent = snapShot.TimeTaken.Value - Convert.ToInt32(Math.Ceiling(Convert.ToDouble(questionTiming.Split(',')[questionIndex - 1])));
                    }
                    else
                    {
                        extraTimeSpent = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(questionTiming.Split(',')[questionIndex - 1]))) - snapShot.TimeTaken.Value;

                    }
                    snapShot.TimeTaken = snapShot.TimeTaken + extraTimeSpent;
                }
                db.Entry(snapShot).State = EntityState.Modified;
                db.SaveChanges();
            }
            else
            {
                snapShot = new AssessmentSnapshot();
                snapShot.Answer = db.Answer.Find(answerId);
                snapShot.Question = db.Question.Find(questionId);
                snapShot.UserAssessment = activeUserAssessment;
                snapShot.TimeTaken = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(questionTiming.Split(',')[questionIndex - 1])));
                db.AssessmentSnapshot.Add(snapShot);
                db.SaveChanges();
            }
        }

        [HttpPost]
        public ActionResult TimeSnapShot()
        {
            if (Request.IsAuthenticated)
            {
                return Content("1");
            }
            return Content("-1");
        }

        [HttpPost]
        public ActionResult Submit(bool submitmethod = false)
        {
            if (Request.IsAuthenticated)
            {
                UserProfile profile = db.UserProfile.Where(up => up.UserName == User.Identity.Name).FirstOrDefault();
                if (profile == null)
                {
                    var data1 = new { result = "-1" };
                    return Json(data1);
                }
                UserAssessment activeUserQuestionPaper = profile.UserAssessment.Where(ua => ua.IsActive ?? false).FirstOrDefault();
                if (activeUserQuestionPaper == null)
                {
                    var data3 = new { result = "-2" };
                    return Json(data3);
                }
                if (submitmethod) //Submitmethod=IsJumpSection
                {
                    List<QuestionPaperSection> questionPaperSectionList = activeUserQuestionPaper.QuestionPaper.QuestionPaperSection.ToList();
                    var data2 = new { result = "" };
                    //foreach (QuestionPaperSection questionPaperSection in questionPaperSectionList)
                    for (int i = 0; i < questionPaperSectionList.Count(); i++)
                    {
                        //int sectionIndex = (i != 0 ? (i - 1) : 0);
                        UserSectionStatus sectionStatus = activeUserQuestionPaper.UserSectionStatus.ToList()[i];
                        sectionStatus.MarkComplete();
                        //activeUserQuestionPaper.CurrentSectionIndex = i;
                        db.Entry(sectionStatus).State = EntityState.Modified;
                        db.Entry(sectionStatus.UserAssessment).State = EntityState.Modified;
                        db.SaveChanges();

                        data2 = new { result = "1" };
                        decimal obtainedMarks = CalculateMark(activeUserQuestionPaper, profile);
                        activeUserQuestionPaper.Marks = obtainedMarks;
                        activeUserQuestionPaper.CurrentSectionIndex = questionPaperSectionList.Count() - 1;
                        db.Entry(activeUserQuestionPaper).State = EntityState.Modified;
                        db.SaveChanges();

                    }
                    return Json(data2);
                }
                else
                {
                    int sectionIndex = ((activeUserQuestionPaper.CurrentSectionIndex ?? 0) != 0 ? ((activeUserQuestionPaper.CurrentSectionIndex ?? 0) - 1) : 0);
                    UserSectionStatus sectionStatus = activeUserQuestionPaper.UserSectionStatus.ToList()[sectionIndex];
                    sectionStatus.MarkComplete();
                    activeUserQuestionPaper.CurrentSectionIndex++;
                    db.Entry(sectionStatus).State = EntityState.Modified;
                    db.Entry(sectionStatus.UserAssessment).State = EntityState.Modified;
                    db.SaveChanges();

                    if (activeUserQuestionPaper.CurrentSectionIndex < activeUserQuestionPaper.QuestionPaper.QuestionPaperSection.Count()) //There are more sections to write
                    {
                        var data2 = new { result = "2" };
                        return Json(data2);
                        //return Take(activeUserQuestionPaper.QuestionPaper.ID);
                    }
                    else                        //All sections completed
                    {
                        var data2 = new { result = "1" };
                        decimal obtainedMarks = CalculateMark(activeUserQuestionPaper, profile);
                        activeUserQuestionPaper.Marks = obtainedMarks;
                        db.Entry(activeUserQuestionPaper).State = EntityState.Modified;
                        db.SaveChanges();
                        return Json(data2);
                    }
                }
                //WebSecurity.Logout();

            }
            var data4 = new { result = "-2" };
            return Json(data4);
        }

        public void SaveInterruption(bool submitmethod = false)
        {
            DateTime interruptedTime = DateTime.Now;
            if (Request.IsAuthenticated)
            {
                UserProfile profile = db.UserProfile.Where(up => up.UserName == User.Identity.Name).FirstOrDefault();
                if (profile != null)
                {
                    UserAssessment activeUserQuestionPaper = profile.UserAssessment.Where(ua => ua.IsActive ?? false).FirstOrDefault();
                    if (activeUserQuestionPaper != null)
                    {
                        if (submitmethod) //Submitmethod=IsJumpSection
                        {
                            List<QuestionPaperSection> questionPaperSectionList = activeUserQuestionPaper.QuestionPaper.QuestionPaperSection.ToList();
                            var data2 = new { result = "" };
                            //foreach (QuestionPaperSection questionPaperSection in questionPaperSectionList)
                            for (int i = 0; i < questionPaperSectionList.Count(); i++)
                            {
                                //int sectionIndex = (i != 0 ? (i - 1) : 0);
                                UserSectionStatus sectionStatus = activeUserQuestionPaper.UserSectionStatus.ToList()[i];
                                //activeUserQuestionPaper.CurrentSectionIndex = i;
                                sectionStatus.InterruptedTime = interruptedTime;
                                DateTime startTime = sectionStatus.StartAfterInterruption != null ? sectionStatus.StartAfterInterruption.Value : sectionStatus.StartTime.Value;
                                double spendTime = interruptedTime.Subtract(startTime).TotalSeconds;
                                if (sectionStatus.RemainingTime == null)
                                {
                                    sectionStatus.RemainingTime = (questionPaperSectionList[i].TimeLimit * 60) - Convert.ToInt32(spendTime);
                                }
                                else
                                {
                                    sectionStatus.RemainingTime = sectionStatus.RemainingTime - Convert.ToInt32(spendTime);
                                }
                                db.Entry(activeUserQuestionPaper).State = EntityState.Modified;


                                db.Entry(sectionStatus).State = EntityState.Modified;
                                db.SaveChanges();


                            }

                        }
                        else
                        {
                            int sectionIndex = ((activeUserQuestionPaper.CurrentSectionIndex ?? 0) != 0 ? ((activeUserQuestionPaper.CurrentSectionIndex ?? 0) - 1) : 0);
                            UserSectionStatus sectionStatus = activeUserQuestionPaper.UserSectionStatus.ToList()[sectionIndex];
                            sectionStatus.InterruptedTime = interruptedTime;
                            db.Entry(sectionStatus).State = EntityState.Modified;
                            db.SaveChanges();


                        }
                        activeUserQuestionPaper.InterruptedTime = interruptedTime;
                        DateTime startTime1 = activeUserQuestionPaper.StartAfterInterruption != null ? activeUserQuestionPaper.StartAfterInterruption.Value : activeUserQuestionPaper.StartTime.Value;
                        double spendTime1 = interruptedTime.Subtract(startTime1).TotalSeconds;
                        if (activeUserQuestionPaper.RemainingTime == null)
                        {
                            activeUserQuestionPaper.RemainingTime = (activeUserQuestionPaper.QuestionPaper.TimeLimit * 60) - Convert.ToInt32(spendTime1);
                        }
                        else
                        {
                            activeUserQuestionPaper.RemainingTime = activeUserQuestionPaper.RemainingTime - Convert.ToInt32(spendTime1);

                        }
                        db.Entry(activeUserQuestionPaper).State = EntityState.Modified;
                        db.SaveChanges();
                        //WebSecurity.Logout();

                    }

                }
            }
        }

        //[HttpPost]
        public void JumpSections(int index = 0)
        {
            if (Request.IsAuthenticated)
            {
                UserProfile profile = db.UserProfile.Where(up => up.UserName == User.Identity.Name).FirstOrDefault();
                if (profile == null)
                {
                    var data1 = new { result = "-1" };
                    Response.Write(data1);
                    // return Json(data1);
                }
                UserAssessment activeUserQuestionPaper = profile.UserAssessment.Where(ua => ua.IsActive ?? false).FirstOrDefault();
                if (activeUserQuestionPaper == null)
                {
                    var data3 = new { result = "-2" };
                    Response.Write(data3);
                    // return Json(data3);
                }
                int sectionIndex = ((activeUserQuestionPaper.CurrentSectionIndex ?? 0) != 0 ? ((activeUserQuestionPaper.CurrentSectionIndex ?? 0) - 1) : 0);
                //UserSectionStatus sectionStatus = activeUserQuestionPaper.UserSectionStatus.ToList()[sectionIndex];
                //sectionStatus.MarkComplete();
                //activeUserQuestionPaper.CurrentSectionIndex++;
                activeUserQuestionPaper.CurrentSectionIndex = index;
                //db.Entry(sectionStatus).State = EntityState.Modified;
                //db.Entry(sectionStatus.UserQuestionPaper).State = EntityState.Modified;
                //db.SaveChanges();

                if (activeUserQuestionPaper.CurrentSectionIndex < activeUserQuestionPaper.QuestionPaper.QuestionPaperSection.Count()) //There are more sections to write
                {
                    var data2 = new { result = "2" };

                    //decimal obtainedMarks = CalculateMark(activeUserQuestionPaper, profile);
                    //activeUserQuestionPaper.Marks = obtainedMarks;
                    db.Entry(activeUserQuestionPaper).State = EntityState.Modified;
                    db.SaveChanges();
                    Response.Write(data2);
                    //return Json(data2);
                    //return Take(activeUserQuestionPaper.QuestionPaper.ID);
                }
                else                        //All sections completed
                {
                    var data2 = new { result = "1" };
                    decimal obtainedMarks = CalculateMark(activeUserQuestionPaper, profile);
                    activeUserQuestionPaper.Marks = obtainedMarks;
                    db.Entry(activeUserQuestionPaper).State = EntityState.Modified;
                    db.SaveChanges();
                    Response.Write(data2);
                    // return Json(data2);
                }
                //WebSecurity.Logout();

            }
            //var data4 = new { result = "-2" };
            // Response.Write(data4);

            //return Json(data4);
        }

        #region CalculateMark

        public decimal CalculateMark(UserAssessment userAssessment, UserProfile userProfile)
        {
            List<QuestionPaperSection> questionPaperSectionList = userAssessment.QuestionPaper.QuestionPaperSection.ToList();
            decimal totalMarksScored = 0;
            foreach (QuestionPaperSection assessemntSection in questionPaperSectionList)
            {
                List<QuestionPaperSectionQuestion> questionPaperSectionQuestionList = assessemntSection.QuestionPaperSectionQuestion.ToList();
                foreach (QuestionPaperSectionQuestion questionPaperSectionQuestion in questionPaperSectionQuestionList)
                {
                    if (questionPaperSectionQuestion.Question.QuestionType.Name.ToLower().Equals("single select"))
                    {                                                                           //Question type is single select

                        Answer correctAnswer = questionPaperSectionQuestion.Question.Answer.Where(a => a.IsCorrect == true).FirstOrDefault();
                        Answer userSelectedAnswer = userAssessment.AssessmentSnapshot.Where(asn => asn.Question.ID == questionPaperSectionQuestion.Question.ID).Select(x => x.Answer).FirstOrDefault();
                        if (correctAnswer != null && userSelectedAnswer != null)
                        {                                               //User confirmed the answer
                            //if (correctAnswerList.Select(x => x.ID).ToList().Contains(userSelectedAnswer.ID))
                            if (correctAnswer.ID == userSelectedAnswer.ID)
                            {                                                                                       //User selected the correct answer
                                decimal mark = questionPaperSectionQuestion.QuestionPaperSection.Mark != null ? questionPaperSectionQuestion.QuestionPaperSection.Mark.Value : (questionPaperSectionQuestion.QuestionPaperSection.QuestionPaper.Marks ?? 0);
                                totalMarksScored = totalMarksScored + mark;
                            }
                            else
                            {                                                                                       //Selected answer was incorrect
                                decimal negativeMarks = questionPaperSectionQuestion.QuestionPaperSection.NegativeMark == null ? (questionPaperSectionQuestion.QuestionPaperSection.QuestionPaper.NegativeMarks ?? 0) : questionPaperSectionQuestion.QuestionPaperSection.NegativeMark.Value;
                                totalMarksScored = totalMarksScored - negativeMarks;
                            }
                        }
                        else
                        {                                               //User not confirmed the answer for that question

                        }
                    }
                    else if (questionPaperSectionQuestion.Question.QuestionType.Name.ToLower().Equals("multi select"))
                    {                                                                           //Question type is multi select
                        List<Answer> correctAnswerList = questionPaperSectionQuestion.Question.Answer.Where(a => a.IsCorrect == true).ToList();
                        List<Answer> userSelectedAnswerList = userAssessment.AssessmentSnapshot.Where(asn => asn.QuestionID == questionPaperSectionQuestion.Question.ID).Select(x => x.Answer).ToList();
                        if (userSelectedAnswerList != null && userSelectedAnswerList.Count() > 0)
                        {                                                                           //user confirmed the answered for the question
                            if (correctAnswerList.Except(userSelectedAnswerList).Count() > 0 || userSelectedAnswerList.Except(correctAnswerList).Count() > 0)
                            {                                                                                   //Some of the answers are incorrect
                                decimal negativeMarks = questionPaperSectionQuestion.QuestionPaperSection.NegativeMark == null ? 0 : questionPaperSectionQuestion.QuestionPaperSection.NegativeMark.Value;
                                totalMarksScored = totalMarksScored - negativeMarks;
                            }
                            else
                            {                                                                                   //User selected the correct answers
                                decimal mark = questionPaperSectionQuestion.QuestionPaperSection.Mark != null ? questionPaperSectionQuestion.QuestionPaperSection.Mark.Value : 0;
                                totalMarksScored = totalMarksScored + mark;
                            }
                        }
                        else
                        {                                                                           //User not confirmed the answer for the question

                        }
                    }
                    else if (questionPaperSectionQuestion.Question.QuestionType.Name.ToLower().Equals("open ended"))
                    {

                    }
                }
            }
            return totalMarksScored;
        }

        #endregion

        #region UploadQuestions  Old

        //[HttpPost]
        //public ActionResult UploadQuestion(HttpPostedFileBase file, OAP.Domain.ViewModel.QuestionPaperViewModel questionPaperViewModel)
        //{
        //    List<OAP.Domain.ViewModel.PassageViewModel> passageViewModelList = new List<OAP.Domain.ViewModel.PassageViewModel>();
        //    if (file != null && questionPaperViewModel.ID != 0)
        //    {                                                           //File selected
        //        string guidStirng = Guid.NewGuid().ToString();
        //        string newPath = Server.MapPath("~/Upload/" + guidStirng + "/" + guidStirng + ".html");
        //        if (!Directory.Exists(Server.MapPath("~/Upload/" + guidStirng)))
        //            Directory.CreateDirectory(Server.MapPath("~/Upload/" + guidStirng));
        //        if (!Directory.Exists(Server.MapPath("~/Upload/" + guidStirng + "/Images")))
        //            Directory.CreateDirectory(Server.MapPath("~/Upload/" + guidStirng + "/Images"));
        //        try
        //        {
        //            string extension = file.FileName.Split('.')[1];
        //            string path = Server.MapPath("~/Upload/" + guidStirng + "/Images" + guidStirng + "." + extension);
        //            file.SaveAs(path);

        //            //Application objWord = new Application();
        //            //objWord.Documents.Open(FileName: path);
        //            //objWord.Visible = false;
        //            //if (objWord.Documents.Count > 0)
        //            //{
        //            //    Microsoft.Office.Interop.Word.Document oDoc = objWord.ActiveDocument;
        //            //    oDoc.SaveAs(FileName: newPath, FileFormat: 10);
        //            //    oDoc.Close(SaveChanges: false);
        //            //}
        //        }
        //        finally
        //        {
        //            //objWord.Application.Quit(SaveChanges: false);
        //        }
        //        HtmlFormatter htmlConverter = new HtmlFormatter();
        //        string fileContent = "";
        //        fileContent = htmlConverter.ConvertHtmlToString(newPath);

        //        //        StringBuilder sb = new StringBuilder();
        //        //        if (!string.IsNullOrEmpty(fileContent))
        //        //        {

        //        //            fileContent = htmlConverter.FormatFileContent(fileContent);
        //        //        }
        //        //        QuestionPaper assessment = db.QuestionPaper.Find(questionPaperViewModel.ID);
        //        //        passageViewModelList = CreatePassageQuestionsAnswers(fileContent, guidStirng, assessment);
        //        //        return View();
        //        //        //return RedirectToAction("QuestionPaperList");
        //        //    }
        //        //    else
        //        //    {                                                               //File not selected
        //        //        return View();
        //        //    }
        //        //}

        //        //public List<OAP.Domain.ViewModel.PassageViewModel> CreatePassageQuestionsAnswers(string fileContent, string guidString, QuestionPaper assessment)
        //        //{
        //        //    int processPassage = 0;
        //        //    bool isContinueQuestion = false;
        //        //    bool isContinueAnswer = false;
        //        //    bool isNewPassage = true;

        //        //    Passage currentPassage = new Passage();
        //        //    Question currentQuestion = new Question();
        //        //    List<Answer> answers = new List<Answer>();
        //        //    StringBuilder sb = new StringBuilder();

        //        //    List<OAP.Domain.ViewModel.PassageViewModel> passageViewModelList = new List<OAP.Domain.ViewModel.PassageViewModel>();
        //        //    List<OAP.Domain.ViewModel.QuestionViewModel> questionViewModelList = new List<OAP.Domain.ViewModel.QuestionViewModel>();
        //        //    OAP.Domain.ViewModel.PassageViewModel passageViewModel = new OAP.Domain.ViewModel.PassageViewModel();
        //        //    OAP.Domain.ViewModel.QuestionViewModel questionViewModel = new OAP.Domain.ViewModel.QuestionViewModel();
        //        //    List<OAP.Domain.ViewModel.AnswerViewModel> answerViewModelList = new List<OAP.Domain.ViewModel.AnswerViewModel>();
        //        //    int passageStartIndex = 0, passageEndIndex = 0;
        //        //    if (!string.IsNullOrEmpty(fileContent))
        //        //    {
        //        //        XDocument document = XDocument.Parse(fileContent);
        //        //        XElement root = document.Elements().FirstOrDefault().Elements().FirstOrDefault();
        //        //        {
        //        //            int totalQuestions = root.Elements().Where(e => !(e.Elements().First().Value.Contains("-"))).Count() / 2;
        //        //            foreach (XElement tr in root.Elements())
        //        //            {
        //        //                if (tr.Value.Trim() == "")
        //        //                {
        //        //                    continue;
        //        //                }
        //        //                if (tr.Name == "tr")
        //        //                {
        //        //                    answers = new List<Answer>();
        //        //                    List<XElement> columns = tr.Elements().ToList();
        //        //                    int count = columns.Count;
        //        //                    var node = (columns[0].Elements().ToList()[0].FirstNode);

        //        //                    if (processPassage == 0)
        //        //                    {                               //Processing first line
        //        //                        var currentNode = (XElement)node;
        //        //                        //if (!string.IsNullOrEmpty(currentNode.Value.ToString()))
        //        //                        //{
        //        //                        if (isNewPassage)
        //        //                        {                                                                   //Questions and answers saved
        //        //                            if (currentNode.Value.ToString().Contains("-"))
        //        //                            {                                                               //Having passage
        //        //                                passageViewModel = new OAP.Domain.ViewModel.PassageViewModel();
        //        //                                passageStartIndex = Convert.ToInt16(currentNode.Value.Split('-')[0]);
        //        //                                passageEndIndex = Convert.ToInt16(currentNode.Value.Split('-')[1]);
        //        //                                string passageString = "";
        //        //                                foreach (var element in columns[1].Elements())
        //        //                                {
        //        //                                    passageString = passageString + element.ToString();
        //        //                                }
        //        //                                currentPassage = new Passage();
        //        //                                currentPassage.Description = passageString;
        //        //                                currentPassage.CanShuffle = true;
        //        //                                db.Passage.Add(currentPassage);
        //        //                                db.SaveChanges();
        //        //                                isContinueQuestion = true;
        //        //                                isNewPassage = false;
        //        //                                isContinueAnswer = false;
        //        //                                passageViewModel = MapperHelper.MapToViewModel(currentPassage);
        //        //                                continue;
        //        //                            }
        //        //                            else
        //        //                            {
        //        //                                isContinueQuestion = true;
        //        //                                isNewPassage = false;
        //        //                                isContinueAnswer = false;
        //        //                                continue;
        //        //                            }
        //        //                        }
        //        //                        else if (isContinueQuestion)
        //        //                        {                                                             //Continue to save Question
        //        //                            questionViewModel = new OAP.Domain.ViewModel.QuestionViewModel();
        //        //                            int questionNumber = Convert.ToInt16(columns[0].Value.ToString());
        //        //                            string questionString = "";
        //        //                            foreach (var element in columns[1].Elements())
        //        //                            {
        //        //                                questionString = questionString + element.ToString();
        //        //                            }
        //        //                            currentQuestion = new Question();
        //        //                            if (questionNumber >= passageStartIndex && questionNumber <= passageEndIndex)
        //        //                                currentQuestion.Passage = currentPassage;
        //        //                            currentQuestion.Description = questionString;
        //        //                            db.Question.Add(currentQuestion);
        //        //                            db.SaveChanges();

        //        //                            if (assessment != null)
        //        //                            {
        //        //                                QuestionPaperQuestion assessmentQuestion = new QuestionPaperQuestion();
        //        //                                assessmentQuestion.QuestionPaper = assessment;
        //        //                                assessmentQuestion.Question = currentQuestion;
        //        //                                db.QuestionPaperQuestion.Add(assessmentQuestion);
        //        //                                db.SaveChanges();
        //        //                            }
        //        //                            List<string> imageSourceStringList = new List<string>();
        //        //                            foreach (var element in columns[1].Elements())
        //        //                            {
        //        //                                if (element.ToString().Contains("img"))
        //        //                                {                                                                                       //Question having image
        //        //                                    var imageNodes = columns[1].Descendants("img");
        //        //                                    var newImageNodes = columns[1].Descendants("img").ToList();
        //        //                                    string actualSource = Server.MapPath("~/Upload/") + guidString + "/" + guidString + "_files";
        //        //                                    int processing = 0;
        //        //                                    foreach (var item in imageNodes)
        //        //                                    {
        //        //                                        string imageSource = item.Attribute("src").Value;
        //        //                                        int totalCount = imageSource.Split('/').Count();
        //        //                                        string imageName = imageSource.Split('/')[totalCount - 1];
        //        //                                        string relativePath = "/Upload/" + currentQuestion.ID;
        //        //                                        string filePath = actualSource + "/" + imageName;
        //        //                                        string azureImageNewPath = AzureHelper.UploadToAzureStorage(filePath, relativePath, true);
        //        //                                        newImageNodes[0].Attribute("src").Value = azureImageNewPath;
        //        //                                        processing++;
        //        //                                        imageSourceStringList.Add(azureImageNewPath);
        //        //                                    }
        //        //                                }
        //        //                            }
        //        //                            foreach (string newImageSource in imageSourceStringList)
        //        //                            {
        //        //                                string searchString = guidString + "_files";
        //        //                                int place = questionString.IndexOf(searchString);

        //        //                                if (place > 0 && !string.IsNullOrEmpty(newImageSource))
        //        //                                {
        //        //                                    List<string> source = newImageSource.Split('/').ToList();
        //        //                                    source.RemoveAt(source.Count() - 1);
        //        //                                    string alteredImageSource = string.Join("/", source.ToArray());
        //        //                                    questionString = questionString.Remove(place, searchString.Length).Insert(place, alteredImageSource);
        //        //                                }
        //        //                            }
        //        //                            currentQuestion.Description = questionString;
        //        //                            db.Entry(currentQuestion).State = EntityState.Modified;
        //        //                            db.SaveChanges();

        //        //                            isContinueQuestion = false;
        //        //                            isNewPassage = false;
        //        //                            isContinueAnswer = true;
        //        //                            questionViewModel = MapperHelper.MapToViewModel(currentQuestion);
        //        //                            questionViewModelList.Add(questionViewModel);
        //        //                            //passageViewModel.QuestionViewModelList = questionViewModelList;
        //        //                            continue;
        //        //                        }
        //        //                        else if (isContinueAnswer)
        //        //                        {
        //        //                            if (string.IsNullOrEmpty(columns[0].Value.ToString()) || columns[0].Value.ToString().ToLower() == "c")
        //        //                            {                                                                                                   //Answer sections
        //        //                                Answer answer = new Answer();
        //        //                                string answerDescription = "";
        //        //                                foreach (var element in columns[1].Elements())
        //        //                                {
        //        //                                    answerDescription = answerDescription + element.ToString();
        //        //                                }
        //        //                                answer.Description = answerDescription;

        //        //                                if (columns[0].Value.ToString().ToLower() == "c")
        //        //                                {
        //        //                                    answer.IsCorrect = true;
        //        //                                }
        //        //                                else
        //        //                                    answer.IsCorrect = false;

        //        //                                answer.Question = currentQuestion;
        //        //                                db.Answer.Add(answer);
        //        //                                db.SaveChanges();

        //        //                                OAP.Domain.ViewModel.AnswerViewModel answerViewModel = new OAP.Domain.ViewModel.AnswerViewModel();
        //        //                                answerViewModel = MapperHelper.MapToViewModel(answer);
        //        //                                answerViewModelList.Add(answerViewModel);
        //        //                                continue;
        //        //                            }
        //        //                            else if (columns[0].Value.ToString().ToLower() == "s")
        //        //                            {                                                                   //Solution for the question
        //        //                                string solutionString = "";
        //        //                                foreach (var element in columns[1].Elements())
        //        //                                {
        //        //                                    solutionString = solutionString + element.ToString();
        //        //                                }
        //        //                                currentQuestion.Solution = solutionString;
        //        //                                db.Entry(currentQuestion).State = EntityState.Modified;
        //        //                                db.SaveChanges();
        //        //                                continue;
        //        //                            }
        //        //                            else if (columns[0].Value.ToString().ToLower() == "t")
        //        //                            {                                                                           //Column containing tags
        //        //                                if (!string.IsNullOrEmpty(columns[0].Value.ToString()))
        //        //                                {
        //        //                                    string tagString = columns[0].Value.ToString();
        //        //                                    List<string> tagList = tagString.Split(',').ToList();
        //        //                                    foreach (string newTag in tagList)
        //        //                                    {
        //        //                                        Tag tags = db.Tag.Where(t => t.Name.ToLower() == newTag.ToLower()).FirstOrDefault();
        //        //                                        if (tags == null)
        //        //                                        {
        //        //                                            tags = new Tag();
        //        //                                            tags.Name = newTag;
        //        //                                            db.Tag.Add(tags);
        //        //                                            db.SaveChanges();
        //        //                                        }
        //        //                                        if (tags != null)
        //        //                                        {
        //        //                                            QuestionTags questionTags = new QuestionTags();
        //        //                                            questionTags.Question = currentQuestion;
        //        //                                            questionTags.Tag = tags;
        //        //                                            db.QuestionTags.Add(questionTags);
        //        //                                        }
        //        //                                    }
        //        //                                    db.SaveChanges();
        //        //                                }
        //        //                                continue;
        //        //                            }
        //        //                            else
        //        //                            {
        //        //                                if (currentNode.Value.ToString().Contains("-"))
        //        //                                {                                                               //Having passage
        //        //                                    questionViewModel.AnswerViewModelList = answerViewModelList;
        //        //                                    questionViewModelList.Add(questionViewModel);
        //        //                                    passageViewModel.QuestionViewModelList = questionViewModelList;

        //        //                                    passageViewModel = new OAP.Domain.ViewModel.PassageViewModel();
        //        //                                    questionViewModel = new OAP.Domain.ViewModel.QuestionViewModel();
        //        //                                    questionViewModelList = new List<OAP.Domain.ViewModel.QuestionViewModel>();
        //        //                                    answerViewModelList = new List<OAP.Domain.ViewModel.AnswerViewModel>();
        //        //                                    passageStartIndex = Convert.ToInt16(currentNode.Value.Split('-')[0]);
        //        //                                    passageEndIndex = Convert.ToInt16(currentNode.Value.Split('-')[1]);
        //        //                                    string passageString = "";
        //        //                                    foreach (var element in columns[1].Elements())
        //        //                                    {
        //        //                                        passageString = passageString + element.ToString();
        //        //                                    }
        //        //                                    currentPassage = new Passage();
        //        //                                    currentPassage.Description = passageString;
        //        //                                    currentPassage.CanShuffle = true;
        //        //                                    db.Passage.Add(currentPassage);
        //        //                                    db.SaveChanges();
        //        //                                    isContinueQuestion = true;
        //        //                                    isNewPassage = false;
        //        //                                    isContinueAnswer = false;
        //        //                                    passageViewModel = MapperHelper.MapToViewModel(currentPassage);
        //        //                                    continue;
        //        //                                }
        //        //                                else
        //        //                                {
        //        //                                    passageViewModel = MapperHelper.MapToViewModel(currentPassage);
        //        //                                    passageViewModelList.Add(passageViewModel);
        //        //                                    isContinueQuestion = true;
        //        //                                    isNewPassage = false;
        //        //                                    isContinueAnswer = false;
        //        //                                    continue;
        //        //                                }
        //        //                            }
        //        //                        }
        //        //                    }
        //        //                }
        //        //            }
        //        //        }
        //        //    }
        //        //    return passageViewModelList;
        //        //}
        //    }
        //    return View();
        //}

        #endregion

        #region GetQuestionPaper by batch id

        public void GetQuestionPaperByBatch(string batchIDString)
        {
            List<OAP.Domain.ViewModel.QuestionPaperViewModel> assessmentViewModelList = new List<Domain.ViewModel.QuestionPaperViewModel>();
            if (!string.IsNullOrEmpty(batchIDString))
            {
                int batchID = Convert.ToInt16(batchIDString);
                IQueryable<QuestionPaper> questionpapers = db.QuestionPaper.Where(ass => ass.BatchAssessment.Where(ba => ba.BatchID == batchID).Count() > 0);
                assessmentViewModelList = MapperHelper.MapToViewModelList(questionpapers);
                assessmentViewModelList = assessmentViewModelList;
            }

            string result = JsonConvert.SerializeObject(assessmentViewModelList);
            Response.Write(result);

        }

        #endregion

        #region AcceptInstruction and Take Test
        //This function is called from jquery after accepting the instruction here we set the 

        public ActionResult TakeTest(int id = 0)
        {
            QuestionPaper questionPaper = db.QuestionPaper.Find(id);
            UserProfile userProfile = db.UserProfile.First(u => u.UserName.Equals(User.Identity.Name));
            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
            //var assessmentsIdList = userProfile.Group.GroupQuestionPaper.Where(ga => (!ga.StartDate.HasValue || (ga.StartDate.HasValue && ga.StartDate.Value <= indianTime)) && (!ga.StartDate.HasValue || (ga.EndDate.HasValue && ga.EndDate.Value >= indianTime))).Select(ga => ga.QuestionPaper.ID).ToList();

            OAP.Models.ViewModels.QuestionPaperViewModel questionPaperViewModel = GetQuestionPaperViewModel(questionPaper);
            

            if (questionPaper.QuestionPaperSection.Any(x => x.TechnicalQuestionPaperSectionQuestion.Count() > 0) && questionPaperViewModel != null)
            {
                foreach(var sectionViewModel in questionPaperViewModel.SectionList)
                {
                    if(sectionViewModel.Name == "Tech" && sectionViewModel.QuestionList.Count == 0)
                    {
                        var questionListModel = GetTechnicalQuestionList(questionPaper);
                        if(questionListModel != null)
                        {
                            sectionViewModel.QuestionList = questionListModel;
                        }
                    }
                }
            }

            var assessmentsIdList = userProfile.Student != null ? userProfile.Student.BatchStudent.SelectMany(ba => ba.Batch.BatchAssessment).Where(ga => (ga.StartDateTime <= indianTime) && ga.EndDateTime >= indianTime).Select(ga => ga.QuestionPaper.ID).Distinct().ToList() : new List<long>();

            assessmentsIdList.AddRange(userProfile.Student != null ? userProfile.Student.StudentAssessment.Where(ga => (ga.StartTime <= indianTime) && ga.EndTime >= indianTime).Select(ga => ga.QuestionPaper.ID).Distinct().ToList() : new List<long>());

            // assessmentsIdList.AddRange(db.FormQuestionPaper.Where(ga => (ga.StartDateTime <= indianTime) && ga.EndDateTime >= indianTime).Select(ga => ga.QuestionPaper.ID).Distinct().ToList());

            if (!assessmentsIdList.Contains(id))
            {
                questionPaperViewModel.NoPermission = true;
                return View(questionPaperViewModel);
            }
            if (questionPaperViewModel == null)
            {
                return HttpNotFound();
            }

            DateTime startTime = SetUserQuestionPaper(id, ref questionPaperViewModel);
            DateTime actualStartTime = questionPaper.UserAssessment.Where(x => x.UserID == userProfile.UserId).FirstOrDefault().StartTime.Value;

            questionPaperViewModel.StartTime = startTime;
            questionPaperViewModel.CurrentSection = questionPaperViewModel.SectionList[questionPaperViewModel.CurrentSectionIndex];
            questionPaperViewModel.CurrentSection.QuestionList = Shuffle(questionPaperViewModel.CurrentSection.QuestionList).OrderBy(q => q.Question.PassageID).ToList();
            //questionPaperViewModel.RemainingSeconds = (questionPaperViewModel.CurrentSection.TimeLimit * 60) - DateTime.Now.Subtract(startTime).TotalSeconds;

            if (questionPaperViewModel.TimeLimit != null && questionPaperViewModel.TimeLimit != 0)
            {                                                                                   //Time limit given for assessment
                // questionPaperViewModel.RemainingSeconds = (questionPaperViewModel.TimeLimit * 60) - DateTime.Now.Subtract(startTime).TotalSeconds;
                questionPaperViewModel.RemainingSeconds = ((questionPaperViewModel.CurrentSection.TimeLimit * 60)) - startTime.Subtract(actualStartTime).TotalSeconds;
            }
            else
            {                                                                                   //Time limt given for section
                //questionPaperViewModel.RemainingSeconds = ((questionPaperViewModel.CurrentSection.TimeLimit * 60)) - DateTime.Now.Subtract(startTime).TotalSeconds;
                questionPaperViewModel.RemainingSeconds = ((questionPaperViewModel.CurrentSection.TimeLimit * 60)) - startTime.Subtract(actualStartTime).TotalSeconds;
            }
            IQueryable<ColorCode> colorCode = db.ColorCode.OrderBy(x => x.Name);
            List<ColorCodeViewModel> colorCodeList = (from c in colorCode
                                                      select new ColorCodeViewModel()
                                                      {
                                                          ID = c.ID,
                                                          Name = c.Name,
                                                          HexCode = c.HexCode,
                                                      }).ToList();
            questionPaperViewModel.ColorCodeViewModelList = colorCodeList;

            ViewBag.Active = colorCodeList.Where(c => c.Name == "Active").Select(x => x.HexCode).FirstOrDefault();
            ViewBag.NotVisited = colorCodeList.Where(c => c.Name == "Not Visited").Select(x => x.HexCode).FirstOrDefault();
            ViewBag.VisitedUnanswer = colorCodeList.Where(c => c.Name == "Visited but Unanswer").Select(x => x.HexCode).FirstOrDefault();
            ViewBag.VisitedAnswered = colorCodeList.Where(c => c.Name == "Visited and Answered").Select(x => x.HexCode).FirstOrDefault();
            ViewBag.Marked = colorCodeList.Where(c => c.Name == "Marked").Select(x => x.HexCode).FirstOrDefault();
            return View("~/Views/QuestionPaper/Take.cshtml", questionPaperViewModel);
        }

        #endregion

        #region Upload Question for assessment

        public ActionResult UploadQuestion()
        {
            int uploadCount = db.Uploads.Count() + 1;
            OAP.Domain.ViewModel.QuestionPaperViewModel questionPaperViewModel = new Domain.ViewModel.QuestionPaperViewModel();
            List<QuestionPaper> assessmentList = db.QuestionPaper.ToList();
            ViewBag.QuestionPapers = new SelectList(assessmentList, "ID", "Name");
            return View(questionPaperViewModel);
        }

        [HttpPost]
        public ActionResult UploadQuestion(HttpPostedFileBase file, OAP.Domain.ViewModel.QuestionPaperViewModel questionPaperViewModel)
        {
            List<HtmlEntities> htmlEntities = GetHtmlEntities();
            string[] notSupportedTags = new string[] { "&rsquo;", " &ldquo;", "&rdquo;", "&lsquo;", " &ndash;", "  &bull;", " &frac;", " &deg;", "&frasl;", "&pi;", " &ang;", " &times;" };
            MashUploadResultViewModel uploadResult = new MashUploadResultViewModel();
            Uploads upload = new Uploads();
            upload.Title = questionPaperViewModel.UploadName ?? "Upload" + DateTime.Now;
            upload.UploadedDate = DateTime.Now;
            upload.UploadedUserID = GetCurrentUserID();
            db.Uploads.Add(upload);
            db.SaveChanges();
            if (file != null)
            {
                string[] fileExtensions = { "doc", "docx" };
                string selectedFileExtension = file.FileName.Split('.')[1];
                List<OAP.Domain.ViewModel.PassageViewModel> passageViewModelList = new List<OAP.Domain.ViewModel.PassageViewModel>();
                if (file != null && questionPaperViewModel.ID != 0 && fileExtensions.Contains(selectedFileExtension))
                {
                    QuestionPaper questionPaper = db.QuestionPaper.Find(questionPaperViewModel.ID);

                    string guidStirng = Guid.NewGuid().ToString();
                    //string newPath = Server.MapPath("~/Upload/" + guidStirng + "/" + guidStirng + ".html");
                    if (!Directory.Exists(Server.MapPath("~/Upload/" + guidStirng)))
                        Directory.CreateDirectory(Server.MapPath("~/Upload/" + guidStirng));
                    if (!Directory.Exists(Server.MapPath("~/Upload/" + guidStirng + "/Images")))
                        Directory.CreateDirectory(Server.MapPath("~/Upload/" + guidStirng + "/Images"));
                    string extension = file.FileName.Split('.')[1];
                    string path = Server.MapPath("~/Upload/" + guidStirng + "/" + guidStirng + "." + file.FileName.Split('.')[1]);
                    //string path = Server.MapPath("~/Upload/" + guidStirng  );

                    try
                    {
                        file.SaveAs(path);
                        HelperClasses.Converter converter = new Converter();
                        converter.ConvertDocxToHtml(path);
                    }
                    finally
                    {
                        //objWord.Application.Quit(SaveChanges: false);
                    }
                    HtmlFormatter htmlConverter = new HtmlFormatter();
                    string fileContent = "";
                    string htmlPath = Server.MapPath("~/Upload/" + guidStirng) + "/" + guidStirng + ".html";
                    fileContent = htmlConverter.ConvertHtmlToString(htmlPath);

                    for (int i = 0; i < htmlEntities.Count(); i++)
                    {
                        fileContent = fileContent.Replace(htmlEntities[i].HtmlCode, htmlEntities[i].XmlCode);
                    }
                    StringBuilder sb = new StringBuilder();
                    if (!string.IsNullOrEmpty(fileContent))
                    {
                        fileContent = htmlConverter.FormatFileContent(fileContent);
                    }
                    //fileContent = FormatFileContent(fileContent);
                    string oldPath = Server.MapPath("~/Upload/" + guidStirng);
                    string questionFilePath = Server.MapPath("~/Upload") + "/" + guidStirng + "/" + guidStirng + "." + file.FileName.Split('.')[1];


                    //string azureImageNewPath = AzureHelper.UploadToAzureStorage(questionFilePath, "OAP/Upload/Questions", false);
                    string azureImageNewPath = "";

                    //passageViewModelList = CreatePassageQuestionsAnswers(fileContent, guidStirng, oldPath, azureImageNewPath);

                    //Directory.Delete(deletePath);
                    //return View("UploadResult", passageViewModelList);
                    uploadResult = CreatePassageQuestionsAnswers(fileContent, guidStirng, oldPath, upload.ID, azureImageNewPath, upload, questionPaper);

                    string deletePath = Server.MapPath("~/Upload/" + guidStirng);
                    var dir = new DirectoryInfo(deletePath);
                    dir.Delete(true);

                    //return View("UploadResult", passageViewModelList);
                    return RedirectToAction("PreviewUploadedQuestions", new RouteValueDictionary(new { controller = "Upload", action = "PreviewUploadedQuestions", id = upload.ID }));
                    //return View(fileContent);
                }
                List<QuestionPaper> assessmentList = db.QuestionPaper.ToList();
                ViewBag.QuestionPapers = new SelectList(assessmentList, "ID", "Name");
                return View(questionPaperViewModel);
            }
            else
            {
                List<QuestionPaper> assessmentList = db.QuestionPaper.ToList();
                ViewBag.QuestionPapers = new SelectList(assessmentList, "ID", "Name");
                return View(questionPaperViewModel);

            }
        }
        public void UploadQuestionbyQuestionPaper()
        {
            var count = Request.Files.Count;

            long assessmentID = Convert.ToInt64(Request["assessmentId"]);
            QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentID);
            if (questionPaper != null)
            {
                OAP.Domain.ViewModel.QuestionPaperViewModel questionPaperViewModel = MapperHelper.MapToViewModel(questionPaper);
                List<HtmlEntities> htmlEntities = GetHtmlEntities();
                string[] notSupportedTags = new string[] { "&rsquo;", " &ldquo;", "&rdquo;", "&lsquo;", " &ndash;", "  &bull;", " &frac;", " &deg;", "&frasl;", "&pi;", " &ang;", " &times;" };
                MashUploadResultViewModel uploadResult = new MashUploadResultViewModel();
                Uploads upload = new Uploads();
                upload.Title = questionPaperViewModel.UploadName ?? "Upload" + DateTime.Now;
                upload.UploadedDate = DateTime.Now;
                upload.UploadedUserID = GetCurrentUserID();
                db.Uploads.Add(upload);
                db.SaveChanges();
                if (count > 0)
                {
                    var file = Request.Files["Questions"];

                    if (file != null)
                    {
                        string[] fileExtensions = { "doc", "docx" };
                        string selectedFileExtension = file.FileName.Split('.')[1];
                        List<OAP.Domain.ViewModel.PassageViewModel> passageViewModelList = new List<OAP.Domain.ViewModel.PassageViewModel>();
                        if (file != null && questionPaperViewModel.ID != 0 && fileExtensions.Contains(selectedFileExtension))
                        {


                            string guidStirng = Guid.NewGuid().ToString();
                            //string newPath = Server.MapPath("~/Upload/" + guidStirng + "/" + guidStirng + ".html");
                            if (!Directory.Exists(Server.MapPath("~/Upload/" + guidStirng)))
                                Directory.CreateDirectory(Server.MapPath("~/Upload/" + guidStirng));
                            if (!Directory.Exists(Server.MapPath("~/Upload/" + guidStirng + "/Images")))
                                Directory.CreateDirectory(Server.MapPath("~/Upload/" + guidStirng + "/Images"));
                            string extension = file.FileName.Split('.')[1];
                            string path = Server.MapPath("~/Upload/" + guidStirng + "/" + guidStirng + "." + file.FileName.Split('.')[1]);
                            //string path = Server.MapPath("~/Upload/" + guidStirng  );

                            try
                            {
                                file.SaveAs(path);
                                HelperClasses.Converter converter = new Converter();
                                converter.ConvertDocxToHtml(path);
                            }
                            finally
                            {
                                //objWord.Application.Quit(SaveChanges: false);
                            }
                            HtmlFormatter htmlConverter = new HtmlFormatter();
                            string fileContent = "";
                            string htmlPath = Server.MapPath("~/Upload/" + guidStirng) + "/" + guidStirng + ".html";
                            fileContent = htmlConverter.ConvertHtmlToString(htmlPath);

                            for (int i = 0; i < htmlEntities.Count(); i++)
                            {
                                fileContent = fileContent.Replace(htmlEntities[i].HtmlCode, htmlEntities[i].XmlCode);
                            }
                            StringBuilder sb = new StringBuilder();
                            if (!string.IsNullOrEmpty(fileContent))
                            {
                                fileContent = htmlConverter.FormatFileContent(fileContent);
                            }
                            //fileContent = FormatFileContent(fileContent);
                            string oldPath = Server.MapPath("~/Upload/" + guidStirng);
                            string questionFilePath = Server.MapPath("~/Upload") + "/" + guidStirng + "/" + guidStirng + "." + file.FileName.Split('.')[1];

                            //string azureImageNewPath = AzureHelper.UploadToAzureStorage(questionFilePath, "OAP/Upload/Questions", false);
                            string azureImageNewPath = "";

                            //passageViewModelList = CreatePassageQuestionsAnswers(fileContent, guidStirng, oldPath, azureImageNewPath);

                            //Directory.Delete(deletePath);
                            //return View("UploadResult", passageViewModelList);
                            uploadResult = CreatePassageQuestionsAnswers(fileContent, guidStirng, oldPath, upload.ID, azureImageNewPath, upload, questionPaper);

                            string deletePath = Server.MapPath("~/Upload/" + guidStirng);
                            var dir = new DirectoryInfo(deletePath);
                            dir.Delete(true);
                            Response.Write("Successfully Uploaded");
                            //return View("UploadResult", passageViewModelList);
                            // return RedirectToAction("PreviewUploadedQuestions", new RouteValueDictionary(new { controller = "Upload", action = "PreviewUploadedQuestions", id = upload.ID }));
                            //return View(fileContent);
                        }
                        //ViewBag.QuestionPapers = new SelectList(db.QuestionPaper, "ID", "Name");
                        //Response.Write("Please try again");

                    }
                    else
                    {
                        List<QuestionPaper> assessmentList = db.QuestionPaper.ToList();
                        ViewBag.QuestionPapers = new SelectList(assessmentList, "ID", "Name");
                        Response.Write("Please try again");

                    }
                }
                else
                {
                    List<QuestionPaper> assessmentList = db.QuestionPaper.ToList();
                    ViewBag.QuestionPapers = new SelectList(assessmentList, "ID", "Name");
                    Response.Write("Please try again");

                }
            }
            else
            {
                List<QuestionPaper> assessmentList = db.QuestionPaper.ToList();
                ViewBag.QuestionPapers = new SelectList(assessmentList, "ID", "Name");
                Response.Write("Please try again");

            }
        }
        public MashUploadResultViewModel CreatePassageQuestionsAnswers(string fileContent, string guidString, string oldPath, long uploadedID, string questionAzurePath, Uploads upload, QuestionPaper questionPaper)
        {
            List<HtmlEntities> htmlEntities = GetHtmlEntities();
            MashUploadResultViewModel uploadResult = new MashUploadResultViewModel();
            int processPassage = 0;
            bool isContinueQuestion = false;
            bool isContinueAnswer = false;
            bool isNewPassage = true;

            Passage currentPassage = new Passage();
            Question currentQuestion = new Question();
            List<Answer> answers = new List<Answer>();
            StringBuilder sb = new StringBuilder();

            List<OAP.Domain.ViewModel.PassageViewModel> passageViewModelList = new List<OAP.Domain.ViewModel.PassageViewModel>();
            List<OAP.Domain.ViewModel.QuestionViewModel> questionViewModelList = new List<OAP.Domain.ViewModel.QuestionViewModel>();
            OAP.Domain.ViewModel.PassageViewModel passageViewModel = new OAP.Domain.ViewModel.PassageViewModel();
            OAP.Domain.ViewModel.QuestionViewModel questionViewModel = new OAP.Domain.ViewModel.QuestionViewModel();
            List<OAP.Domain.ViewModel.AnswerViewModel> answerViewModelList = new List<OAP.Domain.ViewModel.AnswerViewModel>();

            int passageStartIndex = 0, passageEndIndex = 0;

            if (!string.IsNullOrEmpty(fileContent))
            {
                XDocument document = XDocument.Parse(fileContent);
                for (int i = 0; i < htmlEntities.Count(); i++)
                {
                    fileContent = fileContent.Replace(htmlEntities[i].XmlCode, htmlEntities[i].HtmlCode);
                }
                XElement root = document.Elements().FirstOrDefault().Elements().FirstOrDefault();
                root.Elements().FirstOrDefault().Remove();
                {
                    int totalQuestions = root.Elements().Where(e => !(e.Elements().First().Value.Contains("-"))).Count() / 2;
                    foreach (XElement tr in root.Elements().FirstOrDefault().Elements().FirstOrDefault().Elements())
                    {
                        if (tr.Value.Trim() == "")
                        {
                            continue;
                        }
                        if (tr.Name.LocalName.ToLower() == "tr")
                        {
                            answers = new List<Answer>();
                            List<XElement> columns = tr.Elements().ToList();
                            int count = columns.Count;
                            var node = (columns[0].Elements().ToList()[0].FirstNode);

                            if (processPassage == 0)
                            {                               //Processing first line
                                                            //var currentNode = (XElement)node;

                                //if (!string.IsNullOrEmpty(currentNode.Value.ToString()))
                                //{
                                ContinuePassage:
                                if (isNewPassage)
                                {                                                                   //Questions and answers saved
                                    if (columns[0].Value.ToString().Contains("-"))
                                    {                                                               //Having passage
                                        passageViewModel = new OAP.Domain.ViewModel.PassageViewModel();
                                        passageStartIndex = Convert.ToInt16(columns[0].Value.Split('-')[0]);
                                        passageEndIndex = Convert.ToInt16(columns[0].Value.Split('-')[1]);

                                        currentPassage = SavePassage(columns, guidString);

                                        isContinueQuestion = true;
                                        isNewPassage = false;
                                        isContinueAnswer = false;
                                        passageViewModel = MapperHelper.MapToViewModel(currentPassage);
                                        continue;
                                    }
                                    else
                                    {
                                        isContinueQuestion = true;
                                        isNewPassage = false;
                                        isContinueAnswer = false;
                                        goto ContinueQuestion;
                                    }
                                }
                                ContinueQuestion:
                                if (isContinueQuestion)
                                {                                                             //Continue to save Question
                                    questionViewModel = new OAP.Domain.ViewModel.QuestionViewModel();

                                    currentQuestion = SaveQuestion(columns, currentPassage, upload, questionAzurePath, guidString, oldPath, passageStartIndex, passageEndIndex);
                                    questionViewModel = MapperHelper.MapToViewModel(currentQuestion);
                                    questionViewModelList.Add(questionViewModel);

                                    if (questionPaper != null)
                                    {
                                        QuestionPaperQuestion questionPaperQuestion = new QuestionPaperQuestion();
                                        questionPaperQuestion.QuestionPaper = questionPaper;
                                        questionPaperQuestion.Question = currentQuestion;
                                        db.QuestionPaperQuestion.Add(questionPaperQuestion);
                                        db.SaveChanges();
                                    }

                                    isContinueQuestion = false;
                                    isNewPassage = false;
                                    isContinueAnswer = true;
                                    continue;
                                }
                                if (isContinueAnswer)
                                {
                                    if (string.IsNullOrEmpty(columns[0].Value.ToString()) || columns[0].Value.Trim().ToString().ToLower() == "c")
                                    {                                                                                                   //Answer sections
                                        var reader = columns[1].CreateReader();
                                        reader.MoveToContent();
                                        string answerDescription = reader.ReadInnerXml();

                                        QuestionType questionType;
                                        if (string.IsNullOrEmpty(answerDescription))
                                        {                                       //Question has no answers . Question type is OpenEndedQuestion
                                            questionType = db.QuestionType.Where(qt => qt.Name.ToLower().Contains("open ended")).FirstOrDefault();
                                            currentQuestion.QuestionType = questionType;
                                            db.SaveChanges();
                                            continue;
                                        }
                                        else
                                        {                                       //Question have answers. 
                                            OAP.Domain.ViewModel.AnswerViewModel answerViewModel = new OAP.Domain.ViewModel.AnswerViewModel();
                                            answerViewModel = AddAnswerForQuestion(columns, currentQuestion);
                                            answerViewModelList.Add(answerViewModel);
                                            continue;
                                        }
                                    }
                                    else if (columns[0].Value.Trim().ToString().ToLower() == "s")
                                    {                                                                   //Solution for the question
                                        AddSolutionForQuestion(columns, currentQuestion);
                                        continue;
                                    }
                                    else if (columns[0].Value.Trim().ToString().ToLower() == "t")
                                    {                                                                           //Column containing tags
                                        CreateQuestionTags(columns, currentQuestion);
                                        continue;
                                    }
                                    else
                                    {                                                                       //Save answer completed. Continue to New passage or new question
                                        if (currentQuestion.ID != 0 && currentQuestion.QuestionType == null)
                                            UpdateQuestionType(currentQuestion);

                                        if (columns[0].Value.ToString().Contains("-"))
                                        {                                                               //Having passage

                                            isContinueQuestion = false;
                                            isNewPassage = true;
                                            isContinueAnswer = false;
                                            goto ContinuePassage;
                                        }
                                        else
                                        {
                                            passageViewModel = MapperHelper.MapToViewModel(currentPassage);
                                            passageViewModelList.Add(passageViewModel);
                                            isContinueQuestion = true;
                                            isNewPassage = false;
                                            isContinueAnswer = false;
                                            goto ContinueQuestion;
                                            //continue;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            UpdateQuestionType(currentQuestion);

            string questionIDs = "";
            foreach (OAP.Domain.ViewModel.QuestionViewModel question in questionViewModelList)
            {
                question.AnswerViewModelList = answerViewModelList.Where(a => a.QuestionID == question.ID).ToList();
                questionIDs += question.ID + ",";
            }
            uploadResult.questionViewModelList = questionViewModelList;
            uploadResult.UploadedQuestionIDs = questionIDs;
            return uploadResult;
            //return passageViewModelList;
        }

        public List<HtmlEntities> GetHtmlEntities()
        {
            List<HtmlEntities> htmlEntities = new List<HtmlEntities>();
            htmlEntities.Add(new HtmlEntities("&rsquo;", "&#8217;"));
            htmlEntities.Add(new HtmlEntities("&ldquo;", "&#8220;"));
            htmlEntities.Add(new HtmlEntities("&rdquo;", "&#8221;"));
            htmlEntities.Add(new HtmlEntities("&lsquo;", "&#8216;"));
            htmlEntities.Add(new HtmlEntities("&ndash;", "&#8211;"));
            htmlEntities.Add(new HtmlEntities("&bull;", "&#8226;"));
            // htmlEntities.Add(new HtmlEntities("&frac;", ""));
            htmlEntities.Add(new HtmlEntities("&deg;", "&#176;"));
            htmlEntities.Add(new HtmlEntities("&frasl;", "&#8260;"));
            htmlEntities.Add(new HtmlEntities("&pi;", "&#928;"));
            htmlEntities.Add(new HtmlEntities("&ang;", "&#8736;"));
            htmlEntities.Add(new HtmlEntities("&times;", "&#215;"));
            htmlEntities.Add(new HtmlEntities("&hellip", "&#8230;"));
            htmlEntities.Add(new HtmlEntities("&pound", "&#1234;"));
            return htmlEntities;

        }

        public Passage SavePassage(List<XElement> columns, string guidString)
        {
            var reader = columns[1].CreateReader();
            reader.MoveToContent();
            string passageString = reader.ReadInnerXml();

            Passage currentPassage = new Passage();
            currentPassage.CanShuffle = true;
            currentPassage.CreatedDate = DateTime.Now;
            currentPassage.CreatedBy = currentPassage.ModifiedBy = GetCurrentUserID();
            currentPassage.ModifiedDate = DateTime.Now;
            currentPassage.LastUpdated = DateTime.Now;

            db.Passage.Add(currentPassage);
            db.SaveChanges();

            List<string> imageSourceStringList = new List<string>();
            foreach (var element in columns[1].Elements())
            {
                if (element.ToString().Contains("img"))
                {                                                                                       //Question having image
                    var imageNodes = columns[1].Descendants("img");
                    var newImageNodes = columns[1].Descendants("img").ToList();
                    string actualSource = Server.MapPath("~/Upload/") + guidString + "/" + guidString + "_files";
                    int processing = 0;
                    foreach (var item in imageNodes)
                    {
                        string imageSource = item.Attribute("src").Value;
                        int totalCount = imageSource.Split('/').Count();
                        string imageName = imageSource.Split('/')[totalCount - 1];
                        string destinationPath = "OAP/Upload/Passage" + currentPassage.ID;
                        string filePath = actualSource + "/" + imageName;
                        //string azureImageNewPath = AzureHelper.UploadToAzureStorage(questionFilePath, "OAP/Upload/Questions", false);
                        string azureImageNewPath = "";
                        newImageNodes[0].Attribute("src").Value = azureImageNewPath;
                        processing++;
                        imageSourceStringList.Add(azureImageNewPath);
                    }
                }
            }
            foreach (string newImageSource in imageSourceStringList)
            {
                string searchString = guidString + "_files";
                int place = passageString.IndexOf(searchString);

                if (place > 0 && !string.IsNullOrEmpty(newImageSource))
                {
                    List<string> source = newImageSource.Split('/').ToList();
                    source.RemoveAt(source.Count() - 1);
                    string alteredImageSource = string.Join("/", source.ToArray());
                    passageString = passageString.Remove(place, searchString.Length).Insert(place, alteredImageSource);
                }
            }
            currentPassage.Description = passageString;
            // currentPassage.Description = htmlConverter.FormatFileContent(currentPassage.Description);
            currentPassage.Description = currentPassage.Description;


            db.Entry(currentPassage).State = EntityState.Modified;
            db.SaveChanges();
            return currentPassage;
        }

        public Question SaveQuestion(List<XElement> columns, Passage currentPassage, Uploads upload, string questionAzurePath, string guidString, string oldPath, int passageStartIndex, int passageEndIndex)
        {
            Question currentQuestion;

            //int questionNumber = Convert.ToInt16(columns[0].Value.ToString());
            int questionNumber;
            Int32.TryParse(columns[0].Value.ToString(), out questionNumber);
            //int questionNumber = Convert.ToInt32(Convert.ToDouble(columns[0].Value.ToString()));
            string questionString = "";
            //foreach (var element in columns[1].Elements())
            //{
            //    questionString = questionString + element.ToString();
            //}
            var reader = columns[1].CreateReader();
            reader.MoveToContent();
            questionString = reader.ReadInnerXml();

            currentQuestion = new Question();
            if (questionNumber >= passageStartIndex && questionNumber <= passageEndIndex)
                currentQuestion.Passage = currentPassage;
            currentQuestion.Description = questionString;
            //currentQuestion.Uploads = db.Uploads.Find(uploadedID);
            currentQuestion.Uploads = upload;
            currentQuestion.LastUpdated = DateTime.Now;
            currentQuestion.AdminId = GetCurrentUserID();
            currentQuestion.DownloadLink = questionAzurePath;
            currentQuestion.AdminId = GetCurrentUserID();
            db.Question.Add(currentQuestion);
            db.SaveChanges();

            List<string> imageSourceStringList = new List<string>();
            foreach (var element in columns[1].Elements())
            {
                if (element.ToString().Contains("img"))
                {                                                                                       //Question having image
                    var imageNodes = columns[1].Elements().FirstOrDefault().Elements().ToList();
                    string actualSource = Server.MapPath("~/Upload/") + guidString + "/" + guidString + "_files";
                    int processing = 0;
                    var tempElement = imageNodes;
                    int i = 0;
                    foreach (var item in imageNodes)
                    {
                        if (item.ToString().Contains("img"))
                        {
                            string imageSource = item.Attribute("src").Value;
                            int totalCount = imageSource.Split('/').Count();
                            string imageName = imageSource.Split('/')[totalCount - 1];
                            string destinationPath = "/Upload/Question/Images/" + currentQuestion.ID;
                            string filePath = actualSource + "/" + imageName;
                            // string azureImageNewPath = AzureHelper.UploadToAzureStorage(filePath, destinationPath, true);
                            processing++;
                            imageSourceStringList.Add("");
                            i++;
                        }
                    }
                }
            }
            foreach (string newImageSource in imageSourceStringList)
            {
                //oldPath
                //string searchString = guidString + "_files";
                string searchString = oldPath + @"\" + guidString + "_files";
                searchString = searchString.Replace(@"\\", @"\");
                int place = questionString.IndexOf(searchString);

                if (place > 0 && !string.IsNullOrEmpty(newImageSource))
                {
                    List<string> source = newImageSource.Split('/').ToList();
                    source.RemoveAt(source.Count() - 1);
                    string alteredImageSource = string.Join("/", source.ToArray());

                    questionString = questionString.Remove(place, searchString.Length).Insert(place, alteredImageSource);
                }
            }
            currentQuestion.Description = questionString;
            //currentQuestion.Uploads = db.Uploads.Find(uploadedID);
            currentQuestion.LastUpdated = DateTime.Now;
            currentQuestion.AdminId = GetCurrentUserID();
            // currentQuestion.Description = htmlConverter.FormatFileContent(currentQuestion.Description);
            currentQuestion.Description = currentQuestion.Description;


            db.Entry(currentQuestion).State = EntityState.Modified;
            db.SaveChanges();



            return currentQuestion;
        }

        public OAP.Domain.ViewModel.AnswerViewModel AddAnswerForQuestion(List<XElement> columns, Question currentQuestion)
        {
            Answer answer = new Answer();
            string answerDescription = "";

            var reader = columns[1].CreateReader();
            reader.MoveToContent();
            answerDescription = reader.ReadInnerXml();

            answer.Description = answerDescription;

            if (columns[0].Value.ToString().ToLower() == "c")
            {
                answer.IsCorrect = true;
            }
            else
                answer.IsCorrect = false;
            answer.Question = currentQuestion;
            answer.LastUpdated = DateTime.Now;
            // answer.Description = htmlConverter.FormatFileContent(answer.Description);
            answer.Description = answer.Description;

            db.Answer.Add(answer);
            db.SaveChanges();

            OAP.Domain.ViewModel.AnswerViewModel answerViewModel = new OAP.Domain.ViewModel.AnswerViewModel();
            answerViewModel = MapperHelper.MapToViewModel(answer);
            return answerViewModel;
        }

        public void AddSolutionForQuestion(List<XElement> columns, Question currentQuestion)
        {
            if (!String.IsNullOrEmpty(columns[1].Value.ToString()))
            {
                string solutionString = "";
                //foreach (var element in columns[1].Elements())
                //{
                //    solutionString = solutionString + element.ToString();
                //}

                var reader = columns[1].CreateReader();
                reader.MoveToContent();
                solutionString = reader.ReadInnerXml();
                currentQuestion.Solution = solutionString;
                currentQuestion.AdminId = GetCurrentUserID();
                currentQuestion.LastUpdated = DateTime.Now;
                // currentQuestion.Description = htmlConverter.FormatFileContent(currentQuestion.Description);
                currentQuestion.Description = currentQuestion.Description;


                db.Entry(currentQuestion).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void CreateQuestionTags(List<XElement> columns, Question currentQuestion)
        {
            if (!string.IsNullOrEmpty(columns[1].Value.ToString()))
            {
                string tagString = columns[1].Value.ToString();
                List<string> tagList = tagString.Split(',').ToList();
                foreach (string newTag in tagList)
                {
                    OAP.Domain.Tag tags = db.Tag.Where(t => t.Name.ToLower() == newTag.ToLower()).FirstOrDefault();
                    if (tags == null)
                    {
                        tags = new OAP.Domain.Tag();
                        tags.Name = newTag;
                        db.Tag.Add(tags);
                        db.SaveChanges();
                    }
                    if (tags != null)
                    {
                        QuestionTags questionTags = new QuestionTags();
                        questionTags.Question = currentQuestion;
                        questionTags.Tag = tags;
                        questionTags.CreatedBy = GetCurrentUserID();
                        questionTags.CreatedDate = DateTime.Now;
                        db.QuestionTags.Add(questionTags);
                    }
                }
                db.SaveChanges();
            }
        }

        public void UpdateQuestionType(Question currentQuestion)
        {
            QuestionType questionType;
            if (currentQuestion.Answer.Where(a => a.IsCorrect ?? false).Count() > 1)
            {                                                                           //Question have multiple answers
                questionType = db.QuestionType.Where(qt => qt.Name.ToLower().Contains("multi select")).FirstOrDefault();
            }
            else
            {                                                                           //Qustion have only one answer
                questionType = db.QuestionType.Where(qt => qt.Name.ToLower().Contains("single select")).FirstOrDefault();

            }
            if (questionType != null)
            {
                currentQuestion.QuestionType = questionType;
                //currentQuestion.Description = htmlConverter.FormatFileContent(currentQuestion.Description);
                currentQuestion.Description = currentQuestion.Description;

                db.Entry(currentQuestion).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        #region Old

        //public MashUploadResultViewModel CreatePassageQuestionsAnswers(string fileContent, string guidString, string oldPath, long uploadedID, string questionAzurePath, QuestionPaper assessment)
        //{
        //    List<HtmlEntities> htmlEntities = GetHtmlEntities();
        //    MashUploadResultViewModel uploadResult = new MashUploadResultViewModel();
        //    int processPassage = 0;
        //    bool isContinueQuestion = false;
        //    bool isContinueAnswer = false;
        //    bool isNewPassage = true;

        //    Passage currentPassage = new Passage();
        //    Question currentQuestion = new Question();
        //    List<Answer> answers = new List<Answer>();
        //    StringBuilder sb = new StringBuilder();

        //    List<OAP.Domain.ViewModel.PassageViewModel> passageViewModelList = new List<OAP.Domain.ViewModel.PassageViewModel>();
        //    List<OAP.Domain.ViewModel.QuestionViewModel> questionViewModelList = new List<OAP.Domain.ViewModel.QuestionViewModel>();
        //    OAP.Domain.ViewModel.PassageViewModel passageViewModel = new OAP.Domain.ViewModel.PassageViewModel();
        //    OAP.Domain.ViewModel.QuestionViewModel questionViewModel = new OAP.Domain.ViewModel.QuestionViewModel();
        //    List<OAP.Domain.ViewModel.AnswerViewModel> answerViewModelList = new List<OAP.Domain.ViewModel.AnswerViewModel>();
        //    int passageStartIndex = 0, passageEndIndex = 0;
        //    if (!string.IsNullOrEmpty(fileContent))
        //    {
        //        XDocument document = XDocument.Parse(fileContent);
        //        for (int i = 0; i < htmlEntities.Count(); i++)
        //        {
        //            fileContent = fileContent.Replace(htmlEntities[i].XmlCode, htmlEntities[i].HtmlCode);
        //        }
        //        XElement root = document.Elements().FirstOrDefault().Elements().FirstOrDefault();
        //        root.Elements().FirstOrDefault().Remove();
        //        {
        //            int totalQuestions = root.Elements().Where(e => !(e.Elements().First().Value.Contains("-"))).Count() / 2;
        //            foreach (XElement tr in root.Elements().FirstOrDefault().Elements().FirstOrDefault().Elements())
        //            {
        //                if (tr.Value.Trim() == "")
        //                {
        //                    continue;
        //                }
        //                //if (tr.Name == "tr")
        //                if (tr.Name.LocalName.ToLower() == "tr")
        //                {
        //                    answers = new List<Answer>();
        //                    List<XElement> columns = tr.Elements().ToList();
        //                    int count = columns.Count;
        //                    var node = (columns[0].Elements().ToList()[0].FirstNode);

        //                    if (processPassage == 0)
        //                    {                               //Processing first line
        //                    ContinuePassage:
        //                        if (isNewPassage)
        //                        {                                                                   //Questions and answers saved
        //                            if (columns[0].Value.ToString().Contains("-"))
        //                            {                                                               //Having passage
        //                                passageViewModel = new OAP.Domain.ViewModel.PassageViewModel();
        //                                passageStartIndex = Convert.ToInt16(columns[0].Value.Split('-')[0]);
        //                                passageEndIndex = Convert.ToInt16(columns[0].Value.Split('-')[1]);
        //                                string passageString = "";
        //                                foreach (var element in columns[1].Elements())
        //                                {
        //                                    passageString = passageString + element.ToString();
        //                                }
        //                                currentPassage = new Passage();
        //                                currentPassage.CanShuffle = true;
        //                                currentPassage.CreatedDate = DateTime.Now;
        //                                currentPassage.CreatedBy = currentPassage.ModifiedBy = GetCurrentUserID();
        //                                currentPassage.ModifiedDate = DateTime.Now;
        //                                currentPassage.LastUpdated = DateTime.Now;
        //                                currentQuestion.AdminId = GetCurrentUserID();
        //                                List<string> imageSourceStringList = new List<string>();
        //                                foreach (var element in columns[1].Elements())
        //                                {
        //                                    if (element.ToString().Contains("img"))
        //                                    {                                                                                       //Question having image
        //                                        var imageNodes = columns[1].Descendants("img");
        //                                        var newImageNodes = columns[1].Descendants("img").ToList();
        //                                        string actualSource = Server.MapPath("~/Upload/") + guidString + "/" + guidString + "_files";
        //                                        int processing = 0;
        //                                        foreach (var item in imageNodes)
        //                                        {
        //                                            string imageSource = item.Attribute("src").Value;
        //                                            int totalCount = imageSource.Split('/').Count();
        //                                            string imageName = imageSource.Split('/')[totalCount - 1];
        //                                            string relativePath = "OAP/Upload/" + currentQuestion.ID;
        //                                            string filePath = actualSource + "/" + imageName;
        //                                            string azureImageNewPath = AzureHelper.UploadToAzureStorage(filePath, relativePath, true);
        //                                            newImageNodes[0].Attribute("src").Value = azureImageNewPath;
        //                                            processing++;
        //                                            imageSourceStringList.Add(azureImageNewPath);
        //                                        }
        //                                    }
        //                                }
        //                                foreach (string newImageSource in imageSourceStringList)
        //                                {
        //                                    string searchString = guidString + "_files";
        //                                    int place = passageString.IndexOf(searchString);

        //                                    if (place > 0 && !string.IsNullOrEmpty(newImageSource))
        //                                    {
        //                                        List<string> source = newImageSource.Split('/').ToList();
        //                                        source.RemoveAt(source.Count() - 1);
        //                                        string alteredImageSource = string.Join("/", source.ToArray());
        //                                        passageString = passageString.Remove(place, searchString.Length).Insert(place, alteredImageSource);
        //                                    }
        //                                }
        //                                currentPassage.Description = passageString;

        //                                db.Passage.Add(currentPassage);
        //                                db.SaveChanges();
        //                                isContinueQuestion = true;
        //                                isNewPassage = false;
        //                                isContinueAnswer = false;
        //                                passageViewModel = MapperHelper.MapToViewModel(currentPassage);
        //                                continue;
        //                            }
        //                            else
        //                            {
        //                                isContinueQuestion = true;
        //                                isNewPassage = false;
        //                                isContinueAnswer = false;
        //                                goto ContinueQuestion;
        //                                //continue;
        //                            }
        //                        }
        //                    ContinueQuestion:
        //                        if (isContinueQuestion)
        //                        {                                                             //Continue to save Question
        //                            questionViewModel = new OAP.Domain.ViewModel.QuestionViewModel();
        //                            int questionNumber = Convert.ToInt16(columns[0].Value.ToString());
        //                            string questionString = "";
        //                            foreach (var element in columns[1].Elements())
        //                            {
        //                                questionString = questionString + element.ToString();
        //                            }
        //                            currentQuestion = new Question();
        //                            if (questionNumber >= passageStartIndex && questionNumber <= passageEndIndex)
        //                                currentQuestion.Passage = currentPassage;
        //                            currentQuestion.Description = questionString;
        //                            currentQuestion.Uploads = db.Uploads.Find(uploadedID);
        //                            currentQuestion.LastUpdated = DateTime.Now;
        //                            currentQuestion.AdminId = GetCurrentUserID();
        //                            currentQuestion.DownloadLink = questionAzurePath;
        //                            db.Question.Add(currentQuestion);
        //                            db.SaveChanges();

        //                            if (assessment != null)
        //                            {
        //                                QuestionPaperQuestion assessmentQuestion = new QuestionPaperQuestion();
        //                                assessmentQuestion.QuestionPaper = assessment;
        //                                assessmentQuestion.Question = currentQuestion;
        //                                db.QuestionPaperQuestion.Add(assessmentQuestion);
        //                                db.SaveChanges();
        //                            }

        //                            List<string> imageSourceStringList = new List<string>();
        //                            foreach (var element in columns[1].Elements())
        //                            {
        //                                if (element.ToString().Contains("img"))
        //                                {                                                                                       //Question having image
        //                                    var imageNodes = columns[1].Elements().FirstOrDefault().Elements().ToList();
        //                                    string actualSource = Server.MapPath("~/Upload/") + guidString + "/" + guidString + "_files";
        //                                    int processing = 0;
        //                                    var tempElement = imageNodes;
        //                                    int i = 0;
        //                                    foreach (var item in imageNodes)
        //                                    {
        //                                        if (item.ToString().Contains("img"))
        //                                        {
        //                                            string imageSource = item.Attribute("src").Value;
        //                                            int totalCount = imageSource.Split('/').Count();
        //                                            string imageName = imageSource.Split('/')[totalCount - 1];
        //                                            string relativePath = "/Upload/" + currentQuestion.ID;
        //                                            string filePath = actualSource + "/" + imageName;
        //                                            string azureImageNewPath = AzureHelper.UploadToAzureStorage(filePath, relativePath, true);
        //                                            processing++;
        //                                            imageSourceStringList.Add(azureImageNewPath);
        //                                            i++;
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                            foreach (string newImageSource in imageSourceStringList)
        //                            {
        //                                //oldPath
        //                                //string searchString = guidString + "_files";
        //                                string searchString = oldPath + @"\" + guidString + "_files";
        //                                searchString = searchString.Replace(@"\\", @"\");
        //                                int place = questionString.IndexOf(searchString);

        //                                if (place > 0 && !string.IsNullOrEmpty(newImageSource))
        //                                {
        //                                    List<string> source = newImageSource.Split('/').ToList();
        //                                    source.RemoveAt(source.Count() - 1);
        //                                    string alteredImageSource = string.Join("/", source.ToArray());

        //                                    questionString = questionString.Remove(place, searchString.Length).Insert(place, alteredImageSource);
        //                                }
        //                            }
        //                            currentQuestion.Description = questionString;
        //                            currentQuestion.Uploads = db.Uploads.Find(uploadedID);
        //                            currentQuestion.LastUpdated = DateTime.Now;
        //                            currentQuestion.AdminId = GetCurrentUserID();
        //                            db.Entry(currentQuestion).State = EntityState.Modified;
        //                            db.SaveChanges();

        //                            isContinueQuestion = false;
        //                            isNewPassage = false;
        //                            isContinueAnswer = true;
        //                            questionViewModel = MapperHelper.MapToViewModel(currentQuestion);
        //                            questionViewModelList.Add(questionViewModel);
        //                            //passageViewModel.QuestionViewModelList = questionViewModelList;
        //                            continue;
        //                        }
        //                        if (isContinueAnswer)
        //                        {
        //                            if (string.IsNullOrEmpty(columns[0].Value.ToString()) || columns[0].Value.ToString().ToLower() == "c")
        //                            {                                                                                                   //Answer sections
        //                                Answer answer = new Answer();
        //                                string answerDescription = "";
        //                                foreach (var element in columns[1].Elements())
        //                                {
        //                                    answerDescription = answerDescription + element.ToString();
        //                                }
        //                                answer.Description = answerDescription;
        //                                //answer.Description = columns[1].Elements().ToString();
        //                                if (columns[0].Value.ToString().ToLower() == "c")
        //                                {
        //                                    answer.IsCorrect = true;
        //                                }
        //                                else
        //                                    answer.IsCorrect = false;
        //                                answer.Question = currentQuestion;
        //                                answer.LastUpdated = DateTime.Now;
        //                                db.Answer.Add(answer);
        //                                db.SaveChanges();
        //                                OAP.Domain.ViewModel.AnswerViewModel answerViewModel = new OAP.Domain.ViewModel.AnswerViewModel();
        //                                answerViewModel = MapperHelper.MapToViewModel(answer);
        //                                answerViewModelList.Add(answerViewModel);
        //                                continue;
        //                            }
        //                            else if (columns[0].Value.ToString().ToLower() == "s")
        //                            {                                                                   //Solution for the question
        //                                if (!String.IsNullOrEmpty(columns[1].Value.ToString()))
        //                                {
        //                                    string solutionString = "";
        //                                    foreach (var element in columns[1].Elements())
        //                                    {
        //                                        solutionString = solutionString + element.ToString();
        //                                    }
        //                                    currentQuestion.Solution = solutionString;
        //                                    currentQuestion.Uploads = db.Uploads.Find(uploadedID);
        //                                    currentQuestion.AdminId = GetCurrentUserID();
        //                                    currentQuestion.LastUpdated = DateTime.Now;
        //                                    db.Entry(currentQuestion).State = EntityState.Modified;
        //                                    db.SaveChanges();
        //                                }
        //                                continue;
        //                            }
        //                            else if (columns[0].Value.ToString().ToLower() == "t")
        //                            {                                                                           //Column containing tags
        //                                if (!string.IsNullOrEmpty(columns[1].Value.ToString()))
        //                                {
        //                                    string tagString = columns[1].Value.ToString();
        //                                    List<string> tagList = tagString.Split(',').ToList();
        //                                    foreach (string newTag in tagList)
        //                                    {
        //                                        Tag tags = db.Tag.Where(t => t.Name.ToLower() == newTag.ToLower()).FirstOrDefault();
        //                                        if (tags == null)
        //                                        {
        //                                            tags = new Tag();
        //                                            tags.Name = newTag;
        //                                            db.Tag.Add(tags);
        //                                            db.SaveChanges();
        //                                        }
        //                                        if (tags != null)
        //                                        {
        //                                            QuestionTags questionTags = new QuestionTags();
        //                                            questionTags.Question = currentQuestion;
        //                                            questionTags.Tag = tags;
        //                                            questionTags.CreatedBy = GetCurrentUserID();
        //                                            questionTags.CreatedDate = DateTime.Now;
        //                                            db.QuestionTags.Add(questionTags);
        //                                        }
        //                                    }
        //                                    db.SaveChanges();
        //                                }
        //                                continue;
        //                            }
        //                            else
        //                            {
        //                                if (columns[0].Value.ToString().Contains("-"))
        //                                {                                                               //Having passage
        //                                    isContinueQuestion = false;
        //                                    isNewPassage = true;
        //                                    isContinueAnswer = false;
        //                                    goto ContinuePassage;
        //                                }
        //                                else
        //                                {
        //                                    passageViewModel = MapperHelper.MapToViewModel(currentPassage);
        //                                    passageViewModelList.Add(passageViewModel);
        //                                    isContinueQuestion = true;
        //                                    isNewPassage = false;
        //                                    isContinueAnswer = false;
        //                                    goto ContinueQuestion;
        //                                    //continue;
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }



        //        }
        //    }
        //    string questionIDs = "";
        //    foreach (OAP.Domain.ViewModel.QuestionViewModel question in questionViewModelList)
        //    {
        //        question.AnswerViewModelList = answerViewModelList.Where(a => a.QuestionID == question.ID).ToList();
        //        questionIDs += question.ID + ",";
        //    }
        //    uploadResult.questionViewModelList = questionViewModelList;
        //    uploadResult.UploadedQuestionIDs = questionIDs;
        //    return uploadResult;
        //    //return passageViewModelList;
        //}

        //public List<HtmlEntities> GetHtmlEntities()
        //{
        //    List<HtmlEntities> htmlEntities = new List<HtmlEntities>();
        //    htmlEntities.Add(new HtmlEntities("&rsquo;", "&#8217;"));
        //    htmlEntities.Add(new HtmlEntities("&ldquo;", "&#8220;"));
        //    htmlEntities.Add(new HtmlEntities("&rdquo;", "&#8221;"));
        //    htmlEntities.Add(new HtmlEntities("&lsquo;", "&#8216;"));
        //    htmlEntities.Add(new HtmlEntities("&ndash;", "&#8211;"));
        //    htmlEntities.Add(new HtmlEntities("&bull;", "&#8226;"));
        //    // htmlEntities.Add(new HtmlEntities("&frac;", ""));
        //    htmlEntities.Add(new HtmlEntities("&deg;", "&#176;"));
        //    htmlEntities.Add(new HtmlEntities("&frasl;", "&#8260;"));
        //    htmlEntities.Add(new HtmlEntities("&pi;", "&#928;"));
        //    htmlEntities.Add(new HtmlEntities("&ang;", "&#8736;"));
        //    htmlEntities.Add(new HtmlEntities("&times;", "&#215;"));
        //    htmlEntities.Add(new HtmlEntities("&hellip", "&#8230;"));
        //    return htmlEntities;

        //}

        #endregion

        #endregion

        #region Get QuestionPaper Under Batch Old

        public void GetQuestionPaperUnderBatch(string batchIDString)
        {
            int batchID = Convert.ToInt32(batchIDString);
            Batch batch = db.Batch.Find(batchID);
            IQueryable<QuestionPaper> questionpapers = null;
            List<OAP.Domain.ViewModel.QuestionPaperViewModel> assessmentViewModelList = new List<Domain.ViewModel.QuestionPaperViewModel>();
            if (batch != null && User.Identity.IsAuthenticated)
            {
                UserProfile loggedInUserProfile = db.UserProfile.Where(up => up.UserName.ToLower().Equals(User.Identity.Name.ToLower())).FirstOrDefault();
                if (User.IsInRole("SuperAdmin"))
                {                                                           //User is admin
                    questionpapers = batch.BatchAssessment.Select(x => x.QuestionPaper).Distinct().AsQueryable();
                }
                if (User.IsInRole("CollegeAdmin"))
                {                                                           //User is admin
                    questionpapers = batch.BatchAssessment.Select(x => x.QuestionPaper).Distinct().AsQueryable();
                }
                else if (User.IsInRole("Admin"))
                {
                    List<long> userProfileCollegeIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                    questionpapers = batch.BatchAssessment.Where(ba => (userProfileCollegeIDList.Contains(ba.Batch.CollegeID ?? 0))).Select(x => x.QuestionPaper).Distinct().AsQueryable();
                }
                if (questionpapers != null)
                    assessmentViewModelList = MapperHelper.MapToViewModelList(questionpapers);
            }
            Response.Write(JsonConvert.SerializeObject(assessmentViewModelList));
        }

        #endregion

        #region Get QuestionPaper Under Branch

        public void GetQuestionPaperUnderBranch(string branchIDString)
        {
            long branchID = Convert.ToInt32(branchIDString);
            Branch branch = db.Branch.Find(branchID);
            IQueryable<QuestionPaper> questionpapers = null;
            List<OAP.Domain.ViewModel.QuestionPaperViewModel> assessmentViewModelList = new List<Domain.ViewModel.QuestionPaperViewModel>();

            if (branch != null && User.Identity.IsAuthenticated)
            {
                if (User.IsInRole("SuperAdmin"))
                {                                                       //User is admin
                    List<UserAssessment> userQuestionPapers = branch.Student.Where(s => s.UserProfile.Where(up => up.UserAssessment.Where(ua => ua.UserProfile.Student.BranchID == branchID).Count() > 0).Count() > 0).SelectMany(x => x.UserProfile.FirstOrDefault().UserAssessment).Distinct().ToList();
                    questionpapers = userQuestionPapers.Select(x => x.QuestionPaper).Distinct().AsQueryable();
                }
                if (User.IsInRole("CollegeAdmin"))
                {                                                       //User is admin
                    List<UserAssessment> userQuestionPapers = branch.Student.Where(s => s.UserProfile.Where(up => up.UserAssessment.Where(ua => ua.UserProfile.Student.BranchID == branchID).Count() > 0).Count() > 0).SelectMany(x => x.UserProfile.FirstOrDefault().UserAssessment).Distinct().ToList();
                    questionpapers = userQuestionPapers.Select(x => x.QuestionPaper).Distinct().AsQueryable();
                }
                else
                {
                    UserProfile loggedInUserProfile = db.UserProfile.Where(up => up.UserName.ToLower().Equals(User.Identity.Name.ToLower())).FirstOrDefault();
                    List<long> userProfileCollegeIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                    List<UserAssessment> userQuestionPapers = branch.Student.Where(s => (userProfileCollegeIDList.Contains(s.BranchID ?? 0)) && s.UserProfile.Where(up => up.UserAssessment.Where(ua => ua.UserProfile.Student.BranchID == branchID).Count() > 0).Count() > 0).SelectMany(x => x.UserProfile.FirstOrDefault().UserAssessment).Distinct().ToList();
                    questionpapers = userQuestionPapers.Select(x => x.QuestionPaper).Distinct().AsQueryable();
                }
                if (questionpapers != null)
                    assessmentViewModelList = MapperHelper.MapToViewModelList(questionpapers);
                assessmentViewModelList = assessmentViewModelList.OrderBy(x => x.Name).ToList();
            }
            Response.Write(JsonConvert.SerializeObject(assessmentViewModelList));
        }

        #endregion

        #region Get Marks scored by the student

        public void GetMarScoredByQuestionPaperID(string assessmentIDString)
        {
            OAP.Domain.ViewModel.QuestionPaperViewModel questionPaperViewModel = new Domain.ViewModel.QuestionPaperViewModel();
            List<QuestionPaperSectionViewModel> assessmentSectionViewModelList = new List<QuestionPaperSectionViewModel>();
            if (!string.IsNullOrEmpty(assessmentIDString) && User.Identity.IsAuthenticated)
            {
                int assessmentID = Convert.ToInt16(assessmentIDString);
                string loggedInUser = User.Identity.Name;
                UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower().Equals(loggedInUser.ToLower())).FirstOrDefault();
                UserAssessment userQuestionPaper = userProfile.UserAssessment.Where(ua => ua.AssessmentID == assessmentID).FirstOrDefault();
                List<QuestionPaperSection> questionPaperSectionList = userQuestionPaper.QuestionPaper.QuestionPaperSection.ToList();

                questionPaperViewModel = MapperHelper.MapToViewModel(userQuestionPaper.QuestionPaper);
                questionPaperViewModel.Marks = userQuestionPaper.QuestionPaper.Marks ?? 0;

                foreach (QuestionPaperSection questionPaperSection in questionPaperSectionList)
                {
                    QuestionPaperSectionViewModel assessmentSectionViewModel = new QuestionPaperSectionViewModel();
                    assessmentSectionViewModel.Name = questionPaperSection.Name;
                    assessmentSectionViewModel.AvailableQuestionsCount = questionPaperSection.QuestionPaperSectionQuestion.Count();

                    decimal marksScoredInThisSection = 0;
                    List<Question> totalQuestionsListInThisSection = questionPaperSection.QuestionPaperSectionQuestion.Where(asq => !(asq.Question.QuestionCategory.Name.ToLower().Equals("open ended"))).Select(x => x.Question).ToList();
                    List<int> questionIDList = totalQuestionsListInThisSection.Select(x => x.ID).ToList();
                    int totalQuestionInThisSection = totalQuestionsListInThisSection.Count();
                    List<Question> userAnsweredQuestionList = userQuestionPaper.AssessmentSnapshot.Where(asn => (questionIDList.Contains(asn.QuestionID ?? 0))).Select(x => x.Question).ToList();
                    int userAnsweredCountInThisSection = userAnsweredQuestionList.Count();

                    int correctAnswerCountInThisSection = 0;

                    foreach (Question question in userAnsweredQuestionList)
                    {
                        QuestionPaperSectionQuestion questionPaperSectionQuestion = questionPaperSection.QuestionPaperSectionQuestion.Where(asq => asq.QuestionID == question.ID).FirstOrDefault();
                        bool isUserAnsweredCorrect = false;

                        if (question.QuestionType.Name.ToLower().Equals("multi select"))
                        {                                                                       //Question type is multiselect
                            List<Answer> correctAnswerList = question.Answer.Where(a => a.IsCorrect ?? false).ToList();
                            List<Answer> userAnsweredList = question.AssessmentSnapshot.Where(asn => asn.UserAssessment.UserID == userProfile.UserId).Select(x => x.Answer).ToList();
                            if (correctAnswerList.Except(userAnsweredList).Count() <= 0 && userAnsweredList.Except(correctAnswerList).Count() <= 0)
                            {
                                isUserAnsweredCorrect = true;
                                correctAnswerCountInThisSection++;
                            }
                        }
                        else if (question.QuestionType.Name.ToLower().Equals("single select"))
                        {
                            int correctAnswer = question.Answer.Where(a => a.IsCorrect ?? false && a.AssessmentSnapshot.Where(asn => asn.UserAssessment.UserID == userProfile.UserId && asn.QuestionID == question.ID).Count() > 0).Count();
                            if (correctAnswer > 0)
                            {
                                isUserAnsweredCorrect = true;
                                correctAnswerCountInThisSection++;
                            }
                        }
                        if (isUserAnsweredCorrect)
                        {                                                           //User selected the correct answer
                            marksScoredInThisSection = marksScoredInThisSection + questionPaperSectionQuestion.Mark != null ? questionPaperSectionQuestion.Mark ?? 0 : (questionPaperSection.Mark != null ? questionPaperSection.Mark ?? 0 : questionPaperSection.QuestionPaper.Marks ?? 0);
                        }
                        else
                        {                                                           //User answer is wrong
                            marksScoredInThisSection = marksScoredInThisSection - questionPaperSectionQuestion.NegativeMark != null ? questionPaperSectionQuestion.NegativeMark ?? 0 : (questionPaperSection.NegativeMark != null ? questionPaperSection.NegativeMark ?? 0 : questionPaperSection.QuestionPaper.NegativeMarks ?? 0);
                        }
                    }

                    assessmentSectionViewModel.UserAnsweredCount = userAnsweredCountInThisSection;
                    assessmentSectionViewModel.CorrectAnswerCount = correctAnswerCountInThisSection;
                    assessmentSectionViewModel.TotalMarksScored = marksScoredInThisSection;
                    assessmentSectionViewModelList.Add(assessmentSectionViewModel);
                }
            }
            questionPaperViewModel.questionPaperSectionViewModelList = assessmentSectionViewModelList;
            assessmentSectionViewModelList = assessmentSectionViewModelList;
            Response.Write(JsonConvert.SerializeObject(questionPaperViewModel));
        }

        #endregion
        /**First code using  open office xml**/
        /**First code using  open office xml**/
        //public ActionResult OpenDownloadModal(long ID = 0, string AnswerSeries = "", string QuestionSeries = "", string Format = "", string UploadTemplate = "")
        //{
        //    long QuestionID = Convert.ToInt64(QuestionSeries);
        //    long AnswerID = Convert.ToInt64(AnswerSeries);
        //    QuestionPaper questionPaper = db.QuestionPaper.Find(ID);
        //    long templateID = Convert.ToInt64(UploadTemplate);
        //    Series Questionseries = db.Series.Find(QuestionID);
        //    Series Answerseries = db.Series.Find(AnswerID);
        //    //WordprocessingDocument.Open(Server.MapPath("/QuestionPaper/Document/" + questionPaper.Name + "On(" + DateTime.Now + ").doc"), true);
        //    DocumentTemplate documentTemplate = db.DocumentTemplate.Find(templateID);
        //    string PastePath = Server.MapPath("/QuestionPaper/Document");
        //    if (!Directory.Exists(PastePath))
        //    {
        //        Directory.CreateDirectory(PastePath);
        //    }
        //    string existingFileName = System.IO.Path.GetFileName(documentTemplate.FilePath);
        //    string passFileName = Server.MapPath("/QuestionPaper/Document/" + questionPaper.Name + "On(" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ").docx");

        //    string sourcePath = System.IO.Path.Combine(Server.MapPath("/Template/DocumentTemplate/"), existingFileName);
        //    string destinationPath = System.IO.Path.Combine(Server.MapPath("/QuestionPaper/Document/"), questionPaper.Name + "On(" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ").docx");
        //    System.IO.File.Copy(sourcePath, destinationPath);
        //    string QuestionNumberType = Questionseries.Name.ToString();
        //    string AnswerNumberType = Answerseries.Name.ToString();
        //    //IQueryable<Question> questionPaperQuestion = db.QuestionPaperQuestion.Where(x => x.QuestionPaperID == id).Select(y=>y.Question);
        //    //IQueryable<Question> Question =db.Question.Where(y=>y.Que)
        //    //List<OAP.Domain.ViewModel.QuestionViewModel> questionList = MapperHelper.MapToViewModelList(questionPaperQuestion);
        //    //File file;

        //    if (questionPaper != null)
        //    {

        //        OAP.Domain.ViewModel.QuestionPaperViewModel questionPaperViewModel = MapperHelper.MapToViewModel(questionPaper);
        //        questionPaperViewModel.SectionCount = questionPaper.QuestionPaperSection.Count();
        //        if (questionPaperViewModel.TimeLimit == null || questionPaperViewModel.TimeLimit == 0)
        //        {                                                                                   //Time limit not given for assessment
        //            questionPaperViewModel.IsShowSectionTimeLimit = true;
        //        }
        //        else
        //        {                                                                                   //Time limit given for whole assessment
        //            questionPaperViewModel.IsShowSectionTimeLimit = false;
        //        }
        //        using (MemoryStream mem = new MemoryStream())
        //        {
        //            // Create Document
        //            using (WordprocessingDocument wordDocument =
        //                // WordprocessingDocument.Create(mem, WordprocessingDocumentType.Document, true))
        //                //using (WordprocessingDocument document = WordprocessingDocument.Open(documentPath, true))
        //                 WordprocessingDocument.Open(Server.MapPath("/QuestionPaper/Document/" + questionPaper.Name + "On(" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ").docx"), true))
        //            {
        //                // Add a main document part. 
        //                MainDocumentPart mainPart;
        //                if (wordDocument.MainDocumentPart == null)
        //                {
        //                    mainPart = wordDocument.AddMainDocumentPart();
        //                }
        //                else
        //                {
        //                    mainPart = wordDocument.MainDocumentPart;
        //                }

        //                //Create Document tree for simple document. 
        //                if (mainPart.Document == null)
        //                {
        //                    mainPart.Document = new Document();
        //                }

        //                Body body = mainPart.Document.AppendChild(new Body());
        //                DocumentFormat.OpenXml.Wordprocessing.Paragraph para = body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph());


        //                DocumentFormat.OpenXml.Wordprocessing.Run run = para.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Run());
        //                DocumentFormat.OpenXml.Wordprocessing.RunProperties runProperties = run.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.RunProperties());
        //                RunFonts rFont = new RunFonts();
        //                rFont.Ascii = "Arial"; // the font is Arial
        //                //runProperties.Append(new Bold()); // it is Bold
        //                // mainPart.Document.Append(new Bold());
        //                //runProperties.Append(new Bold()); // it is Bold
        //                Bold bold = new Bold();
        //                bold.Val = OnOffValue.FromBoolean(true);
        //                runProperties.AppendChild(bold);
        //                //body.AppendChild(bold);
        //                runProperties.Append(new FontSize() { Val = "28" });

        //                //string printingtext = " ";
        //                //int textlength = questionPaperViewModel.Name.Count();
        //                //if (textlength <= 56)
        //                //{
        //                //    int newcount = 56 - textlength;
        //                //    newcount = newcount / 2;
        //                //    for (int j = 1; j <= newcount; j++)
        //                //    {
        //                //        printingtext = printingtext + "";
        //                //    }
        //                //    printingtext = printingtext + questionPaperViewModel.Name;
        //                //}

        //                mainPart.Document.Body.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new Text(questionPaperViewModel.Name))));
        //                para.ParagraphProperties = new DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties();
        //                para.ParagraphProperties.TextAlignment = new TextAlignment();
        //                para.ParagraphProperties.TextAlignment.Val = VerticalTextAlignmentValues.Top;


        //                //Table table = new Table();
        //                //// Initialize all of the table properties
        //                //TableProperties tblProp = new TableProperties(
        //                //    new TableBorders(
        //                //        new TopBorder() { Val = new EnumValue<BorderValues>(BorderValues.BasicBlackSquares), Size = 16 },
        //                //        new LeftBorder() { Val = new EnumValue<BorderValues>(BorderValues.BasicBlackSquares), Size = 16 },
        //                //        new RightBorder() { Val = new EnumValue<BorderValues>(BorderValues.BasicBlackSquares), Size = 16 },
        //                //        new BottomBorder() { Val = new EnumValue<BorderValues>(BorderValues.BasicBlackSquares), Size = 16 }
        //                //    )

        //                //);
        //                //TableProperties tblPr = new TableProperties();
        //                //TableBorders tblBorders = new TableBorders();

        //                //tblBorders.InsideHorizontalBorder = new InsideHorizontalBorder();
        //                //tblBorders.InsideHorizontalBorder.Val = BorderValues.Single;
        //                //tblBorders.InsideVerticalBorder = new InsideVerticalBorder();
        //                //tblBorders.InsideVerticalBorder.Val = BorderValues.Single;
        //                //tblPr.Append(tblBorders);
        //                //table.Append(tblPr);
        //                //TableCell tc = new TableCell();
        //                IQueryable<QuestionPaperSection> Sections = questionPaper.QuestionPaperSection.AsQueryable();
        //                foreach (QuestionPaperSection section in Sections)
        //                {

        //                    para = body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph());
        //                    run = para.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Run());
        //                    runProperties = run.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.RunProperties());
        //                    rFont = new RunFonts();
        //                    rFont.Ascii = "Arial"; // the font is Arial
        //                    runProperties.Append(new Bold()); // it is Bold
        //                    runProperties.Append(new FontSize() { Val = "28" });

        //                    mainPart.Document.Body.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new Text(section.Name))));
        //                    IQueryable<Question> questionPaperQuestion = section.QuestionPaperSectionQuestion.Select(y => y.Question).OrderBy(q => q.PassageID).AsQueryable();
        //                    int QuestionNumber = 0;

        //                    foreach (Question question in questionPaperQuestion)
        //                    {

        //                        Passage passages = question.Passage;

        //                        // table = new Table();
        //                        IQueryable<Answer> answerList = question.Answer.AsQueryable();
        //                        // TableRow tr = new TableRow();

        //                        //TableCell header_cell = new TableCell();
        //                        //GridSpan gridSpan = new GridSpan();
        //                        if (passages != null)
        //                        {
        //                            if (PrevPassageID != passages.ID)
        //                            {
        //                                QuestionNumber++;
        //                                passageFirstQuestion = QuestionNumber;
        //                                passageLastQuestion = QuestionNumber + (questionPaperQuestion.Where(q => q.PassageID == passages.ID).Count() - 1);
        //                                //tr = new TableRow();
        //                                //header_cell = new TableCell();
        //                                //gridSpan = new GridSpan();
        //                                //gridSpan.Val = answerList.Count();
        //                                string FirstQuestion = "";
        //                                string LastQuestion = "";
        //                                if (QuestionNumberType == "Numeric")
        //                                {
        //                                    FirstQuestion = passageFirstQuestion.ToString();
        //                                    LastQuestion = passageLastQuestion.ToString();
        //                                }
        //                                else if (QuestionNumberType == "Uppercase")
        //                                {
        //                                    FirstQuestion = ToAlphaUpper(passageFirstQuestion);
        //                                    LastQuestion = ToAlphaUpper(passageLastQuestion);
        //                                }
        //                                else if (QuestionNumberType == "Lowercase")
        //                                {
        //                                    FirstQuestion = ToAlphaLower(passageFirstQuestion);
        //                                    LastQuestion = ToAlphaLower(passageLastQuestion);
        //                                }
        //                                else if (QuestionNumberType == "RomanUpperCase")
        //                                {
        //                                    FirstQuestion = ToRomanUpper(passageFirstQuestion);
        //                                    LastQuestion = ToRomanUpper(passageLastQuestion);

        //                                }
        //                                else if (QuestionNumberType == "RomanLowerCase")
        //                                {
        //                                    FirstQuestion = ToRomanLower(passageFirstQuestion);
        //                                    LastQuestion = ToRomanLower(passageLastQuestion);
        //                                }


        //                                string Passage = Regex.Replace("Instructions for Questions (" + FirstQuestion + " to " + LastQuestion + " ) :" + passages.Description, @"<[^>]*>", String.Empty);
        //                                //header_cell.Append(gridSpan);
        //                                mainPart.Document.Body.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new Text(Passage))));
        //                                //////
        //                                //tr.Append(header_cell);
        //                                //table.Append(tr);
        //                                PrevPassageID = passages.ID;

        //                            }
        //                            else
        //                            {
        //                                QuestionNumber++;

        //                            }
        //                        }
        //                        else
        //                        {
        //                            QuestionNumber++;
        //                        }
        //                        string number = "";
        //                        if (QuestionNumberType == "Numeric")
        //                        {
        //                            number = QuestionNumber.ToString();
        //                        }
        //                        else if (QuestionNumberType == "Uppercase")
        //                        {
        //                            number = ToAlphaUpper(QuestionNumber);
        //                        }
        //                        else if (QuestionNumberType == "Lowercase")
        //                        {
        //                            number = ToAlphaLower(QuestionNumber);
        //                        }
        //                        else if (QuestionNumberType == "RomanUpperCase")
        //                        {
        //                            number = ToRomanUpper(QuestionNumber);

        //                        }
        //                        else if (QuestionNumberType == "RomanLowerCase")
        //                        {
        //                            number = ToRomanLower(QuestionNumber);
        //                        }
        //                        //tr = new TableRow();
        //                        //header_cell = new TableCell();
        //                        //gridSpan = new GridSpan();
        //                        //gridSpan.Val = answerList.Count();
        //                        //if (question.ID == 8812)
        //                        //{
        //                        //}
        //                        List<Uri> uriList = FetchLinksFromSource(question.Description ?? "");

        //                        //  string Description = Regex.Replace(number + "." + question.Description.ToString(), @"<[^>]*>", String.Empty,RegexOptions.IgnoreCase);
        //                        //string Description1 = Regex.Replace(number + "." + question.Description ?? "", @"<[^>]*>", String.Empty, RegexOptions.IgnoreCase);
        //                        // string Description2 = Regex.Match(question.Description ?? "", "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase).Groups[1].Value;
        //                        string Description1 = Regex.Replace(number + "." + question.Description ?? "", @"(?i)<(?!img|/img).*?>", String.Empty, RegexOptions.IgnoreCase);
        //                        string fileName = "";
        //                        for (int k = 0; k < uriList.Count(); k++)
        //                        {
        //                            fileName = @"" + uriList[k];

        //                            //string fileName = @""+Description2;
        //                            if (!string.IsNullOrEmpty(fileName))
        //                            {
        //                                //InsertAPicture(wordDocument, fileName);
        //                            }
        //                        }

        //                        string Description = Description1;//+ Description2;

        //                        //header_cell.Append(gridSpan);
        //                        mainPart.Document.Body.Append(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new DocumentFormat.OpenXml.Wordprocessing.Run(new Text(Description))));
        //                        //////
        //                        //tr.Append(header_cell);
        //                        //table.Append(tr);
        //                        //tr = new TableRow();
        //                        Table table = new Table();
        //                        TableRow tr;
        //                        tr = new TableRow();
        //                        TableCell tc;
        //                        tc = new TableCell();

        //                        int AnswerNumber = 1;

        //                        foreach (Answer answer in answerList)
        //                        {
        //                            string Answernumber = "";

        //                            if (AnswerNumberType == "Numeric")
        //                            {
        //                                Answernumber = AnswerNumber.ToString();
        //                            }
        //                            else if (AnswerNumberType == "Uppercase")
        //                            {
        //                                Answernumber = ToAlphaUpper(AnswerNumber);
        //                            }
        //                            else if (AnswerNumberType == "Lowercase")
        //                            {
        //                                Answernumber = ToAlphaLower(AnswerNumber);
        //                            }
        //                            else if (AnswerNumberType == "RomanUpperCase")
        //                            {
        //                                Answernumber = ToRomanUpper(AnswerNumber);
        //                            }
        //                            else if (AnswerNumberType == "RomanLowerCase")
        //                            {
        //                                Answernumber = ToRomanLower(AnswerNumber);
        //                            }

        //                            //string AnswerDescription1 = Regex.Replace(Answernumber + "." + answer.Description.ToString(), @"<[^>]*>", String.Empty, RegexOptions.IgnoreCase);
        //                            //string AnswerDescription2 = Regex.Match(answer.Description.ToString(), "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase).Groups[1].Value;
        //                            string AnswerDescription1 = Regex.Replace(Answernumber + "." + answer.Description ?? "", @"(?i)<(?!img|/img).*?>", String.Empty, RegexOptions.IgnoreCase);

        //                            string AnswerDescription = AnswerDescription1;
        //                            //mainPart.Document.Body.Append(new TableCell(new Paragraph(new Run(new Text(AnswerDescription)))));
        //                            tc = new TableCell(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(new Text(AnswerDescription))));
        //                            AnswerNumber++;
        //                            tr.Append(tc);
        //                        }
        //                        //gridSpan.Val = answerList.Count();

        //                        table.Append(tr);
        //                        //tcp.Append(gridSpan);
        //                        //tc.Append(tcp);
        //                        //table.Append(tr);
        //                        mainPart.Document.Body.Append(table);
        //                    }
        //                }

        //                mainPart.Document.Save();
        //                wordDocument.Close();
        //                // Document document = new Document();
        //                string htmlPath = System.IO.Path.Combine(Server.MapPath("/QuestionPaper/Document/"), questionPaper.Name + "On(" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ").html");
        //                string convertedHtmlPath = System.IO.Path.Combine(Server.MapPath("/QuestionPaper/Document/"), questionPaper.Name + "On(" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + "-1).docx");
        //            }
        //            string Filename = System.IO.Path.GetFileName(System.IO.Path.Combine(Server.MapPath("/QuestionPaper/Document/"), questionPaper.Name + "On(" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ")"));
        //            InsertImage(ID, AnswerSeries, QuestionSeries, UploadTemplate, passFileName);
        //            return SendReportAsDOC(null, Filename, Format);

        //            //string date = DateTime.Now.Date.ToString();
        //        }
        //    }
        //    else
        //    {
        //        return View("QuestionPaperList");
        //    }

        //}
        /////**Using Docx**/
        ////public void OpenDownloadModalDOCX(long ID = 0, string AnswerSeries = "", string QuestionSeries = "", string UploadTemplate = "", string Filename = "")
        ////{
        ////    long QuestionID = Convert.ToInt64(QuestionSeries);
        ////    long AnswerID = Convert.ToInt64(AnswerSeries);
        ////    QuestionPaper questionPaper = db.QuestionPaper.Find(ID);
        ////    long templateID = Convert.ToInt64(UploadTemplate);
        ////    Series Questionseries = db.Series.Find(QuestionID);
        ////    Series Answerseries = db.Series.Find(AnswerID);

        ////    if (questionPaper != null)
        ////    {

        ////        OAP.Domain.ViewModel.QuestionPaperViewModel questionPaperViewModel = MapperHelper.MapToViewModel(questionPaper);
        ////        questionPaperViewModel.SectionCount = questionPaper.QuestionPaperSection.Count();
        ////        if (questionPaperViewModel.TimeLimit == null || questionPaperViewModel.TimeLimit == 0)
        ////        {                                                                                   //Time limit not given for assessment
        ////            questionPaperViewModel.IsShowSectionTimeLimit = true;
        ////        }
        ////        else
        ////        {                                                                                   //Time limit given for whole assessment
        ////            questionPaperViewModel.IsShowSectionTimeLimit = false;
        ////        }
        ////        using (MemoryStream mem = new MemoryStream())
        ////        {
        ////            // Create Document
        ////            using (var wordDocument =
        ////                // WordprocessingDocument.Create(mem, WordprocessingDocumentType.Document, true))
        ////                //using (WordprocessingDocument document = WordprocessingDocument.Open(documentPath, true))
        ////                 Novacode.DocX.Load(Filename))
        ////            {

        ////                IQueryable<QuestionPaperSection> Sections = questionPaper.QuestionPaperSection.AsQueryable();
        ////                foreach (QuestionPaperSection section in Sections)
        ////                {


        ////                    IQueryable<Question> questionPaperQuestion = section.QuestionPaperSectionQuestion.Select(y => y.Question).OrderBy(q => q.PassageID).AsQueryable();
        ////                    int QuestionNumber = 0;

        ////                    foreach (Question question in questionPaperQuestion)
        ////                    {

        ////                        Passage passages = question.Passage;

        ////                        IQueryable<Answer> answerList = question.Answer.AsQueryable();
        ////                        TableRow tr = new TableRow();

        ////                        TableCell header_cell = new TableCell();
        ////                        GridSpan gridSpan = new GridSpan();
        ////                        if (passages != null)
        ////                        {
        ////                            if (PrevPassageID != passages.ID)
        ////                            {
        ////                                QuestionNumber++;
        ////                                passageFirstQuestion = QuestionNumber;
        ////                                passageLastQuestion = QuestionNumber + (questionPaperQuestion.Where(q => q.PassageID == passages.ID).Count() - 1);

        ////                            }
        ////                            else
        ////                            {
        ////                                QuestionNumber++;

        ////                            }
        ////                        }
        ////                        else
        ////                        {
        ////                            QuestionNumber++;
        ////                        }
        ////                        string number = "";

        ////                        header_cell = new TableCell();
        ////                        gridSpan = new GridSpan();
        ////                        gridSpan.Val = answerList.Count();
        ////                        if (question.ID == 8812)
        ////                        {
        ////                        }

        ////                        List<Uri> uriList = FetchLinksFromSource(question.Description.ToString());

        ////                        //  string Description = Regex.Replace(number + "." + question.Description.ToString(), @"<[^>]*>", String.Empty,RegexOptions.IgnoreCase);
        ////                        string Description1 = Regex.Replace(number + "." + question.Description.ToString(), @"<[^>]*>", String.Empty, RegexOptions.IgnoreCase);
        ////                        string Description2 = Regex.Match(question.Description.ToString(), "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase).Groups[1].Value;
        ////                        string document = @"C:\Users\Admin\Downloads\123On(25-06-2015-18-03).docx";
        ////                        string fileName = "";
        ////                        for (int k = 0; k < uriList.Count(); k++)
        ////                        {
        ////                            fileName = @"" + uriList[k];

        ////                            //string fileName = @""+Description2;
        ////                            /**     if (!string.IsNullOrEmpty(fileName))
        ////                                 {

        ////                                     Novacode.Header header_default = wordDocument.Headers.odd;
        ////                                     Novacode.Paragraph p = wordDocument.InsertParagraph("Image");

        ////                                     // InsertImage(doc);

        ////                                     string imageName = "rr.JPEG";
        ////                                     //string url = @"D:\image downloading\image1.jpeg";
        ////                                     string url = fileName;

        ////                                     Novacode.Image img = wordDocument.AddImage(url);

        ////                                     // Insert a Paragraph into the default Header.
        ////                                     Novacode.Picture pic1 = img.CreatePicture();

        ////                                     //Regex.Replace(Description2, "(cool)", "super$1", RegexOptions.IgnoreCase);
        ////                                     Novacode.Paragraph p1 = wordDocument.InsertParagraph();


        ////                                     p1.Pictures.Add(pic1);

        ////                                     // p1.Append("Some more text").Bold();
        ////                                     p1.AppendPicture(pic1);


        ////                                 }*/
        ////                        }

        ////                        tr = new TableRow();
        ////                        //tc = new TableCell(new Paragraph(new Run(new Text("Multiplication table"))));
        ////                        //TableCellProperties tcp = new TableCellProperties();
        ////                        //GridSpan gridSpan = new GridSpan();
        ////                        //gridSpan.Val = 11;

        ////                        // tc.Append(tcp);
        ////                        // tr.Append(tc);

        ////                        int AnswerNumber = 1;

        ////                        foreach (Answer answer in answerList)
        ////                        {
        ////                            string Answernumber = "";



        ////                            //string AnswerDescription = Regex.Match(Answernumber + "." + answer.Description.ToString(), "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase).Groups[1].Value;
        ////                            string AnswerDescription1 = Regex.Replace(Answernumber + "." + answer.Description.ToString(), @"<[^>]*>", String.Empty, RegexOptions.IgnoreCase);
        ////                            string AnswerDescription2 = Regex.Match(answer.Description.ToString(), "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase).Groups[1].Value;
        ////                            //ImagePart AnswerDescriptionnewImage = wordDocument.MainDocumentPart.AddImagePart("images/jpeg");
        ////                            //using (Stream image = new FileStream(AnswerDescription2, FileMode.Open, FileAccess.Read, FileShare.Read))
        ////                            //{
        ////                            //    AnswerDescriptionnewImage.FeedData(image);
        ////                            //}

        ////                            //string fileName1 = @"" + AnswerDescription2;
        ////                            //if (!string.IsNullOrEmpty(fileName1))
        ////                            //{
        ////                            //    InsertAPicture(document, fileName1);
        ////                            //}

        ////                            string AnswerDescription = AnswerDescription1 + AnswerDescription2;
        ////                            //string AnswerDescription = Regex.Replace(Answernumber + "." + answer.Description.ToString(), @"<[^>]*>", String.Empty);
        ////                            //string AnswerDescription = Answernumber + "." + answer.Description.ToString();
        ////                            //header_cell.Append(new Paragraph(new Run(new Text(AnswerDescription))));

        ////                            tr.Append(new TableCell(new Paragraph(new Run(new Text(AnswerDescription)))));
        ////                            AnswerNumber++;
        ////                        }

        ////                    }
        ////                }

        ////                wordDocument.Save();
        ////                string convertedHtmlPath = Path.Combine(Server.MapPath("/QuestionPaper/Document/"), questionPaper.Name + "On(" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm") + "-1).docx");
        ////            }

        ////        }
        ////        // ReplaceTextWithImage(Filename);
        ////        InsertImage(ID, AnswerSeries, QuestionSeries, UploadTemplate, Filename);
        ////    }
        ////    else
        ////    {
        ////    }

        ////}
        //private void InsertImage(long ID = 0, string AnswerSeries = "", string QuestionSeries = "", string UploadTemplate = "", string Filename = "")
        //{
        //    long QuestionID = Convert.ToInt64(QuestionSeries);
        //    long AnswerID = Convert.ToInt64(AnswerSeries);
        //    QuestionPaper questionPaper = db.QuestionPaper.Find(ID);
        //    long templateID = Convert.ToInt64(UploadTemplate);
        //    Series Questionseries = db.Series.Find(QuestionID);
        //    Series Answerseries = db.Series.Find(AnswerID);
        //    Spire.Doc.Document document = new Spire.Doc.Document();

        //    document.LoadFromFile(Filename);
        //    if (questionPaper != null)
        //    {

        //        OAP.Domain.ViewModel.QuestionPaperViewModel questionPaperViewModel = MapperHelper.MapToViewModel(questionPaper);
        //        questionPaperViewModel.SectionCount = questionPaper.QuestionPaperSection.Count();
        //        if (questionPaperViewModel.TimeLimit == null || questionPaperViewModel.TimeLimit == 0)
        //        {                                                                                   //Time limit not given for assessment
        //            questionPaperViewModel.IsShowSectionTimeLimit = true;
        //        }
        //        else
        //        {                                                                                   //Time limit given for whole assessment
        //            questionPaperViewModel.IsShowSectionTimeLimit = false;
        //        }


        //        IQueryable<QuestionPaperSection> Sections = questionPaper.QuestionPaperSection.AsQueryable();
        //        foreach (QuestionPaperSection section in Sections)
        //        {


        //            IQueryable<Question> questionPaperQuestion = section.QuestionPaperSectionQuestion.Select(y => y.Question).OrderBy(q => q.PassageID).AsQueryable();
        //            int QuestionNumber = 0;

        //            foreach (Question question in questionPaperQuestion)
        //            {

        //                Passage passages = question.Passage;

        //                IQueryable<Answer> answerList = question.Answer.AsQueryable();

        //                if (passages != null)
        //                {
        //                    if (PrevPassageID != passages.ID)
        //                    {
        //                        QuestionNumber++;
        //                        passageFirstQuestion = QuestionNumber;
        //                        passageLastQuestion = QuestionNumber + (questionPaperQuestion.Where(q => q.PassageID == passages.ID).Count() - 1);

        //                    }
        //                    else
        //                    {
        //                        QuestionNumber++;

        //                    }
        //                }
        //                else
        //                {
        //                    QuestionNumber++;
        //                }
        //                string number = "";


        //                if (question.ID == 8812)
        //                {
        //                }

        //                List<Uri> uriList = FetchLinksFromSource(question.Description.ToString());

        //                //  string Description = Regex.Replace(number + "." + question.Description.ToString(), @"<[^>]*>", String.Empty,RegexOptions.IgnoreCase);
        //                //string Description2 = Regex.Match(question.Description.ToString(), "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase).Groups[1].Value;
        //                // string Description2 = Regex.Match(question.Description.ToString(), @"^<img src\s?=\s?([^\s]+).*/>$", RegexOptions.IgnoreCase).Groups[1].Value;

        //                string url = "";
        //                string fileName = "";
        //                for (int k = 0; k < uriList.Count(); k++)
        //                {
        //                    fileName = @"" + uriList[k];

        //                    //string fileName = @""+Description2;
        //                    if (!string.IsNullOrEmpty(uriList[k].ToString()))
        //                    {
        //                        //InsertAPicture(wordDocument, fileName);

        //                        //Load image from server to memory stream


        //                        WebClient wc = new WebClient();
        //                        MemoryStream ms = new MemoryStream();
        //                        try
        //                        {
        //                            byte[] bytes = wc.DownloadData(uriList[k]);
        //                            ms = new MemoryStream(bytes);
        //                        }
        //                        catch
        //                        {

        //                        }

        //                        System.Drawing.Image image = System.Drawing.Image.FromStream(ms);
        //                        Spire.Doc.Documents.TextSelection[] selections = null;
        //                        for (int i = 0; i < 1000; i++)
        //                        {
        //                            for (int j = 0; j < 1000; j++)
        //                            {
        //                                string width = @"""" + i.ToString() + @"""";
        //                                string height = @"""" + j.ToString() + @"""";
        //                                string urlBind = @"""" + uriList[k].ToString() + @"""";
        //                                string replaceText = "<img width=" + width + " height=" + height + " src=" + urlBind + " />";
        //                                selections = document.FindAllString(replaceText, false, true);
        //                                if (selections != null)
        //                                {
        //                                    break;
        //                                }
        //                                //Spire.Doc.Documents.TextSelection[] check;
        //                                //check=document.FindAllString(replaceText, false, true);
        //                                //if (check != null)
        //                                //{
        //                                //}
        //                            }
        //                            if (selections != null)
        //                            {
        //                                break;
        //                            }

        //                        }
        //                        // Spire.Doc.Documents.TextSelection[] selections = document.FindAllString(uriList[k].ToString(), false, true);
        //                        if (selections == null)
        //                        {
        //                            selections = document.FindAllString(uriList[k].ToString(), false, true);
        //                        }
        //                        int index = 0;

        //                        Spire.Doc.Fields.TextRange range = null;


        //                        if (selections != null)
        //                        {
        //                            foreach (Spire.Doc.Documents.TextSelection selection in selections)
        //                            {

        //                                Spire.Doc.Fields.DocPicture pic = new Spire.Doc.Fields.DocPicture(document);

        //                                pic.LoadImage(image);



        //                                range = selection.GetAsOneRange();

        //                                index = range.OwnerParagraph.ChildObjects.IndexOf(range);

        //                                range.OwnerParagraph.ChildObjects.Insert(index, pic);

        //                                range.OwnerParagraph.ChildObjects.Remove(range);


        //                            }
        //                        }

        //                    }
        //                }
        //                foreach (Answer answer in question.Answer)
        //                {
        //                    uriList = FetchLinksFromSource(answer.Description.ToString());

        //                    //  string Description = Regex.Replace(number + "." + question.Description.ToString(), @"<[^>]*>", String.Empty,RegexOptions.IgnoreCase);
        //                    //string Description2 = Regex.Match(question.Description.ToString(), "<img.+?src=[\"'](.+?)[\"'].*?>", RegexOptions.IgnoreCase).Groups[1].Value;
        //                    // string Description2 = Regex.Match(question.Description.ToString(), @"^<img src\s?=\s?([^\s]+).*/>$", RegexOptions.IgnoreCase).Groups[1].Value;

        //                    for (int k = 0; k < uriList.Count(); k++)
        //                    {
        //                        fileName = @"" + uriList[k];

        //                        //string fileName = @""+Description2;
        //                        if (!string.IsNullOrEmpty(uriList[k].ToString()))
        //                        {
        //                            //InsertAPicture(wordDocument, fileName);

        //                            //Load image from server to memory stream


        //                            WebClient wc = new WebClient();
        //                            MemoryStream ms = new MemoryStream();
        //                            try
        //                            {
        //                                byte[] bytes = wc.DownloadData(uriList[k]);
        //                                ms = new MemoryStream(bytes);
        //                            }
        //                            catch
        //                            {

        //                            }

        //                            System.Drawing.Image image = System.Drawing.Image.FromStream(ms);



        //                            //Spire.Doc.Documents.TextSelection[] selections = document.FindAllString(uriList[k].ToString(), false, true);
        //                            Spire.Doc.Documents.TextSelection[] selections = null;
        //                            for (int i = 0; i < 1000; i++)
        //                            {
        //                                for (int j = 0; j < 1000; j++)
        //                                {
        //                                    string width = @"""" + i.ToString() + @"""";
        //                                    string height = @"""" + j.ToString() + @"""";
        //                                    string urlBind = @"""" + uriList[k].ToString() + @"""";
        //                                    string replaceText = "<img width=" + width + " height=" + height + " src=" + urlBind + " />";
        //                                    selections = document.FindAllString(replaceText, false, true);
        //                                    if (selections != null)
        //                                    {
        //                                        break;
        //                                    }
        //                                    //Spire.Doc.Documents.TextSelection[] check;
        //                                    //check=document.FindAllString(replaceText, false, true);
        //                                    //if (check != null)
        //                                    //{
        //                                    //}
        //                                }
        //                                if (selections != null)
        //                                {
        //                                    break;
        //                                }

        //                            }

        //                            int index = 0;

        //                            Spire.Doc.Fields.TextRange range = null;


        //                            if (selections != null)
        //                            {
        //                                foreach (Spire.Doc.Documents.TextSelection selection in selections)
        //                                {

        //                                    Spire.Doc.Fields.DocPicture pic = new Spire.Doc.Fields.DocPicture(document);

        //                                    pic.LoadImage(image);



        //                                    range = selection.GetAsOneRange();

        //                                    index = range.OwnerParagraph.ChildObjects.IndexOf(range);

        //                                    range.OwnerParagraph.ChildObjects.Insert(index, pic);

        //                                    range.OwnerParagraph.ChildObjects.Remove(range);


        //                                }
        //                            }

        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    document.SaveToFile(Filename);
        //    SearchAndReplace(Filename);
        //}
        //public static void SearchAndReplace(string document)
        //{
        //    using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(document, true))
        //    {
        //        string docText = null;
        //        using (StreamReader sr = new StreamReader(wordDoc.MainDocumentPart.GetStream()))
        //        {
        //            docText = sr.ReadToEnd();
        //        }

        //        Regex regexText = new Regex("Evaluation Warning : The document was created with Spire.Doc for .NET.");
        //        docText = regexText.Replace(docText, ""); // <-- Replacement text
        //        // regexText = new Regex(@"<[^>]*>");
        //        //docText = regexText.Replace(docText, ""); // <-- Replacement text

        //        //docText = Regex.Replace(docText, @"<[^>]*>", String.Empty, RegexOptions.IgnoreCase);

        //        using (StreamWriter sw = new StreamWriter(wordDoc.MainDocumentPart.GetStream(FileMode.Create)))
        //        {
        //            sw.Write(docText);
        //        }
        //        wordDoc.MainDocumentPart.Document.Save();
        //    }
        //}


        //// public static void ReplaceTextWithImage(string)
        ////private static void ReplaceTextWithImage(string Filename = "")
        ////{
        ////    Spire.Doc.Document doc = new Spire.Doc.Document(Filename);
        ////    int j = 1;
        ////    foreach (Spire.Doc.Section sec in doc.Sections)
        ////    {
        ////        foreach (Spire.Doc.Documents.Paragraph para in sec.Paragraphs)
        ////        {
        ////            List<Spire.Doc.DocumentObject> images = new List<Spire.Doc.DocumentObject>();
        ////            foreach (Spire.Doc.DocumentObject docObj in para.ChildObjects)
        ////            {
        ////                if (docObj.DocumentObjectType == Spire.Doc.Documents.DocumentObjectType.Picture)
        ////                {
        ////                    images.Add(docObj);
        ////                }
        ////            }
        ////            foreach (Spire.Doc.DocumentObject pic in images)
        ////            {
        ////                int index = para.ChildObjects.IndexOf(pic);
        ////                Spire.Doc.Fields.TextRange range = new Spire.Doc.Fields.TextRange(doc);
        ////                range.Text = string.Format("C# Corner Demo Example {0}", j);
        ////                para.ChildObjects.Insert(index, range);
        ////                para.ChildObjects.Remove(pic);
        ////                j++;
        ////            }
        ////        }
        ////    }
        ////    doc.SaveToFile(@"D:\result.docx", Spire.Doc.FileFormat.Docx);
        ////    System.Diagnostics.Process.Start(@"D:\result.docx");
        ////}
        //public List<Uri> FetchLinksFromSource(string htmlSource)
        //{
        //    List<Uri> links = new List<Uri>();
        //    string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
        //    MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
        //    foreach (Match m in matchesImgSrc)
        //    {
        //        string href = m.Groups[1].Value;
        //        //links.Add(new Uri(href));
        //        try
        //        {
        //            links.Add(new Uri(href));
        //        }
        //        catch
        //        {
        //        }
        //    }
        //    return links;
        //}


        //public static void InsertAPicture(WordprocessingDocument wordprocessingDocument, string fileName)
        //{

        //    string localImagePath = @"D:\image downloading\";
        //    // string localImagePath = @"D:\image downloading\image1.jpeg";

        //    List<string> fileNames = fileName.Split('/').ToList();
        //    int filecount = fileNames.Count();
        //    string curectFileName = fileNames[filecount - 1];
        //    try
        //    {
        //        using (WebClient client = new WebClient())
        //        {
        //            client.DownloadFile(fileName, localImagePath + curectFileName);
        //        }
        //    }
        //    catch
        //    {
        //    }


        //    MainDocumentPart mainPart = wordprocessingDocument.MainDocumentPart;

        //    ImagePart imagePart = mainPart.AddImagePart(ImagePartType.Jpeg);
        //    var url = fileName;

        //    var uri = new Uri(url);
        //    var path = System.IO.Path.GetFileName(uri.AbsolutePath);
        //    try
        //    {
        //        using (FileStream stream = new FileStream(localImagePath, FileMode.Open))
        //        {
        //            imagePart.FeedData(stream);
        //            AddImageToBody(wordprocessingDocument, mainPart.GetIdOfPart(imagePart));
        //        }
        //    }
        //    catch
        //    {
        //    }


        //    //ImagePart imagePart = mainPart.AddImagePart(ImagePartType.Gif);
        //    //string imageFileName = System.Web.HttpContext.Current.Server.MapPath(headerImageFile);
        //    //using (FileStream stream = new FileStream(imageFileName, FileMode.Open)) {
        //    //    imagePart.FeedData(stream);

        //}

        //private static void AddImageToBody(WordprocessingDocument wordDoc, string relationshipId)
        //{
        //    // Define the reference of the image.
        //    var element =
        //         new Drawing(
        //             new DW.Inline(
        //                 new DW.Extent() { Cx = 990000L, Cy = 792000L },
        //                 new DW.EffectExtent()
        //                 {
        //                     LeftEdge = 0L,
        //                     TopEdge = 0L,
        //                     RightEdge = 0L,
        //                     BottomEdge = 0L
        //                 },
        //                 new DW.DocProperties()
        //                 {
        //                     Id = (UInt32Value)1U,
        //                     Name = "Picture 1"
        //                 },
        //                 new DW.NonVisualGraphicFrameDrawingProperties(
        //                     new A.GraphicFrameLocks() { NoChangeAspect = true }),
        //                 new A.Graphic(
        //                     new A.GraphicData(
        //                         new PIC.Picture(
        //                             new PIC.NonVisualPictureProperties(
        //                                 new PIC.NonVisualDrawingProperties()
        //                                 {
        //                                     Id = (UInt32Value)0U,
        //                                     Name = "New Bitmap Image.jpg"
        //                                 },
        //                                 new PIC.NonVisualPictureDrawingProperties()),
        //                             new PIC.BlipFill(
        //                                 new A.Blip(
        //                                     new A.BlipExtensionList(
        //                                         new A.BlipExtension()
        //                                         {
        //                                             Uri =
        //                                               "{28A0092B-C50C-407E-A947-70E740481C1C}"
        //                                         })
        //                                 )
        //                                 {
        //                                     Embed = relationshipId,
        //                                     CompressionState =
        //                                     A.BlipCompressionValues.Print
        //                                 },
        //                                 new A.Stretch(
        //                                     new A.FillRectangle())),
        //                             new PIC.ShapeProperties(
        //                                 new A.Transform2D(
        //                                     new A.Offset() { X = 0L, Y = 0L },
        //                                     new A.Extents() { Cx = 990000L, Cy = 792000L }),
        //                                 new A.PresetGeometry(
        //                                     new A.AdjustValueList()
        //                                 ) { Preset = A.ShapeTypeValues.Rectangle }))
        //                     ) { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" })
        //             )
        //             {
        //                 DistanceFromTop = (UInt32Value)0U,
        //                 DistanceFromBottom = (UInt32Value)0U,
        //                 DistanceFromLeft = (UInt32Value)0U,
        //                 DistanceFromRight = (UInt32Value)0U,
        //                 EditId = "50D07946"
        //             });

        //    // Append the reference to body, the element should be in a Run.
        //    wordDoc.MainDocumentPart.Document.Body.AppendChild(new DocumentFormat.OpenXml.Wordprocessing.Paragraph(new Run(element)));
        //}
        //// Create an image part and add it to the document.


        private FileStreamResult SendReportAsDOC(MemoryStream stream, string filename, string Format = "PDF")
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            WebClient wc = new WebClient();
            //stream = new MemoryStream(wc.DownloadData(Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename)));
            //HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + filename);
            //FileStreamResult filestream = new FileStreamResult(stream, "application/docx");
            FileStreamResult filestream;
            if (Format == "PDF")
            {
                //  ikvm.runtime.Startup.addBootClassPathAssembly(
                //System.Reflection.Assembly.GetAssembly(
                //    typeof(org.slf4j.impl.StaticLoggerBinder)));
                //  WordprocessingMLPackage wordMLPackage = WordprocessingMLPackage.load(new java.io.File(Server.MapPath("/QuestionPaper/Document/" + filename + ".docx")));
                //  // WordprocessingMLPackage.load(
                //  java.io.FileOutputStream fos = new java.io.FileOutputStream(new java.io.File(Server.MapPath("/QuestionPaper/Document/" + filename + ".pdf")));

                //  org.docx4j.Docx4J.toPDF(wordMLPackage, fos);
                //  //Spire.Doc.Document document = new Spire.Doc.Document(Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".docx"));
                //  //document.SaveToFile(Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".PDF"), Spire.Doc.FileFormat.PDF);
                //  //FileInfo fileinFo = new FileInfo(Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".docx"));

                //  //if (!System.IO.File.Exists(Server.MapPath("/QuestionPaper/Document/" + filename + ".pdf")))
                //  //{

                //  //    var myFile = System.IO.File.Create(Server.MapPath("/QuestionPaper/Document/" + filename + ".pdf"));
                //  //    myFile.Close();
                //  //}
                //  //fileinFo.CopyTo(Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".pdf"), true);

                //  stream = new MemoryStream(wc.DownloadData(Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".PDF")));
                //  //stream = new MemoryStream(wc.DownloadData(wc.DownloadData(Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".PDF"));
                //FileInfo fileinFo = new FileInfo(Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".docx"));

                //if (!System.IO.File.Exists(Server.MapPath("/QuestionPaper/Document/" + filename + ".pdf")))
                //{

                //    var myFile = System.IO.File.Create(Server.MapPath("/QuestionPaper/Document/" + filename + ".pdf"));
                //    myFile.Close();
                //}
                //fileinFo.CopyTo(Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".pdf"), true);
                //#region Spire Evaluation Version Code
                ////Spire.Doc.Document document = new Spire.Doc.Document(Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".docx"));
                ////document.SaveToFile(Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".pdf"), Spire.Doc.FileFormat.PDF);


                ////stream = new MemoryStream(wc.DownloadData(Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".pdf")));
                ////stream = new MemoryStream(wc.DownloadData(wc.DownloadData(Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".PDF")); 
                //Word2Pdf objWorPdf = new Word2Pdf();
                //string backfolder1 = "D:\\WOrdToPDF\\";
                //string strFileName = "TestFile.docx";
                //object FromLocation = backfolder1 + "\\" + strFileName;
                //string FileExtension = Path.GetExtension(strFileName);
                //string ChangeExtension = strFileName.Replace(FileExtension, ".pdf");
                //if (FileExtension == ".doc" || FileExtension == ".docx")
                //{
                //    object ToLocation = backfolder1 + "\\" + ChangeExtension;
                //    objWorPdf.InputLocation = FromLocation;
                //    objWorPdf.OutputLocation = ToLocation;
                //    objWorPdf.Word2PdfCOnversion();
                //}  
                //#endregion

                //HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + filename + ".PDF");
                //filestream = new FileStreamResult(stream, "application/pdf");

                ConvertToPDf(System.IO.Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".docx"), filename);
                stream = new MemoryStream(wc.DownloadData(System.IO.Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".pdf")));
                //stream = new MemoryStream(wc.DownloadData(wc.DownloadData(Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".PDF"));

                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + filename + ".pdf");
                filestream = new FileStreamResult(stream, "application/pdf");
            }
            else
            {
                stream = new MemoryStream(wc.DownloadData(System.IO.Path.Combine(Server.MapPath("/QuestionPaper/Document/"), filename + ".docx")));
                HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + filename + ".docx");
                filestream = new FileStreamResult(stream, "application/docx");
            }
            return filestream;
        }
        //public static void ChangeHeader(String documentPath)
        //{
        //    // Replace header in target document with header of source document.
        //    using (WordprocessingDocument document = WordprocessingDocument.Open(documentPath, true))
        //    {
        //        // Get the main document part
        //        MainDocumentPart mainDocumentPart = document.MainDocumentPart;

        //        // Delete the existing header and footer parts
        //        mainDocumentPart.DeleteParts(mainDocumentPart.HeaderParts);
        //        mainDocumentPart.DeleteParts(mainDocumentPart.FooterParts);

        //        // Create a new header and footer part
        //        HeaderPart headerPart = mainDocumentPart.AddNewPart<HeaderPart>();
        //        FooterPart footerPart = mainDocumentPart.AddNewPart<FooterPart>();

        //        // Get Id of the headerPart and footer parts
        //        string headerPartId = mainDocumentPart.GetIdOfPart(headerPart);
        //        string footerPartId = mainDocumentPart.GetIdOfPart(footerPart);

        //        //GenerateHeaderPartContent(headerPart);

        //        //GenerateFooterPartContent(footerPart);

        //        // Get SectionProperties and Replace HeaderReference and FooterRefernce with new Id
        //        IEnumerable<SectionProperties> sections = mainDocumentPart.Document.Body.Elements<SectionProperties>();

        //        foreach (var section in sections)
        //        {
        //            // Delete existing references to headers and footers
        //            section.RemoveAllChildren<HeaderReference>();
        //            section.RemoveAllChildren<FooterReference>();

        //            // Create the new header and footer reference node
        //            section.PrependChild<HeaderReference>(new HeaderReference() { Id = headerPartId });
        //            section.PrependChild<FooterReference>(new FooterReference() { Id = footerPartId });
        //        }
        //    }
        //}


        //public static void GenerateHeaderPartContent(HeaderPart part)
        //{
        //    Header header1 = new Header() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "w14 wp14" } };
        //    header1.AddNamespaceDeclaration("wpc", "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas");
        //    header1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
        //    header1.AddNamespaceDeclaration("o", "urn:schemas-microsoft-com:office:office");
        //    header1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
        //    header1.AddNamespaceDeclaration("m", "http://schemas.openxmlformats.org/officeDocument/2006/math");
        //    header1.AddNamespaceDeclaration("v", "urn:schemas-microsoft-com:vml");
        //    header1.AddNamespaceDeclaration("wp14", "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing");
        //    header1.AddNamespaceDeclaration("wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing");
        //    header1.AddNamespaceDeclaration("w10", "urn:schemas-microsoft-com:office:word");
        //    header1.AddNamespaceDeclaration("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
        //    header1.AddNamespaceDeclaration("w14", "http://schemas.microsoft.com/office/word/2010/wordml");
        //    header1.AddNamespaceDeclaration("wpg", "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup");
        //    header1.AddNamespaceDeclaration("wpi", "http://schemas.microsoft.com/office/word/2010/wordprocessingInk");
        //    header1.AddNamespaceDeclaration("wne", "http://schemas.microsoft.com/office/word/2006/wordml");
        //    header1.AddNamespaceDeclaration("wps", "http://schemas.microsoft.com/office/word/2010/wordprocessingShape");

        //    DocumentFormat.OpenXml.Wordprocessing.Paragraph paragraph1 = new DocumentFormat.OpenXml.Wordprocessing.Paragraph() { RsidParagraphAddition = "00164C17", RsidRunAdditionDefault = "00164C17" };

        //    DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties paragraphProperties1 = new DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties();
        //    ParagraphStyleId paragraphStyleId1 = new ParagraphStyleId() { Val = "Header" };

        //    paragraphProperties1.Append(paragraphStyleId1);

        //    Run run1 = new Run();
        //    Text text1 = new Text();
        //    text1.Text = "Header";

        //    run1.Append(text1);

        //    paragraph1.Append(paragraphProperties1);
        //    paragraph1.Append(run1);

        //    header1.Append(paragraph1);

        //    part.Header = header1;
        //}

        //public static void GenerateFooterPartContent(FooterPart part)
        //{
        //    Footer footer1 = new Footer() { MCAttributes = new MarkupCompatibilityAttributes() { Ignorable = "w14 wp14" } };
        //    footer1.AddNamespaceDeclaration("wpc", "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas");
        //    footer1.AddNamespaceDeclaration("mc", "http://schemas.openxmlformats.org/markup-compatibility/2006");
        //    footer1.AddNamespaceDeclaration("o", "urn:schemas-microsoft-com:office:office");
        //    footer1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");
        //    footer1.AddNamespaceDeclaration("m", "http://schemas.openxmlformats.org/officeDocument/2006/math");
        //    footer1.AddNamespaceDeclaration("v", "urn:schemas-microsoft-com:vml");
        //    footer1.AddNamespaceDeclaration("wp14", "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing");
        //    footer1.AddNamespaceDeclaration("wp", "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing");
        //    footer1.AddNamespaceDeclaration("w10", "urn:schemas-microsoft-com:office:word");
        //    footer1.AddNamespaceDeclaration("w", "http://schemas.openxmlformats.org/wordprocessingml/2006/main");
        //    footer1.AddNamespaceDeclaration("w14", "http://schemas.microsoft.com/office/word/2010/wordml");
        //    footer1.AddNamespaceDeclaration("wpg", "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup");
        //    footer1.AddNamespaceDeclaration("wpi", "http://schemas.microsoft.com/office/word/2010/wordprocessingInk");
        //    footer1.AddNamespaceDeclaration("wne", "http://schemas.microsoft.com/office/word/2006/wordml");
        //    footer1.AddNamespaceDeclaration("wps", "http://schemas.microsoft.com/office/word/2010/wordprocessingShape");

        //    DocumentFormat.OpenXml.Wordprocessing.Paragraph paragraph1 = new DocumentFormat.OpenXml.Wordprocessing.Paragraph() { RsidParagraphAddition = "00164C17", RsidRunAdditionDefault = "00164C17" };

        //    DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties paragraphProperties1 = new DocumentFormat.OpenXml.Wordprocessing.ParagraphProperties();
        //    ParagraphStyleId paragraphStyleId1 = new ParagraphStyleId() { Val = "Footer" };

        //    paragraphProperties1.Append(paragraphStyleId1);

        //    Run run1 = new Run();
        //    Text text1 = new Text();
        //    text1.Text = "Footer";

        //    run1.Append(text1);

        //    paragraph1.Append(paragraphProperties1);
        //    paragraph1.Append(run1);

        //    footer1.Append(paragraph1);

        //    part.Footer = footer1;
        //}
        //public static string numberToAlpha(long number, bool isLower = false)
        //{
        //    string returnVal = "";
        //    char c = isLower ? 'a' : 'A';
        //    while (number >= 0)
        //    {
        //        returnVal = (char)(c + number % 26) + returnVal;
        //        number /= 26;
        //        number--;
        //    }

        //    return returnVal;
        //}
        public static string ToRomanLower(int _n)
        {
            if (_n <= 0) return "";

            int[] Nums = { 1 ,  4  ,  5 ,  9  , 10 , 40 ,
                           50,  90 , 100,  400, 500, 900, 1000};
            string[] RomanNums = {"i", "iv", "v", "ix", "x", "xl",
                          "l", "xc", "c", "cd", "d", "cm", "m"};

            string sRtn = ""; int n = _n;
            for (int i = Nums.Length - 1; i >= 0; --i)
            {
                while (n >= Nums[i])
                {
                    n -= Nums[i];
                    sRtn += RomanNums[i];
                }
            }
            return sRtn;
        }

        public static string ToRomanUpper(int _n)
        {
            if (_n <= 0) return "";

            int[] Nums = { 1 ,  4  ,  5 ,  9  , 10 , 40 ,
                           50,  90 , 100,  400, 500, 900, 1000};
            string[] RomanNums = {"I", "IV", "V", "IX", "X", "XL",
                          "L", "XC", "C", "CD", "D", "CM", "M"};

            string sRtn = ""; int n = _n;
            for (int i = Nums.Length - 1; i >= 0; --i)
            {
                while (n >= Nums[i])
                {
                    n -= Nums[i];
                    sRtn += RomanNums[i];
                }
            }
            return sRtn;
        }

        //public static string ToAlphaUpper(int n)
        //{
        //    string result = "";
        //    string[] alphasreies = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
        //    if (n > 25)
        //    {
        //        //for (int j = 1; n >= 26; j++)
        //        //{
        //        //    if (n > 25)
        //        //    {
        //        //        result=alphasreies[]
        //        //    }
        //        //}
        //    }
        //    else if(n>0 && n<27)
        //    {
        //        result = alphasreies[n - 1];
        //    }
        //    return result;

        //}

        public static String ToAlphaUpper(int column)
        {
            if (column <= 0)
            {
                return "";
            }
            if (column <= 26)
            {
                return (char)(column + 64) + "";
            }
            if (column % 26 == 0)
            {
                return ToAlphaUpper((column / 26) - 1) + ToAlphaUpper(26);
            }
            return ToAlphaUpper(column / 26) + ToAlphaUpper(column % 26);
        }

        public static String ToAlphaLower(int column)
        {
            if (column <= 0)
            {
                return "";
            }
            if (column <= 26)
            {
                return (char)(column + 96) + "";
            }
            if (column % 26 == 0)
            {
                return ToAlphaLower((column / 26) - 1) + ToAlphaLower(26);
            }
            return ToAlphaLower(column / 26) + ToAlphaLower(column % 26);
        }


        public void ConvertToPDf(string filePath, string fileName)
        {
            using (var client = new System.Net.WebClient())
            {
                var data = new NameValueCollection();
                data.Add("OutputFormat", "pdf");
                data.Add("OutputFileName", fileName); //Optional
                //!!!MANDATORY!!!
                data.Add("ApiKey", "490863597"); //API Key must be set if you purchased membership with credits. Please login to your control panel to find out your API Key http://www.convertapi.com/prices

                try
                {
                    client.QueryString.Add(data);
                    var response = client.UploadFile("http://do.convertapi.com/word2pdf", filePath);
                    var responseHeaders = client.ResponseHeaders;
                    //var path = System.IO.Path.Combine(@"C:\",);
                    var path = System.IO.Path.Combine(Server.MapPath("/QuestionPaper/Document/"), responseHeaders["OutputFileName"]);
                    System.IO.File.WriteAllBytes(path, response);
                    // Console.WriteLine("The conversion was successful! The word file {0} converted to PDF and saved at {1}", filePath, path);
                }
                catch (System.Net.WebException e)
                {
                    //Console.WriteLine("Exception Message :" + e.Message);
                    //if (e.Status == System.Net.WebExceptionStatus.ProtocolError)
                    //{
                    //    Console.WriteLine("Status Code : {0}", ((System.Net.HttpWebResponse)e.Response).StatusCode);
                    //    Console.WriteLine("Status Description : {0}", ((System.Net.HttpWebResponse)e.Response).StatusDescription);
                    //}

                }


            }
        }
        public class HtmlEntities
        {
            public HtmlEntities(string HtmlCode, string XmlCode)
            {
                // TODO: Complete member initialization
                this.HtmlCode = HtmlCode;
                this.XmlCode = XmlCode;
            }

            public string HtmlCode { get; set; }

            public string XmlCode { get; set; }

        }


        public static int PrevPassageID { get; set; }

        public static int passageFirstQuestion { get; set; }

        public static int passageLastQuestion { get; set; }




    }
}





