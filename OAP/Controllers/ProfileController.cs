﻿using HelperClasses;
using Newtonsoft.Json;
using OAP.Domain;
using OAP.Domain.ViewModel;
using OAP.Domain.ViewModel.DataTableViewModel;
using OAP.Filters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace OAP.Controllers
{
    [InitializeSimpleMembership]
    [Authorize(Roles = "SuperAdmin,CollegeAdmin,Student")]
    public class ProfileController : BaseController
    {
        //
        // GET: /Profile/
        public OAPEntities db = new OAPEntities();

        public ActionResult MyProfile()
        {
            UserProfileViewModel userProfileViewModel = new UserProfileViewModel();
            int userId = WebSecurity.GetUserId(User.Identity.Name);
            UserProfile userProfile = db.UserProfile.Find(userId);
            if (userProfileViewModel.StudentName != null)
            {
                userProfileViewModel.StudentName = userProfile.Student != null ? userProfile.Student.Name : "";
               // userProfileViewModel.StudentName = userProfile.Student.Name;
            }
            userProfileViewModel.StudentName = userProfile.Student != null ? userProfile.Student.Name : "";
            // userProfileViewModel.StudentName = userProfile.Student.Name;
            userProfileViewModel.Name = userProfile.UserName;
            userProfileViewModel.ImageUrl = userProfile.ImageUrl;
            userProfileViewModel.LogoUrl = db.Setting.Where(s => s.Name.ToLower() == "logo").Select(x => x.Value).FirstOrDefault();

            Student student = userProfile.Student != null ? userProfile.Student : new Student();
            if (student != null )
            {
                if (student.College != null)
                {
                    userProfileViewModel.SectionName = student.Section != null ? student.Section.Name : "";
                    userProfileViewModel.BranchName = student.Branch != null ? student.Branch.Name : "";
                    userProfileViewModel.CollegeName = student.College != null ? student.College.Name : "";
                }
                else
                {
                    //userProfileViewModel.CollegeName = db.Student.Where(x => x.UniversityRegisterNumber.ToLower() == userProfile.UserName.ToLower()).FirstOrDefault() != null ? db.Student.Where(x => x.UniversityRegisterNumber.ToLower() == userProfile.UserName.ToLower()).FirstOrDefault().CollegeName : "";
                    userProfileViewModel.CollegeName = db.Student.Where(x => x.UniversityRegisterNumber.ToLower() == userProfile.UserName.ToLower()).FirstOrDefault() != null ? db.Student.Where(x => x.UniversityRegisterNumber.ToLower() == userProfile.UserName.ToLower()).FirstOrDefault().EmailId : "";
                }
                userProfileViewModel.Address = student.Address;
                userProfileViewModel.UniversityRegistryNumber = student.UniversityRegisterNumber;
                userProfileViewModel.EmailId = student.EmailId;
                userProfileViewModel.PhoneNumber = student.PhoneNumber;
            }
            //else
            //{
            //    userProfileViewModel.StudentName=db.StudentRegister.Where(x=>x.UniversityRegisterNumber.ToLower()==)
            //}

            if (User.IsInRole("Student"))
            {                                                       //User is a student
                ViewBag.Layout = "~/Views/Shared/_StudentLayout.cshtml";
            }
            else
            {                                                           //User is College Admin
                ViewBag.Layout = "~/Views/Shared/_CollegeAdminLayout.cshtml";
            }
            return View(userProfileViewModel);
        }

        #region Change Profile Picture

        public ActionResult ChangeProfilePicture(HttpPostedFileBase profileImage)
        {
            if (profileImage != null)
            {                                                                           //Image selected
                string serverPath = Server.MapPath("~/Image/ProfilePicture");
                if (!Directory.Exists(serverPath))
                    Directory.CreateDirectory(serverPath);
                string guidString = Guid.NewGuid().ToString();
                profileImage.SaveAs(serverPath + "/" + guidString + "." + profileImage.FileName.Split('.')[1]);
                UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
                //string azureImageNewPath = AzureHelper.UploadToAzureStorage(serverPath + "/" + guidString + "." + profileImage.FileName.Split('.')[1], "OAP/Images/ProfileImages", false);
                if (userProfile != null)
                {
                    //userProfile.ImageUrl = azureImageNewPath;
                    db.Entry(userProfile).State = EntityState.Modified;
                    db.SaveChanges();
                }

            }
            return RedirectToAction("MyProfile");
        }

        #endregion

        #region Change Logo

        public ActionResult ChangeLogo(HttpPostedFileBase logoImage, string save)
        {
            if (save.ToLower() != "upload")
            {                                                                       //Restore to default logo
                Setting setting = db.Setting.Where(s => s.Name.ToLower() == "logo").FirstOrDefault();
                if (setting != null)
                {
                    setting.Value = "";
                    db.Entry(setting).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            else if (logoImage != null)
            {                                                                           //Image selected
                string serverPath = Server.MapPath("~/Image/Logo");
                if (!Directory.Exists(serverPath))
                    Directory.CreateDirectory(serverPath);
                string guidString = Guid.NewGuid().ToString();
                string newFile = serverPath + "/" + guidString + "." + logoImage.FileName.Split('.')[1];
                logoImage.SaveAs(newFile);

                Setting setting = db.Setting.Where(s => s.Name.ToLower() == "logo").FirstOrDefault();

                //string azureImageNewPath = AzureHelper.UploadToAzureStorage(serverPath + "/" + guidString + "." + logoImage.FileName.Split('.')[1], "OAP/Images/ProfileImages", false);

                System.IO.File.Delete(newFile);

                if (setting != null)
                {
                    //setting.Value = azureImageNewPath;
                    db.Entry(setting).State = EntityState.Modified;
                }
                else
                {
                    setting = new Setting();
                    setting.Name = "logo";
                    //setting.Value = azureImageNewPath;
                    db.Setting.Add(setting);
                }
                db.SaveChanges();

            }
            return RedirectToAction("MyProfile");
        }

        #endregion

        #region Change Theme

        public ActionResult ChangeTheme(UserProfileViewModel userProfileViewModel, string save)
        {
            if (save.ToLower() != "save")
            {                                                           //Restore selected
                Setting setting = db.Setting.Where(s => s.Name.ToLower() == "theme").FirstOrDefault();
                if (setting != null)
                {
                    setting.Value = "";
                    db.Entry(setting).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            else if (!string.IsNullOrEmpty(userProfileViewModel.Theme))
            {                                                               //Theme selected
                Setting setting = db.Setting.Where(s => s.Name.ToLower() == "theme").FirstOrDefault();
                if (setting != null)
                {                                               //Theme setting already configured
                    setting.Value = userProfileViewModel.Theme;
                    db.Entry(setting).State = EntityState.Modified;
                }
                else
                {                                                   //Create new theme
                    setting = new Setting();
                    setting.Name = "theme";
                    setting.Value = userProfileViewModel.Theme;
                    db.Setting.Add(setting);
                }
                db.SaveChanges();
            }
            return RedirectToAction("MyProfile");
        }

        #endregion

        #region Create New User

        public ActionResult CreateUser()
        {
            return View();
        }

        #endregion

        #region Change Password

        public ActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChangePassword(UserProfileViewModel userProfileViewModel)
        {
            if (string.IsNullOrEmpty(userProfileViewModel.Name))
                ModelState.AddModelError("Name", "");
            if (string.IsNullOrEmpty(userProfileViewModel.OldPassword))
                ModelState.AddModelError("OldPassword", "");
            if (string.IsNullOrEmpty(userProfileViewModel.Password))
                ModelState.AddModelError("Password", "");
            if (string.IsNullOrEmpty(userProfileViewModel.ConfirmPassword))
                ModelState.AddModelError("ConfirmPassword", "");

            if (!string.IsNullOrEmpty(userProfileViewModel.StudentName) && !string.IsNullOrEmpty(userProfileViewModel.OldPassword) && !string.IsNullOrEmpty(userProfileViewModel.Password) && !string.IsNullOrEmpty(userProfileViewModel.ConfirmPassword) && userProfileViewModel.Password.Equals(userProfileViewModel.ConfirmPassword))
            {       
                                                                    //Password and confirm password matches
                if (userProfileViewModel.Password.Equals(userProfileViewModel.ConfirmPassword))
                {
                    bool result = WebSecurity.ChangePassword(userProfileViewModel.Name, userProfileViewModel.OldPassword, userProfileViewModel.Password);
                    if (!result)
                    {                                                       //Some error occcured while changing the password
                        ViewBag.ErrorMessage = "User name/Password incorrect!";
                        //ViewBag.error = "Must be give valid Password and ConfirmPassword";
                    }
                    else
                    {                                                           //Change password success
                        return RedirectToAction("LogOff", "Account");
                    }
                }
                else
                {
                    ViewBag.ErrorMessage = "Password mismatch!";
                }
            }
            else
            {                                                                           //Password and confirm password didnt matches
               // ModelState.AddModelError("Password", "");
               // ModelState.AddModelError("ConfirmPassword", "");
                ViewBag.ErrorMessage = "Must be give valid Password and ConfirmPassword";
            }
            return View("~/Views/Profile/MyProfile.cshtml", userProfileViewModel);
        }
        #endregion

    }
}
