﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OAP.Domain;
using OAP.Domain.ViewModel;
using HelperClasses;
using Newtonsoft.Json;
using OfficeOpenXml.Style;
using System.Drawing;
using System.IO;
using OfficeOpenXml;
using System.Net.Http;
using System.Net;
using OAP.Domain.ViewModel.DataTableViewModel;
using System.Globalization;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Text.RegularExpressions;
using System.Web.Helpers;

//using DocumentFormat.OpenXml.Wordprocessing;
//using System.DivideByZeroException;

namespace OAP.Controllers
{
    [Authorize(Roles = "SuperAdmin,CollegeAdmin,Student")]
    public class ReportController : BaseController
    {

        private OAPEntities db = new OAPEntities();
        //anu
       // public static string pageLimitString = ConfigurationManager.AppSettings["pageLimit"];
        //anu
        string[] columns = { "SectionName", "TagNames", "TimeTaken", "Status" };

        //
        // GET: /Report/


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TestReport(long id = 0)
        {
            List<QuestionViewModel> questionViewModelList = GetTimeReportForAssessment(id);
            ReportViewModel reportViewModel = new ReportViewModel();
            //reportViewModel.TimeReport = timeReportModelList;
            reportViewModel.QuestionViewModelList = questionViewModelList;
            reportViewModel.CurrentQuestion = questionViewModelList.FirstOrDefault();
            long studentAnswerId = reportViewModel.CurrentQuestion.TimeReports.Count() > 0 ? reportViewModel.CurrentQuestion.TimeReports.FirstOrDefault().StudentAnswerID : 0;
            if (studentAnswerId != 0)
            {
                reportViewModel.StudentAnswer = reportViewModel.CurrentQuestion.AnswerViewModelList.Where(a => a.ID == studentAnswerId).FirstOrDefault();
            }
            reportViewModel.QuestionPaperID = id;
            reportViewModel.IsFirst = true;
            reportViewModel.IsLast = false;
            return View(reportViewModel);
        }

        [HttpPost]
        public ActionResult TestReport(ReportViewModel reportViewModel)
        {
            int index;
            List<QuestionViewModel> questionViewModelList = GetTimeReportForAssessment(reportViewModel.QuestionPaperID);
            QuestionViewModel displayedQuestion = questionViewModelList.Where(q => q.ID.Equals(reportViewModel.CurrentQuestion.ID)).FirstOrDefault();
            index = questionViewModelList.IndexOf(displayedQuestion);
            if (reportViewModel.IsNext)
            {
                reportViewModel.CurrentQuestion = questionViewModelList[index + 1];
                if (index + 1 != questionViewModelList.Count() - 1)
                {
                    reportViewModel.IsLast = false;

                }
                else
                {
                    reportViewModel.IsLast = true;
                }
                reportViewModel.IsFirst = false;
            }
            else
            {
                reportViewModel.CurrentQuestion = questionViewModelList[index - 1];
                if (index - 1 == 0)
                {
                    reportViewModel.IsFirst = true;
                }
                else
                {
                    reportViewModel.IsFirst = false;
                }
                reportViewModel.IsLast = false;
            }
            long studentAnswerId = reportViewModel.CurrentQuestion.TimeReports.Count() > 0 ? reportViewModel.CurrentQuestion.TimeReports.FirstOrDefault().StudentAnswerID : 0;
            if (studentAnswerId != 0)
            {
                reportViewModel.StudentAnswer = reportViewModel.CurrentQuestion.AnswerViewModelList.Where(a => a.ID == studentAnswerId).FirstOrDefault();
            }
            ModelState.Clear();
            questionViewModelList = questionViewModelList;
            reportViewModel.QuestionViewModelList = questionViewModelList;

            return View(reportViewModel);

        }

        #region questionPaper Time Report

        public ActionResult AssessmentTimeReport(long id = 0)
        {
            List<QuestionViewModel> questionViewModelList = GetTimeReportForAssessment(id);
            ReportViewModel reportViewModel = new ReportViewModel();
            reportViewModel.QuestionViewModelList = questionViewModelList;
            if (questionViewModelList != null && questionViewModelList.Count() > 0)
            {
                reportViewModel.CurrentQuestion = questionViewModelList.FirstOrDefault();
                long studentAnswerId = reportViewModel.CurrentQuestion.TimeReports.Count() > 0 ? reportViewModel.CurrentQuestion.TimeReports.FirstOrDefault().StudentAnswerID : 0;
                if (studentAnswerId != 0)
                {
                    reportViewModel.StudentAnswer = reportViewModel.CurrentQuestion.AnswerViewModelList.Where(a => a.ID == studentAnswerId).FirstOrDefault();
                }
            }
            reportViewModel.QuestionPaperID = id;
            reportViewModel.IsFirst = true;
            reportViewModel.IsLast = false;
            return View(reportViewModel);
        }

        public List<QuestionViewModel> GetTimeReportForAssessment(long id = 0)
        {
            List<QuestionViewModel> questionViewModelList = new List<QuestionViewModel>();
            List<ReportViewModel.TimeReportModel> timeReportModels = new List<ReportViewModel.TimeReportModel>();
            QuestionPaper questionPaper = db.QuestionPaper.Find(id);
            int userId = GetCurrentUserID();
            Student student = db.UserProfile.Find(userId).Student;
            List<Question> questionsAsked = questionPaper != null ? questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).Select(asq => asq.Question).ToList() : new List<Question>(); ;
            IEnumerable<UserAssessment> allStudentAssessments = questionPaper.UserAssessment.Where(ua => ua.UserID == userId);
            //IEnumerable<UserAssessment> studentAssessments = assessment.UserAssessment.Where(ua => ua.Assessment.ID.Equals(assessment.ID));
            foreach (UserAssessment studentAssessment in allStudentAssessments)
            {
                foreach (Question question in questionsAsked)
                {
                    timeReportModels = new List<ReportViewModel.TimeReportModel>();
                    QuestionViewModel questionViewModel = MapperHelper.MapToViewModel(question);

                    List<AssessmentSnapshot> userAssessmentSnapShotList = question.AssessmentSnapshot.Where(asp => asp.QuestionID == question.ID && asp.UserAssessment.UserID == userId && asp.UserAssessment.AssessmentID == id).ToList();
                    IQueryable<Answer> userAnsweredList = userAssessmentSnapShotList.Where(x => x.Answer != null).Select(x => x.Answer).AsQueryable();

                    List<AnswerViewModel> answerViewModelList = MapperHelper.MapToViewModelList(userAnsweredList);
                    questionViewModel.StudentAnsweredViewModelList = answerViewModelList.Skip(answerViewModelList.Count - 1).ToList();

                    if (question.AssessmentSnapshot.Count() > 0)
                    {
                        ReportViewModel.TimeReportModel timeReportModel = new ReportViewModel.TimeReportModel();
                        timeReportModel.StudentTime = question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.UserAssessment.ID == studentAssessment.ID).Count() > 0 ? question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.UserAssessment.ID == studentAssessment.ID).FirstOrDefault().TimeTaken != null ? question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.UserAssessment.ID == studentAssessment.ID).FirstOrDefault().TimeTaken.Value : 0 : 0;
                        timeReportModel.StudentAnswerID = question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.UserAssessment.ID == (studentAssessment.ID)).Count() > 0 ? question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.UserAssessment.ID == studentAssessment.ID).FirstOrDefault().Answer != null ? question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.UserAssessment.ID == studentAssessment.ID).FirstOrDefault().Answer.ID : 0 : 0;

                        //timeReportModel.StudentTime = question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.UserAssessment.ID.Equals(studentAssessment.ID)).Count() > 0 ? question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.UserAssessment.ID.Equals(studentAssessment.ID)).FirstOrDefault().TimeTaken != null ? question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.UserAssessment.ID.Equals(studentAssessment.ID)).FirstOrDefault().TimeTaken.Value : 0 : 0;
                        //timeReportModel.StudentAnswerID = question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.UserAssessment.ID.Equals(studentAssessment.ID)).Count() > 0 ? question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.UserAssessment.ID.Equals(studentAssessment.ID)).FirstOrDefault().Answer != null ? question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.UserAssessment.ID.Equals(studentAssessment.ID)).FirstOrDefault().Answer.ID : 0 : 0;
                        timeReportModel.BestTime = question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.TimeTaken != null && asp.UserAssessment.QuestionPaper.ID == questionPaper.ID && (asp.Answer != null) ? asp.Answer.IsCorrect ?? false : false).Count() > 0 ? question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.TimeTaken != null && asp.UserAssessment.QuestionPaper.ID == questionPaper.ID).Max(asp => asp.TimeTaken.Value) : 0;
                        try
                        {
                            timeReportModel.AverageTime = (question.AssessmentSnapshot.Where(asp => asp.UserAssessment.UserID == userId && asp.TimeTaken != null && asp.UserAssessment.ID == studentAssessment.ID).Sum(asp => asp.TimeTaken.Value)) / question.AssessmentSnapshot.Where(asp => asp.UserAssessment.QuestionPaper.ID == questionPaper.ID).Select(x => x.UserAssessmentID).Distinct().Count();
                        }
                        catch (DivideByZeroException dEx)
                        {
                            timeReportModel.AverageTime = 0;
                        }
                        try
                        {
                            timeReportModel.AverageRatio = (timeReportModel.AverageTime - timeReportModel.StudentTime) / timeReportModel.AverageTime;
                        }
                        catch (DivideByZeroException dEx)
                        {
                            timeReportModel.AverageRatio = 0;
                        }
                        try
                        {
                            timeReportModel.BestRatio = (timeReportModel.BestTime - timeReportModel.StudentTime) / timeReportModel.BestTime;

                        }
                        catch (DivideByZeroException dEx)
                        {
                            timeReportModel.BestRatio = 1;
                        }
                        if (timeReportModel.AverageRatio < 0)
                        {
                            timeReportModel.AverageMessage = "You can finish in " + (timeReportModel.AverageRatio * 100) + " % of Average time";
                            timeReportModel.AverageStatus = -1;
                        }
                        else if (timeReportModel.AverageRatio == 0)
                        {
                            timeReportModel.AverageMessage = "You have finished in the Average time";
                            timeReportModel.AverageStatus = 0;
                        }
                        else
                        {
                            timeReportModel.AverageMessage = "You have finsihed" + (timeReportModel.AverageRatio * 100) + "% faster than Average time";
                        }
                        if (timeReportModel.BestRatio < 0)
                        {
                            timeReportModel.BestMessage = "Best time taken has" + (timeReportModel.AverageRatio * 100) + "% faster than your time";
                            timeReportModel.BestStatus = -1;
                        }
                        else if (timeReportModel.BestRatio == 0)
                        {
                            timeReportModel.BestMessage = "Congratulations..! You have finished as the Best";
                            timeReportModel.BestStatus = 1;
                        }
                        else
                        {
                            timeReportModel.BestMessage = "Sorry No one answered correctly for the question";
                            timeReportModel.BestStatus = 0;

                        }
                        timeReportModels.Add(timeReportModel);
                    }
                    questionViewModel.QuestionPassage = question.Passage != null ? MapperHelper.MapToViewModel(question.Passage) : new PassageViewModel();
                    questionViewModel.AnswerViewModelList = question.Answer.Count() > 0 ? MapperHelper.MapToViewModelList(question.Answer.AsQueryable()) : new List<AnswerViewModel>();
                    questionViewModel.TimeReports = timeReportModels;

                    questionViewModelList.Add(questionViewModel);
                }
            }
            return questionViewModelList;
        }

        #endregion

        public void GetAnswers(long id = 0) //Code for pop up of answers
        {
            //long ID = Convert.ToInt64(id);
            AnswerViewModel answerViewModel = new AnswerViewModel();
            IQueryable<Answer> AnswerList = db.Answer.Where(x => x.QuestionID == id).AsQueryable();
            List<AnswerViewModel> answerViewModelList = MapperHelper.MapToViewModelList(AnswerList);
            Response.Write(JsonConvert.SerializeObject(answerViewModelList));
        }

        #region Question Paper Review
        public ActionResult QuestionPaperReview(long id = 0)
        {
            ViewBag.AssessmentID = id;
            return View();
        }
        public void GetQuestionPaperReview(long id = 0)
        {
            datatableRequest = new DataTableRequest(Request, columns);

            // IQueryable<Question> questions = db.Question;
            int userId = GetCurrentUserID();
            UserAssessment userAssessment = db.UserAssessment.Where(x => x.AssessmentID == id && x.UserID == userId).FirstOrDefault(); // AssessmentID is QuestionPaperID
            ReportViewModel reportViewModel = new ReportViewModel();
            QuestionPaper questionPaper = db.QuestionPaper.Find(id);
            IQueryable<Question> questions = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).Select(z => z.Question).AsQueryable();
            //foreach (var item in collection)
            //{

            //}
            //List<QuestionViewModel> questionViewModelList = MapperHelper.MapToViewModelListforReport(questions, id, userAssessment.ID);
            List<QuestionViewModel> questionViewModelList = new List<QuestionViewModel>();
            //IQueryable<AssessmentSnapshot> assessmentSnapShotList = userAssessment.AssessmentSnapshot.AsQueryable();
            foreach (Question question in questions)
            {
                IQueryable<AssessmentSnapshot> assessmentSnapShotList = question.AssessmentSnapshot.Where(a => a.UserAssessmentID == userAssessment.ID).AsQueryable();
                AssessmentSnapshot assessmentSnapShot = assessmentSnapShotList.LastOrDefault();
                if (assessmentSnapShot == null)
                {
                    assessmentSnapShot = new AssessmentSnapshot();
                }
                QuestionViewModel questionViewModel = new QuestionViewModel();
                questionViewModel.Description = question.Description;
                questionViewModel.TimeTaken = assessmentSnapShot.TimeTaken ?? 0;
                questionViewModel.QuestionID = question.ID;
                questionViewModel.AnswerDescription = assessmentSnapShot.Answer != null ? assessmentSnapShot.Answer.Description ?? "" : "";
                questionViewModel.Status = assessmentSnapShot.Answer != null ? assessmentSnapShot.Answer.IsCorrect : null;
                questionViewModelList.Add(questionViewModel);
            }
            reportViewModel.QuestionViewModelList = questionViewModelList;
            reportViewModel.QuestionPaperName = questionPaper.Name;

            int totalRecords = questions.Count();
            int filteredCount = totalRecords;

            IQueryable<QuestionViewModel> questionViewModels = questionViewModelList.AsQueryable();
            questionViewModelList = questionViewModels.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired).ToList();
            var technicalQuestionViewModelList = new List<Domain.ViewModel.TechnicalQuestionViewModel>();

            QuestionDataTableViewModel dataTableViewModel = new QuestionDataTableViewModel(questionViewModelList,technicalQuestionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);
            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }
        #endregion

        #region Student Assessment Report

        public ActionResult StudentAssessmentReport(int id)
        {
            QuestionPaper questionPaper = db.QuestionPaper.Find(id);
            UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            UserAssessment userAssessment = userProfile.UserAssessment.Where(ua => ua.AssessmentID == id).FirstOrDefault();
            List<QuestionPaperSectionQuestion> questionPaperSectionQuestionList = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).ToList();

            ReportViewModel.QuestionPaperReportModel assessmentReportModel = new ReportViewModel.QuestionPaperReportModel();
            //assessmentReportModel.StudentName = userProfile.UserName;
            assessmentReportModel.StudentName = userProfile.Student.Name;//Need Student Name
            assessmentReportModel.RegisterNumber = userProfile.Student.UniversityRegisterNumber;
            assessmentReportModel.QuestionPaperName = questionPaper.Name;

            int totalQuestions = questionPaper.QuestionPaperSection.SelectMany(ass => ass.QuestionPaperSectionQuestion).Count();
            assessmentReportModel.TotalQuestions = totalQuestions;

            int attendedQuestions = userAssessment.AssessmentSnapshot.Select(x => x.Question).Distinct().Count();
            assessmentReportModel.AttentedQuestions = attendedQuestions;

            //int correctAnswers = userProfile.UserAssessment.Where(ua=>ua.AssessmentID==id && ua.AssessmentSnapshot.Where(ass=>ass.Answer.IsCorrect??false).Count()>0).Select(x => x.AssessmentSnapshot).Count();
            int correctAnswersCount = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();        //Getting correct answer count for single select question

            int totalTimeTakenForCorrectAnswer = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Sum(x => x.TimeTaken ?? 0);      //Get the time of the single select question type


            List<Question> multiSelectQuestionList = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionType.Name.ToLower().Equals("multi select")).Select(x => x.Question).Distinct().ToList();
            foreach (Question question in multiSelectQuestionList)
            {
                List<Answer> correctAnswerList = question.Answer.Where(a => a.IsCorrect ?? false).ToList();
                List<Answer> userAnsweredList = question.AssessmentSnapshot.Where(asn => (asn.Answer != null) ? asn.Answer.IsCorrect ?? false : false).Select(x => x.Answer).ToList();
                if (correctAnswerList.Except(userAnsweredList).Count() > 0 || userAnsweredList.Except(correctAnswerList).Count() > 0)
                {                                                                       //some of the answers are incorrect

                }
                else
                {                                                                       //All the answers are correct
                    correctAnswersCount++;
                    totalTimeTakenForCorrectAnswer += question.AssessmentSnapshot.FirstOrDefault().TimeTaken ?? 0;
                }
            }

            assessmentReportModel.CorrectAnswers = correctAnswersCount;

            assessmentReportModel.TimeTakenForCorrectAnswer = totalTimeTakenForCorrectAnswer;

            decimal marksPercentage = 0;
            if (totalQuestions != 0)
                marksPercentage = ((decimal)correctAnswersCount / (decimal)totalQuestions) * 100;
            assessmentReportModel.MarksPercentage = Math.Round(marksPercentage, 2);

            decimal hitPercentage = 0;
            if (attendedQuestions != 0)
                hitPercentage = ((decimal)correctAnswersCount / (decimal)attendedQuestions) * 100;
            assessmentReportModel.HitPercentage = Math.Round(hitPercentage, 2);

            float avgerageTimeTakenForCorrectAnswer = 0;
            if (correctAnswersCount > 0)
                avgerageTimeTakenForCorrectAnswer = (totalTimeTakenForCorrectAnswer / correctAnswersCount);
            assessmentReportModel.AverageTimeTakenForCorrectAnswer = avgerageTimeTakenForCorrectAnswer;

            List<ReportViewModel.TagReportModel> tagReportModelList = new List<ReportViewModel.TagReportModel>();

            List<Tag> tagsList = questionPaper.QuestionPaperSection.SelectMany(ass => ass.QuestionPaperSectionQuestion).SelectMany(x => x.Question.QuestionTags).Select(x => x.Tag).Distinct().ToList();

            assessmentReportModel.Tags = string.Join(",", tagsList.Select(x => x.Name).ToArray());
            assessmentReportModel.Tags = "Max," + assessmentReportModel.Tag;

            foreach (Tag tags in tagsList)
            {
                ReportViewModel.TagReportModel tagReportModel = new ReportViewModel.TagReportModel();

                int totalQuestionsUnderThisTag = questionPaperSectionQuestionList.Where(asq => asq.Question.QuestionTags.Where(qt => qt.TagID == tags.ID).Count() > 0).Count();
                tagReportModel.TotalQuestions = totalQuestionsUnderThisTag;

                int attendedQuestionsInThisTag = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionTags.Where(qt => qt.TagID == tags.ID).Count() > 0).Select(x => x.Question).Distinct().Count();
                tagReportModel.AttendedQuestions = attendedQuestionsInThisTag;

                int correctAnswerUnderThisTag = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == tags.ID).Count() > 0) && (ass.Answer.IsCorrect ?? false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();     //Get only the single select type questions 
                List<Question> multiSelectTypeQuestionList = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == tags.ID).Count() > 0) && (ass.Question.QuestionType.Name.ToLower().Equals("multi select"))).Select(x => x.Question).Distinct().ToList();
                foreach (Question multiSelectQuestion in multiSelectTypeQuestionList)
                {
                    List<Answer> correctAnswerList = multiSelectQuestion.Answer.Where(a => a.IsCorrect ?? false).ToList();
                    List<Answer> userAnsweredList = multiSelectQuestion.AssessmentSnapshot.Select(x => x.Answer).ToList();
                    if (correctAnswerList.Except(userAnsweredList).Count() > 0 || userAnsweredList.Except(correctAnswerList).Count() > 0)
                    {                                                                       //some of the answers are incorrect

                    }
                    else
                    {                                                                       //All the answers are correct
                        correctAnswerUnderThisTag++;
                    }
                }
                tagReportModel.CorrectAnswers = correctAnswerUnderThisTag;

                float correctAnswerPercentage = 0;
                if (totalQuestionsUnderThisTag != 0)
                correctAnswerPercentage = ((float)correctAnswerUnderThisTag / (float)totalQuestionsUnderThisTag) * 100;
                tagReportModel.CorrectAnswerPercentage = (int)Math.Ceiling(correctAnswerPercentage);

                string tagName = tags.Name;
                tagReportModel.Name = tagName;

                tagReportModelList.Add(tagReportModel);
            }
            assessmentReportModel.TagReportModelList = tagReportModelList;

            return View(assessmentReportModel);
        }

        #endregion

        #region BatchAssessment Report

        public ActionResult BatchAssessmentReportFilter()
        {
            UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            List<BatchViewModel> batchViewModelList = new List<BatchViewModel>();
            IQueryable<Batch> batches = null;
            if (userProfile != null)
            {
                batches = db.Batch.Distinct();
                batchViewModelList = MapperHelper.MapToViewModelList(batches);
            }

            ViewBag.Batches = batches.OrderBy(x => x.Name);
            ViewBag.Assessments = new List<SelectListItem>(); ;
            return View(new BatchViewModel());
        }

        public ActionResult GenerateAssessmentReport(string batchID = "", string assessmentID = "")
        {
            ReportViewModel reportViewModel = new ReportViewModel();
            List<BatchViewModel> batchViewModelList = new List<BatchViewModel>();
            UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            if (!string.IsNullOrEmpty(batchID))
            {                                                               //Batch selected
                int _batchID = Convert.ToInt16(batchID);
                Batch batch = db.Batch.Find(_batchID);
                batchViewModelList.Add(GetBatchDetails(batch, assessmentID));
            }
            else
            {                                                                   //All selected from batch dropdown
                List<Batch> batchList = db.Batch.Where(b => b.College.UserProfileCollege.Where(upc => upc.UserProfileID == userProfile.UserId).Count() > 0).Distinct().ToList();
                foreach (Batch batch in batchList)
                {
                    BatchViewModel batchViewModel = GetBatchDetails(batch);
                    batchViewModelList.Add(batchViewModel);
                }
            }
            reportViewModel.BatchViewModelList = batchViewModelList;
            return View("~/Views/Report/BatchAssessmentReport.cshtml", reportViewModel);
        }

        public BatchViewModel GetBatchDetails(Batch batch, string assessmentID = "")
        {
            BatchViewModel batchViewModel = MapperHelper.MapToViewModel(batch);
            List<QuestionPaper> assessmentList = new List<QuestionPaper>();
            List<long> assessmentIDList = new List<long>();
            if (!string.IsNullOrEmpty(assessmentID))
            {
                assessmentIDList = assessmentID.Split(',').Select(long.Parse).ToList();
                //assessmentIDList.Add(batch.BatchAssessment.Where())
            }
            else
            {
                assessmentIDList = batch.BatchAssessment.Select(x => x.AssessmentID).ToList();
            }
            if (batch != null)
            {
                List<Student> studentList = batch.BatchStudent.Select(x => x.Student).ToList();
                List<StudentViewModel> studentViewModelList = new List<StudentViewModel>();

                foreach (Student student in studentList)
                {
                    StudentViewModel studentViewModel = MapperHelper.MapToViewModel(student);
                    studentViewModel.TakenTestCount = student.UserProfile.Where(up => up.UserAssessment.Where(ua => ua.AssessmentID != null && assessmentIDList.Contains(ua.AssessmentID.Value)).Count() > 0).Count();
                    studentViewModel.RemainingTestCount = assessmentList.Where(al => al.UserAssessment.Where(ua => ua.UserID != student.UserProfile.FirstOrDefault().UserId).Count() > 0).Count();
                    studentViewModelList.Add(studentViewModel);
                }
                batchViewModel.StudentViewModelList = studentViewModelList;
            }
            return batchViewModel;
        }

        #endregion
        #region Assessment Bargraph

        public ActionResult AssessmentReport()
        {
            UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            if (userProfile != null)
            {                                                                                               //User logged in
            }
            else
            {                                                                                               //User not logged in

            }
            return View();
        }

        #endregion

        #region Batch Pass Percentage with one Assessment

        public ActionResult BatchPassPercentageReportFilter()
        {
            IQueryable<QuestionPaper> questionPaper = GetAssessmentsByUser();
            if (questionPaper != null)
                ViewBag.QuestionPapers = questionPaper;
            else
                ViewBag.QuestionPapers = new List<QuestionPaper>().AsQueryable();
            ViewBag.Batches = new List<Batch>().AsQueryable();
            return View();
        }

        public ActionResult GenerateBatchPercentageReport(int assessmentID, string selectedBatchesString)
        {
            UserProfile loggedInUserProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();

            QuestionPaper questionPaper = new QuestionPaper();
            if (assessmentID > 0)
                questionPaper = db.QuestionPaper.Find(assessmentID);

            //string selectedBatchesString = Request.QueryString["BatchIDString"];

            List<int> selectedBatchIDList = new List<int>();
            List<Batch> batchList = new List<Batch>();

            if (!string.IsNullOrEmpty(selectedBatchesString) && selectedBatchesString.ToLower() == "all")
            {
                if (User.IsInRole("SuperAdmin"))
                {                                                       //User is admin. Get all batches of the assessment.
                    batchList = questionPaper.BatchAssessment.Select(x => x.Batch).ToList();
                }
                if (User.IsInRole("CollegeAdmin"))
                {                                                       //User is admin. Get all batches of the assessment.
                    batchList = questionPaper.BatchAssessment.Select(x => x.Batch).ToList();
                }
                else if (User.IsInRole("InstituteAdmin"))
                {
                    List<long> collegeAdminColleges = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                    batchList = questionPaper.BatchAssessment.Select(x => x.Batch).Where(b => b.BatchStudent.Where(bs => (collegeAdminColleges.Contains(bs.Student.CollegeID ?? 0))).Count() > 0).ToList();
                }
            }
            else if (!string.IsNullOrEmpty(selectedBatchesString))
            {
                selectedBatchIDList = selectedBatchesString.Split(',').Select(int.Parse).ToList();
                if (User.IsInRole("SuperAdmin"))
                {
                    batchList = questionPaper.BatchAssessment.Where(ba => (selectedBatchIDList.Contains(ba.BatchID))).Select(x => x.Batch).ToList();
                }
                if (User.IsInRole("CollegeAdmin"))
                {
                    batchList = questionPaper.BatchAssessment.Where(ba => (selectedBatchIDList.Contains(ba.BatchID))).Select(x => x.Batch).ToList();
                }
                else if (User.IsInRole("InstituteAdmin"))
                {
                    List<long> collegeAdminColleges = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                    batchList = questionPaper.BatchAssessment.Where(ba => (selectedBatchIDList.Contains(ba.BatchID))).Select(x => x.Batch).Where(b => b.BatchStudent.Where(bs => (collegeAdminColleges.Contains(bs.Student.CollegeID ?? 0))).Count() > 0).ToList();
                }
            }

            ReportViewModel reportViewModel = GetReportViewModelForBatchPercentageReport(questionPaper, batchList, loggedInUserProfile);
            return View("~/Views/Report/BatchPassPercentageReport.cshtml", reportViewModel);
        }

        public ReportViewModel GetReportViewModelForBatchPercentageReport(QuestionPaper questionPaper, List<Batch> batchList, UserProfile loggedInUserProfile)
        {
            ReportViewModel reportViewModel = new ReportViewModel();
            List<BatchViewModel> batchViewModelList = new List<BatchViewModel>();
            QuestionPaperViewModel assessmentViewModel = MapperHelper.MapToViewModel(questionPaper);
            reportViewModel.questionPaperViewModel = assessmentViewModel;
            int attentedStudentsCount = 0;

            foreach (Batch batch in batchList)
            {
                BatchViewModel batchViewModel = MapperHelper.MapToViewModel(batch);
                List<Student> studentList = new List<Student>();
                if (User.IsInRole("SuperAdmin"))
                {
                    studentList = batch.BatchStudent.Where(bs => bs.Student.UserProfile.FirstOrDefault().UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Count() > 0).Select(x => x.Student).Distinct().ToList();
                }
                if (User.IsInRole("CollegeAdmin"))
                {
                    studentList = batch.BatchStudent.Where(bs => bs.Student.UserProfile.FirstOrDefault().UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Count() > 0).Select(x => x.Student).Distinct().ToList();
                }
                else if (User.IsInRole("InstituteAdmin"))
                {
                    List<long> userProfileCollegeIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                    studentList = batch.BatchStudent.Where(bs => (userProfileCollegeIDList.Contains(bs.Student.CollegeID ?? 0)) && bs.Student.UserProfile.FirstOrDefault().UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Count() > 0).Select(x => x.Student).Distinct().ToList();
                }
                List<UserProfile> studentUserProfileList = studentList.SelectMany(x => x.UserProfile).Distinct().ToList();

                decimal cutOffMark = questionPaper.CutOffMark ?? 0;
                int passCount = 0;
                int failedCount = 0;
                int totalCount = 0;
                decimal passPercentage = 0;
                if (cutOffMark > 0)
                {
                    passCount = studentUserProfileList.Where(stup => stup.UserAssessment.Where(ua => (ua.AssessmentID == questionPaper.ID) && (ua.Marks >= cutOffMark)).Count() > 0).Count();
                    failedCount = studentUserProfileList.Where(stup => stup.UserAssessment.Where(ua => (ua.AssessmentID == questionPaper.ID) && (ua.Marks < cutOffMark)).Count() > 0).Count();
                    totalCount = passCount + failedCount;
                    if (totalCount > passCount)
                    {
                        passPercentage = ((decimal)passCount / (decimal)totalCount) * (decimal)100;
                    }
                    batchViewModel.PassCount = passCount;
                    batchViewModel.FailedCount = failedCount;
                    batchViewModel.PassPercentage = passPercentage;
                    batchViewModel.AttentedStudentsCount = totalCount;
                    batchViewModel.IsCutOffMarkSpecified = true;
                    attentedStudentsCount = attentedStudentsCount + totalCount;
                }
                else
                {
                    batchViewModel.IsCutOffMarkSpecified = false;
                }
                batchViewModelList.Add(batchViewModel);
            }

            reportViewModel.BatchViewModelList = batchViewModelList;
            reportViewModel.AttentedStudentsCount = attentedStudentsCount;
            return reportViewModel;
        }

        //Response.ContentType = "application/vnd.ms-excel";        
        //Response.AddHeader("Content-Disposition", "attachment; filename=test.xls;");                
        //StringWriter stringWrite = new StringWriter();        
        //HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);        
        //dgrExport.DataSource = dtExport;        
        //dgrExport.DataBind();
        //dgrExport.RenderControl(htmlWrite);
        //string headerTable = @"<Table><tr><td><img src=""D:\\Folder\\1.jpg"" \></td></tr></Table>";
        //Response.Write(headerTable);
        //Response.Write(stringWrite.ToString());        
        //Response.End();
        public List<Uri> FetchLinksFromSource(string htmlSource)
        {
            List<Uri> links = new List<Uri>();
            string regexImgSrc = @"<img[^>]*?src\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?>";
            MatchCollection matchesImgSrc = Regex.Matches(htmlSource, regexImgSrc, RegexOptions.IgnoreCase | RegexOptions.Singleline);
            foreach (Match m in matchesImgSrc)
            {
                string href = m.Groups[1].Value;
                //links.Add(new Uri(href));
                try
                {
                    links.Add(new Uri(href));
                }
                catch
                {
                }
            }
            return links;
        }

        [ValidateInput(false)]
        public ActionResult GenerateBatchPercentageReportExcel()
         {
           // string html = htmlvalue;
           string html = Session["HtmlContent"].ToString();
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=ChartExport.xls");
            Response.ContentType = "application/vnd.ms-excel";
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            //Chart.RenderControl(hw);
            string src = Regex.Match(sw.ToString(), "<img.+?src=[\"'](.+?)[\"'].+?>", RegexOptions.IgnoreCase).Groups[1].Value;
            string img = string.Format("<img src = '{0}{1}' />", Request.Url.GetLeftPart(UriPartial.Authority), src);

            Table table = new Table();
            TableRow row = new TableRow();
            row.Cells.Add(new TableCell());
            row.Cells[0].Width = 200;
            row.Cells[0].HorizontalAlign = HorizontalAlign.Center;
            row.Cells[0].Controls.Add(new Label { Text = "Fruits Distribution (India)", ForeColor = Color.Red });
            table.Rows.Add(row);
            row = new TableRow();
            row.Cells.Add(new TableCell());
            row.Cells[0].Controls.Add(new Literal { Text = img });
            table.Rows.Add(row);
            sw = new StringWriter();
            hw = new HtmlTextWriter(sw);
            table.RenderControl(hw);
            Response.Output.Write(html);
            Response.Write(hw);

            //  Response.Write(sw.ToString());
            Response.Flush();
            Response.End();
            return View();
        }



        //[ValidateInput(false)]
        //public ActionResult GenerateBatchPercentageReportExcel()
        //{



        //    //string html1 = htmlvalue;
        //    //string html = Session["HtmlContent"].ToString();
        //    ////goExcelReport();
        //    //Response.Clear();
        //    //Response.AddHeader("content-disposition", "attachment;filename=CostingIndividualReport.xls");
        //    //Response.Charset = "";
        //    //// If you want the option to open the Excel file without saving then 
        //    //// comment out the line below  
        //    //// Response.Cache.SetCacheability(HttpCacheability.NoCache); 
        //    //Response.ContentType = "application/vnd.xls";
        //    //System.IO.StringWriter stringWrite = new System.IO.StringWriter();
        //    //System.Web.UI.HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
        //    ////Reporttxt.RenderControl(htmlWrite);
        //    ////htmlWrite.InnerWriter.NewLine=html;
        //    //Response.Output.Write(html);
        //    //Response.Write(htmlWrite);
        //    //Response.End();
        //    //return View();
        //}
        string htmlvalue;
        [ValidateInput(false)]
        public void SaveData(string html)
        {
            // = html;
           // GenerateBatchPercentageReportExcel(html);
            Session["HtmlContent"] = html;
        }

        #endregion
        

        #region Batch Radar Report //Kalyan
        public ActionResult BatchPassPercentageRadarReportFilter() //Gets actionresult control from Layout; Creates Viewbags which're used in Views/Report/BatchPassPercentageRadarReportFilter; C-V
        {
            OAP.Domain.UserProfile userProfile = db.UserProfile.Where(x => x.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            if (userProfile != null)
            {
                //bool test = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                ViewBag.CanScheduleRetest = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
            }
            ViewBag.College = db.College.GroupBy(x => x.Name).Select(x => x.FirstOrDefault());
            ViewBag.Batches = new List<BatchViewModel>();
            
            ReportViewModel reportviewmodel = new ReportViewModel();

            //reportviewmodel.BatchViewModelList = batchViewModelList;
            return View(reportviewmodel);
        }
        public ActionResult BatchPassPercentageRadarReport(string college, string batch) //parameters are keyvalues from GetBatchRadarReport() in Report.js ; C-VM
        {
            ReportViewModel reportViewModel = new ReportViewModel();

            OAP.Domain.UserProfile userProfile = db.UserProfile.Where(x => x.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            if (userProfile != null)
            {
                //bool test = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                ViewBag.CanScheduleRetest = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
            }
         
            reportViewModel.CollegeID = Convert.ToInt32(college);
            List<string> batchIDList = batch.Split(',').ToList();
            reportViewModel.BatchListString = batchIDList;
            reportViewModel.BatchIDString = batch;
            return View(reportViewModel);
        }

        public void GetBatchPassPercentageRadarReport(string college, string batch) // 29/07/2015 ; Method to calc the passpercentage
        {
            string result = "";
            long collegeID = 0;
            List<decimal> PrecorrectAnswerPercentageList = new List<decimal>();
            List<decimal> PostcorrectAnswerPercentageList = new List<decimal>();
            if (!string.IsNullOrEmpty(batch) && !string.IsNullOrEmpty(college))
            {
                //college = Request.QueryString["college"]; //batch = Request.QueryString["batch"];
                collegeID = Convert.ToInt32(college);
                //}
                //if (!string.IsNullOrEmpty(batch))
                //{
                List<long> batchIDList = batch.Split(',').ToList().Select(long.Parse).ToList();
                // 29/07/2015 ; List<ReportViewModel.QuestionPaperReportModel> resultBatchViewModelList = new List<ReportViewModel.QuestionPaperReportModel>();
                //foreach (int batchID in batchIDList)
                //{
                //    BatchAssessment batchAssessment = db.BatchAssessment.Where(b => b.BatchID == batchID).FirstOrDefault();
                //}
                IQueryable<BatchAssessment> batchAssessments = db.BatchAssessment.Where(x => batchIDList.Contains(x.BatchID));
                IQueryable<Batch> batches = batchAssessments.Select(x => x.Batch).Distinct();
                List<RadarReportModel> radarReportModelList = new List<RadarReportModel>();
                List<string> WholetagsList = new List<string>();
                //List<decimal> correctAnswerPercentageList;
                foreach (Batch qpBatch in batches)
                {
                    List<TagViewModel> tagViewModelList = new List<TagViewModel>();
                    RadarReportModel radarReportModel = new RadarReportModel();

                    IQueryable<ProgramBatch> programBatches = qpBatch.ProgramBatch.AsQueryable();

                    if (programBatches.Count() > 0)
                    {
                        ProgramBatch programBatch = programBatches.LastOrDefault();
                        IQueryable<InstituteProgram> institutePrograms = db.InstituteProgram.Where(i => i.ProgramID == programBatch.ProgramID && i.InstituteID == programBatch.Batch.CollegeID).AsQueryable();
                        if (institutePrograms.Count() > 0)
                        {
                            InstituteProgram instituteProgram = institutePrograms.FirstOrDefault();
                            radarReportModel.name = qpBatch.Name;
                            //radarReportModel.pointPlacement = "on";
                            IQueryable<QuestionPaper> questionPapers;
                            IQueryable<UserProfile> studentUserProfileList;
                            IQueryable<QuestionPaperSectionQuestion> questionPaperQuestion;
                            ReportViewModel.QuestionPaperReportModel resultBatchViewModel = new ReportViewModel.QuestionPaperReportModel();
                            IQueryable<ReportViewModel.QuestionPaperReportModel> batchViewModelList;
                            IQueryable<Question> questionList;
                            IQueryable<QuestionTags> questionTagList;
                            IQueryable<Tag> tagsList;

                            #region PreTraining

                            questionPapers = batchAssessments.Where(x => x.Batch.ID == qpBatch.ID && x.StartDateTime <= instituteProgram.StartDate).Select(x => x.QuestionPaper);
                            if (questionPapers.Count() > 0)
                            {
                                studentUserProfileList = batchAssessments.Where(x => x.Batch.ID == qpBatch.ID).SelectMany(x => x.Batch.BatchStudent).SelectMany(x => x.Student.UserProfile).Distinct();
                                questionPaperQuestion = questionPapers.SelectMany(x => x.QuestionPaperSection).SelectMany(x => x.QuestionPaperSectionQuestion);
                                questionList = questionPaperQuestion.Select(x => x.Question);
                                questionTagList = questionList.SelectMany(x => x.QuestionTags);
                                tagsList = questionTagList.Select(x => x.Tag).GroupBy(x => x.ID).Select(x => x.FirstOrDefault());
                                //List<decimal> correctAnswerPercentageList = new List<decimal>();
                                foreach (Tag tag in tagsList)
                                {

                                    TagViewModel tagViewModel = new TagViewModel();
                                    //List<Question> questionUnderThisAssessment = tag.QuestionTags.Where(qt => qt.Tag.ID == tag.ID && qt.Question.AssessmentSnapshot.Where(ass => ass.UserAssessment.UserProfile.UserProfileCollege.Where(upc => upc.CollegeID == collegeID).Count() > 0).Count() > 0).Select(x => x.Question).ToList();
                                    IQueryable<Question> questionUnderThisAssessment = tag.QuestionTags.Where(qt => qt.Tag.ID == tag.ID).Select(x => x.Question).AsQueryable();
                                    if (questionUnderThisAssessment.Count() > 0)
                                    {
                                        questionUnderThisAssessment = questionUnderThisAssessment.Where(qt => qt.AssessmentSnapshot.Where(ass => ass.UserAssessment.UserProfile.Student.CollegeID == collegeID).Count() > 0);
                                        //int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => qua.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID == qua.AssessmentSnapshot.Where(ass => ass.Question.ID == qua.ID).FirstOrDefault().AnswerID).Count();

                                        List<int> correctAnsweredList = new List<int>();
                                        correctAnsweredList = questionUnderThisAssessment.Select(x => x.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault() != null ? x.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID : 0).ToList();
                                        List<int> userAnsweredList = questionUnderThisAssessment.SelectMany(x => x.AssessmentSnapshot.Where(ass => ass.QuestionID == x.ID).Select(ass => ass.AnswerID ?? 0)).Distinct().ToList();

                                        //int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => qua.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID == qua.AssessmentSnapshot.).Count();
                                        int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => userAnsweredList.Contains(qua.Answer.Where(a => a.IsCorrect ?? false).Select(x => x.ID).FirstOrDefault())).Count();
                                        //int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => correctAnsweredList.Contains(qua.AssessmentSnapshot.Where(ass=>ass.QuestionID==qua.ID && qua.Answer))).Count();
                                        decimal correctAnswerPercentage = (decimal)((correctAnsweredQuestions / (decimal)((userAnsweredList.Count() != 0 ? questionUnderThisAssessment.Count() : 1))) * 100);
                                        tagViewModel.ID = tag.ID;
                                        tagViewModel.Name = tag.Name;
                                        tagViewModel.PreTrainingValue = correctAnswerPercentage;
                                        tagViewModelList.Add(tagViewModel);
                                        //WholetagsList.Add(tag.Name);
                                        //PrecorrectAnswerPercentageList.Add(correctAnswerPercentage);

                                    }

                                }
                            }
                            #endregion
                            #region PostTraining

                            questionPapers = batchAssessments.Where(x => x.Batch.ID == qpBatch.ID && x.EndDateTime > instituteProgram.EndDate).Select(x => x.QuestionPaper);
                            if (questionPapers.Count() > 0)
                            {
                                studentUserProfileList = batchAssessments.Where(x => x.Batch.ID == qpBatch.ID).SelectMany(x => x.Batch.BatchStudent).SelectMany(x => x.Student.UserProfile).Distinct();
                                questionPaperQuestion = questionPapers.SelectMany(x => x.QuestionPaperSection).SelectMany(x => x.QuestionPaperSectionQuestion);
                                questionList = questionPaperQuestion.Select(x => x.Question);
                                questionTagList = questionList.SelectMany(x => x.QuestionTags);
                                tagsList = questionTagList.Select(x => x.Tag).GroupBy(x => x.ID).Select(x => x.FirstOrDefault());

                                //List<decimal> correctAnswerPercentageList = new List<decimal>();
                                foreach (Tag tag in tagsList)
                                {
                                    //List<Question> questionUnderThisAssessment = tag.QuestionTags.Where(qt => qt.Tag.ID == tag.ID && qt.Question.AssessmentSnapshot.Where(ass => ass.UserAssessment.UserProfile.UserProfileCollege.Where(upc => upc.CollegeID == collegeID).Count() > 0).Count() > 0).Select(x => x.Question).ToList();
                                    TagViewModel tagViewModel = new TagViewModel();
                                    IQueryable<Question> questionUnderThisAssessment = tag.QuestionTags.Where(qt => qt.Tag.ID == tag.ID).Select(x => x.Question).AsQueryable();
                                    if (questionUnderThisAssessment.Count() > 0)
                                    {
                                        questionUnderThisAssessment = questionUnderThisAssessment.Where(qt => qt.AssessmentSnapshot.Where(ass => ass.UserAssessment.UserProfile.Student.CollegeID == collegeID).Count() > 0);
                                        //int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => qua.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID == qua.AssessmentSnapshot.Where(ass => ass.Question.ID == qua.ID).FirstOrDefault().AnswerID).Count();

                                        List<int> correctAnsweredList = new List<int>();
                                        correctAnsweredList = questionUnderThisAssessment.Select(x => x.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault() != null ? x.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID : 0).ToList();
                                        List<int> userAnsweredList = questionUnderThisAssessment.SelectMany(x => x.AssessmentSnapshot.Where(ass => ass.QuestionID == x.ID).Select(ass => ass.AnswerID ?? 0)).Distinct().ToList();

                                        //int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => qua.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID == qua.AssessmentSnapshot.).Count();
                                        int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => userAnsweredList.Contains(qua.Answer.Where(a => a.IsCorrect ?? false).Select(x => x.ID).FirstOrDefault())).Count();
                                        //int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => correctAnsweredList.Contains(qua.AssessmentSnapshot.Where(ass=>ass.QuestionID==qua.ID && qua.Answer))).Count();
                                        decimal correctAnswerPercentage = (decimal)((correctAnsweredQuestions / (decimal)((userAnsweredList.Count() != 0 ? questionUnderThisAssessment.Count() : 1))) * 100);
                                        //PostcorrectAnswerPercentageList.Add(correctAnswerPercentage);
                                        //WholetagsList.Add(tag.Name);
                                        tagViewModel = tagViewModelList.Where(t => t.ID == tag.ID).FirstOrDefault();
                                        if (tagViewModel != null)
                                        {
                                            tagViewModel.PostTrainingValue = correctAnswerPercentage;
                                        }
                                        else
                                        {
                                            tagViewModel = new TagViewModel();
                                            tagViewModel.ID = tag.ID;
                                            tagViewModel.Name = tag.Name;
                                            tagViewModel.PreTrainingValue = 0;
                                            tagViewModel.PostTrainingValue = correctAnswerPercentage;
                                            tagViewModelList.Add(tagViewModel);
                                        }
                                    }

                                }
                            }
                            #endregion
                            //WholetagsList = WholetagsList.Distinct().ToList();
                            //if (PrecorrectAnswerPercentageList.Count() < WholetagsList.Count())
                            //{
                            //    int excess = WholetagsList.Count() - PrecorrectAnswerPercentageList.Count();
                            //    for (int i = 0; i < excess; i++)
                            //    {
                            //        PrecorrectAnswerPercentageList.Add(0);
                            //    }
                            //}
                            //if (PostcorrectAnswerPercentageList.Count() < WholetagsList.Count())
                            //{
                            //    int excess = WholetagsList.Count() - PostcorrectAnswerPercentageList.Count();
                            //    for (int i = 0; i < excess; i++)
                            //    {
                            //        PostcorrectAnswerPercentageList.Add(0);
                            //    }
                            //}
                        }
                    }

                    radarReportModel.TagViewModelList = tagViewModelList;
                    //radarReportModel.Labels = WholetagsList;
                    //radarReportModel.PreTrainingdata = PrecorrectAnswerPercentageList;
                    //radarReportModel.PostTrainingdata = PostcorrectAnswerPercentageList;
                    radarReportModelList.Add(radarReportModel);     
                }
                OAP.Domain.UserProfile userProfile = db.UserProfile.Where(x => x.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
                if (userProfile != null)
                {
                    //bool test = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                    ViewBag.CanScheduleRetest = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                }
                      result = JsonConvert.SerializeObject(radarReportModelList);
            }
            Response.Write(result);
        }


        public List<RadarReportModel> ExportBatchPassPercentageRadarReport(string college, string batch, string Format = "") // 29/07/2015 ; Method to calc the passpercentage
        {
            string result = "";
            long collegeID = 0;
            List<decimal> PrecorrectAnswerPercentageList = new List<decimal>();
            List<decimal> PostcorrectAnswerPercentageList = new List<decimal>();
            List<RadarReportModel> radarReportModelList = new List<RadarReportModel>();
            if (!string.IsNullOrEmpty(batch) && !string.IsNullOrEmpty(college))
            {
                //college = Request.QueryString["college"]; //batch = Request.QueryString["batch"];
                collegeID = Convert.ToInt32(college);
                //}
                //if (!string.IsNullOrEmpty(batch))
                //{
                List<long> batchIDList = batch.Split(',').ToList().Select(long.Parse).ToList();
                // 29/07/2015 ; List<ReportViewModel.QuestionPaperReportModel> resultBatchViewModelList = new List<ReportViewModel.QuestionPaperReportModel>();
                //foreach (int batchID in batchIDList)
                //{
                //    BatchAssessment batchAssessment = db.BatchAssessment.Where(b => b.BatchID == batchID).FirstOrDefault();
                //}
                IQueryable<BatchAssessment> batchAssessments = db.BatchAssessment.Where(x => batchIDList.Contains(x.BatchID));
                IQueryable<Batch> batches = batchAssessments.Select(x => x.Batch).Distinct();

                List<string> WholetagsList = new List<string>();
                //List<decimal> correctAnswerPercentageList;
                foreach (Batch qpBatch in batches)
                {
                    List<TagViewModel> tagViewModelList = new List<TagViewModel>();
                    RadarReportModel radarReportModel = new RadarReportModel();

                    IQueryable<ProgramBatch> programBatches = qpBatch.ProgramBatch.AsQueryable();

                    if (programBatches.Count() > 0)
                    {
                        ProgramBatch programBatch = programBatches.LastOrDefault();
                        IQueryable<InstituteProgram> institutePrograms = db.InstituteProgram.Where(i => i.ProgramID == programBatch.ProgramID && i.InstituteID == programBatch.Batch.CollegeID).AsQueryable();
                        if (institutePrograms.Count() > 0)
                        {
                            InstituteProgram instituteProgram = institutePrograms.FirstOrDefault();
                            radarReportModel.name = qpBatch.Name;
                            //radarReportModel.pointPlacement = "on";
                            IQueryable<QuestionPaper> questionPapers;
                            List<UserProfile> studentUserProfileList = new List<UserProfile>();
                            List<QuestionPaperSectionQuestion> questionPaperQuestion = new List<QuestionPaperSectionQuestion>();
                            ReportViewModel.QuestionPaperReportModel resultBatchViewModel = new ReportViewModel.QuestionPaperReportModel();
                            List<ReportViewModel.QuestionPaperReportModel> batchViewModelList = new List<ReportViewModel.QuestionPaperReportModel>();
                            List<Question> questionList = new List<Question>();
                            List<QuestionTags> questionTagList = new List<QuestionTags>();
                            List<Tag> tagsList = new List<Tag>();
                            #region PreTraining

                            questionPapers = batchAssessments.Where(x => x.Batch.ID == qpBatch.ID && x.StartDateTime < instituteProgram.StartDate).Select(x => x.QuestionPaper);
                            if (questionPapers.Count() > 0)
                            {
                                studentUserProfileList = batchAssessments.Where(x => x.Batch.ID == qpBatch.ID).SelectMany(x => x.Batch.BatchStudent).SelectMany(x => x.Student.UserProfile).Distinct().ToList();
                                questionPaperQuestion = questionPapers.SelectMany(x => x.QuestionPaperSection).SelectMany(x => x.QuestionPaperSectionQuestion).ToList();
                                questionList = questionPaperQuestion.Select(x => x.Question).ToList();
                                questionTagList = questionList.SelectMany(x => x.QuestionTags).ToList();
                                tagsList = questionTagList.Select(x => x.Tag).Distinct().ToList();
                                //List<decimal> correctAnswerPercentageList = new List<decimal>();
                                foreach (Tag tag in tagsList)
                                {
                                    TagViewModel tagViewModel = new TagViewModel();
                                    //List<Question> questionUnderThisAssessment = tag.QuestionTags.Where(qt => qt.Tag.ID == tag.ID && qt.Question.AssessmentSnapshot.Where(ass => ass.UserAssessment.UserProfile.UserProfileCollege.Where(upc => upc.CollegeID == collegeID).Count() > 0).Count() > 0).Select(x => x.Question).ToList();
                                    List<Question> questionUnderThisAssessment = tag.QuestionTags.Where(qt => qt.Tag.ID == tag.ID).Select(x => x.Question).ToList();
                                    if (questionUnderThisAssessment.Count() > 0)
                                    {
                                        questionUnderThisAssessment = questionUnderThisAssessment.Where(qt => qt.AssessmentSnapshot.Where(ass => ass.UserAssessment.UserProfile.Student.CollegeID == collegeID).Count() > 0).ToList();
                                        //int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => qua.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID == qua.AssessmentSnapshot.Where(ass => ass.Question.ID == qua.ID).FirstOrDefault().AnswerID).Count();

                                        List<int> correctAnsweredList = new List<int>();
                                        correctAnsweredList = questionUnderThisAssessment.Select(x => x.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault() != null ? x.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID : 0).ToList();
                                        List<int> userAnsweredList = questionUnderThisAssessment.SelectMany(x => x.AssessmentSnapshot.Where(ass => ass.QuestionID == x.ID).Select(ass => ass.AnswerID ?? 0)).Distinct().ToList();

                                        //int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => qua.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID == qua.AssessmentSnapshot.).Count();
                                        int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => userAnsweredList.Contains(qua.Answer.Where(a => a.IsCorrect ?? false).Select(x => x.ID).FirstOrDefault())).Count();
                                        //int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => correctAnsweredList.Contains(qua.AssessmentSnapshot.Where(ass=>ass.QuestionID==qua.ID && qua.Answer))).Count();
                                        decimal correctAnswerPercentage = (decimal)((correctAnsweredQuestions / (decimal)((userAnsweredList.Count() != 0 ? questionUnderThisAssessment.Count() : 1))) * 100);
                                        tagViewModel.ID = tag.ID;
                                        tagViewModel.Name = tag.Name;
                                        tagViewModel.PreTrainingValue = correctAnswerPercentage;
                                        tagViewModelList.Add(tagViewModel);
                                        //WholetagsList.Add(tag.Name);
                                        //PrecorrectAnswerPercentageList.Add(correctAnswerPercentage);

                                    }

                                }
                            }
                            #endregion
                            #region PostTraining

                            questionPapers = batchAssessments.Where(x => x.Batch.ID == qpBatch.ID && x.EndDateTime > instituteProgram.EndDate).Select(x => x.QuestionPaper);
                            if (questionPapers.Count() > 0)
                            {
                                studentUserProfileList = batchAssessments.Where(x => x.Batch.ID == qpBatch.ID).SelectMany(x => x.Batch.BatchStudent).SelectMany(x => x.Student.UserProfile).Distinct().ToList();
                                questionPaperQuestion = questionPapers.SelectMany(x => x.QuestionPaperSection).SelectMany(x => x.QuestionPaperSectionQuestion).ToList();
                                questionList = questionPaperQuestion.Select(x => x.Question).ToList();
                                questionTagList = questionList.SelectMany(x => x.QuestionTags).ToList();
                                tagsList = questionTagList.Select(x => x.Tag).Distinct().ToList();
                                //List<decimal> correctAnswerPercentageList = new List<decimal>();
                                foreach (Tag tag in tagsList)
                                {
                                    //List<Question> questionUnderThisAssessment = tag.QuestionTags.Where(qt => qt.Tag.ID == tag.ID && qt.Question.AssessmentSnapshot.Where(ass => ass.UserAssessment.UserProfile.UserProfileCollege.Where(upc => upc.CollegeID == collegeID).Count() > 0).Count() > 0).Select(x => x.Question).ToList();
                                    TagViewModel tagViewModel = new TagViewModel();
                                    List<Question> questionUnderThisAssessment = tag.QuestionTags.Where(qt => qt.Tag.ID == tag.ID).Select(x => x.Question).ToList();
                                    if (questionUnderThisAssessment.Count() > 0)
                                    {
                                        questionUnderThisAssessment = questionUnderThisAssessment.Where(qt => qt.AssessmentSnapshot.Where(ass => ass.UserAssessment.UserProfile.Student.CollegeID == collegeID).Count() > 0).ToList();
                                        //int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => qua.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID == qua.AssessmentSnapshot.Where(ass => ass.Question.ID == qua.ID).FirstOrDefault().AnswerID).Count();

                                        List<int> correctAnsweredList = new List<int>();
                                        correctAnsweredList = questionUnderThisAssessment.Select(x => x.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID).ToList();
                                        List<int> userAnsweredList = questionUnderThisAssessment.SelectMany(x => x.AssessmentSnapshot.Where(ass => ass.QuestionID == x.ID).Select(ass => ass.AnswerID ?? 0)).Distinct().ToList();

                                        //int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => qua.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID == qua.AssessmentSnapshot.).Count();
                                        int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => userAnsweredList.Contains(qua.Answer.Where(a => a.IsCorrect ?? false).Select(x => x.ID).FirstOrDefault())).Count();
                                        //int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => correctAnsweredList.Contains(qua.AssessmentSnapshot.Where(ass=>ass.QuestionID==qua.ID && qua.Answer))).Count();
                                        decimal correctAnswerPercentage = (decimal)((correctAnsweredQuestions / (decimal)((userAnsweredList.Count() != 0 ? questionUnderThisAssessment.Count() : 1))) * 100);
                                        //PostcorrectAnswerPercentageList.Add(correctAnswerPercentage);
                                        //WholetagsList.Add(tag.Name);
                                        tagViewModel = tagViewModelList.Where(t => t.ID == tag.ID).FirstOrDefault();
                                        if (tagViewModel != null)
                                        {
                                            tagViewModel.PostTrainingValue = correctAnswerPercentage;
                                        }
                                        else
                                        {
                                            tagViewModel = new TagViewModel();
                                            tagViewModel.ID = tag.ID;
                                            tagViewModel.Name = tag.Name;
                                            tagViewModel.PreTrainingValue = 0;
                                            tagViewModel.PostTrainingValue = correctAnswerPercentage;
                                        }
                                        tagViewModelList.Add(tagViewModel);
                                    }

                                }
                            }
                            #endregion
                            //WholetagsList = WholetagsList.Distinct().ToList();
                            //if (PrecorrectAnswerPercentageList.Count() < WholetagsList.Count())
                            //{
                            //    int excess = WholetagsList.Count() - PrecorrectAnswerPercentageList.Count();
                            //    for (int i = 0; i < excess; i++)
                            //    {
                            //        PrecorrectAnswerPercentageList.Add(0);
                            //    }
                            //}
                            //if (PostcorrectAnswerPercentageList.Count() < WholetagsList.Count())
                            //{
                            //    int excess = WholetagsList.Count() - PostcorrectAnswerPercentageList.Count();
                            //    for (int i = 0; i < excess; i++)
                            //    {
                            //        PostcorrectAnswerPercentageList.Add(0);
                            //    }
                            //}
                        }
                    }
                    radarReportModel.TagViewModelList = tagViewModelList;
                    //radarReportModel.Labels = WholetagsList;
                    //radarReportModel.PreTrainingdata = PrecorrectAnswerPercentageList;
                    //radarReportModel.PostTrainingdata = PostcorrectAnswerPercentageList;
                    radarReportModelList.Add(radarReportModel);
                }
                //result = JsonConvert.SerializeObject(radarReportModelList);
            }
            //Response.Write(result);
            return radarReportModelList;
        }


        public ActionResult OpenDownloadModal(string college, string batch, string Format = "")
        {
            List<RadarReportModel> radarReportModelList = ExportBatchPassPercentageRadarReport(college, batch, Format);

            return View();
        }
        #endregion

        // Section Wise Performance Report

        #region PerformancePieReport

        public ActionResult PerformancePieReport(long ID = 0)
        {

            UserAssessment userAssessment = db.UserAssessment.Where(ua => ua.UserProfile.UserName.ToLower() == User.Identity.Name && ua.AssessmentID == ID).FirstOrDefault();
            ReportViewModel reportViewModel = new ReportViewModel();
            QuestionPaper questionPaper = db.QuestionPaper.Find(ID);
            List<QuestionPaperSection> questionPaperSectionListforName = questionPaper.QuestionPaperSection.Distinct().ToList();

            reportViewModel.Sections = string.Join(",", questionPaperSectionListforName.Select(x => x.Name).ToArray());
            reportViewModel.Sections = "Max," + reportViewModel.Sections;

            reportViewModel.TotalSections = questionPaper.QuestionPaperSection.Count();
            if (questionPaper != null)
            {
                List<QuestionPaperSection> questionPaperSectionList = questionPaper.QuestionPaperSection.Distinct().ToList();
                //List<UserSectionStatus> userSectionStatusList= userAssessment.UserSectionStatus.ToList();

                List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();
                int yourscore = 0;
                foreach (QuestionPaperSection questionPaperSection in questionPaperSectionList)
                {
                    QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
                    questionPaperSectionViewModel.ID = questionPaperSection.ID;
                    questionPaperSectionViewModel.Name = questionPaperSection.Name;

                    int totalQuestions = questionPaperSection.QuestionPaperSectionQuestion.Count();
                    questionPaperSectionViewModel.TotalQuestions = totalQuestions;

                    int totalQuestionCount = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).Count();
                    reportViewModel.TotalQuestionCount = totalQuestionCount;

                    int answeredCount = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionPaperSectionQuestion.Where(qps => qps.AssessmentSectionID == questionPaperSection.ID).Count() > 0).Select(x => x.Question).Distinct().Count();
                    questionPaperSectionViewModel.AnsweredCount = answeredCount;

                    int unAttemptedQuestions = totalQuestions - answeredCount;
                    questionPaperSectionViewModel.UnAttemptedQuestions = unAttemptedQuestions;

                    //int correctAnswerCount = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionPaperSectionQuestion.Where(qps => qps.AssessmentSectionID == questionPaperSection.ID).Count() > 0 && ass.Question.Answer.FirstOrDefault().ID == ass.Answer.ID).Count();
                    //int correctAnswerCount = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();

                    int correctAnswerCount = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == questionPaperSection.ID).Count() > 0) && (ass.Answer.IsCorrect ?? false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();     //Get only the single select type questions 
                    List<Question> multiSelectTypeQuestionList = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == questionPaperSection.ID).Count() > 0) && (ass.Question.QuestionType.Name.ToLower().Equals("multi select"))).Select(x => x.Question).Distinct().ToList();
                    foreach (Question multiSelectQuestion in multiSelectTypeQuestionList)
                    {
                        List<Answer> correctAnswerList = multiSelectQuestion.Answer.Where(a => a.IsCorrect ?? false).ToList();
                        List<Answer> userAnsweredList = multiSelectQuestion.AssessmentSnapshot.Select(x => x.Answer).ToList();
                        if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
                        {                                                                       //All the answers are correct
                            correctAnswerCount++;
                        }
                    }
                    questionPaperSectionViewModel.CorrectAnswerCount = correctAnswerCount;

                    yourscore += correctAnswerCount;
                    reportViewModel.YourScore = yourscore;

                    int timetaken = userAssessment.AssessmentSnapshot.Sum(x => x.TimeTaken) ?? 0;
                    //  int val = 16;
                    questionPaperSectionViewModel.UsertotalTimeTaken = timetaken;
                    int wrongAnswerCount = answeredCount - correctAnswerCount;
                    questionPaperSectionViewModel.WrongAnswerCount = wrongAnswerCount;
                    decimal scorePercentage = 0;
                    if (totalQuestions != 0)
                    {
                        scorePercentage = ((decimal)correctAnswerCount / (decimal)totalQuestions) * 100;
                        //questionPaperSectionViewModel.Percentage = (int)Math.Ceiling(scorePercentage);
                        //questionPaperSectionViewModel.ScorePercentage = scorePercentage;
                       questionPaperSectionViewModel.ScorePercentage = Math.Round(scorePercentage, 2);
                    }

                    //float correctAnswerPercentage = 0;
                    //if (totalQuestions != 0)
                    //    correctAnswerPercentage = ((float)correctAnswerCount / (float)totalQuestions) * 100;
                    //questionPaperSectionViewModel.CorrectAnswerPercentage = (int)Math.Ceiling(correctAnswerPercentage);

                    questionPaperSectionViewModelList.Add(questionPaperSectionViewModel);
                    //decimal percentage=totalQuestions
                }

                reportViewModel.TotalTimeTaken = questionPaperSectionViewModelList.Sum(x => x.UsertotalTimeTaken);
                //TimeSpan result = TimeSpan.FromSeconds(reportViewModel.TotalTimeTaken);
                //string fromTimeString = result.ToString("hh':'mm");
                //reportViewModel.totalTimeTakenString = fromTimeString;

                reportViewModel.TestDuration = questionPaper.TimeLimit ?? questionPaper.QuestionPaperSection.Sum(x => x.TimeLimit ?? 0);
                //TimeSpan result1 = TimeSpan.FromMinutes(reportViewModel.TestDuration);
                //string fromTimeString1 = result1.ToString("hh':'mm");
                //reportViewModel.TestDurationString = fromTimeString1;

                reportViewModel.QuestionPaperSectionViewModellList = questionPaperSectionViewModelList;
            }


            return View(reportViewModel);
        }
        #endregion

        #region SectionPerformancePieReport

        public void SectionPerformancePieReport(long ID = 0)
        {

            UserAssessment userAssessment = db.UserAssessment.Where(ua => ua.UserProfile.UserName.ToLower() == User.Identity.Name && ua.AssessmentID == ID).FirstOrDefault();
            ReportViewModel reportViewModel = new ReportViewModel();
            QuestionPaper questionPaper = db.QuestionPaper.Find(ID);
            List<QuestionPaperSection> questionPaperSectionListforName = questionPaper.QuestionPaperSection.Distinct().ToList();

            reportViewModel.Sections = string.Join(",", questionPaperSectionListforName.Select(x => x.Name).ToArray());
            reportViewModel.Sections = "Max," + reportViewModel.Sections;

            reportViewModel.TotalSections = questionPaper.QuestionPaperSection.Count();
            if (questionPaper != null)
            {
                List<QuestionPaperSection> questionPaperSectionList = questionPaper.QuestionPaperSection.Distinct().ToList();
                //List<UserSectionStatus> userSectionStatusList= userAssessment.UserSectionStatus.ToList();

                List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();
                int yourscore = 0;
                foreach (QuestionPaperSection questionPaperSection in questionPaperSectionList)
                {
                    QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
                    questionPaperSectionViewModel.ID = questionPaperSection.ID;
                    questionPaperSectionViewModel.Name = questionPaperSection.Name;

                    int totalQuestions = questionPaperSection.QuestionPaperSectionQuestion.Count();
                    questionPaperSectionViewModel.TotalQuestions = totalQuestions;

                    int totalQuestionCount = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).Count();
                    reportViewModel.TotalQuestionCount = totalQuestionCount;

                    int answeredCount = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionPaperSectionQuestion.Where(qps => qps.AssessmentSectionID == questionPaperSection.ID).Count() > 0).Select(x => x.Question).Distinct().Count();
                    questionPaperSectionViewModel.AnsweredCount = answeredCount;

                    int unAttemptedQuestions = totalQuestions - answeredCount;
                    questionPaperSectionViewModel.UnAttemptedQuestions = unAttemptedQuestions;

                    //int correctAnswerCount = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionPaperSectionQuestion.Where(qps => qps.AssessmentSectionID == questionPaperSection.ID).Count() > 0 && ass.Question.Answer.FirstOrDefault().ID == ass.Answer.ID).Count();
                    //int correctAnswerCount = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();

                    int correctAnswerCount = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == questionPaperSection.ID).Count() > 0) && (ass.Answer.IsCorrect ?? false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();     //Get only the single select type questions 
                    List<Question> multiSelectTypeQuestionList = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == questionPaperSection.ID).Count() > 0) && (ass.Question.QuestionType.Name.ToLower().Equals("multi select"))).Select(x => x.Question).Distinct().ToList();
                    foreach (Question multiSelectQuestion in multiSelectTypeQuestionList)
                    {
                        List<Answer> correctAnswerList = multiSelectQuestion.Answer.Where(a => a.IsCorrect ?? false).ToList();
                        List<Answer> userAnsweredList = multiSelectQuestion.AssessmentSnapshot.Select(x => x.Answer).ToList();
                        if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
                        {                                                                       //All the answers are correct
                            correctAnswerCount++;
                        }
                    }
                    questionPaperSectionViewModel.CorrectAnswerCount = correctAnswerCount;

                    yourscore += correctAnswerCount;
                    reportViewModel.YourScore = yourscore;

                    int timetaken = userAssessment.AssessmentSnapshot.Sum(x => x.TimeTaken) ?? 0;
                    //  int val = 16;
                    questionPaperSectionViewModel.UsertotalTimeTaken = timetaken;
                    int wrongAnswerCount = answeredCount - correctAnswerCount;
                    questionPaperSectionViewModel.WrongAnswerCount = wrongAnswerCount;
                    decimal scorePercentage = 0;
                    if (totalQuestions != 0)
                    {
                        scorePercentage = ((decimal)correctAnswerCount / (decimal)totalQuestions) * 100;
                        //questionPaperSectionViewModel.ScorePercentage = scorePercentage;
                        questionPaperSectionViewModel.ScorePercentage = Math.Round(scorePercentage, 2);
                    }

                    //float correctAnswerPercentage = 0;
                    //if (totalQuestions != 0)
                    //    correctAnswerPercentage = ((float)correctAnswerCount / (float)totalQuestions) * 100;
                    //questionPaperSectionViewModel.CorrectAnswerPercentage = (int)Math.Ceiling(correctAnswerPercentage);

                    questionPaperSectionViewModelList.Add(questionPaperSectionViewModel);
                    //decimal percentage=totalQuestions
                }

                reportViewModel.TotalTimeTaken = questionPaperSectionViewModelList.Sum(x => x.UsertotalTimeTaken);
                //TimeSpan result = TimeSpan.FromSeconds(reportViewModel.TotalTimeTaken);
                //string fromTimeString = result.ToString("hh':'mm");
                //reportViewModel.totalTimeTakenString = fromTimeString;

                reportViewModel.TestDuration = questionPaper.TimeLimit ?? questionPaper.QuestionPaperSection.Sum(x => x.TimeLimit ?? 0);
                //TimeSpan result1 = TimeSpan.FromMinutes(reportViewModel.TestDuration);
                //string fromTimeString1 = result1.ToString("hh':'mm");
                //reportViewModel.TestDurationString = fromTimeString1;

                reportViewModel.QuestionPaperSectionViewModellList = questionPaperSectionViewModelList;
            }

            string result = "";
            result = RenderPartialViewToString("~/Views/Report/SectionPerformancePieReport.cshtml", reportViewModel);

            Response.Write(result);
            // return View(reportViewModel);
        }
        #endregion

        #region QuestionWiseAnalysis
        //anu public ActionResult QuestionWiseAnalysis(long id = 0, int totalPages = 0, int pageNo = 1)
        public ActionResult QuestionWiseAnalysis(long id = 0)    
    {
            ViewBag.AssessmentID = id;
            
            //anu
            //int pageLimit = Convert.ToInt16(pageLimitString);
            //IEnumerable<Question> question = (from y in db.Question
            //                           select y).OrderBy(x => x.ID).Skip(pageLimit * (pageNo - 1)).Take(pageLimit);
            //var remainingItems = (db.Question.Count() % pageLimit) == 0 ? (db.Question.Count() / pageLimit) : (db.Question.Count() / pageLimit) + 1;
            //totalPages = (totalPages == null || totalPages == 0) ? remainingItems : totalPages;
            //ViewBag.TotalPages = totalPages;
            //ViewBag.CurrentPageNo = pageNo;

            ////List<Branch> branchesList = branches.ToList();

            //int questionNumber = 1;
            //List<QuestionViewModel> questionViewModelList = MapperHelper.MapToViewModelList(question.AsQueryable());
            //foreach (var item in questionViewModelList)
            //{
            //    item.QuestionNumber = questionNumber;
            //    questionNumber++;
            //}

            //anu
                return View();
        }
        public void QuestionWiseAnalysisList(long id = 0)
        {
            datatableRequest = new DataTableRequest(Request, columns);

            // IQueryable<Question> questions = db.Question;
            int userId = GetCurrentUserID();
            UserAssessment userAssessment = db.UserAssessment.Where(x => x.AssessmentID == id && x.UserID == userId).FirstOrDefault();
           // ReportViewModel reportViewModel = new ReportViewModel();
            QuestionPaper questionPaper = db.QuestionPaper.Find(id);
            IQueryable<Question> questions = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).Select(z => z.Question).AsQueryable();
            List<QuestionViewModel> questionViewModelList = MapperHelper.MapToViewModelListforReport(questions, id, userAssessment.ID);

            //reportViewModel.QuestionViewModelList = questionViewModelList;
           // reportViewModel.AssessmentName = questionPaper.Name;
            //anu 
            int i = 1;
            int Sno = i;
            foreach (QuestionViewModel questionViewModel in questionViewModelList)
            {
                questionViewModel.Sno = i; // Question Number Squence Order
                i++;
            }
            // anu
            int totalRecords = questions.Count();
            int filteredCount = totalRecords;
            //if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            //{
            //    questions = (from q in questions where q.Description.Contains(datatableRequest.SearchString) select q);
            //    filteredCount = questions.Count();
            //}
            //questions = questions.OrderBy(x => x.ID);
            //if (datatableRequest.ShouldOrder)
            //{
            //    questions = questions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            //}
            IQueryable<QuestionViewModel> questionViewModels = questionViewModelList.AsQueryable();
            questionViewModelList = questionViewModels.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired).ToList();

            HtmlFormatter htmlFormatter = new HtmlFormatter();
            questionViewModelList = questionViewModelList.Select(q => { q.SectionName = htmlFormatter.GetContentFromHtmlWithOutReplaceSpace(q.SectionName); return q; }).ToList();
            var technicalQuestionViewModelList = new List<Domain.ViewModel.TechnicalQuestionViewModel>();

            QuestionDataTableViewModel dataTableViewModel = new QuestionDataTableViewModel(questionViewModelList, technicalQuestionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);
            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }
        #endregion

        #region Department or Branch pass percentage with one Assessment

        public ActionResult BranchPassPercentageReportFilter()
        {
            IQueryable<QuestionPaper> questionPaper = GetAssessmentsByUser();
            if (questionPaper != null) // Get list of Question papers
                ViewBag.Assessments = questionPaper;
            else
                ViewBag.Assessments = new List<QuestionPaper>().AsQueryable();
            ViewBag.Branches = new List<Branch>().AsQueryable();
            return View();
        }

        public ActionResult GenerateBranchPercentageReport(int assessmentID, string selectedBranchesString)
        {
            UserProfile loggedInUserProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();

            QuestionPaper questionPaper = new QuestionPaper();
            if (assessmentID > 0)
                questionPaper = db.QuestionPaper.Find(assessmentID);

            List<long> selectedBranchIDList = new List<long>();
            List<Branch> branchList = new List<Branch>();

            if (!string.IsNullOrEmpty(selectedBranchesString) && selectedBranchesString.ToLower() == "all")
            {
                if (User.IsInRole("Admin"))
                {                                                       //User is admin. Get all branches of the assessment.
                    branchList = questionPaper.UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Select(x => x.UserProfile.Student.Branch).Distinct().ToList();
                }
                else if (User.IsInRole("InstituteAdmin"))
                {
                    List<long> userCollegesIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                    branchList = questionPaper.UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID && ua.UserProfile.Student.Branch.CollegeBranch.Where(cb => userCollegesIDList.Contains(cb.CollegeID)).Count() > 0).Select(x => x.UserProfile.Student.Branch).Distinct().ToList();
                }
            }
            else if (!string.IsNullOrEmpty(selectedBranchesString))
            {
                selectedBranchIDList = selectedBranchesString.Split(',').Select(long.Parse).ToList();
                if (User.IsInRole("Admin"))
                {
                    branchList = questionPaper.UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID && (selectedBranchIDList.Contains(ua.UserProfile.Student.BranchID ?? 0))).Select(x => x.UserProfile.Student.Branch).Distinct().ToList();

                }
                else if (User.IsInRole("InstituteAdmin"))
                {
                    List<long> userCollegesIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                    branchList = questionPaper.UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID && (selectedBranchIDList.Contains(ua.UserProfile.Student.BranchID ?? 0)) && ua.UserProfile.Student.Branch.CollegeBranch.Where(cb => userCollegesIDList.Contains(cb.CollegeID)).Count() > 0).Select(x => x.UserProfile.Student.Branch).Distinct().ToList();
                }
            }

            ReportViewModel reportViewModel = GetReportViewModelForBranchPercentageReport(questionPaper, branchList, loggedInUserProfile);

            return View("~/Views/Report/BranchPassPercentageReport.cshtml", reportViewModel);
        }

        public ReportViewModel GetReportViewModelForBranchPercentageReport(QuestionPaper questionPaper, List<Branch> branchList, UserProfile loggedInUserProfile)
        {
            ReportViewModel reportViewModel = new ReportViewModel();
            List<BranchViewModel> branchViewModelList = new List<BranchViewModel>();
            QuestionPaperViewModel assessmentViewModel = MapperHelper.MapToViewModel(questionPaper);
            reportViewModel.questionPaperViewModel = assessmentViewModel;
            int attentedStudentsCount = 0;

            foreach (Branch branch in branchList)
            {
                if (branch != null)
                {
                    BranchViewModel branchViewModel = MapperHelper.MapToViewModel(branch);
                    List<Student> studentList = new List<Student>();
                    if (User.IsInRole("Admin"))
                    {
                        studentList = branch.Student.Where(s => s.UserProfile.FirstOrDefault().UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Count() > 0).ToList();
                    }
                    else
                    {
                        List<long> userCollegeIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                        studentList = branch.Student.Where(s => (userCollegeIDList.Contains(s.CollegeID ?? 0)) && s.UserProfile.FirstOrDefault().UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Count() > 0).ToList();
                    }

                    List<UserProfile> studentUserProfileList = studentList.SelectMany(x => x.UserProfile).Distinct().ToList();

                    decimal cutOffMark = questionPaper.CutOffMark ?? 0;
                    int passCount = 0;
                    int failedCount = 0;
                    int totalCount = 0;
                    decimal passPercentage = 0;
                    if (cutOffMark > 0)
                    {
                        passCount = studentUserProfileList.Where(stup => stup.UserAssessment.Where(ua => (ua.AssessmentID == questionPaper.ID) && (ua.Marks >= cutOffMark)).Count() > 0).Count();
                        failedCount = studentUserProfileList.Where(stup => stup.UserAssessment.Where(ua => (ua.AssessmentID == questionPaper.ID) && (ua.Marks < cutOffMark)).Count() > 0).Count();
                        totalCount = passCount + failedCount;
                        if (totalCount != 0)
                        {
                            passPercentage = ((decimal)passCount / (decimal)totalCount) * (decimal)100;
                        }
                        else
                        {
                            passPercentage = 0;
                        }

                        branchViewModel.PassCount = passCount;
                        branchViewModel.FailedCount = failedCount;
                        branchViewModel.PassPercentage = passPercentage;
                        branchViewModel.AttentedStudentsCount = totalCount;
                        branchViewModel.IsCutOffMarkSpecified = true;

                        attentedStudentsCount = attentedStudentsCount + totalCount;
                    }
                    else
                    {
                        branchViewModel.IsCutOffMarkSpecified = false;
                    }
                    branchViewModelList.Add(branchViewModel);
                }
            }

            reportViewModel.BranchViewModelList = branchViewModelList;
            reportViewModel.AttentedStudentsCount = attentedStudentsCount;
            return reportViewModel;
        }

        #endregion

        //#region Department or Branch pass percentage with one Assessment


        //public ActionResult GenerateBranchPercentageReport(int assessmentID, string selectedBranchesString)
        //{
        //    UserProfile loggedInUserProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();

        //    QuestionPaper questionPaper = new QuestionPaper();
        //    if (assessmentID > 0)
        //        questionPaper = db.QuestionPaper.Find(assessmentID);

        //    List<long> selectedBranchIDList = new List<long>();
        //    List<Branch> branchList = new List<Branch>();

        //    if (!string.IsNullOrEmpty(selectedBranchesString) && selectedBranchesString.ToLower() == "all")
        //    {
        //        if (User.IsInRole("Admin"))
        //        {                                                       //User is admin. Get all branches of the assessment.
        //            branchList = questionPaper.UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Select(x => x.UserProfile.Student.Branch).Distinct().ToList();
        //        }
        //        else if (User.IsInRole("CollegeAdmin"))
        //        {
        //            List<long> userCollegesIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
        //            branchList = questionPaper.UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID && ua.UserProfile.Student.Branch.CollegeBranch.Where(cb => userCollegesIDList.Contains(cb.CollegeID)).Count() > 0).Select(x => x.UserProfile.Student.Branch).Distinct().ToList();
        //        }
        //    }
        //    else if (!string.IsNullOrEmpty(selectedBranchesString))
        //    {
        //        selectedBranchIDList = selectedBranchesString.Split(',').Select(long.Parse).ToList();
        //        if (User.IsInRole("Admin"))
        //        {
        //            branchList = questionPaper.UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID && (selectedBranchIDList.Contains(ua.UserProfile.Student.BranchID ?? 0))).Select(x => x.UserProfile.Student.Branch).Distinct().ToList();

        //        }
        //        else if (User.IsInRole("CollegeAdmin"))
        //        {
        //            List<long> userCollegesIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
        //            branchList = questionPaper.UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID && (selectedBranchIDList.Contains(ua.UserProfile.Student.BranchID ?? 0)) && ua.UserProfile.Student.Branch.CollegeBranch.Where(cb => userCollegesIDList.Contains(cb.CollegeID)).Count() > 0).Select(x => x.UserProfile.Student.Branch).Distinct().ToList();
        //        }
        //    }

        //    ReportViewModel reportViewModel = GetReportViewModelForBranchPercentageReport(questionPaper, branchList, loggedInUserProfile);

        //    return View("~/Views/Report/BranchPassPercentageReport.cshtml", reportViewModel);
        //}

        //public ReportViewModel GetReportViewModelForBranchPercentageReport(QuestionPaper questionPaper, List<Branch> branchList, UserProfile loggedInUserProfile)
        //{
        //    ReportViewModel reportViewModel = new ReportViewModel();
        //    List<BranchViewModel> branchViewModelList = new List<BranchViewModel>();
        //    QuestionPaperViewModel assessmentViewModel = MapperHelper.MapToViewModel(questionPaper);
        //    reportViewModel.questionPaperViewModel = assessmentViewModel;
        //    int attentedStudentsCount = 0;

        //    foreach (Branch branch in branchList)
        //    {
        //        if (branch != null)
        //        {
        //            BranchViewModel branchViewModel = MapperHelper.MapToViewModel(branch);
        //            List<Student> studentList = new List<Student>();
        //            if (User.IsInRole("Admin"))
        //            {
        //                studentList = branch.Student.Where(s => s.UserProfile.FirstOrDefault().UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Count() > 0).ToList();
        //            }
        //            else
        //            {
        //                List<long> userCollegeIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
        //                studentList = branch.Student.Where(s => (userCollegeIDList.Contains(s.CollegeID ?? 0)) && s.UserProfile.FirstOrDefault().UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Count() > 0).ToList();
        //            }

        //            List<UserProfile> studentUserProfileList = studentList.SelectMany(x => x.UserProfile).Distinct().ToList();

        //            decimal cutOffMark = questionPaper.CutOffMark ?? 0;
        //            int passCount = 0;
        //            int failedCount = 0;
        //            int totalCount = 0;
        //            decimal passPercentage = 0;
        //            if (cutOffMark > 0)
        //            {
        //                passCount = studentUserProfileList.Where(stup => stup.UserAssessment.Where(ua => (ua.AssessmentID == questionPaper.ID) && (ua.Marks >= cutOffMark)).Count() > 0).Count();
        //                failedCount = studentUserProfileList.Where(stup => stup.UserAssessment.Where(ua => (ua.AssessmentID == questionPaper.ID) && (ua.Marks < cutOffMark)).Count() > 0).Count();
        //                totalCount = passCount + failedCount;
        //                if (totalCount != 0)
        //                {
        //                    passPercentage = ((decimal)passCount / (decimal)totalCount) * (decimal)100;
        //                }
        //                else
        //                {
        //                    passPercentage = 0;
        //                }

        //                branchViewModel.PassCount = passCount;
        //                branchViewModel.FailedCount = failedCount;
        //                branchViewModel.PassPercentage = passPercentage;
        //                branchViewModel.AttentedStudentsCount = totalCount;
        //                branchViewModel.IsCutOffMarkSpecified = true;

        //                attentedStudentsCount = attentedStudentsCount + totalCount;
        //            }
        //            else
        //            {
        //                branchViewModel.IsCutOffMarkSpecified = false;
        //            }
        //            branchViewModelList.Add(branchViewModel);
        //        }
        //    }

        //    reportViewModel.BranchViewModelList = branchViewModelList;
        //    reportViewModel.AttentedStudentsCount = attentedStudentsCount;
        //    return reportViewModel;
        //}

        //#endregion

        #region Assessment Pass percentage report

        public ActionResult AssessmentPassPercentageReportFilter()
        {
            if (User.IsInRole("SuperAdmin"))
            {                                                                   //User is admin
                //SelectList batchListItem = new SelectList(db.Batch, "ID", "Name");

                ViewBag.Batches = db.Batch;
                ViewBag.Branches = db.Branch.ToList();
            }
            if (User.IsInRole("CollegeAdmin"))
            {                                                                   //User is admin
                //SelectList batchListItem = new SelectList(db.Batch, "ID", "Name");

                ViewBag.Batches = db.Batch;
                ViewBag.Branches = db.Branch.ToList();
            }
            else if (User.IsInRole("InstituteAdmin"))
            {
                UserProfile loggedInUserProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
                if (loggedInUserProfile != null)
                {
                    List<long> userProfileCollegeIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();

                    List<Student> studentList = loggedInUserProfile.UserProfileCollege.Where(upc => (userProfileCollegeIDList.Contains(upc.CollegeID))).SelectMany(x => x.College.Student).Distinct().ToList();
                    List<UserAssessment> userAssessmentList = studentList.Where(sl => sl.UserProfile.FirstOrDefault().UserAssessment.Count() > 0).SelectMany(x => x.UserProfile.FirstOrDefault().UserAssessment).Distinct().ToList();
                    List<BatchAssessment> batchAssessmentList = userAssessmentList.SelectMany(x => x.QuestionPaper.BatchAssessment).Distinct().ToList();
                    List<Batch> batchList = batchAssessmentList.Where(b => (userProfileCollegeIDList.Contains(b.Batch.CollegeID != null ? b.Batch.CollegeID.Value : 0))).Select(x => x.Batch).ToList();

                    ViewBag.Batches = batchList.AsQueryable();
                    List<CollegeBranch> collegeBranchList = loggedInUserProfile.UserProfileCollege.SelectMany(x => x.College.CollegeBranch).Distinct().ToList();
                    List<Branch> branchList = collegeBranchList.Select(x => x.Branch).ToList();
                    ViewBag.Branches = branchList;

                }
            }
            ViewBag.Assessments = new List<QuestionPaper>().AsQueryable();
            return View();
        }

        public ActionResult GenerateAssessmentPercentageReportByBatch(int batchID, string selectedQuestionPapersString)
        {
            UserProfile loggedInUserProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();

            Batch batch = new Batch();
            if (batchID > 0)
                batch = db.Batch.Find(batchID);

            List<long> selectedAssessmentIDList = new List<long>();
            List<QuestionPaper> assessmentList = new List<QuestionPaper>();

            if (!string.IsNullOrEmpty(selectedQuestionPapersString) && selectedQuestionPapersString.ToLower() == "all")
            {
                if (User.IsInRole("SuperAdmin"))
                {                                                       //User is admin. Get all assessments of the branch.
                    //assessmentList = assessment.UserAssessment.Where(ua => ua.AssessmentID == assessment.ID).Select(x => x.UserProfile.Student.Branch).Distinct().ToList();
                    assessmentList = batch.BatchAssessment.Select(x => x.QuestionPaper).Distinct().ToList();
                }
                if (User.IsInRole("CollegeAdmin"))
                {                                                       //User is admin. Get all assessments of the branch.
                    //assessmentList = assessment.UserAssessment.Where(ua => ua.AssessmentID == assessment.ID).Select(x => x.UserProfile.Student.Branch).Distinct().ToList();
                    assessmentList = batch.BatchAssessment.Select(x => x.QuestionPaper).Distinct().ToList();
                }
                else if (User.IsInRole("InstituteAdmin"))
                {
                    List<long> userCollegesIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                    assessmentList = batch.BatchStudent.Where(bs => (userCollegesIDList.Contains(bs.Student.CollegeID ?? 0))).SelectMany(x => x.Batch.BatchAssessment).Select(x => x.QuestionPaper).Distinct().ToList();
                }
            }
            else if (!string.IsNullOrEmpty(selectedQuestionPapersString))
            {
                selectedAssessmentIDList = selectedQuestionPapersString.Split(',').Select(long.Parse).ToList();
                if (User.IsInRole("SuperAdmin"))
                {
                    assessmentList = batch.BatchAssessment.Where(ba => (selectedAssessmentIDList.Contains(ba.AssessmentID))).Select(x => x.QuestionPaper).Distinct().ToList();

                }
                if (User.IsInRole("CollegeAdmin"))
                {
                    assessmentList = batch.BatchAssessment.Where(ba => (selectedAssessmentIDList.Contains(ba.AssessmentID))).Select(x => x.QuestionPaper).Distinct().ToList();

                }
                else if (User.IsInRole("InstituteAdmin"))
                {
                    List<long> userCollegesIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                    assessmentList = batch.BatchStudent.Where(bs => (userCollegesIDList.Contains(bs.Student.CollegeID ?? 0))).SelectMany(x => x.Batch.BatchAssessment).Where(ba => (selectedAssessmentIDList.Contains(ba.AssessmentID))).Select(x => x.QuestionPaper).Distinct().ToList();
                }
            }

            ReportViewModel reportViewModel = GetReportViewModelForAssessmentPercentageReportByBatch(batch, assessmentList, loggedInUserProfile);
            reportViewModel.Type = "ByBatch";
            return View("~/Views/Report/AssessmentPassPercentageReport.cshtml", reportViewModel);
        }

        public ReportViewModel GetReportViewModelForAssessmentPercentageReportByBatch(Batch batch, List<QuestionPaper> assessmentList, UserProfile loggedInUserProfile)
        {
            ReportViewModel reportViewModel = new ReportViewModel();

            List<QuestionPaperViewModel> assessmentViewModelList = new List<QuestionPaperViewModel>();
            BatchViewModel batchViewModel = MapperHelper.MapToViewModel(batch);
            reportViewModel.BatchViewModel = batchViewModel;
            int attentedStudentsCount = 0;

            foreach (QuestionPaper questionPaper in assessmentList)
            {
                QuestionPaperViewModel assessmentViewModel = MapperHelper.MapToViewModel(questionPaper);
                List<Student> studentList = new List<Student>();
                if (User.IsInRole("SuperAdmin"))
                {
                    studentList = batch.BatchStudent.Where(bs => bs.Student.UserProfile.FirstOrDefault().UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Count() > 0).Select(x => x.Student).Distinct().ToList();
                }
                if (User.IsInRole("CollegeAdmin"))
                {
                    studentList = batch.BatchStudent.Where(bs => bs.Student.UserProfile.FirstOrDefault().UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Count() > 0).Select(x => x.Student).Distinct().ToList();
                }
                else if (User.IsInRole("InstituteAdmin"))
                {
                    List<long> userProfileCollegeIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                    studentList = batch.BatchStudent.Where(bs => (userProfileCollegeIDList.Contains(bs.Student.CollegeID ?? 0)) && bs.Student.UserProfile.FirstOrDefault().UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Count() > 0).Select(x => x.Student).Distinct().ToList();
                }
                List<UserProfile> studentUserProfileList = studentList.SelectMany(x => x.UserProfile).Distinct().ToList();

                decimal cutOffMark = questionPaper.CutOffMark ?? 0;
                int passCount = 0;
                int failedCount = 0;
                int totalCount = 0;
                decimal passPercentage = 0;
                if (cutOffMark > 0)
                {
                    passCount = studentUserProfileList.Where(stup => stup.UserAssessment.Where(ua => (ua.AssessmentID == questionPaper.ID) && (ua.Marks >= cutOffMark)).Count() > 0).Count();
                    failedCount = studentUserProfileList.Where(stup => stup.UserAssessment.Where(ua => (ua.AssessmentID == questionPaper.ID) && (ua.Marks < cutOffMark)).Count() > 0).Count();
                    totalCount = passCount + failedCount;
                    if (totalCount > passCount)
                    {
                        passPercentage = ((decimal)passCount / (decimal)totalCount) * (decimal)100;
                    }

                    assessmentViewModel.PassCount = passCount;
                    assessmentViewModel.FailedCount = failedCount;
                    assessmentViewModel.PassPercentage = passPercentage;
                    assessmentViewModel.AttentedStudentsCount = totalCount;
                    assessmentViewModel.IsCutOffMarkSpecified = true;

                    attentedStudentsCount = attentedStudentsCount + totalCount;
                }
                else
                {
                    assessmentViewModel.IsCutOffMarkSpecified = false;
                }
                assessmentViewModelList.Add(assessmentViewModel);
            }

            reportViewModel.QuestionPaperViewModelList = assessmentViewModelList;
            reportViewModel.AttentedStudentsCount = attentedStudentsCount;

            return reportViewModel;
        }

        public ActionResult GenerateAssessmentPercentageReportByBranch(int branchID, string selectedQuestionPapersString)
        {
            UserProfile loggedInUserProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();

            Branch branch = new Branch();
            if (branchID > 0)
                branch = db.Branch.Find(branchID);

            List<long> selectedAssessmentIDList = new List<long>();
            List<QuestionPaper> assessmentList = new List<QuestionPaper>();

            if (!string.IsNullOrEmpty(selectedQuestionPapersString) && selectedQuestionPapersString.ToLower() == "all")
            {
                if (User.IsInRole("SuperAdmin"))
                {                                                       //User is admin. Get all assessments of the branch.
                    assessmentList = branch.Student.Where(s => s.UserProfile.FirstOrDefault().UserAssessment.Count() > 0).SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Select(x => x.QuestionPaper).Distinct().ToList();
                }
                if (User.IsInRole("CollegeAdmin"))
                {                                                       //User is admin. Get all assessments of the branch.
                    assessmentList = branch.Student.Where(s => s.UserProfile.FirstOrDefault().UserAssessment.Count() > 0).SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Select(x => x.QuestionPaper).Distinct().ToList();
                }
                else if (User.IsInRole("InstituteAdmin"))
                {
                    List<long> userCollegesIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                    assessmentList = branch.Student.Where(s => (userCollegesIDList.Contains(s.CollegeID ?? 0)) && s.UserProfile.FirstOrDefault().UserAssessment.Count() > 0).SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Select(x => x.QuestionPaper).Distinct().ToList();

                }
            }
            else if (!string.IsNullOrEmpty(selectedQuestionPapersString))
            {
                selectedAssessmentIDList = selectedQuestionPapersString.Split(',').Select(long.Parse).ToList();
                if (User.IsInRole("SuperAdmin"))
                {
                    assessmentList = branch.Student.Where(s => s.UserProfile.FirstOrDefault().UserAssessment.Count() > 0).SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Where(ua => selectedAssessmentIDList.Contains(ua.AssessmentID ?? 0)).Select(x => x.QuestionPaper).Distinct().ToList();

                }
                if (User.IsInRole("CollegeAdmin"))
                {
                    assessmentList = branch.Student.Where(s => s.UserProfile.FirstOrDefault().UserAssessment.Count() > 0).SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Where(ua => selectedAssessmentIDList.Contains(ua.AssessmentID ?? 0)).Select(x => x.QuestionPaper).Distinct().ToList();

                }
                else if (User.IsInRole("InstituteAdmin"))
                {
                    List<long> userCollegesIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                    assessmentList = branch.Student.Where(s => (userCollegesIDList.Contains(s.CollegeID ?? 0)) && s.UserProfile.FirstOrDefault().UserAssessment.Count() > 0).SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Where(ua => selectedAssessmentIDList.Contains(ua.AssessmentID ?? 0)).Select(x => x.QuestionPaper).Distinct().ToList();
                }
            }

            ReportViewModel reportViewModel = GetReportViewModelForAssessmentPercentageReportByBranch(branch, assessmentList, loggedInUserProfile);
            reportViewModel.Type = "ByBranch";
            return View("~/Views/Report/AssessmentPassPercentageReport.cshtml", reportViewModel);
        }

        public ReportViewModel GetReportViewModelForAssessmentPercentageReportByBranch(Branch branch, List<QuestionPaper> assessmentList, UserProfile loggedInUserProfile)
        {
            ReportViewModel reportViewModel = new ReportViewModel();

            List<QuestionPaperViewModel> assessmentViewModelList = new List<QuestionPaperViewModel>();
            BranchViewModel branchViewModel = MapperHelper.MapToViewModel(branch);
            reportViewModel.BranchViewModel = branchViewModel;
            int attentedStudentsCount = 0;

            foreach (QuestionPaper questionPaper in assessmentList)
            {
                QuestionPaperViewModel assessmentViewModel = MapperHelper.MapToViewModel(questionPaper);
                List<Student> studentList = new List<Student>();
                if (User.IsInRole("SuperAdmin"))
                {
                    studentList = branch.Student.Where(s => s.UserProfile.FirstOrDefault().UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Count() > 0).ToList();
                }
                if (User.IsInRole("CollegeAdmin"))
                {
                    studentList = branch.Student.Where(s => s.UserProfile.FirstOrDefault().UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Count() > 0).ToList();
                }
                else
                {
                    List<long> userCollegeIDList = loggedInUserProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                    studentList = branch.Student.Where(s => (userCollegeIDList.Contains(s.CollegeID ?? 0)) && s.UserProfile.FirstOrDefault().UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Count() > 0).ToList();
                }
                List<UserProfile> studentUserProfileList = studentList.SelectMany(x => x.UserProfile).Distinct().ToList();

                decimal cutOffMark = questionPaper.CutOffMark ?? 0;
                int passCount = 0;
                int failedCount = 0;
                int totalCount = 0;
                decimal passPercentage = 0;
                if (cutOffMark > 0)
                {
                    passCount = studentUserProfileList.Where(stup => stup.UserAssessment.Where(ua => (ua.AssessmentID == questionPaper.ID) && (ua.Marks >= cutOffMark)).Count() > 0).Count();
                    failedCount = studentUserProfileList.Where(stup => stup.UserAssessment.Where(ua => (ua.AssessmentID == questionPaper.ID) && (ua.Marks < cutOffMark)).Count() > 0).Count();
                    totalCount = passCount + failedCount;
                    if (totalCount > passCount)
                    {
                        passPercentage = ((decimal)passCount / (decimal)totalCount) * (decimal)100;
                    }
                    assessmentViewModel.PassCount = passCount;
                    assessmentViewModel.FailedCount = failedCount;
                    assessmentViewModel.PassPercentage = passPercentage;
                    assessmentViewModel.AttentedStudentsCount = totalCount;
                    assessmentViewModel.IsCutOffMarkSpecified = true;
                    attentedStudentsCount += totalCount;
                }
                else
                {
                    assessmentViewModel.IsCutOffMarkSpecified = false;
                }
                assessmentViewModelList.Add(assessmentViewModel);
            }

            reportViewModel.QuestionPaperViewModelList = assessmentViewModelList;
            reportViewModel.AttentedStudentsCount = attentedStudentsCount;

            return reportViewModel;
        }

        #endregion

        public IQueryable<QuestionPaper> GetAssessmentsByUser()
        {
            IQueryable<QuestionPaper> assessments = null;
            if (User.IsInRole("CollegeAdmin"))
                assessments = db.QuestionPaper.GroupBy(x=>x.Name).Select(x=>x.FirstOrDefault());
            if (User.IsInRole("SuperAdmin"))
                assessments = db.QuestionPaper.GroupBy(x => x.Name).Select(x => x.FirstOrDefault());
            else if (User.IsInRole("InstituteAdmin"))
            {
                UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
                if (userProfile != null)
                {
                    List<Student> studentList = userProfile.UserProfileCollege.SelectMany(x => x.College.Student).Distinct().Where(x => x.UserProfile.Where(up => up.UserAssessment.Count() > 0).Count() > 0).ToList();
                    List<UserProfile> studentUserProfileList = studentList.SelectMany(x => x.UserProfile).Distinct().ToList();
                    List<UserAssessment> studentUserAssessmentList = studentUserProfileList.SelectMany(x => x.UserAssessment).Distinct().ToList();
                    List<QuestionPaper> userAssessmentList = studentUserAssessmentList.Select(x => x.QuestionPaper).Distinct().ToList();
                    assessments = userAssessmentList.AsQueryable();
                    //return userAssessmentList.AsQueryable();
                }
                //else return new List<Assessment>().AsQueryable();
            }

            //else
            //    return new List<Assessment>().AsQueryable();
            return assessments;

        }

        //public IQueryable<College> GetCollegeAndBatch()
        //{
        //    IQueryable<College> college = null;
        //    if (User.IsInRole("Admin"))
        //        college = db.College;
        //    else if (User.IsInRole("CollegeAdmin"))
        //    {
        //        UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
        //        if (userProfile != null)
        //        {                    
        //            List<College> collegeList = collegeList.SelectMany(x => x.Name).Distinct().ToList();
        //            college = collegeList.AsQueryable();
        //            //return userAssessmentList.AsQueryable();
        //        }
        //        //else return new List<Assessment>().AsQueryable();
        //    }

        //    //else
        //    //    return new List<Assessment>().AsQueryable();
        //    return college;
        //}
        //#region ScoreCard


        //public ActionResult ScoreCardIndex()
        //{
        //    ViewBag.CollegeName = db.College.ToList();

        //    //scoard.batchList=db
        //    return View();
        //}

        //public void GetfilterBatch(long selectedCollegeName = 0)
        //{
        //    College college = new College();
        //    ScoreCardViewModel scoard = new ScoreCardViewModel();
        //    IQueryable<Batch> batchStudents = db.Batch;
        //    //BranchViewModel BranchInstituteViewModel = new BranchViewModel();
        //    if (selectedCollegeName != 0)
        //    {
        //        college = db.College.Find(selectedCollegeName);
        //        if (college != null)
        //        {
        //            List<Batch> batchList = db.Batch.Where(x => x.CollegeID == college.ID).ToList();
        //            //List<Batch> batch = batchStudents.Where(x => x.CollegeID == college.ID).ToList();
        //            List<BatchViewModel> batchViewModelList = (from b in batchList
        //                                                       select new BatchViewModel
        //                                                       {
        //                                                           ID = b.ID,
        //                                                           Name = b.Name,
        //                                                           //StudentCount = b.BatchStudent.Select(x => x.Student).Count(),
        //                                                       }).ToList();

        //            scoard.batchList = sh.SortBatchViewModelList(batchViewModelList);
        //        }
        //    }

        //    string result = JsonConvert.SerializeObject(scoard);
        //    Response.Write(result);
        //}

        //public void GetfilterQuestion(string selectedBatch = "")
        //{
        //    Batch batch = new Batch();
        //    ScoreCardViewModel scard = new ScoreCardViewModel();
        //    List<QuestionPaperViewModel> Qlist = new List<QuestionPaperViewModel>();
        //    IQueryable<QuestionPaper> questionPaper = db.QuestionPaper;

        //    if (selectedBatch != "" || selectedBatch != null)
        //    {
        //        List<string> BatchIDList = selectedBatch.Split(',').ToList();
        //        for (int i = 0; i < BatchIDList.Count(); i++)
        //        {
        //            if (BatchIDList[i] != "" && BatchIDList[i] != null)
        //            {
        //                long batchID = Convert.ToInt64(BatchIDList[i]);
        //                batch = db.Batch.Find(batchID);
        //                if (batch != null)
        //                {
        //                    IQueryable<QuestionPaper> questionList = db.BatchAssessment.Where(x => x.BatchID == batch.ID).Select(x => x.QuestionPaper).Distinct().AsQueryable();
        //                    List<QuestionPaperViewModel> questionPaqperViewModelList = (from q in questionList
        //                                                                                select new QuestionPaperViewModel
        //                                                               {
        //                                                                   ID = q.ID,
        //                                                                   Name = q.Name,
        //                                                                   //StudentCount = b.BatchStudent.Select(x => x.Student).Count(),
        //                                                               }).ToList();
        //                    //  scard.questionList = sh.SortQuestionPaperViewModelList(questionPaqperViewModelList);
        //                    Qlist.AddRange(questionPaqperViewModelList);
        //                }
        //            }
        //        }
        //    }
        //    scard.questionList = Qlist;
        //    string result = JsonConvert.SerializeObject(scard);
        //    Response.Write(result);

        //}


        //public ActionResult ScoreCard(string BatchIDs = "", long CollegeID = 0, string QuestionIDs = "")
        //{
        //    List<string> BatchIDList = BatchIDs.Split(',').ToList();
        //    List<string> QuestionIDList = QuestionIDs.Split(',').ToList();

        //    ScoreCardViewModel score = new ScoreCardViewModel();
        //    List<CollegeViewModel> collegeViewModelList = new List<CollegeViewModel>();


        //    IQueryable<College> college = db.College.Where(x => x.ID == CollegeID).AsQueryable();
        //    //List<CollegeViewModel> collegeViewModelList1 = (from q in college
        //    //                                                select new CollegeViewModel
        //    //                                                            {
        //    //                                                                ID = q.ID,
        //    //                                                                Name = q.Name,
        //    //                                                                //StudentCount = b.BatchStudent.Select(x => x.Student).Count(),
        //    //                                                            }).ToList();
        //    foreach (College c in college)
        //    {
        //        CollegeViewModel cmodel = new CollegeViewModel();
        //        cmodel.Name = c.Name;
        //        cmodel.ID = c.ID;
        //        List<BatchViewModel> batchList = new List<BatchViewModel>();
        //        for (int j = 0; j < BatchIDList.Count(); j++)
        //        {
        //            long BID = Convert.ToInt64(BatchIDList[j]);
        //            IQueryable<Batch> batch = c.Batch.Where(x => x.ID == BID).AsQueryable();
        //            foreach (Batch b in batch)
        //            {
        //                BatchViewModel batchModel = new BatchViewModel();
        //                batchModel.Name = b.Name;
        //                batchModel.ID = b.ID;
        //                List<QuestionPaperViewModel> QuestionList = new List<QuestionPaperViewModel>();
        //                for (int k = 0; k < QuestionIDList.Count(); k++)
        //                {
        //                    long QiD = Convert.ToInt64(QuestionIDList[k]);
        //                    IQueryable<QuestionPaper> QPLIst = db.BatchAssessment.Where(x => x.BatchID == b.ID).Select(x => x.QuestionPaper).Where(z => z.ID == QiD).Distinct().AsQueryable();

        //                    foreach (QuestionPaper q in QPLIst)
        //                    {
        //                        QuestionPaperViewModel qModel = new QuestionPaperViewModel();
        //                        qModel.Name = q.Name;
        //                        //
        //                        List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();
        //                        IQueryable<QuestionPaperSection> questionPaperSectionList = db.QuestionPaperSection.Where(x => x.AssessmentID == q.ID).AsQueryable();
        //                        foreach (QuestionPaperSection qpSection in questionPaperSectionList)
        //                        {
        //                            QuestionPaperSectionViewModel QPSectionModel = new QuestionPaperSectionViewModel();
        //                            QPSectionModel.Name = qpSection.Name;
        //                            questionPaperSectionViewModelList.Add(QPSectionModel);
        //                        }
        //                        //
        //                        List<StudentViewModel> studentList = new List<StudentViewModel>();
        //                        List<QuestionPaperSectionViewModel> questionSectionList = new List<QuestionPaperSectionViewModel>();
        //                        IQueryable<Student> SList = db.BatchStudent.Where(x => x.BatchID == b.ID).Select(s => s.Student).AsQueryable();
        //                        foreach (Student stud in SList)
        //                        {
        //                            StudentViewModel studModel = new StudentViewModel();
        //                            studModel.Name = stud.Name;
        //                            studModel.QuestionPaperSectionViewModel = ScoreCardReport(q.ID, stud.ID);
        //                            studentList.Add(studModel);

        //                            //QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
        //                            //questionPaperSectionViewModel = ScoreCardReport(q.ID, stud.ID);
        //                            //questionSectionList.Add(questionPaperSectionViewModel);

        //                        }

        //                        qModel.questionPaperSectionCountList = questionSectionList;
        //                        qModel.questionPaperSectionViewModelList = questionPaperSectionViewModelList;
        //                        qModel.studentViewModelList = studentList;
        //                        QuestionList.Add(qModel);
        //                    }
        //                }
        //                batchModel.AssessmentViewModelList = QuestionList;
        //                batchList.Add(batchModel);
        //            }
        //        }
        //        cmodel.BatchViewModelList = batchList;
        //        collegeViewModelList.Add(cmodel);

        //    }
        //    ViewBag.CollegeName = db.College.ToList();
        //    score.collegeViewModelList = collegeViewModelList;
        //    //scoard.batchList=db
        //    return View(score);
        //}

        //public QuestionPaperSectionViewModel ScoreCardReport(long QiD = 0, long studentID = 0)
        //{
        //    QuestionPaper questionPaper = db.QuestionPaper.Find(QiD);
        //    List<QuestionPaperSection> questionPaperSectionList = db.QuestionPaperSection.Where(x => x.AssessmentID == questionPaper.ID).ToList();
        //    List<QuestionPaperSectionQuestion> questionPaperSectionQuestionList = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).ToList();

        //    UserProfile userProfile = db.UserProfile.Where(up => up.StudentID == studentID).FirstOrDefault();
        //    UserAssessment userAssessment = userProfile.UserAssessment.Where(ua => ua.AssessmentID == QiD).FirstOrDefault();
        //    ReportViewModel.QuestionPaperReportModel assessmentReportModel = new ReportViewModel.QuestionPaperReportModel();
        //    QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
        //    List<QuestionPaperSectionViewModel> qpsvmL = new List<QuestionPaperSectionViewModel>();
        //    foreach (QuestionPaperSection qps in questionPaperSectionList)
        //    {
        //        QuestionPaperSectionViewModel qpsvm = new QuestionPaperSectionViewModel();
        //        if (userAssessment != null)
        //        {
        //            int totalQuestions = questionPaper.QuestionPaperSection.Where(x => x.ID == qps.ID).SelectMany(ass => ass.QuestionPaperSectionQuestion).Count();
        //            qpsvm.TotalQuestionCount = totalQuestions;
        //            //qpsvm.UserAnsweredCount = userAssessment.AssessmentSnapshot.Select(x => x.Question).Select(y => y.QuestionPaperSectionQuestion.Select(z => z.AssessmentSectionID == qps.ID)).Distinct().Count();
        //            qpsvm.UserAnsweredCount = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false)).Select(x => x.Question).Select(y => y.QuestionPaperSectionQuestion.Select(z => z.AssessmentSectionID == qps.ID)).Distinct().Count();        //Getting correct answer count for single select question              
        //            UserSectionStatus userSectionStatus = userAssessment.UserSectionStatus.Where(x => x.SectionID == qps.ID).FirstOrDefault();        //Getting correct answer count for single select question
        //            qpsvm.TotalMarksScored = userSectionStatus != null ? userSectionStatus.Marks ?? 0 : 0;
        //            qpsvm.UnAnswerQuestions = totalQuestions - qpsvm.UserAnsweredCount;
        //        }
        //        else
        //        {
        //            qpsvm.TotalQuestionCount = 0;
        //            qpsvm.UserAnsweredCount = 0;
        //            qpsvm.TotalMarksScored = 0;
        //            qpsvm.UnAnswerQuestions = 0;
        //        }
        //        qpsvmL.Add(qpsvm);
        //    }
        //    questionPaperSectionViewModel.Total = qpsvmL.Sum(x => x.TotalQuestionCount);
        //    questionPaperSectionViewModel.UnAnswer = qpsvmL.Sum(x => x.UnAnswerQuestions);
        //    questionPaperSectionViewModel.Answered = qpsvmL.Sum(x => x.UserAnsweredCount);
        //    questionPaperSectionViewModel.Mark = qpsvmL.Sum(x => x.TotalMarksScored);
        //    questionPaperSectionViewModel.Percentage = qpsvmL.Sum(x => x.Percentage);
        //    questionPaperSectionViewModel.questionPaperSections = qpsvmL;
        //    return questionPaperSectionViewModel;

        //}
        //public ScoreCardViewModel generateHtmlView(string BatchIDs = "", long CollegeID = 0, string QuestionIDs = "")
        //{
        //    List<string> BatchIDList = BatchIDs.Split(',').ToList();
        //    List<string> QuestionIDList = QuestionIDs.Split(',').ToList();

        //    ScoreCardViewModel score = new ScoreCardViewModel();
        //    List<CollegeViewModel> collegeViewModelList = new List<CollegeViewModel>();


        //    IQueryable<College> college = db.College.Where(x => x.ID == CollegeID).AsQueryable();

        //    foreach (College c in college)
        //    {
        //        CollegeViewModel cmodel = new CollegeViewModel();
        //        cmodel.Name = c.Name;
        //        cmodel.ID = c.ID;
        //        List<BatchViewModel> batchList = new List<BatchViewModel>();
        //        for (int j = 0; j < BatchIDList.Count(); j++)
        //        {
        //            long BID = Convert.ToInt64(BatchIDList[j]);
        //            IQueryable<Batch> batch = c.Batch.Where(x => x.ID == BID).AsQueryable();
        //            foreach (Batch b in batch)
        //            {
        //                BatchViewModel batchModel = new BatchViewModel();
        //                batchModel.Name = b.Name;
        //                batchModel.ID = b.ID;
        //                List<QuestionPaperViewModel> QuestionList = new List<QuestionPaperViewModel>();
        //                for (int k = 0; k < QuestionIDList.Count(); k++)
        //                {
        //                    long QiD = Convert.ToInt64(QuestionIDList[k]);
        //                    IQueryable<QuestionPaper> QPLIst = db.BatchAssessment.Where(x => x.BatchID == b.ID).Select(x => x.QuestionPaper).Where(z => z.ID == QiD).Distinct().AsQueryable();

        //                    foreach (QuestionPaper q in QPLIst)
        //                    {
        //                        QuestionPaperViewModel qModel = new QuestionPaperViewModel();
        //                        qModel.Name = q.Name;
        //                        //
        //                        List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();
        //                        IQueryable<QuestionPaperSection> questionPaperSectionList = db.QuestionPaperSection.Where(x => x.AssessmentID == q.ID).AsQueryable();
        //                        foreach (QuestionPaperSection qpSection in questionPaperSectionList)
        //                        {
        //                            QuestionPaperSectionViewModel QPSectionModel = new QuestionPaperSectionViewModel();
        //                            QPSectionModel.Name = qpSection.Name;
        //                            questionPaperSectionViewModelList.Add(QPSectionModel);
        //                        }
        //                        //
        //                        List<StudentViewModel> studentList = new List<StudentViewModel>();
        //                        List<QuestionPaperSectionViewModel> questionSectionList = new List<QuestionPaperSectionViewModel>();
        //                        IQueryable<Student> SList = db.BatchStudent.Where(x => x.BatchID == b.ID).Select(s => s.Student).AsQueryable();
        //                        foreach (Student stud in SList)
        //                        {
        //                            StudentViewModel studModel = new StudentViewModel();
        //                            studModel.Name = stud.Name;
        //                            studModel.QuestionPaperSectionViewModel = ScoreCardReport(q.ID, stud.ID);
        //                            studentList.Add(studModel);

        //                            //QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
        //                            //questionPaperSectionViewModel = ScoreCardReport(q.ID, stud.ID);
        //                            //questionSectionList.Add(questionPaperSectionViewModel);

        //                        }

        //                        qModel.questionPaperSectionCountList = questionSectionList;
        //                        qModel.questionPaperSectionViewModelList = questionPaperSectionViewModelList;
        //                        qModel.studentViewModelList = studentList;
        //                        QuestionList.Add(qModel);
        //                    }
        //                }
        //                batchModel.AssessmentViewModelList = QuestionList;
        //                batchList.Add(batchModel);
        //            }
        //        }
        //        cmodel.BatchViewModelList = batchList;
        //        collegeViewModelList.Add(cmodel);

        //    }
        //    ViewBag.CollegeName = db.College.ToList();
        //    score.collegeViewModelList = collegeViewModelList;
        //    return score;
        //}
        ////public FileStreamResult generateExcelReport(string BatchIDs = "", long CollegeID = 0, string QuestionIDs = "")
        ////{

        ////    ScoreCardViewModel score = new ScoreCardViewModel();

        ////    score = generateHtmlView(BatchIDs, CollegeID, QuestionIDs);
        ////    //string print = "BatchResult_" + score.CollegeName;
        ////    string print = "Score_Card_Detail";
        ////    string ScoreCardName = print;
        ////    string[] ExcelFiles = Directory.GetFiles(Server.MapPath("~/Excel/"), "*.xls").Select(path => Path.GetFileName(path)).ToArray();
        ////    int j = 1;
        ////    for (int i = 0; i < ExcelFiles.Length; i++)
        ////    {
        ////        if (ExcelFiles.Contains(print + ".xls"))
        ////        {
        ////            if (j > 10)
        ////            {
        ////                while (ScoreCardName != print)
        ////                {
        ////                    print = print.Remove(print.Length - 2);
        ////                }
        ////            }
        ////            else
        ////            {
        ////                while (ScoreCardName != print)
        ////                {
        ////                    print = print.Remove(print.Length - 1);
        ////                }
        ////            }
        ////            print = print + j;
        ////            j++;
        ////        }

        ////    }
        ////    string filename = Server.MapPath("~/Excel/" + print + ".xls");
        ////    MemoryStream excelstream = new MemoryStream();
        ////    FileInfo newFile = new FileInfo(filename);
        ////    int colMergeCount = 1;
        ////    using (ExcelPackage xlPackage = new ExcelPackage(newFile))
        ////    {
        ////        ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Score Card " + GetCurrentDateStringforReport());
        ////        foreach (CollegeViewModel c in score.collegeViewModelList)
        ////        {
        ////            foreach (BatchViewModel b in c.BatchViewModelList)
        ////            {
        ////                foreach (QuestionPaperViewModel q in b.AssessmentViewModelList)
        ////                {
        ////                    int temp = (q.questionPaperSectionViewModelList.Count() * 7) + 8;
        ////                    // temp += 7;
        ////                    if (colMergeCount < temp)
        ////                    {
        ////                        colMergeCount = (q.questionPaperSectionViewModelList.Count() * 7) + 8;
        ////                    }

        ////                }
        ////            }
        ////        }
        ////        foreach (CollegeViewModel c in score.collegeViewModelList)
        ////        {
        ////            using (var range = worksheet.Cells[1, 1, 1, colMergeCount])
        ////            {
        ////                range.Value = c.Name;
        ////                range.Merge = true;
        ////                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        ////                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        ////                range.AutoFitColumns();
        ////            }
        ////            int j1 = 2;

        ////            foreach (BatchViewModel b in c.BatchViewModelList)
        ////            {
        ////                using (var range = worksheet.Cells[j1, 1, j1, colMergeCount])
        ////                {
        ////                    range.Value = b.Name;
        ////                    range.Merge = true;
        ////                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        ////                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        ////                    range.AutoFitColumns();
        ////                }

        ////                int k1 = j1 + 1;
        ////                foreach (QuestionPaperViewModel q in b.AssessmentViewModelList)
        ////                {
        ////                    using (var range = worksheet.Cells[k1, 1, k1, colMergeCount])
        ////                    {
        ////                        range.Value = q.Name;
        ////                        range.Merge = true;
        ////                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        ////                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        ////                        range.AutoFitColumns();
        ////                    }
        ////                    using (var range = worksheet.Cells[k1 + 1, 1, k1 + 2, 1])
        ////                    {
        ////                        range.Value = "Student Name";
        ////                        range.Merge = true;
        ////                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        ////                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        ////                        range.AutoFitColumns();
        ////                    }
        ////                    int col = 2;
        ////                    //int limiter = 7;
        ////                    int nextcol = 0;
        ////                    int secListCount = 2;
        ////                    k1++;
        ////                    foreach (QuestionPaperSectionViewModel qpsm in q.questionPaperSectionViewModelList)
        ////                    {
        ////                        nextcol = col + 6;
        ////                        using (var range = worksheet.Cells[k1, col, k1, nextcol])
        ////                        {
        ////                            range.Value = qpsm.Name;
        ////                            range.Merge = true;
        ////                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        ////                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        ////                            range.AutoFitColumns();
        ////                        }
        ////                        col = nextcol + 1;
        ////                        // col++;
        ////                        //i++;
        ////                        //col = col + i + limiter;
        ////                        // limiter += 6;
        ////                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        ////                        {
        ////                            range.Value = "Total Questions";
        ////                            range.Merge = true;
        ////                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        ////                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        ////                            range.AutoFitColumns();
        ////                            secListCount++;
        ////}

        ////public ScoreCardViewModel generateHtmlView(string BatchIDs = "", long CollegeID = 0, string QuestionIDs = "")
        ////{
        ////    List<string> BatchIDList = BatchIDs.Split(',').ToList();
        ////    List<string> QuestionIDList = QuestionIDs.Split(',').ToList();

        ////    ScoreCardViewModel score = new ScoreCardViewModel();
        ////    List<CollegeViewModel> collegeViewModelList = new List<CollegeViewModel>();


        ////    IQueryable<College> college = db.College.Where(x => x.ID == CollegeID).AsQueryable();

        ////    foreach (College c in college)
        ////    {
        ////        CollegeViewModel cmodel = new CollegeViewModel();
        ////        cmodel.Name = c.Name;
        ////        cmodel.ID = c.ID;
        ////        List<BatchViewModel> batchList = new List<BatchViewModel>();
        ////        for (int j = 0; j < BatchIDList.Count(); j++)
        ////        {
        ////            long BID = Convert.ToInt64(BatchIDList[j]);
        ////            IQueryable<Batch> batch = c.Batch.Where(x => x.ID == BID).AsQueryable();
        ////            foreach (Batch b in batch)
        ////            {
        ////                BatchViewModel batchModel = new BatchViewModel();
        ////                batchModel.Name = b.Name;
        ////                batchModel.ID = b.ID;
        ////                List<QuestionPaperViewModel> QuestionList = new List<QuestionPaperViewModel>();
        ////                for (int k = 0; k < QuestionIDList.Count(); k++)
        ////                {
        ////                    long QiD = Convert.ToInt64(QuestionIDList[k]);
        ////                    IQueryable<QuestionPaper> QPLIst = db.BatchAssessment.Where(x => x.BatchID == b.ID).Select(x => x.QuestionPaper).Where(z => z.ID == QiD).Distinct().AsQueryable();

        ////                    foreach (QuestionPaper q in QPLIst)
        ////                    {
        ////                        QuestionPaperViewModel qModel = new QuestionPaperViewModel();
        ////                        qModel.Name = q.Name;
        ////                        //
        ////                        List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();
        ////                        IQueryable<QuestionPaperSection> questionPaperSectionList = db.QuestionPaperSection.Where(x => x.AssessmentID == q.ID).AsQueryable();
        ////                        foreach (QuestionPaperSection qpSection in questionPaperSectionList)
        ////                        {
        ////                            QuestionPaperSectionViewModel QPSectionModel = new QuestionPaperSectionViewModel();
        ////                            QPSectionModel.Name = qpSection.Name;
        ////                            questionPaperSectionViewModelList.Add(QPSectionModel);
        ////                        }
        ////                        //
        ////                        List<StudentViewModel> studentList = new List<StudentViewModel>();
        ////                        List<QuestionPaperSectionViewModel> questionSectionList = new List<QuestionPaperSectionViewModel>();
        ////                        IQueryable<Student> SList = db.BatchStudent.Where(x => x.BatchID == b.ID).Select(s => s.Student).AsQueryable();
        ////                        foreach (Student stud in SList)
        ////                        {
        ////                            StudentViewModel studModel = new StudentViewModel();
        ////                            studModel.Name = stud.Name;
        ////                            studModel.QuestionPaperSectionViewModel = ScoreCardReport(q.ID, stud.ID);
        ////                            studentList.Add(studModel);

        ////                            //QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
        ////                            //questionPaperSectionViewModel = ScoreCardReport(q.ID, stud.ID);
        ////                            //questionSectionList.Add(questionPaperSectionViewModel);

        ////                        }

        ////                        qModel.questionPaperSectionCountList = questionSectionList;
        ////                        qModel.questionPaperSectionViewModelList = questionPaperSectionViewModelList;
        ////                        qModel.studentViewModelList = studentList;
        ////                        QuestionList.Add(qModel);
        ////                    }
        ////                }
        ////                batchModel.AssessmentViewModelList = QuestionList;
        ////                batchList.Add(batchModel);
        ////            }
        ////        }
        ////        cmodel.BatchViewModelList = batchList;
        ////        collegeViewModelList.Add(cmodel);

        ////    }
        ////    ViewBag.CollegeName = db.College.ToList();
        ////    score.collegeViewModelList = collegeViewModelList;
        ////    return score;
        ////}
        //public FileStreamResult generateExcelReport(string BatchIDs = "", long CollegeID = 0, string QuestionIDs = "")
        //{

        //    ScoreCardViewModel score = new ScoreCardViewModel();

        //    score = generateHtmlView(BatchIDs, CollegeID, QuestionIDs);
        //    //string print = "BatchResult_" + score.CollegeName;
        //    string print = "Score_Card_Detail";
        //    string ScoreCardName = print;
        //    string[] ExcelFiles = Directory.GetFiles(Server.MapPath("~/Excel/"), "*.xls").Select(path => Path.GetFileName(path)).ToArray();
        //    int j = 1;
        //    for (int i = 0; i < ExcelFiles.Length; i++)
        //    {
        //        if (ExcelFiles.Contains(print + ".xls"))
        //        {
        //            if (j > 10)
        //            {
        //                while (ScoreCardName != print)
        //                {
        //                    print = print.Remove(print.Length - 2);
        //                }
        //            }
        //            else
        //            {
        //                while (ScoreCardName != print)
        //                {
        //                    print = print.Remove(print.Length - 1);
        //                }
        //            }
        //            print = print + j;
        //            j++;
        //        }

        //    }
        //    string filename = Server.MapPath("~/Excel/" + print + ".xls");
        //    MemoryStream excelstream = new MemoryStream();
        //    FileInfo newFile = new FileInfo(filename);
        //    int colMergeCount = 1;
        //    using (ExcelPackage xlPackage = new ExcelPackage(newFile))
        //    {
        //        ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Score Card " + GetCurrentDateStringforReport());
        //        foreach (CollegeViewModel c in score.collegeViewModelList)
        //        {
        //            foreach (BatchViewModel b in c.BatchViewModelList)
        //            {
        //                foreach (QuestionPaperViewModel q in b.AssessmentViewModelList)
        //                {
        //                    int temp = (q.questionPaperSectionViewModelList.Count() * 7) + 8;
        //                    // temp += 7;
        //                    if (colMergeCount < temp)
        //                    {
        //                        colMergeCount = (q.questionPaperSectionViewModelList.Count() * 7) + 8;
        //                    }

        //                }
        //            }
        //        }
        //        foreach (CollegeViewModel c in score.collegeViewModelList)
        //        {
        //            using (var range = worksheet.Cells[1, 1, 1, colMergeCount])
        //            {
        //                range.Value = c.Name;
        //                range.Merge = true;
        //                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                range.AutoFitColumns();
        //            }
        //            int j1 = 2;

        //            foreach (BatchViewModel b in c.BatchViewModelList)
        //            {
        //                using (var range = worksheet.Cells[j1, 1, j1, colMergeCount])
        //                {
        //                    range.Value = b.Name;
        //                    range.Merge = true;
        //                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                    range.AutoFitColumns();
        //                }

        //                int k1 = j1 + 1;
        //                foreach (QuestionPaperViewModel q in b.AssessmentViewModelList)
        //                {
        //                    using (var range = worksheet.Cells[k1, 1, k1, colMergeCount])
        //                    {
        //                        range.Value = q.Name;
        //                        range.Merge = true;
        //                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        range.AutoFitColumns();
        //                    }
        //                    using (var range = worksheet.Cells[k1 + 1, 1, k1 + 2, 1])
        //                    {
        //                        range.Value = "Student Name";
        //                        range.Merge = true;
        //                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        range.AutoFitColumns();
        //                    }
        //                    int col = 2;
        //                    //int limiter = 7;
        //                    int nextcol = 0;
        //                    int secListCount = 2;
        //                    k1++;
        //                    foreach (QuestionPaperSectionViewModel qpsm in q.questionPaperSectionViewModelList)
        //                    {
        //                        nextcol = col + 6;
        //                        using (var range = worksheet.Cells[k1, col, k1, nextcol])
        //                        {
        //                            range.Value = qpsm.Name;
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                        }
        //                        col = nextcol + 1;
        //                        // col++;
        //                        //i++;
        //                        //col = col + i + limiter;
        //                        // limiter += 6;
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Total Questions";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;
        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Anwered";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;

        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "UnAnwered";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;

        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Correct";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;

        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Incorrect";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;
        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Score";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;

        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Percentage";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;

        //                        }

        //                        //
        //                        //int nextcol = col + 6;
        //                        using (var range = worksheet.Cells[k1, col, k1, nextcol + 7])
        //                        {
        //                            range.Value = "Total";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Total Questions";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;
        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Anwered";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;

        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "UnAnwered";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;

        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Correct";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;

        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Incorrect";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;
        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Score";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;

        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Percentage";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;

        //                        }

        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "UnAnwered";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;

        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Correct";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;

        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Incorrect";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;
        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Score";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;

        //                        }
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Percentage";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;

        //                        }

        //                    }
        //                    //
        //                    //int nextcol = col + 6;
        //                    using (var range = worksheet.Cells[k1, col, k1, nextcol + 7])
        //                    {
        //                        range.Value = "Total";
        //                        range.Merge = true;
        //                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        range.AutoFitColumns();
        //                    }
        //                    using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                    {
        //                        range.Value = "Total Questions";
        //                        range.Merge = true;
        //                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        range.AutoFitColumns();
        //                        secListCount++;
        //                    }
        //                    using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                    {
        //                        range.Value = "Anwered";
        //                        range.Merge = true;
        //                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        range.AutoFitColumns();
        //                        secListCount++;

        //                    }
        //                    using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                    {
        //                        range.Value = "UnAnwered";
        //                        range.Merge = true;
        //                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        range.AutoFitColumns();
        //                        secListCount++;

        //                    }
        //                    using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                    {
        //                        range.Value = "Correct";
        //                        range.Merge = true;
        //                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        range.AutoFitColumns();
        //                        secListCount++;

        //                    }
        //                    using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                    {
        //                        range.Value = "Incorrect";
        //                        range.Merge = true;
        //                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        range.AutoFitColumns();
        //                        secListCount++;
        //                    }
        //                    using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                    {
        //                        range.Value = "Score";
        //                        range.Merge = true;
        //                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        range.AutoFitColumns();
        //                        secListCount++;

        //                    }
        //                    using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                    {
        //                        range.Value = "Percentage";
        //                        range.Merge = true;
        //                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        range.AutoFitColumns();
        //                        secListCount++;

        //                    }

        //                    //
        //                    k1 = k1 + 2;
        //                    foreach (StudentViewModel stud in q.studentViewModelList)
        //                    {
        //                        using (var range = worksheet.Cells[k1, 1])
        //                        {
        //                            range.Value = stud.Name;
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                        }
        //                        secListCount = 2;

        //                        foreach (var item in stud.QuestionPaperSectionViewModel.questionPaperSections)
        //                        {
        //                            using (var range = worksheet.Cells[k1, secListCount])
        //                            {
        //                                range.Value = item.TotalQuestionCount;
        //                                range.Merge = true;
        //                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                                range.AutoFitColumns();
        //                                secListCount++;
        //                            }
        //                            using (var range = worksheet.Cells[k1, secListCount])
        //                            {
        //                                range.Value = item.UserAnsweredCount;
        //                                range.Merge = true;
        //                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                                range.AutoFitColumns();
        //                                secListCount++;
        //                            }
        //                            using (var range = worksheet.Cells[k1, secListCount])
        //                            {
        //                                range.Value = item.UnAnswerQuestions;
        //                                range.Merge = true;
        //                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                                range.AutoFitColumns();
        //                                secListCount++;
        //                            }
        //                            using (var range = worksheet.Cells[k1, secListCount])
        //                            {
        //                                range.Value = item.CorrectAnswerCount;
        //                                range.Merge = true;
        //                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                                range.AutoFitColumns();
        //                                secListCount++;
        //                            }
        //                            using (var range = worksheet.Cells[k1, secListCount])
        //                            {
        //                                range.Value = item.InCorrectAnswer;
        //                                range.Merge = true;
        //                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                                range.AutoFitColumns();
        //                                secListCount++;
        //                            }
        //                            using (var range = worksheet.Cells[k1, secListCount])
        //                            {
        //                                range.Value = item.TotalMarksScored;
        //                                range.Merge = true;
        //                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                                range.AutoFitColumns();
        //                                secListCount++;
        //                            }
        //                            using (var range = worksheet.Cells[k1, secListCount])
        //                            {
        //                                range.Value = item.Percentage + "%";
        //                                range.Merge = true;
        //                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                                range.AutoFitColumns();
        //                                secListCount++;
        //                            }

        //                        }
        //                        using (var range = worksheet.Cells[k1, secListCount])
        //                        {
        //                            range.Value = stud.QuestionPaperSectionViewModel.Total;
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;
        //                        }
        //                        using (var range = worksheet.Cells[k1, secListCount])
        //                        {
        //                            range.Value = stud.QuestionPaperSectionViewModel.Answered;
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;
        //                        }
        //                        using (var range = worksheet.Cells[k1, secListCount])
        //                        {
        //                            range.Value = stud.QuestionPaperSectionViewModel.UnAnswer;
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;
        //                        }
        //                        using (var range = worksheet.Cells[k1, secListCount])
        //                        {
        //                            range.Value = stud.QuestionPaperSectionViewModel.CorrectAnswer;
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;
        //                        }
        //                        using (var range = worksheet.Cells[k1, secListCount])
        //                        {
        //                            range.Value = stud.QuestionPaperSectionViewModel.InCorrectAnswers;
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;
        //                        }
        //                        using (var range = worksheet.Cells[k1, secListCount])
        //                        {
        //                            range.Value = stud.QuestionPaperSectionViewModel.Mark;
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;
        //                        }
        //                        using (var range = worksheet.Cells[k1, secListCount])
        //                        {
        //                            range.Value = stud.QuestionPaperSectionViewModel.MarksPercentage + "%";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;
        //                        }
        //                        k1++;

        //                    }
        //                    j1 = k1;


        //                }
        //                j1++;
        //            }
        //        }
        //        xlPackage.Save();
        //    }

        //    return SendReportAsExcel(excelstream, print);
        //    //return RedirectToAction("ScoreCardIndex");

        //}
        //public string GetCurrentDateStringforReport()
        //{
        //    return DateTime.Now.ToString("dd-MM-yyy hh:mm:ss");
        //}
        //#endregion

        #region ScoreCard
        public ActionResult ScoreCardIndex()
        {
         
            //ViewBag.CollegeName = db.College.OrderBy(c=>c.Name).ToList();
            int userid = GetCurrentUserID();
            OAP.Domain.UserProfile userProfile = db.UserProfile.Where(x => x.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            if (userProfile != null)
            {
                // bool test = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                ViewBag.CanScheduleRetest = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
            }
            // OAP.Domain.UserProfile userProfile = db.UserProfile.Where(x => x.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            if (userProfile != null)
            {
                //bool test = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                ViewBag.CanScheduleRetest = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
            }
            //scoard.batchList=db
            return View();
        }

        public void GetfilterBatch(long selectedCollegeName = 0)
        {
            College college = new College();
            ScoreCardViewModel scoard = new ScoreCardViewModel();
            List<Batch> batchList = db.Batch.ToList();
            //BranchViewModel BranchInstituteViewModel = new BranchViewModel();
            if (selectedCollegeName != 0)
            {
                college = db.College.Find(selectedCollegeName);
                if (college != null)
                {
                    batchList = db.Batch.Where(x => x.CollegeID == college.ID).ToList();
                    //List<Batch> batch = batchStudents.Where(x => x.CollegeID == college.ID).ToList();

                }
            }
            if (batchList != null)
            {
                List<BatchViewModel> batchViewModelList = (from b in batchList
                                                           select new BatchViewModel
                                                           {
                                                               ID = b.ID,
                                                               Name = b.Name,
                                                               //StudentCount = b.BatchStudent.Select(x => x.Student).Count(),
                                                           }).ToList();

                scoard.batchList = batchViewModelList;
            }

            string result = JsonConvert.SerializeObject(scoard);
            Response.Write(result);
        }

        public void GetfilterQuestion(string selectedBatch = "")
        {
            Batch batch = new Batch();
            ScoreCardViewModel scard = new ScoreCardViewModel();
            List<QuestionPaperViewModel> Qlist = new List<QuestionPaperViewModel>();
            IQueryable<QuestionPaper> questionPaper = db.QuestionPaper;

            if (selectedBatch != "" || selectedBatch != null)
            {
                List<string> BatchIDList = selectedBatch.Split(',').ToList();
                for (int i = 0; i < BatchIDList.Count(); i++)
                {
                    if (BatchIDList[i] != "" && BatchIDList[i] != null)
                    {
                        long batchID = Convert.ToInt64(BatchIDList[i]);
                        batch = db.Batch.Find(batchID);
                        if (batch != null)
                        {
                            IQueryable<QuestionPaper> questionList = db.BatchAssessment.Where(x => x.BatchID == batch.ID).Select(x => x.QuestionPaper).Distinct().AsQueryable();
                            List<QuestionPaperViewModel> questionPaqperViewModelList = (from q in questionList
                                                                                        select new QuestionPaperViewModel
                                                                                        {
                                                                                            ID = q.ID,
                                                                                            Name = q.Name,
                                                                                            //StudentCount = b.BatchStudent.Select(x => x.Student).Count(),
                                                                                        }).ToList();
                            //  scard.questionList = sh.SortQuestionPaperViewModelList(questionPaqperViewModelList);
                            Qlist.AddRange(questionPaqperViewModelList);
                        }
                    }
                }
            }
            scard.questionList = Qlist;
            string result = JsonConvert.SerializeObject(scard);
            Response.Write(result);

        }

        public ActionResult ScoreCard(string BatchIDs = "", string QuestionIDs = "")
        {
            if (BatchIDs != "" && QuestionIDs != "")
            {
                List<string> BatchIDList = BatchIDs.Split(',').ToList();
                List<string> QuestionIDList = QuestionIDs.Split(',').ToList();

                ScoreCardViewModel score = new ScoreCardViewModel();
                List<CollegeViewModel> collegeViewModelList = new List<CollegeViewModel>();


                //List<CollegeViewModel> collegeViewModelList1 = (from q in college
                //                                                select new CollegeViewModel
                //                                                            {
                //                                                                ID = q.ID,
                //                                                                Name = q.Name,
                //                                                                //StudentCount = b.BatchStudent.Select(x => x.Student).Count(),
                //                                                            }).ToList();

                List<BatchViewModel> batchList = new List<BatchViewModel>();
                for (int j = 0; j < BatchIDList.Count(); j++)
                {
                    long BID = Convert.ToInt64(BatchIDList[j]);
                    IQueryable<Batch> batch = db.Batch.Where(x => x.ID == BID).AsQueryable();
                    foreach (Batch b in batch)
                    {
                        BatchViewModel batchModel = new BatchViewModel();
                        batchModel.Name = b.Name;
                        batchModel.ID = b.ID;
                        List<QuestionPaperViewModel> QuestionList = new List<QuestionPaperViewModel>();
                        for (int k = 0; k < QuestionIDList.Count(); k++)
                        {
                            long QiD = Convert.ToInt64(QuestionIDList[k]);
                            IQueryable<QuestionPaper> QPLIst = db.BatchAssessment.Where(x => x.BatchID == b.ID).Select(x => x.QuestionPaper).Where(z => z.ID == QiD).Distinct().AsQueryable();

                            foreach (QuestionPaper q in QPLIst)
                            {
                                QuestionPaperViewModel qModel = new QuestionPaperViewModel();
                                qModel.Name = q.Name;
                                //
                                List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();
                                IQueryable<QuestionPaperSection> questionPaperSectionList = db.QuestionPaperSection.Where(x => x.AssessmentID == q.ID).AsQueryable();
                                foreach (QuestionPaperSection qpSection in questionPaperSectionList)
                                {
                                    QuestionPaperSectionViewModel QPSectionModel = new QuestionPaperSectionViewModel();
                                    QPSectionModel.Name = qpSection.Name;
                                    questionPaperSectionViewModelList.Add(QPSectionModel);
                                }
                                //
                                List<StudentViewModel> studentList = new List<StudentViewModel>();
                                List<QuestionPaperSectionViewModel> questionSectionList = new List<QuestionPaperSectionViewModel>();
                                IQueryable<Student> SList = db.BatchStudent.Where(x => x.BatchID == b.ID).Select(s => s.Student).AsQueryable();
                                foreach (Student stud in SList)
                                {
                                    StudentViewModel studModel = new StudentViewModel();
                                    studModel.FullName = stud.Name;
                                    studModel.UniversityRegistryNumber = stud.UniversityRegisterNumber;
                                    studModel.QuestionPaperSectionViewModel = ScoreCardReport(q.ID, stud.ID);
                                    studentList.Add(studModel);

                                    //QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
                                    //questionPaperSectionViewModel = ScoreCardReport(q.ID, stud.ID);
                                    //questionSectionList.Add(questionPaperSectionViewModel);

                                }

                                qModel.questionPaperSectionCountList = questionSectionList;
                                qModel.questionPaperSectionViewModelList = questionPaperSectionViewModelList;
                                qModel.studentViewModelList = studentList;
                                QuestionList.Add(qModel);
                            }
                        }
                        batchModel.QuestionPaperViewModelList = QuestionList;
                        batchList.Add(batchModel);
                    }
                }
                score.batchList = batchList;
                OAP.Domain.UserProfile userProfile = db.UserProfile.Where(x => x.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
                if (userProfile != null)
                {
                    // bool test = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                    ViewBag.CanScheduleRetest = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                }

                return View(score);
            }
            else
            {
                //ViewBag.CollegeName = db.College.OrderBy(c=>c.Name).ToList();
                int userid = GetCurrentUserID();
                OAP.Domain.UserProfile userProfile = db.UserProfile.Where(x => x.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
                if (userProfile != null)
                {
                    // bool test = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                    ViewBag.CanScheduleRetest = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                }
                // OAP.Domain.UserProfile userProfile = db.UserProfile.Where(x => x.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
                if (userProfile != null)
                {
                    //bool test = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                    ViewBag.CanScheduleRetest = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                }
                //scoard.batchList=db

                return RedirectToAction("ScoreCardIndex");
            }
        }

        public QuestionPaperSectionViewModel ScoreCardReport(long QiD = 0, long studentID = 0)
        {
            QuestionPaper questionPaper = db.QuestionPaper.Find(QiD);
            List<QuestionPaperSection> questionPaperSectionList = db.QuestionPaperSection.Where(x => x.AssessmentID == questionPaper.ID).ToList();
            List<QuestionPaperSectionQuestion> questionPaperSectionQuestionList = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).ToList();

            UserProfile userProfile = db.UserProfile.Where(up => up.StudentID == studentID).FirstOrDefault();
            UserAssessment userAssessment = userProfile.UserAssessment.Where(ua => ua.AssessmentID == QiD).FirstOrDefault();
            ReportViewModel.QuestionPaperReportModel assessmentReportModel = new ReportViewModel.QuestionPaperReportModel();
            QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
            List<QuestionPaperSectionViewModel> qpsvmL = new List<QuestionPaperSectionViewModel>();
            foreach (QuestionPaperSection qps in questionPaperSectionList)
            {
                QuestionPaperSectionViewModel qpsvm = new QuestionPaperSectionViewModel();
                if (userAssessment != null)
                {
                    int totalQuestions = questionPaper.QuestionPaperSection.Where(x => x.ID == qps.ID).SelectMany(ass => ass.QuestionPaperSectionQuestion).Count();
                    qpsvm.TotalQuestionCount = totalQuestions;
                    //qpsvm.UserAnsweredCount = userAssessment.AssessmentSnapshot.Select(x => x.Question).Select(y => y.QuestionPaperSectionQuestion.Select(z => z.AssessmentSectionID == qps.ID)).Distinct().Count();
                    // int UserAnswered = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false)).Select(x => x.Question).Select(y => y.QuestionPaperSectionQuestion.Select(z => z.AssessmentSectionID == qps.ID)).Distinct().Count();        //Getting correct answer count for single select question              
                    int UserAnswered = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null))).Select(x => x.Question).Select(y => y.QuestionPaperSectionQuestion.Select(z => z.AssessmentSectionID == qps.ID)).Distinct().Count();        //Getting correct answer count for single select question              
                    qpsvm.UserAnsweredCount = UserAnswered;
                    UserSectionStatus userSectionStatus = userAssessment.UserSectionStatus.Where(x => x.SectionID == qps.ID).FirstOrDefault();        //Getting correct answer count for single select question
                    qpsvm.TotalMarksScored = userSectionStatus != null ? userSectionStatus.Marks ?? 0 : 0;
                    qpsvm.UnAnswerQuestions = totalQuestions - qpsvm.UserAnsweredCount;
                    //qpsvm.CorrectAnswerCount = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false)).Count();        //Getting correct answer count for single select question
                    qpsvm.CorrectAnswerCount = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) && (ass.Answer.IsCorrect ?? false))).Select(x => x.Question).Select(y => y.QuestionPaperSectionQuestion.Select(z => z.AssessmentSectionID == qps.ID)).Distinct().Count();
                    qpsvm.InCorrectAnswer = UserAnswered - qpsvm.CorrectAnswerCount;
                    qpsvm.Percentage = (qpsvm.TotalMarksScored / totalQuestions) * 100;
                    qpsvm.Percentage = Math.Round(qpsvm.Percentage, 2);

                    //int totalQuestions = questionPaper.QuestionPaperSection.SelectMany(ass => ass.QuestionPaperSectionQuestion).Count();
                    //assessmentReportModel.TotalQuestions = totalQuestions;
                    //int attendedQuestions = userAssessment.AssessmentSnapshot.Select(x => x.Question).Distinct().Count(); 
                    //assessmentReportModel.AttentedQuestions = attendedQuestions;
                    ////int correctAnswers = userProfile.UserAssessment.Where(ua=>ua.AssessmentID==id && ua.AssessmentSnapshot.Where(ass=>ass.Answer.IsCorrect??false).Count()>0).Select(x => x.AssessmentSnapshot).Count();
                    //int correctAnswersCount = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();        //Getting correct answer count for single select question
                    //int totalTimeTakenForCorrectAnswer = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Sum(x => x.TimeTaken ?? 0);      //Get the time of the single select question type


                    //List<Question> multiSelectQuestionList = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionType.Name.ToLower().Equals("multi select")).Select(x => x.Question).Distinct().ToList();
                    //foreach (Question question in multiSelectQuestionList)
                    //{
                    //    List<Answer> correctAnswerList = question.Answer.Where(a => a.IsCorrect ?? false).ToList();
                    //    List<Answer> userAnsweredList = question.AssessmentSnapshot.Where(asn => (asn.Answer != null) ? asn.Answer.IsCorrect ?? false : false).Select(x => x.Answer).ToList();
                    //    if (correctAnswerList.Except(userAnsweredList).Count() > 0 || userAnsweredList.Except(correctAnswerList).Count() > 0)
                    //    {                                                                       //some of the answers are incorrect

                    //    }
                    //    else
                    //    {                                                                       //All the answers are correct
                    //        correctAnswersCount++;
                    //        totalTimeTakenForCorrectAnswer += question.AssessmentSnapshot.FirstOrDefault().TimeTaken ?? 0;
                    //    }
                    //}

                    //assessmentReportModel.CorrectAnswers = correctAnswersCount;

                    //assessmentReportModel.TimeTakenForCorrectAnswer = totalTimeTakenForCorrectAnswer;

                    //decimal marksPercentage = 0;
                    //if (totalQuestions != 0)
                    //    marksPercentage = ((decimal)correctAnswersCount / (decimal)totalQuestions) * 100;
                    //assessmentReportModel.MarksPercentage = Math.Round(marksPercentage, 2);

                }
                else
                {
                    qpsvm.TotalQuestionCount = 0;
                    qpsvm.UserAnsweredCount = 0;
                    qpsvm.TotalMarksScored = 0;
                    qpsvm.UnAnswerQuestions = 0;
                    qpsvm.CorrectAnswerCount = 0;
                    qpsvm.InCorrectAnswer = 0;
                    qpsvm.Percentage = 0;




                }
                qpsvmL.Add(qpsvm);
            }
            questionPaperSectionViewModel.Total = qpsvmL.Sum(x => x.TotalQuestionCount);
            questionPaperSectionViewModel.UnAnswer = qpsvmL.Sum(x => x.UnAnswerQuestions);
            questionPaperSectionViewModel.Answered = qpsvmL.Sum(x => x.UserAnsweredCount);
            questionPaperSectionViewModel.CorrectAnswer = qpsvmL.Sum(x => x.CorrectAnswerCount);
            questionPaperSectionViewModel.InCorrectAnswers = qpsvmL.Sum(x => x.InCorrectAnswer);
            questionPaperSectionViewModel.Mark = qpsvmL.Sum(x => x.TotalMarksScored);
            if (questionPaperSectionViewModel.Total != 0)
            {
                questionPaperSectionViewModel.MarksPercentage = (questionPaperSectionViewModel.Mark / questionPaperSectionViewModel.Total) * 100;
                questionPaperSectionViewModel.MarksPercentage = Math.Round(questionPaperSectionViewModel.MarksPercentage, 2);
            }
            else
            {
                questionPaperSectionViewModel.MarksPercentage = 0;
            }
            questionPaperSectionViewModel.questionPaperSections = qpsvmL;
            return questionPaperSectionViewModel;

        }

        public ScoreCardViewModel generateHtmlView(string BatchIDs = "", long CollegeID = 0, string QuestionIDs = "")
        {
            if (BatchIDs != "" && QuestionIDs != "")
            {
                List<string> BatchIDList = BatchIDs.Split(',').ToList();
                List<string> QuestionIDList = QuestionIDs.Split(',').ToList();

                ScoreCardViewModel score = new ScoreCardViewModel();
                List<CollegeViewModel> collegeViewModelList = new List<CollegeViewModel>();


                //List<CollegeViewModel> collegeViewModelList1 = (from q in college
                //                                                select new CollegeViewModel
                //                                                            {
                //                                                                ID = q.ID,
                //                                                                Name = q.Name,
                //                                                                //StudentCount = b.BatchStudent.Select(x => x.Student).Count(),
                //                                                            }).ToList();

                List<BatchViewModel> batchList = new List<BatchViewModel>();
                for (int j = 0; j < BatchIDList.Count(); j++)
                {
                    long BID = Convert.ToInt64(BatchIDList[j]);
                    IQueryable<Batch> batch = db.Batch.Where(x => x.ID == BID).AsQueryable();
                    foreach (Batch b in batch)
                    {
                        BatchViewModel batchModel = new BatchViewModel();
                        batchModel.Name = b.Name;
                        batchModel.ID = b.ID;
                        List<QuestionPaperViewModel> QuestionList = new List<QuestionPaperViewModel>();
                        for (int k = 0; k < QuestionIDList.Count(); k++)
                        {
                            long QiD = Convert.ToInt64(QuestionIDList[k]);
                            IQueryable<QuestionPaper> QPLIst = db.BatchAssessment.Where(x => x.BatchID == b.ID).Select(x => x.QuestionPaper).Where(z => z.ID == QiD).Distinct().AsQueryable();

                            foreach (QuestionPaper q in QPLIst)
                            {
                                QuestionPaperViewModel qModel = new QuestionPaperViewModel();
                                qModel.Name = q.Name;
                                //
                                List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();
                                IQueryable<QuestionPaperSection> questionPaperSectionList = db.QuestionPaperSection.Where(x => x.AssessmentID == q.ID).AsQueryable();
                                foreach (QuestionPaperSection qpSection in questionPaperSectionList)
                                {
                                    QuestionPaperSectionViewModel QPSectionModel = new QuestionPaperSectionViewModel();
                                    QPSectionModel.Name = qpSection.Name;
                                    questionPaperSectionViewModelList.Add(QPSectionModel);
                                }
                                //
                                List<StudentViewModel> studentList = new List<StudentViewModel>();
                                List<QuestionPaperSectionViewModel> questionSectionList = new List<QuestionPaperSectionViewModel>();
                                IQueryable<Student> SList = db.BatchStudent.Where(x => x.BatchID == b.ID).Select(s => s.Student).AsQueryable();
                                foreach (Student stud in SList)
                                {
                                    StudentViewModel studModel = new StudentViewModel();
                                    studModel.FullName = stud.Name;
                                    studModel.UniversityRegistryNumber = stud.UniversityRegisterNumber;
                                    studModel.QuestionPaperSectionViewModel = ScoreCardReport(q.ID, stud.ID);
                                    studentList.Add(studModel);

                                    //QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
                                    //questionPaperSectionViewModel = ScoreCardReport(q.ID, stud.ID);
                                    //questionSectionList.Add(questionPaperSectionViewModel);

                                }

                                qModel.questionPaperSectionCountList = questionSectionList;
                                qModel.questionPaperSectionViewModelList = questionPaperSectionViewModelList;
                                qModel.studentViewModelList = studentList;
                                QuestionList.Add(qModel);
                            }
                        }
                        batchModel.QuestionPaperViewModelList = QuestionList;
                        batchList.Add(batchModel);
                    }
                }
                score.batchList = batchList;
                return score;
            }
            return new ScoreCardViewModel();
        }

        //public FileStreamResult generateExcelReport(string BatchIDs = "", long CollegeID = 0, string QuestionIDs = "")
        //{

        //    ScoreCardViewModel score = new ScoreCardViewModel();

        //    score = generateHtmlView(BatchIDs, CollegeID, QuestionIDs);
        //    //string print = "BatchResult_" + score.CollegeName;
        //    string print = "Score_Card_Detail";
        //    string ScoreCardName = print;
        //    string[] ExcelFiles = Directory.GetFiles(Server.MapPath("~/Excel/"), "*.xls").Select(path => Path.GetFileName(path)).ToArray();
        //    int j = 1;
        //    for (int i = 0; i < ExcelFiles.Length; i++)
        //    {
        //        if (ExcelFiles.Contains(print + ".xls"))
        //        {
        //            if (j > 10)
        //            {
        //                while (ScoreCardName != print)
        //                {
        //                    print = print.Remove(print.Length - 2);
        //                }
        //            }
        //            else
        //            {
        //                while (ScoreCardName != print)
        //                {
        //                    print = print.Remove(print.Length - 1);
        //                }
        //            }
        //            print = print + j;
        //            j++;
        //        }

        //    }
        //    string filename = Server.MapPath("~/Excel/" + print + ".xls");
        //    MemoryStream excelstream = new MemoryStream();
        //    FileInfo newFile = new FileInfo(filename);
        //    int colMergeCount = 1;
        //    using (ExcelPackage xlPackage = new ExcelPackage(newFile))
        //    {
        //        ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Score Card " + GetCurrentDateStringforReport());
        //        foreach (CollegeViewModel c in score.collegeViewModelList)
        //        {
        //            foreach (BatchViewModel b in c.BatchViewModelList)
        //            {
        //                foreach (QuestionPaperViewModel q in b.AssessmentViewModelList)
        //                {
        //                    int temp = (q.questionPaperSectionViewModelList.Count() * 7) + 8;
        //                    // temp += 7;
        //                    if (colMergeCount < temp)
        //                    {
        //                        colMergeCount = (q.questionPaperSectionViewModelList.Count() * 7) + 8;
        //                    }

        //                }
        //            }
        //        }
        //        foreach (CollegeViewModel c in score.collegeViewModelList)
        //        {
        //            using (var range = worksheet.Cells[1, 1, 1, colMergeCount])
        //            {
        //                range.Value = c.Name;
        //                range.Merge = true;
        //                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                range.AutoFitColumns();
        //            }
        //            int j1 = 2;

        //            foreach (BatchViewModel b in c.BatchViewModelList)
        //            {
        //                using (var range = worksheet.Cells[j1, 1, j1, colMergeCount])
        //                {
        //                    range.Value = b.Name;
        //                    range.Merge = true;
        //                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                    range.AutoFitColumns();
        //                }

        //                int k1 = j1 + 1;
        //                foreach (QuestionPaperViewModel q in b.AssessmentViewModelList)
        //                {
        //                    using (var range = worksheet.Cells[k1, 1, k1, colMergeCount])
        //                    {
        //                        range.Value = q.Name;
        //                        range.Merge = true;
        //                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        range.AutoFitColumns();
        //                    }
        //                    using (var range = worksheet.Cells[k1 + 1, 1, k1 + 2, 1])
        //                    {
        //                        range.Value = "Student Name";
        //                        range.Merge = true;
        //                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                        range.AutoFitColumns();
        //                    }
        //                    int col = 2;
        //                    //int limiter = 7;
        //                    int nextcol = 0;
        //                    int secListCount = 2;
        //                    k1++;
        //                    foreach (QuestionPaperSectionViewModel qpsm in q.questionPaperSectionViewModelList)
        //                    {
        //                        nextcol = col + 6;
        //                        using (var range = worksheet.Cells[k1, col, k1, nextcol])
        //                        {
        //                            range.Value = qpsm.Name;
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                        }
        //                        col = nextcol + 1;
        //                        // col++;
        //                        //i++;
        //                        //col = col + i + limiter;
        //                        // limiter += 6;
        //                        using (var range = worksheet.Cells[k1 + 1, secListCount])
        //                        {
        //                            range.Value = "Total Questions";
        //                            range.Merge = true;
        //                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
        //                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
        //                            range.AutoFitColumns();
        //                            secListCount++;
        //}

        //public ScoreCardViewModel generateHtmlView(string BatchIDs = "", long CollegeID = 0, string QuestionIDs = "")
        //{
        //    List<string> BatchIDList = BatchIDs.Split(',').ToList();
        //    List<string> QuestionIDList = QuestionIDs.Split(',').ToList();

        //    ScoreCardViewModel score = new ScoreCardViewModel();
        //    List<CollegeViewModel> collegeViewModelList = new List<CollegeViewModel>();


        //    IQueryable<College> college = db.College.Where(x => x.ID == CollegeID).AsQueryable();

        //    foreach (College c in college)
        //    {
        //        CollegeViewModel cmodel = new CollegeViewModel();
        //        cmodel.Name = c.Name;
        //        cmodel.ID = c.ID;
        //        List<BatchViewModel> batchList = new List<BatchViewModel>();
        //        for (int j = 0; j < BatchIDList.Count(); j++)
        //        {
        //            long BID = Convert.ToInt64(BatchIDList[j]);
        //            IQueryable<Batch> batch = c.Batch.Where(x => x.ID == BID).AsQueryable();
        //            foreach (Batch b in batch)
        //            {
        //                BatchViewModel batchModel = new BatchViewModel();
        //                batchModel.Name = b.Name;
        //                batchModel.ID = b.ID;
        //                List<QuestionPaperViewModel> QuestionList = new List<QuestionPaperViewModel>();
        //                for (int k = 0; k < QuestionIDList.Count(); k++)
        //                {
        //                    long QiD = Convert.ToInt64(QuestionIDList[k]);
        //                    IQueryable<QuestionPaper> QPLIst = db.BatchAssessment.Where(x => x.BatchID == b.ID).Select(x => x.QuestionPaper).Where(z => z.ID == QiD).Distinct().AsQueryable();

        //                    foreach (QuestionPaper q in QPLIst)
        //                    {
        //                        QuestionPaperViewModel qModel = new QuestionPaperViewModel();
        //                        qModel.Name = q.Name;
        //                        //
        //                        List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();
        //                        IQueryable<QuestionPaperSection> questionPaperSectionList = db.QuestionPaperSection.Where(x => x.AssessmentID == q.ID).AsQueryable();
        //                        foreach (QuestionPaperSection qpSection in questionPaperSectionList)
        //                        {
        //                            QuestionPaperSectionViewModel QPSectionModel = new QuestionPaperSectionViewModel();
        //                            QPSectionModel.Name = qpSection.Name;
        //                            questionPaperSectionViewModelList.Add(QPSectionModel);
        //                        }
        //                        //
        //                        List<StudentViewModel> studentList = new List<StudentViewModel>();
        //                        List<QuestionPaperSectionViewModel> questionSectionList = new List<QuestionPaperSectionViewModel>();
        //                        IQueryable<Student> SList = db.BatchStudent.Where(x => x.BatchID == b.ID).Select(s => s.Student).AsQueryable();
        //                        foreach (Student stud in SList)
        //                        {
        //                            StudentViewModel studModel = new StudentViewModel();
        //                            studModel.Name = stud.Name;
        //                            studModel.QuestionPaperSectionViewModel = ScoreCardReport(q.ID, stud.ID);

        //                            //QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
        //                            //questionPaperSectionViewModel = ScoreCardReport(q.ID, stud.ID);
        //                            //questionSectionList.Add(questionPaperSectionViewModel);

        //                        }

        //                        qModel.questionPaperSectionCountList = questionSectionList;
        //                        qModel.questionPaperSectionViewModelList = questionPaperSectionViewModelList;
        //                        qModel.studentViewModelList = studentList;
        //                        QuestionList.Add(qModel);
        //                    }
        //                }
        //                batchModel.AssessmentViewModelList = QuestionList;
        //                batchList.Add(batchModel);
        //            }
        //        }
        //        cmodel.BatchViewModelList = batchList;
        //        collegeViewModelList.Add(cmodel);

        //    }
        //    ViewBag.CollegeName = db.College.ToList();
        //    score.collegeViewModelList = collegeViewModelList;
        //    return score;
        //}

        public FileStreamResult generateExcelReport(string BatchIDs = "", long CollegeID = 0, string QuestionIDs = "")
        {
                ScoreCardViewModel score = new ScoreCardViewModel();

                score = generateHtmlView(BatchIDs, CollegeID, QuestionIDs);
                //string print = "BatchResult_" + score.CollegeName;
                string print = "Score_Card_Detail";
                string ScoreCardName = print;
                if (!Directory.Exists(Server.MapPath("~/Excel/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Excel/"));
                }
                string[] ExcelFiles = Directory.GetFiles(Server.MapPath("~/Excel/"), "*.xls").Select(path => Path.GetFileName(path)).ToArray();
                int j = 1;
                for (int i = 0; i < ExcelFiles.Length; i++)
                {
                    if (ExcelFiles.Contains(print + ".xls"))
                    {
                        if (j > 10)
                        {
                            while (ScoreCardName != print)
                            {
                                print = print.Remove(print.Length - 2);
                            }
                        }
                        else
                        {
                            while (ScoreCardName != print)
                            {
                                print = print.Remove(print.Length - 1);
                            }
                        }
                        print = print + j;
                        j++;
                    }

                }
                string filename = Server.MapPath("~/Excel/" + print + ".xls");
                MemoryStream excelstream = new MemoryStream();
                FileInfo newFile = new FileInfo(filename);
                int colMergeCount = 1;
                using (ExcelPackage xlPackage = new ExcelPackage(newFile))
                {
                    ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Score Card " + GetCurrentDateStringforReport());

                    foreach (BatchViewModel b in score.batchList)
                    {
                        foreach (QuestionPaperViewModel q in b.QuestionPaperViewModelList)
                        {
                            int temp = (q.questionPaperSectionViewModelList.Count() * 7) + 9;
                            // temp += 7;
                            if (colMergeCount < temp)
                            {
                                colMergeCount = (q.questionPaperSectionViewModelList.Count() * 7) + 9;
                            }

                        }
                    }

                    int j1 = 2;

                    foreach (BatchViewModel b in score.batchList)
                    {
                        using (var range = worksheet.Cells[j1, 1, j1, colMergeCount])
                        {
                            range.Value = b.Name;
                            range.Merge = true;
                            range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                            range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            range.AutoFitColumns();
                        }

                        int k1 = j1 + 1;
                        foreach (QuestionPaperViewModel q in b.QuestionPaperViewModelList)
                        {
                            using (var range = worksheet.Cells[k1, 1, k1, colMergeCount])
                            {
                                range.Value = q.Name;
                                range.Merge = true;
                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                range.AutoFitColumns();
                            }
                            using (var range = worksheet.Cells[k1 + 1, 1, k1 + 2, 1])
                            {
                                range.Value = "Student Name";
                                range.Merge = true;
                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                range.AutoFitColumns();
                            }
                            using (var range = worksheet.Cells[k1 + 1, 2, k1 + 2, 2])
                            {
                                range.Value = "Register Number";
                                range.Merge = true;
                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                range.AutoFitColumns();
                            }
                            int col = 3;
                            //int limiter = 7;
                            int nextcol = 0;
                            int secListCount = 3;
                            k1++;
                            foreach (QuestionPaperSectionViewModel qpsm in q.questionPaperSectionViewModelList)
                            {
                                nextcol = col + 6;
                                using (var range = worksheet.Cells[k1, col, k1, nextcol])
                                {
                                    range.Value = qpsm.Name;
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                }
                                col = nextcol + 1;
                                // col++;
                                //i++;
                                //col = col + i + limiter;
                                // limiter += 6;
                                using (var range = worksheet.Cells[k1 + 1, secListCount])
                                {
                                    range.Value = "Total Questions";
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                    secListCount++;
                                }
                                using (var range = worksheet.Cells[k1 + 1, secListCount])
                                {
                                    range.Value = "Answered";
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                    secListCount++;

                                }
                                using (var range = worksheet.Cells[k1 + 1, secListCount])
                                {
                                    range.Value = "UnAnswered";
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                    secListCount++;

                                }
                                using (var range = worksheet.Cells[k1 + 1, secListCount])
                                {
                                    range.Value = "Correct";
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                    secListCount++;

                                }
                                using (var range = worksheet.Cells[k1 + 1, secListCount])
                                {
                                    range.Value = "Incorrect";
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                    secListCount++;
                                }
                                using (var range = worksheet.Cells[k1 + 1, secListCount])
                                {
                                    range.Value = "Score";
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                    secListCount++;

                                }
                                using (var range = worksheet.Cells[k1 + 1, secListCount])
                                {
                                    range.Value = "Percentage";
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                    secListCount++;

                                }

                                //
                                //int nextcol = col + 6;


                                //using (var range = worksheet.Cells[k1 + 1, secListCount])
                                //{
                                //    range.Value = "UnAnwered";
                                //    range.Merge = true;
                                //    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                //    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                //    range.AutoFitColumns();
                                //    secListCount++;

                                //}
                                //using (var range = worksheet.Cells[k1 + 1, secListCount])
                                //{
                                //    range.Value = "Correct";
                                //    range.Merge = true;
                                //    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                //    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                //    range.AutoFitColumns();
                                //    secListCount++;

                                //}
                                //using (var range = worksheet.Cells[k1 + 1, secListCount])
                                //{
                                //    range.Value = "Incorrect";
                                //    range.Merge = true;
                                //    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                //    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                //    range.AutoFitColumns();
                                //    secListCount++;
                                //}
                                //using (var range = worksheet.Cells[k1 + 1, secListCount])
                                //{
                                //    range.Value = "Score";
                                //    range.Merge = true;
                                //    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                //    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                //    range.AutoFitColumns();
                                //    secListCount++;

                                //}
                                //using (var range = worksheet.Cells[k1 + 1, secListCount])
                                //{
                                //    range.Value = "Percentage";
                                //    range.Merge = true;
                                //    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                //    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                //    range.AutoFitColumns();
                                //    secListCount++;

                                //}

                            }
                            using (var range = worksheet.Cells[k1, col, k1, nextcol + 7])
                            {
                                range.Value = "Total";
                                range.Merge = true;
                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                range.AutoFitColumns();
                            }
                            using (var range = worksheet.Cells[k1 + 1, secListCount])
                            {
                                range.Value = "Total Questions";
                                range.Merge = true;
                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                range.AutoFitColumns();
                                secListCount++;
                            }
                            using (var range = worksheet.Cells[k1 + 1, secListCount])
                            {
                                range.Value = "Answered";
                                range.Merge = true;
                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                range.AutoFitColumns();
                                secListCount++;

                            }
                            using (var range = worksheet.Cells[k1 + 1, secListCount])
                            {
                                range.Value = "UnAnswered";
                                range.Merge = true;
                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                range.AutoFitColumns();
                                secListCount++;

                            }
                            using (var range = worksheet.Cells[k1 + 1, secListCount])
                            {
                                range.Value = "Correct";
                                range.Merge = true;
                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                range.AutoFitColumns();
                                secListCount++;

                            }
                            using (var range = worksheet.Cells[k1 + 1, secListCount])
                            {
                                range.Value = "Incorrect";
                                range.Merge = true;
                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                range.AutoFitColumns();
                                secListCount++;
                            }
                            using (var range = worksheet.Cells[k1 + 1, secListCount])
                            {
                                range.Value = "Score";
                                range.Merge = true;
                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                range.AutoFitColumns();
                                secListCount++;

                            }
                            using (var range = worksheet.Cells[k1 + 1, secListCount])
                            {
                                range.Value = "Percentage";
                                range.Merge = true;
                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                range.AutoFitColumns();
                                secListCount++;

                            }
                            //
                            //int nextcol = col + 6;
                            //using (var range = worksheet.Cells[k1, col, k1, nextcol + 7])
                            //{
                            //    range.Value = "Total";
                            //    range.Merge = true;
                            //    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                            //    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //    range.AutoFitColumns();
                            //}
                            //using (var range = worksheet.Cells[k1 + 1, secListCount])
                            //{
                            //    range.Value = "Total Questions";
                            //    range.Merge = true;
                            //    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                            //    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //    range.AutoFitColumns();
                            //    secListCount++;
                            //}
                            //using (var range = worksheet.Cells[k1 + 1, secListCount])
                            //{
                            //    range.Value = "Anwered";
                            //    range.Merge = true;
                            //    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                            //    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //    range.AutoFitColumns();
                            //    secListCount++;

                            //}
                            //using (var range = worksheet.Cells[k1 + 1, secListCount])
                            //{
                            //    range.Value = "UnAnwered";
                            //    range.Merge = true;
                            //    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                            //    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //    range.AutoFitColumns();
                            //    secListCount++;

                            //}
                            //using (var range = worksheet.Cells[k1 + 1, secListCount])
                            //{
                            //    range.Value = "Correct";
                            //    range.Merge = true;
                            //    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                            //    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //    range.AutoFitColumns();
                            //    secListCount++;

                            //}
                            //using (var range = worksheet.Cells[k1 + 1, secListCount])
                            //{
                            //    range.Value = "Incorrect";
                            //    range.Merge = true;
                            //    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                            //    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //    range.AutoFitColumns();
                            //    secListCount++;
                            //}
                            //using (var range = worksheet.Cells[k1 + 1, secListCount])
                            //{
                            //    range.Value = "Score";
                            //    range.Merge = true;
                            //    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                            //    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //    range.AutoFitColumns();
                            //    secListCount++;

                            //}
                            //using (var range = worksheet.Cells[k1 + 1, secListCount])
                            //{
                            //    range.Value = "Percentage";
                            //    range.Merge = true;
                            //    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                            //    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //    range.AutoFitColumns();
                            //    secListCount++;

                            //}

                            //
                            k1 = k1 + 2;
                            foreach (StudentViewModel stud in q.studentViewModelList)
                            {
                                using (var range = worksheet.Cells[k1, 1])
                                {
                                    range.Value = stud.FullName;
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                }

                                using (var range = worksheet.Cells[k1, 2])
                                {
                                    range.Value = stud.UniversityRegistryNumber;
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                }
                                secListCount = 3;

                                foreach (var item in stud.QuestionPaperSectionViewModel.questionPaperSections)
                                {
                                    using (var range = worksheet.Cells[k1, secListCount])
                                    {
                                        range.Value = item.TotalQuestionCount;
                                        range.Merge = true;
                                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        range.AutoFitColumns();
                                        secListCount++;
                                    }
                                    using (var range = worksheet.Cells[k1, secListCount])
                                    {
                                        range.Value = item.UserAnsweredCount;
                                        range.Merge = true;
                                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        range.AutoFitColumns();
                                        secListCount++;
                                    }
                                    using (var range = worksheet.Cells[k1, secListCount])
                                    {
                                        range.Value = item.UnAnswerQuestions;
                                        range.Merge = true;
                                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        range.AutoFitColumns();
                                        secListCount++;
                                    }
                                    using (var range = worksheet.Cells[k1, secListCount])
                                    {
                                        range.Value = item.CorrectAnswerCount;
                                        range.Merge = true;
                                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        range.AutoFitColumns();
                                        secListCount++;
                                    }
                                    using (var range = worksheet.Cells[k1, secListCount])
                                    {
                                        range.Value = item.InCorrectAnswer;
                                        range.Merge = true;
                                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        range.AutoFitColumns();
                                        secListCount++;
                                    }
                                    using (var range = worksheet.Cells[k1, secListCount])
                                    {
                                        range.Value = item.TotalMarksScored;
                                        range.Merge = true;
                                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        range.AutoFitColumns();
                                        secListCount++;
                                    }
                                    using (var range = worksheet.Cells[k1, secListCount])
                                    {
                                        range.Value = item.Percentage + "%";
                                        range.Merge = true;
                                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                        range.AutoFitColumns();
                                        secListCount++;
                                    }

                                }
                                using (var range = worksheet.Cells[k1, secListCount])
                                {
                                    range.Value = stud.QuestionPaperSectionViewModel.Total;
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                    secListCount++;
                                }
                                using (var range = worksheet.Cells[k1, secListCount])
                                {
                                    range.Value = stud.QuestionPaperSectionViewModel.Answered;
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                    secListCount++;
                                }
                                using (var range = worksheet.Cells[k1, secListCount])
                                {
                                    range.Value = stud.QuestionPaperSectionViewModel.UnAnswer;
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                    secListCount++;
                                }
                                using (var range = worksheet.Cells[k1, secListCount])
                                {
                                    range.Value = stud.QuestionPaperSectionViewModel.CorrectAnswer;
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                    secListCount++;
                                }
                                using (var range = worksheet.Cells[k1, secListCount])
                                {
                                    range.Value = stud.QuestionPaperSectionViewModel.InCorrectAnswers;
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                    secListCount++;
                                }
                                using (var range = worksheet.Cells[k1, secListCount])
                                {
                                    range.Value = stud.QuestionPaperSectionViewModel.Mark;
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                    secListCount++;
                                }
                                using (var range = worksheet.Cells[k1, secListCount])
                                {
                                    range.Value = stud.QuestionPaperSectionViewModel.MarksPercentage + "%";
                                    range.Merge = true;
                                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    range.AutoFitColumns();
                                    secListCount++;
                                }
                                k1++;

                            }
                            j1 = k1;


                        }
                        j1++;

                    }
                    xlPackage.Save();
                }
                return SendReportAsExcel(excelstream, print);

            }

        public string GetCurrentDateStringforReport()
        {
            return DateTime.Now.ToString("dd-MM-yyy hh:mm:ss");
        }
        #endregion

        private FileStreamResult SendReportAsExcel(MemoryStream stream, string filename)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            WebClient wc = new WebClient();
            stream = new MemoryStream(wc.DownloadData(Server.MapPath("~/Excel/" + filename + ".xls")));
            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + filename + ".xls");
            FileStreamResult filestream = new FileStreamResult(stream, "application/xls");

            string deletePath = Server.MapPath("~/Excel/" + filename + ".xls");
            if (System.IO.File.Exists(deletePath))
            {
                System.IO.File.Delete(deletePath);
            }

            return filestream;
        }

        #region Section Wise BarGraph

        public ActionResult SectionWiseBarGraph(int id)
        {
            QuestionPaper questionPaper = db.QuestionPaper.Find(id);
            UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            UserAssessment userAssessment = userProfile.UserAssessment.Where(ua => ua.AssessmentID == id).FirstOrDefault();
            List<QuestionPaperSectionQuestion> questionPaperSectionQuestionList = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).ToList();
            ReportViewModel.SectionReportModel sectionReportModel = new ReportViewModel.SectionReportModel();
           // sectionReportModel.StudentName = userProfile.UserName;
            sectionReportModel.StudentName = userProfile.Student.Name;
            sectionReportModel.RegisterNumber = userProfile.Student.UniversityRegisterNumber;
            sectionReportModel.QuestionPaperName = questionPaper.Name;

            int totalQuestions = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).Count();
            sectionReportModel.TotalQuestions = totalQuestions;
            List<QuestionPaperSection> questionPaperSectionListforName = questionPaper.QuestionPaperSection.Distinct().ToList();

            sectionReportModel.Sections = string.Join(",", questionPaperSectionListforName.Select(x => x.Name).ToArray());
            sectionReportModel.Sections = "Max," + sectionReportModel.Sections;

            sectionReportModel.TotalSections = questionPaper.QuestionPaperSection.Count();

            //int attendedQuestions = userAssessment.AssessmentSnapshot.Select(x => x.Question).Distinct().Count();
            //sectionReportModel.AttentedQuestions = attendedQuestions;
            int correctAnswersCount = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();

            List<Question> multiSelectQuestionList = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionType.Name.ToLower().Equals("multi select")).Select(x => x.Question).Distinct().ToList();
            foreach (Question question in multiSelectQuestionList)
            {
                List<Answer> correctAnswerList = question.Answer.Where(a => a.IsCorrect ?? false).ToList();
                List<Answer> userAnsweredList = question.AssessmentSnapshot.Where(asn => (asn.Answer != null) ? asn.Answer.IsCorrect ?? false : false).Select(x => x.Answer).ToList();
                if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
                {                                                                       //All the answers are correct
                    correctAnswersCount++;
                }
            }


            List<ReportViewModel.SectionReportModel> SectionReportModelList = new List<ReportViewModel.SectionReportModel>();
            List<QuestionPaperSection> questionPaperSectionList = questionPaper.QuestionPaperSection.ToList();
            foreach (QuestionPaperSection section in questionPaperSectionList)
            {
                ReportViewModel.SectionReportModel SectionReportModel = new ReportViewModel.SectionReportModel();
                SectionReportModel.SectionName = section.Name;
                int totalQuestionsUnderThisSection = questionPaperSectionQuestionList.Where(asq => asq.AssessmentSectionID == section.ID).Count();
                SectionReportModel.TotalQuestions = totalQuestionsUnderThisSection;

                int attendedQuestionsUnderThisSection = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == section.ID).Count() > 0).Select(x => x.Question).Distinct().Count();
                SectionReportModel.AttendedQuestions = attendedQuestionsUnderThisSection;

                int unattendedQuestionUnderThisSection = totalQuestionsUnderThisSection - attendedQuestionsUnderThisSection;
                SectionReportModel.UnattendedQuestions = unattendedQuestionUnderThisSection;

                int correctAnswerUnderThisSection = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == section.ID).Count() > 0) && (ass.Answer.IsCorrect ?? false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();     //Get only the single select type questions 
                List<Question> multiSelectTypeQuestionList = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == section.ID).Count() > 0) && (ass.Question.QuestionType.Name.ToLower().Equals("multi select"))).Select(x => x.Question).Distinct().ToList();
                foreach (Question multiSelectQuestion in multiSelectTypeQuestionList)
                {
                    List<Answer> correctAnswerList = multiSelectQuestion.Answer.Where(a => a.IsCorrect ?? false).ToList();
                    List<Answer> userAnsweredList = multiSelectQuestion.AssessmentSnapshot.Select(x => x.Answer).ToList();
                    if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
                    {                                                                       //All the answers are correct
                        correctAnswerUnderThisSection++;
                    }
                }
                SectionReportModel.CorrectAnswers = correctAnswerUnderThisSection;

                int wrongAnswerUnderThisSection = attendedQuestionsUnderThisSection - correctAnswerUnderThisSection;
                SectionReportModel.WrongAnswers = wrongAnswerUnderThisSection;

                decimal correctAnswerPercentage = 0;
                if (totalQuestionsUnderThisSection != 0)
                    correctAnswerPercentage = ((decimal)correctAnswerUnderThisSection / (decimal)totalQuestionsUnderThisSection) * 100;
                SectionReportModel.CorrectAnswerPercentage = Math.Round(correctAnswerPercentage, 2);

                //float marksPercentage = 0;
                //if (totalQuestions != 0)
                //    marksPercentage = ((float)correctAnswersCount / (float)totalQuestions) * 100;
                //SectionReportModel.MarksPercentage = marksPercentage;

                SectionReportModelList.Add(SectionReportModel);
            }
            sectionReportModel.SectionReportModelList = SectionReportModelList;
            return View(sectionReportModel);

        }

        #endregion
        // Anu
        #region View Student Report
        public ActionResult ViewStudentReport(long id = 0)
        {
            ViewBag.QuestionID = id;
            ReportViewModel reportViewModel = new ReportViewModel();
            ////reportViewModel.sectionReportModel = GetSectionReportModelForSectionWiseBargraph(id);
            List<QuestionViewModel> questionViewModelList = GetTimeReportForAssessment(id);
            reportViewModel.QuestionViewModelList = questionViewModelList;

            //anu
            //QuestionPaper questionPaper=new QuestionPaper();
            //int userId = GetCurrentUserID();
            //UserProfile userProfile = db.UserProfile.Find(userId);
            //Batch batch = userProfile.Student.BatchStudent.Select(x => x.Batch).FirstOrDefault();
            //reportViewModel.CanReview = db.BatchAssessment.Where(x => x.BatchID == batch.ID && x.AssessmentID == id).FirstOrDefault()!= null?db.BatchAssessment.Where(x => x.BatchID == batch.ID && x.AssessmentID == id).FirstOrDefault().CanReview ?? false:false;   
            
            return View(reportViewModel);
        }

        #endregion
        #region CanReview Report
        public ActionResult CanReviewReport(long id = 0)
        {
            ViewBag.QuestionID = id;
            ReportViewModel reportViewModel = new ReportViewModel();
            List<QuestionViewModel> questionViewModelList = GetTimeReportForAssessment(id);
            reportViewModel.QuestionViewModelList = questionViewModelList;
            return View(reportViewModel);
        }
        #endregion
        public void StudentAssmentReportsSummary(long id = 0)
        {
            string result = "";
            QuestionPaper questionPaper = db.QuestionPaper.Find(id);
            UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            UserAssessment userAssessment = userProfile.UserAssessment.Where(ua => ua.AssessmentID == id).FirstOrDefault();
            List<QuestionPaperSectionQuestion> questionPaperSectionQuestionList = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).ToList();

            ReportViewModel.QuestionPaperReportModel assessmentReportModel = new ReportViewModel.QuestionPaperReportModel();

            assessmentReportModel.StudentName = userProfile.Student.Name;
            assessmentReportModel.RegisterNumber = userProfile.Student.UniversityRegisterNumber;
            assessmentReportModel.QuestionPaperName = questionPaper.Name;

            int totalQuestions = questionPaper.QuestionPaperSection.SelectMany(ass => ass.QuestionPaperSectionQuestion).Count();
            assessmentReportModel.TotalQuestions = totalQuestions;

            int attendedQuestions = userAssessment.AssessmentSnapshot.Select(x => x.Question).Distinct().Count();
            assessmentReportModel.AttentedQuestions = attendedQuestions;

            //int correctAnswers = userProfile.UserAssessment.Where(ua=>ua.AssessmentID==id && ua.AssessmentSnapshot.Where(ass=>ass.Answer.IsCorrect??false).Count()>0).Select(x => x.AssessmentSnapshot).Count();
            int correctAnswersCount = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();        //Getting correct answer count for single select question

            int totalTimeTakenForCorrectAnswer = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Sum(x => x.TimeTaken ?? 0);      //Get the time of the single select question type


            List<Question> multiSelectQuestionList = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionType.Name.ToLower().Equals("multi select")).Select(x => x.Question).Distinct().ToList();
            foreach (Question question in multiSelectQuestionList)
            {
                List<Answer> correctAnswerList = question.Answer.Where(a => a.IsCorrect ?? false).ToList();
                List<Answer> userAnsweredList = question.AssessmentSnapshot.Where(asn => (asn.Answer != null) ? asn.Answer.IsCorrect ?? false : false).Select(x => x.Answer).ToList();
                if (correctAnswerList.Except(userAnsweredList).Count() > 0 || userAnsweredList.Except(correctAnswerList).Count() > 0)
                {                                                                       //some of the answers are incorrect

                }
                else
                {                                                                       //All the answers are correct
                    correctAnswersCount++;
                    totalTimeTakenForCorrectAnswer += question.AssessmentSnapshot.FirstOrDefault().TimeTaken ?? 0;
                }
            }

            assessmentReportModel.CorrectAnswers = correctAnswersCount;

            assessmentReportModel.TimeTakenForCorrectAnswer = totalTimeTakenForCorrectAnswer;

            decimal marksPercentage = 0;
            if (totalQuestions != 0)
                marksPercentage = ((decimal)correctAnswersCount / (decimal)totalQuestions) * 100;
            assessmentReportModel.MarksPercentage = Math.Round(marksPercentage, 2);
            
            decimal hitPercentage = 0;
            if (attendedQuestions != 0)
                hitPercentage = ((decimal)correctAnswersCount / (decimal)attendedQuestions) * 100;
            assessmentReportModel.HitPercentage = Math.Round(hitPercentage, 2);

            float avgerageTimeTakenForCorrectAnswer = 0;
            if (correctAnswersCount > 0)
                avgerageTimeTakenForCorrectAnswer = (totalTimeTakenForCorrectAnswer / correctAnswersCount);
            assessmentReportModel.AverageTimeTakenForCorrectAnswer = avgerageTimeTakenForCorrectAnswer;

            List<ReportViewModel.TagReportModel> tagReportModelList = new List<ReportViewModel.TagReportModel>();

            List<Tag> tagsList = questionPaper.QuestionPaperSection.SelectMany(ass => ass.QuestionPaperSectionQuestion).SelectMany(x => x.Question.QuestionTags).Select(x => x.Tag).Distinct().ToList();

            assessmentReportModel.Tags = string.Join(",", tagsList.Select(x => x.Name).ToArray());
            assessmentReportModel.Tags = "Max," + assessmentReportModel.Tag;

            foreach (Tag tags in tagsList)
            {
                ReportViewModel.TagReportModel tagReportModel = new ReportViewModel.TagReportModel();

                int totalQuestionsUnderThisTag = questionPaperSectionQuestionList.Where(asq => asq.Question.QuestionTags.Where(qt => qt.TagID == tags.ID).Count() > 0).Count();
                tagReportModel.TotalQuestions = totalQuestionsUnderThisTag;

                int attendedQuestionsInThisTag = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionTags.Where(qt => qt.TagID == tags.ID).Count() > 0).Select(x => x.Question).Distinct().Count();
                tagReportModel.AttendedQuestions = attendedQuestionsInThisTag;

                int correctAnswerUnderThisTag = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == tags.ID).Count() > 0) && (ass.Answer.IsCorrect ?? false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();     //Get only the single select type questions 
                List<Question> multiSelectTypeQuestionList = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == tags.ID).Count() > 0) && (ass.Question.QuestionType.Name.ToLower().Equals("multi select"))).Select(x => x.Question).Distinct().ToList();
                foreach (Question multiSelectQuestion in multiSelectTypeQuestionList)
                {
                    List<Answer> correctAnswerList = multiSelectQuestion.Answer.Where(a => a.IsCorrect ?? false).ToList();
                    List<Answer> userAnsweredList = multiSelectQuestion.AssessmentSnapshot.Select(x => x.Answer).ToList();
                    if (correctAnswerList.Except(userAnsweredList).Count() > 0 || userAnsweredList.Except(correctAnswerList).Count() > 0)
                    {                                                                       //some of the answers are incorrect

                    }
                    else
                    {                                                                       //All the answers are correct
                        correctAnswerUnderThisTag++;
                    }
                }
                tagReportModel.CorrectAnswers = correctAnswerUnderThisTag;

                float correctAnswerPercentage = 0;
                if (totalQuestionsUnderThisTag != 0)
                    correctAnswerPercentage = ((float)correctAnswerUnderThisTag / (float)totalQuestionsUnderThisTag) * 100;
                tagReportModel.CorrectAnswerPercentage = (int)Math.Ceiling(correctAnswerPercentage);

                string tagName = tags.Name;
                tagReportModel.Name = tagName;

                tagReportModelList.Add(tagReportModel);
            }
            assessmentReportModel.TagReportModelList = tagReportModelList;
            //return assessmentReportModel;
            result = RenderPartialViewToString("~/Views/Report/SummaryStudentAssessmentReport.cshtml", assessmentReportModel);
            Response.Write(result);
        }
        public void PerformancePieReportSummary(long ID = 0)
        {
            string result = "";
            UserAssessment userAssessment = db.UserAssessment.Where(ua => ua.UserProfile.UserName.ToLower() == User.Identity.Name && ua.AssessmentID == ID).FirstOrDefault();
            ReportViewModel reportViewModel = new ReportViewModel();
            QuestionPaper questionPaper = db.QuestionPaper.Find(ID);
            List<QuestionPaperSection> questionPaperSectionListforName = questionPaper.QuestionPaperSection.Distinct().ToList();

            reportViewModel.Sections = string.Join(",", questionPaperSectionListforName.Select(x => x.Name).ToArray());
            reportViewModel.Sections = "Max," + reportViewModel.Sections;

            reportViewModel.TotalSections = questionPaper.QuestionPaperSection.Count();
            if (questionPaper != null)
            {
                List<QuestionPaperSection> questionPaperSectionList = questionPaper.QuestionPaperSection.Distinct().ToList();
                List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();
                int yourscore = 0;
                foreach (QuestionPaperSection questionPaperSection in questionPaperSectionList)
                {
                    QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
                    questionPaperSectionViewModel.ID = questionPaperSection.ID;
                    questionPaperSectionViewModel.Name = questionPaperSection.Name;

                    int totalQuestions = questionPaperSection.QuestionPaperSectionQuestion.Count();
                    questionPaperSectionViewModel.TotalQuestions = totalQuestions;

                    int totalQuestionCount = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).Count();
                    reportViewModel.TotalQuestionCount = totalQuestionCount;

                    int answeredCount = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionPaperSectionQuestion.Where(qps => qps.AssessmentSectionID == questionPaperSection.ID).Count() > 0).Select(x => x.Question).Distinct().Count();
                    questionPaperSectionViewModel.AnsweredCount = answeredCount;

                    int unAttemptedQuestions = totalQuestions - answeredCount;
                    questionPaperSectionViewModel.UnAttemptedQuestions = unAttemptedQuestions;


                    int correctAnswerCount = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == questionPaperSection.ID).Count() > 0) && (ass.Answer.IsCorrect ?? false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();     //Get only the single select type questions 
                    List<Question> multiSelectTypeQuestionList = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == questionPaperSection.ID).Count() > 0) && (ass.Question.QuestionType.Name.ToLower().Equals("multi select"))).Select(x => x.Question).Distinct().ToList();
                    foreach (Question multiSelectQuestion in multiSelectTypeQuestionList)
                    {
                        List<Answer> correctAnswerList = multiSelectQuestion.Answer.Where(a => a.IsCorrect ?? false).ToList();
                        List<Answer> userAnsweredList = multiSelectQuestion.AssessmentSnapshot.Select(x => x.Answer).ToList();
                        if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
                        {                                                                       //All the answers are correct
                            correctAnswerCount++;
                        }
                    }
                    questionPaperSectionViewModel.CorrectAnswerCount = correctAnswerCount;

                    yourscore += correctAnswerCount;
                    reportViewModel.YourScore = yourscore;

                    int timetaken = userAssessment.AssessmentSnapshot.Sum(x => x.TimeTaken) ?? 0;
                    questionPaperSectionViewModel.UsertotalTimeTaken = timetaken;

                    int wrongAnswerCount = answeredCount - correctAnswerCount;
                    questionPaperSectionViewModel.WrongAnswerCount = wrongAnswerCount;

                    decimal scorePercentage = 0;
                    if (totalQuestions != 0)
                    {
                        scorePercentage = ((decimal)correctAnswerCount / (decimal)totalQuestions) * 100;
                        //questionPaperSectionViewModel.ScorePercentage = scorePercentage;
                        questionPaperSectionViewModel.ScorePercentage = Math.Round(scorePercentage, 2);
                    }


                    questionPaperSectionViewModelList.Add(questionPaperSectionViewModel);

                }

                reportViewModel.TotalTimeTaken = questionPaperSectionViewModelList.Sum(x => x.UsertotalTimeTaken);


                reportViewModel.TestDuration = questionPaper.TimeLimit ?? questionPaper.QuestionPaperSection.Sum(x => x.TimeLimit ?? 0);

                reportViewModel.QuestionPaperSectionViewModellList = questionPaperSectionViewModelList;
            }


            result = RenderPartialViewToString("~/Views/Report/SummaryPerformancePieReport.cshtml", reportViewModel);
            Response.Write(result);
        }
        public ActionResult SectionBarGraphSummary(long id = 0)
        {
            ReportViewModel.SectionReportModel sectionReportModel = new ReportViewModel.SectionReportModel();
            sectionReportModel = GetSectionReportModelForSectionWiseBargraph(id);
            return View("~/Views/Report/SummarySectionWiseBarGraph.cshtml", sectionReportModel);
        }
        public ReportViewModel.SectionReportModel GetSectionReportModelForSectionWiseBargraph(long id)
        {
            QuestionPaper questionPaper = db.QuestionPaper.Find(id);
            UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            UserAssessment userAssessment = userProfile.UserAssessment.Where(ua => ua.AssessmentID == id).FirstOrDefault();
            List<QuestionPaperSectionQuestion> questionPaperSectionQuestionList = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).ToList();
            ReportViewModel.SectionReportModel sectionReportModel = new ReportViewModel.SectionReportModel();
            sectionReportModel.StudentName = userProfile.Student.Name;
            sectionReportModel.RegisterNumber = userProfile.Student.UniversityRegisterNumber;
            sectionReportModel.QuestionPaperName = questionPaper.Name;

            int totalQuestions = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).Count();
            sectionReportModel.TotalQuestions = totalQuestions;
            List<QuestionPaperSection> questionPaperSectionListforName = questionPaper.QuestionPaperSection.Distinct().ToList();

            sectionReportModel.Sections = string.Join(",", questionPaperSectionListforName.Select(x => x.Name).ToArray());
            sectionReportModel.Sections = "Max," + sectionReportModel.Sections;

            sectionReportModel.TotalSections = questionPaper.QuestionPaperSection.Count();

            //int attendedQuestions = userAssessment.AssessmentSnapshot.Select(x => x.Question).Distinct().Count();
            //sectionReportModel.AttentedQuestions = attendedQuestions;
            int correctAnswersCount = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();

            List<Question> multiSelectQuestionList = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionType.Name.ToLower().Equals("multi select")).Select(x => x.Question).Distinct().ToList();
            foreach (Question question in multiSelectQuestionList)
            {
                List<Answer> correctAnswerList = question.Answer.Where(a => a.IsCorrect ?? false).ToList();
                List<Answer> userAnsweredList = question.AssessmentSnapshot.Where(asn => (asn.Answer != null) ? asn.Answer.IsCorrect ?? false : false).Select(x => x.Answer).ToList();
                if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
                {                                                                       //All the answers are correct
                    correctAnswersCount++;
                }
            }


            List<ReportViewModel.SectionReportModel> SectionReportModelList = new List<ReportViewModel.SectionReportModel>();
            List<QuestionPaperSection> questionPaperSectionList = questionPaper.QuestionPaperSection.ToList();
            foreach (QuestionPaperSection section in questionPaperSectionList)
            {
                ReportViewModel.SectionReportModel SectionReportModel = new ReportViewModel.SectionReportModel();
                SectionReportModel.SectionName = section.Name;
                int totalQuestionsUnderThisSection = questionPaperSectionQuestionList.Where(asq => asq.AssessmentSectionID == section.ID).Count();
                SectionReportModel.TotalQuestions = totalQuestionsUnderThisSection;

                int attendedQuestionsUnderThisSection = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == section.ID).Count() > 0).Select(x => x.Question).Distinct().Count();
                SectionReportModel.AttendedQuestions = attendedQuestionsUnderThisSection;

                int unattendedQuestionUnderThisSection = totalQuestionsUnderThisSection - attendedQuestionsUnderThisSection;
                SectionReportModel.UnattendedQuestions = unattendedQuestionUnderThisSection;

                int correctAnswerUnderThisSection = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == section.ID).Count() > 0) && (ass.Answer.IsCorrect ?? false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();     //Get only the single select type questions 
                List<Question> multiSelectTypeQuestionList = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == section.ID).Count() > 0) && (ass.Question.QuestionType.Name.ToLower().Equals("multi select"))).Select(x => x.Question).Distinct().ToList();
                foreach (Question multiSelectQuestion in multiSelectTypeQuestionList)
                {
                    List<Answer> correctAnswerList = multiSelectQuestion.Answer.Where(a => a.IsCorrect ?? false).ToList();
                    List<Answer> userAnsweredList = multiSelectQuestion.AssessmentSnapshot.Select(x => x.Answer).ToList();
                    if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
                    {                                                                       //All the answers are correct
                        correctAnswerUnderThisSection++;
                    }
                }
                SectionReportModel.CorrectAnswers = correctAnswerUnderThisSection;

                int wrongAnswerUnderThisSection = attendedQuestionsUnderThisSection - correctAnswerUnderThisSection;
                SectionReportModel.WrongAnswers = wrongAnswerUnderThisSection;

                decimal correctAnswerPercentage = 0;
                if (totalQuestionsUnderThisSection != 0)
                    correctAnswerPercentage = ((decimal)correctAnswerUnderThisSection / (decimal)totalQuestionsUnderThisSection) * 100;
               // SectionReportModel.CorrectAnswerPercentage = correctAnswerPercentage;
                SectionReportModel.CorrectAnswerPercentage = Math.Round(correctAnswerPercentage, 2);


                //float marksPercentage = 0;
                //if (totalQuestions != 0)
                //    marksPercentage = ((float)correctAnswersCount / (float)totalQuestions) * 100;
                //SectionReportModel.MarksPercentage = marksPercentage;

                SectionReportModelList.Add(SectionReportModel);
            }
            sectionReportModel.SectionReportModelList = SectionReportModelList;
            return sectionReportModel;
        }
        //public ActionResult SectionBarGraphSummary(long id = 0)
        //{
        //    ReportViewModel reportViewModel = new ReportViewModel();
        //    reportViewModel.sectionReportModel = new ReportViewModel.SectionReportModel();
        //    reportViewModel.sectionReportModel = GetSectionReportModelForSectionWiseBargraph(id);
        //   //ReportViewModel.SectionReportModel sectionReportModel = new ReportViewModel.SectionReportModel();
        //    //sectionReportModel = GetSectionReportModelForSectionWiseBargraph(id);
        //    return View(reportViewModel);
        //    //return reportViewModel;
            
        //   // return View("~/Views/Report/SummarySectionWiseBarGraph.cshtml", reportViewModel);
        //}
        //public ReportViewModel.SectionReportModel GetSectionReportModelForSectionWiseBargraph(long id)
        //{
        //    QuestionPaper questionPaper = db.QuestionPaper.Find(id);
        //    UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
        //    UserAssessment userAssessment = userProfile.UserAssessment.Where(ua => ua.AssessmentID == id).FirstOrDefault();
        //    List<QuestionPaperSectionQuestion> questionPaperSectionQuestionList = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).ToList();
        //    ReportViewModel.SectionReportModel sectionReportModel = new ReportViewModel.SectionReportModel();
        //    sectionReportModel.StudentName = userProfile.UserName;
        //    sectionReportModel.RegisterNumber = userProfile.Student.UniversityRegisterNumber;
        //    sectionReportModel.AssessmentName = questionPaper.Name;

        //    int totalQuestions = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).Count();
        //    sectionReportModel.TotalQuestions = totalQuestions;
        //    List<QuestionPaperSection> questionPaperSectionListforName = questionPaper.QuestionPaperSection.Distinct().ToList();

        //    sectionReportModel.Sections = string.Join(",", questionPaperSectionListforName.Select(x => x.Name).ToArray());
        //    sectionReportModel.Sections = "Max," + sectionReportModel.Sections;

        //    sectionReportModel.TotalSections = questionPaper.QuestionPaperSection.Count();

        //    //int attendedQuestions = userAssessment.AssessmentSnapshot.Select(x => x.Question).Distinct().Count();
        //    //sectionReportModel.AttentedQuestions = attendedQuestions;
        //    int correctAnswersCount = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();

        //    List<Question> multiSelectQuestionList = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionType.Name.ToLower().Equals("multi select")).Select(x => x.Question).Distinct().ToList();
        //    foreach (Question question in multiSelectQuestionList)
        //    {
        //        List<Answer> correctAnswerList = question.Answer.Where(a => a.IsCorrect ?? false).ToList();
        //        List<Answer> userAnsweredList = question.AssessmentSnapshot.Where(asn => (asn.Answer != null) ? asn.Answer.IsCorrect ?? false : false).Select(x => x.Answer).ToList();
        //        if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
        //        {                                                                       //All the answers are correct
        //            correctAnswersCount++;
        //        }
        //    }


        //    List<ReportViewModel.SectionReportModel> SectionReportModelList = new List<ReportViewModel.SectionReportModel>();
        //    List<QuestionPaperSection> questionPaperSectionList = questionPaper.QuestionPaperSection.ToList();
        //    foreach (QuestionPaperSection section in questionPaperSectionList)
        //    {
        //        ReportViewModel.SectionReportModel SectionReportModel = new ReportViewModel.SectionReportModel();
        //        SectionReportModel.SectionName = section.Name;
        //        int totalQuestionsUnderThisSection = questionPaperSectionQuestionList.Where(asq => asq.AssessmentSectionID == section.ID).Count();
        //        SectionReportModel.TotalQuestions = totalQuestionsUnderThisSection;

        //        int attendedQuestionsUnderThisSection = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == section.ID).Count() > 0).Select(x => x.Question).Distinct().Count();
        //        SectionReportModel.AttendedQuestions = attendedQuestionsUnderThisSection;

        //        int unattendedQuestionUnderThisSection = totalQuestionsUnderThisSection - attendedQuestionsUnderThisSection;
        //        SectionReportModel.UnattendedQuestions = unattendedQuestionUnderThisSection;

        //        int correctAnswerUnderThisSection = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == section.ID).Count() > 0) && (ass.Answer.IsCorrect ?? false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();     //Get only the single select type questions 
        //        List<Question> multiSelectTypeQuestionList = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == section.ID).Count() > 0) && (ass.Question.QuestionType.Name.ToLower().Equals("multi select"))).Select(x => x.Question).Distinct().ToList();
        //        foreach (Question multiSelectQuestion in multiSelectTypeQuestionList)
        //        {
        //            List<Answer> correctAnswerList = multiSelectQuestion.Answer.Where(a => a.IsCorrect ?? false).ToList();
        //            List<Answer> userAnsweredList = multiSelectQuestion.AssessmentSnapshot.Select(x => x.Answer).ToList();
        //            if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
        //            {                                                                       //All the answers are correct
        //                correctAnswerUnderThisSection++;
        //            }
        //        }
        //        SectionReportModel.CorrectAnswers = correctAnswerUnderThisSection;

        //        int wrongAnswerUnderThisSection = attendedQuestionsUnderThisSection - correctAnswerUnderThisSection;
        //        SectionReportModel.WrongAnswers = wrongAnswerUnderThisSection;

        //        float correctAnswerPercentage = 0;
        //        if (totalQuestionsUnderThisSection != 0)
        //            correctAnswerPercentage = ((float)correctAnswerUnderThisSection / (float)totalQuestionsUnderThisSection) * 100;
        //        SectionReportModel.CorrectAnswerPercentage = correctAnswerPercentage;


        //        //float marksPercentage = 0;
        //        //if (totalQuestions != 0)
        //        //    marksPercentage = ((float)correctAnswersCount / (float)totalQuestions) * 100;
        //        //SectionReportModel.MarksPercentage = marksPercentage;

        //        SectionReportModelList.Add(SectionReportModel);
        //    }
        //    sectionReportModel.SectionReportModelList = SectionReportModelList;
        //    return sectionReportModel;
        //}
        //public void SectionBarGraphSummary(long id = 0)
        //{
        //    string result = "";
        //    QuestionPaper questionPaper = db.QuestionPaper.Find(id);
        //    UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
        //    UserAssessment userAssessment = userProfile.UserAssessment.Where(ua => ua.AssessmentID == id).FirstOrDefault();
        //    List<QuestionPaperSectionQuestion> questionPaperSectionQuestionList = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).ToList();
        //    ReportViewModel.SectionReportModel sectionReportModel = new ReportViewModel.SectionReportModel();
        //    sectionReportModel.StudentName = userProfile.UserName;
        //    sectionReportModel.RegisterNumber = userProfile.Student.UniversityRegisterNumber;
        //    sectionReportModel.AssessmentName = questionPaper.Name;

        //    int totalQuestions = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).Count();
        //    sectionReportModel.TotalQuestions = totalQuestions;
        //    List<QuestionPaperSection> questionPaperSectionListforName = questionPaper.QuestionPaperSection.Distinct().ToList();

        //    sectionReportModel.Sections = string.Join(",", questionPaperSectionListforName.Select(x => x.Name).ToArray());
        //    sectionReportModel.Sections = "Max," + sectionReportModel.Sections;

        //    sectionReportModel.TotalSections = questionPaper.QuestionPaperSection.Count();

        //    //int attendedQuestions = userAssessment.AssessmentSnapshot.Select(x => x.Question).Distinct().Count();
        //    //sectionReportModel.AttentedQuestions = attendedQuestions;
        //    int correctAnswersCount = userAssessment.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();

        //    List<Question> multiSelectQuestionList = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionType.Name.ToLower().Equals("multi select")).Select(x => x.Question).Distinct().ToList();
        //    foreach (Question question in multiSelectQuestionList)
        //    {
        //        List<Answer> correctAnswerList = question.Answer.Where(a => a.IsCorrect ?? false).ToList();
        //        List<Answer> userAnsweredList = question.AssessmentSnapshot.Where(asn => (asn.Answer != null) ? asn.Answer.IsCorrect ?? false : false).Select(x => x.Answer).ToList();
        //        if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
        //        {                                                                       //All the answers are correct
        //            correctAnswersCount++;
        //        }
        //    }


        //    List<ReportViewModel.SectionReportModel> SectionReportModelList = new List<ReportViewModel.SectionReportModel>();
        //    List<QuestionPaperSection> questionPaperSectionList = questionPaper.QuestionPaperSection.ToList();
        //    foreach (QuestionPaperSection section in questionPaperSectionList)
        //    {
        //        ReportViewModel.SectionReportModel SectionReportModel = new ReportViewModel.SectionReportModel();
        //        SectionReportModel.SectionName = section.Name;
        //        int totalQuestionsUnderThisSection = questionPaperSectionQuestionList.Where(asq => asq.AssessmentSectionID == section.ID).Count();
        //        SectionReportModel.TotalQuestions = totalQuestionsUnderThisSection;

        //        int attendedQuestionsUnderThisSection = userAssessment.AssessmentSnapshot.Where(ass => ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == section.ID).Count() > 0).Select(x => x.Question).Distinct().Count();
        //        SectionReportModel.AttendedQuestions = attendedQuestionsUnderThisSection;

        //        int unattendedQuestionUnderThisSection = totalQuestionsUnderThisSection - attendedQuestionsUnderThisSection;
        //        SectionReportModel.UnattendedQuestions = unattendedQuestionUnderThisSection;

        //        int correctAnswerUnderThisSection = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == section.ID).Count() > 0) && (ass.Answer.IsCorrect ?? false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();     //Get only the single select type questions 
        //        List<Question> multiSelectTypeQuestionList = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == section.ID).Count() > 0) && (ass.Question.QuestionType.Name.ToLower().Equals("multi select"))).Select(x => x.Question).Distinct().ToList();
        //        foreach (Question multiSelectQuestion in multiSelectTypeQuestionList)
        //        {
        //            List<Answer> correctAnswerList = multiSelectQuestion.Answer.Where(a => a.IsCorrect ?? false).ToList();
        //            List<Answer> userAnsweredList = multiSelectQuestion.AssessmentSnapshot.Select(x => x.Answer).ToList();
        //            if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
        //            {                                                                       //All the answers are correct
        //                correctAnswerUnderThisSection++;
        //            }
        //        }
        //        SectionReportModel.CorrectAnswers = correctAnswerUnderThisSection;

        //        int wrongAnswerUnderThisSection = attendedQuestionsUnderThisSection - correctAnswerUnderThisSection;
        //        SectionReportModel.WrongAnswers = wrongAnswerUnderThisSection;

        //        float correctAnswerPercentage = 0;
        //        if (totalQuestionsUnderThisSection != 0)
        //            correctAnswerPercentage = ((float)correctAnswerUnderThisSection / (float)totalQuestionsUnderThisSection) * 100;
        //        SectionReportModel.CorrectAnswerPercentage = correctAnswerPercentage;


        //        //float marksPercentage = 0;
        //        //if (totalQuestions != 0)
        //        //    marksPercentage = ((float)correctAnswersCount / (float)totalQuestions) * 100;
        //        //SectionReportModel.MarksPercentage = marksPercentage;

        //        SectionReportModelList.Add(SectionReportModel);
        //    }
        //    sectionReportModel.SectionReportModelList = SectionReportModelList;
        //    result = RenderPartialViewToString("~/Views/Report/SummarySectionWiseBarGraph.cshtml", sectionReportModel);
        //    Response.Write(result);
        //}
        public void AssessmentTimeReportSummary(long id = 0)
        {
            string result = "";
            List<QuestionViewModel> questionViewModelList = GetTimeReportForAssessment(id);
            ReportViewModel reportViewModel = new ReportViewModel();
            //reportViewModel.TimeReport = timeReportModelList;
            reportViewModel.QuestionViewModelList = questionViewModelList;
            reportViewModel.CurrentQuestion = questionViewModelList.FirstOrDefault();
            long studentAnswerId = reportViewModel.CurrentQuestion.TimeReports.Count() > 0 ? reportViewModel.CurrentQuestion.TimeReports.FirstOrDefault().StudentAnswerID : 0;
            if (studentAnswerId != 0)
            {
                reportViewModel.StudentAnswer = reportViewModel.CurrentQuestion.AnswerViewModelList.Where(a => a.ID == studentAnswerId).FirstOrDefault();
            }
            reportViewModel.QuestionPaperID = id;
            reportViewModel.IsFirst = true;
            reportViewModel.IsLast = false;
            result = RenderPartialViewToString("~/Views/Report/SummaryAssessmentTimeReport.cshtml", reportViewModel);
            Response.Write(result);
            // return reportViewModel;
        }
        //public void QuestionWiseAnalysisSummary(long id = 0)
        //{
        //    string result1 = " ";
        //    datatableRequest = new DataTableRequest(Request, columns);
        //    int userId = GetCurrentUserID();
        //    UserAssessment userAssessment = db.UserAssessment.Where(x => x.AssessmentID == id && x.UserID == userId).FirstOrDefault();
        //    ReportViewModel reportViewModel = new ReportViewModel();
        //    QuestionPaper questionPaper = db.QuestionPaper.Find(id);
        //    IQueryable<Question> questions = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).Select(z => z.Question).AsQueryable();
        //    List<QuestionViewModel> questionViewModelList = MapperHelper.MapToViewModelListforReport(questions, id, userAssessment.ID);
        //    reportViewModel.QuestionViewModelList = questionViewModelList;
        //    reportViewModel.AssessmentName = questionPaper.Name;

        //    int totalRecords = questions.Count();
        //    int filteredCount = totalRecords;
        //    IQueryable<QuestionViewModel> questionViewModels = questionViewModelList.AsQueryable();
        //    questionViewModelList = questionViewModels.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired).ToList();
        //    HtmlFormatter htmlFormatter = new HtmlFormatter();
        //    questionViewModelList = questionViewModelList.Select(q => { q.SectionName = htmlFormatter.GetContentFromHtmlWithOutReplaceSpace(q.SectionName); return q; }).ToList();
        //    QuestionDataTableViewModel dataTableViewModel = new QuestionDataTableViewModel(questionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);
        //    string result = JsonConvert.SerializeObject(dataTableViewModel);
        //    //Response.Write(result);
        //    result1 = RenderPartialViewToString("~/Views/Report/SummaryQuestionWiseAnalysis.cshtml", reportViewModel);
        //    Response.Write(result1);
        //}
        //public void QuestionPaperReviewSummary(long id = 0)
        //{
        //    string result2 = " ";
        //    datatableRequest = new DataTableRequest(Request, columns);
        //    int userId = GetCurrentUserID();
        //    UserAssessment userAssessment = db.UserAssessment.Where(x => x.AssessmentID == id && x.UserID == userId).FirstOrDefault(); // AssessmentID is QuestionPaperID
        //    ReportViewModel reportViewModel = new ReportViewModel();
        //    QuestionPaper questionPaper = db.QuestionPaper.Find(id);
        //    IQueryable<Question> questions = questionPaper.QuestionPaperSection.SelectMany(x => x.QuestionPaperSectionQuestion).Select(z => z.Question).AsQueryable();
        //    List<QuestionViewModel> questionViewModelList = new List<QuestionViewModel>();
        //    foreach (Question question in questions)
        //    {
        //        IQueryable<AssessmentSnapshot> assessmentSnapShotList = question.AssessmentSnapshot.Where(a => a.UserAssessmentID == userAssessment.ID).AsQueryable();
        //        AssessmentSnapshot assessmentSnapShot = assessmentSnapShotList.LastOrDefault();
        //        if (assessmentSnapShot == null)
        //        {
        //            assessmentSnapShot = new AssessmentSnapshot();
        //        }
        //        QuestionViewModel questionViewModel = new QuestionViewModel();
        //        questionViewModel.Description = question.Description;
        //        questionViewModel.TimeTaken = assessmentSnapShot.TimeTaken ?? 0;
        //        questionViewModel.QuestionID = question.ID;
        //        questionViewModel.AnswerDescription = assessmentSnapShot.Answer != null ? assessmentSnapShot.Answer.Description ?? "" : "";
        //        questionViewModel.Status = assessmentSnapShot.Answer != null ? assessmentSnapShot.Answer.IsCorrect : null;
        //        questionViewModelList.Add(questionViewModel);
        //    }
        //    reportViewModel.QuestionViewModelList = questionViewModelList;
        //    reportViewModel.AssessmentName = questionPaper.Name;

        //    int totalRecords = questions.Count();
        //    int filteredCount = totalRecords;

        //    IQueryable<QuestionViewModel> questionViewModels = questionViewModelList.AsQueryable();
        //    questionViewModelList = questionViewModels.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired).ToList();
        //    QuestionDataTableViewModel dataTableViewModel = new QuestionDataTableViewModel(questionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);
        //    string result = JsonConvert.SerializeObject(dataTableViewModel);
        //    // Response.Write(result);
        //    result2 = RenderPartialViewToString("~/Views/Report/QuestionPaperReview.cshtml", reportViewModel);
        //    Response.Write(result2);
        //}
        
       
        public object[] assessmentID { get; set; }
    
        //public string GetSuffix(int no)
        //{
        //    string suffixString = "";
        //    if (no % 10 == 1)
        //        suffixString = "st";
        //    else if (no % 10 == 2)
        //        suffixString = "nd";
        //    else if (no % 10 == 3)
        //        suffixString = "rd";
        //    else
        //        suffixString = "th";
        //    return suffixString;
        //}

        public string GetSuffix(int no)
        {
            int j = no % 10;
            int k = no % 100;
            if (j == 1 && k != 11)
            {
                return no + "st";
            }
            if (j == 2 && k != 12)
            {
                return no + "nd";
            }
            if (j == 3 && k != 13)
            {
                return no + "rd";
            }
            return no + "th";
        }

        //Sukanya 
        //public ActionResult ConsoleReportFile(long collegeIDs, long batchIDs, long assessmentIDs)
        //{

        //    College college = new College();
        //    if (Convert.ToInt64(collegeIDs) > 0)
        //        college = db.College.Find(collegeIDs);

        //    Batch batch = new Batch();
        //    if (Convert.ToInt64(batchIDs) > 0)
        //        batch = db.Batch.Find(batchIDs);
        //    IQueryable<BatchAssessment> questionPaperList;
        //    BatchAssessment questionPaper = new BatchAssessment();
        //    IEnumerable<Branch> branch = college.CollegeBranch.Select(c => c.Branch).AsEnumerable();
        //    if (Convert.ToInt64(assessmentIDs) > 0)
        //    {
        //        questionPaperList = db.BatchAssessment.Where(b => b.AssessmentID == assessmentIDs && b.BatchID == batchIDs).OrderByDescending(b => b.StartDateTime).AsQueryable();
        //        questionPaper = questionPaperList.FirstOrDefault();
        //    }

        //    List<BranchViewModel> BranchInstituteViewModelList = MapperHelper.MapToViewModelList(branch);
        //    ReportViewModel reportViewModel = new ReportViewModel();
        //    reportViewModel.collegeName = college != null ? college.Name : "";
        //    reportViewModel.batchName = batch != null ? batch.Name : "";
        //    reportViewModel.AssessmentName = questionPaper != null ? questionPaper.Name : "";
        //    reportViewModel.BranchViewModelList = BranchInstituteViewModelList;
        //    return View(reportViewModel);
        //}



        //public ActionResult ConsoleStreamwiseBarGraph()
        //{
        //    ViewBag.College = new List<CollegeViewModel>();
        //    ViewBag.Batch = new List<BatchViewModel>();
        //    ViewBag.Assessment = new List<BatchAssessmentViewModel>();
        //    return View();
        //}
        //public ActionResult ConsoleStreamwiseBarGraph(long collegeIDs, long batchIDs, long assessmentIDs)
        //{
        //    College college = new College();
        //    if (Convert.ToInt64(collegeIDs) > 0)
        //        college = db.College.Find(collegeIDs);
        //    List<BatchViewModel> batchViewModelList = new List<BatchViewModel>();

        //    Batch batch = new Batch();
        //    if (Convert.ToInt64(batchIDs) > 0)
        //        batch = db.Batch.Find(batchIDs);
        //    IEnumerable<Branch> branch = college.CollegeBranch.Select(c => c.Branch).AsEnumerable();


        //    return View();
        //}

        #region Consolidated Report

        public ActionResult ConsolidatedReport()
        {
            ViewBag.College = new List<CollegeViewModel>();
            ViewBag.Batch = new List<BatchViewModel>();
            ViewBag.Assessment = new List<BatchQuestionPaperViewModel>();
            return View();
        }

        public ActionResult ConsolidatedReportFile(long collegeIDs, long batchIDs, long assessmentIDs)
        {
            //List<QuestionPaperSection> sectionList = new List<QuestionPaperSection>();
            College college = new College();
            if (Convert.ToInt64(collegeIDs) > 0) { college = db.College.Find(collegeIDs); }
            Batch batch = new Batch();
            if (Convert.ToInt64(batchIDs) > 0) { batch = db.Batch.Find(batchIDs); }
            IQueryable<BatchAssessment> questionPaperList;
            BatchAssessment questionPaper = new BatchAssessment();
            //BatchAssessment batchAssessment = new BatchAssessment();
            IEnumerable<Branch> branch = college.CollegeBranch.Select(c => c.Branch).Distinct().AsEnumerable();
            List<QuestionPaperSection> sectionList = new List<QuestionPaperSection>();
            if (Convert.ToInt64(assessmentIDs) > 0)
            {
                questionPaperList = db.BatchAssessment.Where(b => b.AssessmentID == assessmentIDs && b.BatchID == batchIDs).OrderByDescending(b => b.StartDateTime).AsQueryable();
                questionPaper = questionPaperList.FirstOrDefault();
                sectionList = db.QuestionPaper.Find(assessmentIDs) != null ? db.QuestionPaper.Find(assessmentIDs).QuestionPaperSection.ToList() : new List<QuestionPaperSection>();
            }
            QuestionPaper questionPapers = db.QuestionPaper.Find(assessmentIDs);
            sectionList = questionPaper != null ? db.QuestionPaper.Find(assessmentIDs).QuestionPaperSection.Distinct().ToList() : new List<QuestionPaperSection>();
            //anu
            //DateTime testStartDate = questionPapers.StatDateTime ?? new DateTime();
            //DateTime testEndDate = questionPapers.EndDateTime ?? new DateTime();
            //DateTime testStartDate = batchAssessment.StatDateTime;
            //DateTime testEndDate = batchAssessment.EndDateTime;
            DateTime testStartDate = questionPaper.StartDateTime;
            DateTime testEndDate = questionPaper.EndDateTime;
            string dateConductedString = "";
            //if (testStartDate == testEndDate)
            //{
            //    dateConductedString = testStartDate.Day + " " + GetSuffix(testStartDate.Day) + " " + testStartDate.ToString("MMM", CultureInfo.InvariantCulture) + " " + testStartDate.Year;
            //}
            //else if ((testStartDate - testEndDate).TotalDays == 0)
            //{
            //    dateConductedString = testStartDate.Day + "" + GetSuffix(testStartDate.Day) + " and " + testEndDate.Day + " " + GetSuffix(testEndDate.Day) + " " + testStartDate.ToString("MMM", CultureInfo.InvariantCulture) + " " + testStartDate.Year;
            //}
            //else
            //{
            //    dateConductedString = testStartDate.Day + "" + GetSuffix(testStartDate.Day) + " to " + testEndDate.Day + " " + GetSuffix(testEndDate.Day) + " " + testStartDate.ToString("MMM", CultureInfo.InvariantCulture) + " " + testStartDate.Year;
            //}
            dateConductedString = testStartDate.ToString("dd MMMM yyyy").Replace(testStartDate.Day.ToString(), GetSuffix(testStartDate.Day));
            //anu
            List<BranchViewModel> BranchInstituteViewModelList = MapperHelper.MapToViewModelList(branch);
            ReportViewModel reportViewModel = new ReportViewModel();
            reportViewModel.collegeName = college != null ? college.Name : "";
            reportViewModel.batchName = batch != null ? batch.Name : "";
            reportViewModel.QuestionPaperName = questionPaper != null ? questionPaper.Name : "";
            reportViewModel.QuestionPaperSectionViewModellList = MapperHelper.MapToViewModelList(sectionList);
            reportViewModel.BranchViewModelList = BranchInstituteViewModelList;
            reportViewModel.TotalQuestionCount = sectionList.Select(s => s.QuestionPaperSectionQuestion).Count();
            reportViewModel.QuestionPaperSectionViewModellList = MapperHelper.MapToViewModelList(sectionList);
            reportViewModel.DateConductedString = dateConductedString;
            int count = batch.BatchStudent.Select(b => b.Student).Count();
            reportViewModel.StudentCount = count.ToString();

           // reportViewModel.BarChartViewModel = GetScoreHistogram(collegeIDs, batchIDs, assessmentIDs); //Creates a buffer ViewModel to connect the ActionResult View and Method
            reportViewModel.StreamWiseBarChartViewModel = GetStreamWiseHistogram(collegeIDs, batchIDs, assessmentIDs); //Creates a buffer ViewModel to connect the ActionResult View and Method
            reportViewModel.SectionWisePieChartViewModel = GetSectionWisePieChart(collegeIDs, batchIDs, assessmentIDs); //Creates a buffer ViewModel to connect the ActionResult View and Method
            reportViewModel.TagWiseBarChartViewModelList = SubSectionPerformanceList(reportViewModel.QuestionPaperSectionViewModellList, collegeIDs); //Creates a buffer ViewModel to connect the ActionResult View and Method
            reportViewModel.DeptWisePieChartViewModelList = GetDeptWisePieChart(collegeIDs, batchIDs, assessmentIDs); //Creates a buffer ViewModel to connect the ActionResult View and Method
            reportViewModel.GetDeptWiseBarChartViewModelList = GetDeptWiseBarChart(collegeIDs, batchIDs, assessmentIDs); //Creates a buffer ViewModel to connect the ActionResult View and Method
            reportViewModel.GetDeptWiseAvgHorzBarChartModelList = GetDeptWiseAvgHorzBarChart(collegeIDs, batchIDs, assessmentIDs); //Creates a buffer ViewModel to connect the ActionResult View and Method

            return View(reportViewModel);
        }

        //public ActionResult PretrainingDiagnosticReport(long collegeIDs, long batchIDs, long assessmentIDs)
        //{
        //    IQueryable<UserProfile> userProfileList = db.UserProfile.Where(b => b.Student.BatchStudent.Where(bs => bs.BatchID == batchIDs).Count() > 0);
        //    List<MarkViewModel> resultViewModelList = new List<MarkViewModel>();

        //    MarkViewModel markViewModel = new MarkViewModel();
        //    markViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks <= 15).Count() > 0));
        //    markViewModel.LabelName = "<15";
        //    markViewModel.studentCount = markViewModel.StudentViewModelList.Count();
        //    resultViewModelList.Add(markViewModel);

        //    markViewModel = new MarkViewModel();
        //    markViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 15 && ua.Marks <= 30).Count() > 0));
        //    markViewModel.LabelName = "15-30";
        //    markViewModel.studentCount = markViewModel.StudentViewModelList.Count();
        //    resultViewModelList.Add(markViewModel);

        //    markViewModel = new MarkViewModel();
        //    markViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 31 && ua.Marks <= 45).Count() > 0));
        //    markViewModel.LabelName = "31-45";
        //    markViewModel.studentCount = markViewModel.StudentViewModelList.Count();
        //    resultViewModelList.Add(markViewModel);

        //    markViewModel = new MarkViewModel();
        //    markViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 46 && ua.Marks <= 60).Count() > 0));
        //    markViewModel.LabelName = "46-60";
        //    markViewModel.studentCount = markViewModel.StudentViewModelList.Count();
        //    resultViewModelList.Add(markViewModel);

        //    markViewModel = new MarkViewModel();
        //    markViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 61 && ua.Marks <= 75).Count() > 0));
        //    markViewModel.LabelName = "61-75";
        //    markViewModel.studentCount = markViewModel.StudentViewModelList.Count(); ;
        //    resultViewModelList.Add(markViewModel);

        //    ReportViewModel reportViewModel = new ReportViewModel();
        //    reportViewModel.MarkViewModelList = resultViewModelList;

        //    //MarkViewModel lessThan15MarkViewModel = new MarkViewModel();
        //    //lessThan15MarkViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks <= 15).Count() > 0));
        //    //lessThan15MarkViewModel.LabelName = "<15";
        //    //lessThan15MarkViewModel.studentCount = lessThan15MarkViewModel.StudentViewModelList.Count(); ;

        //    //MarkViewModel fifteenTo30MarkViewModel = new MarkViewModel();
        //    //fifteenTo30MarkViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 15 && ua.Marks <= 30).Count() > 0));
        //    //fifteenTo30MarkViewModel.LabelName = "15-30";
        //    //fifteenTo30MarkViewModel.studentCount = fifteenTo30MarkViewModel.StudentViewModelList.Count(); ;

        //    //MarkViewModel thirtyoneTo45MarkViewModel = new MarkViewModel();
        //    //thirtyoneTo45MarkViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 31 && ua.Marks <= 45).Count() > 0));
        //    //thirtyoneTo45MarkViewModel.LabelName = "31-45";
        //    //thirtyoneTo45MarkViewModel.studentCount = thirtyoneTo45MarkViewModel.StudentViewModelList.Count(); ;

        //    //MarkViewModel fortysixTo60MarkViewModel = new MarkViewModel();
        //    //fortysixTo60MarkViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 46 && ua.Marks <= 60).Count() > 0));
        //    //fortysixTo60MarkViewModel.LabelName = "46-60";
        //    //fortysixTo60MarkViewModel.studentCount = fortysixTo60MarkViewModel.StudentViewModelList.Count(); ;

        //    //MarkViewModel sixtyoneTo75MarkViewModel = new MarkViewModel();
        //    //sixtyoneTo75MarkViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 61 && ua.Marks <= 75).Count() > 0));
        //    //sixtyoneTo75MarkViewModel.LabelName = "61-75";
        //    //sixtyoneTo75MarkViewModel.studentCount = sixtyoneTo75MarkViewModel.StudentViewModelList.Count(); ;

        //    //List<MarkViewModel> DiagnosticViewModel = new List<MarkViewModel>();

        //    //DiagnosticViewModel.Add(lessThan15MarkViewModel);
        //    //DiagnosticViewModel.Add(fifteenTo30MarkViewModel);
        //    //DiagnosticViewModel.Add(thirtyoneTo45MarkViewModel);
        //    //DiagnosticViewModel.Add(fortysixTo60MarkViewModel);
        //    //DiagnosticViewModel.Add(sixtyoneTo75MarkViewModel);

        //    //ReportViewModel drep = new ReportViewModel();
        //    //drep.MarkViewModelList = DiagnosticViewModel;
        //    return View(reportViewModel);
        //}
        //GetScoreHistogram() is the method counterpart for ActionResult PretrainingDiagnosticReport()
        //public ReportViewModel GetScoreHistogram(long collegeIDs, long batchIDs, long assessmentIDs)
        //{
        //    IQueryable<UserProfile> userProfileList = db.UserProfile.Where(b => b.Student.BatchStudent.Where(bs => bs.BatchID == batchIDs).Count() > 0);
        //    List<MarkViewModel> resultViewModelList = new List<MarkViewModel>();

        //    MarkViewModel markViewModel = new MarkViewModel();
        //    markViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks <= 15).Count() > 0));
        //    markViewModel.LabelName = "<15";
        //    markViewModel.studentCount = markViewModel.StudentViewModelList.Count();
        //    resultViewModelList.Add(markViewModel);

        //    markViewModel = new MarkViewModel();
        //    markViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 15 && ua.Marks <= 30).Count() > 0));
        //    markViewModel.LabelName = "15-30";
        //    markViewModel.studentCount = markViewModel.StudentViewModelList.Count();
        //    resultViewModelList.Add(markViewModel);

        //    markViewModel = new MarkViewModel();
        //    markViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 31 && ua.Marks <= 45).Count() > 0));
        //    markViewModel.LabelName = "31-45";
        //    markViewModel.studentCount = markViewModel.StudentViewModelList.Count();
        //    resultViewModelList.Add(markViewModel);

        //    markViewModel = new MarkViewModel();
        //    markViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 46 && ua.Marks <= 60).Count() > 0));
        //    markViewModel.LabelName = "46-60";
        //    markViewModel.studentCount = markViewModel.StudentViewModelList.Count();
        //    resultViewModelList.Add(markViewModel);

        //    markViewModel = new MarkViewModel();
        //    markViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 61 && ua.Marks <= 75).Count() > 0));
        //    markViewModel.LabelName = "61-75";
        //    markViewModel.studentCount = markViewModel.StudentViewModelList.Count(); ;
        //    resultViewModelList.Add(markViewModel);

        //    ReportViewModel reportViewModel = new ReportViewModel();
        //    reportViewModel.MarkViewModelList = resultViewModelList;

        //    //MarkViewModel lessThan15MarkViewModel = new MarkViewModel();
        //    //lessThan15MarkViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks <= 15).Count() > 0));
        //    //lessThan15MarkViewModel.LabelName = "<15";
        //    //lessThan15MarkViewModel.studentCount = lessThan15MarkViewModel.StudentViewModelList.Count(); ;

        //    //MarkViewModel fifteenTo30MarkViewModel = new MarkViewModel();
        //    //fifteenTo30MarkViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 15 && ua.Marks <= 30).Count() > 0));
        //    //fifteenTo30MarkViewModel.LabelName = "15-30";
        //    //fifteenTo30MarkViewModel.studentCount = fifteenTo30MarkViewModel.StudentViewModelList.Count(); ;

        //    //MarkViewModel thirtyoneTo45MarkViewModel = new MarkViewModel();
        //    //thirtyoneTo45MarkViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 31 && ua.Marks <= 45).Count() > 0));
        //    //thirtyoneTo45MarkViewModel.LabelName = "31-45";
        //    //thirtyoneTo45MarkViewModel.studentCount = thirtyoneTo45MarkViewModel.StudentViewModelList.Count(); ;

        //    //MarkViewModel fortysixTo60MarkViewModel = new MarkViewModel();
        //    //fortysixTo60MarkViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 46 && ua.Marks <= 60).Count() > 0));
        //    //fortysixTo60MarkViewModel.LabelName = "46-60";
        //    //fortysixTo60MarkViewModel.studentCount = fortysixTo60MarkViewModel.StudentViewModelList.Count(); ;

        //    //MarkViewModel sixtyoneTo75MarkViewModel = new MarkViewModel();
        //    //sixtyoneTo75MarkViewModel.StudentViewModelList = MapperHelper.MapToViewModelList(userProfileList.Where(stup => stup.UserAssessment.Where(ua => ua.AssessmentID == assessmentIDs && ua.Marks > 61 && ua.Marks <= 75).Count() > 0));
        //    //sixtyoneTo75MarkViewModel.LabelName = "61-75";
        //    //sixtyoneTo75MarkViewModel.studentCount = sixtyoneTo75MarkViewModel.StudentViewModelList.Count(); ;

        //    //List<MarkViewModel> DiagnosticViewModel = new List<MarkViewModel>();

        //    //DiagnosticViewModel.Add(lessThan15MarkViewModel);
        //    //DiagnosticViewModel.Add(fifteenTo30MarkViewModel);
        //    //DiagnosticViewModel.Add(thirtyoneTo45MarkViewModel);
        //    //DiagnosticViewModel.Add(fortysixTo60MarkViewModel);
        //    //DiagnosticViewModel.Add(sixtyoneTo75MarkViewModel);

        //    //ReportViewModel drep = new ReportViewModel();
        //    //drep.MarkViewModelList = DiagnosticViewModel;

        //    // return View(reportViewModel);
        //    return reportViewModel;
        //}

        public ActionResult ConsoleStreamWiseBarGraph(long collegeIDs, long batchIDs, long assessmentIDs)
        {
            College college = new College();
            if (Convert.ToInt64(collegeIDs) > 0)
                college = db.College.Find(collegeIDs);

            // List<BatchViewModel> batchViewModel = new List<BatchViewModel>();
            ReportViewModel reportViewModel = new ReportViewModel();
            Batch batch = db.Batch.Find(batchIDs);
            BatchViewModel batchViewModel = MapperHelper.MapToViewModel(batch);
            List<QuestionPaper> assessmentList = new List<QuestionPaper>();
            List<long> assessmentIDList = new List<long>();
            if (batch != null)
            {
                IQueryable<Student> studentList = batch.BatchStudent.Select(x => x.Student).AsQueryable();
                IEnumerable<Branch> branches = college.CollegeBranch.Select(c => c.Branch).Distinct().AsEnumerable();
                List<BranchViewModel> branchViewModelList = new List<BranchViewModel>();
                List<StudentViewModel> studentViewModelList = new List<StudentViewModel>();
                foreach (Branch branch in branches)
                {
                    BranchViewModel branchViewModel = new BranchViewModel();
                    branchViewModel.Name = branch.Name;
                    branchViewModel.PassPercentageAverage = branch.Student.Count() > 0 ? branch.Student.SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Average(x => x.Marks ?? 0) : 0;
                    // decimal average = branch.Student.SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Average(x => x.Marks??0);
                    branchViewModelList.Add(branchViewModel);
                }
                reportViewModel.BranchViewModelList = branchViewModelList;
                reportViewModel.StudentViewModelList = studentViewModelList;
            }
            return View(reportViewModel);
        }
        //GetStreamWiseHistogram() is the method counterpart for ActionResult ConsoleStreamWiseBarGraph()
        public ReportViewModel GetStreamWiseHistogram(long collegeIDs, long batchIDs, long assessmentIDs)
        {
            College college = new College();
            if (Convert.ToInt64(collegeIDs) > 0)
                college = db.College.Find(collegeIDs);

            // List<BatchViewModel> batchViewModel = new List<BatchViewModel>();
            ReportViewModel reportViewModel = new ReportViewModel();
            Batch batch = db.Batch.Find(batchIDs);
            BatchViewModel batchViewModel = MapperHelper.MapToViewModel(batch);
            List<QuestionPaper> assessmentList = new List<QuestionPaper>();
            List<long> assessmentIDList = new List<long>();
            if (batch != null)
            {
                IQueryable<Student> studentList = batch.BatchStudent.Select(x => x.Student).AsQueryable();
                IEnumerable<Branch> branches = college.CollegeBranch.Select(c => c.Branch).Distinct().AsEnumerable();
                List<BranchViewModel> branchViewModelList = new List<BranchViewModel>();
                List<StudentViewModel> studentViewModelList = new List<StudentViewModel>();
                foreach (Branch branch in branches)
                {
                    BranchViewModel branchViewModel = new BranchViewModel();
                    branchViewModel.Name = branch.Name;
                    if (branch.Student.Count() > 0)
                    {
                        IQueryable<UserAssessment> userAssessment = branch.Student.SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).AsQueryable();
                        if (userAssessment.Count() > 0)
                        {
                            branchViewModel.PassPercentageAverage = userAssessment.Average(x => x.Marks ?? 0);
                        }
                    }
                    // decimal average = branch.Student.SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Average(x => x.Marks??0);
                    branchViewModelList.Add(branchViewModel);
                }

                reportViewModel.BranchViewModelList = branchViewModelList;
                reportViewModel.StudentViewModelList = studentViewModelList;
            }
            return reportViewModel;
        }

        public ActionResult DeptWiseAvgHorzBarChart(long collegeIDs, long batchIDs, long assessmentIDs)
        {
            ReportViewModel ReportViewModel = new ReportViewModel();
            College college = new College();
            if (Convert.ToInt64(collegeIDs) > 0)
                college = db.College.Find(collegeIDs);

            // List<BatchViewModel> batchViewModel = new List<BatchViewModel>();
            Batch batch = db.Batch.Find(batchIDs);
            BatchViewModel batchViewModel = MapperHelper.MapToViewModel(batch);
            List<QuestionPaper> assessmentList = new List<QuestionPaper>();
            List<long> assessmentIDList = new List<long>();
            ReportViewModel.subDeptWiseBarChartViewModelList = new List<ReportViewModel>();
            if (batch != null)
            {
                IQueryable<Student> studentList = batch.BatchStudent.Select(x => x.Student).AsQueryable();
                IQueryable<Branch> branches = college.CollegeBranch.Where(c => c.Branch.ParentBranchID == null).Select(c => c.Branch).Distinct().AsQueryable();
                foreach (Branch branch in branches)
                {
                    ReportViewModel reportViewModel = new ReportViewModel();
                    IQueryable<Branch> Childbranches = db.Branch.Where(b => b.ParentBranchID == branch.ID).Distinct();
                    List<BranchViewModel> branchViewModelList = new List<BranchViewModel>();
                    List<StudentViewModel> studentViewModelList = new List<StudentViewModel>();
                    foreach (Branch Childbranch in Childbranches)
                    {
                        BranchViewModel branchViewModel = new BranchViewModel();
                        branchViewModel.Name = Childbranch.Name;
                        branchViewModel.PassPercentageAverage = Childbranch.Student.Count() > 0 ? Childbranch.Student.SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Count() > 0 ? Childbranch.Student.SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Average(x => x.Marks ?? 0) : 0 : 0;
                        // decimal average = branch.Student.SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Average(x => x.Marks??0);
                        branchViewModelList.Add(branchViewModel);
                    }
                    reportViewModel.branchName = branch.Name;
                    reportViewModel.BranchViewModelList = branchViewModelList;
                    ReportViewModel.subDeptWiseBarChartViewModelList.Add(reportViewModel);
                }
            }
            return View(ReportViewModel);
        }
        public List<ReportViewModel> GetDeptWiseAvgHorzBarChart(long collegeIDs, long batchIDs, long assessmentIDs)
        {
            List<ReportViewModel> reportViewModelList = new List<ReportViewModel>();
            ReportViewModel ReportViewModel = new ReportViewModel();
            College college = new College();
            if (Convert.ToInt64(collegeIDs) > 0)
                college = db.College.Find(collegeIDs);

            // List<BatchViewModel> batchViewModel = new List<BatchViewModel>();
            Batch batch = db.Batch.Find(batchIDs);
            BatchViewModel batchViewModel = MapperHelper.MapToViewModel(batch);
            List<QuestionPaper> assessmentList = new List<QuestionPaper>();
            List<long> assessmentIDList = new List<long>();
            ReportViewModel.subDeptWiseBarChartViewModelList = new List<ReportViewModel>();
            if (batch != null)
            {
                IQueryable<Student> studentList = batch.BatchStudent.Select(x => x.Student).AsQueryable();
                IQueryable<Branch> branches = college.CollegeBranch.Where(c => c.Branch.ParentBranchID == null).Select(c => c.Branch).Distinct().AsQueryable();
                foreach (Branch branch in branches)
                {
                    ReportViewModel reportViewModel = new ReportViewModel();
                    IQueryable<Branch> Childbranches = db.Branch.Where(b => b.ParentBranchID == branch.ID).Distinct();
                    List<BranchViewModel> branchViewModelList = new List<BranchViewModel>();
                    List<StudentViewModel> studentViewModelList = new List<StudentViewModel>();
                    foreach (Branch Childbranch in Childbranches)
                    {
                        BranchViewModel branchViewModel = new BranchViewModel();
                        branchViewModel.Name = Childbranch.Name;
                        branchViewModel.PassPercentageAverage = Childbranch.Student.Count() > 0 ? Childbranch.Student.SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Count() > 0 ? Childbranch.Student.SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Average(x => x.Marks ?? 0) : 0 : 0;
                        // decimal average = branch.Student.SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Average(x => x.Marks??0);
                        branchViewModelList.Add(branchViewModel);
                    }
                    reportViewModel.branchName = branch.Name;
                    reportViewModel.BranchViewModelList = branchViewModelList;
                    reportViewModelList.Add(reportViewModel);
                    ReportViewModel.subDeptWiseBarChartViewModelList.Add(reportViewModel);
                }
            }
            return reportViewModelList;
        }

        public ActionResult ConsoleSectionWiseReport(long collegeIDs, long batchIDs, long assessmentIDs)
        {
            ReportViewModel reportViewModel = new ReportViewModel();
            QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentIDs);
            //List<QuestionPaperSection> questionPaperSectionListforName = questionPaper.QuestionPaperSection.Distinct().ToList();
            IQueryable<UserProfile> userProfileList = db.UserProfile.Where(b => b.Student.BatchStudent.Where(bs => bs.BatchID == batchIDs).Count() > 0);
            List<QuestionPaperSection> questionPaperSectionList = questionPaper.QuestionPaperSection.Distinct().ToList();

            //reportViewModel.Sections = string.Join(",", questionPaperSectionListforName.Select(x => x.Name).ToArray());
            //reportViewModel.Sections = "Max," + reportViewModel.Sections;

            reportViewModel.TotalSections = questionPaper.QuestionPaperSection.Count();
            if (questionPaper != null)
            {
                IEnumerable<UserAssessment> userAssessmentList = userProfileList.SelectMany(u => u.UserAssessment);
                List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();

                int yourscore = 0;
                decimal percentage = 0;
                foreach (QuestionPaperSection questionPaperSection in questionPaperSectionList)
                {
                    QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
                    questionPaperSectionViewModel.ID = questionPaperSection.ID;
                    questionPaperSectionViewModel.Name = questionPaperSection.Name;

                    int totalQuestions = questionPaperSection.QuestionPaperSectionQuestion.Count();
                    questionPaperSectionViewModel.TotalQuestions = totalQuestions;
                    foreach (UserAssessment userAssessment in userAssessmentList)
                    {
                        int correctAnswerCount = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == questionPaperSection.ID).Count() > 0) && (ass.Answer.IsCorrect ?? false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();     //Get only the single select type questions 
                        List<Question> multiSelectTypeQuestionList = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == questionPaperSection.ID).Count() > 0) && (ass.Question.QuestionType.Name.ToLower().Equals("multi select"))).Select(x => x.Question).Distinct().ToList();
                        foreach (Question multiSelectQuestion in multiSelectTypeQuestionList)
                        {
                            List<Answer> correctAnswerList = multiSelectQuestion.Answer.Where(a => a.IsCorrect ?? false).ToList();
                            List<Answer> userAnsweredList = multiSelectQuestion.AssessmentSnapshot.Select(x => x.Answer).ToList();
                            if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
                            {                                                                       //All the answers are correct
                                correctAnswerCount++;
                            }
                        }
                        questionPaperSectionViewModel.CorrectAnswerCount = correctAnswerCount;

                        // yourscore += correctAnswerCount;
                        reportViewModel.YourScore = yourscore;
                        float scorePercentage = 0;
                        if (totalQuestions != 0)
                        {
                            scorePercentage = ((float)correctAnswerCount / (float)totalQuestions) * 100;
                            questionPaperSectionViewModel.Percentage = (int)Math.Ceiling(scorePercentage);
                            percentage += questionPaperSectionViewModel.Percentage;
                        }


                    }
                    questionPaperSectionViewModel.Percentage = percentage;
                    questionPaperSectionViewModelList.Add(questionPaperSectionViewModel);
                    reportViewModel.QuestionPaperSectionViewModellList = questionPaperSectionViewModelList;
                }
            }
            //string result = "";
            //result = RenderPartialViewToString("~/Views/Report/ConsoleSectionWiseReport.cshtml", reportViewModel);
            //Response.Write(result);
            return View(reportViewModel);
        }
        //GetSectionWisePieChart() is the method counterpart for ActionResult ConsoleSectionWiseReport()                
        public ReportViewModel GetSectionWisePieChart(long collegeIDs, long batchIDs, long assessmentIDs)
        {
            ReportViewModel reportViewModel = new ReportViewModel();
            QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentIDs);
            //List<QuestionPaperSection> questionPaperSectionListforName = questionPaper.QuestionPaperSection.Distinct().ToList();
            IQueryable<UserProfile> userProfileList = db.UserProfile.Where(b => b.Student.BatchStudent.Where(bs => bs.BatchID == batchIDs).Count() > 0);
            //IQueryable<UserProfile> userProfileList = db.Student.Where(c => c.CollegeID == collegeIDs).SelectMany(s => s.UserProfile);
            //userProfileList = userProfileList.Where(b => b.Student.BatchStudent.Where(bs => bs.BatchID == batchIDs).Count() > 0);
            List<QuestionPaperSection> questionPaperSectionList = questionPaper.QuestionPaperSection.Distinct().ToList();

            //reportViewModel.Sections = string.Join(",", questionPaperSectionListforName.Select(x => x.Name).ToArray());
            //reportViewModel.Sections = "Max," + reportViewModel.Sections;

            reportViewModel.TotalSections = questionPaper.QuestionPaperSection.Count();
            if (questionPaper != null)
            {
                IEnumerable<UserAssessment> userAssessmentList = userProfileList.SelectMany(u => u.UserAssessment);
                List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();

                int yourscore = 0;
                decimal percentage = 0;
                foreach (QuestionPaperSection questionPaperSection in questionPaperSectionList)
                {
                    QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
                    questionPaperSectionViewModel.ID = questionPaperSection.ID;
                    questionPaperSectionViewModel.Name = questionPaperSection.Name;

                    int totalQuestions = questionPaperSection.QuestionPaperSectionQuestion.Count();
                    questionPaperSectionViewModel.TotalQuestions = totalQuestions;
                    foreach (UserAssessment userAssessment in userAssessmentList)
                    {
                        int correctAnswerCount = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == questionPaperSection.ID).Count() > 0) && (ass.Answer.IsCorrect ?? false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();     //Get only the single select type questions 
                        List<Question> multiSelectTypeQuestionList = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == questionPaperSection.ID).Count() > 0) && (ass.Question.QuestionType.Name.ToLower().Equals("multi select"))).Select(x => x.Question).Distinct().ToList();
                        foreach (Question multiSelectQuestion in multiSelectTypeQuestionList)
                        {
                            List<Answer> correctAnswerList = multiSelectQuestion.Answer.Where(a => a.IsCorrect ?? false).ToList();
                            List<Answer> userAnsweredList = multiSelectQuestion.AssessmentSnapshot.Select(x => x.Answer).ToList();
                            if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
                            {                                                                       //All the answers are correct
                                correctAnswerCount++;
                            }
                        }
                        questionPaperSectionViewModel.CorrectAnswerCount = correctAnswerCount;
                        // yourscore += correctAnswerCount;
                        reportViewModel.YourScore = yourscore;
                        float scorePercentage = 0;
                        if (totalQuestions != 0)
                        {
                            scorePercentage = ((float)correctAnswerCount / (float)totalQuestions) * 100;
                            questionPaperSectionViewModel.Percentage = (int)Math.Ceiling(scorePercentage);
                            percentage += questionPaperSectionViewModel.Percentage;
                        }
                    }
                    questionPaperSectionViewModel.Percentage = percentage;
                    questionPaperSectionViewModelList.Add(questionPaperSectionViewModel);
                    reportViewModel.QuestionPaperSectionViewModellList = questionPaperSectionViewModelList;
                }
            }
            //string result = "";
            //result = RenderPartialViewToString("~/Views/Report/ConsoleSectionWiseReport.cshtml", reportViewModel);
            //Response.Write(result);
            return reportViewModel;
        }

        public ActionResult DeptWisePieChart(long collegeIDs, long batchIDs, long assessmentIDs)
        {
            ReportViewModel reportViewModel = new ReportViewModel();
            QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentIDs);
            //List<QuestionPaperSection> questionPaperSectionListforName = questionPaper.QuestionPaperSection.Distinct().ToList();
            //IQueryable<UserProfile> userProfileList = db.UserProfile.Where(b => b.Student.BatchStudent.Where(bs => bs.BatchID == batchIDs).Count() > 0);
            IQueryable<UserProfile> userProfileList = db.Student.Where(c => c.CollegeID == collegeIDs).SelectMany(s => s.UserProfile);
            userProfileList = userProfileList.Where(b => b.Student.BatchStudent.Where(bs => bs.BatchID == batchIDs).Count() > 0);
            List<QuestionPaperSection> questionPaperSectionList = questionPaper.QuestionPaperSection.Distinct().ToList();

            //reportViewModel.Sections = string.Join(",", questionPaperSectionListforName.Select(x => x.Name).ToArray());
            //reportViewModel.Sections = "Max," + reportViewModel.Sections;

            reportViewModel.TotalSections = questionPaper.QuestionPaperSection.Count();
            if (questionPaper != null)
            {
                IEnumerable<UserAssessment> userAssessmentList = userProfileList.SelectMany(u => u.UserAssessment);
                List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();

                int yourscore = 0;
                decimal percentage = 0;
                foreach (QuestionPaperSection questionPaperSection in questionPaperSectionList)
                {
                    QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
                    questionPaperSectionViewModel.ID = questionPaperSection.ID;
                    questionPaperSectionViewModel.Name = questionPaperSection.Name;

                    int totalQuestions = questionPaperSection.QuestionPaperSectionQuestion.Count();
                    questionPaperSectionViewModel.TotalQuestions = totalQuestions;
                    foreach (UserAssessment userAssessment in userAssessmentList)
                    {
                        int correctAnswerCount = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == questionPaperSection.ID).Count() > 0) && (ass.Answer.IsCorrect ?? false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();     //Get only the single select type questions 
                        List<Question> multiSelectTypeQuestionList = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == questionPaperSection.ID).Count() > 0) && (ass.Question.QuestionType.Name.ToLower().Equals("multi select"))).Select(x => x.Question).Distinct().ToList();
                        foreach (Question multiSelectQuestion in multiSelectTypeQuestionList)
                        {
                            List<Answer> correctAnswerList = multiSelectQuestion.Answer.Where(a => a.IsCorrect ?? false).ToList();
                            List<Answer> userAnsweredList = multiSelectQuestion.AssessmentSnapshot.Select(x => x.Answer).ToList();
                            if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
                            {                                                                       //All the answers are correct
                                correctAnswerCount++;
                            }
                        }
                        questionPaperSectionViewModel.CorrectAnswerCount = correctAnswerCount;
                        // yourscore += correctAnswerCount;
                        reportViewModel.YourScore = yourscore;
                        float scorePercentage = 0;
                        if (totalQuestions != 0)
                        {
                            scorePercentage = ((float)correctAnswerCount / (float)totalQuestions) * 100;
                            questionPaperSectionViewModel.Percentage = (int)Math.Ceiling(scorePercentage);
                            percentage += questionPaperSectionViewModel.Percentage;
                        }
                    }
                    questionPaperSectionViewModel.Percentage = percentage;
                    questionPaperSectionViewModelList.Add(questionPaperSectionViewModel);
                    reportViewModel.QuestionPaperSectionViewModellList = questionPaperSectionViewModelList;
                }
            }
            //string result = "";
            //result = RenderPartialViewToString("~/Views/Report/ConsoleSectionWiseReport.cshtml", reportViewModel);
            //Response.Write(result);
            return View(reportViewModel);
        }
        public List<ReportViewModel> GetDeptWisePieChart(long collegeIDs, long batchIDs, long assessmentIDs)
        {
            List<ReportViewModel> reportViewModelList = new List<ReportViewModel>();
            QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentIDs);
            //List<QuestionPaperSection> questionPaperSectionListforName = questionPaper.QuestionPaperSection.Distinct().ToList();
            //IQueryable<UserProfile> userProfileList = db.UserProfile.Where(b => b.Student.BatchStudent.Where(bs => bs.BatchID == batchIDs).Count() > 0);
            IQueryable<Branch> branchList = db.CollegeBranch.Where(c => c.CollegeID == collegeIDs).Select(c => c.Branch).Distinct();
            foreach (Branch branch in branchList)
            {
                ReportViewModel reportViewModel = new ReportViewModel();
                IQueryable<UserProfile> userProfileList = db.Student.Where(c => c.BranchID == branch.ID).SelectMany(s => s.UserProfile).Distinct();
                userProfileList = userProfileList.Where(b => b.Student.BatchStudent.Where(bs => bs.BatchID == batchIDs).Count() > 0);

                List<QuestionPaperSection> questionPaperSectionList = questionPaper.QuestionPaperSection.Distinct().ToList();

                //reportViewModel.Sections = string.Join(",", questionPaperSectionListforName.Select(x => x.Name).ToArray());
                //reportViewModel.Sections = "Max," + reportViewModel.Sections;

                reportViewModel.branchIDs = branchList.Select(b => b.ID).ToList();
                reportViewModel.branchID = branch.ID;
                reportViewModel.branchName = branch.Name;
                reportViewModel.TotalSections = questionPaper.QuestionPaperSection.Distinct().Count();
                if (questionPaper != null)
                {
                    IEnumerable<UserAssessment> userAssessmentList = userProfileList.SelectMany(u => u.UserAssessment);
                    List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();

                    int yourscore = 0;
                    decimal percentage = 0;
                    foreach (QuestionPaperSection questionPaperSection in questionPaperSectionList)
                    {
                        QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
                        questionPaperSectionViewModel.ID = questionPaperSection.ID;
                        questionPaperSectionViewModel.Name = questionPaperSection.Name;

                        int totalQuestions = questionPaperSection.QuestionPaperSectionQuestion.Count();
                        questionPaperSectionViewModel.TotalQuestions = totalQuestions;
                        foreach (UserAssessment userAssessment in userAssessmentList)
                        {
                            int correctAnswerCount = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == questionPaperSection.ID).Count() > 0) && (ass.Answer.IsCorrect ?? false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count();     //Get only the single select type questions 
                            List<Question> multiSelectTypeQuestionList = userAssessment.AssessmentSnapshot.Where(ass => (ass.Question.QuestionTags.Where(qt => qt.TagID == questionPaperSection.ID).Count() > 0) && (ass.Question.QuestionType.Name.ToLower().Equals("multi select"))).Select(x => x.Question).Distinct().ToList();
                            foreach (Question multiSelectQuestion in multiSelectTypeQuestionList)
                            {
                                List<Answer> correctAnswerList = multiSelectQuestion.Answer.Where(a => a.IsCorrect ?? false).ToList();
                                List<Answer> userAnsweredList = multiSelectQuestion.AssessmentSnapshot.Select(x => x.Answer).ToList();
                                if (correctAnswerList.Except(userAnsweredList).Count() < 0 || userAnsweredList.Except(correctAnswerList).Count() < 0)
                                {                                                                       //All the answers are correct
                                    correctAnswerCount++;
                                }
                            }
                            questionPaperSectionViewModel.CorrectAnswerCount = correctAnswerCount;
                            // yourscore += correctAnswerCount;
                            reportViewModel.YourScore = yourscore;
                            float scorePercentage = 0;
                            if (totalQuestions != 0)
                            {
                                scorePercentage = ((float)correctAnswerCount / (float)totalQuestions) * 100;
                                questionPaperSectionViewModel.Percentage = (int)Math.Ceiling(scorePercentage);
                                percentage += questionPaperSectionViewModel.Percentage;
                            }
                        }
                        questionPaperSectionViewModel.Percentage = percentage;
                        questionPaperSectionViewModelList.Add(questionPaperSectionViewModel);
                        reportViewModel.QuestionPaperSectionViewModellList = questionPaperSectionViewModelList;
                    }
                }
                reportViewModelList.Add(reportViewModel);
            }

            //string result = "";
            //result = RenderPartialViewToString("~/Views/Report/ConsoleSectionWiseReport.cshtml", reportViewModel);
            //Response.Write(result);
            return reportViewModelList;
        }

        public ActionResult DeptWiseBarChart(long collegeIDs, long batchIDs, long assessmentIDs)
        {
            College college = new College();
            if (Convert.ToInt64(collegeIDs) > 0)
                college = db.College.Find(collegeIDs);

            // List<BatchViewModel> batchViewModel = new List<BatchViewModel>();
            ReportViewModel reportViewModel = new ReportViewModel();
            Batch batch = db.Batch.Find(batchIDs);
            BatchViewModel batchViewModel = MapperHelper.MapToViewModel(batch);
            List<QuestionPaper> assessmentList = new List<QuestionPaper>();
            List<long> assessmentIDList = new List<long>();
            if (batch != null)
            {
                IQueryable<Student> studentList = batch.BatchStudent.Select(x => x.Student).AsQueryable();
                IEnumerable<Branch> branches = college.CollegeBranch.Select(c => c.Branch).Distinct().AsEnumerable();
                List<BranchViewModel> branchViewModelList = new List<BranchViewModel>();
                List<StudentViewModel> studentViewModelList = new List<StudentViewModel>();
                foreach (Branch branch in branches)
                {
                    BranchViewModel branchViewModel = new BranchViewModel();
                    branchViewModel.Name = branch.Name;
                    branchViewModel.PassPercentageAverage = branch.Student.Count() > 0 ? branch.Student.SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Average(x => x.Marks ?? 0) : 0;
                    // decimal average = branch.Student.SelectMany(x => x.UserProfile).SelectMany(x => x.UserAssessment).Average(x => x.Marks??0);
                    branchViewModelList.Add(branchViewModel);
                }
                reportViewModel.BranchViewModelList = branchViewModelList;
                reportViewModel.StudentViewModelList = studentViewModelList;
            }
            return View(reportViewModel);
        }
        public List<ReportViewModel> GetDeptWiseBarChart(long collegeIDs, long batchIDs, long assessmentIDs)
        {
            List<ReportViewModel> reportViewModelList = new List<ReportViewModel>();
            QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentIDs);
            //List<QuestionPaperSection> questionPaperSectionListforName = questionPaper.QuestionPaperSection.Distinct().ToList();
            //IQueryable<UserProfile> userProfileList = db.UserProfile.Where(b => b.Student.BatchStudent.Where(bs => bs.BatchID == batchIDs).Count() > 0);
            IQueryable<Branch> branchList = db.CollegeBranch.Where(c => c.CollegeID == collegeIDs).Select(c => c.Branch).Distinct();
            foreach (Branch branch in branchList)
            {
                ReportViewModel reportViewModel = new ReportViewModel();
                IQueryable<UserProfile> userProfileList = db.Student.Where(c => c.BranchID == branch.ID).SelectMany(s => s.UserProfile).Distinct();
                userProfileList = userProfileList.Where(b => b.Student.BatchStudent.Where(bs => bs.BatchID == batchIDs).Count() > 0);
                List<QuestionPaperSection> questionPaperSectionList = questionPaper.QuestionPaperSection.Distinct().ToList();
                //reportViewModel.Sections = string.Join(",", questionPaperSectionListforName.Select(x => x.Name).ToArray());
                //reportViewModel.Sections = "Max," + reportViewModel.Sections;
                reportViewModel.branchIDs = branchList.Select(b => b.ID).ToList();
                reportViewModel.branchID = branch.ID;
                reportViewModel.branchName = branch.Name;
                reportViewModel.TotalSections = questionPaper.QuestionPaperSection.Distinct().Count();
                if (questionPaper != null)
                {
                    IEnumerable<UserAssessment> userAssessmentList = userProfileList.SelectMany(u => u.UserAssessment);
                    List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();
                    questionPaperSectionViewModelList = MapperHelper.MapToViewModelList(questionPaperSectionList);
                    //int yourscore = 0;
                    //decimal percentage = 0;
                    foreach (QuestionPaperSectionViewModel Section in questionPaperSectionViewModelList)
                    {
                        QuestionPaperSection questionPaperSection = db.QuestionPaperSection.Find(Section.ID);
                        //ReportViewModel reportViewModel = new ReportViewModel();
                        List<QuestionPaperSectionQuestion> questionPaperQuestion = new List<QuestionPaperSectionQuestion>();
                        List<Question> questionList = new List<Question>();

                        List<string> WholetagsList = new List<string>();
                        List<TagViewModel> tagViewModelList = new List<TagViewModel>();
                        //IQueryable<QuestionPaper> questionPapers = db.BatchAssessment.Where(b => b.AssessmentID == assessmentIDs && b.BatchID == batchIDs).Select(b => b.QuestionPaper);
                        List<QuestionTags> questionTagList = new List<QuestionTags>();
                        List<Tag> tagsList = new List<Tag>();


                        questionPaperQuestion = questionPaperSection.QuestionPaperSectionQuestion.ToList();
                        questionList = questionPaperQuestion.Select(x => x.Question).ToList();
                        questionTagList = questionList.SelectMany(x => x.QuestionTags).ToList();
                        tagsList = questionTagList.Select(x => x.Tag).Distinct().ToList();
                        foreach (Tag tag in tagsList)
                        {
                            TagViewModel tagViewModel = new TagViewModel();
                            List<Question> questionUnderThisAssessment = tag.QuestionTags.Where(qt => qt.Tag.ID == tag.ID).Select(x => x.Question).ToList();
                            if (questionUnderThisAssessment.Count() > 0)
                            {
                                questionUnderThisAssessment = questionUnderThisAssessment.Where(qt => qt.AssessmentSnapshot.Where(ass => ass.UserAssessment.UserProfile.Student.CollegeID == collegeIDs).Count() > 0).ToList();
                                List<int> correctAnsweredList = new List<int>();
                                correctAnsweredList = questionUnderThisAssessment.Select(x => x.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault() != null ? x.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID : 0).ToList();
                                List<int> userAnsweredList = questionUnderThisAssessment.SelectMany(x => x.AssessmentSnapshot.Where(ass => ass.QuestionID == x.ID).Select(ass => ass.AnswerID ?? 0)).Distinct().ToList();

                                int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => userAnsweredList.Contains(qua.Answer.Where(a => a.IsCorrect ?? false).Select(x => x.ID).FirstOrDefault())).Count();
                                decimal correctAnswerPercentage = (decimal)((correctAnsweredQuestions / (decimal)((userAnsweredList.Count() != 0 ? questionUnderThisAssessment.Count() : 1))) * 100);
                                tagViewModel.ID = tag.ID;
                                tagViewModel.Name = tag.Name;
                                tagViewModel.PreTrainingValue = correctAnswerPercentage;
                                tagViewModelList.Add(tagViewModel);
                            }
                        }
                        reportViewModel.TagViewModelList = tagViewModelList;
                        reportViewModelList.Add(reportViewModel);
                    }
                }
                reportViewModelList.Add(reportViewModel);
            }
            return reportViewModelList;
        }

        public ActionResult SubSectionPerformance(long collegeIDs = 0, long batchIDs = 0, long assessmentIDs = 0)
        {

            UserAssessment userAssessment = db.UserAssessment.Where(ua => ua.UserProfile.UserName.ToLower() == User.Identity.Name && ua.AssessmentID == assessmentIDs).FirstOrDefault();
            ReportViewModel reportViewModel = new ReportViewModel();
            QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentIDs);
            List<QuestionPaperSection> questionPaperSectionListforName = questionPaper.QuestionPaperSection.Distinct().ToList();
            ReportViewModel.QuestionPaperReportModel assessmentReportModel = new ReportViewModel.QuestionPaperReportModel();
            IEnumerable<QuestionTags> questionTags = questionPaperSectionListforName.SelectMany(q => q.QuestionPaperSectionQuestion).Select(qs => qs.Question).SelectMany(q => q.QuestionTags);
            IQueryable<UserProfile> userProfileList = db.UserProfile.Where(b => b.Student.BatchStudent.Where(bs => bs.BatchID == batchIDs).Count() > 0);
            List<QuestionPaperSectionQuestion> questionPaperQuestion = new List<QuestionPaperSectionQuestion>();
            List<Question> questionList = new List<Question>();

            List<string> WholetagsList = new List<string>();
            List<TagViewModel> tagViewModelList = new List<TagViewModel>();
            IQueryable<QuestionPaper> questionPapers = db.BatchAssessment.Where(b => b.AssessmentID == assessmentIDs && b.BatchID == batchIDs).Select(b => b.QuestionPaper);
            List<QuestionTags> questionTagList = new List<QuestionTags>();
            List<Tag> tagsList = new List<Tag>();

            if (questionPapers.Count() > 0)
            {
                questionPaperQuestion = questionPapers.SelectMany(x => x.QuestionPaperSection).SelectMany(x => x.QuestionPaperSectionQuestion).ToList();
                questionList = questionPaperQuestion.Select(x => x.Question).ToList();
                questionTagList = questionList.SelectMany(x => x.QuestionTags).ToList();
                tagsList = questionTagList.Select(x => x.Tag).Distinct().ToList();
                foreach (Tag tag in tagsList)
                {
                    TagViewModel tagViewModel = new TagViewModel();
                    List<Question> questionUnderThisAssessment = tag.QuestionTags.Where(qt => qt.Tag.ID == tag.ID).Select(x => x.Question).ToList();
                    if (questionUnderThisAssessment.Count() > 0)
                    {
                        questionUnderThisAssessment = questionUnderThisAssessment.Where(qt => qt.AssessmentSnapshot.Where(ass => ass.UserAssessment.UserProfile.Student.CollegeID == collegeIDs).Count() > 0).ToList();
                        List<int> correctAnsweredList = new List<int>();
                        correctAnsweredList = questionUnderThisAssessment.Select(x => x.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault() != null ? x.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID : 0).ToList();
                        List<int> userAnsweredList = questionUnderThisAssessment.SelectMany(x => x.AssessmentSnapshot.Where(ass => ass.QuestionID == x.ID).Select(ass => ass.AnswerID ?? 0)).Distinct().ToList();

                        int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => userAnsweredList.Contains(qua.Answer.Where(a => a.IsCorrect ?? false).Select(x => x.ID).FirstOrDefault())).Count();
                        decimal correctAnswerPercentage = (decimal)((correctAnsweredQuestions / (decimal)((userAnsweredList.Count() != 0 ? questionUnderThisAssessment.Count() : 1))) * 100);
                        tagViewModel.ID = tag.ID;
                        tagViewModel.Name = tag.Name;
                        tagViewModel.PreTrainingValue = correctAnswerPercentage;
                        tagViewModelList.Add(tagViewModel);

                    }

                }
            }
            reportViewModel.TagViewModelList = tagViewModelList;
            reportViewModel.TotalSections = questionPaper.QuestionPaperSection.Count();

            return View(reportViewModel);
        }
        public List<ReportViewModel> SubSectionPerformanceList(List<QuestionPaperSectionViewModel> QuestionPaperSectionViewModellList, long collegeIDs)
        { //<<<<<<<<<<Bar Chart>>>>>>>>>>
            List<ReportViewModel> reportViewModelList = new List<ReportViewModel>();
            //UserAssessment userAssessment = db.UserAssessment.Where(ua => ua.UserProfile.UserName.ToLower() == User.Identity.Name && ua.AssessmentID == assessmentIDs).FirstOrDefault();
            //ReportViewModel reportViewModel = new ReportViewModel();
            //QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentIDs);
            //List<QuestionPaperSection> questionPaperSectionListforName = questionPaper.QuestionPaperSection.Distinct().ToList();
            //ReportViewModel.QuestionPaperReportModel assessmentReportModel = new ReportViewModel.QuestionPaperReportModel();
            //IEnumerable<QuestionTags> questionTags = questionPaperSectionListforName.SelectMany(q => q.QuestionPaperSectionQuestion).Select(qs => qs.Question).SelectMany(q => q.QuestionTags);
            //IQueryable<UserProfile> userProfileList = db.UserProfile.Where(b => b.Student.BatchStudent.Where(bs => bs.BatchID == batchIDs).Count() > 0);
            foreach (QuestionPaperSectionViewModel Section in QuestionPaperSectionViewModellList)
            {
                QuestionPaperSection questionPaperSection = db.QuestionPaperSection.Find(Section.ID);
                ReportViewModel reportViewModel = new ReportViewModel();
                List<QuestionPaperSectionQuestion> questionPaperQuestion = new List<QuestionPaperSectionQuestion>();
                List<Question> questionList = new List<Question>();

                List<string> WholetagsList = new List<string>();
                List<TagViewModel> tagViewModelList = new List<TagViewModel>();
                //IQueryable<QuestionPaper> questionPapers = db.BatchAssessment.Where(b => b.AssessmentID == assessmentIDs && b.BatchID == batchIDs).Select(b => b.QuestionPaper);
                List<QuestionTags> questionTagList = new List<QuestionTags>();
                List<Tag> tagsList = new List<Tag>();


                questionPaperQuestion = questionPaperSection.QuestionPaperSectionQuestion.ToList();
                questionList = questionPaperQuestion.Select(x => x.Question).ToList();
                questionTagList = questionList.SelectMany(x => x.QuestionTags).ToList();
                tagsList = questionTagList.Select(x => x.Tag).Distinct().ToList();
                foreach (Tag tag in tagsList)
                {
                    TagViewModel tagViewModel = new TagViewModel();
                    List<Question> questionUnderThisAssessment = tag.QuestionTags.Where(qt => qt.Tag.ID == tag.ID).Select(x => x.Question).ToList();
                    if (questionUnderThisAssessment.Count() > 0)
                    {
                        questionUnderThisAssessment = questionUnderThisAssessment.Where(qt => qt.AssessmentSnapshot.Where(ass => ass.UserAssessment.UserProfile.Student.CollegeID == collegeIDs).Count() > 0).ToList();
                        List<int> correctAnsweredList = new List<int>();
                        correctAnsweredList = questionUnderThisAssessment.Select(x => x.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault() != null ? x.Answer.Where(a => a.IsCorrect ?? false).FirstOrDefault().ID : 0).ToList();
                        List<int> userAnsweredList = questionUnderThisAssessment.SelectMany(x => x.AssessmentSnapshot.Where(ass => ass.QuestionID == x.ID).Select(ass => ass.AnswerID ?? 0)).Distinct().ToList();

                        int correctAnsweredQuestions = questionUnderThisAssessment.Where(qua => userAnsweredList.Contains(qua.Answer.Where(a => a.IsCorrect ?? false).Select(x => x.ID).FirstOrDefault())).Count();
                        decimal correctAnswerPercentage = (decimal)((correctAnsweredQuestions / (decimal)((userAnsweredList.Count() != 0 ? questionUnderThisAssessment.Count() : 1))) * 100);
                        tagViewModel.ID = tag.ID;
                        tagViewModel.Name = tag.Name;
                        tagViewModel.PreTrainingValue = correctAnswerPercentage;
                        tagViewModelList.Add(tagViewModel);

                    }

                }
                reportViewModel.TagViewModelList = tagViewModelList;
                reportViewModelList.Add(reportViewModel);
            }


            return reportViewModelList;
        }

        #endregion

        //public string[] columns { get; set; }

        public int branchIDs { get; set; }
    }

    public class RadarReportModel
    {

        public string name { get; set; }

        public List<decimal> data { get; set; }

        // public string pointPlacement { get; set; }

        public List<decimal> PreTrainingdata { get; set; }

        public List<decimal> PostTrainingdata { get; set; }

        public List<string> Labels { get; set; }

        public List<TagViewModel> TagViewModelList { get; set; }
    }
}
