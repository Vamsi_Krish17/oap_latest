﻿using HelperClasses;
using OAP.Domain;
using OAP.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OAP.Controllers
{
    public class StudentResumeController : BaseController
    {
        private OAPEntities db = new OAPEntities();
        //
        // GET: /StudentResume/

        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /StudentResume/Details/5

        public ActionResult Details()
        {
            int userId = GetCurrentUserID();
            UserProfile userProfile = db.UserProfile.Find(userId);
            Student student = userProfile.Student;
            StudentResume studentResume = student.StudentResume.FirstOrDefault();
            StudentResumeViewModel studentResumeViewModel = new StudentResumeViewModel();
            if (studentResume != null)
            {
                studentResumeViewModel = MapperHelper.MapToViewModel(studentResume);
            }
            return View(studentResumeViewModel);
        }

        //
        // GET: /StudentResume/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /StudentResume/Create

        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /StudentResume/Edit/5

        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /StudentResume/Edit/5

        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /StudentResume/Delete/5

        public ActionResult Upload(HttpPostedFileBase file, long ID = 0)
        {
            int userId = GetCurrentUserID();
            UserProfile userProfile = db.UserProfile.Find(userId);
            Student student = userProfile.Student;
            string guidString = Guid.NewGuid().ToString();
            if (!Directory.Exists(Server.MapPath("~/Upload/" + student.ID)))
                Directory.CreateDirectory(Server.MapPath("~/Upload/" + student.ID));


            if (ID == 0)
            {
                StudentResume studentResume = new StudentResume();
                studentResume.Student = student;
                studentResume.ModifiedDate = studentResume.UploadedDate = DateTime.Now;
                if (file != null)
                {
                    string path = Server.MapPath("~/Upload/" + student.ID + "/" + guidString +"."+ file.FileName.Split('.')[1]);
                    file.SaveAs(path);
                    studentResume.FilePath = guidString + "." + file.FileName.Split('.')[1];
                }
                db.StudentResume.Add(studentResume);

            }
            else
            {
                StudentResume studentResume = db.StudentResume.Find(ID);
                studentResume.Student = student;
                studentResume.UploadedDate = DateTime.Now;
                if (file != null)
                {
                    string existingFilePath = Server.MapPath("~/Upload/" + student.ID + "/" + studentResume.FilePath);
                    if (System.IO.File.Exists(existingFilePath))
                    {
                        System.IO.File.Delete(existingFilePath);
                    }
                    string path = Server.MapPath("~/Upload/" + student.ID + "/" + guidString + "." + file.FileName.Split('.')[1]);
                    file.SaveAs(path);
                    studentResume.FilePath = guidString + "." + file.FileName.Split('.')[1];
                }
                db.Entry(studentResume).State = EntityState.Modified;
            }
            db.SaveChanges();
            return RedirectToAction("Details");
        }

        //
        // POST: /StudentResume/Delete/5

        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
