﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HelperClasses;
using OAP.Domain.ViewModel;
//using OAP.DataAccess;
using Newtonsoft.Json;
using OAP.Domain;
using WebMatrix.WebData;
using OAP.Domain.ViewModel.DataTableViewModel;
using OAP.Models.ViewModels;
using System.Globalization;
using System.Data.Entity;


namespace OAP.Controllers
{
    public class DashboardController : BaseController
    {
        private OAPEntities db = new OAPEntities();
        private static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");
        public ActionResult Index()
        {

            //if (User.IsInRole("SuperAdmin"))
            if (User.IsInRole("SuperAdmin"))
            {                                                           //User is SuperAdmin
                return View("~/Views/Dashboard/SuperAdminDashboard.cshtml");
            }
            //if (User.IsInRole("InstituteAdmin"))
            if (User.IsInRole("CollegeAdmin"))
            {
                OAP.Domain.UserProfile userProfile = db.UserProfile.Where(x => x.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
                if (userProfile != null)
                {
                    bool test = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                    ViewBag.CanScheduleRetest = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                }
                else
                {
                    ViewBag.CanScheduleRetest = false;
                }

                return View("~/Views/DashBoard/CollegeAdminDashboard.cshtml");
            }
            else if (User.IsInRole("Student"))
            {                                                           //User is student
                UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
                int userID = userProfile.UserId;
                if (User.IsInRole("Student"))
                {
                    StudentViewModel studentViewModel = new StudentViewModel();
                    int userId = GetCurrentUserID();
                    DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                  //  studentViewModel.ScheduledTestCount = db.BatchAssessment.Where(f => f.StartDateTime < DateTime.Now && f.EndDateTime > DateTime.Now).Count();
                    studentViewModel.ScheduledTestCount = userProfile.Student.BatchStudent.SelectMany(ba => ba.Batch.BatchAssessment).Where(ga => (ga.StartDateTime <= indianTime) && ga.EndDateTime >= indianTime).Distinct().Count();

                    studentViewModel.TakenTestCount = userProfile.UserAssessment.Where(ua => ua.UserID == userId).Select(x => x.QuestionPaper).Count();
                    if (studentViewModel.TakenTestCount < studentViewModel.ScheduledTestCount)
                    {
                        studentViewModel.RemainingTestCount = studentViewModel.ScheduledTestCount - studentViewModel.TakenTestCount;
                    }
                    else
                    {
                        studentViewModel.RemainingTestCount = studentViewModel.TakenTestCount - studentViewModel.ScheduledTestCount;
                    }

                    IEnumerable<QuestionPaper> questionPapers = userProfile.UserAssessment.Where(ua => ua.FinishTime != null).Select(ua => ua.QuestionPaper).Distinct();
                    List<OAP.Domain.ViewModel.QuestionPaperViewModel> questionPaperViewModelList = (from a in questionPapers
                                                                                                    select new OAP.Domain.ViewModel.QuestionPaperViewModel()
                                                                                                    {
                                                                                                        ID = a.ID,
                                                                                                        Name = a.Name,
                                                                                                        Code = a.Code,
                                                                                                        CutOffMark = a.CutOffMark != null ? a.CutOffMark.Value : 0,
                                                                                                        Description = a.Description,
                                                                                                        DisplayMarks = a.DisplayMarks != null ? a.DisplayMarks.Value : false,
                                                                                                        DisplayResult = a.DisplayResult != null ? a.DisplayResult.Value : false,
                                                                                                        EndDate = a.EndDateTime != null ? a.EndDateTime.Value : new DateTime(),
                                                                                                        StartDate = a.StatDateTime != null ? a.StatDateTime.Value : new DateTime(),
                                                                                                        Instructions = a.Instructions,
                                                                                                        //Marks = a.UserAssessment.Where(ua => ua.UserID == userId).Select(u => u.Marks ?? 0).FirstOrDefault(),
                                                                                                        Marks = a.UserAssessment.Where(ua => ua.UserID == userId).Select(ua => ua.AssessmentSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count()).FirstOrDefault(),

                                                                                                        FinishTime = a.UserAssessment.Where(ua => ua.UserID == userId).Select(u => u.FinishTime).FirstOrDefault(),
                                                                                                        StartTime = a.UserAssessment.Where(ua => ua.UserID == userId).Select(u => u.StartTime).FirstOrDefault(),
                                                                                                        NegativeMarks = a.NegativeMarks != null ? a.NegativeMarks.Value : 0,
                                                                                                        IsNegativePercentage = a.IsNegativePercentage != null ? a.IsNegativePercentage.Value : false,
                                                                                                        ReleaseKey = a.ReleaseKey != null ? a.ReleaseKey.Value : false,
                                                                                                        TimeLimit = a.TimeLimit != null ? a.TimeLimit.Value : 0,
                                                                                                        ShowInstructions = a.ShowInstruction ?? false,
                                                                                                        ShowReport = a.ShowReport ?? false,
                                                                                                        createdDate = a.CreatedDate,
                                                                                                        CanJumpSections = a.CanJumpSections ?? false,
                                                                                                        IsHaveRetestRequest = a.IsHaveRetestRequest ?? false,
                                                                                                        CanShuffle = a.CanShuffle ?? false
                                                                                                    }).OrderBy(x=>x.Name).ToList();
                    questionPaperViewModelList = questionPaperViewModelList;
                    //studentViewModel.MarksList = questionPaperViewModelList.Select(s=>s.Marks).ToList();
                    studentViewModel.AssessmentViewModelList = questionPaperViewModelList;
                    return View("~/Views/Dashboard/StudentDashboard.cshtml", studentViewModel);

                }
                else
                {
                    if (User.Identity.IsAuthenticated)
                        return RedirectToAction("LogOff", "Account");
                    else
                        return RedirectToAction("Login", "Account");
                }
            }
            return View();
        }
        string[] columns = { "QuestionPaperName", "Date", "Marks", "BatchRank", "CollegeRank", "NationalRank", "LastUpdatedDate" };
        //    public void StudentList()
        //    {
        //        UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
        //        int userID = userProfile.UserId;

        //        datatableRequest = new DataTableRequest(Request, columns);

        //        StudentViewModel studentViewModel = new StudentViewModel();
        //        int userId = GetCurrentUserID();
        //        //studentViewModel.ScheduledTestCount = userProfile.Student.BatchStudent.SelectMany(x => x.Batch.BatchAssessment).Select(x => x.QuestionPaper).Distinct().Count();
        //        //studentViewModel.TakenTestCount = userProfile.UserAssessment.Count();
        //        //studentViewModel.RemainingTestCount = studentViewModel.ScheduledTestCount - studentViewModel.TakenTestCount;

        //        DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
        //        //studentViewModel.ScheduledTestCount = userProfile.Student.BatchStudent.SelectMany(ba => ba.Batch.BatchAssessment).Where(ga => (ga.StartDateTime <= indianTime) && ga.EndDateTime >= indianTime).Select(ga => ga.QuestionPaper.ID).Distinct().Count();
        //        studentViewModel.ScheduledTestCount = userProfile.Student.BatchStudent.SelectMany(x => x.Batch.BatchAssessment).Count();
        //        studentViewModel.TakenTestCount = userProfile.UserAssessment.Where(ua => ua.UserID == userId).Count();
        //        if (studentViewModel.TakenTestCount < studentViewModel.ScheduledTestCount)
        //        {
        //            studentViewModel.RemainingTestCount = studentViewModel.ScheduledTestCount - studentViewModel.TakenTestCount;
        //        }
        //        else
        //        {
        //            studentViewModel.RemainingTestCount = studentViewModel.TakenTestCount - studentViewModel.ScheduledTestCount;
        //        }
        //        IEnumerable<QuestionPaper> questionPapers = userProfile.UserAssessment.Select(ua => ua.QuestionPaper).Distinct();
        //        List<OAP.Domain.ViewModel.QuestionPaperViewModel> questionPaperViewModelList = (from a in questionPapers
        //                                                                                        select new QuestionPaperViewModel()
        //                                                                                        {
        //                                                                                            ID = a.ID,
        //                                                                                            QuestionPaperID = a.UserAssessment.Where(ua => ua.UserID == userId).Select(u => u.ID).FirstOrDefault(),
        //                                                                                            Name = a.BatchAssessment.LastOrDefault() != null ? a.BatchAssessment.LastOrDefault().Name : "",
        //                                                                                            Code = a.Code,
        //                                                                                            CutOffMark = a.CutOffMark != null ? a.CutOffMark.Value : 0,
        //                                                                                            Description = a.Description,
        //                                                                                            DisplayMarks = a.DisplayMarks != null ? a.DisplayMarks.Value : false,
        //                                                                                            DisplayResult = a.DisplayResult != null ? a.DisplayResult.Value : false,
        //                                                                                            EndDate = a.EndDateTime != null ? a.EndDateTime.Value : new DateTime(),
        //                                                                                            StartDate = a.StatDateTime != null ? a.StatDateTime.Value : new DateTime(),
        //                                                                                            Instructions = a.Instructions,
        //                                                                                            //Marks = a.UserAssessment.Where(ua => ua.UserID == userId).Select(u => u.Marks ?? 0).FirstOrDefault(),
        //                                                                                            Marks = a.UserAssessment.Where(ua => ua.UserID == userId).Select(ua => ua.QuestionPaperSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count()).FirstOrDefault(),

        //                                                                                            FinishTime = a.UserAssessment.Where(ua => ua.UserID == userId).Select(u => u.FinishTime).FirstOrDefault(),
        //                                                                                            NegativeMarks = a.NegativeMarks != null ? a.NegativeMarks.Value : 0,
        //                                                                                            IsNegativePercentage = a.IsNegativePercentage != null ? a.IsNegativePercentage.Value : false,
        //                                                                                            ReleaseKey = a.ReleaseKey != null ? a.ReleaseKey.Value : false,
        //                                                                                            TimeLimit = a.TimeLimit != null ? a.TimeLimit.Value : 0,
        //                                                                                            ShowInstructions = a.ShowInstruction ?? false,
        //                                                                                            ShowReport = a.ShowReport ?? false,
        //                                                                                            createdDate = a.CreatedDate,
        //                                                                                            CanJumpSections = a.CanJumpSections ?? false,
        //                                                                                            IsHaveRetestRequest = a.IsHaveRetestRequest ?? false,
        //                                                                                            CanShuffle = a.CanShuffle ?? false
        //                                                                                        }).ToList();
        //        questionPaperViewModelList = questionPaperViewModelList);
        //        studentViewModel.QuestionPaperViewModelList = questionPaperViewModelList;
        //        List<long> questionPaperIDList = new List<long>();
        //        questionPaperIDList = questionPapers.Select(x => x.ID).ToList();
        //        // return View(assessmentViewModelList);

        //        //Rank Show on Dashboard

        //        IQueryable<Rank> rank = userProfile.Rank.Where(y => y.UserID == userID).AsQueryable().OrderByDescending(x => x.ID);
        //        //IQueryable<Rank> rank = userProfile.Rank.Where(y => y.UserID == userID).AsQueryable().OrderByDescending(x => x.ID).Take(3);
        //        List<RankViewModel> rankViewmodelList = new List<RankViewModel>();

        //        if (rank.Count() != 0)
        //        {
        //            rankViewmodelList = (from q in rank
        //                                 select new RankViewModel
        //                                 {
        //                                     ID = q.ID,
        //                                     QuestionPaperID = q.QuestionPaperID ?? 0,
        //                                     QuestionPaperName = q.QuestionPaper.Name,
        //                                     DateString = q.QuestionPaper.UserAssessment.Where(ua => ua.UserID == userId).Select(ua => ua.FinishTime).FirstOrDefault().ToString(),
        //                                     Mark = q.QuestionPaper.UserAssessment.Where(ua => ua.UserID == userId).Select(ua => ua.QuestionPaperSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count()).FirstOrDefault(),
        //                                     UserID = q.UserID ?? 0,
        //                                     BatchRank = q.BatchRank ?? 0,
        //                                     DepartmentRank = q.DepartmentRank ?? 0,
        //                                     CollegeRank = q.CollegeRank ?? 0,
        //                                     NationalRank = q.NationalRank ?? 0,
        //                                     InternationalRank = q.InternationalRank ?? 0,
        //                                     LastUpdatedDateString = (q.LastUpdatedDate ?? DateTime.Now).ToString("dd-MM-yyyy hh:mm:ss"),
        //                                 }).ToList();
        //            studentViewModel.rankViewmodelList = rankViewmodelList;
        //        }
        //        else
        //        {
        //            IEnumerable<UserAssessment> UserAssessment = userProfile.UserAssessment.Where(ua => questionPaperIDList.Contains(ua.QuestionPaperID ?? 0)).AsEnumerable();
        //            IEnumerable<Batch> batch = userProfile.Student.BatchStudent.Select(x => x.Batch);
        //            List<int> batchIDList = new List<int>();
        //            batchIDList = batch.Select(x => x.ID).ToList();
        //            RankCalculate(questionPaperIDList, UserAssessment, batchIDList, userID);
        //            IQueryable<Rank> rank1 = userProfile.Rank.Where(y => y.UserID == userID).AsQueryable().OrderByDescending(x => x.ID);
        //            //IQueryable<Rank> rank1 = userProfile.Rank.Where(y => y.UserID == userID).AsQueryable().OrderByDescending(x => x.ID).Take(3);
        //            rankViewmodelList = (from q in rank1
        //                                 select new RankViewModel
        //                                 {
        //                                     ID = q.ID,
        //                                     QuestionPaperID = q.QuestionPaperID ?? 0,
        //                                     QuestionPaperName = q.QuestionPaper.Name,
        //                                     Date = q.QuestionPaper.UserAssessment.Where(ua => ua.UserID == userId).Select(ua => ua.FinishTime).FirstOrDefault(),
        //                                     DateString = q.QuestionPaper.UserAssessment.Where(ua => ua.UserID == userId).Select(ua => ua.FinishTime).FirstOrDefault().ToString(),
        //                                     UserID = q.UserID ?? 0,
        //                                     //Mark = q.QuestionPaper.UserAssessment.Select(ua => ua.Marks).FirstOrDefault(),
        //                                     Mark = q.QuestionPaper.UserAssessment.Where(ua => ua.UserID == userId).Select(ua => ua.QuestionPaperSnapshot.Where(ass => ((ass.Answer != null) ? ass.Answer.IsCorrect ?? false : false) && (ass.Question.QuestionType.Name.ToLower().Equals("single select"))).Count()).FirstOrDefault(),
        //                                     BatchRank = q.BatchRank ?? 0,
        //                                     DepartmentRank = q.DepartmentRank ?? 0,
        //                                     CollegeRank = q.CollegeRank ?? 0,
        //                                     NationalRank = q.NationalRank ?? 0,
        //                                     InternationalRank = q.InternationalRank ?? 0,
        //                                     LastUpdatedDateString = (q.LastUpdatedDate ?? DateTime.Now).ToString(),
        //                                     LastUpdatedDate = (q.LastUpdatedDate ?? DateTime.Now),
        //                                 }).ToList();
        //            studentViewModel.rankViewmodelList = rankViewmodelList;
        //        }

        //        rankViewmodelList = rankViewmodelList.OrderByDescending(r => r.Date).GroupBy(r => r.QuestionPaperName).Select(r => r.FirstOrDefault()).ToList();

        //        int totalRecords = rankViewmodelList.Count();
        //        int filteredCount = totalRecords;
        //        if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
        //        {
        //            rankViewmodelList = (from c in rankViewmodelList where c.QuestionPaperName.ToLower().Contains(datatableRequest.SearchString.ToLower()) || c.QuestionPaperName.ToLower().Contains(datatableRequest.SearchString.ToLower()) select c).ToList();
        //            filteredCount = rankViewmodelList.Count();
        //        }
        //        rankViewmodelList = rankViewmodelList.OrderBy(x => x.QuestionPaperName).ToList();
        //        if (datatableRequest.ShouldOrder)
        //        {
        //            rankViewmodelList = rankViewmodelList.AsQueryable().OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending).ToList();
        //        }
        //        rankViewmodelList = rankViewmodelList.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired).ToList();

        //        RankDataTableViewModel dataTableViewModel = new RankDataTableViewModel(rankViewmodelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

        //        string result = JsonConvert.SerializeObject(dataTableViewModel);
        //        Response.Write(result);
        //    }
        //    public ActionResult StudentRankView()
        //    {
        //        StudentViewModel sm = new StudentViewModel();
        //        List<RankViewModel> rmList = new List<RankViewModel>();
        //        UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
        //        int userID = userProfile.UserId;
        //        IQueryable<Rank> rank = userProfile.Rank.Where(y => y.UserID == userID).AsQueryable().OrderByDescending(x => x.ID);
        //        if (rank.Count() != 0)
        //        {
        //            List<RankViewModel> rankViewmodelList = (from q in rank
        //                                                     select new RankViewModel
        //                                                     {
        //                                                         ID = q.ID,
        //                                                         QuestionPaperID = q.QuestionPaperID ?? 0,
        //                                                         QuestionPaperName = q.QuestionPaper.Name,
        //                                                         UserID = q.UserID ?? 0,
        //                                                         BatchRank = q.BatchRank ?? 0,
        //                                                         DepartmentRank = q.DepartmentRank ?? 0,
        //                                                         CollegeRank = q.CollegeRank ?? 0,
        //                                                         NationalRank = q.NationalRank ?? 0,
        //                                                         InternationalRank = q.InternationalRank ?? 0,
        //                                                         LastUpdatedDate = q.LastUpdatedDate ?? DateTime.Now,
        //                                                     }).ToList();
        //            rmList = rankViewmodelList;
        //        }
        //        sm.rankViewmodelList = rmList;
        //        return View(sm);
        //    }
        //    #region LoadMiniStatistics For Admin

        public void LoadMiniStatisticsForAdmin()
        {
            MiniStatisticsModel miniStatisticsModel = new MiniStatisticsModel();

            webpages_Roles userRoles = db.webpages_Roles.Where(wpr => wpr.RoleName.ToLower() == "student").FirstOrDefault();
            int totalStudentsCount = db.UserProfile.Where(up => up.webpages_Roles.Where(wpr => wpr.RoleId == userRoles.RoleId).Count() > 0).Count();
            miniStatisticsModel.TotalStudents = totalStudentsCount;

            //Total questionpapers
            int createdTestsCount = db.UserAssessment.Count();
            miniStatisticsModel.CreatedTests = createdTestsCount;

            //Active users who are not logged out on today
            CultureInfo enZa = new CultureInfo("en-ZA");
            //DateTime today = DateTime.Parse(DateTime.Now.ToString(), enZa);
            DateTime today = DateTime.Now;

            int currentlyLoggedInUserCount = db.UserLogInLog.Where(ull => !ull.IsLoggedOut && !ull.UserProfile.UserName.Contains(User.Identity.Name)).ToList().Where(x => (DateTime.Compare(x.LogInTime.Date, today.Date)) == 0).Select(x => x.UserProfile).Distinct().Count();
            miniStatisticsModel.ActiveUsers = currentlyLoggedInUserCount;

            //Active test
            int activeTestsCount = db.UserAssessment.Where(ass => (ass.StartTime == null ? false : (ass.StartTime.Value <= today)) && (ass.FinishTime == null ? false : (ass.FinishTime.Value >= today))).Count();
            miniStatisticsModel.ActiveTests = activeTestsCount;

            //Today completed Tests
            DateTime dateToCompare = today;
            int completedTestsToday = db.UserAssessment.ToList().Where(ass => (ass.FinishTime == null ? false : (DateTime.Compare(ass.FinishTime.Value.Date, dateToCompare.Date) == 0))).Count();
            miniStatisticsModel.CompletedTestsToday = completedTestsToday;

            //Completed tests for the last 7 days
            dateToCompare = today.AddDays(-7);
            int completedTestsLastSeven = db.UserAssessment.Where(ass => (ass.FinishTime == null ? false : (ass.FinishTime.Value >= dateToCompare && ass.FinishTime.Value < DateTime.Now))).Count();
            miniStatisticsModel.CompletedTestsLastSeven = completedTestsLastSeven;

            //Completed tests Last 30
            dateToCompare = today.AddDays(-30);
            int completedTestsLastThirty = db.UserAssessment.Where(ass => (ass.FinishTime == null ? false : (ass.FinishTime.Value >= dateToCompare && ass.FinishTime.Value < DateTime.Now))).Count();
            miniStatisticsModel.CompletedTestsLastThirty = completedTestsLastThirty;

            //Total completed tests
            int CompletedTestsAll = db.UserAssessment.Where(ass => (ass.FinishTime == null ? false : (ass.FinishTime.Value < DateTime.Now))).Count();
            miniStatisticsModel.CompletedTestsAll = CompletedTestsAll;

            Response.Write(JsonConvert.SerializeObject(miniStatisticsModel));
        }

        //    #endregion

        //    #region Not Used

        //    public ActionResult AdminDashboard()
        //    {
        //        ViewBag.Message = "Modify this template to jump-start your ASP.NET MVC application.";
        //        webpages_Roles webPageRole = db.webpages_Roles.Where(wpr => wpr.RoleName.ToLower() == "student").FirstOrDefault();
        //        UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower() && up.webpages_Roles.Where(wpr => wpr.RoleName == webPageRole.RoleName).Count() > 0).FirstOrDefault();
        //        if (Request.IsAuthenticated && User.IsInRole("Student"))
        //        {
        //            List<QuestionPaperMinimalViewModel> availableQuestionPapers = new List<QuestionPaperMinimalViewModel>();
        //            //UserProfile userProfile = db.UserProfile.First(u => u.UserName.Equals(User.Identity.Name));
        //            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
        //            var questionpapers = userProfile.Group.GroupQuestionPaper.Where(ga => (!ga.StartDate.HasValue || (ga.StartDate.HasValue && ga.StartDate.Value <= indianTime)) && (!ga.StartDate.HasValue || (ga.EndDate.HasValue && ga.EndDate.Value >= indianTime))).Select(ga => new QuestionPaperMinimalViewModel() { QuestionPaperName = ga.QuestionPaper.Name, QuestionPaperID = ga.QuestionPaper.ID }).ToList();
        //            var assessmentIdList = questionpapers.Select(a => a.QuestionPaperID).ToList();
        //            var completedList = db.UserAssessment.Where(ua => assessmentIdList.Contains(ua.QuestionPaper.ID) && ua.FinishTime.HasValue && ua.UserProfile.UserId == userProfile.UserId).Select(a => a.QuestionPaper.ID).ToList();
        //            foreach (int completedQuestionPaperId in completedList)
        //            {
        //                QuestionPaperMinimalViewModel assessment = questionpapers.FirstOrDefault(a => a.QuestionPaperID == completedQuestionPaperId);
        //                if (assessment != null)
        //                {
        //                    questionpapers.Remove(assessment);
        //                }
        //            }

        //            return View(questionpapers);
        //        }
        //        return View(new List<QuestionPaperMinimalViewModel>());
        //    }

        //    public ActionResult StudentDashBoard()
        //    {
        //        UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
        //        if (User.IsInRole("Student"))
        //        {
        //            StudentViewModel studentViewModel = new StudentViewModel();
        //            int userId = GetCurrentUserID();
        //            // UserProfile userProfile = db.UserProfile.Find(userId);
        //            studentViewModel.ScheduledTestCount = userProfile.Student.BatchStudent.SelectMany(x => x.Batch.BatchAssessment).Select(x => x.QuestionPaper).Distinct().Count();
        //            studentViewModel.TakenTestCount = userProfile.UserAssessment.Count();
        //            studentViewModel.RemainingTestCount = studentViewModel.ScheduledTestCount - studentViewModel.TakenTestCount;
        //            IEnumerable<QuestionPaper> questionpapers = userProfile.UserAssessment.Select(ua => ua.QuestionPaper).Distinct();
        //            List<OAP.Domain.ViewModel.QuestionPaperViewModel> assessmentViewModelList = MapperHelper.MapToViewModelList(questionpapers);
        //            studentViewModel.QuestionPaperViewModelList = assessmentViewModelList;
        //            // return View(assessmentViewModelList);
        //            return View(studentViewModel);

        //        }
        //        else
        //        {
        //            if (User.Identity.IsAuthenticated)
        //                return RedirectToAction("LogOff", "Account");
        //            else
        //                return RedirectToAction("Login", "Account");
        //        }

        //    }

        //    #endregion

        //    public void RankList(string selectedColleges, string selectedBatches, string selectedBranches, string selectedQuestionPapers)
        //    {
        //        datatableRequest = new DataTableRequest(Request, columns);
        //        List<RankViewModel> rankViewModelList = new List<RankViewModel>();
        //        IQueryable<UserProfile> userProfileList = db.UserProfile.Where(u => u.StudentID != null).AsQueryable();
        //        if (selectedColleges != "")
        //        {
        //            List<long> collegeIDList = selectedColleges.Split(',').Select(long.Parse).ToList();
        //            if (collegeIDList.Count() > 0)
        //            {
        //                userProfileList = userProfileList.Where(u => collegeIDList.Contains(u.Student.CollegeID ?? 0)).AsQueryable();
        //            }
        //            if (selectedBatches != "")
        //            {
        //                List<long> BatchIDList = selectedBatches.Split(',').Select(long.Parse).ToList();

        //                if (BatchIDList.Count() > 0)
        //                {
        //                    userProfileList = userProfileList.Where(u => u.Student.BatchStudent.Where(b => BatchIDList.Contains(b.BatchID)).Count() > 0).AsQueryable();
        //                }

        //                if (selectedQuestionPapers != "")
        //                {
        //                    List<long> QuestionPaperIDList = selectedQuestionPapers.Split(',').Select(long.Parse).ToList();

        //                    if (QuestionPaperIDList.Count() > 0)
        //                    {
        //                        userProfileList = userProfileList.Where(u => u.UserAssessment.Where(ua => QuestionPaperIDList.Contains(ua.QuestionPaperID ?? 0)).Count() > 0).AsQueryable();
        //                    }
        //                }
        //            }
        //            else if (selectedBranches != "")
        //            {
        //                List<long> BranchIDList = selectedBranches.Split(',').Select(long.Parse).ToList();

        //                if (BranchIDList.Count() > 0)
        //                {
        //                    userProfileList = userProfileList.Where(u => BranchIDList.Contains(u.Student.BranchID ?? 0)).AsQueryable();
        //                }
        //            }
        //        }
        //        if (userProfileList.Count() > 0)
        //        {
        //            int userId = GetCurrentUserID();
        //            List<long> colleges = db.College.Where(c => c.UserProfileCollege.Where(upc => upc.UserProfileID == userId).Count() > 0).Select(c => c.ID).ToList();
        //            if (colleges.Count() > 0)
        //            {
        //                userProfileList = userProfileList.Where(u => colleges.Contains(u.Student.CollegeID ?? 0)).AsQueryable();
        //            }
        //            List<int> userIDList = userProfileList.Select(u => u.UserId).ToList();
        //            IQueryable<UserAssessment> UserAssessmentList = db.UserAssessment.Where(u => userIDList.Contains(u.UserID ?? 0)).AsQueryable();
        //            UserAssessmentList = UserAssessmentList.OrderBy(u => u.Marks);
        //            int i = 1;
        //            foreach (UserAssessment UserAssessment in UserAssessmentList)
        //            {
        //                RankViewModel rankViewModel = new RankViewModel();
        //                rankViewModel.StudentName = UserAssessment.UserProfile.Student.Name;
        //                rankViewModel.RegisterNumber = UserAssessment.UserProfile.UserName;
        //                rankViewModel.College = UserAssessment.UserProfile.Student.College.Name;
        //                rankViewModel.OverallRank = i;
        //                i++;
        //                rankViewModelList.Add(rankViewModel);
        //            }
        //        }
        //        int totalRecords = rankViewModelList.Count();
        //        int filteredCount = totalRecords;
        //        if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
        //        {
        //            rankViewModelList = (from c in rankViewModelList where c.RegisterNumber.ToLower().Contains(datatableRequest.SearchString.ToLower()) || c.StudentName.ToLower().Contains(datatableRequest.SearchString.ToLower()) select c).ToList();
        //            filteredCount = rankViewModelList.Count();
        //        }
        //        rankViewModelList = rankViewModelList.OrderBy(x => x.OverallRank).ToList();
        //        if (datatableRequest.ShouldOrder)
        //        {
        //            rankViewModelList = rankViewModelList.AsQueryable().OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending).ToList();
        //        }
        //        rankViewModelList = rankViewModelList.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired).ToList();

        //        RankDataTableViewModel dataTableViewModel = new RankDataTableViewModel(rankViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

        //        string result = JsonConvert.SerializeObject(dataTableViewModel);
        //        Response.Write(result);
        //    }

        public ActionResult TestList()
        {
            StudentViewModel studentViewModel = GetTestDetails();
            return View(studentViewModel);
        }

        public StudentViewModel GetTestDetails()
        {
            StudentViewModel studentViewModel = new StudentViewModel();
            int userId = WebSecurity.GetUserId(User.Identity.Name);
            UserProfile userProfile = db.UserProfile.Find(userId);
            Student student = userProfile.Student;

            List<OAP.Domain.ViewModel.BatchQuestionPaperViewModel> assessmentViewModelList = new List<OAP.Domain.ViewModel.BatchQuestionPaperViewModel>();

            DateTime indianTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
         
              //  IQueryable<BatchAssessment> questionpapers = db.BatchAssessment.Where(f => f.StartDateTime <= DateTime.Now || f.EndDateTime >= DateTime.Now).AsQueryable();

                IQueryable<BatchAssessment> questionpapers=userProfile.Student.BatchStudent.SelectMany(ba => ba.Batch.BatchAssessment).Where(ga => (ga.StartDateTime <= indianTime) && ga.EndDateTime >= indianTime).Distinct().AsQueryable();
                //IEnumerable<QuestionPaper> questionpapers = student.BatchStudent.SelectMany(ba => ba.Batch.BatchQuestionPaper).Where(ga => (ga.StartDateTime <= indianTime) && ga.EndDateTime >= indianTime).Select(ga => ga.QuestionPaper).Distinct().ToList();
                foreach (BatchAssessment batchAssessment in questionpapers)
                {
                    BatchQuestionPaperViewModel formViewModel = new BatchQuestionPaperViewModel();
                    //questionPaperViewModel.Name = db.BatchQuestionPaper.Where(u => u.BatchID == batchStudent.BatchID && u.QuestionPaperID == questionPaper.ID).FirstOrDefault() != null ? db.BatchQuestionPaper.Where(u => u.BatchID == batchStudent.BatchID && u.QuestionPaperID == questionPaper.ID).FirstOrDefault().Name : "";
                    formViewModel.Name = batchAssessment.Name;
                    formViewModel.ID = batchAssessment.AssessmentID;
                    bool isHaveRetestRequest = batchAssessment.QuestionPaper != null ? batchAssessment.QuestionPaper.IsHaveRetestRequest ?? false : false;
                    formViewModel.IsHaveRetestRequest = isHaveRetestRequest;
                    formViewModel.ShowReport = batchAssessment.QuestionPaper != null ? batchAssessment.QuestionPaper.ShowReport ?? false : false;
                    //formViewModel.CanReview = formQuestionPaper.CanReview ?? true;
                    formViewModel.StartDate = batchAssessment.StartDateTime;
                    formViewModel.QuestionPaperViewModel = MapperHelper.MapToViewModelForTestList(batchAssessment.QuestionPaper,batchAssessment);
                    if (batchAssessment.QuestionPaper.UserAssessment.Where(ua => ua.UserProfile.UserName.ToLower() == User.Identity.Name.ToLower() && ua.FinishTime != null).Count() > 0)
                    {
                        formViewModel.IsQuestionPaperTaken = true;
                    }
                    else
                    {
                        formViewModel.IsQuestionPaperTaken = false;
                    }

                    int availableBatchQuestionPaperCount = 0;
                    //if (assessment.BatchQuestionPaper.FirstOrDefault().StartDateTime <= DateTime.Now && assessment.BatchQuestionPaper.FirstOrDefault().EndDateTime >= DateTime.Now)
                    //int availableBatchQuestionPaperCount = batchQuestionPaper.Where(ba => ba.Batch.BatchStudent.Where(bs => bs.StudentID == userProfile.StudentID).Count() > 0 && ba.StartDateTime <= indianTime && ba.EndDateTime >= indianTime).Count();
                    if (batchAssessment.StartDateTime <= indianTime && batchAssessment.EndDateTime >= indianTime)
                    {
                        availableBatchQuestionPaperCount = db.BatchAssessment.Where(f => f.StartDateTime < DateTime.Now && f.EndDateTime > DateTime.Now).Count();
                    }
                    //if (assessment.BatchQuestionPaper.FirstOrDefault().StartDateTime <= indianTime && assessment.BatchQuestionPaper.FirstOrDefault().EndDateTime >= indianTime)
                    if (availableBatchQuestionPaperCount > 0)
                    {                                                                                               //No available questionpapers in this time
                        if (formViewModel.IsQuestionPaperTaken)
                        {
                            if (batchAssessment.QuestionPaper.RequestReTest.Where(ret => ret.UserProfile.UserId == userId).Count() <= 0)
                            {
                                formViewModel.Status = "QuestionPaper already taken. Click here to request for retest";
                                formViewModel.IsShowRetestRequest = true;
                                formViewModel.IsRetestRequestRejected = false;
                            }
                            else if (batchAssessment.QuestionPaper.RequestReTest.Where(ret => ret.UserProfile.UserId == userId && (ret.IsProcessed == null)).Count() > 0)
                            {                                                                                           //Retest request made . not processed.
                                formViewModel.Status = "Retest request made. Waiting for approval";
                                formViewModel.IsShowRetestRequest = false;
                                formViewModel.IsRetestRequestRejected = false;
                            }
                            else if (batchAssessment.QuestionPaper.RequestReTest.Where(ret => ret.UserProfile.UserId == userId && (ret.IsProcessed != null) && (ret.IsApproved != null && ret.IsApproved.Value == true)).Count() > 0)
                            {                                                                                       //Retest request made. Processed.
                                formViewModel.Status = "QuestionPaper already taken. Click here to request for retest";
                                formViewModel.IsShowRetestRequest = true;
                                formViewModel.IsRetestRequestRejected = false;
                            }
                            else if (batchAssessment.QuestionPaper.RequestReTest.Where(ret => ret.UserProfile.UserId == userId && ret.IsApproved == null && (ret.IsProcessed ?? false)).Count() > 0)
                            {
                                formViewModel.Status = "Retest request made. Waiting for approval";
                                formViewModel.IsShowRetestRequest = false;
                                formViewModel.IsRetestRequestRejected = false;
                            }
                            else if (batchAssessment.QuestionPaper.RequestReTest.Where(ret => ret.UserProfile.UserId == userId && (ret.IsApproved != null && ret.IsApproved.Value == false) && (ret.IsProcessed ?? false)).Count() > 0)
                            {
                                formViewModel.Status = "Request rejected. Please contact admin.";
                                formViewModel.IsShowRetestRequest = false;
                                formViewModel.IsRetestRequestRejected = true;
                                formViewModel.RejectReason = batchAssessment.QuestionPaper.RequestReTest.Where(ret => ret.UserProfile.UserId == userId).FirstOrDefault().Comments;
                            }
                        }
                        else
                        {
                            if (batchAssessment.QuestionPaper.UserAssessment.Where(ua => ua.UserProfile.UserName.ToLower() == User.Identity.Name.ToLower() && ua.FinishTime == null).Count() > 0)
                            {                                                                                                                       //Test started but not completed
                                formViewModel.Status = "Test started .. Incomplete";
                            }
                            else
                            {                                                                                                                       //Test completed
                                formViewModel.Status = "You can take the Test Now";
                            }
                        }
                    }
                    //else if (assessment.BatchQuestionPaper.FirstOrDefault().StartDateTime > DateTime.Now)
                    else if (batchAssessment.StartDateTime > indianTime)
                    {                                                           //Test not start 
                        if (formViewModel.IsQuestionPaperTaken)
                            formViewModel.Status = "QuestionPaper already taken. Click here to request for retest";
                        else if (availableBatchQuestionPaperCount <= 0)
                        {
                            formViewModel.IsQuestionPaperBatchDateNotInRange = true;
                            formViewModel.Status = "You can't take the test now.";
                        }
                    }
                    else if (batchAssessment.StartDateTime < indianTime && batchAssessment.EndDateTime < indianTime)
                    {                                                                           //Test ended
                        if (formViewModel.IsQuestionPaperTaken)
                            formViewModel.Status = "QuestionPaper already taken. Click here to request for retest";
                        else
                        {
                            formViewModel.IsQuestionPaperBatchDateNotInRange = true;
                            formViewModel.Status = "Your test has been ended.";
                        }
                    }
                    assessmentViewModelList.Add(formViewModel);
                }
                studentViewModel.batchAssessmentViewModelList=new List<BatchQuestionPaperViewModel>();
                studentViewModel.batchAssessmentViewModelList.AddRange(assessmentViewModelList);
                if (assessmentViewModelList != null)
                {
                    studentViewModel.AssessmentViewModelList = new List<Domain.ViewModel.QuestionPaperViewModel>();
                    studentViewModel.AssessmentViewModelList.AddRange(assessmentViewModelList.Select(a => a.QuestionPaperViewModel));
                }
            return studentViewModel;
        }

        public void CollegeList()
        {
            string[] columns = { "Name", "Code", "TotalStudents" };
            datatableRequest = new DataTableRequest(Request, columns);
            UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();

            IQueryable<College> colleges = db.College.Where(c => c.UserProfileCollege.Where(upc => upc.UserProfileID == userProfile.UserId).Count() > 0);
            int totalRecords = colleges.Count();
            int filteredCount = totalRecords;
            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                colleges = (from c in db.College where c.Code.Contains(datatableRequest.SearchString) || c.Name.Contains(datatableRequest.SearchString) select c);
                filteredCount = colleges.Count();
            }
            colleges = colleges.OrderBy(x => x.Name);
            if (datatableRequest.ShouldOrder)
            {
                colleges = colleges.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            }
            colleges = colleges.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            //List<CollegeViewModel> collegeViewModelList = MapperHelper.MapToViewModelList(colleges);
            List<CollegeViewModel> collegeViewModelList = (from c in colleges
                                                           select new CollegeViewModel()
                                                           {
                                                               Name = c.Name,
                                                               Code = c.Code,
                                                               ID = c.ID,
                                                               TotalStudents = c.Student.Count()
                                                           }).ToList();

            CollegeDataTableViewModel dataTableViewModel = new CollegeDataTableViewModel(collegeViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        #region SetGuageMeters

        //public void GetScoredInQuestionPapers()
        public void GetQuestionPaperScores()
        {
            UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            IEnumerable<QuestionPaper> questionpapers = userProfile.UserAssessment.Where(ua => ua.FinishTime != null && ua.UserID == userProfile.UserId).Select(ua => ua.QuestionPaper);

            List<StudentVersatileViewModel> studentVersatileViewModelList = new List<StudentVersatileViewModel>();
            List<decimal> averateList;
            foreach (QuestionPaper questionPaper in questionpapers)
            {
                averateList = new List<decimal>();
                //averateList = assessment.UserAssessment.Where(ua => ua.UserID == userProfile.UserId).Select(x => x.Marks != null ? x.Marks.Value : 0).Distinct().ToList();
                averateList = questionPaper.UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID).Select(x => x.Marks != null ? x.Marks.Value : 0).OrderBy(x => x).Distinct().ToList();

                //decimal averageMarks =(averateList!=null && averateList.Count()>0)? averateList.Average(x=>x):0;
                decimal scoredMarks = questionPaper.UserAssessment.Where(ua => ua.AssessmentID == questionPaper.ID && ua.UserID == userProfile.UserId).Select(x => x.Marks != null ? x.Marks.Value : 0).FirstOrDefault();

                float index = averateList.IndexOf(scoredMarks);

                float scoredPercentile = 0;
                if (averateList.Count() > 1)
                    scoredPercentile = (index / ((float)averateList.Count() - 1)) * 100;
                else
                    scoredPercentile = 100;

                StudentVersatileViewModel studentVersatileViewModel = new StudentVersatileViewModel();
                studentVersatileViewModel.QuestionPaperID = questionPaper.ID;
                //studentVersatileViewModel.AverageScored = averageMarks > scoredMarks ? averageMarks : scoredMarks;
                studentVersatileViewModel.AveragePercentile = 100;
                studentVersatileViewModel.ScoredPercentile = scoredPercentile;
                studentVersatileViewModelList.Add(studentVersatileViewModel);
                //studentVersatileViewModelList.Add(studentVersatileViewModel);
            }
            string result = JsonConvert.SerializeObject(studentVersatileViewModelList);
            Response.Write(result);
        }

        #endregion


        #region Program Details

        public void LoadProgramsForAdmin()
        {
            ProgramViewModel dashboard = new ProgramViewModel();
            //IEnumerable<InstituteProgram> institutePrograms = db.InstituteProgram;
            //List<ProgramViewModel> programDetails = (from p in institutePrograms
            //                                         select new ProgramViewModel()
            //                                       {
            //                                           CollegeID = p.InstituteID != null ? p.InstituteID.Value : 0,
            //                                           StartDate = p.StartDate != null ? p.StartDate.Value : GetDefaultDateTime(),
            //                                           EndDate = p.EndDate != null ? p.EndDate.Value : GetDefaultDateTime(),
            //                                           ProgramID = p.ProgramID != null ? p.ProgramID.Value : 0,
            //                                           collegeName = p.College != null ? p.College.Name : "",
            //                                           ProgramName = p.Program != null ? p.Program.Name : "",
            //                                       }).ToList();
            //dashboard.programDetails = sh.SortProgram(programDetails);
            IEnumerable<BatchAssessment> batchQuestionPaperList = db.BatchAssessment;
            List<BatchViewModel> batchQuestionPapers = (from b in batchQuestionPaperList
                                                        select new BatchViewModel()
                                                      {
                                                          collegeName = b.Batch != null ? b.Batch.College != null ? b.Batch.College.Name : "" : "",
                                                          StartDate = b.StartDateTime,
                                                          EndDate = b.EndDateTime,
                                                          // EndDate = b.EndDateTime,
                                                          BatchName = b.Batch != null ? b.Batch.Name : "",
                                                          BatchID = b.Batch != null ? b.Batch.ID : 0,
                                                          QuestionPaperID = b.ID,
                                                          QuestionPaperName = b.QuestionPaper != null ? b.QuestionPaper.Name : "",
                                                      }).ToList();
            dashboard.batchQuestionPapers = batchQuestionPapers;
            Response.Write(JsonConvert.SerializeObject(dashboard));
        }

        #endregion


        public void AutoCreatedQuestionPaper(long QuestionPaperTemplateID = 0, string QuestionPaperName = "", string Number = "")
        {
            int UserID = GetCurrentUserID();
            long result = 0;
            string compeleteQuestionPaperName = QuestionPaperName + "_" + Number;
            result = MapperHelper.CreateQuestionPaper(QuestionPaperTemplateID, compeleteQuestionPaperName, UserID);
            Response.Write(result);
        }

        //#region RankUpdate
        //public void UpdateRankDetails()
        //{
        //    UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
        //    int userID = userProfile.UserId;
        //    IEnumerable<QuestionPaper> questionPapers = userProfile.UserAssessment.Where(ua => ua.FinishTime != null).Select(ua => ua.QuestionPaper).Distinct();
        //    List<long> questionPaperIDList = new List<long>();
        //    questionPaperIDList = questionPapers.Select(x => x.ID).ToList();
        //    IEnumerable<UserAssessment> UserAssessment = userProfile.UserAssessment.Where(ua => questionPaperIDList.Contains(ua.QuestionPaperID ?? 0)).AsEnumerable();
        //    IEnumerable<Batch> batch = userProfile.Student.BatchStudent.Select(x => x.Batch);
        //    List<int> batchIDList = new List<int>();
        //    batchIDList = batch.Select(x => x.ID).ToList();
        //    RankCalculate(questionPaperIDList, UserAssessment, batchIDList, userID);

        //    Response.Write("True");
        //}

        //public void RankCalculate(List<long> questionPaperIDList, IEnumerable<UserAssessment> UserAssessment, List<int> batchIDList, int userID)
        //{
        //    IQueryable<BatchStudent> batchStudent;
        //    IQueryable<Student> student;
        //    IQueryable<UserProfile> userProfile;
        //    List<BatchAssessment> batchQuestionPaper;
        //    IQueryable<Batch> batch;
        //    //Batch wise rank
        //    foreach (int batchID in batchIDList)
        //    {
        //        batchStudent = db.BatchStudent.Where(x => x.BatchID == batchID);
        //        student = batchStudent.Select(x => x.Student);
        //        userProfile = student.SelectMany(x => x.UserProfile);
        //        batchQuestionPaper = db.BatchAssessment.Where(y => y.BatchID == batchID).ToList();
        //        List<long> QuestionPaperIDList = new List<long>();
        //        QuestionPaperIDList = batchQuestionPaper.Select(x => x.QuestionPaperID).ToList();
        //        foreach (long QuestionID in QuestionPaperIDList)
        //        {
        //            List<UserAssessment> userAssment = userProfile.SelectMany(x => x.UserAssessment).Where(y => y.QuestionPaperID == QuestionID).Distinct().OrderByDescending(z => z.Marks).ToList();
        //            int i = 1;
        //            foreach (UserAssessment uS in userAssment)
        //            {

        //                Rank rank = new Rank();
        //                rank = db.Rank.Where(x => x.QuestionPaperID == uS.QuestionPaperID && x.UserID == userID).FirstOrDefault();
        //                if (rank != null)
        //                {
        //                    //Rank ra = new Rank();
        //                    //ra.UserID = rank.UserID;
        //                    //rank.UserProfile = uS.UserProfile;
        //                    //ra.QuestionPaperID = rank.QuestionPaperID ?? 0;
        //                    rank.BatchRank = i;
        //                    rank.LastUpdatedDate = DateTime.Now;
        //                    //db.Rank.Add(ra);
        //                    db.Entry(rank).State = EntityState.Modified;
        //                }
        //                else
        //                {
        //                    Rank r = new Rank();
        //                    r.UserID = uS.UserID;
        //                    //rank.UserProfile = uS.UserProfile;
        //                    r.QuestionPaperID = uS.QuestionPaperID ?? 0;
        //                    r.BatchRank = i;
        //                    r.LastUpdatedDate = DateTime.Now;
        //                    db.Rank.Add(r);
        //                }
        //                i++;
        //            }
        //            db.SaveChanges();
        //        }
        //    }

        //    //College wise Rank

        //    /*  student = db.UserProfile.Where(x => x.UserId == userID).Select(y => y.Student);


        //      int bID = batchIDList[0];
        //      batch = db.Batch.Where(x=>x.ID==bID);
        //      Batch b = db.Batch.Where(x=>x.ID==bID).FirstOrDefault();
        //      IQueryable<Batch> batchforCollege = db.Batch.Where(x => x.CollegeID == b.CollegeID);

        //      IQueryable<BatchAssessment> bQuestionPaper = batchforCollege.SelectMany(x => x.BatchAssessment).AsQueryable();
        //      batchStudent = db.BatchStudent.Where(x => x.Batch == batch).AsQueryable();
        //      student = batchStudent.Select(x => x.Student);

        //      userProfile = student.SelectMany(x => x.UserProfile.Where(y => y.UserId == userID)).AsQueryable();*/

        //    //long collegeID = userProfile.Select(x => x.Student.CollegeID??0);
        //    //userPro

        //    //National Level Rank
        //    for (int i = 0; i < questionPaperIDList.Count(); i++)
        //    {
        //        long Qid = questionPaperIDList[i];
        //        IEnumerable<UserAssessment> uQuestionPaper = db.UserAssessment.Where(x => x.QuestionPaperID == Qid).Distinct().OrderByDescending(z => z.Marks).AsEnumerable();
        //        int j = 1;
        //        foreach (UserAssessment uS in uQuestionPaper)
        //        {
        //            Rank rank = new Rank();
        //            rank = db.Rank.Where(x => x.QuestionPaperID == uS.QuestionPaperID && x.UserID == userID).FirstOrDefault();
        //            if (rank != null)
        //            {

        //                rank.NationalRank = j;
        //                rank.LastUpdatedDate = DateTime.Now;
        //                db.Entry(rank).State = EntityState.Modified;
        //                //db.Rank.Add(rank);
        //            }
        //            else
        //            {
        //                Rank r = new Rank();
        //                //if (rank.BatchRank == null)
        //                //{
        //                //    r.BatchRank = 0;
        //                //}
        //                //else
        //                //{
        //                //    r.BatchRank = rank.BatchRank;
        //                //}                       
        //                r.UserID = uS.UserID;
        //                //rank.UserProfile = uS.UserProfile;
        //                r.QuestionPaperID = uS.QuestionPaperID ?? 0;
        //                r.NationalRank = j;
        //                r.BatchRank = 0;
        //                r.LastUpdatedDate = DateTime.Now;
        //                db.Rank.Add(r);
        //            }
        //            j++;
        //        }
        //        db.SaveChanges();
        //    }
        //    //national Level Rank end

        //}
        //#endregion

        #region RankUpdate
        public void UpdateRankDetails()
        {
            UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            int userID = userProfile.UserId;
            IEnumerable<QuestionPaper> questionPapers = userProfile.UserAssessment.Where(ua => ua.FinishTime != null).Select(ua => ua.QuestionPaper).Distinct();
            List<long> questionPaperIDList = new List<long>();
            questionPaperIDList = questionPapers.Select(x => x.ID).ToList();
            IEnumerable<UserAssessment> UserAssessment = userProfile.UserAssessment.Where(ua => questionPaperIDList.Contains(ua.AssessmentID ?? 0)).AsEnumerable();
            IEnumerable<Batch> batch = userProfile.Student.BatchStudent.Select(x => x.Batch);
            List<int> batchIDList = new List<int>();
            batchIDList = batch.Select(x => x.ID).ToList();
            RankCalculate(questionPaperIDList, UserAssessment, batchIDList, userID);
            Response.Write("True");
        }

        public void RankCalculate(List<long> questionPaperIDList, IEnumerable<UserAssessment> UserAssessment, List<int> batchIDList, int userID)
        {
            IQueryable<BatchStudent> batchStudent;
            IQueryable<Student> student;
            IQueryable<UserProfile> userProfile;
            List<BatchAssessment> batchQuestionPaper;
            IQueryable<Batch> batch;
            //Batch wise rank
            foreach (int batchID in batchIDList)
            {
                batchStudent = db.BatchStudent.Where(x => x.BatchID == batchID);
                student = batchStudent.Select(x => x.Student);
                userProfile = student.SelectMany(x => x.UserProfile);
                batchQuestionPaper = db.BatchAssessment.Where(y => y.BatchID == batchID).ToList();
                List<long> QuestionPaperIDList = new List<long>();
                QuestionPaperIDList = batchQuestionPaper.Select(x => x.AssessmentID).ToList();
                foreach (long QuestionID in QuestionPaperIDList)
                {
                    List<UserAssessment> userAssment = userProfile.SelectMany(x => x.UserAssessment).Where(y => y.AssessmentID == QuestionID).OrderByDescending(z => z.Marks).ToList();

                    int i = 1;
                    foreach (UserAssessment uS in userAssment)
                    {

                        Rank rank = new Rank();
                        rank = db.Rank.Where(x => x.AssessmentID == uS.AssessmentID && x.UserID == userID).FirstOrDefault();
                        if (rank != null)
                        {
                            rank.BatchRank = i;
                            rank.LastUpdatedDate = DateTime.Now;
                            //db.Rank.Add(ra);
                            db.Entry(rank).State = EntityState.Modified;
                        }
                        else
                        {
                            Rank r = new Rank();
                            r.UserID = uS.UserID;
                            r.AssessmentID = uS.AssessmentID ?? 0;
                            r.BatchRank = i;
                            r.CollegeRank = 0;
                            r.DepartmentRank = 0;
                            r.NationalRank = 0;
                            r.LastUpdatedDate = DateTime.Now;
                            db.Rank.Add(r);
                        }
                        i++;
                    }
                    db.SaveChanges();
                }
            }


            //Department wise rank

            int bID1 = batchIDList[0];
            //batch = db.Batch.Where(x=>x.ID==bID);
            Batch b1 = db.Batch.Where(x => x.ID == bID1).FirstOrDefault();
            IQueryable<Batch> batchforCollege1 = db.Batch.Where(x => x.CollegeID == b1.CollegeID);

            batchStudent = batchforCollege1.SelectMany(x => x.BatchStudent);

            IQueryable<Branch> branch = batchStudent.Select(x => x.Student).Select(s => s.Branch).Distinct();

            if (branch.Count() != 0 && branch != null)
            {

                student = batchStudent.Select(x => x.Student).GroupBy(x => x.BranchID).Select(x => x.FirstOrDefault());
                foreach (Student br in student.ToList())
                {
                    IQueryable<UserProfile> BranchUserProfile = batchStudent.Select(x => x.Student).Where(y => y.BranchID == br.BranchID).SelectMany(z => z.UserProfile).AsQueryable();
                    IQueryable<UserAssessment> BranchuserQuestionPaper = BranchUserProfile.SelectMany(s1 => s1.UserAssessment).AsQueryable();
                    for (int j = 0; j < questionPaperIDList.Count(); j++)
                    {
                        long id = Convert.ToInt64(questionPaperIDList[j]);
                        IEnumerable<UserAssessment> uQuestionPaper = BranchuserQuestionPaper.Where(x => x.AssessmentID == id).OrderByDescending(z => z.Marks).AsEnumerable();
                        int k = 1;
                        if (uQuestionPaper.Count() != 0)
                        {
                            foreach (UserAssessment uS in uQuestionPaper.ToList())
                            {
                                Rank rank = new Rank();
                                rank = db.Rank.Where(x => x.AssessmentID == uS.AssessmentID && x.UserID == userID).FirstOrDefault();
                                if (rank != null)
                                {
                                    rank.DepartmentRank = k;
                                    rank.LastUpdatedDate = DateTime.Now;
                                    db.Entry(rank).State = EntityState.Modified;
                                    //db.SaveChanges();
                                    //db.Rank.Add(rank);
                                }
                                k++;
                            }
                        }
                    }
                }
            }

            //IQueryable<UserProfile> usment = student.SelectMany(x => x.UserProfile);

            //Department wise Rank end


            //College wise Rank

            student = db.UserProfile.Where(x => x.UserId == userID).Select(y => y.Student);


            int bID = batchIDList[0];
            //batch = db.Batch.Where(x=>x.ID==bID);
            Batch b = db.Batch.Where(x => x.ID == bID).FirstOrDefault();
            IQueryable<Batch> batchforCollege = db.Batch.Where(x => x.CollegeID == b.CollegeID);

            IQueryable<BatchAssessment> bQuestionPaper = batchforCollege.SelectMany(x => x.BatchAssessment).AsQueryable();
            IQueryable<UserProfile> StudentUserProfile = batchforCollege.SelectMany(bc => bc.BatchStudent.SelectMany(s => s.Student.UserProfile)).AsQueryable();
            IQueryable<UserAssessment> StudentuserQuestionPaper = StudentUserProfile.SelectMany(s => s.UserAssessment).AsQueryable();

            foreach (BatchAssessment batchIDs in bQuestionPaper.ToList())
            {
                //long Qid = questionPaperIDList[i];
                IEnumerable<UserAssessment> uQuestionPaper = StudentuserQuestionPaper.Where(x => x.AssessmentID == batchIDs.AssessmentID).OrderByDescending(z => z.Marks).AsEnumerable();
                int j = 1;
                foreach (UserAssessment uS in uQuestionPaper.ToList())
                {
                    Rank rank = new Rank();
                    rank = db.Rank.Where(x => x.AssessmentID == uS.AssessmentID && x.UserID == userID).FirstOrDefault();
                    if (rank != null)
                    {
                        rank.CollegeRank = j;
                        rank.LastUpdatedDate = DateTime.Now;
                        db.Entry(rank).State = EntityState.Modified;
                        //db.SaveChanges();
                        //db.Rank.Add(rank);
                    }
                    j++;
                }
                db.SaveChanges();
            }
            //College wise Rank end

            //National Level Rank
            for (int i = 0; i < questionPaperIDList.Count(); i++)
            {
                long Qid = questionPaperIDList[i];
                IEnumerable<UserAssessment> uQuestionPaper = db.UserAssessment.Where(x => x.AssessmentID == Qid).OrderByDescending(z => z.Marks).AsEnumerable();
                int j = 1;
                foreach (UserAssessment uS in uQuestionPaper)
                {
                    Rank rank = new Rank();
                    rank = db.Rank.Where(x => x.AssessmentID == uS.AssessmentID && x.UserID == userID).FirstOrDefault();
                    if (rank != null)
                    {
                        rank.NationalRank = j;
                        rank.LastUpdatedDate = DateTime.Now;
                        db.Entry(rank).State = EntityState.Modified;
                        //db.Rank.Add(rank);
                    }
                    //else
                    //{

                    //Rank r = new Rank();
                    ////if (rank.BatchRank == null)
                    ////{
                    ////    r.BatchRank = 0;
                    ////}
                    ////else
                    ////{
                    ////    r.BatchRank = rank.BatchRank;
                    ////}                       
                    //r.UserID = uS.UserID;
                    ////rank.UserProfile = uS.UserProfile;
                    //r.QuestionPaperID = uS.QuestionPaperID ?? 0;
                    //r.NationalRank = j;
                    //r.BatchRank = 0;
                    //r.LastUpdatedDate = DateTime.Now;
                    //db.Rank.Add(r);
                    // }
                    j++;
                }
                db.SaveChanges();
            }
            //national Level Rank end

        }
        #endregion


    }


    public class StudentVersatileViewModel
    {

        public long QuestionPaperID { get; set; }

        public int AveragePercentile { get; set; }

        public float ScoredPercentile { get; set; }
    }

    public class ProgramDetails
    {
        public long ProgramID { get; set; }

        public long CollegeID { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }



        public int StartDateDay
        {
            get
            {
                return StartDate.Day;
            }
        }
        public int StartDateMonth
        {
            get
            {
                return StartDate.Month;
            }
        }
        public int StartDateYear
        {
            get
            {
                return StartDate.Year;
            }
        }
        public int EndDateDay
        {
            get
            {
                return EndDate.Day;
            }
        }
        public int EndDateMonth
        {
            get
            {
                return EndDate.Month;
            }
        }
        public int EndDateYear
        {
            get
            {
                return EndDate.Year;
            }
        }

        public string ProgramName { get; set; }

        public string collegeName { get; set; }
    }
}
