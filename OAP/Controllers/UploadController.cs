﻿using OAP.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Runtime.InteropServices;
using DocumentFormat.OpenXml.Packaging;
//using NetOffice.OfficeApi;
using System.Text;
using System.Collections;
//using Microsoft.Office.Interop.Word;
using System.Xml.Linq;
using System.Net;
using System.Text.RegularExpressions;
using OAP.Domain;
using HelperClasses;
using System.Xml;
using System.Data.Entity;
using OAP.Domain.ViewModel.DataTableViewModel;
using Newtonsoft.Json;
using System.Web.Routing;
using OfficeOpenXml;
using System.Web.Script.Serialization;
using OAP;
using System.Data;

namespace OAP.Controllers
{
    public class UploadController : BaseController
    {
        //
        // GET: /UploadDetail/
        OAPEntities db = new OAPEntities();
        HtmlFormatter htmlConverter = new HtmlFormatter();

        // protected Microsoft.Office.Interop.Word.ApplicationClass objWord = new ApplicationClass();

        #region Index

        public ActionResult Index()
        {
            return View();
        }

        string[] columns = { "Title", "UploadedDate" };

        public void List()
        {
            datatableRequest = new DataTableRequest(Request, columns);

            //IQueryable<Uploads> uploads = db.Uploads;
            IQueryable<Uploads> uploads = (from u in db.Uploads
                                           group u by new
                                           {
                                               u.Title
                                           } into up
                                           select up.FirstOrDefault());

            int totalRecords = uploads.Count();
            int filteredCount = totalRecords;
            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                uploads = (from u in db.Uploads where u.Title.Contains(datatableRequest.SearchString) select u);
                filteredCount = uploads.Count();
            }
            uploads = uploads.OrderBy(x => x.Title);
            if (datatableRequest.ShouldOrder)
            {
                uploads = uploads.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            }
            uploads = uploads.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            List<UploadQuestionViewModel> uploadQuestionViewModelList = MapperHelper.MapToViewModelList(uploads);

            UploadQuestionDataTableViewModel dataTableViewModel = new UploadQuestionDataTableViewModel(uploadQuestionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        #endregion

        #region IndexTemp

        public ActionResult IndexTemp()
        {

            return View();
        }
        //string[] columns = { "Title", "UploadedDate" };
//        public void TempList()
//        {

//            datatableRequest = new DataTableRequest(Request, columns);

//            //IQueryable<Uploads> uploads = db.Uploads;
//            IQueryable<TempUploads> tempUploads = (from u in db.TempUploads
//                                                   group u by new
//                                                   {
//                                                       u.Title
//                                                   } into up
//                                                   select up.FirstOrDefault());

//            int totalRecords = tempUploads.Count();
//            int filteredCount = totalRecords;
//            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
//            {
//                tempUploads = (from u in db.TempUploads where u.Title.Contains(datatableRequest.SearchString) select u);
//                filteredCount = tempUploads.Count();
//            }
//            tempUploads = tempUploads.OrderBy(x => x.Title);
//            List<TempUploads> tempUploadsList = tempUploads.ToList();

//            //anu Sorting based on Date
//            if (datatableRequest.OrderByColumn == "UploadedDate")
//            {
//                //  tempUploads
//                // tempUploads = tempUploads.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending); 
                
//                if (datatableRequest.IsOrderAsccending == true)
//                    {
//                        tempUploadsList = tempUploadsList.OrderBy(p => p.UploadedDate).ToList();
//                    }
//                    else
//                    {
//                        tempUploadsList = tempUploadsList.OrderByDescending(p => p.UploadedDate).ToList();
//                    }
//                }
//            else
//            {
//                // anu for getting alphanumeric values
//                using (NaturalComparer comparer = new NaturalComparer())
//                {
//                    tempUploadsList.Sort(comparer);
//                }
//                //end by anu 
//            }
//            //  tempUploads
//            // tempUploads = tempUploads.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending); 
//            //else
//            //{
//            //    if (datatableRequest.IsOrderAsccending == true)
//             //   {
//             //       tempUploadsList = tempUploadsList.OrderBy(p => p.UploadedDate).ToList();
//             //   }
//             //   else
//             //   {
//             //       tempUploadsList = tempUploadsList.OrderByDescending(p => p.UploadedDate).ToList();
//             //   }
            

////}


//            tempUploads = tempUploadsList.AsQueryable().Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
//            List<UploadQuestionViewModel> uploadQuestionViewModelList = MapperHelper.MapToViewModelList(tempUploads);

//            UploadQuestionDataTableViewModel dataTableViewModel = new UploadQuestionDataTableViewModel(uploadQuestionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

//            string result = JsonConvert.SerializeObject(dataTableViewModel);
//            Response.Write(result);
//        }
        #endregion

        #region Upload Questions

        public ActionResult UploadQuestions()
        {
            StudentViewModel studentViewModel = new StudentViewModel();
            int uploadCount = db.Uploads.Count() + 1;
            studentViewModel.UploadName = "Upload " + uploadCount;
            return View(studentViewModel);
        }

        [HttpPost]
        public ActionResult UploadQuestions(HttpPostedFileBase file, StudentViewModel studentViewModel)
        {
            List<HtmlEntities> htmlEntities = GetHtmlEntities();
            string[] notSupportedTags = new string[] { "&rsquo;", " &ldquo;", "&rdquo;", "&lsquo;", " &ndash;", "  &bull;", " &frac;", " &deg;", "&frasl;", "&pi;", " &ang;", " &times;" };
            MashUploadResultViewModel uploadResult = new MashUploadResultViewModel();
            Uploads upload = new Uploads();
            upload.Title = studentViewModel.UploadName;
            upload.UploadedDate = DateTime.Now;
            upload.UploadedUserID = GetCurrentUserID();
            db.Uploads.Add(upload);
            db.SaveChanges();
            List<PassageViewModel> passageViewModelList = new List<PassageViewModel>();
            if (file != null)
            {
                string guidStirng = Guid.NewGuid().ToString();
                //string newPath = Server.MapPath("~/Upload/" + guidStirng + "/" + guidStirng + ".html");
                if (!Directory.Exists(Server.MapPath("~/Upload/" + guidStirng)))
                    Directory.CreateDirectory(Server.MapPath("~/Upload/" + guidStirng));
                //if (!Directory.Exists(Server.MapPath("~/Upload/" + guidStirng + "/Images")))
                //    Directory.CreateDirectory(Server.MapPath("~/Upload/" + guidStirng + "/Images"));
                string path = Server.MapPath("~/Upload/" + guidStirng + "/" + guidStirng + "." + file.FileName.Split('.')[1]);
                string htmlPathTest = Server.MapPath("~/Upload/" + guidStirng + "/" + guidStirng + "_Test.html");
                file.SaveAs(htmlPathTest);
                try
                {
                    file.SaveAs(path);
                    HelperClasses.Converter converter = new Converter();
                    converter.ConvertDocxToHtml(path);
                }
                finally
                {
                    //objWord.Application.Quit(SaveChanges: false);
                }
               
                string fileContent = "";
                string htmlPath = Server.MapPath("~/Upload/" + guidStirng) + "/" + guidStirng + ".html";
                fileContent = htmlConverter.ConvertHtmlToString(htmlPath);

                for (int i = 0; i < htmlEntities.Count(); i++)
                {
                    fileContent = fileContent.Replace(htmlEntities[i].HtmlCode, htmlEntities[i].XmlCode);
                }

                StringBuilder sb = new StringBuilder();
                if (!string.IsNullOrEmpty(fileContent))
                {
                    fileContent = htmlConverter.FormatFileContent(fileContent);
                }

                string oldPath = Server.MapPath("~/Upload/" + guidStirng);
                string questionFilePath = Server.MapPath("~/Upload") + "/" + guidStirng + "/" + guidStirng + "." + file.FileName.Split('.')[1];

                //string azureImageNewPath = AzureHelper.UploadToAzureStorage(questionFilePath, "OAP/Upload/Questions", false);

                //passageViewModelList = CreatePassageQuestionsAnswers(fileContent, guidStirng, oldPath, azureImageNewPath);
                uploadResult = CreatePassageQuestionsAnswers(fileContent, guidStirng, oldPath, upload.ID, questionFilePath, upload);

                string deletePath = Server.MapPath("~/Upload/" + guidStirng);
                var dir = new DirectoryInfo(deletePath);
                dir.Delete(true);

                return RedirectToAction("PreviewUploadedQuestions", new RouteValueDictionary(new { controller = "Upload", action = "PreviewUploadedQuestions", id = upload.ID }));
            }
            return View(studentViewModel);
            // return View("~/Views/UploadResult", passageViewModelList);
        }

        public MashUploadResultViewModel CreatePassageQuestionsAnswers(string fileContent, string guidString, string oldPath, long uploadedID, string questionAzurePath, Uploads upload)
        {
            List<HtmlEntities> htmlEntities = GetHtmlEntities();
            MashUploadResultViewModel uploadResult = new MashUploadResultViewModel();
            int processPassage = 0;
            bool isContinueQuestion = false;
            bool isContinueAnswer = false;
            bool isNewPassage = true;

            Passage currentPassage = new Passage();
            Question currentQuestion = new Question();
            List<Answer> answers = new List<Answer>();
            StringBuilder sb = new StringBuilder();

            List<PassageViewModel> passageViewModelList = new List<PassageViewModel>();
            List<QuestionViewModel> questionViewModelList = new List<QuestionViewModel>();
            PassageViewModel passageViewModel = new PassageViewModel();
            QuestionViewModel questionViewModel = new QuestionViewModel();
            List<AnswerViewModel> answerViewModelList = new List<AnswerViewModel>();

            int passageStartIndex = 0, passageEndIndex = 0;

            if (!string.IsNullOrEmpty(fileContent))
            {
                XDocument document = XDocument.Parse(fileContent);
                for (int i = 0; i < htmlEntities.Count(); i++)
                {
                    fileContent = fileContent.Replace(htmlEntities[i].XmlCode, htmlEntities[i].HtmlCode);
                }
                XElement root = document.Elements().FirstOrDefault().Elements().FirstOrDefault();
                root.Elements().FirstOrDefault().Remove();
                {
                    int totalQuestions = root.Elements().Where(e => !(e.Elements().First().Value.Contains("-"))).Count() / 2;
                    foreach (XElement tr in root.Elements().FirstOrDefault().Elements().FirstOrDefault().Elements())
                    {
                        if (tr.Value.Trim() == "")
                        {
                            continue;
                        }
                        if (tr.Name.LocalName.ToLower() == "tr")
                        {
                            answers = new List<Answer>();
                            List<XElement> columns = tr.Elements().ToList();
                            int count = columns.Count;
                            var node = (columns[0].Elements().ToList()[0].FirstNode);

                            if (processPassage == 0)
                            {                               //Processing first line
                            //var currentNode = (XElement)node;

                            //if (!string.IsNullOrEmpty(currentNode.Value.ToString()))
                            //{
                            ContinuePassage:
                                if (isNewPassage)
                                {                                                                   //Questions and answers saved
                                    if (columns[0].Value.ToString().Contains("-"))
                                    {                                                               //Having passage
                                        passageViewModel = new PassageViewModel();
                                        passageStartIndex = Convert.ToInt16(columns[0].Value.Split('-')[0]);
                                        passageEndIndex = Convert.ToInt16(columns[0].Value.Split('-')[1]);

                                        currentPassage = SavePassage(columns, guidString);

                                        isContinueQuestion = true;
                                        isNewPassage = false;
                                        isContinueAnswer = false;
                                        passageViewModel = MapperHelper.MapToViewModel(currentPassage);
                                        continue;
                                    }
                                    else
                                    {
                                        isContinueQuestion = true;
                                        isNewPassage = false;
                                        isContinueAnswer = false;
                                        goto ContinueQuestion;
                                    }
                                }
                            ContinueQuestion:
                                if (isContinueQuestion)
                                {                                                             //Continue to save Question
                                    questionViewModel = new QuestionViewModel();

                                    currentQuestion = SaveQuestion(columns, currentPassage, upload, questionAzurePath, guidString, oldPath, passageStartIndex, passageEndIndex);
                                    questionViewModel = MapperHelper.MapToViewModel(currentQuestion);
                                    questionViewModelList.Add(questionViewModel);

                                    isContinueQuestion = false;
                                    isNewPassage = false;
                                    isContinueAnswer = true;
                                    continue;
                                }
                                if (isContinueAnswer)
                                {
                                    if (string.IsNullOrEmpty(columns[0].Value.ToString()) || columns[0].Value.Trim().ToString().ToLower() == "c" || columns[0].Value.ToString() == " ")
                                    {                                                                                                   //Answer sections
                                        var reader = columns[1].CreateReader();
                                        reader.MoveToContent();
                                        string answerDescription = reader.ReadInnerXml();

                                        QuestionType questionType;
                                        if (string.IsNullOrWhiteSpace(answerDescription))
                                        {                                       //Question has no answers . Question type is OpenEndedQuestion
                                            questionType = db.QuestionType.Where(qt => qt.Name.ToLower().Contains("open ended")).FirstOrDefault();
                                            currentQuestion.QuestionType = questionType;
                                            db.SaveChanges();
                                            continue;
                                        }
                                        else
                                        {                                       //Question have answers. 
                                            AnswerViewModel answerViewModel = new AnswerViewModel();
                                            answerViewModel = AddAnswerForQuestion(columns, currentQuestion, guidString, oldPath);
                                            answerViewModelList.Add(answerViewModel);
                                            continue;
                                        }
                                    }
                                    else if (columns[0].Value.Trim().ToString().ToLower() == "s")
                                    {                                                                   //Solution for the question
                                        AddSolutionForQuestion(columns, currentQuestion);
                                        continue;
                                    }
                                    else if (columns[0].Value.Trim().ToString().ToLower() == "t")
                                    {                                                                           //Column containing tags
                                        CreateQuestionTags(columns, currentQuestion);
                                        continue;
                                    }
                                    else
                                    {                                                                       //Save answer completed. Continue to New passage or new question
                                        if (currentQuestion.ID != 0 && currentQuestion.QuestionType == null)
                                            UpdateQuestionType(currentQuestion);
                                        isContinueQuestion = false;
                                        isNewPassage = true;
                                        isContinueAnswer = false;
                                        goto ContinuePassage;
                                        //if (columns[0].Value.ToString().Contains("-"))
                                        //{                                                               //Having passage

                                        //    isContinueQuestion = false;
                                        //    isNewPassage = true;
                                        //    isContinueAnswer = false;
                                        //    goto ContinuePassage;
                                        //}
                                        //else
                                        //{
                                        //    passageViewModel = MapperHelper.MapToViewModel(currentPassage);
                                        //    passageViewModelList.Add(passageViewModel);
                                        //    isContinueQuestion = true;
                                        //    isNewPassage = false;
                                        //    isContinueAnswer = false;
                                        //    goto ContinueQuestion;
                                        //    //continue;
                                        //}
                                    }
                                }
                            }
                        }
                    }
                }
            }
            UpdateQuestionType(currentQuestion);

            string questionIDs = "";
            foreach (QuestionViewModel question in questionViewModelList)
            {
                question.AnswerViewModelList = answerViewModelList.Where(a => a.QuestionID == question.ID).ToList();
                questionIDs += question.ID + ",";
            }
            uploadResult.questionViewModelList = questionViewModelList;
            uploadResult.UploadedQuestionIDs = questionIDs;
            return uploadResult;
            //return passageViewModelList;
        }

        public List<HtmlEntities> GetHtmlEntities()
        {
            List<HtmlEntities> htmlEntities = new List<HtmlEntities>();
            htmlEntities.Add(new HtmlEntities("&rsquo;", "&#8217;"));
            htmlEntities.Add(new HtmlEntities("&ldquo;", "&#8220;"));
            htmlEntities.Add(new HtmlEntities("&rdquo;", "&#8221;"));
            htmlEntities.Add(new HtmlEntities("&lsquo;", "&#8216;"));
            htmlEntities.Add(new HtmlEntities("&ndash;", "&#8211;"));
            htmlEntities.Add(new HtmlEntities("&bull;", "&#8226;"));
            // htmlEntities.Add(new HtmlEntities("&frac;", ""));
            htmlEntities.Add(new HtmlEntities("&deg;", "&#176;"));
            htmlEntities.Add(new HtmlEntities("&frasl;", "&#8260;"));
            htmlEntities.Add(new HtmlEntities("&pi;", "&#928;"));
            htmlEntities.Add(new HtmlEntities("&ang;", "&#8736;"));
            htmlEntities.Add(new HtmlEntities("&times;", "&#215;"));
            htmlEntities.Add(new HtmlEntities("&hellip", "&#8230;"));
            htmlEntities.Add(new HtmlEntities("&pound", "&#1234;"));
            //sc
            htmlEntities.Add(new HtmlEntities("&radic", "&#8888;"));

            return htmlEntities;

        }

        public Passage SavePassage(List<XElement> columns, string guidString)
        {
            var reader = columns[1].CreateReader();
            reader.MoveToContent();
            string passageString = reader.ReadInnerXml();

            Passage currentPassage = new Passage();
            currentPassage.CanShuffle = true;
            currentPassage.CreatedDate = DateTime.Now;
            currentPassage.CreatedBy = currentPassage.ModifiedBy = GetCurrentUserID();
            currentPassage.ModifiedDate = DateTime.Now;
            currentPassage.LastUpdated = DateTime.Now;

            db.Passage.Add(currentPassage);
            db.SaveChanges();

            List<string> imageSourceStringList = new List<string>();
            foreach (var element in columns[1].Elements())
            {
                if (element.ToString().Contains("img"))
                {                                                                                       //Question having image
                    var imageNodes = columns[1].Descendants("img");
                    var newImageNodes = columns[1].Descendants("img").ToList();
                    string actualSource = Server.MapPath("~/Upload/") + guidString + "/" + guidString + "_files";
                    int processing = 0;
                    foreach (var item in imageNodes)
                    {
                        string imageSource = item.Attribute("src").Value;
                        int totalCount = imageSource.Split('/').Count();
                        string imageName = imageSource.Split('/')[totalCount - 1];
                        string destinationPath = "OAP/Upload/Passage" + currentPassage.ID;
                        string filePath = actualSource + "/" + imageName;
                       // string azureImageNewPath = AzureHelper.UploadToAzureStorage(filePath, destinationPath, true);
                        newImageNodes[0].Attribute("src").Value = filePath;
                        processing++;
                        imageSourceStringList.Add(filePath);
                    }
                }
            }
            foreach (string newImageSource in imageSourceStringList)
            {
                string searchString = guidString + "_files";
                int place = passageString.IndexOf(searchString);

                if (place > 0 && !string.IsNullOrEmpty(newImageSource))
                {
                    List<string> source = newImageSource.Split('/').ToList();
                    source.RemoveAt(source.Count() - 1);
                    string alteredImageSource = string.Join("/", source.ToArray());
                    passageString = passageString.Remove(place, searchString.Length).Insert(place, alteredImageSource);
                }
            }
            currentPassage.Description = passageString;
          //  currentPassage.Description = htmlConverter.FormatFileContent(currentPassage.Description);

            db.Entry(currentPassage).State = EntityState.Modified;
            db.SaveChanges();
            return currentPassage;
        }

        public Question SaveQuestion(List<XElement> columns, Passage currentPassage, Uploads upload, string questionAzurePath, string guidString, string oldPath, int passageStartIndex, int passageEndIndex)
        {
            Question currentQuestion;

            int questionNumber = Convert.ToInt16(columns[0].Value.ToString());
            string questionString = "";
            var reader = columns[1].CreateReader();
            reader.MoveToContent();
            questionString = reader.ReadInnerXml();

            currentQuestion = new Question();
            if (questionNumber >= passageStartIndex && questionNumber <= passageEndIndex)
                currentQuestion.Passage = currentPassage;
            currentQuestion.Description = questionString;
            currentQuestion.Uploads = upload;
            currentQuestion.LastUpdated = DateTime.Now;
            currentQuestion.AdminId = GetCurrentUserID();
            currentQuestion.DownloadLink = questionAzurePath;
            currentQuestion.AdminId = GetCurrentUserID();
            db.Question.Add(currentQuestion);
            db.SaveChanges();

            List<string> imageSourceStringList = new List<string>();
            foreach (var element in columns[1].Elements())
            {
                if (element.ToString().Contains("img"))
                {                                                                                       //Question having image
                    var imageNodes = columns[1].Elements().FirstOrDefault().Elements().ToList();
                    string actualSource = Server.MapPath("~/Upload/") + guidString + "/" + guidString + "_files";
                    int processing = 0;
                    var tempElement = imageNodes;
                    int i = 0;
                    foreach (var item in imageNodes)
                    {
                        if (item.ToString().Contains("img"))
                        {
                            string imageSource = item.Attribute("src").Value;
                            int totalCount = imageSource.Split('/').Count();
                            string imageName = imageSource.Split('/')[totalCount - 1];
                            string destinationPath = "/Upload/Question/Images/" + currentQuestion.ID;
                            string filePath = actualSource + "/" + imageName;
                           // string azureImageNewPath = AzureHelper.UploadToAzureStorage(filePath, destinationPath, true);
                            processing++;
                            imageSourceStringList.Add(filePath);
                            i++;
                        }
                    }
                }
            }
            foreach (string newImageSource in imageSourceStringList)
            {
                //oldPath
                //string searchString = guidString + "_files";
                string searchString = oldPath + @"\" + guidString + "_files";
                searchString = searchString.Replace(@"\\", @"\");
                int place = questionString.IndexOf(searchString);

                if (place > 0 && !string.IsNullOrEmpty(newImageSource))
                {
                    List<string> source = newImageSource.Split('/').ToList();
                    source.RemoveAt(source.Count() - 1);
                    string alteredImageSource = string.Join("/", source.ToArray());

                    questionString = questionString.Remove(place, searchString.Length).Insert(place, alteredImageSource);
                }
            }
            currentQuestion.Description = questionString;
            currentQuestion.LastUpdated = DateTime.Now;
            currentQuestion.AdminId = GetCurrentUserID();
          ///  currentQuestion.Description = htmlConverter.FormatFileContent(currentQuestion.Description);
            db.Entry(currentQuestion).State = EntityState.Modified;
            db.SaveChanges();
            return currentQuestion;
        }

        public AnswerViewModel AddAnswerForQuestion(List<XElement> columns, Question currentQuestion, string guidString, string oldPath)
        {
            Answer answer = new Answer();
            string answerDescription = "";

            var reader = columns[1].CreateReader();
            reader.MoveToContent();
            answerDescription = reader.ReadInnerXml();

            answer.Description = answerDescription;

            if (columns[0].Value.ToString().ToLower() == "c")
            {
                answer.IsCorrect = true;
            }
            else
                answer.IsCorrect = false;
            answer.Question = currentQuestion;
            answer.LastUpdated = DateTime.Now;
            answer.Description = htmlConverter.FormatFileContent(answer.Description);
           // answer.Description = answer.Description;

            db.Answer.Add(answer);
            db.SaveChanges();

            List<string> imageSourceStringList = new List<string>();
            foreach (var element in columns[1].Elements())
            {
                if (element.ToString().Contains("img"))
                {                                                                                       //Question having image
                    var imageNodes = columns[1].Elements().FirstOrDefault().Elements().ToList();
                    string actualSource = Server.MapPath("~/Upload/") + guidString + "/" + guidString + "_files";
                    int processing = 0;
                    var tempElement = imageNodes;
                    int i = 0;
                    foreach (var item in imageNodes)
                    {
                        if (item.ToString().Contains("img"))
                        {
                            string imageSource = item.Attribute("src").Value;
                            int totalCount = imageSource.Split('/').Count();
                            string imageName = imageSource.Split('/')[totalCount - 1];
                            string destinationPath = "/Upload/Answer/Images/" + currentQuestion.ID;
                            string filePath = actualSource + "/" + imageName;
                           // string azureImageNewPath = AzureHelper.UploadToAzureStorage(filePath, destinationPath, true);
                            processing++;
                            imageSourceStringList.Add(filePath);
                            i++;
                        }
                    }
                }
            }
            foreach (string newImageSource in imageSourceStringList)
            {
                string searchString = oldPath + @"\" + guidString + "_files";
                searchString = searchString.Replace(@"\\", @"\");
                int place = answerDescription.IndexOf(searchString);

                if (place > 0 && !string.IsNullOrEmpty(newImageSource))
                {
                    List<string> source = newImageSource.Split('/').ToList();
                    source.RemoveAt(source.Count() - 1);
                    string alteredImageSource = string.Join("/", source.ToArray());

                    answerDescription = answerDescription.Remove(place, searchString.Length).Insert(place, alteredImageSource);
                }
            }

            answer.Description = answerDescription;
           // answer.Description = htmlConverter.FormatFileContent(answer.Description);

            db.Entry(answer).State = EntityState.Modified;
            db.SaveChanges();

            AnswerViewModel answerViewModel = new AnswerViewModel();
            answerViewModel = MapperHelper.MapToViewModel(answer);
            return answerViewModel;
        }

        public void AddSolutionForQuestion(List<XElement> columns, Question currentQuestion)
        {
            if (!String.IsNullOrEmpty(columns[1].Value.ToString()))
            {
                string solutionString = "";
                //foreach (var element in columns[1].Elements())
                //{
                //    solutionString = solutionString + element.ToString();
                //}

                var reader = columns[1].CreateReader();
                reader.MoveToContent();
                solutionString = reader.ReadInnerXml();
                currentQuestion.Solution = solutionString;
                currentQuestion.AdminId = GetCurrentUserID();
                currentQuestion.LastUpdated = DateTime.Now;
              //  currentQuestion.Description = htmlConverter.FormatFileContent(currentQuestion.Description);
                db.Entry(currentQuestion).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void CreateQuestionTags(List<XElement> columns, Question currentQuestion)
        {
            if (!string.IsNullOrEmpty(columns[1].Value.ToString()))
            {
                string tagString = columns[1].Value.ToString();
                List<string> tagList = tagString.Split(',').ToList();
                foreach (string newTag in tagList)
                {
                    Tag tags = db.Tag.Where(t => t.Name.ToLower() == newTag.ToLower()).FirstOrDefault();
                    if (tags == null)
                    {
                        tags = new Tag();
                        tags.Name = newTag;
                        db.Tag.Add(tags);
                        db.SaveChanges();
                    }
                    if (tags != null)
                    {
                        QuestionTags questionTags = new QuestionTags();
                        questionTags.Question = currentQuestion;
                        questionTags.Tag = tags;
                        questionTags.CreatedBy = GetCurrentUserID();
                        questionTags.CreatedDate = DateTime.Now;
                        db.QuestionTags.Add(questionTags);
                    }
                }
                db.SaveChanges();
            }
        }

        public void UpdateQuestionType(Question currentQuestion)
        {
            QuestionType questionType;
            if (currentQuestion.Answer.Where(a => a.IsCorrect ?? false).Count() > 1)
            {                                                                           //Question have multiple answers
                questionType = db.QuestionType.Where(qt => qt.Name.ToLower().Contains("multi select")).FirstOrDefault();
            }
            else
            {                                                                           //Qustion have only one answer
                questionType = db.QuestionType.Where(qt => qt.Name.ToLower().Contains("single select")).FirstOrDefault();

            }
            if (questionType != null)
            {
                currentQuestion.QuestionType = questionType;
              //  currentQuestion.Description = htmlConverter.FormatFileContent(currentQuestion.Description);
                currentQuestion.Description = currentQuestion.Description;

                db.Entry(currentQuestion).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        #endregion

        #region Old

        public bool CreateQuestionsFromFileContent(string fileContent)
        {
            Dictionary<int, List<Answer>> dictionary;
            bool result = true;
            List<Question> questions = new List<Question>();
            dictionary = new Dictionary<int, List<Answer>>();
            List<Answer> answers;

            XDocument document = XDocument.Parse(fileContent);
            int questionIndex = 0;
            //foreach (XElement root in document.Elements())
            Passage passage = new Passage();
            int startIndex = 0, endIndex = 0, passageIndex = 1;

            XElement root = document.Elements().FirstOrDefault().Elements().FirstOrDefault();
            {
                int totalQuestions = root.Elements().Where(e => !(e.Elements().First().Value.Contains("-"))).Count() / 2;
                //int totalQuestions = root.Elements().Count() / 2;
                //questionAddProgress.Total = totalQuestions;
                foreach (XElement tr in root.Elements())
                {
                    if (tr.Value.Trim() == "")
                    {
                        continue;
                    }
                    if (tr.Name == "tr")
                    {
                        answers = new List<Answer>();
                        List<XElement> columns = tr.Elements().ToList();
                        int count = columns.Count;
                        if (count == 2) // Question
                        {
                            var node = (columns[0].Elements().ToList()[0].FirstNode);
                            if (node != null)
                            {
                                try
                                {
                                    var questionNumberNode = (XElement)node;
                                    if (questionNumberNode.Value.Contains("-"))
                                    {
                                        string[] indexes = questionNumberNode.Value.Split('-');
                                        startIndex = Convert.ToInt32(indexes[0]);
                                        endIndex = Convert.ToInt32(indexes[1]);
                                        passage.Description = (GetDescription(columns[1].Elements().ToList()[0].FirstNode));
                                        passage.ID = passageIndex * -1;
                                        passageIndex++;
                                        continue;
                                    }
                                }
                                catch (Exception)
                                {
                                    var questionNumber = node.ToString();
                                    if (questionNumber.Contains("-"))
                                    {
                                        string[] indexes = questionNumber.Split('-');
                                        startIndex = Convert.ToInt32(indexes[0]);
                                        endIndex = Convert.ToInt32(indexes[1]);
                                        passage.Description = (GetDescription(columns[1].Elements().ToList()[0].FirstNode));
                                        passage.ID = passageIndex * -1;
                                        passageIndex++;
                                        continue;
                                    }
                                }
                            }
                            try
                            {
                                StringBuilder sb = new StringBuilder();

                                string questionDescription = (GetDescription(columns[1]));//.ToString();
                                //string questionDescription = ExtractHtmlInnerText(GetDescription(columns[1].Elements().ToList()[0].FirstNode));//.ToString();
                                Question question = new Question()
                                {
                                    Description = questionDescription,
                                    //QuestionCategoryID = questionCategory,
                                    QuestionTypeID = 1,
                                    //AdminId = LocalHelper.LoggedInAdmin.ID,
                                    ShowQuestion = true,
                                    LastUpdated = DateTime.Now
                                };
                                questionIndex++;
                                if (questionIndex >= startIndex && questionIndex <= endIndex)
                                {
                                    question.Description = passage.Description + "<br/><br/>" + question.Description;
                                    question.PassageID = passage.ID;
                                }

                                sb.Append("<b>" + questionIndex.ToString() + "</b>" + question.Description);
                                sb.Append("<br/><br/>");
                                //lblParse.Content = lblParse.Content + "," + questionIndex;
                                //questionAddProgress.Parsed = questionIndex;
                                //_serviceClient.AddQuestionAsync(question, questionIndex);
                                string s = sb.ToString();
                            }
                            catch (Exception ex)
                            {
                                throw;
                            }

                        }
                        else
                        {
                            try
                            {
                                for (int i = 1; i < count; i++)
                                {
                                    string answerDescription = (GetDescription(columns[i].Elements().ToList()[0].FirstNode));
                                    //string answerDescription = ExtractHtmlInnerText(GetDescription(columns[i].Elements().ToList()[0].FirstNode));
                                    Answer answer = new Answer()
                                    {
                                        Description = answerDescription,
                                        IsCorrect = (i == 1), //First option is correct
                                    };
                                    answers.Add(answer);
                                }
                                //answers = (List<Answer>)Shuffle(answers);

                                dictionary.Add(questionIndex, answers);
                            }
                            catch (Exception ex)
                            {
                                throw;
                            }
                        }
                    }
                }

            }


            return result;
        }

        private string GetDescription(XNode xNode)
        {
            if (xNode.NodeType == System.Xml.XmlNodeType.Element)
            {
                XElement element = (XElement)xNode;
                GetImageName(ref element);
            }
            return xNode.ToString(); ;
        }

        private void GetImageName(ref XElement xElement)
        {
            XName name = XName.Get("img");
            List<XElement> images = xElement.Elements(name).ToList();
            foreach (XElement img in images)
            {
                XName src = XName.Get("src");
                XAttribute attribute = img.Attribute(src);
                string fileName = Guid.NewGuid() + System.IO.Path.GetExtension(attribute.Value);


                string myDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                string tempDirectory = string.Format("{0}\\temp", myDocuments);

                System.IO.File.Copy(tempDirectory + "\\" + attribute.Value, tempDirectory + "\\" + fileName);
                //UploadFile(fileName, System.IO.File.OpenRead(tempDirectory + "\\" + attribute.Value));

                attribute.Value = Server.MapPath("~/Upload/QuestionImages/") + fileName;
            }
            for (int i = 0; i < xElement.Elements().Count(); i++)
            {
                XElement element = xElement.Elements().ElementAt(i);
                GetImageName(ref element);
            }
        }

        //private void GetImageName(ref XElement xElement)
        //{
        //    XName name = XName.Get("img");
        //    List<XElement> images = xElement.Elements(name).ToList();
        //    foreach (XElement img in images)
        //    {
        //        XName src = XName.Get("src");
        //        XAttribute attribute = img.Attribute(src);
        //        string fileName = Guid.NewGuid() + System.IO.Path.GetExtension(attribute.Value);


        //        string myDocuments = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        //        string tempDirectory = string.Format("{0}\\temp", myDocuments);

        //        System.IO.File.Copy(tempDirectory + "\\" + attribute.Value, tempDirectory + "\\" + fileName);
        //        UploadFile(fileName, System.IO.File.OpenRead(tempDirectory + "\\" + attribute.Value));
        //        attribute.Value = LocalHelper.WebAppUrl + "/QuestionImages/" + fileName;
        //    }
        //    for (int i = 0; i < xElement.Elements().Count(); i++)
        //    {
        //        XElement element = xElement.Elements().ElementAt(i);
        //        GetImageName(ref element);
        //    }
        //}


        //[HttpPost]
        //public ActionResult UploadDetailNew(HttpPostedFileBase file, StudentViewModel studentViewModel)
        //{
        //    object missingType = Type.Missing;
        //    object readOnly = true;
        //    object isVisible = false;
        //    object documentFormat = 8;
        //    string randomName = DateTime.Now.Ticks.ToString();
        //    object htmlFilePath = Server.MapPath("~/Upload/") + randomName + ".htm";
        //    string directoryPath = Server.MapPath("~/Upload/") + randomName + "_files";

        //    ApplicationClass applicationclass = new ApplicationClass();
        //    string newFilePath = Server.MapPath("~/Upload/" + file.FileName);
        //    file.SaveAs(newFilePath);
        //    object fileName = newFilePath;


        //    object missing = System.Reflection.Missing.Value;
        //    Microsoft.Office.Interop.Word.Application wordApp = new Microsoft.Office.Interop.Word.Application();

        //    Microsoft.Office.Interop.Word.Document wordDoc = null;

        //    wordDoc = wordApp.Documents.Open(ref fileName, ref missing, ref readOnly, ref missing, ref missing,
        //            ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref missing, ref isVisible, ref missing,
        //            ref missing, ref missing);


        //    applicationclass.Documents.Open(ref fileName,
        //                            ref readOnly,
        //                            ref missingType, ref missingType, ref missingType,
        //                            ref missingType, ref missingType, ref missingType,
        //                            ref missingType, ref missingType, ref isVisible,
        //                            ref missingType, ref missingType, ref missingType,
        //                            ref missingType, ref missingType);
        //    applicationclass.Visible = false;

        //    Document document = applicationclass.ActiveDocument;
        //    //document.Save();
        //    try
        //    {
        //        wordDoc.SaveAs(ref htmlFilePath, ref missing, ref missing, ref missing, ref missing, ref missing,
        //            ref missing, ref missing, ref missing, ref missing, ref missing,
        //            ref missing, ref missing, ref missing, ref missing, ref missing);


        //        //document.SaveAs2(ref htmlFilePath, ref documentFormat, ref missingType,
        //        //       ref missingType, ref missingType, ref missingType,
        //        //       ref missingType, ref missingType, ref missingType,
        //        //       ref missingType, ref missingType, ref missingType,
        //        //       ref missingType, ref missingType, ref missingType,
        //        //       ref missingType);

        //    }
        //    catch
        //    { }
        //    finally
        //    {
        //        document.Close(ref missingType, ref missingType, ref missingType);
        //        wordDoc.Close(ref missingType, ref missingType, ref missingType);
        //    }

        //    byte[] bytes;
        //    using (FileStream fs = new FileStream(htmlFilePath.ToString(), FileMode.Open, FileAccess.Read))
        //    {
        //        BinaryReader reader = new BinaryReader(fs);
        //        bytes = reader.ReadBytes((int)fs.Length);
        //        fs.Close();
        //    }
        //    Response.BinaryWrite(bytes);
        //    Response.Flush();

        //    //Delete the Html File
        //    //System.IO.File.Delete(htmlFilePath.ToString());
        //    //foreach (string fl in Directory.GetFiles(directoryPath))
        //    //{
        //    //    System.IO.File.Delete(fl);
        //    //}
        //    //Directory.Delete(directoryPath);
        //    Response.End();

        //    ReadHtmlFile(htmlFilePath.ToString());
        //    return View(bytes);
        //}

        public System.Text.StringBuilder ReadHtmlFile(string htmlFileNameWithPath)
        {
            System.Text.StringBuilder storeContent = new System.Text.StringBuilder();

            try
            {
                using (System.IO.StreamReader htmlReader = new System.IO.StreamReader(htmlFileNameWithPath))
                {
                    string lineStr;
                    while ((lineStr = htmlReader.ReadLine()) != null)
                    {
                        storeContent.Append(lineStr);
                    }
                }
            }
            catch (Exception objError)
            {
                throw objError;
            }

            return storeContent;
        }

        public ActionResult UploadResult()
        {
            return View();
        }

        #endregion

        #region Preview Uploaded Questions

        public ActionResult PreviewUploadedQuestions(long id = 0)
        {
            Uploads upload = db.Uploads.Find(id);
            if (upload == null)
            {
                return HttpNotFound();
            }
            UploadQuestionViewModel uploadQuestionViewModel = new UploadQuestionViewModel();
            List<QuestionViewModel> questionsUnderUpload = MapperHelper.MapToViewModelListToShowFullQuestion(upload.Question.AsQueryable());
            if (questionsUnderUpload.Count() > 0)
            {
                uploadQuestionViewModel.UploadedQuestions = questionsUnderUpload;
                uploadQuestionViewModel.QuestionViewModel = questionsUnderUpload.FirstOrDefault();
                uploadQuestionViewModel.QuestionViewModel.CorrectAnswerID = uploadQuestionViewModel.QuestionViewModel.AnswerViewModelList.Where(a => a.isCorrect).Select(a => a.ID).FirstOrDefault();
                uploadQuestionViewModel.CurrentIndex = 0;
                uploadQuestionViewModel.TotalQuestions = questionsUnderUpload.Count();
                uploadQuestionViewModel.SelectedIndex = 0;
                uploadQuestionViewModel.Marks = Convert.ToInt32(uploadQuestionViewModel.QuestionViewModel.Marks);
            }

            //uploadQuestionViewModel.Marks = Convert.ToInt32(uploadQuestionViewModel.QuestionViewModel.Marks);

            return View(uploadQuestionViewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult PreviewUploadedQuestions(UploadQuestionViewModel uploadQuestionViewModel, string selIndex)
        {
            List<Question> uploadedQuestions = db.Question.Where(q => q.UploadID != null && q.UploadID == uploadQuestionViewModel.ID).ToList();
            List<QuestionViewModel> questionViewModelList = MapperHelper.MapToViewModelListToShowFullQuestion(uploadedQuestions.AsQueryable());
            Question questionToSaved = uploadedQuestions[uploadQuestionViewModel.CurrentIndex];
            questionToSaved = MapperHelper.MapToDomainObject(db, uploadQuestionViewModel.QuestionViewModel, questionToSaved);
            questionToSaved.Marks = uploadQuestionViewModel.Marks;
            questionToSaved.NegativeMarks = 0;
            if (questionToSaved.ComplexityID == 0)
            {
                questionToSaved.ComplexityID = null;
            }
            if (questionToSaved.SubjectID == 0)
            {
                questionToSaved.SubjectID = null;
            }
            if (uploadQuestionViewModel.QuestionViewModel.QuestionType.ToLower().Equals("open ended"))
            {
                questionToSaved.Solution = uploadQuestionViewModel.QuestionViewModel.Solution;
            }
            db.Entry(questionToSaved).State = EntityState.Modified;
            db.SaveChanges();

            Answer answer;
            if (uploadQuestionViewModel.QuestionViewModel.AnswerViewModelList != null)
            {
                if (uploadQuestionViewModel.QuestionViewModel.QuestionType.ToLower().Equals("single select"))
                {                                                                                   //Question type is single select
                    foreach (AnswerViewModel answerViewModel in uploadQuestionViewModel.QuestionViewModel.AnswerViewModelList)
                    {
                        answer = db.Answer.Find(answerViewModel.ID);
                        answer.Description = answerViewModel.Description;
                        answer.IsCorrect = uploadQuestionViewModel.QuestionViewModel.CorrectAnswerID == answerViewModel.ID ? true : false;
                        answer.LastUpdated = DateTime.Now;
                      //  answer.Description = htmlConverter.FormatFileContent(answer.Description);
                        db.Entry(answer).State = EntityState.Modified;
                    }
                }
                else if (uploadQuestionViewModel.QuestionViewModel.QuestionType.ToLower().Equals("multi select"))
                {                                                                               //Question type is multiselect
                    foreach (AnswerViewModel answerViewModel in uploadQuestionViewModel.QuestionViewModel.AnswerViewModelList)
                    {
                        answer = db.Answer.Find(answerViewModel.ID);
                        answer.Description = answerViewModel.Description;
                        answer.IsCorrect = answerViewModel.isCorrect;
                        answer.LastUpdated = DateTime.Now;
                       // answer.Description = htmlConverter.FormatFileContent(answer.Description);
                        db.Entry(answer).State = EntityState.Modified;
                    }
                    //db.SaveChanges();
                }

                db.SaveChanges();
            }
            if (uploadQuestionViewModel.QuestionViewModel.QuestionPassage != null && uploadQuestionViewModel.QuestionViewModel.QuestionPassage.ID != 0)
            {
                Passage passage = db.Passage.Find(uploadQuestionViewModel.QuestionViewModel.QuestionPassage.ID);
                passage = MapperHelper.MapToDomainObject(db, uploadQuestionViewModel.QuestionViewModel.QuestionPassage, passage);
                passage.LastUpdated = DateTime.Now;
                passage.ModifiedDate = DateTime.Now;
                passage.ModifiedBy = GetCurrentUserID();
               // passage.Description = htmlConverter.FormatFileContent(passage.Description);
                db.Entry(passage).State = EntityState.Modified;
                db.SaveChanges();
            }

            if (selIndex.ToLower() == "finish")
            {
                return RedirectToAction("Index");
            }

            int selectedIndex = Convert.ToInt32(selIndex);
            Question questionToLoad = questionToSaved;
            questionToLoad = uploadedQuestions[selectedIndex];
            ModelState.Clear(); //Required for array of items update. Otherwise overridden with first item in array. CHECK


            uploadQuestionViewModel.QuestionViewModel = MapperHelper.MapToViewModelToShowFullQuestion(questionToLoad);
            uploadQuestionViewModel.QuestionViewModel.CorrectAnswerID = uploadQuestionViewModel.QuestionViewModel.AnswerViewModelList.Where(a => a.isCorrect).Select(a => a.ID).FirstOrDefault();

            uploadQuestionViewModel.Marks = Convert.ToInt32(uploadQuestionViewModel.QuestionViewModel.Marks);

            uploadQuestionViewModel.CurrentIndex = uploadQuestionViewModel.SelectedIndex = selectedIndex;
            uploadQuestionViewModel.TotalQuestions = questionViewModelList.Count();

            return View(uploadQuestionViewModel);
        }

        #endregion

        #region QuestionChangesAfterUpload

        public ActionResult ChangeUploadedQuestions(int uploadID)
        {
            UploadQuestionViewModel uploadQuestionViewModel = new UploadQuestionViewModel();
            uploadQuestionViewModel.ID = uploadID;
            return View(uploadQuestionViewModel);
        }

        [HttpPost]
        public ActionResult ChangeUploadedQuestions(UploadQuestionViewModel uploadQuestionViewModel, HttpPostedFileBase file)
        {
            if (file != null && uploadQuestionViewModel.ID != 0)
            {                                                                           //File is not null
                string fileName = file.FileName;
                string fileExtension = fileName.Split('.')[1];
                string[] excelFileExtensions = { "xls", "xlsx" };
                if (excelFileExtensions.Contains(fileExtension))
                {                                                                       //Uploaded file is in excel format
                    List<MashUploadResultViewModel> mashUploadResultViewModelList = ChangeQuestionDetails(file, uploadQuestionViewModel);
                    ViewBag.ReturnUrl = "Upload/Index";
                    return View("~/Views/Shared/UploadedResult.cshtml", mashUploadResultViewModelList);
                }
                else
                {                                                                       //Uploaded file not in excel format
                    //return View(questionComplexityViewModel);
                }
            }
            else
            {                                                                           //File is null                                          
                //return View(questionComplexityViewModel);
            }
            return View(uploadQuestionViewModel);
        }

        public List<MashUploadResultViewModel> ChangeQuestionDetails(HttpPostedFileBase file, UploadQuestionViewModel uploadQuestionViewModel)
        {
            List<MashUploadResultViewModel> mashUploadResultViewModelList = new List<MashUploadResultViewModel>();
            string filePath = System.IO.Path.GetFullPath(file.FileName);
            string extension = Path.GetExtension(file.FileName);
            string fileName = Guid.NewGuid().ToString() + extension;
            string fullPath = Server.MapPath("//ExcelUpload//QuestionUploadChange//" + fileName);
            string directory = Path.GetDirectoryName(fullPath);


            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
            file.SaveAs(fullPath);
            fullPath = fullPath.Replace(@"\\", @"\");


            FileInfo newFile = new FileInfo(fullPath);

            ExcelPackage pck = new ExcelPackage(newFile);

            ExcelWorksheet worksheet = pck.Workbook.Worksheets[1];
            int iRowCnt = worksheet.Dimension.End.Row;
            Uploads uploads = db.Uploads.Find(uploadQuestionViewModel.ID);
            int loggedInUserID = GetCurrentUserID();
            UserProfile loggedInUserProfile = db.UserProfile.Find(loggedInUserID);
            if (iRowCnt - 1 == uploads.Question.Count())
            {
                for (int i = 2; i <= iRowCnt; i++)
                {
                    bool uploadResult = true;
                    string uploadReason = "";
                    string uploadMessage = "";
                    string uploadName = "";
                    MashUploadResultViewModel mashUploadResultViewModel = new MashUploadResultViewModel();
                    int questionNumber = 0;
                    try
                    {

                        string questionNumberString = worksheet.Cells[i, 1].Text.ToString();
                        if (!string.IsNullOrEmpty(questionNumberString))
                            questionNumber = Convert.ToInt32(questionNumberString) - 1;
                        else
                        {
                            uploadResult = false;
                            uploadMessage += " Invalid question number in row " + i + ". ";
                            break;
                        }
                        Question question = uploads.Question.ToList()[questionNumber];
                        string subjectName = worksheet.Cells[i, 3].Text.ToString();
                        if (!string.IsNullOrEmpty(subjectName))
                        {
                            Subject subject = db.Subject.Where(s => s.Name.ToLower() == subjectName.ToLower()).FirstOrDefault();

                            if (subject == null)
                            {
                                subject = new Subject();
                                CreateSubject(subjectName, ref subject, loggedInUserID);
                                db.Subject.Add(subject);
                                uploadMessage += "New subject created. ";
                            }
                            question.Subject = subject;
                        }
                        else
                        {
                        }

                        List<QuestionTags> questionTagsList = question.QuestionTags.ToList();
                        db.QuestionTags.RemoveRange(questionTagsList);

                        string topic = worksheet.Cells[i, 5].Text.ToString();

                        string subTopic = worksheet.Cells[i, 6].Text.ToString();

                        CreateQuestionTags(topic, subTopic, ref question);

                        string difficultyString = worksheet.Cells[i, 7].Text.ToString();
                        int difficulty = 0;
                        if (!string.IsNullOrEmpty(difficultyString))
                            difficulty = Convert.ToInt32(difficultyString);

                        question.Difficulty = difficulty;

                        string markString = worksheet.Cells[i, 9].Text.ToString();
                        decimal mark = 0;
                        if (!string.IsNullOrEmpty(markString))
                            mark = Convert.ToDecimal(markString);
                        question.Marks = mark;

                        string negativeMarkString = worksheet.Cells[i, 10].Text.ToString();
                        decimal negativeMark = 0;
                        if (!string.IsNullOrEmpty(negativeMarkString))
                            negativeMark = Convert.ToDecimal(negativeMarkString);
                        question.NegativeMarks = negativeMark;

                        string answerKey = worksheet.Cells[i, 8].Text.ToString();
                        if (!string.IsNullOrEmpty(answerKey))
                        {
                            AssignAnswerByAnswerKey(answerKey, ref question);
                        }
                      //  question.Description = htmlConverter.FormatFileContent(question.Description);
                        question.Description = question.Description;

                        db.Entry(question).State = EntityState.Modified;

                        db.SaveChanges();
                    }
                    catch
                    {
                        uploadResult = false;
                        uploadMessage = "Failed for " + i;
                    }
                    if (string.IsNullOrEmpty(uploadMessage))
                        uploadMessage = "Successfully updated. ";
                    mashUploadResultViewModel.Name = "Question " + (questionNumber + 1);
                    mashUploadResultViewModel.IsSuccess = uploadResult;
                    mashUploadResultViewModel.Reason = uploadReason;
                    mashUploadResultViewModel.Message = uploadMessage;
                    mashUploadResultViewModelList.Add(mashUploadResultViewModel);
                }
            }
            else
            {
                MashUploadResultViewModel mashUploadResultViewModel = new MashUploadResultViewModel();
                mashUploadResultViewModel.IsSuccess = false;
                mashUploadResultViewModel.Message = "Question given doesnot match with existing questions record.";
                mashUploadResultViewModelList.Add(mashUploadResultViewModel);
            }

            return mashUploadResultViewModelList;
        }

        public void CreateSubject(string name, ref Subject subject, int userId)
        {
            subject.Name = name;
            subject.CreatedBy = userId;
            subject.ModifiedBy = userId;

        }

        public void CreateQuestionTags(string parentTagName, string subTagName, ref Question question)
        {
            if (!string.IsNullOrEmpty(parentTagName))
            {
                Tag parentTag = db.Tag.Where(t => t.Name.ToLower() == parentTagName.ToLower() && t.ParentTagID == null || t.ParentTagID == 0).FirstOrDefault();
                long parentTagID = parentTag != null ? parentTag.ID : 0;
                Tag subTag = db.Tag.Where(t => t.Name.ToLower() == subTagName.ToLower() && (t.ParentTagID != null || t.ParentTagID != 0) && t.ParentTagID != parentTagID).FirstOrDefault();
                if (parentTag == null)
                {
                    parentTag = new Tag();
                    parentTag.Name = parentTagName;
                    db.Tag.Add(parentTag);

                    if (subTag == null && !string.IsNullOrEmpty(subTagName))
                    {
                        subTag = new Tag();
                        subTag.Name = subTagName;
                        subTag.ParentTagID = parentTag.ID;
                        db.Tag.Add(subTag);
                    }
                }
                else if (parentTag != null)
                {
                    if (subTag == null)
                    {
                        subTag = new Tag();
                        subTag.Name = subTagName;
                        subTag.ParentTagID = parentTag.ID;
                        db.Tag.Add(subTag);
                    }
                }

                if (parentTag != null)
                {
                    QuestionTags parentQuestionTags = new QuestionTags();
                    parentQuestionTags.Question = question;
                    parentQuestionTags.Tag = parentTag;
                    question.QuestionTags.Add(parentQuestionTags);
                    //db.QuestionTags.Add(parentQuestionTags);
                }
                if (subTag != null)
                {
                    QuestionTags subQuestionTags = new QuestionTags();
                    subQuestionTags.Question = question;
                    subQuestionTags.Tag = parentTag;
                    question.QuestionTags.Add(subQuestionTags);
                    //db.QuestionTags.Add(subQuestionTags);
                }
            }

        }

        public void AssignAnswerByAnswerKey(string answerKey, ref Question question)
        {
            int correctOption = (int)Convert.ToChar(answerKey.ToLower());
            correctOption = correctOption - 97;
            List<Answer> answerList = question.Answer.ToList();
            int i = 0;
            foreach (Answer answer in answerList)
            {
                if (i != correctOption)
                    answer.IsCorrect = false;
                else
                    answer.IsCorrect = true;
               // answer.Description = htmlConverter.FormatFileContent(answer.Description);
                answer.Description =answer.Description;

                db.Entry(answer).State = EntityState.Modified;
                i++;
            }
        }

        #endregion


        #region QuestionImageUpload

        public void UploadQuestionImageByEditor()
        {
            if (HttpContext.Request.Files.Count > 0)
            {
                string CKEditorFuncNum = Request["CKEditorFuncNum"];
                HttpPostedFileBase file = HttpContext.Request.Files[0];
                string filePath = "";
                string imageName = file.FileName;
                string guidString = Guid.NewGuid().ToString();
                if (!Directory.Exists(Server.MapPath("~/Upload/Question/Images/" + guidString + "/")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/Upload/Question/Images/" + guidString + "/"));
                }
                string path = System.IO.Path.Combine(Server.MapPath("~/Upload/Question/Images/" + guidString + "/"), imageName);
                file.SaveAs(path);

                Response.Write("<script>window.parent.CKEDITOR.tools.callFunction(" + CKEditorFuncNum + ", \"" + path + "\");</script>");
            }

        }

        public ActionResult UploadedImages()
        {
            string guidString = Request["GUIDString"] != null ? Request["GUIDString"] : "";
            var appData = Server.MapPath("~/Upload/Question/Images/" + guidString + "/");
            var images = Directory.GetFiles(appData).Select(x => new ImageViewModel
            {
                url = Url.Content("/upload/question/images/" + guidString + "/" + Path.GetFileName(x))
            });
            return View(images);
        }
        #endregion

        #region Delete
        public void Delete(string uploadIDString)
        {
            int i = 0;
            int TotalQueationCount = 0;
            bool result = true;
            try
            {
                if (!string.IsNullOrEmpty(uploadIDString))
                {
                    List<int> uploadIDList = uploadIDString.Split(',').Select(int.Parse).ToList();
                    List<Uploads> uploadsList = new List<Uploads>();
                    TotalQueationCount = uploadIDList.Count();
                    foreach (int uploadID in uploadIDList)
                    {

                        //Question question = new Question();
                        Uploads upload = db.Uploads.Find(uploadID);
                        //Question question = db.Question.Where(x => x.UploadID == upload.ID);
                        if (upload.Question.Select(x => x.QuestionPaperSectionQuestion).Count() > 0)
                        {
                            List<QuestionPaperSectionQuestion> assessmentSectionQuestionList = upload.Question.SelectMany(x => x.QuestionPaperSectionQuestion).ToList();
                            if (assessmentSectionQuestionList == null || assessmentSectionQuestionList.Count() == 0)
                            {
                                List<QuestionPaperQuestion> assessmentQuestionList = upload.Question.SelectMany(x => x.QuestionPaperQuestion).ToList();
                                db.QuestionPaperSectionQuestion.RemoveRange(assessmentSectionQuestionList);
                                db.QuestionPaperQuestion.RemoveRange(assessmentQuestionList);

                                IEnumerable<Question> questionsUnderUpload = upload.Question;
                                if (questionsUnderUpload.Count() > 0)
                                {
                                    IEnumerable<QuestionPaperSectionQuestion> questionPaperSectionQuestions = questionsUnderUpload.SelectMany(x => x.QuestionPaperSectionQuestion);
                                    if (questionPaperSectionQuestions.Count() <= 0)
                                    {
                                        List<Passage> passageList = new List<Passage>();

                                        db.Answer.RemoveRange(questionsUnderUpload.SelectMany(x => x.Answer));
                                        db.QuestionTags.RemoveRange(questionsUnderUpload.SelectMany(x => x.QuestionTags));
                                        db.Question.RemoveRange(questionsUnderUpload);
                                        db.Passage.RemoveRange(passageList);

                                    }
                                    else
                                    {
                                        result = false;
                                    }
                                }
                                db.Uploads.Remove(upload);
                                db.SaveChanges();
                            }
                        }
                        else if (upload.Question.Select(x => x.QuestionPaperSectionQuestion).Count() == 0)
                        {
                            db.Uploads.Remove(upload);
                            db.SaveChanges();
                            result = true;
                        }

                        i++;
                    }
                }
            }
            catch
            {
                result = false;
            }
            string DeleteRatio = i.ToString() + "/" + TotalQueationCount.ToString();
            Response.Write(DeleteRatio);
        }

        #endregion

        //public void MoveTempQuestionToActualQuestion(long uploadID = 0)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        TempUploads tempUploads = db.TempUploads.Find(uploadID);
        //        Uploads uploads = new Uploads();
        //        uploads.ID = tempUploads.ID;
        //        uploads.Title = tempUploads.Title;
        //        uploads.UploadedDate = tempUploads.UploadedDate;
        //        uploads.UploadedUserID = tempUploads.UploadedUserID;

        //        db.Uploads.Add(uploads);
        //        IQueryable<TempQuestion> tempQuestionList = db.TempQuestion.Where(q => q.UploadID != null && q.UploadID == uploadID).AsQueryable();
        //        List<QuestionViewModel> tempQuestionViewmodelList = MapperHelper.MapToViewModelList(tempQuestionList);
        //        Question question = new Question();
        //        foreach (QuestionViewModel Qmodel in tempQuestionViewmodelList)
        //        {
        //            Passage passage = new Passage();
        //            if (question.PassageID != null)
        //            {
        //                TempPassage tempPassage = db.TempPassage.Find(question.PassageID);
        //                PassageViewModel passageViewModel = MapperHelper.MapToViewModel(tempPassage);
        //                passage.Description = passageViewModel.Description;
        //                passage.CreatedDate = DateTime.Now;
        //                passage.CreatedBy = GetCurrentUserID();
        //                passage.LastUpdated = passageViewModel.LastUpdated;
        //                passage.ModifiedBy = GetCurrentUserID();
        //                passage.ModifiedDate = DateTime.Now;
        //                passage.CanShuffle = passageViewModel.CanShuffle;
        //                db.Passage.Add(passage);
        //            }
        //            question.Description = Qmodel.Description;
        //            question.LastUpdated = DateTime.Now;
        //            question.DownloadLink = Qmodel.DownloadLink;
        //            question.Marks = Qmodel.Marks;
        //            question.NegativeMarks = Qmodel.NegativeMarks;
        //            question.UploadID = uploads.ID;
        //            db.Question.Add(question);
        //            db.SaveChanges();


        //            IQueryable<TempAnswer> tempAnswers = db.TempAnswer.Where(x => x.QuestionID == Qmodel.ID).AsQueryable();
        //            List<AnswerViewModel> answerList = MapperHelper.MapToViewModelList(tempAnswers);
        //            foreach (AnswerViewModel Amodel in answerList)
        //            {
        //                Answer ans = new Answer();
        //                ans.Description = Amodel.Description;
        //                ans.LastUpdated = DateTime.Now;
        //                ans.IsCorrect = Amodel.isCorrect;
        //                ans.Description = htmlConverter.FormatFileContent(ans.Description);

        //                db.Answer.Add(ans);
        //            }
        //            db.SaveChanges();

        //            IQueryable<TempQuestionTags> tempQuestionTags = db.TempQuestionTags.Where(x => x.QuestionID == Qmodel.ID).AsQueryable();
        //            IQueryable<TempTag> Tag = tempQuestionTags.Select(t => t.TempTag);
        //            foreach (TempTag tags in Tag)
        //            {
        //                Tag tag = new Tag();
        //                tag.ID = tags.ID;
        //                tag.Name = tags.Name;
        //                tag.ParentTagID = tags.ParentTagID;
        //                db.Tag.Add(tag);
        //            }

        //            db.SaveChanges();

        //            //TempSubject
        //            if (question.SubjectID != null)
        //            {
        //                IQueryable<TempSubject> tempSubjects = db.TempSubject.Where(x => x.ID == Qmodel.ID).AsQueryable();
        //                foreach (TempSubject sub in tempSubjects)
        //                {
        //                    Subject subs = new Subject();

        //                    subs.ID = sub.ID;
        //                    subs.Description = sub.Description;
        //                    subs.Name = sub.Name;
        //                    subs.Hours = sub.Hours;
        //                    subs.ModifiedBy = sub.ModifiedBy;
        //                    subs.ModifiedDate = sub.ModifiedDate;
        //                    subs.CreatedDate = sub.CreatedDate;
        //                    subs.CreatedBy = sub.CreatedBy;
        //                    db.Subject.Add(subs);
        //                }

        //                db.SaveChanges();
        //            }
        //            if (question.ComplexityID != null)
        //            {
        //                IQueryable<TempComplexity> tempComplexitys = db.TempComplexity.Where(x => x.ID == Qmodel.ID).AsQueryable();
        //                foreach (TempComplexity comps in tempComplexitys)
        //                {
        //                    Complexity comp = new Complexity();
        //                    comp.ID = comps.ID;
        //                    comp.ComplexityLevel = comps.ComplexityLevel;
        //                    db.Complexity.Add(comp);
        //                };

        //                db.SaveChanges();
        //            }
        //            if (question.PassageQuestionOrder.Count() != 0)
        //            {
        //                IQueryable<TempPassageQuestionOrder> tempPassageQuestionOrders = db.TempPassageQuestionOrder.Where(x => x.ID == Qmodel.QuestionCategoryID).AsQueryable();
        //                foreach (TempPassageQuestionOrder passg in tempPassageQuestionOrders)
        //                {
        //                    PassageQuestionOrder pass = new PassageQuestionOrder();
        //                    pass.ID = passg.ID;
        //                    pass.Order = passg.Order;
        //                    pass.CreatedBy = passg.CreatedBy;
        //                    pass.CreatedDate = passg.CreatedDate;
        //                    db.PassageQuestionOrder.Add(pass);
        //                };

        //                db.SaveChanges();
        //            }
        //            if (question.QuestionCategoryID != null)
        //            {
        //                IQueryable<TempQuestionCategory> tempQuestionCategorys = db.TempQuestionCategory.Where(x => x.ID == Qmodel.QuestionCategoryID).AsQueryable();
        //                foreach (TempQuestionCategory ques in tempQuestionCategorys)
        //                {
        //                    QuestionCategory que = new QuestionCategory();
        //                    que.ID = ques.ID;
        //                    que.Description = ques.Description;
        //                    que.Name = ques.Name;
        //                    que.ParentCategoryID = ques.ParentCategoryID;
        //                    que.AdminID = ques.AdminID;
        //                    db.QuestionCategory.Add(que);
        //                };

        //                db.SaveChanges();
        //            }
        //            if (question.QuestionTypeID != null)
        //            {
        //                IQueryable<TempQuestionType> tempQuestionTypes = db.TempQuestionType.Where(x => x.ID == Qmodel.QuestionID).AsQueryable();
        //                foreach (TempQuestionType types in tempQuestionTypes)
        //                {
        //                    QuestionType type = new QuestionType();
        //                    type.ID = types.ID;
        //                    type.Name = types.Name;
        //                    type.Description = types.Description;
        //                    db.QuestionType.Add(type);
        //                };


        //                db.SaveChanges();
        //            }
        //            //TempUploads tempUploads = db.TempUploads.Find(uploadID);
        //            //Uploads uploads = new Uploads();
        //            ////uploads.ID = tempUploads.ID;
        //            //uploads.Title = tempUploads.Title;
        //            //uploads.UploadedDate = tempUploads.UploadedDate;
        //            //uploads.UploadedUserID = tempUploads.UploadedUserID;

        //            //db.Uploads.Add(uploads);
        //            //db.SaveChanges();

        //        }

        //        DeleteTempQuestion(uploadID);
        //    }
        //}

        public void DeleteTempQuestion(long uploadID)
        {
            int i = 0;
            int TotalQueationCount = 0;
            bool result = true;
            try
            {
                if (uploadID != 0)
                {
                    //Question question = new Question();
                    TempUploads upload = db.TempUploads.Find(uploadID);
                    IQueryable<TempQuestion> tempQuestions = db.TempQuestion.Where(t => t.UploadID == uploadID);
                    //foreach (TempQuestion tempQuestion in tempQuestions)
                    //{
                    //    if (tempQuestion.TempComplexity != null)
                    //    {
                    //        db.TempComplexity.Remove(tempQuestion.TempComplexity);
                    //    }
                    //    if (tempQuestion.TempPassage != null)
                    //    {
                    //        db.TempPassage.Remove(tempQuestion.TempPassage);
                    //    }
                    //    if (tempQuestion.TempQuestionCategory != null)
                    //    {
                    //        db.TempQuestionCategory.Remove(tempQuestion.TempQuestionCategory);
                    //    }
                    //    if (tempQuestion.TempQuestionType != null)
                    //    {
                    //        db.TempQuestionType.Remove(tempQuestion.TempQuestionType);
                    //    }
                    //    if (tempQuestion.TempSubject != null)
                    //    {
                    //        db.TempSubject.Remove(tempQuestion.TempSubject);
                    //    }
                    //    if (tempQuestion.TempUploads != null)
                    //    {
                    //        db.TempUploads.Remove(tempQuestion.TempUploads);
                    //    }
                    //    if (tempQuestion!= null)
                    //    {
                    //        db.TempQuestion.Remove(tempQuestion);
                    //    }
                    //    db.SaveChanges();




                    TempUploads tempUploads = db.TempUploads.Find(uploadID);
                    IEnumerable<TempQuestion> questionsUnderUpload = tempUploads.TempQuestion;
                    if (questionsUnderUpload.Count() > 0)
                    {
                        List<Passage> passageList = new List<Passage>();

                        db.TempAnswer.RemoveRange(questionsUnderUpload.SelectMany(x => x.TempAnswer));
                        db.TempQuestionTags.RemoveRange(questionsUnderUpload.SelectMany(x => x.TempQuestionTags));
                        db.TempComplexity.RemoveRange(questionsUnderUpload.Where(x => x.TempComplexity != null).Select(x => x.TempComplexity)).AsEnumerable();
                        //db.TempQuestionType.RemoveRange(questionsUnderUpload.Where(x => x.TempQuestionType != null).SelectMany(x => x.TempQuestionType).AsEnumerable());
                        IEnumerable<TempQuestionType> test = questionsUnderUpload.Where(x => x.TempQuestionType != null).Select(y => y.TempQuestionType);
                        db.TempQuestionType.RemoveRange(questionsUnderUpload.Where(x => x.TempQuestionType != null).Select(y => y.TempQuestionType));
                        db.TempSubject.RemoveRange(questionsUnderUpload.Where(x => x.TempSubject != null).Select(x => x.TempSubject)).AsEnumerable();
                        //  db.TempUploads.RemoveRange(questionsUnderUpload.Where(x => x.TempUploads != null).Select(x => x.TempUploads)).AsEnumerable();
                        db.SaveChanges();
                        db.TempQuestion.RemoveRange(questionsUnderUpload).AsEnumerable();
                        db.TempPassage.RemoveRange(questionsUnderUpload.Where(x => x.TempPassage != null).Select(x => x.TempPassage)).AsEnumerable();
                        db.SaveChanges();

                    }







                    //}
                    //Question question = db.Question.Where(x => x.UploadID == upload.ID);
                    db.TempUploads.Remove(upload);
                    db.SaveChanges();
                }
            }
            catch
            {
                result = false;
            }
            string DeleteRatio = i.ToString() + "/" + TotalQueationCount.ToString();
            Response.Write(DeleteRatio);
        }

        //#region Upload Question Temp

        //public ActionResult UploadQuestionsTemp()
        //{
        //    StudentViewModel studentViewModel = new StudentViewModel();
        //    int uploadCount = db.TempUploads.Count() + 1;
        //    studentViewModel.UploadName = "Upload " + uploadCount;
        //    return View(studentViewModel);
        //}

        //[HttpPost]
        //public ActionResult UploadQuestionsTemp(HttpPostedFileBase file, StudentViewModel studentViewModel)
        //{
        //    List<HtmlEntities> htmlEntities = GetHtmlEntities();
        //    string[] notSupportedTags = new string[] { "&rsquo;", " &ldquo;", "&rdquo;", "&lsquo;", " &ndash;", "  &bull;", " &frac;", " &deg;", "&frasl;", "&pi;", " &ang;", " &times;" };
        //    MashUploadResultViewModel uploadResult = new MashUploadResultViewModel();
        //    TempUploads upload = new TempUploads();
        //    upload.Title = studentViewModel.UploadName;
        //    upload.UploadedDate = DateTime.Now;
        //    upload.UploadedUserID = GetCurrentUserID();
        //    db.TempUploads.Add(upload);
        //    db.SaveChanges();
        //    List<PassageViewModel> passageViewModelList = new List<PassageViewModel>();
        //    if (file != null)
        //    {
        //        string guidStirng = Guid.NewGuid().ToString();
        //        //string newPath = Server.MapPath("~/Upload/" + guidStirng + "/" + guidStirng + ".html");
        //        if (!Directory.Exists(Server.MapPath("~/Upload/" + guidStirng)))
        //            Directory.CreateDirectory(Server.MapPath("~/Upload/" + guidStirng));
        //        //if (!Directory.Exists(Server.MapPath("~/Upload/" + guidStirng + "/Images")))
        //        //    Directory.CreateDirectory(Server.MapPath("~/Upload/" + guidStirng + "/Images"));
        //        string path = Server.MapPath("~/Upload/" + guidStirng + "/" + guidStirng + "." + file.FileName.Split('.')[1]);
        //        string htmlPathTest = Server.MapPath("~/Upload/" + guidStirng + "/" + guidStirng + "_Test.html");
        //        file.SaveAs(htmlPathTest);
        //        try
        //        {
        //            file.SaveAs(path);
        //            HelperClasses.Converter converter = new Converter();
        //            converter.ConvertDocxToHtml(path);
        //        }
        //        finally
        //        {
        //            //objWord.Application.Quit(SaveChanges: false);
        //        }

        //        string fileContent = "";
        //        string htmlPath = Server.MapPath("~/Upload/" + guidStirng) + "/" + guidStirng + ".html";
        //        fileContent = htmlConverter.ConvertHtmlToString(htmlPath);

        //        for (int i = 0; i < htmlEntities.Count(); i++)
        //        {
        //            fileContent = fileContent.Replace(htmlEntities[i].HtmlCode, htmlEntities[i].XmlCode);
        //        }

        //        StringBuilder sb = new StringBuilder();
        //        if (!string.IsNullOrEmpty(fileContent))
        //        {
        //            fileContent = htmlConverter.FormatFileContent(fileContent);
        //        }

        //        string oldPath = Server.MapPath("~/Upload/" + guidStirng);
        //        string questionFilePath = Server.MapPath("~/Upload") + "/" + guidStirng + "/" + guidStirng + "." + file.FileName.Split('.')[1];

        //        string azureImageNewPath = AzureHelper.UploadToAzureStorage(questionFilePath, "OAP/Upload/Questions", false);

        //        //passageViewModelList = CreatePassageQuestionsAnswers(fileContent, guidStirng, oldPath, azureImageNewPath);
        //        uploadResult = CreatePassageQuestionsAnswersTemp(fileContent, guidStirng, oldPath, upload.ID, azureImageNewPath, upload);

        //        string deletePath = Server.MapPath("~/Upload/" + guidStirng);
        //        var dir = new DirectoryInfo(deletePath);
        //        dir.Delete(true);

        //        return RedirectToAction("PreviewUploadedQuestionsTemp", new RouteValueDictionary(new { controller = "Upload", action = "PreviewUploadedQuestionsTemp", id = upload.ID }));
        //        //return RedirectToAction("PreviewQuestions", new RouteValueDictionary(new { controller = "Upload", action = "PreviewQuestions", id = upload.ID }));Coment by Sukanya
        //    }
        //    return View(studentViewModel);
        //}
        ////anu  Upload Name Validation
        //public void UploadNameValidation(string UploadName)
        //{
        //    TempUploads tempUploads = db.TempUploads.Where(t => t.Title.ToLower() == UploadName.ToLower()).FirstOrDefault();
        //    if (tempUploads != null)        
        //    {
        //        Response.Write(true);
        //    }
        //    else 
        //    {
        //        Response.Write(false);
        //    }
        //}
        //// end by anu
        //public MashUploadResultViewModel CreatePassageQuestionsAnswersTemp(string fileContent, string guidString, string oldPath, long tempUploadedID, string questionAzurePath, TempUploads upload)
        //{
        //    List<HtmlEntities> htmlEntities = GetHtmlEntities();
        //    MashUploadResultViewModel uploadResult = new MashUploadResultViewModel();
        //    int processPassage = 0;
        //    bool isContinueQuestion = false;
        //    bool isContinueAnswer = false;
        //    bool isNewPassage = true;

        //    TempPassage currentPassage = new TempPassage();
        //    TempQuestion currentQuestion = new TempQuestion();
        //    List<Answer> answers = new List<Answer>();
        //    StringBuilder sb = new StringBuilder();

        //    List<PassageViewModel> passageViewModelList = new List<PassageViewModel>();
        //    List<QuestionViewModel> questionViewModelList = new List<QuestionViewModel>();
        //    PassageViewModel passageViewModel = new PassageViewModel();
        //    QuestionViewModel questionViewModel = new QuestionViewModel();
        //    List<AnswerViewModel> answerViewModelList = new List<AnswerViewModel>();

        //    int passageStartIndex = 0, passageEndIndex = 0;

        //    if (!string.IsNullOrEmpty(fileContent))
        //    {
        //        XDocument document = XDocument.Parse(fileContent);
        //        for (int i = 0; i < htmlEntities.Count(); i++)
        //        {
        //            fileContent = fileContent.Replace(htmlEntities[i].XmlCode, htmlEntities[i].HtmlCode);
        //        }
        //        XElement root = document.Elements().FirstOrDefault().Elements().FirstOrDefault();
        //        root.Elements().FirstOrDefault().Remove();
        //        {
        //            int totalQuestions = root.Elements().Where(e => !(e.Elements().First().Value.Contains("-"))).Count() / 2;
        //            foreach (XElement tr in root.Elements().FirstOrDefault().Elements().FirstOrDefault().Elements())
        //            {
        //                if (tr.Value.Trim() == "")
        //                {
        //                    continue;
        //                }
        //                if (tr.Name.LocalName.ToLower() == "tr")
        //                {
        //                    answers = new List<Answer>();
        //                    List<XElement> columns = tr.Elements().ToList();
        //                    int count = columns.Count;
        //                    var node = (columns[0].Elements().ToList()[0].FirstNode);

        //                    if (processPassage == 0)
        //                    {                               //Processing first line
        //                    //var currentNode = (XElement)node;

        //                    //if (!string.IsNullOrEmpty(currentNode.Value.ToString()))
        //                    //{
        //                    ContinuePassage:
        //                        if (isNewPassage)
        //                        {                                                                   //Questions and answers saved
        //                            if (columns[0].Value.ToString().Contains("-"))
        //                            {                                                               //Having passage
        //                                passageViewModel = new PassageViewModel();
        //                                passageStartIndex = Convert.ToInt16(columns[0].Value.Split('-')[0]);
        //                                passageEndIndex = Convert.ToInt16(columns[0].Value.Split('-')[1]);

        //                                currentPassage = SavePassageTemp(columns, guidString);

        //                                isContinueQuestion = true;
        //                                isNewPassage = false;
        //                                isContinueAnswer = false;
        //                                passageViewModel = MapperHelper.MapToViewModelTemp(currentPassage);
        //                                continue;
        //                            }
        //                            else
        //                            {
        //                                isContinueQuestion = true;
        //                                isNewPassage = false;
        //                                isContinueAnswer = false;
        //                                goto ContinueQuestion;
        //                            }
        //                        }
        //                    ContinueQuestion:
        //                        if (isContinueQuestion)
        //                        {                                                             //Continue to save Question
        //                            questionViewModel = new QuestionViewModel();

        //                            currentQuestion = SaveQuestionTemp(columns, currentPassage, upload, questionAzurePath, guidString, oldPath, passageStartIndex, passageEndIndex);
        //                            questionViewModel = MapperHelper.MapToViewModel(currentQuestion);
        //                            questionViewModelList.Add(questionViewModel);

        //                            isContinueQuestion = false;
        //                            isNewPassage = false;
        //                            isContinueAnswer = true;
        //                            continue;
        //                        }
        //                        if (isContinueAnswer)
        //                        {
        //                            if (string.IsNullOrEmpty(columns[0].Value.ToString()) || columns[0].Value.Trim().ToString().ToLower() == "c" || columns[0].Value.ToString() == " ")
        //                            {                                                                                                   //Answer sections
        //                                var reader = columns[1].CreateReader();
        //                                reader.MoveToContent();
        //                                string answerDescription = reader.ReadInnerXml();

        //                                TempQuestionType questionType;
        //                                if (string.IsNullOrWhiteSpace(answerDescription))
        //                                {                                       //Question has no answers . Question type is OpenEndedQuestion
        //                                    questionType = db.TempQuestionType.Where(qt => qt.Name.ToLower().Contains("open ended")).FirstOrDefault();
        //                                    currentQuestion.TempQuestionType = questionType;
        //                                    db.SaveChanges();
        //                                    continue;
        //                                }
        //                                else
        //                                {                                       //Question have answers. 
        //                                    AnswerViewModel answerViewModel = new AnswerViewModel();
        //                                    answerViewModel = AddAnswerForQuestionTemp(columns, currentQuestion, guidString, oldPath);
        //                                    answerViewModelList.Add(answerViewModel);
        //                                    continue;
        //                                }
        //                            }
        //                            else if (columns[0].Value.Trim().ToString().ToLower() == "s")
        //                            {                                                                   //Solution for the question
        //                                AddSolutionForQuestionTemp(columns, currentQuestion);
        //                                continue;
        //                            }
        //                            else if (columns[0].Value.Trim().ToString().ToLower() == "t")
        //                            {                                                                           //Column containing tags
        //                                CreateQuestionTagsTemp(columns, currentQuestion);
        //                                continue;
        //                            }
        //                            else
        //                            {                                                                       //Save answer completed. Continue to New passage or new question
        //                                if (currentQuestion.ID != 0 && currentQuestion.TempQuestionType == null)
        //                                    UpdateQuestionTypeTemp(currentQuestion);
        //                                isContinueQuestion = false;
        //                                isNewPassage = true;
        //                                isContinueAnswer = false;
        //                                goto ContinuePassage;
        //                                //if (columns[0].Value.ToString().Contains("-"))
        //                                //{                                                               //Having passage

        //                                //    isContinueQuestion = false;
        //                                //    isNewPassage = true;
        //                                //    isContinueAnswer = false;
        //                                //    goto ContinuePassage;
        //                                //}
        //                                //else
        //                                //{
        //                                //    passageViewModel = MapperHelper.MapToViewModel(currentPassage);
        //                                //    passageViewModelList.Add(passageViewModel);
        //                                //    isContinueQuestion = true;
        //                                //    isNewPassage = false;
        //                                //    isContinueAnswer = false;
        //                                //    goto ContinueQuestion;
        //                                //    //continue;
        //                                //}
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    UpdateQuestionTypeTemp(currentQuestion);

        //    string questionIDs = "";
        //    foreach (QuestionViewModel question in questionViewModelList)
        //    {
        //        question.AnswerViewModelList = answerViewModelList.Where(a => a.QuestionID == question.ID).ToList();
        //        questionIDs += question.ID + ",";
        //    }
        //    uploadResult.questionViewModelList = questionViewModelList;
        //    uploadResult.UploadedQuestionIDs = questionIDs;
        //    return uploadResult;
        //    //return passageViewModelList;
        //}

        //public List<HtmlEntities> GetHtmlEntities()
        //{
        //    List<HtmlEntities> htmlEntities = new List<HtmlEntities>();
        //    htmlEntities.Add(new HtmlEntities("&rsquo;", "&#8217;"));
        //    htmlEntities.Add(new HtmlEntities("&ldquo;", "&#8220;"));
        //    htmlEntities.Add(new HtmlEntities("&rdquo;", "&#8221;"));
        //    htmlEntities.Add(new HtmlEntities("&lsquo;", "&#8216;"));
        //    htmlEntities.Add(new HtmlEntities("&ndash;", "&#8211;"));
        //    htmlEntities.Add(new HtmlEntities("&bull;", "&#8226;"));
        //    // htmlEntities.Add(new HtmlEntities("&frac;", ""));
        //    htmlEntities.Add(new HtmlEntities("&deg;", "&#176;"));
        //    htmlEntities.Add(new HtmlEntities("&frasl;", "&#8260;"));
        //    htmlEntities.Add(new HtmlEntities("&pi;", "&#928;"));
        //    htmlEntities.Add(new HtmlEntities("&ang;", "&#8736;"));
        //    htmlEntities.Add(new HtmlEntities("&times;", "&#215;"));
        //    htmlEntities.Add(new HtmlEntities("&hellip", "&#8230;"));
        //    htmlEntities.Add(new HtmlEntities("&pound;", "&#163;"));
        //    htmlEntities.Add(new HtmlEntities("&radic;", "&#8730;"));
        //    htmlEntities.Add(new HtmlEntities("&loz;", "&#9674;"));
        //    htmlEntities.Add(new HtmlEntities("&yen;", "&#165;"));
        //    htmlEntities.Add(new HtmlEntities("&infin;", "&#8734;"));
        //    htmlEntities.Add(new HtmlEntities("&forall;", "&#8704;"));
        //    htmlEntities.Add(new HtmlEntities("&part;", "&#8706;"));
        //    htmlEntities.Add(new HtmlEntities("&ordm;", "&#186;"));
        //    htmlEntities.Add(new HtmlEntities("&fnof;", "&#402;"));
        //    htmlEntities.Add(new HtmlEntities("&lambda;", "&#955;"));
        //    //////////////////////////////////////////////
        //    htmlEntities.Add(new HtmlEntities("&minus;", "&#8722;"));
        //    htmlEntities.Add(new HtmlEntities("&times;", "&#215;"));
        //    htmlEntities.Add(new HtmlEntities("&divide;", "&#247;"));
        //    htmlEntities.Add(new HtmlEntities("&ne;", "&#8800;"));
        //    htmlEntities.Add(new HtmlEntities("&asymp;", "&#8776;"));
        //    htmlEntities.Add(new HtmlEntities("&lt;", "&#60;"));
        //    htmlEntities.Add(new HtmlEntities("&le;", "&#8804;"));
        //    htmlEntities.Add(new HtmlEntities("&gt;", "&#62;"));
        //    htmlEntities.Add(new HtmlEntities("&ge;", "&#8805;"));
        //    htmlEntities.Add(new HtmlEntities("&plusmn;", "&#177;"));
        //    htmlEntities.Add(new HtmlEntities("&prop;", "&#8733;"));
        //    htmlEntities.Add(new HtmlEntities("&sum;", "&#8721;"));
        //    htmlEntities.Add(new HtmlEntities("&prod;", "&#8719;"));
        //    htmlEntities.Add(new HtmlEntities("&lfloor;", "&#8970;"));
        //    htmlEntities.Add(new HtmlEntities("&rfloor;", "&#8971;"));
        //    htmlEntities.Add(new HtmlEntities("&lceil;", "&#8968;"));
        //    htmlEntities.Add(new HtmlEntities("&isin;", "&#8712;"));
        //    htmlEntities.Add(new HtmlEntities("&notin;", "&#8713;"));
        //    htmlEntities.Add(new HtmlEntities("&rceil;", "&#8969;"));
        //    htmlEntities.Add(new HtmlEntities("&ni;", "&#8715;"));
        //    htmlEntities.Add(new HtmlEntities("&int;", "&#8747;"));
        //    htmlEntities.Add(new HtmlEntities("&larr;", "&#8592;"));
        //    htmlEntities.Add(new HtmlEntities("&rarr;", "&#8594;"));
        //    htmlEntities.Add(new HtmlEntities("&pi;", "&#960;"));
        //    htmlEntities.Add(new HtmlEntities("&Sigma;", "&#931;"));
        //    htmlEntities.Add(new HtmlEntities("&rarr;", "&#8594;"));
        //    htmlEntities.Add(new HtmlEntities("&circ;", "&#710;"));
        //    htmlEntities.Add(new HtmlEntities("&nabla;", "&#8711;"));
        //    htmlEntities.Add(new HtmlEntities("&Delta;", "&#916;"));
        //    htmlEntities.Add(new HtmlEntities("&alpha;", "&#945;"));
        //    htmlEntities.Add(new HtmlEntities("&beta;", "&#946;"));
        //    htmlEntities.Add(new HtmlEntities("&gamma;", "&#947;"));
        //    htmlEntities.Add(new HtmlEntities("&delta;", "&#948;"));
        //    htmlEntities.Add(new HtmlEntities("&theta;", "&#952;"));
        //    htmlEntities.Add(new HtmlEntities("&Omega;", "&#937;"));
        //    htmlEntities.Add(new HtmlEntities("&omega;", "&#969;"));
        //    htmlEntities.Add(new HtmlEntities("&phi;", "&#966;"));
        //    htmlEntities.Add(new HtmlEntities("&thetasym;", "&#977;"));
        //    htmlEntities.Add(new HtmlEntities("&rho;", "&#961;"));
        //    htmlEntities.Add(new HtmlEntities("&equiv;", "&#8801;"));
        //    htmlEntities.Add(new HtmlEntities("&epsilon;", "&#949;"));
        //    htmlEntities.Add(new HtmlEntities("&mu;", "&#956;"));
        //    htmlEntities.Add(new HtmlEntities("&deg;", "&#176;"));
        //    htmlEntities.Add(new HtmlEntities("&sup2;", "&#178;"));
        //    htmlEntities.Add(new HtmlEntities("&laquo;", "&#171;"));
        //    htmlEntities.Add(new HtmlEntities("&tilde;", "&#732;"));
        //    htmlEntities.Add(new HtmlEntities("&eta;", "&#951;"));
        //    htmlEntities.Add(new HtmlEntities("&harr;", "&#8596;"));
        //    htmlEntities.Add(new HtmlEntities("&hArr;", "&#8660;"));
        //    htmlEntities.Add(new HtmlEntities("&ang;", "&#8736;"));
        //    htmlEntities.Add(new HtmlEntities("&and;", "&#8743;"));
        //    htmlEntities.Add(new HtmlEntities("&or;", "&#8744;"));
        //    htmlEntities.Add(new HtmlEntities("&cap;", "&#8745;"));
        //    htmlEntities.Add(new HtmlEntities("&cup;", "&#8746;"));
        //    htmlEntities.Add(new HtmlEntities("&sim;", "&#8764;"));
        //    htmlEntities.Add(new HtmlEntities("&cong;", "&#8773;"));
        //    htmlEntities.Add(new HtmlEntities("&sub;", "&#8834;"));
        //    htmlEntities.Add(new HtmlEntities("&sup;", "&#8835;"));
        //    htmlEntities.Add(new HtmlEntities("&nsub;", "&#8836;"));
        //    htmlEntities.Add(new HtmlEntities("&sube;", "&#8838;"));
        //    htmlEntities.Add(new HtmlEntities("&supe;", "&#8839;"));
        //    htmlEntities.Add(new HtmlEntities("&psi;", "&#968;"));         
        //    htmlEntities.Add(new HtmlEntities("&micro;", "&#181;"));
        //    htmlEntities.Add(new HtmlEntities("&para;", "&#182;"));
        //    htmlEntities.Add(new HtmlEntities("&eta;", "&#951;"));
        //    return htmlEntities;

        //}

        //public TempPassage SavePassageTemp(List<XElement> columns, string guidString)
        //{
        //    var reader = columns[1].CreateReader();
        //    reader.MoveToContent();
        //    string passageString = reader.ReadInnerXml();

        //    TempPassage currentPassage = new TempPassage();
        //    currentPassage.CanShuffle = true;
        //    currentPassage.CreatedDate = DateTime.Now;
        //    currentPassage.CreatedBy = currentPassage.ModifiedBy = GetCurrentUserID();
        //    currentPassage.ModifiedDate = DateTime.Now;
        //    currentPassage.LastUpdated = DateTime.Now;

        //    db.TempPassage.Add(currentPassage);
        //    db.SaveChanges();

        //    List<string> imageSourceStringList = new List<string>();
        //    foreach (var element in columns[1].Elements())
        //    {
        //        if (element.ToString().Contains("img"))
        //        {                                                                                       //Question having image
        //            var imageNodes = columns[1].Descendants("img");
        //            var newImageNodes = columns[1].Descendants("img").ToList();
        //            string actualSource = Server.MapPath("~/Upload/") + guidString + "/" + guidString + "_files";
        //            int processing = 0;
        //            foreach (var item in imageNodes)
        //            {
        //                string imageSource = item.Attribute("src").Value;
        //                int totalCount = imageSource.Split('/').Count();
        //                string imageName = imageSource.Split('/')[totalCount - 1];
        //                string destinationPath = "OAP/Upload/Passage" + currentPassage.ID;
        //                string filePath = actualSource + "/" + imageName;
        //                string azureImageNewPath = AzureHelper.UploadToAzureStorage(filePath, destinationPath, true);
        //                newImageNodes[0].Attribute("src").Value = azureImageNewPath;
        //                processing++;
        //                imageSourceStringList.Add(azureImageNewPath);
        //            }
        //        }
        //    }
        //    foreach (string newImageSource in imageSourceStringList)
        //    {
        //        string searchString = guidString + "_files";
        //        int place = passageString.IndexOf(searchString);

        //        if (place > 0 && !string.IsNullOrEmpty(newImageSource))
        //        {
        //            List<string> source = newImageSource.Split('/').ToList();
        //            source.RemoveAt(source.Count() - 1);
        //            string alteredImageSource = string.Join("/", source.ToArray());
        //            passageString = passageString.Remove(place, searchString.Length).Insert(place, alteredImageSource);
        //        }
        //    }
        //    currentPassage.Description = passageString;

        //    currentPassage.Description = htmlConverter.FormatFileContent(currentPassage.Description);
        //    db.Entry(currentPassage).State = EntityState.Modified;
        //    db.SaveChanges();
        //    return currentPassage;
        //}

        //public TempQuestion SaveQuestionTemp(List<XElement> columns, TempPassage currentPassage, TempUploads upload, string questionAzurePath, string guidString, string oldPath, int passageStartIndex, int passageEndIndex)
        //{
        //    TempQuestion currentQuestion;

        //    int questionNumber = Convert.ToInt16(columns[0].Value.ToString());
        //    string questionString = "";
        //    var reader = columns[1].CreateReader();
        //    reader.MoveToContent();
        //    questionString = reader.ReadInnerXml();

        //    currentQuestion = new TempQuestion();
        //    if (questionNumber >= passageStartIndex && questionNumber <= passageEndIndex)
        //        currentQuestion.TempPassage = currentPassage;
        //    currentQuestion.Description = questionString;
        //    currentQuestion.TempUploads = upload;
        //    currentQuestion.LastUpdated = DateTime.Now;
        //    currentQuestion.AdminId = GetCurrentUserID();
        //    currentQuestion.DownloadLink = questionAzurePath;
        //    currentQuestion.AdminId = GetCurrentUserID();

        //    db.TempQuestion.Add(currentQuestion);
        //    db.SaveChanges();

        //    List<string> imageSourceStringList = new List<string>();
        //    foreach (var element in columns[1].Elements())
        //    {
        //        if (element.ToString().Contains("img"))
        //        {                                                                                       //Question having image
        //            var imageNodes = columns[1].Elements().FirstOrDefault().Elements().ToList();
        //            string actualSource = Server.MapPath("~/Upload/") + guidString + "/" + guidString + "_files";
        //            int processing = 0;
        //            var tempElement = imageNodes;
        //            int i = 0;
        //            foreach (var item in imageNodes)
        //            {
        //                if (item.ToString().Contains("img"))
        //                {
        //                    string imageSource = item.Attribute("src").Value;
        //                    int totalCount = imageSource.Split('/').Count();
        //                    string imageName = imageSource.Split('/')[totalCount - 1];
        //                    string destinationPath = "/Upload/Question/Images/" + currentQuestion.ID;
        //                    string filePath = actualSource + "/" + imageName;
        //                    string azureImageNewPath = AzureHelper.UploadToAzureStorage(filePath, destinationPath, true);
        //                    processing++;
        //                    imageSourceStringList.Add(azureImageNewPath);
        //                    i++;
        //                }
        //            }
        //        }
        //    }
        //    foreach (string newImageSource in imageSourceStringList)
        //    {
        //        //oldPath
        //        //string searchString = guidString + "_files";
        //        string searchString = oldPath + @"\" + guidString + "_files";
        //        searchString = searchString.Replace(@"\\", @"\");
        //        int place = questionString.IndexOf(searchString);

        //        if (place > 0 && !string.IsNullOrEmpty(newImageSource))
        //        {
        //            List<string> source = newImageSource.Split('/').ToList();
        //            source.RemoveAt(source.Count() - 1);
        //            string alteredImageSource = string.Join("/", source.ToArray());

        //            questionString = questionString.Remove(place, searchString.Length).Insert(place, alteredImageSource);
        //        }
        //    }

        //    questionString = htmlConverter.FormatFileContent(questionString);
        //    currentQuestion.Description = questionString;
        //    currentQuestion.LastUpdated = DateTime.Now;
        //    currentQuestion.AdminId = GetCurrentUserID();
        //    currentPassage.Description = htmlConverter.FormatFileContent(currentPassage.Description);
        //    db.Entry(currentQuestion).State = EntityState.Modified;
        //    db.SaveChanges();
        //    return currentQuestion;
        //}

        //public AnswerViewModel AddAnswerForQuestionTemp(List<XElement> columns, TempQuestion currentQuestion, string guidString, string oldPath)
        //{
        //    HtmlFormatter htmlConverter = new HtmlFormatter();
        //    TempAnswer answer = new TempAnswer();
        //    string answerDescription = "";

        //    var reader = columns[1].CreateReader();
        //    reader.MoveToContent();
        //    answerDescription = reader.ReadInnerXml();

        //    answer.Description = answerDescription;

        //    if (columns[0].Value.ToString().ToLower() == "c")
        //    {
        //        answer.IsCorrect = true;
        //    }
        //    else
        //        answer.IsCorrect = false;
        //    answer.TempQuestion = currentQuestion;
        //    answer.LastUpdated = DateTime.Now;
        //    answer.Description = htmlConverter.FormatFileContent(answer.Description);

        //    db.TempAnswer.Add(answer);
        //    db.SaveChanges();

        //    List<string> imageSourceStringList = new List<string>();
        //    foreach (var element in columns[1].Elements())
        //    {
        //        if (element.ToString().Contains("img"))
        //        {                                                                                       //Question having image
        //            var imageNodes = columns[1].Elements().FirstOrDefault().Elements().ToList();
        //            string actualSource = Server.MapPath("~/Upload/") + guidString + "/" + guidString + "_files";
        //            int processing = 0;
        //            var tempElement = imageNodes;
        //            int i = 0;
        //            foreach (var item in imageNodes)
        //            {
        //                if (item.ToString().Contains("img"))
        //                {
        //                    string imageSource = item.Attribute("src").Value;
        //                    int totalCount = imageSource.Split('/').Count();
        //                    string imageName = imageSource.Split('/')[totalCount - 1];
        //                    string destinationPath = "/Upload/Answer/Images/" + currentQuestion.ID;
        //                    string filePath = actualSource + "/" + imageName;
        //                    string azureImageNewPath = AzureHelper.UploadToAzureStorage(filePath, destinationPath, true);
        //                    processing++;
        //                    imageSourceStringList.Add(azureImageNewPath);
        //                    i++;
        //                }
        //            }
        //        }
        //    }
        //    foreach (string newImageSource in imageSourceStringList)
        //    {
        //        string searchString = oldPath + @"\" + guidString + "_files";
        //        searchString = searchString.Replace(@"\\", @"\");
        //        int place = answerDescription.IndexOf(searchString);

        //        if (place > 0 && !string.IsNullOrEmpty(newImageSource))
        //        {
        //            List<string> source = newImageSource.Split('/').ToList();
        //            source.RemoveAt(source.Count() - 1);
        //            string alteredImageSource = string.Join("/", source.ToArray());

        //            answerDescription = answerDescription.Remove(place, searchString.Length).Insert(place, alteredImageSource);
        //        }
        //    }

        //    answer.Description = answerDescription;
        //    answer.Description = htmlConverter.FormatFileContent(answer.Description);

        //    db.Entry(answer).State = EntityState.Modified;
        //    db.SaveChanges();

        //    AnswerViewModel answerViewModel = new AnswerViewModel();
        //    answerViewModel = MapperHelper.MapToViewModelTemp(answer);
        //    return answerViewModel;
        //}

        //public void AddSolutionForQuestionTemp(List<XElement> columns, TempQuestion currentQuestion)
        //{
        //    if (!String.IsNullOrEmpty(columns[1].Value.ToString()))
        //    {
        //        string solutionString = "";
        //        //foreach (var element in columns[1].Elements())
        //        //{
        //        //    solutionString = solutionString + element.ToString();
        //        //}

        //        var reader = columns[1].CreateReader();
        //        reader.MoveToContent();
        //        solutionString = reader.ReadInnerXml();
        //        currentQuestion.Solution = solutionString;
        //        currentQuestion.AdminId = GetCurrentUserID();
        //        currentQuestion.LastUpdated = DateTime.Now;
        //        currentQuestion.Description = htmlConverter.FormatFileContent(currentQuestion.Description);
        //        db.Entry(currentQuestion).State = EntityState.Modified;
        //        db.SaveChanges();
        //    }
        //}

        //public void CreateQuestionTagsTemp(List<XElement> columns, TempQuestion currentQuestion)
        //{
        //    if (!string.IsNullOrEmpty(columns[1].Value.ToString()))
        //    {
        //        string tagString = columns[1].Value.ToString();
        //        List<string> tagList = tagString.Split(',').ToList();
        //        foreach (string newTag in tagList)
        //        {
        //            TempTag tags = db.TempTag.Where(t => t.Name.ToLower() == newTag.ToLower()).FirstOrDefault();
        //            if (tags == null)
        //            {
        //                tags = new TempTag();
        //                tags.Name = newTag;
        //                db.TempTag.Add(tags);
        //                db.SaveChanges();
        //            }
        //            if (tags != null)
        //            {
        //                TempQuestionTags questionTags = new TempQuestionTags();
        //                questionTags.TempQuestion = currentQuestion;
        //                questionTags.TempTag = tags;
        //                questionTags.CreatedBy = GetCurrentUserID();
        //                questionTags.CreatedDate = DateTime.Now;
        //                db.TempQuestionTags.Add(questionTags);
        //            }
        //        }
        //        db.SaveChanges();
        //    }
        //}

        //public void UpdateQuestionTypeTemp(TempQuestion currentQuestion)
        //{
        //    TempQuestionType questionType;
        //    if (currentQuestion.TempAnswer.Where(a => a.IsCorrect ?? false).Count() > 1)
        //    {                                                                           //Question have multiple answers
        //        questionType = db.TempQuestionType.Where(qt => qt.Name.ToLower().Contains("multi select")).FirstOrDefault();
        //    }
        //    else
        //    {                                                                           //Qustion have only one answer
        //        questionType = db.TempQuestionType.Where(qt => qt.Name.ToLower().Contains("single select")).FirstOrDefault();

        //    }
        //    if (questionType != null)
        //    {
        //        currentQuestion.TempQuestionType = questionType;
        //        currentQuestion.Description = htmlConverter.FormatFileContent(currentQuestion.Description);
        //        db.Entry(currentQuestion).State = EntityState.Modified;
        //        db.SaveChanges();
        //    }
        //}

        //#endregion

        #region Preview Uploaded Questions Temp

        //public ActionResult PreviewUploadedQuestionsTemp(long id = 0)
        //{
        //    TempUploads upload = db.TempUploads.Find(id);
        //    if (upload == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    UploadQuestionViewModel uploadQuestionViewModel = new UploadQuestionViewModel();
        //    List<QuestionViewModel> questionsUnderUpload = MapperHelper.MapToViewModelListToShowFullQuestionTemp(upload.TempQuestion.AsQueryable());
        //    if (questionsUnderUpload.Count() > 0)
        //    {
        //        uploadQuestionViewModel.UploadedQuestions = questionsUnderUpload;
        //        uploadQuestionViewModel.QuestionViewModel = questionsUnderUpload.FirstOrDefault();
        //        uploadQuestionViewModel.QuestionViewModel.Description = uploadQuestionViewModel.QuestionViewModel.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                                                                                     .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                                                                                     .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                                                                                     .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]","α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                                                                                     .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]","ρ").Replace("[equiv]","≡")
        //                                                                                                                     .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                                                                                     .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                                                                                     .Replace("[cong]","≅").Replace("[sub]","⊂").Replace("[sup]","⊃").Replace("[nsub]","⊄").Replace("[sube]","⊆").Replace("[supe]","⊇").Replace("[Psi]","Ψ")
        //                                                                                                                     .Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯").Replace("[Graphemica]", "⪔")
        //                                                                                                                     .Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η");
        //        foreach (AnswerViewModel answer in uploadQuestionViewModel.QuestionViewModel.AnswerViewModelList)
        //        {
        //            answer.Description = answer.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                   .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                   .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                   .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                   .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                   .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                   .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                   .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ")
        //                                                   .Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯").Replace("[Graphemica]", "⪔")
        //                                                   .Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η");
        //        }
        //        uploadQuestionViewModel.QuestionViewModel.CorrectAnswerID = uploadQuestionViewModel.QuestionViewModel.AnswerViewModelList.Where(a => a.isCorrect).Select(a => a.ID).FirstOrDefault();
        //        uploadQuestionViewModel.CurrentIndex = 0;
        //        uploadQuestionViewModel.TotalQuestions = questionsUnderUpload.Count();
        //        uploadQuestionViewModel.SelectedIndex = 0;
        //        uploadQuestionViewModel.Marks = Convert.ToInt32(uploadQuestionViewModel.QuestionViewModel.Marks);
        //    }
        //    uploadQuestionViewModel.UploadedQuestions = questionsUnderUpload;

        //    //uploadQuestionViewModel.Marks = Convert.ToInt32(uploadQuestionViewModel.QuestionViewModel.Marks);

        //    return View(uploadQuestionViewModel);
        //}

        //[HttpPost]
        //[ValidateInput(false)]
        //public ActionResult PreviewUploadedQuestionsTemp(UploadQuestionViewModel uploadQuestionViewModel, string selIndex)
        //{
        //    List<TempQuestion> uploadedQuestions = db.TempQuestion.Where(q => q.UploadID != null && q.UploadID == uploadQuestionViewModel.ID).ToList();
        //    List<QuestionViewModel> questionViewModelList = MapperHelper.MapToViewModelListToShowFullQuestionTemp(uploadedQuestions.AsQueryable());
        //    TempQuestion questionToSaved = uploadedQuestions[uploadQuestionViewModel.CurrentIndex];
        //    questionToSaved = MapperHelper.MapToDomainObjectTemp(db, uploadQuestionViewModel.QuestionViewModel, questionToSaved);
        //    questionToSaved.Marks = uploadQuestionViewModel.Marks;
        //    questionToSaved.NegativeMarks = 0;
        //    if (questionToSaved.ComplexityID == 0)
        //    {
        //        questionToSaved.ComplexityID = null;
        //    }
        //    if (questionToSaved.SubjectID == 0)
        //    {
        //        questionToSaved.SubjectID = null;
        //    }
        //    if (uploadQuestionViewModel.QuestionViewModel.QuestionType != null)
        //    {
        //        if (uploadQuestionViewModel.QuestionViewModel.QuestionType.ToLower().Equals("open ended"))
        //        {
        //            questionToSaved.Solution = uploadQuestionViewModel.QuestionViewModel.Solution;
        //        }
        //    }
        //    db.Entry(questionToSaved).State = EntityState.Modified;
        //    db.SaveChanges();

        //    TempAnswer answer;
        //    if (uploadQuestionViewModel.QuestionViewModel.AnswerViewModelList != null)
        //    {
        //        if (uploadQuestionViewModel.QuestionViewModel.QuestionType != null)
        //        {
        //            if (uploadQuestionViewModel.QuestionViewModel.QuestionType.ToLower().Equals("single select"))
        //            {                                                                                   //Question type is single select
        //                foreach (AnswerViewModel answerViewModel in uploadQuestionViewModel.QuestionViewModel.AnswerViewModelList)
        //                {
        //                    answer = db.TempAnswer.Find(answerViewModel.ID);
        //                    answer.Description = answerViewModel.Description;
        //                    answer.Description = answer.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                           .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                           .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                           .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                           .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                           .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                           .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                           .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[drachma]", "₯").Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ")
        //                                                           .Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯").Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η");
        //                    answer.IsCorrect = uploadQuestionViewModel.QuestionViewModel.CorrectAnswerID == answerViewModel.ID ? true : false;
        //                    answer.LastUpdated = DateTime.Now;
        //                    answer.Description = htmlConverter.FormatFileContent(answer.Description);
        //                    db.Entry(answer).State = EntityState.Modified;
        //                }
        //            }

        //            else if (uploadQuestionViewModel.QuestionViewModel.QuestionType.ToLower().Equals("multi select"))
        //            {                                                                               //Question type is multiselect
        //                foreach (AnswerViewModel answerViewModel in uploadQuestionViewModel.QuestionViewModel.AnswerViewModelList)
        //                {
        //                    answer = db.TempAnswer.Find(answerViewModel.ID);
        //                    answer.Description = answerViewModel.Description;
        //                    answer.Description = answer.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                           .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                           .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                           .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                           .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                           .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                           .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                           .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆")
        //                                                           .Replace("[supe]", "⊇").Replace("[Psi]", "Ψ").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯").Replace("[Graphemica]", "⪔")
        //                                                           .Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η");

        //                    answer.IsCorrect = answerViewModel.isCorrect;
        //                    answer.LastUpdated = DateTime.Now;
        //                    answer.Description = htmlConverter.FormatFileContent(answer.Description);
        //                    db.Entry(answer).State = EntityState.Modified;
        //                }
        //                //db.SaveChanges();
        //            }
        //        }

        //        db.SaveChanges();
        //    }
        //    if (uploadQuestionViewModel.QuestionViewModel.QuestionPassage != null && uploadQuestionViewModel.QuestionViewModel.QuestionPassage.ID != 0)
        //    {
        //        TempPassage passage = db.TempPassage.Find(uploadQuestionViewModel.QuestionViewModel.QuestionPassage.ID);
        //        passage = MapperHelper.MapToDomainObjectTemp(db, uploadQuestionViewModel.QuestionViewModel.QuestionPassage, passage);
        //        passage.LastUpdated = DateTime.Now;
        //        passage.ModifiedDate = DateTime.Now;
        //        passage.ModifiedBy = GetCurrentUserID();
        //        passage.Description = htmlConverter.FormatFileContent(passage.Description);
        //        db.Entry(passage).State = EntityState.Modified;
        //        db.SaveChanges();
        //    }

        //    if (selIndex.ToLower() == "finish")
        //    {
        //        return RedirectToAction("IndexTemp");
        //    }

        //    int selectedIndex = Convert.ToInt32(selIndex);
        //    TempQuestion questionToLoad = questionToSaved;
        //    questionToLoad = uploadedQuestions[selectedIndex];
        //    ModelState.Clear(); //Required for array of items update. Otherwise overridden with first item in array. CHECK


        //    uploadQuestionViewModel.QuestionViewModel = MapperHelper.MapToViewModelToShowFullQuestionTemp(questionToLoad);
        //    uploadQuestionViewModel.QuestionViewModel.CorrectAnswerID = uploadQuestionViewModel.QuestionViewModel.AnswerViewModelList.Where(a => a.isCorrect).Select(a => a.ID).FirstOrDefault();

        //    uploadQuestionViewModel.Marks = Convert.ToInt32(uploadQuestionViewModel.QuestionViewModel.Marks);

        //    uploadQuestionViewModel.CurrentIndex = uploadQuestionViewModel.SelectedIndex = selectedIndex;
        //    uploadQuestionViewModel.TotalQuestions = questionViewModelList.Count();
        //    uploadQuestionViewModel.UploadedQuestions = questionViewModelList;
        //    return View(uploadQuestionViewModel);
        //}

        public ActionResult PreviewQuestions(long id = 0)
        {
            ViewBag.UploadID = id;
            return View();
        }

        public void GetPreviewQuestions(long ID = 0)
        {
            datatableRequest = new DataTableRequest(Request, columns);
            UploadQuestionViewModel uploadQuestionViewModel = new UploadQuestionViewModel();
            List<QuestionViewModel> questionViewModelList = new List<QuestionViewModel>();

            IQueryable<TempQuestion> tempQuestions = db.TempUploads.Find(ID).TempQuestion.AsQueryable();
            if (tempQuestions != null)
            {
                int totalRecords = tempQuestions.Count();
                int filteredCount = totalRecords;
                if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
                {
                    tempQuestions = (from u in tempQuestions where u.Description.Contains(datatableRequest.SearchString) select u);
                    filteredCount = tempQuestions.Count();
                }
                tempQuestions = tempQuestions.OrderBy(x => x.Description);
                //if (datatableRequest.ShouldOrder)
                //{
                //    tempQuestions = tempQuestions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
                //}
                foreach (var question in tempQuestions)
                {
                    QuestionViewModel questionViewModel = new QuestionViewModel();
                    questionViewModel.Description = question.Description;
                    questionViewModel.Description = questionViewModel.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                 .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                 .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                 .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                 .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                 .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                 .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ");
                    questionViewModel.ID = question.ID;
                    IQueryable<TempAnswer> answerList = question.TempAnswer.AsQueryable();
                    List<AnswerViewModel> answerViewModelList = MapperHelper.MapToViewModelListTemp(answerList);
                    if (question.TempPassage != null)
                    {
                        questionViewModel.Passage = question.TempPassage.Description;
                        questionViewModel.PassageID = question.TempPassage.ID;
                    }
                    questionViewModel.AnswerViewModelList = answerViewModelList;
                    questionViewModelList.Add(questionViewModel);

                    // IQueryable<TempAnswer> tempAnswers = db.TempAnswer.Where(x => x.QuestionID == question.ID).AsQueryable();


                }


                // TempUploads tempUploads = db.TempUploads.Where(x => x.QuestionID == ID).FirstOrDefault();
                IQueryable<QuestionViewModel> questionViewModels = questionViewModelList.AsQueryable();
                questionViewModelList = questionViewModels.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired).ToList();
                var technicalQuestionViewModelList = new List<Domain.ViewModel.TechnicalQuestionViewModel>();

                QuestionDataTableViewModel dataTableViewModel = new QuestionDataTableViewModel(questionViewModelList,technicalQuestionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);
                string result = JsonConvert.SerializeObject(dataTableViewModel);

                Response.Write(result);
            }
        }
        public void GetAnswer(long id = 0) //Code for pop up of answers
        {
            //long ID = Convert.ToInt64(id);
            AnswerViewModel answerViewModel = new AnswerViewModel();
            IQueryable<TempAnswer> AnswerList = db.TempAnswer.Where(x => x.QuestionID == id).AsQueryable();
            List<AnswerViewModel> answerViewModelList = MapperHelper.MapToViewModelListTemp(AnswerList);
            Response.Write(JsonConvert.SerializeObject(answerViewModelList));
        }

        public ActionResult QuestionEdit(long QuestionID = 0)
        {
            TempQuestion tempQuestions = db.TempQuestion.Find(QuestionID);

            QuestionViewModel questionViewModel = new QuestionViewModel();
            questionViewModel.Description = tempQuestions.Description;
            questionViewModel.ID = tempQuestions.ID;
            IQueryable<TempAnswer> answerList = tempQuestions.TempAnswer.AsQueryable();
            List<AnswerViewModel> answerViewModelList = MapperHelper.MapToViewModelListTemp(answerList);
            questionViewModel.Passage = tempQuestions.TempPassage.Description;
            questionViewModel.PassageID = tempQuestions.TempPassage.ID;
            questionViewModel.AnswerViewModelList = answerViewModelList;

            return View(questionViewModel);
        }
        public void EditUploadedQuestions(long QuestionID = 0)
        {
            UploadQuestionViewModel uploadQuestionViewModel = new UploadQuestionViewModel();
            //List<QuestionViewModel> questionViewModelList = new List<QuestionViewModel>();
            //  IQueryable<TempQuestion> tempQuestions = db.TempUploads.Find(QuestionID ).TempQuestion.AsQueryable();
            TempQuestion tempQuestions = db.TempQuestion.Find(QuestionID);


            QuestionViewModel questionViewModel = new QuestionViewModel();
            questionViewModel.Description = tempQuestions.Description;
            questionViewModel.ID = tempQuestions.ID;
            IQueryable<TempAnswer> answerList = tempQuestions.TempAnswer.AsQueryable();
            List<AnswerViewModel> answerViewModelList = MapperHelper.MapToViewModelListTemp(answerList);
            if (questionViewModel.Passage != null)
            {
                questionViewModel.Passage = tempQuestions.TempPassage.Description;
                questionViewModel.PassageID = tempQuestions.TempPassage.ID;
            }
            questionViewModel.AnswerViewModelList = answerViewModelList;
            //TempQuestionType qtype = db.TempQuestionType.Where(x => x.TempQuestion == tempQuestions).FirstOrDefault();
            //TempQuestionType qtype = tempQuestions.TempQuestionType.Name;
            if (questionViewModel.QuestionType != null)
            {
                questionViewModel.QuestionType = tempQuestions.TempQuestionType.Name;
            }
            //return View("~/Views/Upload/QuestionEdit.cshtml", questionViewModel);
            //string result = RenderPartialViewToString("~/Views/Upload/PreviewUploadedQuestionsTemp.cshtml", questionViewModel);//coment by sukanya
            string result = RenderPartialViewToString("~/Views/Upload/QuestionEdit.cshtml", questionViewModel);
            //IQueryable<QuestionViewModel> questionViewModels = questionViewModelList.AsQueryable();
            //string result = JsonConvert.SerializeObject(questionViewModel);

            Response.Write(result);
        }

        public void PassageEdit(int passageid)
        {
            TempPassage passage = db.TempPassage.Find(passageid);
            PassageViewModel passageViewModel = MapperHelper.MapToViewModelTemp(passage);

            IQueryable<Question> questions = (from q in db.Question where (q.PassageID == passageid) select q);
            List<QuestionViewModel> QuestionViewModelList = MapperHelper.MapToViewModelList(questions);
            QuestionViewModelList = QuestionViewModelList;
            passageViewModel.QuestionViewModelList = QuestionViewModelList;
            string result = RenderPartialViewToString("PassageEdit", passageViewModel);
            Response.Write(result);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Edit(PassageViewModel passageViewModel, string save)
        {
            TempPassage passage = db.TempPassage.Find(passageViewModel.ID);
            if (ModelState.IsValid)
            {
                JavaScriptSerializer jsonSerializer = new JavaScriptSerializer();
                List<QuestionOrder> questionOrderList = new List<QuestionOrder>();
                if (!String.IsNullOrEmpty(passageViewModel.AnswerOrder))
                    questionOrderList = JsonConvert.DeserializeObject<List<QuestionOrder>>(passageViewModel.AnswerOrder); //jsonSerializer.Deserialize<AnswerOrder[]>();
                passage.ModifiedDate = DateTime.Now;
                passage.ModifiedBy = GetCurrentUserID();
                passage = MapperHelper.MapToDomainObjectTemp(db, passageViewModel, passage);
                passage.LastUpdated = DateTime.Now;
              //  passage.Description = htmlConverter.FormatFileContent(passage.Description);
                passage.Description =passage.Description;

                db.Entry(passage).State = EntityState.Modified;

                OrderQuestionForPassage(questionOrderList, passageViewModel.CanShuffle, passage);

                db.SaveChanges();
                if (save.Equals("SaveAddNewQuestion"))
                {                                                           //Add new question
                    return RedirectToAction("Create", "Question", new RouteValueDictionary(new { PassageID = passage.ID }));
                }
                else if (save.Equals("SaveAddQuestion"))
                {                                                           //Add existing question
                    return RedirectToAction("AddExistingQuestions", "Passage", new RouteValueDictionary(new { id = passage.ID }));
                }

                return RedirectToAction("Index", "Question");
            }
            IQueryable<TempQuestion> questions = (from q in db.TempQuestion where (q.PassageID == passageViewModel.ID) select q);
            //List<QuestionViewModel> QuestionViewModelList = MapperHelper.MapToViewModelListTemp(questions);
            //QuestionViewModelList = sh.SortQuestionViewModel(QuestionViewModelList);
            //passageViewModel.QuestionViewModelList = QuestionViewModelList;
            return View(passageViewModel);
        }
        public void OrderQuestionForPassage(List<QuestionOrder> questionOrderList, bool canShuffle, TempPassage passage)
        {
            //Passage passage = db.Passage.Find(passageID);
            List<TempPassageQuestionOrder> passageQuestionOrderList = passage.TempPassageQuestionOrder.Where(pqo => pqo.TempPassage.ID == passage.ID).ToList();

            db.TempPassageQuestionOrder.RemoveRange(passageQuestionOrderList);
            db.SaveChanges();
            if (!canShuffle)
            {
                if (passage != null)
                {
                    int i = 1;
                    foreach (QuestionOrder questionID in questionOrderList)
                    {
                        TempQuestion question = db.TempQuestion.Find(questionID.id);
                        if (question != null)
                        {
                            TempPassageQuestionOrder passageQuestionOrder = new TempPassageQuestionOrder();
                            passageQuestionOrder.TempPassage = passage;
                            passageQuestionOrder.TempQuestion = question;
                            passageQuestionOrder.Order = i;
                            db.TempPassageQuestionOrder.Add(passageQuestionOrder);
                            i++;
                        }
                    }
                }
            }
        }

        #endregion

        //public void QuestionSave(long questionID, string qDescription, string ansDescription, long answerID)
        //{
        //    Question question;
        //    Answer answer;

        //    question = db.Question.Find(questionID);
        //    answer = db.Answer.Find(answerID);

        //    question.Description = qDescription;
        //    answer.Description = ansDescription;

        //    db.Question.Add(question);
        //    db.Answer.Add(answer);

        //    db.SaveChanges();

        //}


        public class HtmlEntities
        {

            public HtmlEntities(string HtmlCode, string XmlCode)
            {
                // TODO: Complete member initialization
                this.HtmlCode = HtmlCode;
                this.XmlCode = XmlCode;
            }
            public string HtmlCode { get; set; }
            public string XmlCode { get; set; }

        }

        public class ImageViewModel
        {
            public string url { get; set; }
        }
    }


}

