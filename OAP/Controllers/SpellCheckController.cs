﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Xml;
namespace OAP.Controllers
{
    public class SpellCheckController : Controller
    {

        #region Current

        //private const string GoogleSpellCheckRpc = "https://www.google.com/tbproxy/spell?";
        //private const string GoogleFlagTextAlreadClipped = "textalreadyclipped";
        //private const string GoogleFlagIgnoreDups = "ignoredups";
        //private const string GoogleFlagIgnoreDigits = "ignoredigits";
        //private const string GoogleFlagIgnoreAllCaps = "ignoreallcaps";

        //private bool IgnoreDups { get; set; }
        //private bool IgnoreDigits { get; set; }
        //private bool TextAlreadClipped { get; set; }
        //private bool IgnoreAllCaps { get; set; }

        //public void ProcessRequest(HttpContext context)
        //{
        //    string engine = context.Request.Form[1];
        //    string lang = context.Request.Form["lang"];
        //    string text = context.Request.Form[3];
        //    string suggest = context.Request.Form[2];
        //    SetSwitches(context);
        //    string result = SpellCheck(text, lang, engine, suggest);
        //    context.Response.ContentType = "application/js";
        //    string jsonStr = "{\"outcome\":\"success\",\"data\":[" + result + "]}";
        //    if (suggest == "get_incorrect_words")
        //    {
        //        context.Response.Write(jsonStr);
        //    }
        //    else
        //    {
        //        context.Response.Write(result);
        //    }
        //}

        //public bool IsReusable
        //{
        //    get { return false; }
        //}


        //private string SpellCheck(string text, string lang, string engine, string suggest)
        //{
        //    if (0 == string.Compare(suggest, "undefined", StringComparison.OrdinalIgnoreCase))
        //    {
        //        suggest = string.Empty;
        //    }
        //    if (0 != string.Compare(engine, "google", true))
        //    {
        //        throw new NotImplementedException("Only google spell check engine is support at this moment.");
        //    }
        //    string xml;
        //    List<string> result;
        //    if (string.IsNullOrEmpty(suggest) || suggest == "get_incorrect_words")
        //    {
        //        xml = GetSpellCheckRequest(text, lang);
        //        result = GetListOfMisspelledWords(xml, text);
        //    }
        //    else
        //    {
        //        xml = GetSpellCheckRequest(text, lang);
        //        result = GetListOfSuggestWords(xml, text);
        //    }
        //    return ConvertStringListToJavascriptArrayString(result);
        //}

        //private static bool SetBooleanSwitch(HttpContext context, string queryStringParameter)
        //{
        //    byte tryParseZeroOne;
        //    string queryStringValue = context.Request.QueryString[queryStringParameter];
        //    if (!string.IsNullOrEmpty(queryStringValue) && byte.TryParse(queryStringValue, out tryParseZeroOne))
        //    {
        //        if (1 < tryParseZeroOne || 0 > tryParseZeroOne)
        //        {
        //            throw new InvalidOperationException(string.Format("Query string parameter '{0}' only supports values of 1 and 0.", queryStringParameter));
        //        }
        //        return tryParseZeroOne == 1;
        //    }
        //    return false;
        //}

        //private static List<string> GetListOfSuggestWords(string xml, string suggest)
        //{
        //    if (string.IsNullOrEmpty(xml) || string.IsNullOrEmpty(suggest))
        //    {
        //        return null;
        //    }
        //    // 
        //    XmlDocument xdoc = new XmlDocument();
        //    xdoc.LoadXml(xml);
        //    if (!xdoc.HasChildNodes)
        //    {
        //        return null;
        //    }
        //    XmlNodeList nodeList = xdoc.SelectNodes("//c");
        //    if (null == nodeList || 0 >= nodeList.Count)
        //    {
        //        return null;
        //    }
        //    List<string> list = new List<string>();
        //    foreach (XmlNode node in nodeList)
        //    {
        //        list.AddRange(node.InnerText.Split('\t'));
        //        return list;
        //    }
        //    return list;
        //}

        //private static List<string> GetListOfMisspelledWords(string xml, string text)
        //{
        //    if (string.IsNullOrEmpty(xml) || string.IsNullOrEmpty(text))
        //    {
        //        return null;
        //    }
        //    XmlDocument xdoc = new XmlDocument();
        //    xdoc.LoadXml(xml);
        //    if (!xdoc.HasChildNodes)
        //    {
        //        return null;
        //    }
        //    XmlNodeList nodeList = xdoc.SelectNodes("//c");
        //    if (null == nodeList || 0 >= nodeList.Count)
        //    {
        //        return null;
        //    }
        //    List<string> list = new List<string>();
        //    foreach (XmlNode node in nodeList)
        //    {
        //        int offset = Convert.ToInt32(node.Attributes["o"].Value);
        //        int length = Convert.ToInt32(node.Attributes["l"].Value);
        //        list.Add(text.Substring(offset, length));
        //    }
        //    return list;
        //}

        //private static string ConstructRequestUrl(string text, string lang)
        //{
        //    if (string.IsNullOrEmpty(text))
        //    {
        //        return string.Empty;
        //    }
        //    lang = string.IsNullOrEmpty(lang) ? "en" : lang;
        //    return string.Format("{0}lang={1}&text={2}", GoogleSpellCheckRpc, lang, text);
        //}

        //private static string ConvertStringListToJavascriptArrayString(ICollection<string> list)
        //{
        //    StringBuilder stringBuilder = new StringBuilder();
        //    stringBuilder.Append("[");
        //    if (null != list && 0 < list.Count)
        //    {
        //        bool showSeperator = false;
        //        foreach (string word in list)
        //        {
        //            if (showSeperator)
        //            {
        //                stringBuilder.Append(",");
        //            }
        //            stringBuilder.AppendFormat("\"{0}\"", word);
        //            showSeperator = true;
        //        }
        //    }
        //    stringBuilder.Append("]");
        //    return stringBuilder.ToString();
        //}

        //private void SetSwitches(HttpContext context)
        //{
        //    IgnoreAllCaps = SetBooleanSwitch(context, GoogleFlagIgnoreAllCaps);
        //    IgnoreDigits = SetBooleanSwitch(context, GoogleFlagIgnoreDigits);
        //    IgnoreDups = SetBooleanSwitch(context, GoogleFlagIgnoreDups);
        //    TextAlreadClipped = SetBooleanSwitch(context, GoogleFlagTextAlreadClipped);
        //}


        //private string GetSpellCheckRequest(string text, string lang)
        //{
        //    string requestUrl = ConstructRequestUrl(text, lang);
        //    string requestContentXml = ConstructSpellRequestContentXml(text);
        //    byte[] buffer = Encoding.UTF8.GetBytes(requestContentXml);

        //    WebClient webClient = new WebClient();
        //    webClient.Headers.Add("Content-Type", "text/xml");
        //    try
        //    {
        //        byte[] response = webClient.UploadData(requestUrl, "POST", buffer);
        //        return Encoding.UTF8.GetString(response);
        //    }
        //    catch (ArgumentException)
        //    {
        //        return string.Empty;
        //    }
        //}

        //private string ConstructSpellRequestContentXml(string text)
        //{
        //    XmlDocument doc = new XmlDocument(); // Create the XML Declaration, and append it to XML document
        //    XmlDeclaration declaration = doc.CreateXmlDeclaration("1.0", null, null);
        //    doc.AppendChild(declaration);
        //    XmlElement root = doc.CreateElement("spellrequest");
        //    root.SetAttribute(GoogleFlagTextAlreadClipped, TextAlreadClipped ? "1" : "0");
        //    root.SetAttribute(GoogleFlagIgnoreDups, IgnoreDups ? "1" : "0");
        //    root.SetAttribute(GoogleFlagIgnoreDigits, IgnoreDigits ? "1" : "0");
        //    root.SetAttribute(GoogleFlagIgnoreAllCaps, IgnoreAllCaps ? "1" : "0");
        //    doc.AppendChild(root);
        //    XmlElement textElement = doc.CreateElement("text");
        //    textElement.InnerText = text;
        //    root.AppendChild(textElement);
        //    return doc.InnerXml;
        //}

        #endregion

        #region Old
        //
        // GET: /SpellCheck/

        private const string GoogleSpellCheckRpc = "https://www.google.com/tbproxy/spell?";
        //private const string GoogleSpellCheckRpc = "https://www.google.com/complete/search?";

        #region page method

        public string SpellCheck(string text, string lang, string engine, string suggest)
        {
            if (0 == string.Compare(suggest, "undefined", StringComparison.OrdinalIgnoreCase))
            {
                suggest = string.Empty;
            }
            string xml;
            List<string> result;
            if (string.IsNullOrEmpty(suggest))
            {
                xml = GetSpellCheckRequest(text, lang);
                result = GetListOfMisspelledWords(xml, text);
            }
            else
            {
                xml = GetSpellCheckRequest(suggest, lang);
                result = GetListOfSuggestWords(xml, suggest);
            }
            return new JavaScriptSerializer().Serialize(result);
        }

        #endregion

        #region private methods

        private List<string> GetListOfSuggestWords(string xml, string suggest)
        {
            if (string.IsNullOrEmpty(xml) || string.IsNullOrEmpty(suggest))
            {
                return null;
            }
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(xml);
            if (!xdoc.HasChildNodes)
            {
                return null;
            }
            XmlNodeList nodeList = xdoc.SelectNodes("//c");
            if (null == nodeList || 0 >= nodeList.Count)
            {
                return null;
            }
            List<string> list = new List<string>();
            foreach (XmlNode node in nodeList)
            {
                foreach (string s in node.InnerText.Split('\t'))
                {
                    list.Add(s);
                }
                return list;
            }
            return list;
        }

        private List<string> GetListOfMisspelledWords(string xml, string text)
        {
            if (string.IsNullOrEmpty(xml) || string.IsNullOrEmpty(text))
            {
                return null;
            }
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(xml);
            if (!xdoc.HasChildNodes)
            {
                return null;
            }
            XmlNodeList nodeList = xdoc.SelectNodes("//c");
            if (null == nodeList || 0 >= nodeList.Count)
            {
                return null;
            }
            List<string> list = new List<string>();
            foreach (XmlNode node in nodeList)
            {
                int offset = Convert.ToInt32(node.Attributes["o"].Value);
                int length = Convert.ToInt32(node.Attributes["l"].Value);
                list.Add(text.Substring(offset, length));
            }
            return list;
        }

        private string GetSpellCheckRequest(string text, string lang)
        {
            string requestUrl = ConstructRequestUrl(text, lang);
            string requestContentXml = ConstructSpellRequestContentXml(text);
            byte[] buffer = Encoding.UTF8.GetBytes(requestContentXml);

            WebClient webClient = new WebClient();
            webClient.Headers.Add("Content-Type", "text/xml");
            byte[] response = webClient.UploadData(requestUrl, "POST", buffer);
            return Encoding.UTF8.GetString(response);
        }

        private static string ConstructRequestUrl(string text, string lang)
        {
            if (string.IsNullOrEmpty(text))
            {
                return string.Empty;
            }
            lang = string.IsNullOrEmpty(lang) ? "en" : lang;
            return string.Format("{0}lang={1}&text={2}", GoogleSpellCheckRpc, lang, text);
        }

        private string ConstructSpellRequestContentXml(string text)
        {
            XmlDocument doc = new XmlDocument(); // Create the XML Declaration, and append it to XML document
            XmlDeclaration declaration = doc.CreateXmlDeclaration("1.0", null, null);
            doc.AppendChild(declaration);
            XmlElement root = doc.CreateElement("spellrequest");
            root.SetAttribute("textalreadyclipped", "0");
            root.SetAttribute("ignoredups", "0");
            root.SetAttribute("ignoredigits", "1");
            root.SetAttribute("ignoreallcaps", "1");
            doc.AppendChild(root);
            XmlElement textElement = doc.CreateElement("text");
            textElement.InnerText = text;
            root.AppendChild(textElement);
            return doc.InnerXml;
        }


        #endregion

        public ActionResult Index()
        {
            return View();
        }

        #endregion

    }
}
