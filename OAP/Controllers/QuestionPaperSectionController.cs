﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OAP.Domain.ViewModel;
using OAP.Domain;
using HelperClasses;
using System.Data.Entity;
using OAP.Domain.ViewModel.DataTableViewModel;
using Newtonsoft.Json;
using System.Data;


namespace OAP.Controllers
{
    [Authorize(Roles = "SuperAdmin, CollegeAdmin, Admin")]
    public class QuestionPaperSectionController : BaseController
    {
        private OAPEntities db = new OAPEntities();
        //
        // GET: /QuestionPaperSection/
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create(QuestionPaperViewModel assessmentViewModel)
        {
            QuestionPaperSectionViewModel assessmentSectionViewModel = new QuestionPaperSectionViewModel();
            assessmentSectionViewModel.QuestionPaperID = assessmentViewModel.ID;
            assessmentSectionViewModel.SectionCount = assessmentViewModel.SectionCount;

            return View(assessmentSectionViewModel);
        }

        [HttpPost]
        public ActionResult Create(QuestionPaperSectionViewModel assessmentSectionViewModel)
        {
            return View();
        }

        #region AddQuestionsUnderQuestionPaperSection

        public ActionResult QuestionPaperSectionQuestions(int id)
        {
            QuestionPaperSectionViewModel assessmentSectionViewModel = new QuestionPaperSectionViewModel();
            assessmentSectionViewModel.ID = id;
            IQueryable<Tag> tagsList = db.Tag.GroupBy(x => x.Name).Select(x => x.FirstOrDefault());
            QuestionPaperSection questionPaperSection = db.QuestionPaperSection.Find(id);
            assessmentSectionViewModel.QuestionPaperID = questionPaperSection.AssessmentID;
            List<TagViewModel> tagViewModelList = MapperHelper.MapToViewModelList(tagsList.OrderBy(x=>x.Name));
            tagViewModelList = tagViewModelList;
            assessmentSectionViewModel.TagViewModelList = tagViewModelList;
            IQueryable<Subject> subjects = db.Subject.GroupBy(x => x.Name).Select(x => x.FirstOrDefault());
            //List<SubjectViewModel> subjectViewModelist = MapperHelper.MapToViewModelList(subjects);
            //subjectViewModelist = sh.SortSubjectViewModel(subjectViewModelist);
            //assessmentSectionViewModel.SubjectViewModeList = subjectViewModelist;
            ViewBag.Complexity = db.Complexity.OrderBy(x => x.ComplexityLevel);
            return View(assessmentSectionViewModel);
        }

        public ActionResult QuestionPaperSectionQuestionsPopup(int id)
        {
            QuestionPaperSectionViewModel assessmentSectionViewModel = new QuestionPaperSectionViewModel();
            assessmentSectionViewModel.ID = id;
            IQueryable<Tag> tagsList = db.Tag;
            QuestionPaperSection questionPaperSection = db.QuestionPaperSection.Find(id);
            assessmentSectionViewModel.QuestionPaperID = questionPaperSection.AssessmentID;
            List<TagViewModel> tagViewModelList = MapperHelper.MapToViewModelList(tagsList);
            tagViewModelList = tagViewModelList;
            assessmentSectionViewModel.TagViewModelList = tagViewModelList;
            return View(assessmentSectionViewModel);
        }


        public void GetSelectedQuestions(int id, string tags, string subjects, int complexity = 0)
        {
            string[] columns = { "Description" };
            datatableRequest = new DataTableRequest(Request, columns);
            IQueryable<Question> questions = db.QuestionPaperSectionQuestion.Where(ass => ass.AssessmentSectionID == id).Select(x => x.Question);
            IQueryable<TechnicalQuestion> technicalQuestions = db.TechnicalQuestionPaperSectionQuestion.Where(ass => ass.TechnicalAssessmentSectionID == id).Select(x => x.TechnicalQuestion);

            if (!string.IsNullOrWhiteSpace(tags))
            {
                List<long> tagIDList = tags.Split(',').ToList().Select(long.Parse).ToList();
                questions = questions.Where(q => q.QuestionTags.Where(qt => tagIDList.Contains(qt.TagID)).Count() > 0);
                technicalQuestions = technicalQuestions.Where(q => q.TechnicalQuestionTags.Where(qt => tagIDList.Contains(qt.TagID)).Count() > 0);
            }
            if (!string.IsNullOrWhiteSpace(subjects))
            {
                List<long> subjectIDlist = subjects.Split(',').ToList().Select(long.Parse).ToList();
                questions = questions.Where(q => (subjectIDlist.Contains(q.SubjectID ?? 0)));
                technicalQuestions = technicalQuestions.Where(q => (subjectIDlist.Contains(q.SubjectID ?? 0)));
            }
            if (complexity != 0)
            {
                questions = questions.Where(q => (q.Complexity.ID == complexity));
                technicalQuestions = technicalQuestions.Where(q => (q.Complexity.ID == complexity));
            }

            int totalRecords = 0;
            try
            {
                if (questions != null && questions.ToList().Count() != 0)
                {
                    totalRecords = questions.ToList().Count();
                }

                if (technicalQuestions != null && technicalQuestions.ToList().Count() != 0)
                {
                    totalRecords = totalRecords + technicalQuestions.ToList().Count();
                }
            }
            catch (NullReferenceException)
            {
                totalRecords = 0;
            }
            
            int filteredCount = totalRecords;

            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                questions = (from q in db.Question where q.Description.Contains(datatableRequest.SearchString) select q);
                technicalQuestions = (from q in db.TechnicalQuestion where q.Description.Contains(datatableRequest.SearchString) select q);

                filteredCount = questions.Count() + technicalQuestions.Count();
            }

            questions = questions.OrderBy(x => x.Description);
            technicalQuestions = technicalQuestions.OrderBy(x => x.Description);

            if (datatableRequest.ShouldOrder)
            {
                questions = questions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
                technicalQuestions = technicalQuestions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            }

            questions = questions.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            technicalQuestions = technicalQuestions.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);

            List<QuestionViewModel> questionViewModelList = MapperHelper.MapToViewModelList(questions);
            var technicalQuestionViewModelList = MapperHelper.MapToTechnicalViewModelList(technicalQuestions);

            QuestionDataTableViewModel dataTableViewModel = new QuestionDataTableViewModel(questionViewModelList, technicalQuestionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        public void GetOtherQuestions(int id, string tags, string subjects, int complexity = 0)
        {
            string[] columns = { "Description" };
            datatableRequest = new DataTableRequest(Request, columns);
            IQueryable<Question> questions = (from q in db.Question
                                              group q by new
                                              {
                                                  q.Description
                                              } into qs
                                              select qs.FirstOrDefault());


            IQueryable<TechnicalQuestion> technicalQuestions = (from q in db.TechnicalQuestion
                                                                group q by new
                                                                {
                                                                    q.Description
                                                                } into qs
                                                                select qs.FirstOrDefault());

            QuestionPaperSection questionPaperSection = db.QuestionPaperSection.Find(id);

            int totalRecords = 0;
            int filteredCount = 0;
            if (questionPaperSection != null)
            {
                List<Question> existingQuestions = questionPaperSection.QuestionPaperSectionQuestion.Select(x => x.Question).Distinct().ToList();
                List<TechnicalQuestion> existingTechnicalQuestions = questionPaperSection.TechnicalQuestionPaperSectionQuestion.Select(x => x.TechnicalQuestion).Distinct().ToList();

                List<int> existingQuestionIDs = questionPaperSection.QuestionPaperSectionQuestion.Select(x => x.Question.ID).Distinct().ToList();
                List<int> existingTechnicalQuestionIDs = questionPaperSection.TechnicalQuestionPaperSectionQuestion.Select(x => x.TechnicalQuestion.ID).Distinct().ToList();

                if (!string.IsNullOrWhiteSpace(subjects))
                {
                    List<long> subjectIDlist = subjects.Split(',').ToList().Select(long.Parse).ToList();
                    questions = questions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0 && (subjectIDlist.Contains(q.SubjectID ?? 0)));
                    technicalQuestions = technicalQuestions.Where(q => q.TechnicalQuestionPaperSectionQuestion.Where(asq => asq.TechnicalAssessmentSectionID == id).Count() <= 0 && (subjectIDlist.Contains(q.SubjectID ?? 0)));
                }
                else
                {
                    questions = questions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0);
                    technicalQuestions = technicalQuestions.Where(q => q.TechnicalQuestionPaperSectionQuestion.Where(asq => asq.TechnicalAssessmentSectionID == id).Count() <= 0);
                }

                if (!string.IsNullOrWhiteSpace(tags))
                {
                    List<long> tagIDList = tags.Split(',').ToList().Select(long.Parse).ToList();
                    questions = questions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0 && (q.QuestionTags.Where(qt => tagIDList.Contains(qt.TagID)).Count() > 0));
                    technicalQuestions = technicalQuestions.Where(q => q.TechnicalQuestionPaperSectionQuestion.Where(asq => asq.TechnicalAssessmentSectionID == id).Count() <= 0 && (q.TechnicalQuestionTags.Where(qt => tagIDList.Contains(qt.TagID)).Count() > 0));
                }
                else
                {
                    questions = questions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0);
                    technicalQuestions = technicalQuestions.Where(q => q.TechnicalQuestionPaperSectionQuestion.Where(asq => asq.TechnicalAssessmentSectionID == id).Count() <= 0);
                }

                if (complexity != 0)
                {

                    questions = questions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0 && (q.Complexity.ID == complexity));
                    technicalQuestions = technicalQuestions.Where(q => q.TechnicalQuestionPaperSectionQuestion.Where(asq => asq.TechnicalAssessmentSectionID == id).Count() <= 0 && (q.Complexity.ID == complexity));
                }
                else
                {
                    questions = questions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0);
                    technicalQuestions = technicalQuestions.Where(q => q.TechnicalQuestionPaperSectionQuestion.Where(asq => asq.TechnicalAssessmentSectionID == id).Count() <= 0);
                }

                try
                {
                    if (questions != null && questions.ToList().Count() != 0)
                    {
                        totalRecords = questions.ToList().Count();
                    }

                    if (technicalQuestions != null && technicalQuestions.ToList().Count() != 0)
                    {
                        totalRecords = totalRecords + technicalQuestions.ToList().Count();
                    }
                }
                catch (NullReferenceException)
                {
                    totalRecords = 0;
                }

                filteredCount = totalRecords;

                if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
                {
                    questions = (from q in db.Question where q.Description.Contains(datatableRequest.SearchString) && q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 select q);
                    technicalQuestions = (from q in db.TechnicalQuestion where q.Description.Contains(datatableRequest.SearchString) select q);

                    filteredCount = questions.Count() + technicalQuestions.Count();
                }

                questions = questions.OrderBy(x => x.Description);
                technicalQuestions = technicalQuestions.OrderBy(x => x.Description);

                if (datatableRequest.ShouldOrder)
                {
                    questions = questions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
                    technicalQuestions = technicalQuestions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
                }

                questions = questions.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
                technicalQuestions = technicalQuestions.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            }

            List<QuestionViewModel> questionViewModelList = MapperHelper.MapToViewModelList(questions);
            var technicalQuestionViewModelList = MapperHelper.MapToTechnicalViewModelList(technicalQuestions);

            QuestionDataTableViewModel dataTableViewModel = new QuestionDataTableViewModel(questionViewModelList, technicalQuestionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);


        }

        public void MapQuestions(string questionIDString, string assessmentSectionIDString)
        {
            int assessmentSectionID = Convert.ToInt16(assessmentSectionIDString);
            List<int> questionIDList = questionIDString.Split(',').ToList().Select(int.Parse).ToList();

            QuestionPaperSection questionPaperSection = db.QuestionPaperSection.Find(assessmentSectionID);
            foreach (int questionID in questionIDList)
            {
                
                Question question = db.Question.Find(questionID);

                if(question == null)
                {
                    TechnicalQuestionPaperSectionQuestion technicalQuestionPaperSectionQuestion = new TechnicalQuestionPaperSectionQuestion();
                    TechnicalQuestion technicalQuestion = db.TechnicalQuestion.Find(questionID);
                    technicalQuestionPaperSectionQuestion.TechnicalQuestion = technicalQuestion;
                    technicalQuestionPaperSectionQuestion.QuestionPaperSection = questionPaperSection;
                    db.TechnicalQuestionPaperSectionQuestion.Add(technicalQuestionPaperSectionQuestion);
                }
                else
                {
                    QuestionPaperSectionQuestion questionPaperSectionQuestion = new QuestionPaperSectionQuestion();
                    questionPaperSectionQuestion.Question = question;
                    questionPaperSectionQuestion.QuestionPaperSection = questionPaperSection;
                    db.QuestionPaperSectionQuestion.Add(questionPaperSectionQuestion);
                }
            }
            db.SaveChanges();
            Response.Write("true");
        }

        public void UnMapQuestions(string questionIDString, string assessmentSectionIDString)
        {
            int assessmentSectionID = Convert.ToInt16(assessmentSectionIDString);
            List<int> questionIDList = questionIDString.Split(',').ToList().Select(int.Parse).ToList();

            List<QuestionPaperSectionQuestion> questionPaperSectionQuestionList = new List<QuestionPaperSectionQuestion>();

            QuestionPaperSection questionPaperSection = db.QuestionPaperSection.Find(assessmentSectionID);

            foreach (int questionID in questionIDList)
            {
                QuestionPaperSectionQuestion questionPaperSectionQuestion = questionPaperSection.QuestionPaperSectionQuestion.Where(asq => asq.Question.ID == questionID).FirstOrDefault();
                //QuestionPaperSectionQuestion assessmentSectionQuestion = db.QuestionPaperSectionQuestion.Where(asq => asq.Question.ID == questionID && asq.QuestionPaperSection.ID==assessmentSectionID).FirstOrDefault();
                if (questionPaperSectionQuestion != null)
                {
                    db.QuestionPaperSectionQuestion.Remove(questionPaperSectionQuestion);
                }
            }
            db.SaveChanges();
            Response.Write("true");

        }

        #endregion

        #region AddRandomQuestions under QuestionPaper Section

        public ActionResult RandomQuestionSelection(int id)
        {
            QuestionPaperSectionViewModel questionPaperSectionViewModel = GetQuestionPaperViewModelForRandomQuestionSelection(id);
            //IQueryable<Question> questions= db.Question.Where(q => q.QuestionPaperSectionQuestion.Where(asq => asq.QuestionPaperSection.ID != id).Count() > 0);

            return View(questionPaperSectionViewModel);
        }

     

        public QuestionPaperSectionViewModel GetQuestionPaperViewModelForRandomQuestionSelection(int id)
        {
            QuestionPaperSectionViewModel assessmentSectionViewModel = new QuestionPaperSectionViewModel();

            IQueryable<Question> questions = db.Question.Where(q => q.QuestionPaperSectionQuestion.Where(asq => asq.QuestionPaperSection.ID != id).Count() <= 0);
            List<int> questionIDs = questions.Select(x => x.ID).ToList();
            IQueryable<QuestionTags> questionTags = db.QuestionTags.Where(qt => questionIDs.Contains(qt.Question.ID));

            List<QuestionViewModel> questionViewModelList = MapperHelper.MapToViewModelList(questionTags.Select(s => s.Question).Distinct());
            List<TagViewModel> tagViewModelList = MapperHelper.MapToViewModelList(questionTags.Select(x => x.Tag).Distinct());

            List<int> selectedQuestions = new List<int>();
            if (tagViewModelList != null && tagViewModelList.Count() > 0)
            {
                for (int i = 0; i < tagViewModelList.Count(); i++)
                {
                    long tagID = tagViewModelList[i].ID;

                    tagViewModelList[i].AvailableQuestionsCount = questionTags.Where(qt => qt.Tag.ID == tagID && (qt.Question.QuestionPaperSectionQuestion.Where(asq => asq.QuestionPaperSection.ID == id).Count() <= 0)).Select(qt => qt.Question).Distinct().Count();
                    selectedQuestions.AddRange(questionTags.Where(qt => qt.Tag.ID == tagID && (qt.Question.QuestionPaperSectionQuestion.Where(asq => asq.QuestionPaperSection.ID == id).Count() <= 0)).Select(x => x.Question.ID).ToList());
                }
            }

            int availableQuestionsCount = selectedQuestions.Count();

            assessmentSectionViewModel.AvailableQuestionsCount = availableQuestionsCount;
            assessmentSectionViewModel.TagViewModelList = tagViewModelList;


            return assessmentSectionViewModel;
        }

        public void GenerateRandomQuestions(int id, string tags, string subjects, int count, int complexity = 0)
        {
            string result = "";
            IQueryable<Question> allQuestions = db.Question;
            IQueryable<Question> filteredQuestions;
            if (!string.IsNullOrWhiteSpace(tags))
            {
                List<long> tagIDList = tags.Split(',').ToList().Select(long.Parse).ToList();
                filteredQuestions = allQuestions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0 && (q.QuestionTags.Where(qt => tagIDList.Contains(qt.TagID)).Count() > 0));
            }
            else
            {
                filteredQuestions = allQuestions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0);
            }

            if (!string.IsNullOrWhiteSpace(subjects))
            {
                List<long> subjectIDlist = subjects.Split(',').ToList().Select(long.Parse).ToList();
                filteredQuestions = allQuestions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0 && (subjectIDlist.Contains(q.SubjectID ?? 0)));
            }
            else
            {
                filteredQuestions = allQuestions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0);
            }
            if (complexity != 0)
            {
                filteredQuestions = allQuestions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0 && (q.Complexity.ID == complexity));
            }
            if (filteredQuestions.Count() >= count)
            {
                filteredQuestions = filteredQuestions.Take(count);
                List<int> questionIDList = filteredQuestions.Select(x => x.ID).ToList();

                QuestionPaperSection questionPaperSection = db.QuestionPaperSection.Find(id);
                foreach (int questionID in questionIDList)
                {
                    Question question = db.Question.Find(questionID);

                    QuestionPaperSectionQuestion questionPaperSectionQuestion = new QuestionPaperSectionQuestion();
                    questionPaperSectionQuestion.QuestionPaperSection = questionPaperSection;
                    questionPaperSectionQuestion.Question = question;
                    db.QuestionPaperSectionQuestion.Add(questionPaperSectionQuestion);
                }
                db.SaveChanges();
                result = "true";
            }
            else
            {
                result = "false" + "," + filteredQuestions.Count();
            }
            Response.Write(result);


        }

        #endregion

        #region Edit Sections of QuestionPapers

        [ValidateInput(false)]
        public ActionResult EditQuestionPaperSections(QuestionPaperViewModel assessmentViewModel)
        {
            QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentViewModel.ID);
            QuestionPaperSectionViewModel assessmentSectionViewModel = new QuestionPaperSectionViewModel();

            if (questionPaper != null)
            {
                IEnumerable<QuestionPaperSection> questionPaperSection = questionPaper.QuestionPaperSection;
                List<QuestionPaperSectionViewModel> assessmentSectionViewModelList = MapperHelper.MapToViewModelList(questionPaperSection);
                assessmentSectionViewModel.questionPaperSections = assessmentSectionViewModelList;
                assessmentSectionViewModel.QuestionPaperID = questionPaper.ID;
                if (questionPaper.TimeLimit > 0)
                {
                    assessmentViewModel.IsShowSectionTimeLimit = false;
                }
                else
                {
                    assessmentViewModel.IsShowSectionTimeLimit = true;
                }

                assessmentSectionViewModel.IsShowTimeLimit = assessmentViewModel.IsShowSectionTimeLimit;
            }
            return View(assessmentSectionViewModel);
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult EditQuestionPaperSections(QuestionPaperSectionViewModel assessmentSectionViewModel)
        {
            foreach (QuestionPaperSectionViewModel section in assessmentSectionViewModel.questionPaperSections)
            {
                int minutes = ((section.TimeLimitTimeSpan.Hours * 60) + section.TimeLimitTimeSpan.Minutes);
                section.TimeLimit = minutes;
                if (!string.IsNullOrEmpty(section.Name))
                {
                    QuestionPaperSection questionPaperSection = db.QuestionPaperSection.Find(section.ID);
                    questionPaperSection = MapperHelper.MapToDomainObject(db, section, questionPaperSection);
                    
                    questionPaperSection.ModifiedBy = GetCurrentUserID();
                    questionPaperSection.ModifiedDate = DateTime.Now;
                    db.Entry(questionPaperSection).State = EntityState.Modified;
                }
            }
            db.SaveChanges();

            QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentSectionViewModel.ID);
            IEnumerable<QuestionPaperSection> questionPaperSections;
            questionPaperSections = questionPaper.QuestionPaperSection;
            //IEnumerable<QuestionPaperSection> questionPaperSections = questionPaper.QuestionPaperSection;
            List<QuestionPaperSectionViewModel> assessmentSectionViewModelList = MapperHelper.MapToViewModelList(questionPaperSections);
            assessmentSectionViewModel.questionPaperSections = assessmentSectionViewModelList;
            return View(assessmentSectionViewModel);
        }

        #endregion

        #region Delete QuestionPaper Section

        public void Delete(string idString)
        {
            string result = "";
            if (!string.IsNullOrEmpty(idString))
            {                                               //idString is not empty
                int assessmentSectionID = Convert.ToInt16(idString);
                QuestionPaperSection questionPaperSection = db.QuestionPaperSection.Find(assessmentSectionID);

                QuestionPaper questionPaper = questionPaperSection.QuestionPaper;

                if (questionPaperSection != null)
                {                                                       //Valid assessmentSectionID
                    IEnumerable<QuestionPaperSectionQuestion> sectionQuestions = questionPaperSection.QuestionPaperSectionQuestion;
                    db.QuestionPaperSectionQuestion.RemoveRange(sectionQuestions);
                    db.SaveChanges();
                    db.QuestionPaperSection.Remove(questionPaperSection);
                    db.SaveChanges();
                    result = "Record successfully deleted";

                }
                else
                {                                                       //Invalid assessmentSectionID
                    result = "Section not found";
                }
            }
            else
            {                                               //idString empty
                result = "Invalid section";
            }
            Response.Write(result);
        }

        #endregion
    }
}
