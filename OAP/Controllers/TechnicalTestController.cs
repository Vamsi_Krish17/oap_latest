﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OAP.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;

namespace OAP.Controllers
{
    public class TechnicalTestController : Controller
    {
        //
        // GET: /TechnicalTest/

        public ActionResult Details()
        {
            return View();
        }

        public ActionResult PracticeSessionDetails()
        {
            //GEt All topic from data base ater
            return View();
        }

        public ActionResult OpenCodeEditor()
        {
            //Get all language
            var languageList = GetAllLanguage();

            return View(languageList);
        }

        [HttpPost]
        public void VerifyCode(string input)
        {
            var url = "https://api.judge0.com";
            var urlParameters = "submissions?wait=true";

            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(url),
            };

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync(urlParameters).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;

                // Parse the response body.

            }
            else
            {
                client.Dispose();
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);

            }
        }


        private List<CodingLanguageModel> GetAllLanguage()
        {
            var url = "https://api.judge0.com";
            var urlParameters = "/languages";

            HttpClient client = new HttpClient
            {
                BaseAddress = new Uri(url),
            };

            // Add an Accept header for JSON format.
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync(urlParameters).Result;
            if (response.IsSuccessStatusCode)
            {
                var result = response.Content.ReadAsStringAsync().Result;
                var rootResult = JsonConvert.DeserializeObject<List<CodingLanguageModel>>(result);

                client.Dispose();
                return rootResult;
                // Parse the response body.

            }
            else
            {
                client.Dispose();
                Console.WriteLine("{0} ({1})", (int)response.StatusCode, response.ReasonPhrase);
                return new List<CodingLanguageModel>();
            }
        }

        public ActionResult PracticeWindow(int testId)
        {
            var technicalListResults = GetListOfQuestionsModel();

            if(technicalListResults != null)
            {
                return View(technicalListResults);
            }

            return View();
        }
        
        private TechnicalQuestionModel GetListOfQuestionsModel()
        {
            var technicalQuestions = new TechnicalQuestionModel()
            {
                ListOfLanguages = GetAllLanguage(),
                questionsModel = GetQuestionsModel()
            };

            var langList = GetAllLanguage();

            return technicalQuestions;
        }

        private List<QuestionsModel> GetQuestionsModel()
        {
            return new List<QuestionsModel>();
        }

    }
}
