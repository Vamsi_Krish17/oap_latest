﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using OAP.Domain.ViewModel;
using System.IO;
using OfficeOpenXml;
using OAP.Domain;
using HelperClasses;
using WebMatrix.WebData;
using Newtonsoft.Json;
using System.Data.SqlClient;
using OAP.Domain.ViewModel.DataTableViewModel;
using OAP.Filters;
using System.Web.Security;
using System.Data.Entity;

namespace OAP.Controllers
{
    [InitializeSimpleMembership]
    [Authorize(Roles = "SuperAdmin,CollegeAdmin,Student")]
    //public class StudentController : AccountController
    public class StudentController : BaseController
    {
        public static string saved = "";
        public OAPEntities db = new OAPEntities();
        string[] columns = { "UniversityRegisterNumber" };

        #region Index

        public ActionResult Index()
        {
            ViewBag.Saved = saved;
            saved = "";
            ViewBag.Branches = db.Branch.OrderBy(x=>x.Name);
            OAP.Domain.UserProfile userProfile = db.UserProfile.Where(x => x.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            if (userProfile != null)
            {
                // bool test = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                ViewBag.CanScheduleRetest = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
            }
            return View();
        }

        public void List( string branch)
        {
            long  branchID;
            branchID = 0;
            if (!string.IsNullOrEmpty(branch))
                branchID = Convert.ToInt32(branch);
            long userOrganisationId = 0;
            if (User.IsInRole("CollegeAdmin"))
            {
                int userId = GetCurrentUserID();
                userOrganisationId = db.UserProfile.Find(userId).UserOrganisation.FirstOrDefault() != null ? db.UserProfile.Find(userId).UserOrganisation.FirstOrDefault().ID : 0;
            }

            datatableRequest = new DataTableRequest(Request, columns);
            IQueryable<Student> students;
            if (User.IsInRole("CollegeAdmin") || User.IsInRole("SuperAdmin"))
            {
                //students = db.Student;
                students = (from s in db.Student
                            group s by new
                            {
                                s.UniversityRegisterNumber
                            } into st
                            select st.FirstOrDefault());
                if (User.IsInRole("CollegeAdmin"))
                {
                    students = from s in students where s.UserOrganisationID == userOrganisationId select s;

                }

            }
            else
            {
                UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
                students = db.Student.Where(s => s.College.UserProfileCollege.Where(upc => upc.UserProfileID == userProfile.UserId).Count() > 0);
                students = (from s in students
                            group s by new
                            {
                                s.UniversityRegisterNumber
                            } into st
                            select st.FirstOrDefault());
            }

            int totalRecords = students.Count();
            int filteredCount = totalRecords;
            students = students.OrderBy(s => s.Name);
            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                students = (from s in students where s.Name.ToLower().Contains(datatableRequest.SearchString.ToLower()) || s.Branch.Name.ToLower().Contains(datatableRequest.SearchString.ToLower()) || s.UniversityRegisterNumber.ToLower().Contains(datatableRequest.SearchString.ToLower()) select s);
                filteredCount = students.Count();
            }
            students = students.OrderBy(x => x.UserProfile.FirstOrDefault().UserName);
            if (datatableRequest.ShouldOrder)
            {
                students = students.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            }

           if (branchID != 0)
            {
                students = (from s in students where s.BranchID == branchID select s);
            }
            filteredCount = students.Count();
            //End filter

            students = students.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            List<StudentViewModel> studentViewModelList = MapperHelper.MapToViewModelList(students);


            StudentDataTableViewModel dataTableViewModel = new StudentDataTableViewModel(studentViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        #endregion

        #region Create

        public ActionResult Create()
        {
            ViewBag.Branches = db.Branch;
            return View();
        }
        [HttpPost]
        public ActionResult Create(StudentViewModel studentViewModel, string save)
        {
            OAP.Domain.UserProfile userProfile = db.UserProfile.Where(x => x.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            if (userProfile != null)
            {
                // bool test = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                ViewBag.CanScheduleRetest = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
            }

            if (save == "Save" && !String.IsNullOrEmpty(studentViewModel.UniversityRegistryNumber) && (studentViewModel.Password == studentViewModel.ConfirmPassword))
            {
                try
                {
                    bool result = true;
                    try
                    {
                        WebSecurity.CreateUserAndAccount(studentViewModel.UniversityRegistryNumber, studentViewModel.Password);
                    }
                    catch
                    {
                        result = false;
                    }
                    UpdateStudent(studentViewModel);
                    Roles.AddUserToRole(studentViewModel.UniversityRegistryNumber, "Student");
                    saved = "Student created successfully!";
                    //return View("Index");
                    return RedirectToAction("Index");
                }
                catch
                {
                    ViewBag.Branches = db.Branch;                    
                    return View(studentViewModel);
                }
            }
            else
            {
                if (String.IsNullOrEmpty(studentViewModel.FullName))
                {
                    ModelState.AddModelError("Name", "");
                }
                if (String.IsNullOrEmpty(studentViewModel.Password))
                {
                    ModelState.AddModelError("Password", "Password should not be Empty");
                }
                if (studentViewModel.Password != studentViewModel.ConfirmPassword)
                {
                    ModelState.AddModelError("Password", "");
                    ModelState.AddModelError("ConfirmPassword", "");
                }
                ViewBag.Branches = db.Branch;
                return View(studentViewModel);
            }

        }

        #endregion

        public List<SelectListItem> GetYearRangeByYear(int year)
        {
            List<SelectListItem> yearListItem = new List<SelectListItem>();
            for (int i = year - 5; i <= year + 5; i++)
            {
                SelectListItem yearItem = new SelectListItem();
                yearItem.Text = i.ToString();
                yearItem.Value = i.ToString();
                yearListItem.Add(yearItem);
            }
            return yearListItem;
        }

        #region Edit

        public ActionResult Edit(int id)
        {
            StudentViewModel studentViewModel = new Domain.ViewModel.StudentViewModel();
            Student student = db.Student.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            UserProfile userProfile = GetStudentUserProfileByID(student.UserProfile.FirstOrDefault().UserId);

            studentViewModel = MapperHelper.MapToViewModel(userProfile.Student);
            ViewBag.Branches = db.Branch;

            return View(studentViewModel);
        }

        [HttpPost]
        public ActionResult Edit(StudentViewModel studentViewModel)
        {
            if (ModelState.IsValid)
            {
                UpdateStudent(studentViewModel);
                saved = "Changes Saved Successfully!";
                ViewBag.Branches = db.Branch;
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Branches = db.Branch;
                return View(studentViewModel);
            }
        }

        #endregion

        #region Mass Upload

        #region MassUpload
        public ActionResult MassUpload()
        {
            //    CollegeReposiroty _collegeRepository = new CollegeReposiroty();
            OAP.Domain.UserProfile userProfile = db.UserProfile.Where(x => x.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            if (userProfile != null)
            {
                // bool test = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
                ViewBag.CanScheduleRetest = userProfile.UserProfileCollege.Count() > 0 ? userProfile.UserProfileCollege.FirstOrDefault() != null ? userProfile.UserProfileCollege.FirstOrDefault().CanScheduleRetest : false : false;
            }
            ViewBag.Branches = db.Branch;

            ViewBag.Controller = "Student";
            ViewBag.Action = "MassUpload";
            ViewBag.PageTitle = "Student";
            ViewBag.ReturnUrl = "Student/Index";
            //return View("~/Views/Shared/_MassUpload.cshtml");
            return View();
        }

        [HttpPost]
        public ActionResult MassUpload(HttpPostedFileBase file, StudentViewModel studentViewModel)
        {
            long userOrganisationId = 0;
            if (User.IsInRole("CollegeAdmin"))
            {
                int userId = GetCurrentUserID();
                userOrganisationId = db.UserProfile.Find(userId).UserOrganisation.FirstOrDefault() != null ? db.UserProfile.Find(userId).UserOrganisation.FirstOrDefault().ID : 0;
            }

            List<MashUploadResultViewModel> mashUploadResultViewModelList = new List<MashUploadResultViewModel>();
            if (file != null)                       //Mass UPload
            {
                string filePath = System.IO.Path.GetFullPath(file.FileName);
                string extension = Path.GetExtension(file.FileName);
                string fileName = Guid.NewGuid().ToString() + extension;
                string fullPath = Server.MapPath("//ExcelUpload//Student//" + fileName);
                string directory = Path.GetDirectoryName(fullPath);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                file.SaveAs(fullPath);
                fullPath = fullPath.Replace(@"\\", @"\");


                FileInfo newFile = new FileInfo(fullPath);

                ExcelPackage pck = new ExcelPackage(newFile);

                ExcelWorksheet worksheet = pck.Workbook.Worksheets[1];
                int iRowCnt = worksheet.Dimension.End.Row;

                Branch branch = null;
                if (studentViewModel.BranchID > 0)
                    branch = db.Branch.Find(studentViewModel.BranchID);

                for (int i = 3; i <= iRowCnt; i++)
                {

                    College selectedCollege;
                    Branch selectedBranch;
                    Section selectedSection;

                    string studentName = worksheet.Cells[i, 2].Text.ToString().Trim() ?? "";
                    string universityRegisterNumber = worksheet.Cells[i, 1].Text.ToString().Trim() ?? "";

                    if (string.IsNullOrEmpty(studentName))
                    {
                        studentName = universityRegisterNumber ?? "";
                    }

                    //UserProfileRepository _userProfileRepository = new UserProfileRepository();
                    if (!string.IsNullOrEmpty(universityRegisterNumber) )
                    {
                        UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == universityRegisterNumber.ToLower()).FirstOrDefault();
                        if (userProfile == null)
                        {                                               //No user with this name
                            string password = worksheet.Cells[i, 3].Text.ToString().Trim() ?? "";

                            password = password != "" ? password : universityRegisterNumber;
                            bool result = true;
                            try
                            {
                                WebSecurity.CreateUserAndAccount(universityRegisterNumber, password);
                            }
                            catch
                            {
                                result = false;
                            }
                            if (result)
                            {
                                userProfile = db.UserProfile.Where(up => up.UserName.ToLower().Equals(universityRegisterNumber.ToLower())).FirstOrDefault();
                                Roles.AddUserToRole(userProfile.UserName, "Student");

                                Student student = new Student();

                                student.Name = studentName;
                                student.UniversityRegisterNumber = worksheet.Cells[i, 1].Text.ToString().Trim() ?? studentName;


                                string branchName = worksheet.Cells[i, 5].Text.ToString().Trim() ?? "";
                                if (studentViewModel.BranchID > 0)
                                {                                                       //Branch selected from drop down
                                    selectedBranch = branch;
                                }
                                else
                                {                                                       //Branch not selected from drop down . Take the branch name from xl
                                    selectedBranch = db.Branch.Where(b => b.Name.ToLower() == branchName.ToLower()).FirstOrDefault();
                                }

                                if (selectedBranch != null)
                                    student.Branch = selectedBranch;

                                student.EmailId = worksheet.Cells[i, 9].Text.ToString() ?? "";
                                student.PhoneNumber = worksheet.Cells[i, 10].Text.ToString() ?? "";
                                if (User.IsInRole("CollegeAdmin"))
                                {
                                    student.UserOrganisationID = userOrganisationId;
                                }
                                db.Student.Add(student);

                                userProfile.Student = student;
                                db.Entry(userProfile).State = EntityState.Modified;

                                db.SaveChanges();

                                mashUploadResultViewModelList.Add(CreateMashUploadResultViewModel(studentName, true, "Success", ""));
                            }
                            else
                            {
                                mashUploadResultViewModelList.Add(CreateMashUploadResultViewModel(studentName, true, "Failed", "Error"));
                            }
                        }
                        else
                        {
                            mashUploadResultViewModelList.Add(CreateMashUploadResultViewModel(studentName, true, "Failed", "Duplicate Student name"));
                        }

                    }
                }
                ViewBag.ReturnUrl = "Student/Index";
                return View("~/Views/Shared/UploadedResult.cshtml", mashUploadResultViewModelList);
            }
            ModelState.Clear();
            ViewBag.Branches = db.Branch;

            ViewBag.Controller = "Student";
            ViewBag.Action = "MassUpload";
            ViewBag.PageTitle = "Student";
            ViewBag.ReturnUrl = "Student/Index";
            return View(studentViewModel);
        }

        public MashUploadResultViewModel CreateMashUploadResultViewModel(string name, bool isSuccess, string Message, string reason)
        {
            MashUploadResultViewModel mashUploadResultViewModel = new Domain.ViewModel.MashUploadResultViewModel();
            mashUploadResultViewModel.Name = name;
            mashUploadResultViewModel.IsSuccess = isSuccess;
            mashUploadResultViewModel.Message = Message;
            mashUploadResultViewModel.Reason = reason;
            return mashUploadResultViewModel;
        }
        #endregion

        #endregion

        #region Report

        public ActionResult GenerateAssessmentReport(int assessmentID)
        {
            IQueryable<UserAssessment> userAssessments = db.UserAssessment.Where(ua => ua.QuestionPaper.ID == assessmentID);
            List<UserQuestionPaperViewModel> userAssessmentViewModelList = MapperHelper.MapToViewModelList(userAssessments);

            if (userAssessmentViewModelList != null && userAssessmentViewModelList.Count() > 0)
            {
                for (int i = 0; i < userAssessmentViewModelList.Count(); i++)
                {
                    UserAssessment currentUserAssessment = userAssessments.Where(ua => ua.AssessmentID == userAssessmentViewModelList[i].ID).FirstOrDefault();
                    userAssessmentViewModelList[i].TotalQuestions = currentUserAssessment.QuestionPaper.QuestionPaperSection.SelectMany(ass => ass.QuestionPaperSectionQuestion).Count();
                    userAssessmentViewModelList[i].AttendedQuestionsCount = currentUserAssessment.AssessmentSnapshot.Select(x => x.Question).Count();
                    userAssessmentViewModelList[i].CorrectAnswerCount = currentUserAssessment.AssessmentSnapshot.Where(ass => ass.Answer.IsCorrect ?? false).Count();
                    userAssessmentViewModelList[i].AverageTimeTaken = currentUserAssessment.AssessmentSnapshot.Average(x => x.TimeTaken ?? 0);
                    userAssessmentViewModelList[i].AverageTimeTakenForCorrectAnswer = currentUserAssessment.AssessmentSnapshot.Where(ass => ass.Answer.IsCorrect ?? false).Average(x => x.TimeTaken ?? 0);
                    userAssessmentViewModelList[i].AverageTimeTakenForWrongAnswer = currentUserAssessment.AssessmentSnapshot.Where(ass => !(ass.Answer.IsCorrect ?? false)).Average(x => x.TimeTaken ?? 0);
                }
            }
            return View(userAssessmentViewModelList);
        }

        #endregion

        #region Delete

        public void Delete(string selectedStudents)
        {
            bool result = true;
            if (!string.IsNullOrEmpty(selectedStudents))
            {                                                                   //Students selected
                try
                {
                    List<long> studentIDList = selectedStudents.Split(',').Select(long.Parse).ToList();
                    foreach (long studentID in studentIDList)
                    {
                        Student student = db.Student.Find(studentID);

                        UserProfile userProfile = student.UserProfile.FirstOrDefault();

                        if (userProfile != null)
                        {
                            List<UserAssessment> userAssessmentList = userProfile.UserAssessment.ToList();
                            db.UserAssessment.RemoveRange(userAssessmentList);

                            List<UserLogInLog> userLoginLogList = userProfile.UserLogInLog.ToList();
                            db.UserLogInLog.RemoveRange(userLoginLogList);
                        }

                        List<BatchStudent> batchStudentList = student.BatchStudent.ToList();
                        db.BatchStudent.RemoveRange(batchStudentList);

                        db.Student.Remove(student);

                        db.SaveChanges();

                        if (userProfile != null)
                        {
                            if (userProfile.webpages_Roles.Count() > 0)
                                Roles.RemoveUserFromRole(userProfile.UserName, userProfile.webpages_Roles.FirstOrDefault().RoleName);
                            Membership.DeleteUser(userProfile.UserName);
                        }
                    }
                }
                catch
                {
                    result = false;
                }
            }
            else
            {                                                                       //No students selected
                result = false;
            }
            Response.Write(result);
        }

        #endregion

        #region Repository
        public IEnumerable<Student> GetStudentsList(int pageLimit, int pageNo)
        {
            IEnumerable<Student> students = (from c in db.UserProfile where (c.webpages_Roles.FirstOrDefault().RoleName == "Student") select c.Student).OrderBy(x => x.ID).Skip(pageLimit * (pageNo - 1)).Take(pageLimit);
            return students;
        }

        public bool UpdateStudent(StudentViewModel studentViewModel)
        {
            long userOrganisationId = 0;
            if (User.IsInRole("CollegeAdmin"))
            {
                int userId=GetCurrentUserID();
                userOrganisationId = db.UserProfile.Find(userId).UserOrganisation.FirstOrDefault()!=null?db.UserProfile.Find(userId).UserOrganisation.FirstOrDefault().ID:0;
            }

            bool result = true;
            UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == studentViewModel.UniversityRegistryNumber.ToLower()).FirstOrDefault();
            if (userProfile != null)
            {
                userProfile.UserName = studentViewModel.UniversityRegistryNumber;
                db.Entry(userProfile).State = EntityState.Modified;
            }
            Student student = userProfile.Student;
            if (student != null)
            {
                try
                {
                    userProfile.UserName = studentViewModel.UniversityRegistryNumber;
                    student.UniversityRegisterNumber = studentViewModel.UniversityRegistryNumber;
                    student.Branch = db.Branch.Find(studentViewModel.BranchID);
                    student.EmailId = studentViewModel.EmailId;
                    student.Name = studentViewModel.FullName;
                    if (User.IsInRole("CollegeAdmin"))
                    {
                        student.UserOrganisationID = userOrganisationId;
                    }
                    db.Entry(userProfile.Student).State = EntityState.Modified;

                    db.SaveChanges();
                }
                catch
                {
                    result = false;
                }
            }
            else
            {
                student = new Student();
                student.UniversityRegisterNumber = studentViewModel.UniversityRegistryNumber;
                student.Branch = db.Branch.Find(studentViewModel.BranchID);
                student.EmailId = studentViewModel.EmailId;
                student.Name = studentViewModel.FullName;
                if (User.IsInRole("CollegeAdmin"))
                {
                    student.UserOrganisationID = userOrganisationId;
                }
                db.Student.Add(student);
                userProfile.Student = student;
                db.Entry(userProfile).State = EntityState.Modified;

            }
            db.SaveChanges();

            return result;
        }

        public UserProfile GetStudentUserProfileByID(long id)
        {
            UserProfile userProfile = db.UserProfile.Find(id);
            return userProfile;
        }

        public int GetTotalStudentRecords()
        {
            int count = (from c in db.UserProfile where (c.webpages_Roles.FirstOrDefault().RoleName == "Student") select c.Student).Count();
            return count;
        }
        #endregion

        public void ResetStudentPassword(string selectedStudents,string password)
        {
            bool result = true;
            if (!string.IsNullOrEmpty(selectedStudents))
            {                                                                   //Students selected
                try
                {
                    List<long> studentIDList = selectedStudents.Split(',').Select(long.Parse).ToList();
                    foreach (long studentID in studentIDList)
                    {
                        Student student = db.Student.Find(studentID);

                        UserProfile userProfile = student.UserProfile.FirstOrDefault();
                        var token = WebSecurity.GeneratePasswordResetToken(userProfile.UserName);
                        WebSecurity.ResetPassword(token, password);
                    }
                }
                catch
                {
                    result = false;
                }
            }
            else
            {                                                                       //No students selected
                result = false;
            }
            Response.Write(result);
        }

       

    }
}
