﻿using HelperClasses;
using Newtonsoft.Json;
using OAP.Domain;
using OAP.Domain.ViewModel;
using OAP.Domain.ViewModel.DataTableViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Xml.Linq;

namespace OAP.Controllers
{
    public class QuestionPaperWizardController : BaseController
    {
        HtmlFormatter htmlConverter = new HtmlFormatter();
        public ActionResult QuestionFromTemplate()
        {
            QuestionWizardViewModel questionWizardViewModel = new QuestionWizardViewModel();
            ViewBag.QuestionPaperTemplate = db.QuestionPaperTemplate!=null?db.QuestionPaperTemplate.OrderBy(q=>q.Name).ToList():new List<QuestionPaperTemplate>();
            return View(questionWizardViewModel);
        }
        public ActionResult Wizard(int id = 0)
        {

            ViewBag.Complexity = db.Complexity.ToList();
            //QuestionPaperViewModel questionPaperViewmodel = new QuestionPaperViewModel();
            //List<TagViewModel> tag1 = MapperHelper.MapToViewModelList(db.Tag);
            //questionPaperViewmodel.tagViewModelList = tag1;
            //SorterHelper sh = new SorterHelper();
            //string result = "false";

            //IQueryable<Tag> tagsList = db.Tag;
            //List<TagViewModel> tagViewModelList = MapperHelper.MapToViewModelList(tagsList);
            //questionPaperViewmodel.tagViewModelList = tagViewModelList);
            return View();
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Wizard(OAP.Domain.ViewModel.QuestionPaperViewModel assessmentViewModel)
        {
            QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentViewModel.ID);
            CultureInfo cultureInfo = new CultureInfo("en-US");
            if (assessmentViewModel.TimeLimit == 0)
            {
                int minutes = (assessmentViewModel.TimeLimitTimeSpan.Hours * 60) + (assessmentViewModel.TimeLimitTimeSpan.Minutes);
                assessmentViewModel.TimeLimit = minutes;
            }
            //string selectedDate = assessmentViewModel.StartDate.ToString("dd/MM/yyy");
            //DateTime cultureBasedDate = DateTime.Parse(selectedDate, cultureInfo);      //Converting all the given  date to US format
            //assessment.StatDateTime = cultureBasedDate;

            //selectedDate = assessmentViewModel.EndDate.ToString("dd/MM/yyy");
            //cultureBasedDate = DateTime.Parse(selectedDate, cultureInfo);
            //assessment.EndDateTime = cultureBasedDate;
            questionPaper = MapperHelper.MapToDomainObject(db, assessmentViewModel, questionPaper);
            if (ModelState.IsValid)
            {
                
                questionPaper.ModifiedBy = GetCurrentUserID();
                questionPaper.ModifiedDate = DateTime.Now;
                db.Entry(questionPaper).State = EntityState.Modified;
                questionPaper.ModifiedBy = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault().UserId;  
                db.SaveChanges();
                assessmentViewModel.ID = questionPaper.ID;
                CreateSectionsforQuestionPaper(assessmentViewModel, questionPaper);
                return RedirectToAction("EditQuestionPaperSections", "QuestionPaperSection", assessmentViewModel);
                //return RedirectToAction("Index");
            }
            assessmentViewModel.questionPaperSectionViewModelList = MapperHelper.MapToViewModelList(questionPaper.QuestionPaperSection);
            assessmentViewModel.SectionCount = assessmentViewModel.questionPaperSectionViewModelList.Count();

            return View(assessmentViewModel);
        }

        public void CreateSectionsforQuestionPaper(OAP.Domain.ViewModel.QuestionPaperViewModel assessmentViewModel, QuestionPaper questionPaper)
        {
            int i = 0;
            List<QuestionPaperSection> questionPaperSection = questionPaper.QuestionPaperSection.ToList();
            if (questionPaperSection.Count() > 0)
            {
                int extraSections = assessmentViewModel.SectionCount - questionPaperSection.Count();
                if (extraSections > 0)
                {
                    i = questionPaperSection.Count();
                }
                else
                {
                    i = -1;
                }
            }
            if (i != -1)
            {
                for (; i < assessmentViewModel.SectionCount; i++)
                {
                    QuestionPaperSection questionPaperSec = new QuestionPaperSection();
                    questionPaperSec.ModifiedDate = DateTime.Now;
                    questionPaperSec.CreatedDate = DateTime.Now;
                    questionPaperSec.CreatedBy = GetCurrentUserID();
                    questionPaperSec.ModifiedBy = GetCurrentUserID();
                    questionPaperSec.QuestionPaper = questionPaper;
                    questionPaperSec.Name = "Section " + (i + 1).ToString();
                    db.QuestionPaperSection.Add(questionPaperSec);
                    db.SaveChanges();
                }
            }


        }

        public void getQuestionPaperByID(string selectedQuestionPaperID)
        {
            long QuestionPaperID = Convert.ToInt64(selectedQuestionPaperID);
            QuestionPaperTemplate questionPaper = db.QuestionPaperTemplate.Find(QuestionPaperID);
            List<QuestionPaperSectionTemplate> questionPaperSectionTemplateList = db.QuestionPaperSectionTemplate.Where(q => q.QuestionPaperTemplateID == QuestionPaperID).ToList();
            List<QuestionPaperSectionTemplateViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionTemplateViewModel>();
            foreach (QuestionPaperSectionTemplate questionPaperSectionTemplate in questionPaperSectionTemplateList)
            {
                QuestionPaperSectionTemplateViewModel questionPaperSectionTemplateViewModel = new QuestionPaperSectionTemplateViewModel();
                questionPaperSectionTemplateViewModel.ID = questionPaperSectionTemplate.ID;
                questionPaperSectionTemplateViewModel.QuestionPaperSectionName = questionPaperSectionTemplate.Name;
                questionPaperSectionTemplateViewModel.TimeLimit = questionPaperSectionTemplate.TimeLimit ?? 0;
                questionPaperSectionTemplateViewModel.SectionCount = questionPaperSectionTemplate.QuestionPaperSectionTemplateRule.Select(q => q.Count).Sum() ?? 0;
                questionPaperSectionViewModelList.Add(questionPaperSectionTemplateViewModel);
                List<QuestionPaperSectionTemplateRule> sectionQuestionList = db.QuestionPaperSectionTemplateRule.Where(q => q.QuestionPaperSectionTemplateID == questionPaperSectionTemplate.ID).ToList();
                questionPaperSectionTemplateViewModel.questionPaperSectionRuleList = new List<QuestionPaperSectionTemplateRuleViewModel>();
                foreach (QuestionPaperSectionTemplateRule sectionTemplate in sectionQuestionList)
                {
                    QuestionPaperSectionTemplateRuleViewModel questionPaperSectionTemplateRuleViewModel = new QuestionPaperSectionTemplateRuleViewModel();
                    int i = 0;
                    foreach (QuestionPaperSectionTemplateRuleComplexity complexity in sectionTemplate.QuestionPaperSectionTemplateRuleComplexity)
                    {
                        if (i == 0)
                        {
                            questionPaperSectionTemplateRuleViewModel.ComplexityString = (complexity.Complexity != null ? complexity.Complexity.ComplexityLevel ?? 0 : 0).ToString();
                            i++;
                        }
                        else
                        {
                            questionPaperSectionTemplateRuleViewModel.ComplexityString += "," + (complexity.Complexity != null ? complexity.Complexity.ComplexityLevel ?? 0 : 0).ToString();
                        }
                    }
                    i = 0;
                    foreach (QuestionPaperSectionTemplateRuleSubject subject in sectionTemplate.QuestionPaperSectionTemplateRuleSubject)
                    {
                        if (i == 0)
                        {
                            questionPaperSectionTemplateRuleViewModel.SubjectString = subject.Subject != null ? subject.Subject.Name : "";
                            i++;
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(questionPaperSectionTemplateRuleViewModel.SubjectString))
                            {
                                questionPaperSectionTemplateRuleViewModel.SubjectString = subject.Subject != null ? subject.Subject.Name : "";
                            }
                            else
                            {
                                questionPaperSectionTemplateRuleViewModel.SubjectString += "," + (subject.Subject != null ? subject.Subject.Name : "");
                            }
                        }
                    }
                    i = 0;
                    foreach (QuestionPaperSectionTemplateRuleTag tag in sectionTemplate.QuestionPaperSectionTemplateRuleTag)
                    {
                        if (i == 0)
                        {
                            questionPaperSectionTemplateRuleViewModel.TagString = tag.Tag != null ? tag.Tag.Name : "";
                            i++;
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(questionPaperSectionTemplateRuleViewModel.TagString))
                            {
                                questionPaperSectionTemplateRuleViewModel.TagString = tag.Tag != null ? tag.Tag.Name : "";
                            }
                            else
                            {
                                questionPaperSectionTemplateRuleViewModel.TagString += "," + (tag.Tag != null ? tag.Tag.Name : "");
                            }
                        }
                    }
                    questionPaperSectionTemplateViewModel.questionPaperSectionRuleList.Add(questionPaperSectionTemplateRuleViewModel);
                }
            }
            string Result = JsonConvert.SerializeObject(questionPaperSectionViewModelList);
            Response.Write(Result);
        }

        public void UploadQuestions()
        {
            var UploadedFile = Request.Files["UploadedFile"];
            long SectionID = Convert.ToInt64(Request["SectionID"]);
            List<HtmlEntities> htmlEntities = GetHtmlEntities();
            string[] notSupportedTags = new string[] { "&rsquo;", " &ldquo;", "&rdquo;", "&lsquo;", " &ndash;", "  &bull;", " &frac;", " &deg;", "&frasl;", "&pi;", " &ang;", " &times;" };
            MashUploadResultViewModel uploadResult = new MashUploadResultViewModel();
            Uploads upload = new Uploads();
            int uploadCount = db.Uploads.Count() + 1;
            //studentViewModel.UploadName = "Upload " + uploadCount;
            upload.Title = "Upload " + uploadCount;
            upload.UploadedDate = DateTime.Now;
            upload.UploadedUserID = GetCurrentUserID();
            db.Uploads.Add(upload);
            db.SaveChanges();
            List<PassageViewModel> passageViewModelList = new List<PassageViewModel>();
            if (UploadedFile != null)
            {
                string guidStirng = Guid.NewGuid().ToString();
                //string newPath = Server.MapPath("~/Upload/" + guidStirng + "/" + guidStirng + ".html");
                if (!Directory.Exists(Server.MapPath("~/Upload/" + guidStirng)))
                    Directory.CreateDirectory(Server.MapPath("~/Upload/" + guidStirng));
                //if (!Directory.Exists(Server.MapPath("~/Upload/" + guidStirng + "/Images")))
                //    Directory.CreateDirectory(Server.MapPath("~/Upload/" + guidStirng + "/Images"));
                string path = Server.MapPath("~/Upload/" + guidStirng + "/" + guidStirng + "." + UploadedFile.FileName.Split('.')[1]);
                string htmlPathTest = Server.MapPath("~/Upload/" + guidStirng + "/" + guidStirng + "_Test.html");
                UploadedFile.SaveAs(htmlPathTest);
                try
                {
                    UploadedFile.SaveAs(path);
                    HelperClasses.Converter converter = new Converter();
                    converter.ConvertDocxToHtml(path);
                }
                finally
                {
                    //objWord.Application.Quit(SaveChanges: false);
                }
                HtmlFormatter htmlConverter = new HtmlFormatter();
                string fileContent = "";
                string htmlPath = Server.MapPath("~/Upload/" + guidStirng) + "/" + guidStirng + ".html";
                fileContent = htmlConverter.ConvertHtmlToString(htmlPath);

                for (int i = 0; i < htmlEntities.Count(); i++)
                {
                    fileContent = fileContent.Replace(htmlEntities[i].HtmlCode, htmlEntities[i].XmlCode);
                }

                StringBuilder sb = new StringBuilder();
                if (!string.IsNullOrEmpty(fileContent))
                {
                    fileContent = htmlConverter.FormatFileContent(fileContent);
                }

                string oldPath = Server.MapPath("~/Upload/" + guidStirng);
                string questionFilePath = Server.MapPath("~/Upload") + "/" + guidStirng + "/" + guidStirng + "." + UploadedFile.FileName.Split('.')[1];

//                string azureImageNewPath = AzureHelper.UploadToAzureStorage(questionFilePath, "OAP/Upload/Questions", false);
                string azureImageNewPath = "";

                //passageViewModelList = CreatePassageQuestionsAnswers(fileContent, guidStirng, oldPath, azureImageNewPath);
                uploadResult = CreatePassageQuestionsAnswers(fileContent, guidStirng, oldPath, upload.ID, azureImageNewPath, upload, SectionID);

                string deletePath = Server.MapPath("~/Upload/" + guidStirng);
                var dir = new DirectoryInfo(deletePath);
                dir.Delete(true);

                //Response.Write("True");
                //return RedirectToAction("PreviewUploadedQuestions", new RouteValueDictionary(new { controller = "Upload", action = "PreviewUploadedQuestions", id = upload.ID }));
            }
            Response.Write("True");
            //return View(studentViewModel);
            // return View("~/Views/UploadResult", passageViewModelList);
        }

        public List<HtmlEntities> GetHtmlEntities()
        {
            List<HtmlEntities> htmlEntities = new List<HtmlEntities>();
            htmlEntities.Add(new HtmlEntities("&rsquo;", "&#8217;"));
            htmlEntities.Add(new HtmlEntities("&ldquo;", "&#8220;"));
            htmlEntities.Add(new HtmlEntities("&rdquo;", "&#8221;"));
            htmlEntities.Add(new HtmlEntities("&lsquo;", "&#8216;"));
            htmlEntities.Add(new HtmlEntities("&ndash;", "&#8211;"));
            htmlEntities.Add(new HtmlEntities("&bull;", "&#8226;"));
            // htmlEntities.Add(new HtmlEntities("&frac;", ""));
            htmlEntities.Add(new HtmlEntities("&deg;", "&#176;"));
            htmlEntities.Add(new HtmlEntities("&frasl;", "&#8260;"));
            htmlEntities.Add(new HtmlEntities("&pi;", "&#928;"));
            htmlEntities.Add(new HtmlEntities("&ang;", "&#8736;"));
            htmlEntities.Add(new HtmlEntities("&times;", "&#215;"));
            htmlEntities.Add(new HtmlEntities("&hellip", "&#8230;"));
            htmlEntities.Add(new HtmlEntities("&pound", "&#1234;"));
            htmlEntities.Add(new HtmlEntities("&radic;", "&#8730;"));
            htmlEntities.Add(new HtmlEntities("&loz;", "&#9674;"));

            return htmlEntities;

        }
        public MashUploadResultViewModel CreatePassageQuestionsAnswers(string fileContent, string guidString, string oldPath, long uploadedID, string questionAzurePath, Uploads upload, long SectionID=0)
        {
            List<HtmlEntities> htmlEntities = GetHtmlEntities();
            MashUploadResultViewModel uploadResult = new MashUploadResultViewModel();
            int processPassage = 0;
            bool isContinueQuestion = false;
            bool isContinueAnswer = false;
            bool isNewPassage = true;

            Passage currentPassage = new Passage();
            Question currentQuestion = new Question();
            List<Answer> answers = new List<Answer>();
            StringBuilder sb = new StringBuilder();

            List<PassageViewModel> passageViewModelList = new List<PassageViewModel>();
            List<QuestionViewModel> questionViewModelList = new List<QuestionViewModel>();
            PassageViewModel passageViewModel = new PassageViewModel();
            QuestionViewModel questionViewModel = new QuestionViewModel();
            List<AnswerViewModel> answerViewModelList = new List<AnswerViewModel>();

            int passageStartIndex = 0, passageEndIndex = 0;

            if (!string.IsNullOrEmpty(fileContent))
            {
                XDocument document = XDocument.Parse(fileContent);
                for (int i = 0; i < htmlEntities.Count(); i++)
                {
                    fileContent = fileContent.Replace(htmlEntities[i].XmlCode, htmlEntities[i].HtmlCode);
                }
                XElement root = document.Elements().FirstOrDefault().Elements().FirstOrDefault();
                root.Elements().FirstOrDefault().Remove();
                {
                    int totalQuestions = root.Elements().Where(e => !(e.Elements().First().Value.Contains("-"))).Count() / 2;
                    foreach (XElement tr in root.Elements().FirstOrDefault().Elements().FirstOrDefault().Elements())
                    {
                        if (tr.Value.Trim() == "")
                        {
                            continue;
                        }
                        if (tr.Name.LocalName.ToLower() == "tr")
                        {
                            answers = new List<Answer>();
                            List<XElement> columns = tr.Elements().ToList();
                            int count = columns.Count;
                            var node = (columns[0].Elements().ToList()[0].FirstNode);

                            if (processPassage == 0)
                            {                               //Processing first line
                            //var currentNode = (XElement)node;

                            //if (!string.IsNullOrEmpty(currentNode.Value.ToString()))
                            //{
                            ContinuePassage:
                                if (isNewPassage)
                                {                                                                   //Questions and answers saved
                                    if (columns[0].Value.ToString().Contains("-"))
                                    {                                                               //Having passage
                                        passageViewModel = new PassageViewModel();
                                        passageStartIndex = Convert.ToInt16(columns[0].Value.Split('-')[0]);
                                        passageEndIndex = Convert.ToInt16(columns[0].Value.Split('-')[1]);

                                        currentPassage = SavePassage(columns, guidString);

                                        isContinueQuestion = true;
                                        isNewPassage = false;
                                        isContinueAnswer = false;
                                        passageViewModel = MapperHelper.MapToViewModel(currentPassage);
                                        continue;
                                    }
                                    else
                                    {
                                        isContinueQuestion = true;
                                        isNewPassage = false;
                                        isContinueAnswer = false;
                                        goto ContinueQuestion;
                                    }
                                }
                            ContinueQuestion:
                                if (isContinueQuestion)
                                {                                                             //Continue to save Question
                                    questionViewModel = new QuestionViewModel();

                                    currentQuestion = SaveQuestion(columns, currentPassage, upload, questionAzurePath, guidString, oldPath, passageStartIndex, passageEndIndex, SectionID);
                                    questionViewModel = MapperHelper.MapToViewModel(currentQuestion);
                                    questionViewModelList.Add(questionViewModel);

                                    isContinueQuestion = false;
                                    isNewPassage = false;
                                    isContinueAnswer = true;
                                    continue;
                                }
                                if (isContinueAnswer)
                                {
                                    if (string.IsNullOrEmpty(columns[0].Value.ToString()) || columns[0].Value.Trim().ToString().ToLower() == "c" || columns[0].Value.ToString() == " ")
                                    {                                                                                                   //Answer sections
                                        var reader = columns[1].CreateReader();
                                        reader.MoveToContent();
                                        string answerDescription = reader.ReadInnerXml();

                                        QuestionType questionType;
                                        if (string.IsNullOrWhiteSpace(answerDescription))
                                        {                                       //Question has no answers . Question type is OpenEndedQuestion
                                            questionType = db.QuestionType.Where(qt => qt.Name.ToLower().Contains("open ended")).FirstOrDefault();
                                            currentQuestion.QuestionType = questionType;
                                            db.SaveChanges();
                                            continue;
                                        }
                                        else
                                        {                                       //Question have answers. 
                                            AnswerViewModel answerViewModel = new AnswerViewModel();
                                            answerViewModel = AddAnswerForQuestion(columns, currentQuestion, guidString, oldPath);
                                            answerViewModelList.Add(answerViewModel);
                                            continue;
                                        }
                                    }
                                    else if (columns[0].Value.Trim().ToString().ToLower() == "s")
                                    {                                                                   //Solution for the question
                                        AddSolutionForQuestion(columns, currentQuestion);
                                        continue;
                                    }
                                    else if (columns[0].Value.Trim().ToString().ToLower() == "t")
                                    {                                                                           //Column containing tags
                                        CreateQuestionTags(columns, currentQuestion);
                                        continue;
                                    }
                                    else
                                    {                                                                       //Save answer completed. Continue to New passage or new question
                                        if (currentQuestion.ID != 0 && currentQuestion.QuestionType == null)
                                            UpdateQuestionType(currentQuestion);
                                        isContinueQuestion = false;
                                        isNewPassage = true;
                                        isContinueAnswer = false;
                                        goto ContinuePassage;
                                        //if (columns[0].Value.ToString().Contains("-"))
                                        //{                                                               //Having passage

                                        //    isContinueQuestion = false;
                                        //    isNewPassage = true;
                                        //    isContinueAnswer = false;
                                        //    goto ContinuePassage;
                                        //}
                                        //else
                                        //{
                                        //    passageViewModel = MapperHelper.MapToViewModel(currentPassage);
                                        //    passageViewModelList.Add(passageViewModel);
                                        //    isContinueQuestion = true;
                                        //    isNewPassage = false;
                                        //    isContinueAnswer = false;
                                        //    goto ContinueQuestion;
                                        //    //continue;
                                        //}
                                    }
                                }
                            }
                        }
                    }
                }
            }
            UpdateQuestionType(currentQuestion);

            string questionIDs = "";
            foreach (QuestionViewModel question in questionViewModelList)
            {
                question.AnswerViewModelList = answerViewModelList.Where(a => a.QuestionID == question.ID).ToList();
                questionIDs += question.ID + ",";
            }
            uploadResult.questionViewModelList = questionViewModelList;
            uploadResult.UploadedQuestionIDs = questionIDs;
            return uploadResult;
            //return passageViewModelList;
        }

        public void UpdateQuestionType(Question currentQuestion)
        {
            QuestionType questionType;
            if (currentQuestion.Answer.Where(a => a.IsCorrect ?? false).Count() > 1)
            {                                                                           //Question have multiple answers
                questionType = db.QuestionType.Where(qt => qt.Name.ToLower().Contains("multi select")).FirstOrDefault();
            }
            else
            {                                                                           //Qustion have only one answer
                questionType = db.QuestionType.Where(qt => qt.Name.ToLower().Contains("single select")).FirstOrDefault();

            }
            if (questionType != null)
            {
                currentQuestion.QuestionType = questionType;
              //  currentQuestion.Description = htmlConverter.FormatFileContent(currentQuestion.Description);
                currentQuestion.Description = currentQuestion.Description;

                db.Entry(currentQuestion).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public AnswerViewModel AddAnswerForQuestion(List<XElement> columns, Question currentQuestion, string guidString, string oldPath)
        {
            Answer answer = new Answer();
            string answerDescription = "";

            var reader = columns[1].CreateReader();
            reader.MoveToContent();
            answerDescription = reader.ReadInnerXml();

            answer.Description = answerDescription;

            if (columns[0].Value.ToString().ToLower() == "c")
            {
                answer.IsCorrect = true;
            }
            else
                answer.IsCorrect = false;
            answer.Description = htmlConverter.FormatFileContent(answer.Description);
           // answer.Description = answer.Description;

            answer.Question = currentQuestion;
            answer.LastUpdated = DateTime.Now;
            db.Answer.Add(answer);
            db.SaveChanges();

            List<string> imageSourceStringList = new List<string>();
            foreach (var element in columns[1].Elements())
            {
                if (element.ToString().Contains("img"))
                {                                                                                       //Question having image
                    var imageNodes = columns[1].Elements().FirstOrDefault().Elements().ToList();
                    string actualSource = Server.MapPath("~/Upload/") + guidString + "/" + guidString + "_files";
                    int processing = 0;
                    var tempElement = imageNodes;
                    int i = 0;
                    foreach (var item in imageNodes)
                    {
                        if (item.ToString().Contains("img"))
                        {
                            string imageSource = item.Attribute("src").Value;
                            int totalCount = imageSource.Split('/').Count();
                            string imageName = imageSource.Split('/')[totalCount - 1];
                            string destinationPath = "/Upload/Answer/Images/" + currentQuestion.ID;
                            string filePath = actualSource + "/" + imageName;
                            //                string azureImageNewPath = AzureHelper.UploadToAzureStorage(questionFilePath, "OAP/Upload/Questions", false);
                            string azureImageNewPath = ""; 
                            //string azureImageNewPath = AzureHelper.UploadToAzureStorage(filePath, destinationPath, true);
                            processing++;
                            imageSourceStringList.Add(azureImageNewPath);
                            i++;
                        }
                    }
                }
            }
            foreach (string newImageSource in imageSourceStringList)
            {
                string searchString = oldPath + @"\" + guidString + "_files";
                searchString = searchString.Replace(@"\\", @"\");
                int place = answerDescription.IndexOf(searchString);

                if (place > 0 && !string.IsNullOrEmpty(newImageSource))
                {
                    List<string> source = newImageSource.Split('/').ToList();
                    source.RemoveAt(source.Count() - 1);
                    string alteredImageSource = string.Join("/", source.ToArray());

                    answerDescription = answerDescription.Remove(place, searchString.Length).Insert(place, alteredImageSource);
                }
            }

            answer.Description = answerDescription;
            //answer.Description = htmlConverter.FormatFileContent(answer.Description);
            db.Entry(answer).State = EntityState.Modified;
            db.SaveChanges();

            AnswerViewModel answerViewModel = new AnswerViewModel();
            answerViewModel = MapperHelper.MapToViewModel(answer);
            return answerViewModel;
        }

        public void AddSolutionForQuestion(List<XElement> columns, Question currentQuestion)
        {
            if (!String.IsNullOrEmpty(columns[1].Value.ToString()))
            {
                string solutionString = "";
                //foreach (var element in columns[1].Elements())
                //{
                //    solutionString = solutionString + element.ToString();
                //}

                var reader = columns[1].CreateReader();
                reader.MoveToContent();
                solutionString = reader.ReadInnerXml();
                currentQuestion.Solution = solutionString;
                currentQuestion.AdminId = GetCurrentUserID();
                currentQuestion.LastUpdated = DateTime.Now;
                ///currentQuestion.Description = htmlConverter.FormatFileContent(currentQuestion.Description);
                currentQuestion.Description = currentQuestion.Description;

                db.Entry(currentQuestion).State = EntityState.Modified;
                db.SaveChanges();
            }
        }

        public void CreateQuestionTags(List<XElement> columns, Question currentQuestion)
        {
            if (!string.IsNullOrEmpty(columns[1].Value.ToString()))
            {
                string tagString = columns[1].Value.ToString();
                List<string> tagList = tagString.Split(',').ToList();
                foreach (string newTag in tagList)
                {
                    Tag tags = db.Tag.Where(t => t.Name.ToLower() == newTag.ToLower()).FirstOrDefault();
                    if (tags == null)
                    {
                        tags = new Tag();
                        tags.Name = newTag;
                        db.Tag.Add(tags);
                        db.SaveChanges();
                    }
                    if (tags != null)
                    {
                        QuestionTags questionTags = new QuestionTags();
                        questionTags.Question = currentQuestion;
                        questionTags.Tag = tags;
                        questionTags.CreatedBy = GetCurrentUserID();
                        questionTags.CreatedDate = DateTime.Now;
                        db.QuestionTags.Add(questionTags);
                    }
                }
                db.SaveChanges();
            }
        }

        public Passage SavePassage(List<XElement> columns, string guidString)
        {
            var reader = columns[1].CreateReader();
            reader.MoveToContent();
            string passageString = reader.ReadInnerXml();

            Passage currentPassage = new Passage();
            currentPassage.CanShuffle = true;
            currentPassage.CreatedDate = DateTime.Now;
            currentPassage.CreatedBy = currentPassage.ModifiedBy = GetCurrentUserID();
            currentPassage.ModifiedDate = DateTime.Now;
            currentPassage.LastUpdated = DateTime.Now;
          //  currentPassage.Description = htmlConverter.FormatFileContent(currentPassage.Description);
            currentPassage.Description = currentPassage.Description;
            db.Passage.Add(currentPassage);
            db.SaveChanges();

            List<string> imageSourceStringList = new List<string>();
            foreach (var element in columns[1].Elements())
            {
                if (element.ToString().Contains("img"))
                {                                                                                       //Question having image
                    var imageNodes = columns[1].Descendants("img");
                    var newImageNodes = columns[1].Descendants("img").ToList();
                    string actualSource = Server.MapPath("~/Upload/") + guidString + "/" + guidString + "_files";
                    int processing = 0;
                    foreach (var item in imageNodes)
                    {
                        string imageSource = item.Attribute("src").Value;
                        int totalCount = imageSource.Split('/').Count();
                        string imageName = imageSource.Split('/')[totalCount - 1];
                        string destinationPath = "OAP/Upload/Passage" + currentPassage.ID;
                        string filePath = actualSource + "/" + imageName;
                        //string azureImageNewPath = AzureHelper.UploadToAzureStorage(filePath, destinationPath, true);
                        //                string azureImageNewPath = AzureHelper.UploadToAzureStorage(questionFilePath, "OAP/Upload/Questions", false);
                        string azureImageNewPath = "";
                        newImageNodes[0].Attribute("src").Value = azureImageNewPath;
                        processing++;
                        imageSourceStringList.Add(azureImageNewPath);
                    }
                }
            }
            foreach (string newImageSource in imageSourceStringList)
            {
                string searchString = guidString + "_files";
                int place = passageString.IndexOf(searchString);

                if (place > 0 && !string.IsNullOrEmpty(newImageSource))
                {
                    List<string> source = newImageSource.Split('/').ToList();
                    source.RemoveAt(source.Count() - 1);
                    string alteredImageSource = string.Join("/", source.ToArray());
                    passageString = passageString.Remove(place, searchString.Length).Insert(place, alteredImageSource);
                }
            }
            currentPassage.Description = passageString;

         //   currentPassage.Description = htmlConverter.FormatFileContent(currentPassage.Description);
            currentPassage.Description = currentPassage.Description;

            db.Entry(currentPassage).State = EntityState.Modified;
            db.SaveChanges();
            return currentPassage;
        }

        public Question SaveQuestion(List<XElement> columns, Passage currentPassage, Uploads upload, string questionAzurePath, string guidString, string oldPath, int passageStartIndex, int passageEndIndex,long SectionID=0)
        {
            Question currentQuestion;
            int questionNumber;
            Int32.TryParse(columns[0].Value.ToString(), out questionNumber);
            //int questionNumber = Convert.ToInt16(columns[0].Value.ToString());
            string questionString = "";
            var reader = columns[1].CreateReader();
            reader.MoveToContent();
            questionString = reader.ReadInnerXml();

            currentQuestion = new Question();
            if (questionNumber >= passageStartIndex && questionNumber <= passageEndIndex)
                currentQuestion.Passage = currentPassage;
            currentQuestion.Description = questionString;
            currentQuestion.Uploads = upload;
            currentQuestion.LastUpdated = DateTime.Now;
            currentQuestion.AdminId = GetCurrentUserID();
            currentQuestion.DownloadLink = questionAzurePath;
            currentQuestion.AdminId = GetCurrentUserID();
           // currentQuestion.Description = htmlConverter.FormatFileContent(currentQuestion.Description);
            currentQuestion.Description = currentQuestion.Description;

            db.Question.Add(currentQuestion);
            //db.SaveChanges();

            int QuestionPaperSectionID = Convert.ToInt32(SectionID);
            QuestionPaperSectionQuestion questionPaperSectionQuestion = new QuestionPaperSectionQuestion();
            questionPaperSectionQuestion.AssessmentSectionID = QuestionPaperSectionID;
            questionPaperSectionQuestion.QuestionID = currentQuestion.ID;

            db.QuestionPaperSectionQuestion.Add(questionPaperSectionQuestion);
            db.SaveChanges();

            List<string> imageSourceStringList = new List<string>();
            foreach (var element in columns[1].Elements())
            {
                if (element.ToString().Contains("img"))
                {                                                                                       //Question having image
                    var imageNodes = columns[1].Elements().FirstOrDefault().Elements().ToList();
                    string actualSource = Server.MapPath("~/Upload/") + guidString + "/" + guidString + "_files";
                    int processing = 0;
                    var tempElement = imageNodes;
                    int i = 0;
                    foreach (var item in imageNodes)
                    {
                        if (item.ToString().Contains("img"))
                        {
                            string imageSource = item.Attribute("src").Value;
                            int totalCount = imageSource.Split('/').Count();
                            string imageName = imageSource.Split('/')[totalCount - 1];
                            string destinationPath = "/Upload/Question/Images/" + currentQuestion.ID;
                            string filePath = actualSource + "/" + imageName;
                           // string azureImageNewPath = AzureHelper.UploadToAzureStorage(filePath, destinationPath, true);
                            //                string azureImageNewPath = AzureHelper.UploadToAzureStorage(questionFilePath, "OAP/Upload/Questions", false);
                            string azureImageNewPath = "";
                            processing++;
                            imageSourceStringList.Add(azureImageNewPath);
                            i++;
                        }
                    }
                }
            }
            foreach (string newImageSource in imageSourceStringList)
            {
                //oldPath
                //string searchString = guidString + "_files";
                string searchString = oldPath + @"\" + guidString + "_files";
                searchString = searchString.Replace(@"\\", @"\");
                int place = questionString.IndexOf(searchString);

                if (place > 0 && !string.IsNullOrEmpty(newImageSource))
                {
                    List<string> source = newImageSource.Split('/').ToList();
                    source.RemoveAt(source.Count() - 1);
                    string alteredImageSource = string.Join("/", source.ToArray());

                    questionString = questionString.Remove(place, searchString.Length).Insert(place, alteredImageSource);
                }
            }
            currentQuestion.Description = questionString;
            currentQuestion.LastUpdated = DateTime.Now;
            currentQuestion.AdminId = GetCurrentUserID();
           // currentQuestion.Description = htmlConverter.FormatFileContent(currentQuestion.Description);
            currentQuestion.Description = currentQuestion.Description;

            db.Entry(currentQuestion).State = EntityState.Modified;
            db.SaveChanges();
            return currentQuestion;
        }

        public void CreateQuestionPaper(int questionID=0, string questionName="")
        {
            //try
            //{
                long QuestionPaperID = Convert.ToInt64(questionID);
                QuestionPaperTemplate questionPapertemplate = db.QuestionPaperTemplate.Find(QuestionPaperID);
                List<QuestionPaperSectionTemplate> questionPaperSectionTemplateList = db.QuestionPaperSectionTemplate.Where(q => q.QuestionPaperTemplateID == QuestionPaperID).ToList();
                         int questionCount = 0;
                         int ruleCount = 0;
                foreach (QuestionPaperSectionTemplate questionPaperSectionTemplate in questionPaperSectionTemplateList)
                {
                    List<QuestionPaperSectionTemplateRule> QuestionPaperSectionTemplateRuleList = db.QuestionPaperSectionTemplateRule.Where(q => q.QuestionPaperSectionTemplateID == questionPaperSectionTemplate.ID).ToList();
                    foreach (QuestionPaperSectionTemplateRule sectionTemplateRule in QuestionPaperSectionTemplateRuleList)
                    {
                      ruleCount+=sectionTemplateRule.Count??0;
                        List<Tag> tags = sectionTemplateRule.QuestionPaperSectionTemplateRuleTag.Select(q => q.Tag).ToList();
                        List<Subject> Subjects = sectionTemplateRule.QuestionPaperSectionTemplateRuleSubject.Select(q => q.Subject).ToList();
                        List<Complexity> Complexities = sectionTemplateRule.QuestionPaperSectionTemplateRuleComplexity.Select(q => q.Complexity).ToList();
                        List<Question> questionCheckList=new List<Question>();
                        foreach (Tag tag in tags)
                        {
                            questionCheckList = db.QuestionTags.Where(q => q.TagID == tag.ID).Select(q=>q.Question).ToList();
                            foreach (Subject subj in Subjects)
                            {
                                questionCheckList = questionCheckList.Where(q => q.SubjectID == subj.ID).ToList();
                                foreach (Complexity complexity in Complexities)
                                {
                                    if (complexity != null)
                                    {
                                        questionCheckList = questionCheckList.Where(q => q.ComplexityID == complexity.ID).ToList();
                                    }
                                }
                            }
                        }
                        if (questionCheckList != null)
                        {
                            questionCount += questionCheckList.Count();
                        }
                        else
                        {
                            questionCount +=0;
                        }
                    }
                }
                if (questionCount >= ruleCount)
                {
                QuestionPaper questionPaper = new QuestionPaper();
                questionPaper.Name = questionName;
                questionPaper.Code = questionPapertemplate.Code;
                questionPaper.Description = questionPapertemplate.Description;
                questionPaper.CutOffMark = questionPapertemplate.CutOffMark;
                questionPaper.DisplayMarks = questionPapertemplate.DisplayMarks;
                questionPaper.DisplayResult = questionPapertemplate.DisplayResult;
                questionPaper.TimeLimit = questionPapertemplate.TimeLimit;
                questionPaper.Marks = questionPapertemplate.Marks;
                questionPaper.NegativeMarks = questionPapertemplate.NegativeMarks;
                questionPaper.IsNegativePercentage = questionPapertemplate.IsNegativePercentage;
                questionPaper.Instructions = questionPapertemplate.Instructions;
                questionPaper.ReleaseKey = questionPapertemplate.ReleaseKey;
                questionPaper.CreatedBy = GetCurrentUserID();
                questionPaper.CreatedDate = DateTime.Now;
                questionPaper.QuestionPaperTemplateID = QuestionPaperID;
                db.QuestionPaper.Add(questionPaper);
                db.SaveChanges();
                List<QuestionPaperSectionTemplateViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionTemplateViewModel>();
                foreach (QuestionPaperSectionTemplate questionPaperSectionTemplate in questionPaperSectionTemplateList)
                {
                    QuestionPaperSectionTemplateViewModel questionPaperSectionTemplateViewModel = new QuestionPaperSectionTemplateViewModel();
                    questionPaperSectionTemplateViewModel.ID = questionPaperSectionTemplate.ID;
                    questionPaperSectionTemplateViewModel.QuestionPaperSectionName = questionPaperSectionTemplate.Name;
                    questionPaperSectionTemplateViewModel.TimeLimit = questionPaperSectionTemplate.TimeLimit ?? 0;
                    questionPaperSectionTemplateViewModel.SectionCount = questionPaperSectionTemplate.QuestionPaperSectionTemplateRule.Select(q => q.Count).Sum() ?? 0;
                    questionPaperSectionViewModelList.Add(questionPaperSectionTemplateViewModel);
                    List<QuestionPaperSectionTemplateRule> QuestionPaperSectionTemplateRuleList = db.QuestionPaperSectionTemplateRule.Where(q => q.QuestionPaperSectionTemplateID == questionPaperSectionTemplate.ID).ToList();
                    questionPaperSectionTemplateViewModel.questionPaperSectionRuleList = new List<QuestionPaperSectionTemplateRuleViewModel>();

                    QuestionPaperSection questionPaperSection = new QuestionPaperSection();
                    questionPaperSection.AssessmentID = questionPaper.ID;
                    questionPaperSection.CreatedBy = GetCurrentUserID();
                    questionPaperSection.CreatedDate = DateTime.Now;
                    questionPaperSection.CutOffMark = questionPaperSectionTemplate.CutOffMark;
                    questionPaperSection.Description = questionPaperSectionTemplate.Description;
                    questionPaperSection.IsNegativeMarkPercentage = questionPaperSectionTemplate.IsNegativeMarkPercentage;
                    questionPaperSection.Mark = questionPaperSectionTemplate.Mark;
                    questionPaperSection.Name = questionPaperSectionTemplate.Name;
                    questionPaperSection.TimeLimit = questionPaperSectionTemplate.TimeLimit;
                    questionPaperSection.NegativeMark = questionPaperSectionTemplate.NegativeMark;
                    db.QuestionPaperSection.Add(questionPaperSection);
                    db.SaveChanges();

                    foreach (QuestionPaperSectionTemplateRule sectionTemplateRule in QuestionPaperSectionTemplateRuleList)
                    {
                        List<Tag> tags = sectionTemplateRule.QuestionPaperSectionTemplateRuleTag.Select(q => q.Tag).ToList();
                        List<Subject> Subjects = sectionTemplateRule.QuestionPaperSectionTemplateRuleSubject.Select(q => q.Subject).ToList();
                        List<Complexity> Complexities = sectionTemplateRule.QuestionPaperSectionTemplateRuleComplexity.Select(q => q.Complexity).ToList();
                        for (int i = 0; i < sectionTemplateRule.Count; i++)
                        {
                            var random = new Random();
                            var Tagitems=new Tag();
                            var Subjectitems = new Subject();
                            var Complexityitems = new Complexity();
                            if (tags.Count() != 0)
                            {
                            var Tag = new List<Tag>();
                            var query = from item in tags
                                        orderby random.Next()
                                        select item;

                                Tagitems = query.FirstOrDefault();
                            }
                            if (Subjects.Count() != 0)
                            {
                            var subjects = new List<Subject>();
                            var Subjectquery = from item in Subjects
                                               orderby random.Next()
                                               select item;
                                Subjectitems = Subjectquery.FirstOrDefault();
                            }
                            if (Complexities.Count() != 0)
                            {
                            var complexities = new List<Complexity>();
                            var Complexityquery = from item in Complexities
                                                  orderby random.Next()
                                                  select item;
                                Complexityitems = Complexityquery.FirstOrDefault();
                            }
                            long complexdityID=(Complexityitems!=null?Complexityitems.ID:0);

                            List<Question> questionList = new List<Question>();

                            if (Tagitems.ID != 0 && Subjectitems.ID != 0 && complexdityID != 0)
                            {
                                questionList = db.Question.Where(q => q.QuestionTags.Where(qt => qt.TagID == Tagitems.ID).Count() > 0 && q.SubjectID == Subjectitems.ID && q.ComplexityID == complexdityID).ToList(); 
                            }
                            else if (Tagitems.ID != 0 && Subjectitems.ID == 0 && complexdityID == 0)
                            {
                                questionList = db.Question.Where(q => q.QuestionTags.Where(qt => qt.TagID == Tagitems.ID).Count() > 0).ToList(); 
                            }
                            else if (Tagitems.ID != 0 && Subjectitems.ID != 0 && complexdityID == 0)
                            {
                                questionList = db.Question.Where(q => q.QuestionTags.Where(qt => qt.TagID == Tagitems.ID).Count() > 0 && q.SubjectID == Subjectitems.ID).ToList(); 
                            }
                            else if (Tagitems.ID != 0 && Subjectitems.ID == 0 && complexdityID != 0)
                            {
                                questionList = db.Question.Where(q => q.QuestionTags.Where(qt => qt.TagID == Tagitems.ID).Count() > 0 && q.ComplexityID == complexdityID).ToList(); 
                            }

                            //List<Question> questionList = db.Question.Where(q => q.QuestionTags.Where(qt => qt.TagID == Tagitems.ID).Count() > 0 && q.SubjectID == Subjectitems.ID && q.ComplexityID == complexdityID).ToList(); 
                            if (questionList.Count() > 0)
                            {
                                var result = new List<Question>();
                                var Questionquery = from item in questionList
                                                    orderby random.Next()
                                                    select item;
                                var question = Questionquery.FirstOrDefault();
                                QuestionPaperSectionQuestion questionPaperSectionQuestion = new QuestionPaperSectionQuestion();
                                questionPaperSectionQuestion.AssessmentSectionID = questionPaperSection.ID;
                                questionPaperSectionQuestion.QuestionID = question.ID;
                                questionPaperSectionQuestion.Mark = question.Marks;
                                questionPaperSectionQuestion.NegativeMark = question.NegativeMarks;
                                db.QuestionPaperSectionQuestion.Add(questionPaperSectionQuestion);
                                db.SaveChanges();
                            }
                        }
                    }
                }
                Response.Write(true);
            }
                else
                {
                    Response.Write("count exceed");
                }
            //}
            //catch
            //{
            //    Response.Write(false);

            //}
        }


        public void SaveQuestionPaper(QuestionPaperViewModel questionPaperViewModel)
        {
            string result = "false";

            try
            {
                QuestionPaper questionPaper ;
                if (questionPaperViewModel.ID == 0)
                {
                    questionPaper= new QuestionPaper();
                }
                else
                {
                    questionPaper=db.QuestionPaper.Find(questionPaperViewModel.ID);
                }

                int minutes = ((questionPaperViewModel.TimeLimitTimeSpan.Hours == 0 ? questionPaperViewModel.TimeLimitTimeSpan.Days : questionPaperViewModel.TimeLimitTimeSpan.Hours * 60)) + (questionPaperViewModel.TimeLimitTimeSpan.Minutes);
                    questionPaperViewModel.TimeLimit = minutes;
                

                questionPaper.Code = questionPaperViewModel.Code;
                questionPaper.CutOffMark = questionPaperViewModel.CutOffMark;
                questionPaper.Description = questionPaperViewModel.Description;
                questionPaper.DisplayMarks = questionPaperViewModel.DisplayMarks;
                questionPaper.DisplayResult = questionPaperViewModel.DisplayResult;
                questionPaper.Marks = questionPaperViewModel.Marks;
                questionPaper.Name = questionPaperViewModel.Name;
                questionPaper.NegativeMarks = questionPaperViewModel.NegativeMarks;
                questionPaper.TimeLimit = questionPaperViewModel.TimeLimit;
                questionPaper.CreatedBy = questionPaper.ModifiedBy = GetCurrentUserID();
                questionPaper.ModifiedDate = questionPaper.CreatedDate = DateTime.Now;
                questionPaper.CanJumpSections = questionPaperViewModel.CanJumpSections;
                questionPaper.ShowInstruction = questionPaperViewModel.ShowInstructions;
                questionPaper.Instructions = questionPaperViewModel.Instructions;
                questionPaper.ShowReport = questionPaperViewModel.ShowReport;
                questionPaper.IsHaveRetestRequest = questionPaperViewModel.IsHaveRetestRequest;
                questionPaper.CanShuffle = questionPaperViewModel.CanShuffle;
               


                if (questionPaperViewModel.ID == 0)
                {
                    db.QuestionPaper.Add(questionPaper);
                }
                else
                {
                    db.Entry(questionPaper).State = EntityState.Modified;
                }
                db.SaveChanges();

                QuestionPaperSection questionPaperSection = new QuestionPaperSection();
                int sectionCount = questionPaperViewModel.NoOfSections;
                int existingSectionCount = questionPaper.QuestionPaperSection.Count;
                int sectiontobeCreated = 0;
                if (sectionCount < existingSectionCount)
                {
                    sectiontobeCreated = existingSectionCount - sectionCount;

                }
                else
                {
                    sectiontobeCreated = sectionCount - existingSectionCount;

                }
                questionPaperViewModel = MapperHelper.MapToViewModel(questionPaper);
                questionPaperViewModel.questionPaperSectionViewModelList = new List<QuestionPaperSectionViewModel>();
                if (sectionCount < existingSectionCount)
                {
                   // for (int i = 0; i < sectiontobeCreated; i++)
                   // {
                        questionPaperSection = new QuestionPaperSection();
                       // questionPaperSection = db.QuestionPaperSection.Where(m => m.QuestionPaperID == questionPaper.ID).LastOrDefault();
                       //List<QuestionPaperSectionQuestion> questionPaperSectionQuestionlist =db.QuestionPaperSectionQuestion.Where(d=>d.QuestionPaperSectionID==questionPaperSection.ID).ToList();
                       //db.QuestionPaperSectionQuestion.RemoveRange(questionPaperSectionQuestionlist);
                       // db.QuestionPaperSection.Remove(questionPaperSection);
                       // db.SaveChanges();

                        IEnumerable<QuestionPaperSection> itemstobeDeleted=questionPaper.QuestionPaperSection.Skip(Math.Max(0, questionPaper.QuestionPaperSection.Count() - sectiontobeCreated));
                        IEnumerable<QuestionPaperSectionQuestion> QpsqtobeDeleted = itemstobeDeleted.SelectMany(x => x.QuestionPaperSectionQuestion);
                        db.QuestionPaperSectionQuestion.RemoveRange(QpsqtobeDeleted);
                        db.QuestionPaperSection.RemoveRange(itemstobeDeleted);
                        db.SaveChanges();
                   // }
                    //QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
                    //questionPaperSectionViewModel.ID = questionPaperSection.ID;
                    //questionPaperSectionViewModel.Name = questionPaperSection.Name;
                    questionPaperViewModel.questionPaperSectionViewModelList=MapperHelper.MapToViewModelList(questionPaper.QuestionPaperSection);
                }
                else
                {
                    for (int i = 0; i < sectiontobeCreated; i++)
                    {
                   
                        questionPaperSection = new QuestionPaperSection();
                        questionPaperSection.AssessmentID = questionPaper.ID;
                        questionPaperSection.Name = "Section " + (1 + i + existingSectionCount);
                        questionPaperSection.CreatedBy = questionPaperSection.ModifiedBy = GetCurrentUserID();
                        questionPaperSection.ModifiedDate = questionPaperSection.CreatedDate = DateTime.Now;
                        db.QuestionPaperSection.Add(questionPaperSection);
                        db.SaveChanges();
                        QuestionPaperSectionViewModel questionPaperSectionViewModel = new QuestionPaperSectionViewModel();
                        questionPaperSectionViewModel.ID = questionPaperSection.ID;
                        questionPaperSectionViewModel.Name = questionPaperSection.Name;
                        questionPaperViewModel.questionPaperSectionViewModelList.Add(questionPaperSectionViewModel);
                    }
                }
                
                List<TagViewModel> tag1 = MapperHelper.MapToViewModelList(db.Tag);
                questionPaperViewModel.tagViewModelList = tag1;
                questionPaperViewModel.NoOfSections = questionPaper.QuestionPaperSection.Count();

                if (questionPaperViewModel.TimeLimit != 0)
                    questionPaperViewModel.IsShowSectionTimeLimit = false;
                else
                    questionPaperViewModel.IsShowSectionTimeLimit = true;

                result = JsonConvert.SerializeObject(questionPaperViewModel);
            }
            catch (Exception e)
            {
                result = "false" + e.InnerException.Message;
            }


            Response.Write(result);
        }

        public void SaveQuestionPaperSection(QuestionPaperSectionViewModel questionPaperSectionViewModel)
        {
            string result = "false";
            try
            {
                if (questionPaperSectionViewModel.ID != 0)
                {
                    int minutes = (questionPaperSectionViewModel.TimeLimitTimeSpan.Hours * 60) + (questionPaperSectionViewModel.TimeLimitTimeSpan.Minutes);
                    questionPaperSectionViewModel.TimeLimit = minutes;

                    QuestionPaperSection questionPaperSection = db.QuestionPaperSection.Find(questionPaperSectionViewModel.ID);
                    questionPaperSection.Name = questionPaperSectionViewModel.Name;
                    questionPaperSection.Description = questionPaperSectionViewModel.Description;
                    questionPaperSection.CutOffMark = questionPaperSectionViewModel.CutOffMark;
                    questionPaperSection.NegativeMark = questionPaperSectionViewModel.NegativeMark;
                    questionPaperSection.TimeLimit = questionPaperSectionViewModel.TimeLimit;
                    questionPaperSection.Mark = questionPaperSectionViewModel.Mark; ;

                    questionPaperSection.ModifiedBy = GetCurrentUserID();
                    questionPaperSection.ModifiedDate = DateTime.Now;
                    db.Entry(questionPaperSection).State = EntityState.Modified;
                    db.SaveChanges();
                    result = "true";
                }
            }
            catch
            {
                result = "false";
            }
            Response.Write(result);

        }


        public void GetSubjects(long sectionTemplateID = 0)
        {
            string result = "";
            IEnumerable<Subject> allSubjects = db.Subject;
            List<SubjectViewModel> subjectViewModelList = (from s in allSubjects
                                                           select new SubjectViewModel()
                                                           {
                                                               ID = s.ID,
                                                               Name = s.Name,
                                                               Description = s.Description,
                                                               Hours = s.Hours,
                                                           }).ToList();
            subjectViewModelList = subjectViewModelList;
            result = JsonConvert.SerializeObject(subjectViewModelList);
            //QuestionPaperSectionTemplate questionPaperSectionTemplate = db.QuestionPaperSectionTemplate.Find(sectionTemplateID);
            //if (questionPaperSectionTemplate != null)
            //{
            //    IEnumerable<Subject> allSubjects = db.Subject;
            //    IEnumerable<Subject> existingSubjects = questionPaperSectionTemplate.QuestionPaperSectionTemplateRule.SelectMany(x => x.QuestionPaperSectionTemplateRuleSubject).Select(x => x.Subject);
            //    IEnumerable<Subject> remainingSubjects = allSubjects.Except(existingSubjects);
            //    List<SubjectViewModel> subjectViewModelList = (from s in allSubjects
            //                                                   select new SubjectViewModel()
            //                                                   {
            //                                                       ID = s.ID,
            //                                                       Name = s.Name,
            //                                                       Description = s.Description,
            //                                                       Hours = s.Hours,
            //                                                   }).ToList();
            //    subjectViewModelList = sh.SortSubjectViewModel(subjectViewModelList);
            //    result = JsonConvert.SerializeObject(subjectViewModelList);
            //}
            Response.Write(result);
        }

        public void GetTags(long sectionTemplateID = 0)
        {
            string result = "";
            IEnumerable<Tag> allTags = db.Tag;
            List<TagViewModel> tagViewModelList = (from s in allTags
                                                   select new TagViewModel()
                                                   {
                                                       ID = s.ID,
                                                       Name = s.Name,
                                                       IsSelected = false,
                                                   }).ToList();
            tagViewModelList = tagViewModelList;
            result = JsonConvert.SerializeObject(tagViewModelList);
            //QuestionPaperSectionTemplate questionPaperSectionTemplate = db.QuestionPaperSectionTemplate.Find(sectionTemplateID);
            //if (questionPaperSectionTemplate != null)
            //{
            //    IEnumerable<Tag> allTags = db.Tag;
            //    IEnumerable<Tag> existingTags = questionPaperSectionTemplate.QuestionPaperSectionTemplateRule.SelectMany(x => x.QuestionPaperSectionTemplateRuleTag).Select(x => x.Tag);
            //    IEnumerable<Tag> remainingTags = allTags.Except(existingTags);
            //    List<TagViewModel> tagViewModelList = (from s in remainingTags
            //                                           select new TagViewModel()
            //                                           {
            //                                               ID = s.ID,
            //                                               Name = s.Name,
            //                                               IsSelected = false,
            //                                           }).ToList();
            //    tagViewModelList = tagViewModelList);
            //    result = JsonConvert.SerializeObject(tagViewModelList);
            //}
            Response.Write(result);
        }

        public void GetQuestions(long sectionTemplateID = 0)
        {
            string result = "";
            IEnumerable<Question> Question = db.Question;
            List<QuestionViewModel> questionViewModelList = (from s in Question
                                                        select new QuestionViewModel()
                                                   {
                                                       ID = s.ID,
                                                       Description = s.Description,
                                                       //IsSelected = false,
                                                   }).ToList();
            questionViewModelList = questionViewModelList;
            result = JsonConvert.SerializeObject(questionViewModelList);
            Response.Write(result);
        }

        public void GetQuestionPaperSectionOuestionsPopup(int questionPaperSectionID = 0)
        {
            string result = "";
            QuestionPaperSectionViewModel assessmentSectionViewModel = new QuestionPaperSectionViewModel();
            assessmentSectionViewModel.ID = questionPaperSectionID;
            IQueryable<Tag> tagsList = db.Tag;
            QuestionPaperSection questionPaperSection = db.QuestionPaperSection.Find(questionPaperSectionID);
            if (questionPaperSection != null)
            {
                assessmentSectionViewModel.QuestionPaperID = questionPaperSection.AssessmentID;
                List<TagViewModel> tagViewModelList = MapperHelper.MapToViewModelList(tagsList);
                tagViewModelList = tagViewModelList;
                assessmentSectionViewModel.TagViewModelList = tagViewModelList;
                //return View(assessmentSectionViewModel);
                result = RenderPartialViewToString("~/Views/QuestionPaperSection/QuestionPaperSectionQuestionsPopup.cshtml", assessmentSectionViewModel);
            }
            Response.Write(result);
        }

        public void GetSelectedQuestions(string tags, string subjects, int complexity=0,int id=0)
        {
            string[] columns = { "Description" };
            datatableRequest = new DataTableRequest(Request, columns);
            IQueryable<Question> questions;
            IQueryable<TechnicalQuestion> technicalQuestions;

            if (!string.IsNullOrWhiteSpace(tags))
            {
                List<long> tagIDList = tags.Split(',').ToList().Select(long.Parse).ToList();
                questions = db.QuestionPaperSectionQuestion.Where(ass => ass.AssessmentSectionID == id).Select(x => x.Question).Where(q => q.QuestionTags.Where(qt => tagIDList.Contains(qt.TagID)).Count() > 0);
                technicalQuestions = db.TechnicalQuestionPaperSectionQuestion.Where(ass => ass.TechnicalAssessmentSectionID == id).Select(x => x.TechnicalQuestion).Where(q => q.TechnicalQuestionTags.Where(qt => tagIDList.Contains(qt.TagID)).Count() > 0);
            }
            else
            {
                questions = db.QuestionPaperSectionQuestion.Where(ass => ass.AssessmentSectionID == id).Select(x => x.Question);
                technicalQuestions = db.TechnicalQuestionPaperSectionQuestion.Where(ass => ass.TechnicalAssessmentSectionID == id).Select(x => x.TechnicalQuestion);
            }

            int totalRecords = questions.Count() + technicalQuestions.Count();
            int filteredCount = totalRecords;

            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                questions = (from q in db.Question where q.Description.Contains(datatableRequest.SearchString) select q);
                technicalQuestions = (from q in db.TechnicalQuestion where q.Description.Contains(datatableRequest.SearchString) select q);
                filteredCount = questions.Count() + technicalQuestions.Count();
            }

            questions = questions.OrderBy(x => x.Description);
            technicalQuestions = technicalQuestions.OrderBy(x => x.Description);

            if (datatableRequest.ShouldOrder)
            {
                questions = questions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
                technicalQuestions = technicalQuestions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            }
            questions = questions.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            technicalQuestions = technicalQuestions.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);

            List<QuestionViewModel> questionViewModelList = MapperHelper.MapToViewModelList(questions);
            var technicalQuestionViewModelList = MapperHelper.MapToTechnicalViewModelList(technicalQuestions);

            QuestionDataTableViewModel dataTableViewModel = new QuestionDataTableViewModel(questionViewModelList,technicalQuestionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }
        public void GetOtherQuestions(string tags, string subjects, int complexity = 0, int id = 0)
        {

            string[] columns = { "Description" };
            datatableRequest = new DataTableRequest(Request, columns);
            IQueryable<Question> allquestions = db.Question;
            IQueryable<Question> questions = allquestions;
            IQueryable<TechnicalQuestion> technicalQuestions = db.TechnicalQuestion;

            QuestionPaperSection questionPaperSection = db.QuestionPaperSection.Find(id);
            int totalRecords = 0;
            int filteredCount = 0;
            if (questionPaperSection != null)
            {
                List<Question> existingQuestions = questionPaperSection.QuestionPaperSectionQuestion.Select(x => x.Question).Distinct().ToList();
                List<TechnicalQuestion> technicalExistingQuestions = questionPaperSection.TechnicalQuestionPaperSectionQuestion.Select(x => x.TechnicalQuestion).Distinct().ToList();

                List<int> existingQuestionIDs = questionPaperSection.QuestionPaperSectionQuestion.Select(x => x.Question.ID).Distinct().ToList();
                List<int> technicalExistingQuestionIDs = questionPaperSection.TechnicalQuestionPaperSectionQuestion.Select(x => x.TechnicalQuestion.ID).Distinct().ToList();

                questions = db.Question.Where(q => !existingQuestionIDs.Contains(q.ID));
                technicalQuestions = db.TechnicalQuestion.Where(q => !technicalExistingQuestionIDs.Contains(q.ID));

                if (!string.IsNullOrWhiteSpace(subjects))
                {
                    List<long> subjectIDlist = subjects.Split(',').ToList().Select(long.Parse).ToList();
                    questions = questions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0 && (subjectIDlist.Contains(q.SubjectID ?? 0)));
                    technicalQuestions = technicalQuestions.Where(q => q.TechnicalQuestionPaperSectionQuestion.Where(asq => asq.TechnicalAssessmentSectionID == id).Count() <= 0 && (subjectIDlist.Contains(q.SubjectID ?? 0)));
                }
                else
                {
                    questions = questions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0);
                    technicalQuestions = technicalQuestions.Where(q => q.TechnicalQuestionPaperSectionQuestion.Where(asq => asq.TechnicalAssessmentSectionID == id).Count() <= 0);
                }

                if (!string.IsNullOrWhiteSpace(tags))
                {
                    List<long> tagIDList = tags.Split(',').ToList().Select(long.Parse).ToList();
                    questions = questions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0 && (q.QuestionTags.Where(qt => tagIDList.Contains(qt.TagID)).Count() > 0));
                    technicalQuestions = technicalQuestions.Where(q => q.TechnicalQuestionPaperSectionQuestion.Where(asq => asq.TechnicalAssessmentSectionID == id).Count() <= 0 && (q.TechnicalQuestionTags.Where(qt => tagIDList.Contains(qt.TagID)).Count() > 0));
                }
                else
                {
                    questions = questions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0);
                    technicalQuestions = technicalQuestions.Where(q => q.TechnicalQuestionPaperSectionQuestion.Where(asq => asq.TechnicalAssessmentSectionID == id).Count() <= 0);
                }

                if (complexity != 0)
                {

                    questions = questions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0 && (q.Complexity.ID == complexity));
                    technicalQuestions = technicalQuestions.Where(q => q.TechnicalQuestionPaperSectionQuestion.Where(asq => asq.TechnicalAssessmentSectionID == id).Count() <= 0 && (q.Complexity.ID == complexity));
                }
                else
                {
                    questions = questions.Where(q => q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 && q.QuestionPaperSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0);
                    technicalQuestions = technicalQuestions.Where(q => q.TechnicalQuestionPaperSectionQuestion.Where(asq => asq.TechnicalAssessmentSectionID == id).Count() <= 0);
                }

                try
                {
                    if (questions != null && questions.ToList().Count() != 0)
                    {
                        totalRecords = questions.ToList().Count();
                    }

                    if (technicalQuestions != null && technicalQuestions.ToList().Count() != 0)
                    {
                        totalRecords = totalRecords + technicalQuestions.ToList().Count();
                    }
                }
                catch (NullReferenceException)
                {
                    totalRecords = 0;
                }

                filteredCount = totalRecords;

                if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
                {
                    questions = (from q in db.Question where q.Description.Contains(datatableRequest.SearchString) && q.Answer.Where(a => a.IsCorrect ?? false).Count() > 0 select q);
                    technicalQuestions = (from q in db.TechnicalQuestion where q.Description.Contains(datatableRequest.SearchString) select q);
                    filteredCount = questions.Count() + technicalQuestions.Count();
                }

                questions = questions.OrderBy(x => x.Description);
                technicalQuestions = technicalQuestions.OrderBy(x => x.Description);

                if (datatableRequest.ShouldOrder)
                {
                    questions = questions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
                    technicalQuestions = technicalQuestions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
                }

                questions = questions.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
                technicalQuestions = technicalQuestions.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            }
            
            List<QuestionViewModel> questionViewModelList = MapperHelper.MapToViewModelList(questions);
            var technicalQuestionViewModelList = MapperHelper.MapToTechnicalViewModelList(technicalQuestions);

            QuestionDataTableViewModel dataTableViewModel = new QuestionDataTableViewModel(questionViewModelList, technicalQuestionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);


        }
    }

    public class HtmlEntities
    {

        public HtmlEntities(string HtmlCode, string XmlCode)
        {
            // TODO: Complete member initialization
            this.HtmlCode = HtmlCode;
            this.XmlCode = XmlCode;
        }
        public string HtmlCode { get; set; }
        public string XmlCode { get; set; }

    }
}