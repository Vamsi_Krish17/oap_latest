﻿using HelperClasses;
using Newtonsoft.Json;
using OAP.Domain;
using OAP.Domain.ViewModel;
using OAP.Domain.ViewModel.DataTableViewModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace OAP.Controllers
{
    [Authorize(Roles = "SuperAdmin")]
    public class MemberController : BaseController
    {
        //
        // GET: /Member/

        public ActionResult Index()
        {
            return View();
        }

        string[] columns = { "UserName","EmailID", "OrganizationName" };

        public void List()
        {
            datatableRequest = new DataTableRequest(Request, columns);

            //IQueryable<UserProfile> members = db.webpages_Roles.Where(r => r.RoleName.ToLower() == "admin").SelectMany(x => x.UserProfile);
            IQueryable<UserProfile> members = db.webpages_Roles.Where(r => r.RoleName.ToLower() == "CollegeAdmin").SelectMany(x => x.UserProfile);

            int totalRecords = members.Count();
            int filteredCount = totalRecords;

            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                //members = (from u in db.webpages_Roles.Where(r => r.RoleName.ToLower() == "admin").SelectMany(x => x.UserProfile) where u.UserName.Contains(datatableRequest.SearchString) select u);
                members = (from u in db.webpages_Roles.Where(r => r.RoleName.ToLower() == "CollegeAdmin").SelectMany(x => x.UserProfile) where u.UserName.Contains(datatableRequest.SearchString) select u);
                filteredCount = members.Count();
            }
            members = members.OrderBy(x => x.UserName);
            if (datatableRequest.ShouldOrder)
            {
                members = members.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            }
            members = members.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            List<MemberViewModel> memberViewModelList = MapperHelper.MapToViewModelList(members);


            MemberDataTableViewModel dataTableViewModel = new MemberDataTableViewModel(memberViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }


        public ActionResult Edit(int id=0)
        {
            UserProfile userprofile = db.UserProfile.Find(id);
            if(userprofile==null)
            {
                return HttpNotFound();
            }
            MemberViewModel memberViewModel = MapperHelper.MapToViewModel(userprofile);
            return View(memberViewModel);

        }

        [HttpPost]
        public ActionResult Edit(MemberViewModel memberViewModel,HttpPostedFileBase file)
        {
            UserProfile userProfile = db.UserProfile.Find(memberViewModel.ID);
            userProfile.EmailID = memberViewModel.EmailID;
            db.Entry(userProfile).State = EntityState.Modified;
            db.SaveChanges();

            UserOrganisation userOrganisation;
            string fileName;
            if (memberViewModel.UserOrganisationViewModel != null)
            {
                if (memberViewModel.UserOrganisationViewModel.ID == 0)
                {
                    memberViewModel.UserOrganisationViewModel.UserId = memberViewModel.ID;
                    userOrganisation = new UserOrganisation();
                    userOrganisation = MapperHelper.MapToDomainObject(db, memberViewModel.UserOrganisationViewModel);
                    if (file != null)
                    {                                                                           //Image selected
                        string serverPath = Server.MapPath("~/Uploaded/OrganisationLogo");
                        if (!Directory.Exists(serverPath))
                            Directory.CreateDirectory(serverPath);
                        string guidString = Guid.NewGuid().ToString();
                        fileName = guidString + "." + file.FileName.Split('.')[1];
                        file.SaveAs(serverPath + "/" + guidString + "." + file.FileName.Split('.')[1]);
                        userOrganisation.OrgLogo = fileName;
                    }
                    db.UserOrganisation.Add(userOrganisation);
                    db.SaveChanges();
                }
                else
                {
                    userOrganisation = db.UserOrganisation.Find(memberViewModel.UserOrganisationViewModel.ID);
                    userOrganisation = MapperHelper.MapToDomainObject(db, memberViewModel.UserOrganisationViewModel,userOrganisation);
                    if (file != null)
                    {                                                                           //Image selected
                        string serverPath = Server.MapPath("~/Uploaded/OrganisationLogo");
                        if (!Directory.Exists(serverPath))
                            Directory.CreateDirectory(serverPath);
                        string guidString = Guid.NewGuid().ToString();
                        fileName = guidString + "." + file.FileName.Split('.')[1];
                        file.SaveAs(serverPath + "/" + guidString + "." + file.FileName.Split('.')[1]);
                        userOrganisation.OrgLogo = fileName;
                    }
                    db.Entry(userOrganisation).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }

            return RedirectToAction("Index");
        }

        public void Delete(string memberIDString)
        {
            bool result = true;
            try
            {
                if (!string.IsNullOrEmpty(memberIDString))
                {
                    List<long> memberIDList = memberIDString.Split(',').Select(long.Parse).ToList();
                    foreach (long memberID in memberIDList)
                    {
                        var membership = (SimpleMembershipProvider)Membership.Provider;
                        string userName = db.UserProfile.Find(memberID).UserName;
                        membership.DeleteAccount(userName);
                        Roles.RemoveUserFromRoles(userName, Roles.GetRolesForUser(userName));
                        db.UserOrganisation.RemoveRange(db.UserProfile.Find(memberID).UserOrganisation);
                        db.SaveChanges();
                        membership.DeleteUser(userName, true);
                    }
                }
                else
                {
                    result = false;
                }
            }
            catch
            {
                result = false;
            }

            Response.Write(result);
        }

    }
}
