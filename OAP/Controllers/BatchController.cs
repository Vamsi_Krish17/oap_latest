﻿using HelperClasses;
using Newtonsoft.Json;
using OAP.Domain;
using OAP.Domain.ViewModel;
using OAP.Domain.ViewModel.DataTableViewModel;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;
using OfficeOpenXml.Style;
using System.Drawing;
using System.Net;
using System.Net.Http;
using System.Data;

namespace OAP.Controllers
{
    [Authorize(Roles = "SuperAdmin,CollegeAdmin,Admin")]
    public class BatchController : BaseController
    {
        //
        // GET: /Batch/

        string[] columns = { "Name" };
        private OAPEntities db = new OAPEntities();

        public ActionResult Index()
        {
            return View();
        }

        public void List()
        {
            string[] columns = { "Name","StudentCount"};
            datatableRequest = new DataTableRequest(Request, columns);

            // IQueryable<Batch> batches = db.Batch;
            IQueryable<Batch> batches = (from b in db.Batch
                                         group b by new
                                         {
                                             b.Name
                                         } into bt
                                         select bt.FirstOrDefault());

            int totalRecords = batches.Count();
            int filteredCount = totalRecords;
            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                batches = (from q in db.Batch where q.Name.Contains(datatableRequest.SearchString) select q);
                filteredCount = batches.Count();
            }
            batches = batches.OrderBy(x => x.Name);
            if (datatableRequest.ShouldOrder)
            {
                batches = batches.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            }
            batches = batches.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            List<BatchViewModel> batchViewModelList = MapperHelper.MapToViewModelList(batches);
            BatchDataTableViewModel dataTableViewModel = new BatchDataTableViewModel(batchViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        #region Create

        public ActionResult Create()
        {
            ViewBag.Colleges = db.College.OrderBy(x=>x.Name);
            return View();
        }

        [HttpPost]
        public ActionResult Create(BatchViewModel batchViewModel)
        {
            if (ModelState.IsValid)
            {
                Batch batch = MapperHelper.MapToDomainObject(db, batchViewModel);
                string userName = User.Identity.Name;
                UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == userName.ToLower()).FirstOrDefault();
                batch.UserProfile = userProfile;
                batch.UserProfile1 = userProfile;
                batch.CreatedDate = DateTime.Now;
                batch.ModifiedDate = DateTime.Now;
                db.Batch.Add(batch);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Colleges = db.College.OrderBy(x=>x.Name);
                return View(batchViewModel);
            }
        }

        #endregion

        #region Edit

        public ActionResult Edit(int id)
        {
            Batch batch = db.Batch.Find(id);
            BatchViewModel batchViewModel = MapperHelper.MapToViewModel(batch);
            //ViewBag.Colleges = db.College);

            return View(batchViewModel);
        }

        [HttpPost]
        public ActionResult Edit(BatchViewModel batchViewModel)
        {
            if (ModelState.IsValid)
            {
                Batch batch = db.Batch.Find(batchViewModel.ID);
                batch = MapperHelper.MapToDomainObject(db, batchViewModel, batch);
                string userName = User.Identity.Name;
                UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == userName.ToLower()).FirstOrDefault();
                batch.ModifiedBy = userProfile.UserId;
                db.Entry(batch).State = EntityState.Modified;
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                ViewBag.Colleges = db.College;

                return View(batchViewModel);
            }
        }

        #endregion

        #region Delete

        public ActionResult Delete(int id)
        {
            Batch batch = db.Batch.Find(id);
            BatchViewModel batchViewModel = MapperHelper.MapToViewModel(batch);
            return View(batchViewModel);
        }

        [HttpPost]
        public ActionResult Delete(BatchViewModel batchViewModel)
        {
            //Batch batch = db.Batch.Find(batchViewModel.ID);

            //db.BatchStudent.RemoveRange(batch.BatchStudent);
            //db.BatchAssessment.RemoveRange(batch.BatchAssessment);

            //db.Batch.Remove(batch);
            //db.SaveChanges();

            DeleteBatches(batchViewModel.ID.ToString());
            return RedirectToAction("Index");
        }

        public void DeleteBatches(string batchIDString)
        {
            bool result = true;

            try
            {
                List<int> batchIDList = batchIDString.Split(',').Select(int.Parse).ToList();

                foreach (int batchID in batchIDList)
                {
                    Batch batch = db.Batch.Find(batchID);
                    db.BatchAssessment.RemoveRange(batch.BatchAssessment);
                    db.BatchStudent.RemoveRange(batch.BatchStudent);
                    db.Batch.Remove(batch);
                }
                db.SaveChanges();
            }
            catch { result = false; }
            Response.Write(result);
        }

        #endregion

        #region MapStudents

        public ActionResult BatchStudents(int id)
        {
            Batch batch = db.Batch.Find(id);
            BatchViewModel batchViewModel = MapperHelper.MapToViewModel(batch);

            //ViewBag.Colleges = db.College;
            //ViewBag.Sections = new List<Section>();
            //ViewBag.Branches = new List<Branch>();

            //ViewBag.StartYear = GetYearRangeByYear(DateTime.Now.Year);
            //ViewBag.EndYear = new List<SelectListItem>();

            IQueryable<College> colleges = db.College;
            List<CollegeViewModel> collegeViewModelList = MapperHelper.MapToViewModelList(colleges);
            batchViewModel.CollegeViewModelList = collegeViewModelList;
            return View(batchViewModel);
        }

        //public List<SelectListItem> GetYearRangeByYear(int year)
        //{
        //    List<SelectListItem> yearListItem = new List<SelectListItem>();
        //    for (int i = year - 5; i <= year + 5; i++)
        //    {
        //        SelectListItem yearItem = new SelectListItem();
        //        yearItem.Text = i.ToString();
        //        yearItem.Value = i.ToString();
        //        yearListItem.Add(yearItem);
        //    }
        //    return yearListItem;
        //}

        public void GetSelectedStudents(int id, string selectedColleges, string selectedBranches, string selectedSections)
        {
            string[] columns = { "Name", "UniversityRegistryNumber" };
            datatableRequest = new DataTableRequest(Request, columns);
            IQueryable<Student> students;
            if (!string.IsNullOrWhiteSpace(selectedColleges))
            {                                                                       //College selected
                List<long> collegeIDList = selectedColleges.Split(',').ToList().Select(long.Parse).ToList();
                if (!string.IsNullOrEmpty(selectedBranches) && !string.IsNullOrEmpty(selectedSections))
                {                                                                   //Branch and section selected
                    List<long> branchIDList = selectedBranches.Split(',').ToList().Select(long.Parse).ToList();
                    List<long> sectionIDList = selectedSections.Split(',').ToList().Select(long.Parse).ToList();
                    students = db.BatchStudent.Where(bs => collegeIDList.Contains(bs.Student.College.ID) && branchIDList.Contains(bs.Student.Branch.ID) && sectionIDList.Contains(bs.Student.Section.ID)).Select(x => x.Student);
                }
                else
                {
                    if (!string.IsNullOrEmpty(selectedBranches))
                    {                                                       //Branch selected             
                        List<long> branchIDList = selectedBranches.Split(',').ToList().Select(long.Parse).ToList();
                        students = db.BatchStudent.Where(bs => collegeIDList.Contains(bs.Student.College.ID) && branchIDList.Contains(bs.Student.Branch.ID)).Select(x => x.Student);
                    }
                    else if (!string.IsNullOrEmpty(selectedSections))
                    {                                                           //Section selected
                        List<long> sectionIDList = selectedSections.Split(',').ToList().Select(long.Parse).ToList();
                        students = db.BatchStudent.Where(bs => collegeIDList.Contains(bs.Student.College.ID) && sectionIDList.Contains(bs.Student.Branch.ID)).Select(x => x.Student);
                    }
                    else
                    {
                        students = db.BatchStudent.Where(bs => collegeIDList.Contains(bs.Student.College.ID)).Select(x => x.Student);
                    }
                }
            }
            else
            {
                //students = db.AssessmentSectionQuestion.Where(ass => ass.AssessmentSectionID == id).Select(x => x.Question);
                students = db.BatchStudent.Where(bs => bs.Batch.ID == id).Select(x => x.Student);
            }

            int totalRecords = students.Count();
            int filteredCount = totalRecords;

            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                students = (from q in db.Student where q.UserProfile.FirstOrDefault().UserName.Contains(datatableRequest.SearchString) select q);
                filteredCount = students.Count();
            }

            students = students.OrderBy(x => x.UserProfile.FirstOrDefault().UserName);
            //if (datatableRequest.ShouldOrder)
            //{
            //    students = students.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            //}
            students = students.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            students = students.OrderBy(s => s.Name);
            List<StudentViewModel> studentViewModelList = MapperHelper.MapToViewModelList(students);
            studentViewModelList = studentViewModelList;
            StudentDataTableViewModel dataTableViewModel = new StudentDataTableViewModel(studentViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        public void GetOtherStudents(int id, string selectedColleges, string selectedBranches, string selectedSections)
        {
            string[] columns = { "Name", "UniversityRegistryNumber" };
            datatableRequest = new DataTableRequest(Request, columns);

            //IQueryable<Question> questions = db.Question.Where(q => q.AssessmentSectionQuestion.Count() <= 0 && q.AssessmentSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0);
            IQueryable<Student> students = db.Student;
            if (!string.IsNullOrWhiteSpace(selectedColleges))
            {
                //List<long> tagIDList = filterString.Split(',').ToList().Select(long.Parse).ToList();
                //questions = db.Question.Where(q => q.AssessmentSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0 && (q.QuestionTags.Where(qt => tagIDList.Contains(qt.TagID)).Count() > 0));
            }
            else
            {
                //students = db.Question.Where(q => q.AssessmentSectionQuestion.Where(asq => asq.AssessmentSectionID == id).Count() <= 0);
            }

            if (!string.IsNullOrWhiteSpace(selectedColleges))
            {                                                                       //College selected
                List<long> collegeIDList = selectedColleges.Split(',').ToList().Select(long.Parse).ToList();
                if (!string.IsNullOrEmpty(selectedBranches) && !string.IsNullOrEmpty(selectedSections))
                {                                                                   //Branch and section selected
                    List<long> branchIDList = selectedBranches.Split(',').ToList().Select(long.Parse).ToList();
                    List<long> sectionIDList = selectedSections.Split(',').ToList().Select(long.Parse).ToList();
                    students = db.Student.Where(s => collegeIDList.Contains(s.College.ID) && branchIDList.Contains(s.Branch.ID) && sectionIDList.Contains(s.Section.ID));
                }
                else
                {
                    if (!string.IsNullOrEmpty(selectedBranches))
                    {                                                       //Branch selected             
                        List<long> branchIDList = selectedBranches.Split(',').ToList().Select(long.Parse).ToList();
                        students = db.Student.Where(s => collegeIDList.Contains(s.College.ID) && branchIDList.Contains(s.Branch.ID));
                    }
                    else if (!string.IsNullOrEmpty(selectedSections))
                    {                                                           //Section selected
                        List<long> sectionIDList = selectedSections.Split(',').ToList().Select(long.Parse).ToList();
                        students = db.Student.Where(s => collegeIDList.Contains(s.College.ID) && sectionIDList.Contains(s.Section.ID));
                    }
                    else
                    {
                        students = db.Student.Where(s => collegeIDList.Contains(s.College.ID));
                    }
                }
            }
            else
            {
                students = db.Student.Where(s => s.BatchStudent.Where(bs => bs.Batch.ID == id).Count() <= 0);
            }

            int totalRecords = students.Count();
            int filteredCount = totalRecords;

            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                students = (from s in db.Student where s.UserProfile.FirstOrDefault().UserName.Contains(datatableRequest.SearchString) select s);
                filteredCount = students.Count();
            }

            students = students.OrderBy(x => x.UserProfile.FirstOrDefault().UserName);
            //if (datatableRequest.ShouldOrder)
            //{
            //    students = students.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            //}
            students = students.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            List<StudentViewModel> studentViewModelList = MapperHelper.MapToViewModelList(students);
            studentViewModelList = studentViewModelList;
            StudentDataTableViewModel dataTableViewModel = new StudentDataTableViewModel(studentViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        public void MapStudents(string batchIDString, string selectedStudentsString)
        {
            long batchID = Convert.ToInt16(batchIDString);
            Batch batch = db.Batch.Find(batchID);
            List<long> studentIDList = selectedStudentsString.Split(',').ToList().Select(long.Parse).ToList();

            foreach (long studentID in studentIDList)
            {
                Student student = db.Student.Find(studentID);
                BatchStudent batchStudent = new BatchStudent();
                batchStudent.Batch = batch;
                batchStudent.Student = student;
                db.BatchStudent.Add(batchStudent);
            }
            db.SaveChanges();
            Response.Write("Success");
        }

        public void UnMapStudents(string batchIDString, string selectedStudentsString)
        {
            long batchID = Convert.ToInt16(batchIDString);
            Batch batch = db.Batch.Find(batchID);
            List<long> studentIDList = selectedStudentsString.Split(',').ToList().Select(long.Parse).ToList();

            List<BatchStudent> batchStudentList = db.BatchStudent.Where(bs => bs.Batch.ID == batchID && studentIDList.Contains(bs.Student.ID)).ToList();

            db.BatchStudent.RemoveRange(batchStudentList);
            db.SaveChanges();

        }

        #endregion

        #region CollegeRelatedProcess

        public void GetSectionsYearByCollege(string collegeIDString)
        {
            //List<long> collegeIDList = new List<long>();
            //if (!string.IsNullOrEmpty(collegeIDString))
            //{
            //    collegeIDList = collegeIDString.Split(',').ToList().Select(long.Parse).ToList();
            //}
            //IQueryable<Section> sections = db.Section.Where(s => collegeIDList.Intersect(s.CollegeSection.Select(x => x.College.ID).ToList()).Any());
            //List<SectionViewModel> sectionViewModelList = MapperHelper.MapToViewModelList(sections);
            IQueryable<Branch> branches = db.Branch;
            List<BranchViewModel> branchViewModelList = MapperHelper.MapToViewModelList(branches);
            CollegeViewModel collegeViewModel = new CollegeViewModel();
            //collegeViewModel.SectionViewModelList = sh.SortSectionViewModel(sectionViewModelList);
           // collegeViewModel.SectionViewModelList = new List<SectionViewModel>();
            collegeViewModel.BranchViewModelList = branchViewModelList;
            string result = JsonConvert.SerializeObject(collegeViewModel);
            Response.Write(result);
        }


        #endregion

        #region MapAssessment

        public ActionResult BatchAssessments(int id = 0)
        {
            Batch batch = db.Batch.Find(id);
            BatchViewModel batchViewModel = MapperHelper.MapToViewModel(batch);
            return View(batchViewModel);
        }

        public void GetSelectedAssessments(int id)
        {
            string[] columns = { "Name" };
            datatableRequest = new DataTableRequest(Request, columns);
            IQueryable<QuestionPaper> questionPapers = db.BatchAssessment.Where(ba => ba.Batch.ID == id).Select(x => x.QuestionPaper);

            int totalRecords = questionPapers.Count();
            int filteredCount = totalRecords;

            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                questionPapers = (from ba in questionPapers where ba.Name.Contains(datatableRequest.SearchString) select ba);
                filteredCount = questionPapers.Count();
            }

            questionPapers = questionPapers.OrderBy(x => x.Name);

            questionPapers = questionPapers.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            List<QuestionPaperViewModel> questionPaperViewModelList = MapperHelper.MapToViewModelList(questionPapers);
            questionPaperViewModelList = questionPaperViewModelList;
            QuestionPaperDataTableViewModel dataTableViewModel = new QuestionPaperDataTableViewModel(questionPaperViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        public void GetOtherAssessments(int id)
        {
            string[] columns = { "Name" };
            datatableRequest = new DataTableRequest(Request, columns);
            List<long> mappedQuestionPapers = db.BatchAssessment.Select(x => x.QuestionPaper.ID).Distinct().ToList();
            // = db.QuestionPaper.Where(a => );
            IQueryable<QuestionPaper> assessments = (from b in db.QuestionPaper
                                                     where !mappedQuestionPapers.Contains(b.ID)
                                                     group b by new
                                                     {
                                                         b.Name
                                                     } into bt
                                                     select bt.FirstOrDefault());


            int totalRecords = assessments.Count();
            int filteredCount = totalRecords;

            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                assessments = (from a in assessments where a.Name.Contains(datatableRequest.SearchString) && a.BatchAssessment.Where(ba => ba.Batch.ID == id).Count() <= 0 select a);
                filteredCount = assessments.Count();
            }

            assessments = assessments.OrderBy(x => x.Name);

            assessments = assessments.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            List<QuestionPaperViewModel> assessmentViewModelList = MapperHelper.MapToViewModelList(assessments);

            QuestionPaperDataTableViewModel dataTableViewModel = new QuestionPaperDataTableViewModel(assessmentViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        public void UnMapAssessments(string batchIDString, string selectedAssessments)
        {
            bool result = true;
            try
            {
                int batchID = Convert.ToInt16(batchIDString);
                List<long> assessmentsIDList = selectedAssessments.Split(',').ToList().Select(long.Parse).ToList();
                List<BatchAssessment> batchAssessmentList = db.BatchAssessment.Where(ba => ba.Batch.ID == batchID && assessmentsIDList.Contains(ba.QuestionPaper.ID)).ToList();
                db.BatchAssessment.RemoveRange(batchAssessmentList);
                db.SaveChanges();
            }
            catch
            {
                result = false;
            }
            Response.Write(result);

        }

        public void MapAssessments(string batchIDString, string selectedAssessments, string startDateString, string endDateString)
        {
            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            DateTime startDateTime = DateTime.Parse(startDateString, currentCulture);
            DateTime endDateTime = DateTime.Parse(endDateString, currentCulture);
            bool result = true;
            try
            {
                int batchID = Convert.ToInt16(batchIDString);
                Batch batch = db.Batch.Find(batchID);
                List<int> assessmentsIDList = selectedAssessments.Split(',').ToList().Select(int.Parse).ToList();
                foreach (int assessmentID in assessmentsIDList)
                {
                    QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentID);
                    BatchAssessment batchAssessment = new BatchAssessment();
                    batchAssessment.Batch = batch;
                    batchAssessment.QuestionPaper = questionPaper;
                    batchAssessment.StartDateTime = startDateTime;
                    batchAssessment.EndDateTime = endDateTime;
                    db.BatchAssessment.Add(batchAssessment);
                    db.SaveChanges();

                }
                //                db.SaveChanges();

            }
            catch
            {
                result = false;
            }
            Response.Write(result);

        }

        #endregion

        public string Test()
        {

            CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            // pass the DateTimeFormat information to DateTime.Parse
            string iDate = "15/04/2005 10:20";
            DateTime myDateTime = DateTime.Parse(iDate, currentCulture);

            //CultureInfo currentCulture = Thread.CurrentThread.CurrentCulture;
            //DateTime oDate = Convert.ToDateTime(iDate,currentCulture);

            return iDate.ToString();
        }

        #region Mass Upload

        public ActionResult UploadStudentsForBatch()
        {
            ViewBag.Branches = db.Branch;
            ViewBag.Batches = db.Batch;
            return View();
        }

        public List<SelectListItem> GetBatchSelectListItem()
        {
            List<SelectListItem> selectListItem = db.Batch.Select(b => new SelectListItem { Text = b.Name, Value = b.ID.ToString() }).ToList();

            SelectListItem multipleItem = new SelectListItem();
            multipleItem.Text = "Multiple";
            multipleItem.Value = "M";
            selectListItem.Add(multipleItem);

            return selectListItem;
        }

        public List<SelectListItem> GetYearSelectListItem(int startYear, int count)
        {
            List<SelectListItem> selectListItem = new List<SelectListItem>();

            for (int i = 0; i < count; i++)
            {
                SelectListItem listItem = new SelectListItem();
                listItem.Text = startYear.ToString();
                listItem.Value = startYear.ToString();
                selectListItem.Add(listItem);
                startYear++;
            }

            return selectListItem;
        }

        [HttpPost]
        public ActionResult UploadStudentsForBatch(HttpPostedFileBase file, BatchViewModel batchViewModel)
        {
            //StudentRepository studentRepository = new StudentRepository();
            List<MashUploadResultViewModel> mashUploadResultViewModelList = new List<MashUploadResultViewModel>();
            if (file != null)                       //Mass UPload
            {
                string filePath = System.IO.Path.GetFullPath(file.FileName);
                string extension = Path.GetExtension(file.FileName);
                string fileName = Guid.NewGuid().ToString() + extension;
                string fullPath = Server.MapPath("//BatchUpload//Student//" + fileName);
                string directory = Path.GetDirectoryName(fullPath);
                if (!Directory.Exists(directory))
                {
                    Directory.CreateDirectory(directory);
                }
                file.SaveAs(fullPath);
                fullPath = fullPath.Replace(@"\\", @"\");

                FileInfo newFile = new FileInfo(fullPath);

                ExcelPackage pck = new ExcelPackage(newFile);

                //webpages_Roles studentRole = db.webpages_Roles.Where(wr => wr.RoleName.ToLower() == "student").FirstOrDefault();

                foreach (ExcelWorksheet worksheet in pck.Workbook.Worksheets)
                {
                    //ExcelWorksheet worksheet = pck.Workbook.Worksheets[1];
                    int iRowCnt = worksheet.Dimension.End.Row;


                    for (int i = 2; i <= iRowCnt; i++)
                    {
                        string universityRegisterNumber = worksheet.Cells[i, 1].Text.ToString().Trim() ?? "";
                        if (string.IsNullOrEmpty(universityRegisterNumber))
                            break;
                        string studentName = worksheet.Cells[i, 2].Text.ToString().Trim() ?? "";
                        string batchName = worksheet.Cells[i, 3].Text.ToString().Trim() ?? "";

                        studentName = studentName == "" ? universityRegisterNumber : studentName;

                        UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == universityRegisterNumber.ToLower()).FirstOrDefault();
                        bool isNewStudent = true;

                        Batch batch = db.Batch.Find(batchViewModel.BatchID);
                        Branch branch = db.Branch.Find(batchViewModel.BranchID);
                        if (userProfile == null)
                        {
                            string password = universityRegisterNumber;
                            WebSecurity.CreateUserAndAccount(universityRegisterNumber, password);
                            userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == universityRegisterNumber).FirstOrDefault();

                            Roles.AddUserToRole(userProfile.UserName, "Student");

                            userProfile.Student = new Student();
                            userProfile.Student.UniversityRegisterNumber = universityRegisterNumber;
                            userProfile.Student.Name = studentName;
                            if (branch != null)
                            {
                                userProfile.Student.BranchID = branch.ID;
                            }

                            db.Student.Add(userProfile.Student);
                            db.SaveChanges();

                        }
                        else
                        {
                            isNewStudent = false;
                        }

                        if (!string.IsNullOrEmpty(batchName))
                        {                                                           //Batch not given in the uploaded file
                            batch = db.Batch.Where(b => b.Name.ToLower() == batchName.ToLower()).FirstOrDefault();
                            if (batch == null)
                            {                                                               //No batches in this name
                                batch = CreateNewBatch(batchName, userProfile);
                            }
                        }
                        else
                        {                                                           //Take batchname from work sheet
                            batchName = worksheet.Name;
                            batch = db.Batch.Where(b => b.Name.ToLower() == batchName.ToLower()).FirstOrDefault();
                            if (batch == null)
                            {                                                           //No batches in this name
                                batch = CreateNewBatch(batchName, userProfile);
                            }
                        }
                        if (!isNewStudent)
                        {                                                      //Student already registered
                            BatchStudent batchStudent = db.BatchStudent.Where(bs => bs.Student.ID == userProfile.Student.ID && (bs.Batch.ID == batchViewModel.ID || bs.Batch.ID == batch.ID)).FirstOrDefault();
                            if (batchStudent != null)
                            {
                                //db.BatchStudent.Remove(batchStudent);
                                //db.SaveChanges();
                            }
                            else
                            {
                                batchStudent = new BatchStudent();
                                batchStudent.Batch = batch;
                                batchStudent.Student = userProfile.Student;
                                db.BatchStudent.Add(batchStudent);
                                db.SaveChanges();
                            }
                            mashUploadResultViewModelList.Add(CreateMashUploadResultViewModel(studentName, true, "Success", "Student already registered and mapped to batch"));
                            //mashUploadResultViewModelList.Add(CreateMashUploadResultViewModel(studentName, false, "Failed", "Student alerady registered"));
                        }
                        else
                        {                                                                        //Student not registered
                            string password = universityRegisterNumber;
                            bool result = true;
                            try
                            {
                                //WebSecurity.CreateUserAndAccount(universityRegisterNumber, password);
                                //userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == universityRegisterNumber).FirstOrDefault();

                                //Student student = new Student();
                                //student.UniversityRegisterNumber = universityRegisterNumber;
                                //db.Student.Add(student);
                                //db.SaveChanges();

                                BatchStudent batchStudent = new BatchStudent();
                                batchStudent.Batch = batch;
                                batchStudent.Student = userProfile.Student;
                                db.BatchStudent.Add(batchStudent);
                                db.SaveChanges();
                                mashUploadResultViewModelList.Add(CreateMashUploadResultViewModel(studentName, true, "Success", "Student created successfully"));
                            }
                            catch
                            {
                                mashUploadResultViewModelList.Add(CreateMashUploadResultViewModel(studentName, false, "Failed", "Error occured on saving student"));
                            }
                        }
                    }
                }
                ViewBag.ReturnUrl = "Batch/Index";
                System.IO.File.Delete(fullPath);
                return View("~/Views/Shared/UploadedResult.cshtml", mashUploadResultViewModelList);
            }

            ModelState.Remove("ID");
            ModelState.Remove("BranchID");
            ModelState.Remove("SectionID");
            ModelState.Remove("StartYear");
            ModelState.Remove("EndYear");

            ViewBag.Branches = db.Branch;
            ViewBag.Batches = db.Batch;
            return View(batchViewModel);
        }

        public Batch CreateNewBatch(string batchName, UserProfile userProfile)
        {
            Batch batch = new Batch();
            batch.Name = batchName;
            batch.CreatedDate = DateTime.Now;
            batch.ModifiedDate = DateTime.Now;
            // batch.CreatedBy = userProfile.UserId;
            // batch.ModifiedBy = userProfile.UserId;
            batch.CreatedBy = GetCurrentUserID();
            batch.ModifiedBy = GetCurrentUserID();
            db.Batch.Add(batch);
            db.SaveChanges();
            return batch;
        }

        //[HttpPost]
        //public ActionResult UploadStudentsForBatch(HttpPostedFileBase file, BatchViewModel batchViewModel)
        //{
        //    StudentRepository studentRepository = new StudentRepository();
        //    List<MashUploadResultViewModel> mashUploadResultViewModelList = new List<MashUploadResultViewModel>();
        //    if (file != null)                       //Mass UPload
        //    {
        //        string filePath = System.IO.Path.GetFullPath(file.FileName);
        //        string extension = Path.GetExtension(file.FileName);
        //        string fileName = Guid.NewGuid().ToString() + extension;
        //        string fullPath = Server.MapPath("//BatchUpload//Student//" + fileName);
        //        string directory = Path.GetDirectoryName(fullPath);
        //        if (!Directory.Exists(directory))
        //        {
        //            Directory.CreateDirectory(directory);
        //        }
        //        file.SaveAs(fullPath);
        //        fullPath = fullPath.Replace(@"\\", @"\");

        //        FileInfo newFile = new FileInfo(fullPath);

        //        ExcelPackage pck = new ExcelPackage(newFile);


        //        foreach (ExcelWorksheet worksheet in pck.Workbook.Worksheets)
        //        {
        //            //ExcelWorksheet worksheet = pck.Workbook.Worksheets[1];
        //            int iRowCnt = worksheet.Dimension.End.Row;

        //            for (int i = 2; i <= iRowCnt; i++)
        //            {
        //                string studentName = worksheet.Cells[i, 1].Text.ToString().Trim() ?? "";
        //                if (!String.IsNullOrEmpty(studentName))
        //                {
        //                    string universityRegistryNumber = worksheet.Cells[i, 3].Text.ToString().Trim() ?? "";
        //                    Student student = db.UserProfile.Where(up => up.UserName.ToLower() == studentName.ToLower() && up.Student != null && up.Student.UniversityRegisterNumber.ToLower() == universityRegistryNumber.ToLower()).Select(x => x.Student).FirstOrDefault();
        //                    if (student != null)
        //                    {
        //                        BatchStudent batchStudent = db.BatchStudent.Where(bs => bs.Student.ID == student.ID && bs.Batch.ID == batchViewModel.ID).FirstOrDefault();
        //                        if (batchStudent != null)
        //                        {
        //                            db.BatchStudent.Remove(batchStudent);
        //                            db.SaveChanges();
        //                        }
        //                        batchStudent = new BatchStudent();
        //                        batchStudent.Batch = db.Batch.Find(batchViewModel.ID);
        //                        batchStudent.Student = student;
        //                        db.BatchStudent.Add(batchStudent);
        //                        db.SaveChanges();
        //                        mashUploadResultViewModelList.Add(CreateMashUploadResultViewModel(studentName, true, "Success", "Student added to the Batch"));
        //                    }
        //                    else
        //                    {
        //                        string password = worksheet.Cells[i, 2].Text.ToString().Trim() ?? "";
        //                        bool result = true;
        //                        try
        //                        {
        //                            WebSecurity.CreateUserAndAccount(studentName, password);
        //                        }
        //                        catch
        //                        {
        //                            result = false;
        //                        }
        //                        if (result)
        //                        {
        //                            StudentViewModel studentViewModel = new Domain.ViewModel.StudentViewModel();
        //                            studentViewModel.Name = studentName;
        //                            studentViewModel.StartYearID = Convert.ToInt32(worksheet.Cells[i, 4].Text.ToString().Trim() ?? "0");
        //                            studentViewModel.EndYearID = Convert.ToInt32(worksheet.Cells[i, 5].Text.ToString().Trim() ?? "0");
        //                            studentViewModel.EmailId = worksheet.Cells[i, 6].Text.ToString() ?? "";
        //                            studentViewModel.PhoneNumber = worksheet.Cells[i, 7].Text.ToString() ?? "";
        //                            studentViewModel.UniversityRegistryNumber = worksheet.Cells[i, 3].Text.ToString().Trim() ?? "";
        //                            studentViewModel.CollegeID = batchViewModel.CollegeID;
        //                            studentViewModel.BranchID = batchViewModel.BranchID;
        //                            studentViewModel.SectionID = batchViewModel.SectionID;
        //                            StudentRepository _studentRepository = new StudentRepository();
        //                            try
        //                            {
        //                                _studentRepository.UpdateStudent(studentViewModel);
        //                                Roles.AddUserToRole(studentViewModel.Name, "Student");
        //                                BatchStudent batchStudent = new BatchStudent();
        //                                batchStudent.Batch = db.Batch.Find(batchViewModel.ID);
        //                                batchStudent.Student = db.UserProfile.Where(up => up.Student != null && up.UserName.ToLower() == studentViewModel.Name.ToLower()).Select(s => s.Student).FirstOrDefault();
        //                                db.BatchStudent.Add(batchStudent);
        //                                db.SaveChanges();
        //                                mashUploadResultViewModelList.Add(CreateMashUploadResultViewModel(studentName, true, "Success", "Student created and added to the batch"));
        //                            }
        //                            catch (Exception ex)
        //                            {
        //                                mashUploadResultViewModelList.Add(CreateMashUploadResultViewModel(studentName, false, "Success", "Student creation failed due to non-availablity of any one of the following information College,Branch,Section,University Register Number"));
        //                            }
        //                        }
        //                        else
        //                        {
        //                            mashUploadResultViewModelList.Add(CreateMashUploadResultViewModel(studentName, false, "Failed", "Student creation failed due to duplication of Student Name"));
        //                        }

        //                    }
        //                }
        //            }
        //        }

        //        ViewBag.ReturnUrl = "Batch/Index";
        //        return View("~/Views/Shared/UploadedResult.cshtml", mashUploadResultViewModelList);
        //    }
        //    ViewBag.Controller = "Batch";
        //    ViewBag.Action = "Index";
        //    ViewBag.PageTitle = "Batch";
        //    ViewBag.ReturnUrl = "Batch/Index";
        //    return View("~/Views/Shared/_MassUpload.cshtml");

        //}

        public MashUploadResultViewModel CreateMashUploadResultViewModel(string name, bool isSuccess, string Message, string reason)
        {
            MashUploadResultViewModel mashUploadResultViewModel = new Domain.ViewModel.MashUploadResultViewModel();
            mashUploadResultViewModel.Name = name;
            mashUploadResultViewModel.IsSuccess = isSuccess;
            mashUploadResultViewModel.Message = Message;
            mashUploadResultViewModel.Reason = reason;
            return mashUploadResultViewModel;
        }

        #endregion

        #region Get Batch Under Assessment

        public void GetBatchUnderQuestionPaper(int assessmentID)
        {
            QuestionPaper questionPaper = db.QuestionPaper.Find(assessmentID);
            List<BatchViewModel> batchViewModelList = new List<BatchViewModel>();
            UserProfile userProfile = db.UserProfile.Where(up => up.UserName.ToLower() == User.Identity.Name.ToLower()).FirstOrDefault();
            if (questionPaper != null && userProfile != null)
            {
                List<Batch> batchList = new List<Batch>();
                if (User.IsInRole("SuperAdmin"))
                {
                    batchList = questionPaper.BatchAssessment.Select(x => x.Batch).Where(b => b.BatchStudent.Count() > 0).ToList();
                }
                if (User.IsInRole("CollegeAdmin"))
                {
                    batchList = questionPaper.BatchAssessment.Select(x => x.Batch).Where(b => b.BatchStudent.Count() > 0).ToList();
                }
                else if (User.IsInRole("InstituteAdmin"))
                {
                    List<long> collegeIdList = userProfile.UserProfileCollege.Select(x => x.CollegeID).ToList();
                    batchList = questionPaper.BatchAssessment.Select(x => x.Batch).Where(b => b.BatchStudent.Where(bs => (collegeIdList.Contains(bs.Student.CollegeID ?? 0))).Count() > 0).ToList();
                }

                batchViewModelList = MapperHelper.MapToViewModelList(batchList.AsQueryable());
                batchViewModelList = batchViewModelList;
            }
            Response.Write(JsonConvert.SerializeObject(batchViewModelList));
        }

        #endregion


        #region Batch Assessments

        public ActionResult BatchQuestionPaper()
        {
            ViewBag.Question = (from b in db.QuestionPaper
                                group b by new
                                {
                                    b.Name
                                } into bt
                                select bt.FirstOrDefault());
            //ViewBag.Sections = db.Section.ToList();
            ViewBag.Batches = (from b in db.Batch
                               group b by new
                               {
                                   b.Name
                               } into bt
                               select bt.FirstOrDefault());

            return View();
        }
        public void GetBatchAssessmentList()
        {
            datatableRequest = new DataTableRequest(Request, columns);

            // IQueryable<Assessment> assessments = db.Assessment;
            IQueryable<BatchAssessment> batchAssessments = db.BatchAssessment;

            if (Request.QueryString["college"] != null && Request.QueryString["college"] != "0" && Request.QueryString["college"] != "")
            {
                long collegeID = Convert.ToInt64(Request.QueryString["college"]);
                batchAssessments = batchAssessments.Where(a => a.Batch.College.ID == collegeID);
            }
            if (Request.QueryString["Batch"] != null && Request.QueryString["Batch"] != "0")
            {
                long batchID = Convert.ToInt64(Request.QueryString["Batch"]);
                batchAssessments = batchAssessments.Where(a => a.BatchID == batchID);
            }
            if (Request.QueryString["Question"] != null && Request.QueryString["Question"] != "0")
            {
                long assessmentID = Convert.ToInt64(Request.QueryString["Question"]);
                batchAssessments = batchAssessments.Where(a => a.AssessmentID == assessmentID);
            }
            int totalRecords = batchAssessments.Count();
            int filteredCount = totalRecords;
            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                batchAssessments = (from c in db.BatchAssessment where c.QuestionPaper.Name.Contains(datatableRequest.SearchString) || (c.Batch != null ? c.Batch.Name.Contains(datatableRequest.SearchString) : false) || (c.Batch != null ? c.Batch.College != null ? c.Batch.College.Name.Contains(datatableRequest.SearchString) : false : false) select c);
                filteredCount = batchAssessments.Count();
            }
            batchAssessments = batchAssessments.OrderBy(x => x.Name);
            //if (datatableRequest.ShouldOrder)
            //{
            //    batchAssessments = batchAssessments.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            //}
            batchAssessments = batchAssessments.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            List<OAP.Domain.ViewModel.BatchQuestionPaperViewModel> batchAssessmentViewModelList = MapperHelper.MapToViewModelList(batchAssessments.OrderBy(x => x.Batch.Name));
            //batchAssessmentViewModelList = sh.SortBatchAssessmentViewModelList(batchAssessmentViewModelList);
            BatchQuestionPaperDataTableViewModel dataTableViewModel = new BatchQuestionPaperDataTableViewModel(batchAssessmentViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);

        }

        public ActionResult BatchQuestionPaperAdd()
        {
            ViewBag.College = db.College.GroupBy(x => x.Name).Select(x => x.FirstOrDefault());
            ViewBag.Question = (from b in db.QuestionPaper
                                group b by new
                                {
                                    b.Name
                                } into bt
                                select bt.FirstOrDefault());
            ViewBag.Batches = MapperHelper.MapToViewModelList((from b in db.Batch
                                                               group b by new
                                                               {
                                                                   b.Name
                                                               } into bt
                                                               select bt.FirstOrDefault()));
            return View();
        }
        [HttpPost]
        public ActionResult BatchQuestionPaperAdd(BatchQuestionPaperViewModel batchAssessmentViewModel)
        {
            if (batchAssessmentViewModel.BatchIDString == null || batchAssessmentViewModel.QuestionPaperID == 0 || string.IsNullOrWhiteSpace(batchAssessmentViewModel.StartDateString) || string.IsNullOrWhiteSpace(batchAssessmentViewModel.EndDateString))
            {
                ViewBag.College = db.College;
                ViewBag.Question = db.QuestionPaper;
                ViewBag.Batches1 = db.Batch;
                return View(batchAssessmentViewModel);
            }

            if (batchAssessmentViewModel.BatchIDString != null)
            {
                List<string> BatchIDList = batchAssessmentViewModel.BatchIDString.Split(',').ToList();
                foreach (string batchID in BatchIDList)
                {
                    long id = Convert.ToInt64(batchID);
                    OAP.Domain.BatchAssessment batchAssessment = new BatchAssessment();
                    batchAssessment.Name = batchAssessmentViewModel.Name;
                    batchAssessment.CanReview = batchAssessmentViewModel.CanReview;
                    batchAssessment.BatchID = Convert.ToInt32(id);
                    batchAssessment.AssessmentID = Convert.ToInt32(batchAssessmentViewModel.QuestionPaperID);
                    batchAssessment.StartDateTime = DateTime.ParseExact(batchAssessmentViewModel.StartDateString, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture);
                    batchAssessment.EndDateTime = DateTime.ParseExact(batchAssessmentViewModel.EndDateString, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture);
                    batchAssessment.CanReview = batchAssessmentViewModel.CanReview;
                    db.BatchAssessment.Add(batchAssessment);
                }
            }
            db.SaveChanges();
            return RedirectToAction("BatchQuestionPaper");
        }

        public ActionResult BatchQuestionPaperEdit(long id = 0)
        {
            OAP.Domain.BatchAssessment batchAssessment = db.BatchAssessment.Find(id);
            if (batchAssessment == null)
            {
                return HttpNotFound();
            }
            BatchQuestionPaperViewModel batchAssessmentViewModel = MapperHelper.MapToViewModel(batchAssessment);
            ViewBag.College = db.College;
            ViewBag.Question = (from b in db.QuestionPaper
                                group b by new
                                {
                                    b.Name
                                } into bt
                                select bt.FirstOrDefault()); ;
            ViewBag.Batches = (from b in db.Batch
                               group b by new
                               {
                                   b.Name
                               } into bt
                               select bt.FirstOrDefault()); ;

            ViewBag.Name = batchAssessment.Name;

            //BatchAssessment batchAssessment = db.BatchAssessment.Find(id);
            List<Student> studentList = batchAssessment.Batch.BatchStudent.Select(x => x.Student).ToList();
            List<QuestionPaperSection> questionPaperSectionList = batchAssessment.QuestionPaper.QuestionPaperSection.ToList();
            List<QuestionPaperSectionViewModel> questionPaperSectionModelList = MapperHelper.MapToViewModelList(questionPaperSectionList);
            //List<StudentViewModel> studentModelList = MapperHelper.MapToViewModelList(studentList);

            List<StudentViewModel> studentModelList = new List<StudentViewModel>();

            foreach (Student item in studentList)
            {
                IEnumerable<UserSectionStatus> userAssessments = item.UserProfile.FirstOrDefault().UserAssessment.Where(y => y.AssessmentID == batchAssessment.AssessmentID).SelectMany(z => z.UserSectionStatus);
                List<StudentSectionMarksModel> studentsMarksModelList = (from ua in userAssessments
                                                                         select new StudentSectionMarksModel()
                                                                         {
                                                                             Marks = ua.Marks != null ? ua.Marks.Value : 0,
                                                                             // Name=ua.UserProfile.Student.Name,
                                                                             //UniversityRegNo=ua.UserProfile.Student.UniversityRegisterNumber,
                                                                             //SectionIndex=ua.CurrentSectionIndex??-1,
                                                                             //StudentID=ua.UserProfile.Student.ID,
                                                                             Total = userAssessments.Sum(x => x.Marks ?? 0),

                                                                         }).OrderBy(x => x.SectionIndex).ToList();
                StudentViewModel studentViewModel = MapperHelper.MapToViewModel(item);
                studentViewModel.SectionMarksDetails = studentsMarksModelList;
                studentModelList.Add(studentViewModel);
            }



            batchAssessmentViewModel.QuestionPaperSectionList = questionPaperSectionModelList;
            batchAssessmentViewModel.studentViewModelList = studentModelList;
            batchAssessmentViewModel.SectionCounts = questionPaperSectionModelList.Count();

            batchAssessmentViewModel.CanReview = batchAssessment.CanReview ?? false;

            return View(batchAssessmentViewModel);
        }

        [HttpPost]
        public ActionResult BatchQuestionPaperEdit(BatchQuestionPaperViewModel batchAssessmentViewModel)
        {
            if (batchAssessmentViewModel.BatchID == 0 || batchAssessmentViewModel.QuestionPaperID == 0 || string.IsNullOrWhiteSpace(batchAssessmentViewModel.StartDateString) || string.IsNullOrWhiteSpace(batchAssessmentViewModel.EndDateString))
            {
                ViewBag.College = db.College;
                ViewBag.Question = db.QuestionPaper;
                ViewBag.Batches = db.Batch;
                return View(batchAssessmentViewModel);
            }
            OAP.Domain.BatchAssessment batchAssessment = db.BatchAssessment.Find(batchAssessmentViewModel.ID);
            batchAssessment.Name = batchAssessmentViewModel.Name;
            batchAssessment.BatchID = Convert.ToInt32(batchAssessmentViewModel.BatchID);
            batchAssessment.AssessmentID = Convert.ToInt32(batchAssessmentViewModel.QuestionPaperID);
            batchAssessment.StartDateTime = DateTime.ParseExact(batchAssessmentViewModel.StartDateString, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture);
            batchAssessment.EndDateTime = DateTime.ParseExact(batchAssessmentViewModel.EndDateString, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture);
            batchAssessment.CanReview = batchAssessmentViewModel.CanReview;
            db.Entry(batchAssessment).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("BatchQuestionPaper");
        }
        public void QuestionDetails(string selectedQuestionPaperID)
        {
            string result = "";
            if (!string.IsNullOrWhiteSpace(selectedQuestionPaperID))
            {
                QuestionPaper questionPaper = db.QuestionPaper.Find(Convert.ToInt64(selectedQuestionPaperID));

                QuestionPaperViewModel QuestionPaperViewModel = MapperHelper.MapToViewModelForProgram(questionPaper);
                result = JsonConvert.SerializeObject(QuestionPaperViewModel);
            }
            Response.Write(result);

        #endregion

        }
        public void GetQuestionPaperByID(string selectedQuestionPaperID)
        {
            long QuestionPaperID = Convert.ToInt64(selectedQuestionPaperID);
            QuestionPaper questionPaper = db.QuestionPaper.Find(QuestionPaperID);
            List<QuestionPaperSection> questionPaperSectionList = db.QuestionPaperSection.Where(q => q.AssessmentID == QuestionPaperID).ToList();
            List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = MapperHelper.MapToViewModelList(questionPaperSectionList);
            foreach (QuestionPaperSectionViewModel questionPaperSection in questionPaperSectionViewModelList)
            {
                List<QuestionPaperSectionQuestion> sectionQuestionList = db.QuestionPaperSectionQuestion.Where(q => q.AssessmentSectionID == questionPaperSection.ID).ToList();
                questionPaperSection.questionPaperSectionQuestionList = MapperHelper.MapToViewModelList(sectionQuestionList);
            }
            string Result = JsonConvert.SerializeObject(questionPaperSectionViewModelList);
            Response.Write(Result);
        }

        public void LoadBatchAssessmentBatchResults(long batchAssessmentID)
        {
            //List<QuestionPaperSection> questionpapersection = db.BatchAssessment.SelectMany(x => x.QuestionPaper.QuestionPaperSection).ToList();
            //List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = MapperHelper.MapToViewModelList(questionpapersection);
            //questionPaperSectionViewModelList = sh.SortQuestionPaperSection(questionPaperSectionViewModelList);
            //string Result = JsonConvert.SerializeObject(questionPaperSectionViewModelList);
            //Response.Write(Result);
            // IEnumerable<BatchAssessment> batchAssessmentList = db.BatchAssessment.Where(x => x.Batch.BatchStudent.Where(y=>y.BatchID==BatchID)).ToList();


            BatchAssessment batchAssessment = db.BatchAssessment.Find(batchAssessmentID);
            List<Student> studentList = batchAssessment.Batch.BatchStudent.Select(x => x.Student).ToList();
            List<StudentViewModel> studentModelList = MapperHelper.MapToViewModelList(studentList.AsQueryable());

            List<QuestionPaperSection> questionPaperSectionList = batchAssessment.QuestionPaper.QuestionPaperSection.ToList();
            List<QuestionPaperSectionViewModel> questionPaperSectionModelList = MapperHelper.MapToViewModelList(questionPaperSectionList);

            BatchQuestionPaperViewModel batchAssmentViewModel = new BatchQuestionPaperViewModel();
            batchAssmentViewModel.studentViewModelList = studentModelList;
            batchAssmentViewModel.QuestionPaperSectionList = questionPaperSectionModelList;

            string Result = JsonConvert.SerializeObject(batchAssmentViewModel);
            Response.Write(Result);
        }

        public ActionResult BatchResultDownloadAsExcelFile(long id)
        {
            OAP.Domain.BatchAssessment batchAssessment = db.BatchAssessment.Find(id);
            if (batchAssessment == null)
            {
                // return HttpNotFound();
            }
            BatchQuestionPaperViewModel batchAssessmentViewModel = MapperHelper.MapToViewModel(batchAssessment);

            List<Student> studentList = batchAssessment.Batch.BatchStudent.Select(x => x.Student).ToList();
            List<QuestionPaperSection> questionPaperSectionList = batchAssessment.QuestionPaper.QuestionPaperSection.ToList();
            List<QuestionPaperSectionViewModel> questionPaperSectionModelList = MapperHelper.MapToViewModelList(questionPaperSectionList);

            List<StudentViewModel> studentModelList = new List<StudentViewModel>();

            foreach (Student item in studentList)
            {
                IEnumerable<UserSectionStatus> userAssessments = item.UserProfile.FirstOrDefault().UserAssessment.Where(y => y.AssessmentID == batchAssessment.AssessmentID).SelectMany(z => z.UserSectionStatus);
                List<StudentSectionMarksModel> studentsMarksModelList = (from ua in userAssessments
                                                                         select new StudentSectionMarksModel()
                                                                         {
                                                                             Marks = ua.Marks != null ? ua.Marks.Value : 0,
                                                                             Total = userAssessments.Sum(x => x.Marks ?? 0),

                                                                         }).OrderBy(x => x.SectionIndex).ToList();
                StudentViewModel studentViewModel = MapperHelper.MapToViewModel(item);
                studentViewModel.SectionMarksDetails = studentsMarksModelList;
                studentModelList.Add(studentViewModel);
            }

            batchAssessmentViewModel.QuestionPaperSectionList = questionPaperSectionModelList;
            batchAssessmentViewModel.studentViewModelList = studentModelList;
            batchAssessmentViewModel.SectionCounts = questionPaperSectionModelList.Count();

            //ExcelProcess

            string print = "BatchResult_" + batchAssessmentViewModel.Name;
            string Name = print;
            string[] ExcelFiles = Directory.GetFiles(Server.MapPath("~/Excel/"), "*.xls").Select(path => Path.GetFileName(path)).ToArray();
            int j = 1;
            for (int i = 0; i < ExcelFiles.Length; i++)
            {
                if (ExcelFiles.Contains(print + ".xls"))
                {
                    if (j > 10)
                    {
                        while (Name != print)
                        {
                            print = print.Remove(print.Length - 2);
                        }
                    }
                    else
                    {
                        while (Name != print)
                        {
                            print = print.Remove(print.Length - 1);
                        }
                    }
                    print = print + j;
                    j++;
                }

            }
            string filename = Server.MapPath("~/Excel/" + print + ".xls");
            MemoryStream excelstream = new MemoryStream();
            FileInfo newFile = new FileInfo(filename);
            using (ExcelPackage xlPackage = new ExcelPackage(newFile))
            {
                ExcelWorksheet worksheet = xlPackage.Workbook.Worksheets.Add("Batch Result " + GetCurrentDateStringforReport());

                using (var range = worksheet.Cells[1, 1])
                {
                    range.Value = "Register No.";
                    range.Merge = true;
                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    range.AutoFitColumns();
                }
                using (var range = worksheet.Cells[1, 2])
                {
                    range.Value = "Name";
                    range.Merge = true;
                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    range.AutoFitColumns();
                }
                int i = 3;
                foreach (QuestionPaperSectionViewModel questionPaperSectionViewModel in questionPaperSectionModelList)
                {
                    using (var range = worksheet.Cells[1, i])
                    {
                        range.Value = questionPaperSectionViewModel.Name;
                        range.Merge = true;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.AutoFitColumns();
                    }
                    i++;
                }
                using (var range = worksheet.Cells[1, i])
                {
                    range.Value = "Total";
                    range.Merge = true;
                    range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                    range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    range.AutoFitColumns();
                }


                int k = 0;
                foreach (StudentViewModel studentViewModel in studentModelList)
                {
                    using (var range = worksheet.Cells[2 + k, 1])
                    {
                        range.Value = studentViewModel.UniversityRegistryNumber;
                        range.Merge = true;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.AutoFitColumns();
                    }
                    using (var range = worksheet.Cells[2 + k, 2])
                    {
                        range.Value = studentViewModel.FullName;
                        range.Merge = true;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.AutoFitColumns();
                    }
                    double totalmark = 0.0;
                    int celladdress = 3;
                    for (int m = 0; m < batchAssessmentViewModel.SectionCounts; m++)
                    {
                        if (studentViewModel.SectionMarksDetails.Count() > m)
                        {
                            using (var range = worksheet.Cells[2 + k, celladdress])
                            {
                                totalmark = totalmark + (double)studentViewModel.SectionMarksDetails[m].Marks;
                                range.Value = studentViewModel.SectionMarksDetails[m].Marks;
                                range.Merge = true;
                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                range.AutoFitColumns();
                            }
                        }
                        else
                        {
                            using (var range = worksheet.Cells[2 + k, celladdress])
                            {
                                range.Value = " ";
                                range.Merge = true;
                                range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                                range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                range.AutoFitColumns();
                            }
                        }

                        celladdress++;
                    }

                    using (var range = worksheet.Cells[2 + k, celladdress])
                    {
                        range.Value = totalmark;
                        range.Merge = true;
                        range.Style.Border.BorderAround(ExcelBorderStyle.Thin, Color.Black);
                        range.Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        range.AutoFitColumns();
                    }
                    k++;
                }
                xlPackage.Save();
            }
            return SendReportAsExcel(excelstream, print);
            // Response.Write("true");
        }

        private FileStreamResult SendReportAsExcel(MemoryStream stream, string filename)
        {
            var response = new HttpResponseMessage(HttpStatusCode.OK);
            WebClient wc = new WebClient();
            stream = new MemoryStream(wc.DownloadData(Server.MapPath("~/Excel/" + filename + ".xls")));
            HttpContext.Response.AddHeader("content-disposition", "attachment; filename=" + filename + ".xls");
            FileStreamResult filestream = new FileStreamResult(stream, "application/xls");
            return filestream;
        }

        public string GetCurrentDateStringforReport()
        {
            return DateTime.Now.ToString("dd-MM-yyy hh:mm:ss");
        }



    }


}
