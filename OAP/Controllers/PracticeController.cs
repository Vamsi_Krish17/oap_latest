﻿using HelperClasses;
using Newtonsoft.Json;
using OAP.Domain;
using OAP.Domain.ViewModel;
using OAP.Domain.ViewModel.DataTableViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static OAP.Controllers.BaseController;
using static OAP.Domain.ViewModel.DataTableViewModel.QuestionDataTableViewModel;

namespace OAP.Controllers
{
    [Authorize(Roles = "SuperAdmin,CollegeAdmin,Admin,Staff")]
    public class PracticeController : Controller
    {
        private OAPEntities db = new OAPEntities();
        public static string saved = "";
        public DataTableRequest datatableRequest { get; set; }

        //
        // GET: /Practice/

        public ActionResult Index()
        {
            ViewBag.Saved = saved;
            saved = "";
            return View();
        }


        public ActionResult Create()
        {
            //ViewBag.Branches = db.Branch;
            return View();
        }

        [HttpPost]
        public ActionResult Create(PracticeSchoolViewModel practiceSchoolViewModel, string save)
        {
            if (save == "Save")
            {
                PracticeSchool practiceSchool = MapperHelper.MapToDomainObject(practiceSchoolViewModel);
                db.PracticeSchool.Add(practiceSchool);
                db.SaveChanges();
                saved = "Session created successfully!";
                return RedirectToAction("Index");
            }
            else
            {
                return View(practiceSchoolViewModel);
            }
        }

        public void List()
        {
            var practiceSessionList = db.PracticeSchool.ToList();
            int totalRecords = practiceSessionList.Count();
            var practiceSessionViewModelList = new List<PracticeSchoolViewModel>();
            foreach (var ps in practiceSessionList)
            {
                practiceSessionViewModelList.Add(new PracticeSchoolViewModel()
                {
                    ID = ps.ID,
                    IsTechnicalSession = ps.IsTechnicalSession ?? false,
                    SessionName = ps.SessionName,
                    Section1 = ps.Section1,
                    Section2 = ps.Section2,
                    Section3 = ps.Section3,
                    Section4 = ps.Section4
                });
            }
            PracticeSchoolDataTable practiceSchoolDataTable = new PracticeSchoolDataTable(practiceSessionViewModelList, 0, totalRecords, 0);

            string result = JsonConvert.SerializeObject(practiceSchoolDataTable);
            Response.Write(result);
        }

        public ActionResult Edit(int id = 0)
        {
            Session["ID"] = id;
            PracticeSchool practiceSchool = db.PracticeSchool.Find(id);
            if (practiceSchool == null)
            {
                return HttpNotFound();
            }
            PracticeSchoolViewModel practiceSchoolViewModel = new PracticeSchoolViewModel();
            practiceSchoolViewModel.IsTechnicalSession = practiceSchool.IsTechnicalSession ?? false;
            practiceSchoolViewModel.Section1 = practiceSchool.Section1;
            practiceSchoolViewModel.Section2 = practiceSchool.Section2;
            practiceSchoolViewModel.Section3 = practiceSchool.Section3;
            practiceSchoolViewModel.Section4 = practiceSchool.Section4;
            practiceSchoolViewModel.SessionName = practiceSchool.SessionName;
            return View(practiceSchoolViewModel);
        }


        public ActionResult AddLevel(int id = 0)
        {
            //ViewBag.Branches = db.Branch;
            return View();
        }

        [HttpPost]
        public ActionResult AddLevel(PracticeSchoolLevelModel practiceSchoolLevelModel, string save)
        {
            if (save == "Save")
            {
                PracticeSchoolLevel practiceSchoolLevel = new PracticeSchoolLevel();
                practiceSchoolLevel.LevelName = practiceSchoolLevelModel.LevelName;
                practiceSchoolLevel.PracticeSchoolId = practiceSchoolLevelModel.ID;
                db.PracticeSchoolLevel.Add(practiceSchoolLevel);
                db.SaveChanges();
                saved = "Level created successfully!";
                return RedirectToAction("Level/" + practiceSchoolLevelModel.ID);
            }
            else
            {
                return View(practiceSchoolLevelModel);
            }
        }

        public void ListLevel(string id)
        {
            int psId = 0;
            if (!string.IsNullOrEmpty(id))
                psId = Convert.ToInt32(id);
            var practiceSessionLevelList = db.PracticeSchoolLevel.Where(x => x.PracticeSchoolId == psId).ToList();
            int totalRecords = practiceSessionLevelList.Count();
            var practiceLevel = new List<PracticeSchoolLevelModel>();
            foreach (var pl in practiceSessionLevelList)
            {
                practiceLevel.Add(new PracticeSchoolLevelModel()
                {
                    ID = pl.ID,
                    LevelName = pl.LevelName,
                    PracticeSchoolID = pl.PracticeSchoolId,
                });
            }
            PracticeSchoolLevelDataTable practiceSchoolLevelDataTable = new PracticeSchoolLevelDataTable(practiceLevel, 0, totalRecords, 0);

            string result = JsonConvert.SerializeObject(practiceSchoolLevelDataTable);
            Response.Write(result);
        }


        public ActionResult Level(int id = 0)
        {
            ViewBag.PracticeSchoolId = id;
            ViewBag.Saved = saved;
            saved = "";
            return View();
        }


        public ActionResult MapLevelQuestions(string id)
        {
            int levelId = 0;
            if (!string.IsNullOrEmpty(id))
                levelId = Convert.ToInt32(id);
            LevelTechnicalQuestionViewModel levelTechnicalQuestionViewModel = new LevelTechnicalQuestionViewModel();
            levelTechnicalQuestionViewModel.SessionLevelId = levelId;
            IQueryable<Tag> tagsList = db.Tag.GroupBy(x => x.Name).Select(x => x.FirstOrDefault());
            levelTechnicalQuestionViewModel.TagViewModelList = MapperHelper.MapToViewModelList(tagsList.OrderBy(x => x.Name));
            return View(levelTechnicalQuestionViewModel);
        }



        public void GetSelectedQuestions(int id, string tags)
        {
            string[] columns = { "Description" };
            datatableRequest = new DataTableRequest(Request, columns);
            IQueryable<TechnicalQuestion> questions = db.PracticeSchoolLevelQuestion.Where(x => x.PracticeSchoolLevelId == id).Select(x => x.TechnicalQuestion);

            if (!string.IsNullOrWhiteSpace(tags))
            {
                List<long> tagIDList = tags.Split(',').ToList().Select(long.Parse).ToList();
                questions = questions.Where(q => q.PracticeSchoolLevelQuestion.Where(asq => asq.PracticeSchoolLevelId == id).Count() <= 0 && (q.TechnicalQuestionTags.Where(qt => tagIDList.Contains(qt.TagID)).Count() > 0));
            }
            int totalRecords = 0;
            int filteredCount = 0;
            totalRecords = questions.Count();
            filteredCount = totalRecords;

            if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
            {
                //questions = (from q in db.TechnicalQuestion where q.Description.Contains(datatableRequest.SearchString) select q);
                questions = questions.Where(q => q.Description.Contains(datatableRequest.SearchString)).Select(x => x);
                filteredCount = questions.Count();
            }

            questions = questions.OrderBy(x => x.Description);
            if (datatableRequest.ShouldOrder)
            {
                questions = questions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
            }
            questions = questions.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            List<TechnicalQuestionViewModel> questionViewModelList = MapperHelper.MapToTechnicalViewModelList(questions);

            TechnicalQuestionDataTableViewModel dataTableViewModel = new TechnicalQuestionDataTableViewModel(questionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }


        public void GetAvailableQuestions(int id, string tags)
        {
            string[] columns = { "Description" };
            datatableRequest = new DataTableRequest(Request, columns);
            IQueryable<TechnicalQuestion> questions = (from q in db.TechnicalQuestion
                                                       group q by new
                                                       {
                                                           q.Description
                                                       } into qs
                                                       select qs.FirstOrDefault());
            PracticeSchoolLevel practiceSchoolLevelQuestion = db.PracticeSchoolLevel.Find(id);
            int totalRecords = 0;
            int filteredCount = 0;
            if (practiceSchoolLevelQuestion != null)
            {
                List<TechnicalQuestion> existingQuestions = practiceSchoolLevelQuestion.PracticeSchoolLevelQuestion.Select(x => x.TechnicalQuestion).Distinct().ToList();
                List<int> existingQuestionIDs = existingQuestions.Select(x => x.ID).Distinct().ToList();

                if (!string.IsNullOrWhiteSpace(tags))
                {
                    List<long> tagIDList = tags.Split(',').ToList().Select(long.Parse).ToList();
                    questions = questions.Where(q => q.PracticeSchoolLevelQuestion.Where(asq => asq.PracticeSchoolLevelId == id).Count() <= 0 && (q.TechnicalQuestionTags.Where(qt => tagIDList.Contains(qt.TagID)).Count() > 0));
                }



                if (!string.IsNullOrWhiteSpace(datatableRequest.SearchString))
                {
                    questions = (from q in db.TechnicalQuestion where q.Description.Contains(datatableRequest.SearchString) select q);
                    filteredCount = questions.Count();
                }

                questions = questions.OrderBy(x => x.Description);
                if (datatableRequest.ShouldOrder)
                {
                    questions = questions.OrderByField(datatableRequest.OrderByColumn, datatableRequest.IsOrderAsccending);
                }
                questions = questions.Where(q => !existingQuestionIDs.Contains(q.ID));
                totalRecords = questions.Count();
                filteredCount = totalRecords;
                questions = questions.Skip(datatableRequest.ItemsToSkip).Take(datatableRequest.LengthRequired);
            }

            List<TechnicalQuestionViewModel> questionViewModelList = MapperHelper.MapToTechnicalViewModelList(questions);

            TechnicalQuestionDataTableViewModel dataTableViewModel = new TechnicalQuestionDataTableViewModel(questionViewModelList, datatableRequest.DrawRequestNumber, totalRecords, filteredCount);

            string result = JsonConvert.SerializeObject(dataTableViewModel);
            Response.Write(result);
        }

        public void MapQuestions(string questionIDString, string Id)
        {
            int sessionLevelId = Convert.ToInt16(Id);
            List<int> questionIDList = questionIDString.Split(',').ToList().Select(int.Parse).ToList();

            PracticeSchoolLevel practiceSchoolLevel = db.PracticeSchoolLevel.Find(sessionLevelId);
            foreach (int questionID in questionIDList)
            {
                //TechnicalQuestion question = db.TechnicalQuestion.Find(questionID);

                PracticeSchoolLevelQuestion practiceSchoolLevelQuestion = new PracticeSchoolLevelQuestion();
                practiceSchoolLevelQuestion.PracticeSchoolLevelId = sessionLevelId;
                practiceSchoolLevelQuestion.TechnicalQuestionId = questionID;
                db.PracticeSchoolLevelQuestion.Add(practiceSchoolLevelQuestion);
            }
            db.SaveChanges();
            Response.Write("true");
        }

        public void UnMapQuestions(string questionIDString, string Id)
        {
            int sectionLevelId = Convert.ToInt16(Id);
            List<int> questionIDList = questionIDString.Split(',').ToList().Select(int.Parse).ToList();

            List<PracticeSchoolLevelQuestion> practiceSchoolLevelQuestionList = new List<PracticeSchoolLevelQuestion>();

            PracticeSchoolLevel practiceSchoolLevel = db.PracticeSchoolLevel.Find(sectionLevelId);

            foreach (int questionID in questionIDList)
            {
                PracticeSchoolLevelQuestion practiceSchoolLevelQuestion = practiceSchoolLevel.PracticeSchoolLevelQuestion.Where(asq => asq.TechnicalQuestionId == questionID).FirstOrDefault();
                if (practiceSchoolLevelQuestion != null)
                {
                    db.PracticeSchoolLevelQuestion.Remove(practiceSchoolLevelQuestion);
                }
            }
            db.SaveChanges();
            Response.Write("true");

        }
    }
}
