﻿using OAP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace OAP.Attribute
{
    public class CustomErrorHandler : HandleErrorAttribute
    {
        public  OAPEntities db = new OAPEntities();

        private bool IsAjax(ExceptionContext filterContext)
        {
            return filterContext.HttpContext.Request.Headers["X-Requested-With"] == "XMLHttpRequest";
        }
        public override void OnException(ExceptionContext filterContext)
        {
            // Write error logging code here if you wish.
            var currentController = (string)filterContext.RouteData.Values["controller"];
            var currentActionName = (string)filterContext.RouteData.Values["action"];
            LogException(filterContext.Exception, currentController, currentActionName,db);

            if (filterContext.ExceptionHandled || !filterContext.HttpContext.IsCustomErrorEnabled)
            {
                return;
            }

            // if the request is AJAX return JSON else view.
            if (IsAjax(filterContext))
            {
                //Because its a exception raised after ajax invocation
                //Lets return Json
                filterContext.Result = new JsonResult()
                {
                    Data = filterContext.Exception.Message,
                    JsonRequestBehavior = JsonRequestBehavior.AllowGet
                };

                filterContext.ExceptionHandled = true;
                filterContext.HttpContext.Response.Clear();
            }
            else
            {
                //Normal Exception
                //So let it handle by its default ways.
                base.OnException(filterContext);

            }

        }


        public static string GetErrorAsString(Exception ex)
        {
            Exception objTempException = ex;
            int intExceptionLevel = 0;
            string strIndent = null;
            StringBuilder objSB = new StringBuilder();
            string strFormData = null;
            objSB.Append("-----------------" + System.DateTime.Now.ToString() + "-----------------" + Environment.NewLine);


            while ((objTempException != null))
            {
                //is this the 1st, 2nd, etc exception in the hierarchy 
                intExceptionLevel += 1;
                objSB.Append(intExceptionLevel.ToString() + ": Error Description:" + objTempException.Message + Environment.NewLine);

                if (objTempException.Source != null)
                {
                    objSB.Append(intExceptionLevel.ToString() + ": Source:" + objTempException.Source.Replace(Environment.NewLine, Environment.NewLine + intExceptionLevel.ToString() + ": ") + Environment.NewLine);
                }

                if (objTempException.StackTrace != null)
                {
                    objSB.Append(intExceptionLevel.ToString() + ": Stack Trace:" + objTempException.StackTrace.Replace(Environment.NewLine, Environment.NewLine + intExceptionLevel.ToString() + ": ") + Environment.NewLine);
                }

                if (objTempException.TargetSite != null)
                {
                    objSB.Append(intExceptionLevel.ToString() + ": Target Site:" + objTempException.TargetSite.ToString().Replace(Environment.NewLine, Environment.NewLine + intExceptionLevel.ToString() + ": ") + Environment.NewLine);
                }

                //get the next exception to log 

                objTempException = objTempException.InnerException;
            }


            return objSB.ToString();
        }


        internal static void LogException(Exception exception, string currentController, string currentActionName,OAPEntities db)
        {
            ErrorLog errorLog = new ErrorLog();
            errorLog.Message = GetErrorAsString(exception);
            errorLog.StackTrace = exception != null ? exception.StackTrace : "";
            errorLog.Source = exception != null ? exception.Source : "";
            errorLog.Controller = currentController;
            errorLog.Action = currentActionName;
            errorLog.EventDateUTC = DateTime.UtcNow;
            errorLog.InnerException = GetErrorAsString(exception.InnerException);
            db.ErrorLog.Add(errorLog);
            db.SaveChanges();
        }

    }
}


