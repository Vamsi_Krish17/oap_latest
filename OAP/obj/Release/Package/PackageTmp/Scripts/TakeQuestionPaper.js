﻿var currentQuestion;
var selectedQuestionIndex;
var selectedSectionIndex;
var selectedSectionsIndex;
var totalQuestions;
var totalSections;
var totalQuestionPaperQuestions;
var isSectionJump;
var MarkQuestionIndex = 0;
var ColorQuestionID = 0;

var sectionHide = 0;
var questionHide = 0;
var QuestionHight = 0;
var SectionHight = 0;
var QuestionTop = 0;
var SectionTop = 0;
var testmethod;
var ExistingQuestionContainer = 0;
var NewQuestionContainer = 1;

var CurrentQuestionColor;
var MarkedQuestionColor;
var ViewUnAnswerQuestionColor;
var ViewAnswerQuestionColor;
var NotVisitedQuestions;
var testSubmitted = false;
var sectionSubmitted = false;

function RemoveAdditionalStyles() {
    $(".Active").removeClass("NotVisited");
    $(".Active").removeClass("VisitedAnswered");
    $(".Active").removeClass("VisitedUnanswer");
    $(".Active").removeClass("CurrentQuestion");
    $(".Active").removeClass("Marked");
    $(".Active").removeClass("VisitedAnswered");

}
$(function () {
    CurrentQuestionColor = $('.ActionColor1').val();
    MarkedQuestionColor = $('.ActionColor2').val();
    NotVisitedQuestions = $('.ActionColor3').val();
    ViewAnswerQuestionColor = $('.ActionColor4').val();
    NotVisitedQuestions = $('.ActionColor5').val();
    //window.history.forward();
    //function noBack() { window.history.forward(); }
    //window.onbeforeunload = function () { return "Your work will be lost."; };
    AlignBottomContainer();

    //$.gritter.add({
    //    // (string | mandatory) the heading of the notification
    //    title: 'This is a regular notice!',
    //    // (string | mandatory) the text inside the notification
    //    //text: 'This will fade out after a certain amount of time. Vivamus eget tincidunt velit. Cum sociis natoque penatibus et <a href="#" style="color:#ccc">magnis dis parturient</a> montes, nascetur ridiculus mus.',
    //    //// (string | optional) the image to display on the left
    //    //image: 'images/avatar-mini.jpg',
    //    //// (bool | optional) if you want it to fade out on its own or just sit there
    //    //sticky: false,
    //    // (int | optional) the time you want it to be alive for before fading out
    //    time: ''
    //});
    testmethod = $(".isSectionJump").val();
    isSectionJump = $(".isSectionJump").val();
    if (testmethod == "True") {
        totalQuestions = $(".section1").val();
    }
    else {
        totalQuestions = $(".QuestionContainer").length;
    }
    totalSections = $(".SectionContainer").length;
    totalQuestionPaperQuestions = $(".QuestionContainer").length;
    selectedQuestionIndex = 1;
    selectedSectionsIndex = 1;
    ShowCurrentSections();
    ShowCurrentQuestion();
    EnableButtons();

    $("#btnPrev").click(function () {        //anu 
        RemoveAdditionalStyles();
        var lastStatus = $(".Active").attr("status");   // Move Previous section to prev's section 
        //alert(" Prev   lastStatus  ==" + lastStatus);
        if (lastStatus == "Marked") {
            lastStatus = "Marked";
            //$(".Active").addClass("Marked");
            //          $(".Active").addClass(lastStatus);
        } else if (lastStatus == "VisitedAnswered") {
            lastStatus = "VisitedAnswered";
            //$(".Active").addClass("VisitedAnswered");
        }
        else
            lastStatus = "VisitedUnAnswer";

        //else if (lastStatus == "NotVisited") {
        //    lastStatus = "VisitedUnAnswer";
        //}
        $(".Active").addClass(lastStatus);
        $('.Active').removeClass("Active");
        $(".Active").attr("status", lastStatus);
        //g----------
        //if (lastStatus == "VisitedAnswered") {
        //    lastStatus = "VisitedAnswered";
        //    //            $(".Active").addClass("VisitedAnswered");
        //}

        //else if (lastStatus == "NotVisited") {
        //    lastStatus = "NotVisited";
        //    //        $(".Active").addClass(lastStatus);
        //}
       // $(".Active").addClass(lastStatus);
        //$('.Section' + selectedSectionsIndex).closest('.Active').removeClass('Active');   
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('NotVisited').addClass('Active');
        MovePrevious();
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').addClass('Active');
        //$('.Section' + selectedSectionsIndex).closest('.Active').removeClass('Active').addClass('VisitedUnanswer');
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('NotVisited').addClass('Active');

        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('NotVisited');
        //  $('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('VisitedAnswered');
    });
    //$("#btnConfirm").click(function () {
    //    $('.CurrentQuestion').removeClass('NotVisited').addClass('VisitedAnswered');
    //});
    $("#btnMark").click(function () { // anu 
        //$("input[value='" + MarkQuestionIndex + "']").addClass("QuestionMarked");
        RemoveAdditionalStyles();
        //$("this").attr("status", "Marked");
        $(".Active").attr("status", "Marked");
        $(".Active").addClass("Marked");
        $(".Active").removeClass("Active");
        $(".QuestionNumber:nth-of-type(" + selectedQuestionIndex + ")").addClass("Active");
        //anu
        //var totalQuestionsInThisSection = parseInt($(".SectionQuestionNumbers" + selectedSectionsIndex).children(".QuestionNumber").length);
        

        //var lastStatus = $(".Active").attr("status");
        //if (lastStatus == "Marked") {
        //    lastStatus = "Marked";
        //    //$(".Active").addClass("Marked");
        //}
        //else if (laststatus == "VisitedAnswered") {
        //    laststatus = "VisitedAnswered";
        //    //$(".active").addclass(laststatus);
        //}
        //else if(laststatus == "VisitedUnAnswer")
        //{
        //    laststatus = "VisitedUnAnswer";
        //}
        //$(".Active").addClass("Marked");
        //$(".Active").attr("status", lastStatus);
        //$('.Active').removeClass("Active");

        markQuestion();
        MoveNext();
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('Marked').addClass('Active');
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('VisitedUnanswer').addClass('Marked');
    });
    $("#btnNext").click(function () {  // anu
        //var lastStatus = $(".Active").attr("status");
        ////alert("lastStatus  " + lastStatus);
        //RemoveAdditionalStyles();

        //if (lastStatus != "VisitedAnswered") {
        //    lastStatus = "VisitedUnAnswer";
        //    $(".Active").addClass("VisitedUnAnswer");
        //}
        //else {
        //    $(".Active").addClass(lastStatus);
        //}
        //$(".Active").attr("status", lastStatus);
        ////$('.Section' + selectedSectionsIndex).closest('.Active').removeClass('Active').addClass('VisitedUnanswer');
        //$('.Section' + selectedSectionsIndex).closest('.Active').removeClass('Active');
        ////$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('NotVisited').addClass('Active');
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').addClass('Active');
        MoveNext();
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('VisitedUnanswer');
    });

    $("#btnConfirm").click(function () { // anu multi question active
        //$("#btnConfirm").one("click", function () {
        //ConfirmAnswer();
        //alert();
        //console.log("Test");
        $(this).prop('disabled', true);

        //setTimeout(function () {

        ConfirmAnswer();



        //setTimeout(function () {
        //$('#someid').addClass("done");
        //$('.Active').removeClass('Active').addClass('VisitedUnanswer');
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('Marked').addClass('VisitedAnswered');
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('NotVisited').addClass('VisitedAnswered');
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('VisitedUnanswer').addClass('VisitedAnswered');
        //}, 2000);
        //}, 1000);
        $(this).removeAttr('disabled');


        // $('.Section' + selectedSectionsIndex).attr("disabled", "disabled");
    });

    $('input[type="radio"]').click(function () {

    });

   // $(".SectionToggle").html("Hide Sections")

    $(".SectionContainers").show();
    sectionHide = 1;
    ShowSections();

    $(".QuestionNumberToggle").html("Hide Question Numbers")

    if (testmethod == "False") {
        $(".QuestionNumbersContainer").show();
        questionHide = 1;
        ShowQuestionNumber();
    }
    else {
        var containerlength = $(".lengthFinder div").length;
        var numberClass;
        $(".QuestionNumbersContainer").show();
        for (var j = 1; j <= containerlength; j++) {
            if (j == NewQuestionContainer) {
                numberClass = ".SectionQuestionNumbers" + j
                $(".QuestionNumbersContainer").children(numberClass).show();
            }
            else {
                numberClass = ".SectionQuestionNumbers" + j
                $(".QuestionNumbersContainer").children(numberClass).hide();
            }
        }
        // $(numberClass).show();
        questionHide = 1;
        ShowQuestionNumber();
    }

    $(".QuestionNumber").click(function () {
        HideCurrentQuestion();
        //selectedQuestionIndex = $(this).val();
        selectedQuestionIndex = $(this).attr("id");

        var lastStatus = $(".Active").attr("status");
        RemoveAdditionalStyles();

        if (lastStatus != "VisitedAnswered" && lastStatus != "Marked") {
            lastStatus = "VisitedUnAnswer";
            $(".Active").addClass("VisitedUnAnswer");
        }
        else {
            $(".Active").addClass(lastStatus);
        }
        $(".Active").attr("status", lastStatus);
        $(".Active").removeClass("Active");
       // $(this).addClass("Active");
        $(".QuestionNumber:nth-of-type(" + selectedQuestionIndex + ")").addClass("Active");

        EnableButtons();
        ShowCurrentQuestion();

        //$('.Section' + selectedSectionsIndex).closest('.Active').removeClass('Active').addClass('VisitedUnanswer');
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('NotVisited');
        //$('.Section' + selectedSectionsIndex).closest('.Active').removeClass('Active').addClass('VisitedUnanswer');
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('VisitedUnanswer').addClass('Active');
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('VisitedAnswered');
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('NotVisited');
    });

    $(".SectionName").click(function () {
        if ($(".isSectionJump").val() == "True") {
            var lastStatus = $(".Active").attr("status");
            //alert("lastStatus  " + lastStatus);
            RemoveAdditionalStyles();
            if (lastStatus == "Marked") {
                lastStatus = "Marked";
            }
            else if (lastStatus == "VisitedAnswered" || lastStatus == "VisitedAnswered") {
                lastStatus = "VisitedAnswered";
            }
            else
                lastStatus = "VisitedUnAnswer";
            //if (lastStatus != "VisitedAnswered") {
            //    lastStatus = "VisitedUnAnswer";
            //    $(".Active").addClass("VisitedUnAnswer");
            //}
            //else {
            //    $(".Active").addClass(lastStatus);
            //}
            $(".Active").addClass(lastStatus);
            $(".Active").attr("status", lastStatus);
            selectedQuestionIndex = 1;
            $(".SectionName").removeClass("SectionSelected");
            $(this).addClass("SectionSelected");
            $('.CurrentQuestion').removeClass('CurrentQuestion').addClass('NotVisited');
            $('.Active').removeClass('Active').addClass('NotVisited');
            HideCurrentQuestion();
            HideCurrentSections();
            //selectedQuestionIndex = $(this).val();
            //addClass("QuestionMarked");
            selectedSectionsIndex = $(this).attr("id");
            //$(".SectionContainers input").children().removeClass('QuestionMarked');
            // $(this).addClass('QuestionMarked');
            EnableButtons();
            ShowCurrentSections();
            if (questionHide == 1 && testmethod == "True") {
                var containerlength = $(".lengthFinder div").length;
                var numberClass;
                if ($(".QuestionNumberToggle").html() == "Hide Question Numbers") {
                    // $(this).html("Hide Question Numbers")
                    $(".QuestionNumbersContainer").show();
                    for (var j = 1; j <= containerlength; j++) {
                        //if (j == selectedSectionsIndex) {
                        //    numberClass = ".SectionQuestionNumbers" + j
                        //    $(".QuestionNumbersContainer").children(numberClass).show();
                        //}
                        //else {
                        numberClass = ".SectionQuestionNumbers" + j
                        $(".QuestionNumbersContainer").children(numberClass).hide();
                        //}
                    }
                    numberClass = ".SectionQuestionNumbers" + selectedSectionsIndex
                    $(".QuestionNumbersContainer").children(numberClass).show();

                    // $(numberClass).show();
                    questionHide = 1;
                    ShowQuestionNumber();
                }
            }
            ShowCurrentQuestion();
        }
        else {
            //alert("Section Jumping not allowed for this test");

            ShowAlert("You cannot switch between sections in this test!");
        }
    });


    $(".QuestionNumberToggle").click(function () {

        if (testmethod == "False") {
            if ($(this).html() == "Show Question Numbers") {
                $(this).html("Hide Question Numbers")
                $(".QuestionNumbersContainer").show();
                questionHide = 1;
                ShowQuestionNumber();
            }
            else if ($(this).html() == "Hide Question Numbers") {
                $(this).html("Show Question Numbers")
                $(".QuestionNumbersContainer").hide();
                questionHide = 0;
                HideQuestionNumber();
            }
        }
        else {
            var containerlength = $(".lengthFinder div").length;
            var numberClass;
            if ($(this).html() == "Show Question Numbers") {
                $(this).html("Hide Question Numbers")
                $(".QuestionNumbersContainer").show();
                //$(".QuestionNumbersContainer").child.each(function () {

                //});

                //if (ExistingQuestionContainer != 0) {
                //    numberClass = ".SectionQuestionNumbers" + ExistingQuestionContainer
                //    $(".QuestionNumbersContainer").children(numberClass).hide();
                //}
                for (var j = 1; j <= containerlength; j++) {
                    if (j == NewQuestionContainer) {
                        numberClass = ".SectionQuestionNumbers" + j
                        $(".QuestionNumbersContainer").children(numberClass).show();
                    }
                    else {
                        numberClass = ".SectionQuestionNumbers" + j
                        $(".QuestionNumbersContainer").children(numberClass).hide();
                    }
                }

                // $(numberClass).show();
                questionHide = 1;
                ShowQuestionNumber();
            }
            else if ($(this).html() == "Hide Question Numbers") {
                $(this).html("Show Question Numbers")
                if (ExistingQuestionContainer != 0) {
                    numberClass = ".SectionQuestionNumbers" + ExistingQuestionContainer
                    $(".QuestionNumbersContainer").children(numberClass).hide();
                }
                if (NewQuestionContainer != 0) {
                    numberClass = ".SectionQuestionNumbers" + NewQuestionContainer
                    $(".QuestionNumbersContainer").children(numberClass).hide();
                }
                $(".QuestionNumbersContainer").hide();
                questionHide = 0;
                HideQuestionNumber();
            }


        }
    });


    //cmd by raja
   $(".SectionToggle").click(function () {

        if ($(this).html() == "Show Sections") {
            $(this).html("Hide Sections")
            $(".SectionContainers").show();
            sectionHide = 1;
            ShowSections();
        }
        else if ($(this).html() == "Hide Sections") {
            $(this).html("Show Sections")
            $(".SectionContainers").hide();
            sectionHide = 0;
            HideSections();
        }
    });
    //end by raja



    //$(".MarkQuetion").click(function () {
    //    $("input[value='" + MarkQuestionIndex + "']").addClass("QuestionMarked");
    //});

    //$("#btnSubmit").click(function () {
    //    $("#dialog-confirm").dialog({
    //        resizable: false,
    //        height: 200,
    //        modal: true,
    //        buttons: {
    //            "Confirm Submit": function () {
    //                alert();
    //                SubmitTest();
    //            },
    //            Cancel: function () {
    //                $(this).dialog("close");
    //            }
    //        }
    //    });
    //});

    //Coment BY RAJA ..>
    //$("#btnSubmit").click(function () {

    //    SubmitTest();

    //    //$("#dialog-confirm").dialog({
    //    //    resizable: false,
    //    //    height: 200,
    //    //    modal: true,
    //    //    buttons: {
    //    //        "Confirm Submit": function () {
    //    //            alert();
    //    //        },
    //    //        Cancel: function () {
    //    //            $(this).dialog("close");
    //    //        }
    //    //    }
    //    //});
    //});
    //Coment BY RAJA ..>

    LoadSavedAnswers();
    LoadTimeCounter();
});

$(window).resize(function () {
    AlignQuestionNumberContainer();
    AlignSectionContainer();
});



function QuestionNumberButtonClick() {
    //if (testmethod == "False") {
    //    if ($(this).html() == "Show Question Numbers") {
    //        $(this).html("Hide Question Numbers")
    //        $(".QuestionNumbersContainer").show();
    //        questionHide = 1;
    //        ShowQuestionNumber();
    //    }
    //    else if ($(this).html() == "Hide Question Numbers") {
    //        $(this).html("Show Question Numbers")
    //        $(".QuestionNumbersContainer").hide();
    //        questionHide = 0;
    //        HideQuestionNumber();
    //    }
    //}
    //else {
    //    var numberClass;
    //    if ($(this).html() == "Show Question Numbers") {
    //        $(this).html("Hide Question Numbers")
    //        $(".QuestionNumbersContainer").show();
    //        if (ExistingQuestionContainer != 0) {
    //            numberClass = ".SectionQuestionNumbers" + ExistingQuestionContainer
    //            $(".QuestionNumbersContainer").children(numberClass).hide();
    //        }
    //        numberClass = ".SectionQuestionNumbers" + NewQuestionContainer
    //        $(".QuestionNumbersContainer").children(numberClass).show();
    //        // $(numberClass).show();
    //        questionHide = 1;
    //        ShowQuestionNumber();
    //    }
    //    else if ($(this).html() == "Hide Question Numbers") {
    //        $(this).html("Show Question Numbers")
    //        if (ExistingQuestionContainer != 0) {
    //            numberClass = ".SectionQuestionNumbers" + ExistingQuestionContainer
    //            $(".QuestionNumbersContainer").children(numberClass).hide();
    //        }
    //        if (NewQuestionContainer != 0) {
    //            numberClass = ".SectionQuestionNumbers" + NewQuestionContainer
    //            $(".QuestionNumbersContainer").children(numberClass).hide();
    //        }
    //        $(".QuestionNumbersContainer").hide();
    //        questionHide = 0;
    //        HideQuestionNumber();
    //    }


    //}
}
function AlignQuestionNumberContainer() {
    var questionListHeight;
    var top;
    if (sectionHide == 0 && questionHide == 1) {
        QuestionHight = 468;
        QuestionTop = 98;
    }
    else if (questionHide == 1 && sectionHide == 1) {
        //sectionhight = parseint(($(".questionlist").height() / 2) + $(".sectioncontainers").height() + 30);
        //sectiontop = parseint($(".sectioncontainers").width() + 22);

        //SectionHight = parseInt(($(".QuestionList").height() / 4) + 20);
        SectionHight = 150;
        SectionTop = 98;
        QuestionHight = 318;//$(".SectionContainers").height() + 30;

        //SectionHight = parseInt(($(".QuestionList").height() / 4) + 20);
        //SectionTop = 98;
        //QuestionHight =$(".SectionContainers").height() + 30;
        QuestionTop = 250;//parseInt($(".SectionContainers").width() + 22);
        $(".SectionContainers").css("top", SectionTop);
        $(".SectionContainers").css("height", SectionHight);
        if ($('.SectionContainers').css('display') != 'none') {
            var windowWidth = parseInt($(window).width());
            var questionNumberContainerWidth = parseInt($(".SectionContainers").width());
            var newQuestionNumberContainerWidth = windowWidth - questionNumberContainerWidth - 50;

            $(".QuestionList").css("width", newQuestionNumberContainerWidth);
        }
    }
    //else {
    //    QuestionHight = parseInt(($(".QuestionList").height() / 2) + $(".SectionContainers").height() + 30);
    //    QuestionTop = parseInt($(".SectionContainers").width() + 22);
    //}    
    $(".QuestionNumbersContainer").css("height", QuestionHight);
    $(".QuestionNumbersContainer").css("top", QuestionTop);
    if ($('.QuestionNumbersContainer').css('display') != 'none') {
        var windowWidth = parseInt($(window).width());
        var questionNumberContainerWidth = parseInt($(".QuestionNumbersContainer").width());
        var newQuestionNumberContainerWidth = windowWidth - questionNumberContainerWidth - 50;
        $(".QuestionList").css("width", newQuestionNumberContainerWidth);
    }

}

function markQuestion() {
    //$("input[value='" + MarkQuestionIndex + "']").addClass("QuestionMarked");
    //$(".SectionQuestionNumbers1 input").children("#1").addClass("QuestionMarked");
    $(".SectionQuestionNumbers1 input").children("#1").css("background-color", "red");
}

function AlignSectionContainer() {
    var questionListHeight;
    if (questionHide == 0 && sectionHide == 1) {
        SectionHight = 468;
        SectionTop = 98;
    }
    else if (questionHide == 1 && sectionHide == 1) {
        //sectionhight = parseint(($(".questionlist").height() / 2) + $(".sectioncontainers").height() + 30);
        //sectiontop = parseint($(".sectioncontainers").width() + 22);

        //SectionHight = parseInt(($(".QuestionList").height() / 4) + 20);
        SectionHight = 150;
        SectionTop = 98;
        QuestionHight = 318;//$(".SectionContainers").height() + 30;
        QuestionTop = 250;//parseInt($(".SectionContainers").width() + 22);
        $(".QuestionNumbersContainer").css("height", QuestionHight);
        $(".QuestionNumbersContainer").css("top", QuestionTop);
        if ($('.QuestionNumbersContainer').css('display') != 'none') {
            var windowWidth = parseInt($(window).width());
            var questionNumberContainerWidth = parseInt($(".QuestionNumbersContainer").width());
            var newQuestionNumberContainerWidth = windowWidth - questionNumberContainerWidth - 50;
            $(".QuestionList").css("width", newQuestionNumberContainerWidth);
        }
    }
    //else {
    //    SectionHight = parseInt(($(".QuestionList").height() / 4) + 20);
    //}

    $(".SectionContainers").css("top", SectionTop);
    $(".SectionContainers").css("height", SectionHight);
    if ($('.SectionContainers').css('display') != 'none') {
        var windowWidth = parseInt($(window).width());
        var questionNumberContainerWidth = parseInt($(".SectionContainers").width());
        var newQuestionNumberContainerWidth = windowWidth - questionNumberContainerWidth - 50;

        $(".QuestionList").css("width", newQuestionNumberContainerWidth);
    }

}


function ShowQuestionNumber() {

    //var numberContainerWidth = parseInt($(".QuestionNumbersContainer").width());
    //var questionListWidth = parseInt($(".QuestionList").width());

    //var newQuestionListWidth = (questionListWidth - numberContainerWidth) - 12;

    //$(".QuestionList").css("width", newQuestionListWidth);

    AlignButtons();
    AlignQuestionNumberContainer();

    //var bottomContainerWidth = $(".bottomContainer").css("margin-top").replace("px", "");
    //bottomContainerWidth = parseInt(bottomContainerWidth);
    //var questionListWidth = parseInt($(".QuestionList").height());

    //if (bottomContainerWidth < questionListWidth) {
    //    var newMargin = questionListWidth + 10;
    //    $(".bottomContainer").css("margin-top", newMargin + "px");
    //}   

    AlignBottomContainer();
    $(".ExamControllers").css("position", "fixed");
    $(".ExamControllers").css("z-index", 1);


}

function ShowSections() {
    AlignButtons();
    AlignSectionContainer();
    AlignBottomContainer();
    $(".ExamControllers").css("position", "fixed");
    $(".ExamControllers").css("z-index", 1);


}

function AlignBottomContainer() {
    //var bottomContainerWidth = $(".bottomContainer").css("margin-top").replace("px", "");
    //bottomContainerWidth = parseInt(bottomContainerWidth);
    //var questionListWidth = parseInt($(".QuestionList").height());

    //if (bottomContainerWidth < questionListWidth) {
    //    var newMargin = questionListWidth + 10;
    //    $(".bottomContainer").css("margin-top", newMargin + "px");
    //}
}

function HideQuestionNumber() {
    $(".QuestionList").css("width", "100%");

    var questionlistheight;

    sectionhight = 468;
    sectiontop = 98;

    $(".sectioncontainers").css("height", sectionhight);
    if ($('.sectioncontainers').css('display') != 'none') {
        var windowwidth = parseInt($(window).width());
        var questionnumbercontainerwidth = parseInt($(".sectioncontainers").width());
        var newquestionnumbercontainerwidth = windowwidth - questionnumbercontainerwidth - 50;

        $(".questionlist").css("width", newquestionnumbercontainerwidth);
    }
    //$(".bottomContainer").css("margin-top", "");
    //$(".ExamControllers").css("position", "relative");
    //$(".ExamControllers").css("top", 0);

}

function HideSections() {
    $(".QuestionList").css("width", "100%");

    var questionListHeight;
    var top;
    if (sectionHide == 0) {
        QuestionHight = 468;
        QuestionTop = 98;
    }
    //else {
    //    QuestionHight = parseInt(($(".QuestionList").height() / 2) + $(".SectionContainers").height() + 30);
    //    QuestionTop = parseInt($(".SectionContainers").width() + 22);
    //}
    $(".QuestionNumbersContainer").css("height", QuestionHight);
    $(".QuestionNumbersContainer").css("top", QuestionTop);
    if ($('.QuestionNumbersContainer').css('display') != 'none') {
        var windowWidth = parseInt($(window).width());
        var questionNumberContainerWidth = parseInt($(".QuestionNumbersContainer").width());
        var newQuestionNumberContainerWidth = windowWidth - questionNumberContainerWidth - 50;
        $(".QuestionList").css("width", newQuestionNumberContainerWidth);
    }
    //$(".bottomContainer").css("margin-top", "");
    //$(".ExamControllers").css("position", "relative");
    //$(".ExamControllers").css("top", 0);

}

function AlignButtons() {
    //var bottomContainerWidth = $(".bottomContainer").css("margin-top").replace("px", "");

    //bottomContainerWidth = parseInt(bottomContainerWidth);
    //var questionListWidth = parseInt($(".QuestionList").height());

    //if (bottomContainerWidth < questionListWidth) {
    //    var newMargin = questionListWidth + 10;
    //    $(".bottomContainer").css("margin-top", newMargin + "px");
    //}
    //else {
    //    $(".bottomContainer").css("margin-top", "");
    //}
}

function LoadTimeCounter() {
    if (TimeLimit != 0)
        $('#defaultCountdown').countdown(
    {
        until: TimeLimit,
        onTick: TimeHandler
    });
    else {
        if ($('#defaultCountdown').length > 0)
            $('#defaultCountdown').css({ 'display': 'none' });
    }
}

function SubmitTest() {
    OpenModal("loadingModal");
    //$("#SubmitConfirm").modal('hide');
    //$("#loadingModal").modal(
    //    {
    //        show: true,
    //        backdrop: 'static',
    //    });
    var PostData = '';
    var submitmethod = $(".isSectionJump").val();
    $.ajax({
        url: '/QuestionPaper/Submit',
        data: {
            submitted: true,
            submitmethod: submitmethod
        },
        type: 'POST',
        success: function (data) {
            $("#loadingModal").modal('hide');
            if (data.result == "1") {                //Test Fully submitted successful
                OpenModal('testSubmissionSuccessAlertModal');
                testSubmitted = true;
                //alert("Test submitted successfully");
                //window.location = "/DashBoard/Index";
            }
            else if (data.result == "2") { // Section submit successful
                window.location = window.location;
                sectionSubmitted = true;
            }
            else if (data.result == "-2") { //Not authenticated

                //alert("You've been logged out");
                //window.location = "/Account/LogOff";

                OpenModal('loggedOutAlertModal');
            }
            else if (data.result == "-1") { //failed
                //alert("Unable to submit test. Please try again or contact your supervisor if problem persists");
                OpenModal("submitTestErrorAlertModal");
            }
        }
    });
}

function LoadSavedAnswers() {

    $.each(SavedAnswers, function (i, savedAnswer) {
        //$("button[QuestionID='" + savedAnswer.QuestionID + "']").removeClass("btn-info").addClass("btn-success");
        $("input[QuestionID='" + savedAnswer.QuestionID + "']").addClass("VisitedAnswered");
        $(".VisitedAnswered").attr("status", "VisitedAnswered")
        $("#rdo" + savedAnswer.AnswerID).attr('checked', 'checked');
    });

}



function MoveNext() {
    //if (selectedQuestionIndex != totalQuestions) {
    var lastStatus = $(".Active").attr("status");

    var totalSections = $(".SectionName").length;
    var totalQuestionsInCurrentSection = $(".SectionQuestionNumbers" + selectedSectionsIndex).children(".QuestionNumber").length;
    if (selectedQuestionIndex == totalQuestionsInCurrentSection) {
        //RemoveAdditionalStyles();
        //alert(lastStatus);
        //if (lastStatus == "Marked") {
        //    lastStatus = "Marked";
        //}
        //$(".Active").addClass(lastStatus);
        //$(".Active").attr("status", lastStatus);

        var nextSection = parseInt(selectedSectionsIndex) + 1;
        //$(".SectionContainers").children("#" + (nextSection)).click();
    }
    else {
        //if (lastStatus != "VisitedAnswered" && lastStatus != "Marked") {
        //    lastStatus = "VisitedUnAnswer";
        //    $(".Active").addClass("VisitedUnAnswer");
        //}
        //else {
        //    $(".Active").addClass(lastStatus);
        //}
        //$(".Active").attr("status", lastStatus);
        //$(".Active").removeClass("Active");
        //// $(this).addClass("Active");
        //$(".QuestionNumber:nth-of-type(" + selectedQuestionIndex + ")").addClass("Active");


        RemoveAdditionalStyles();
        $(".Active").removeClass(lastStatus);

        if (lastStatus == "Marked") {
            lastStatus = "Marked";
        }
        else if (lastStatus == "VisitedAnswered" || lastStatus == "VisitedAnswered") {
            lastStatus = "VisitedAnswered";
        }
        else {
            lastStatus = "VisitedUnAnswer";
        }

        $(".Active").addClass(lastStatus);
        $(".Active").attr("status", lastStatus);
        $(".Active").removeClass("Active");
        //$('.Section' + selectedSectionsIndex).closest('.Active').removeClass('Active').addClass('VisitedUnanswer');
        $('.Section' + selectedSectionsIndex).closest('.Active').removeClass('Active');
        //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('NotVisited').addClass('Active');
        $('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').addClass('Active');


        GetTimeSpendForQuestion();
        HideCurrentQuestion();
        selectedQuestionIndex++;
        EnableButtons();
        ShowCurrentQuestion();
        AlignQuestionNumberContainer();
    }
    $(".QuestionNumber:nth-of-type(" + selectedQuestionIndex + ")").addClass("Active");
}

function MovePrevious() {

    var totalQuestionsInCurrentSection = $(".SectionQuestionNumbers" + selectedSectionsIndex).children(".QuestionNumber").length;
    if (selectedSectionsIndex > 1 && selectedQuestionIndex == 1) {                                                                   //Move to previous section
        //$(".SectionContainers").children("#" + (selectedSectionsIndex - 1)).click();
    }
    else {
        GetTimeSpendForQuestion();
        HideCurrentQuestion();
        selectedQuestionIndex--;
        EnableButtons();
        ShowCurrentQuestion();
        AlignQuestionNumberContainer();
    }
}
function EnableButtons() {
    var classname = ".section" + selectedSectionsIndex;
    if (testmethod == "True") {
        totalQuestions = $(classname).val();

    }
    else {
        totalQuestions = $(".QuestionContainer").length;
    }

    var currentSelectedQuestion = $(classname).children(".Active").attr("id");
    if (currentSelectedQuestion == "" || currentSelectedQuestion == undefined || currentSelectedQuestion == NaN) {
        currentSelectedQuestion = 1;
    }
   // selectedQuestionIndex = 1;
    //selectedQuestionIndex = currentSelectedQuestion;
    $('#QuestionNumberInfo').html(selectedQuestionIndex);
    $('#TotalSectionQuestions').html(totalQuestions);
    if (testmethod == "True") {
        var templength = $('.SectionContainer').length;
        //if (selectedQuestionIndex == 1 && selectedSectionsIndex == 1) {
        var totalSections = $(".SectionName").length;

        if (selectedQuestionIndex == 1 && selectedSectionsIndex == 1) {
            $("#btnPrev").attr("disabled", "disabled");
            //$('#QuestionNumberInfo').html(selectedQuestionIndex);
            //$('#TotalSectionQuestions').html(totalQuestions);

        }
        else {
            $("#btnPrev").removeAttr("disabled");
            //$('#TotalSectionQuestions').html(totalQuestions);
            //$('#QuestionNumberInfo').html(selectedQuestionIndex);

        }
        //if (selectedQuestionIndex == totalQuestions && selectedSectionsIndex == templength)
        if (selectedQuestionIndex == totalQuestions && selectedSectionsIndex == totalSections) {
            $("#btnNext").attr("disabled", "disabled");
            //$('#QuestionNumberInfo').html(selectedQuestionIndex);
            //$('#TotalSectionQuestions').html(totalQuestions);
        }
        else {
            //$('#QuestionNumberInfo').html(selectedQuestionIndex);
            //$('#TotalSectionQuestions').html(totalQuestions);
            //document.getElementById('QuestionNumberInfo').InnerHTML = '7';
            $("#btnNext").removeAttr("disabled");
        }
    }
    else {
        if (selectedQuestionIndex == 1 ) {
            $("#btnPrev").attr("disabled", "disabled");
            //$('#QuestionNumberInfo').html(selectedQuestionIndex);
            //$('#TotalSectionQuestions').html(totalQuestions);

        }
        else {
            $("#btnPrev").removeAttr("disabled");
            //$('#TotalSectionQuestions').html(totalQuestions);
            //$('#QuestionNumberInfo').html(selectedQuestionIndex);

        }
        if (selectedQuestionIndex == totalQuestions ) {
            $("#btnNext").attr("disabled", "disabled");
            //$('#QuestionNumberInfo').html(selectedQuestionIndex);
            //$('#TotalSectionQuestions').html(totalQuestions);
        }
        else {
            //$('#QuestionNumberInfo').html(selectedQuestionIndex);
            //$('#TotalSectionQuestions').html(totalQuestions);
            //document.getElementById('QuestionNumberInfo').InnerHTML = '7';
            $("#btnNext").removeAttr("disabled");
        }
    }

    //document.getElementById('QuestionNumberInfo').InnerHTML = '7';
    // $(".QuestionNumberInfo").val(selectedQuestionIndex++);
}

function HideCurrentQuestion() {
    $('.Section' + selectedSectionsIndex).closest("input[value='" + selectedQuestionIndex + "']").removeClass("CurrentQuestion");
    $(".QuestionContainer").hide();
    // $(".QuestionContainer:eq(" + selectedQuestionIndex + ")").hide();
    $(".QuestionContainer:nth-of-type(" + selectedQuestionIndex + ")").hide();
}

function ShowCurrentQuestion() {
    //alert($(".SectionQuestionNumbers" + selectedSectionsIndex).children("#"+selectedQuestionIndex).attr("id"));
    //alert("selectedSectionsIndex   "+selectedSectionsIndex+"    selectedQuestionIndex   ="+selectedQuestionIndex);
    $(".SectionQuestionNumbers" + selectedSectionsIndex).children("#" + selectedQuestionIndex).addClass("Active").addClass(".CurrentQuestion");
    //alert(selectedQuestionIndex);
    //$('.Section' + selectedSectionsIndex).closest("input[value='" + selectedQuestionIndex + "']").addClass("CurrentQuestion");
    //$('.Section' + selectedSectionsIndex).closest('.CurrentQuestion').removeClass('NotVisited').addClass('Active');
    // $(".QuestionContainer:eq(" + selectedQuestionIndex + ")").show();
    $(".QuestionContainer:nth-of-type(" + selectedQuestionIndex + ")").show();
    $(".QuestionNumber:nth-of-type(" + selectedQuestionIndex + ")").addClass("Active")

    //$(".QuestionNumberInfo").val(selectedQuestionIndex + 1);

    //
    //document.getElementById('QuestionNumberInfo').InnerHTML = '7';
    AlignButtons();

}


function HideCurrentSections() {
    ExistingQuestionContainer = selectedSectionsIndex;
    $(".SectionQuestionNumbers" + selectedSectionsIndex).removeClass(".CurrentSection");
    $("input[value='" + selectedQuestionIndex + "']").removeClass("CurrentSection");

    // $(".QuestionContainer:eq(" + selectedQuestionIndex + ")").hide();
    $(".SectionContainer:nth-of-type(" + selectedSectionsIndex + ")").hide();
    $(".SectionQuestionNumbers" + selectedSectionsIndex).hide();

}

function ShowCurrentSections() {
    //selectedQuestionIndex = 1;
    NewQuestionContainer = selectedSectionsIndex;
    $(".SectionQuestionNumbers" + selectedSectionsIndex).removeClass(".CurrentSection");
    $("input[value='" + selectedQuestionIndex + "']").addClass("CurrentSection");
    $('.CurrentQuestion').removeClass('CurrentQuestion');
    $('.Active').removeClass('Active');
    // $(".QuestionContainer:eq(" + selectedQuestionIndex + ")").show();
    $(".SectionContainer:nth-of-type(" + selectedSectionsIndex + ")").show();
    $(".SectionQuestionNumbers" + selectedSectionsIndex ).show();
    AlignButtons();

}

var lastAnsweredID = "";
function ConfirmAnswer() {
    GetTimeSpendForQuestion();
    // alert("Please Wait");
    //anu changes

    // var questionId = $(".QuestionContainer:eq(" + selectedQuestionIndex + ")").attr("QuestionID");
    var QuestionIdentifier;
    var questionId;
    QuestionIdentifier = "." + selectedSectionsIndex.toString() + selectedQuestionIndex.toString();
    if (testmethod == "True") {
        questionId = $(QuestionIdentifier).attr("QuestionID");
    }
    else {
        questionId = $(".QuestionContainer:nth-of-type(" + selectedQuestionIndex + ")").attr("QuestionID");
    }

    var questionType = $("#hdn" + questionId).val();

    var answerId = "";
    if (questionType != undefined) {
        if (questionType.toLowerCase() == "single select") {            //Question type is single select
            answerId = $("input[name='rdo" + questionId + "']:checked").val();
        }
        else if (questionType.toLowerCase() == "multi select") {        //Question type is multiselect
            $("input[name='chk" + questionId + "']:checked").each(function () {
                if (answerId == "") {
                    answerId = $(this).val();
                }
                else {
                    answerId = answerId + "," + $(this).val();
                }
            })
        }
    }

    var answerContent = "";
    answerContent = $("#txt" + questionId).val();
    if (lastAnsweredID != answerId) {                                           //Disable double click
        //console.log("answerId   " + answerId + "   answerContent    " + answerContent);
        if ((answerId == undefined || answerId == "") && (answerContent == undefined || answerContent == "")) {
            //if (answerId=="" && answerContent=="") {
            //alert("Please select an answer");
            ShowAlert("Please select an answer");
            return;
        }
        lastAnsweredID = answerId;

        var timeString = GetQuestionTimingsString();
        var timeStr = timeString.split(',');
        var timeValue = timeStr[selectedQuestionIndex - 1];
        $.ajax({
            url: '/QuestionPaper/Confirm',
            data: {
                questionId: questionId,
                questionIndex: selectedQuestionIndex,
                answerIdString: answerId,
                questionTiming: timeString,
               // questionTiming: timeValue,
                questionType: questionType,
                answerContent: answerContent,
            },
            type: 'POST',
            success: function (data) {
                var tempresult = data;
                if (data.result == "1") { // successful
                    //$("button[id='" + data.questionIndex + "']").addClass("btn-success");
                    $("input[QuestionId='" + data.questionId + "']").addClass("VisitedAnswered");
                    var lastStatus = $(".Active").attr("status");
                    if (lastStatus != "VisitedAnswered")
                        $(".Active").attr("status", "VisitedAnswered")
                    MarkQuestionIndex = data.questionIndex;
                    if (isSectionJump == "True") {
                        if (selectedQuestionIndex != totalQuestions && totalQuestions != 0) {
                            // $('.btnConfirm').removeAttr("disabled");
                            MoveNext();
                        }
                        else {
                            if (selectedSectionsIndex < totalSections) {
                                //HideCurrentQuestion();
                                //HideCurrentSections();
                                //selectedSectionsIndex++;
                                //ShowCurrentSections();
                                //ShowCurrentQuestion();
                                MoveNext();
                                //EnableButtons();
                            }
                            else {
                              //  alert('This is a last section, you can either review or submit the test.')
                                ShowAlert("This is a last section, you can either review or submit the test.");
                            }
                            //$(".btnSubmitTest").click();
                        }
                    }
                    else {

                        if (selectedQuestionIndex != totalQuestions && totalQuestions != 0) {
                            //$(".Active").addClass("ConfirmAnswer");
                            MoveNext();

                        }
                        else {
                            //HideCurrentSections();
                            //selectedSectionsIndex++;
                            //ShowCurrentSections();

                            //$(".btnSubmitTest").click();

                            //alert('Section is end, please submit the section');
                            ShowAlert("Section ended, you can submit now!");
                        }
                    }

                }

                else if (data.result == "-2") { //Not authenticated
                    //alert("You've been logged out");
                    ShowAlert("You've been logged out");
                    window.location = "/Home/Login";
                }

                else if (data.result == "-1") { //failed
                  //  alert("Unable to save data. Please try again or contact your supervisor if problem persists");
                   ShowAlert("Unable to save data. Please try again or contact your supervisor if the problem persists");

                }
            }
        });
    }
}

function GetTimeSpendForQuestion() {
    endDate = new Date();
    var timeSpent = Math.abs((startDate - endDate) / 1000);
    //questionTimings[selectedQuestionIndex - 1] = timeSpent;
    UpdateQuestionTiming(timeSpent);
    startDate = new Date();
    endDate = "";
    timeSpent = "";
}

function UpdateQuestionTiming(timeSpent) {
    //if (previousOne) {
    //    if (questionTimings[selectedQuestionIndex] == undefined) {
    //        questionTimings[selectedQuestionIndex] = timeSpent;
    //    }
    //    else {
    //        questionTimings[selectedQuestionIndex] = questionTimings[selectedQuestionIndex] + timeSpent;
    //    }

    //}
    //else {
    if (questionTimings[selectedQuestionIndex - 1] == undefined) {
        questionTimings[selectedQuestionIndex - 1] = timeSpent;
    }
    else {
        questionTimings[selectedQuestionIndex - 1] = questionTimings[selectedQuestionIndex - 1] + timeSpent;
    }
    //}



}

function GetQuestionTimingsString() {
    var timingString = "";
    for (var i = 0; i < questionTimings.length; i++) {
        // if (questionTimings[i] != undefined) {
        if (timingString == "") {
            timingString = questionTimings[i];
        }
        else {
            timingString += "," + questionTimings[i];

        }
        //}

    }
    return timingString + ",";
}
//function LoadTimeCounter() {
//    if (TimeLimit != 0)
//        $('#defaultCountdown').countdown(
//    {
//        until: TimeLimit,
//        onTick: TimeHandler
//    });
//    else {
//        if ($('#defaultCountdown').length > 0)
//            $('#defaultCountdown').css({ 'display': 'none' });
//    }
//}


var KeepHighlighting = false;
function TimeHandler(periods) {
    var SecondsRemaining = $.countdown.periodsToSeconds(periods);

    if (SecondsRemaining % 60 == 0) { //Take time snapshot
        //if (SecondsRemaining == 0) {
        //    SecondsRemaining = -1;
        //}
        //var PostData = '';
        //$.ajax({
        //    url: '/Assessment/TimeSnapShot',
        //    data: '',
        //    type: 'POST',
        //    success: function (data) {
        //        alert(data);
        //    }
        //});
    }

    if (KeepHighlighting) {
        if (SecondsRemaining % 2 == 0)
            $(this).css({ 'color': 'Black' });
        else
            $(this).css({ 'color': 'Red' });
    }
    if (SecondsRemaining == 0) {
        KeepHighlighting = false;
        openLoader('Time up. Please wait for auto submission');
        ShowNotify();
        setTimeout('SubmitTest();', 3000);
    }
}

function openLoader(message) {
    $("#spnLoaderMessage").html(message);
    //$("#dlgLoader").dialog({
    //    modal: true,
    //    width: 400,
    //    draggable: false,
    //    beforeclose: function (event, ui) { return false; }
    //});
}

function ShowNotify() {
}






//--------------------------------------------------------------------

//Disabling RightClick
if (document.all) {
    document.onkeydown = function () {
        var key_f5 = 116; // 116 = F5    
        var escape = 27;
        var key_f11 = 122;
        var key_backspace = 8;
        if (key_f5 == event.keyCode) {
            event.keyCode = 0;
            event.returnValue = false;
            ShowAlert("Refreshing has been disabled.");
        }
        //else if (key_f11 == event.keyCode) {
        //    event.keyCode = 0;
        //    event.returnValue = false;
        //    ShowAlert("Full screen can not disabled");

        //}
        //else if (escape == event.keyCode) {
        //    event.keyCode = 0;
        //    event.returnValue = false;
        //    ShowAlert("Full screen can not disabled");

        //}
        //if (key_backspace == event.keyCode) {
        //    event.keyCode = 0;
        //    event.returnValue = false;
        //    alert("Backspace has been disabled.");
        //}

       // launchFullScreen(document.documentElement);
        return false;
    }
}
else {
    document.onkeydown = function (e) {
        var key_f5 = 116; // 116 = F5 
        var escape = 27;
        var key_f11 = 122;
        var key_backspace = 8;
        //if (document.referrer == previousPageURL)
        //{
        //    S("Its a back button click...");
        //    //Specific code here...
        //}
        launchFullScreen(document.documentElement);
        if (!e) e = window.event;
        key = e.keycode ? e.keycode : e.which;
        if (key == key_f5) {
            ShowAlert("Refreshing has been disabled.");
            e.returnValue = false;
            return false;
        }
        //else if (key_f11 == event.keyCode) {
        //    event.keyCode = 0;
        //    event.returnValue = false;
        //    ShowAlert("Full screen can not disabled");

        //}
        //else if (escape == event.keyCode) {
        //    event.keyCode = 0;
        //    event.returnValue = false;
        //    ShowAlert("Full screen can not disabled");

        //}

      
        var c = e.keyCode
        var ctrlDown = e.ctrlKey || e.metaKey // Mac support
        // Check for Alt+Gr (http://en.wikipedia.org/wiki/AltGr_key)
        if (ctrlDown && e.altKey) return true
            // Check for ctrl+c, v and x
        else if (ctrlDown && c == 67) return false // c
        else if (ctrlDown && c == 86) return false // v
        else if (ctrlDown && c == 88) return false // x
        // Otherwise allow
       // return true
        //if (key == key_backspace) {

        //    alert("Backspace has been disabled.");
        //    e.returnValue = false;
        //    return false;
        //}

    }
}

document.onclick = function () {
  //  launchFullScreen(document.documentElement);
}
function DisableBackButton() {
    window.history.forward()
}
//DisableBackButton();
window.onload = DisableBackButton;
window.onpageshow = function (evt) {
    if (evt.persisted) DisableBackButton()
}
window.onunload = function () {
    void (0)
}


//window.onhashchange = function () {
//    //blah blah blah
//    alert("Backspace has been disabled.");
//    return false;

//}

window.onbeforeunload = function () {
    var submitmethod = $(".isSectionJump").val();
    if (!testSubmitted) {
        if (!sectionSubmitted) {
            $.ajax({
                url: '/QuestionPaper/SaveInterruption',
                type: 'POST',
                data: {
                    submitmethod: submitmethod
                },
                success: function (data) {
                   
                },
            });
        }
    }
    return "Are you sure ! You want leave this page. ";

    //return false;
};

//Disabling Rightclick
var message = "Function Disabled!";

function clickIE4() {
    if (event.button == 2) {
        ShowAlert(message);
        return false;
    }
}

////function clickNS4(e) {
////    if (document.layers || document.getElementById && !document.all) {
////        if (e.which == 2 || e.which == 3) {
////            alert(message);
////            return false;
////        }
////    }
//}
if (document.layers) {
    document.captureEvents(Event.MOUSEDOWN);
    document.onmousedown = clickNS4;
}
else if (document.all && !document.getElementById) {
    document.onmousedown = clickIE4;
}

document.oncontextmenu = new Function("ShowAlert(message);return false")

//--------------------------------------------------------


function launchFullScreen(element) {
    if (element.requestFullScreen) {
        element.requestFullScreen();
    } else if (element.mozRequestFullScreen) {
        element.mozRequestFullScreen();
    } else if (element.webkitRequestFullScreen) {
        element.webkitRequestFullScreen();
    }
}


function OpenModal(modalName) {
    CloseModal();
    $("#" + modalName).modal({
        show: true,
        backdrop: 'static',
    });
}

function CloseModal() {
    $(".modal").modal('hide');
}

function ShowAlert(alertContent) {
    $(".modal").modal('hide');
    $(".alertContent").html(alertContent);
    $("#alertModal").modal({
        show: true,
        backdrop: 'static',
    });
}