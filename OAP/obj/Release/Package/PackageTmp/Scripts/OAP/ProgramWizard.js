﻿
function PostDetails(currentIndex) {
    collegeID = $(".ddlInstitute").val();
    var programName = $(".progName").val();
    var programDescription = $(".progDesc").val();
    var programID = $(".programID").val();
    switch (currentIndex) {
        case 0:
            if (programName != "" && collegeID != "" && selectedCourseTemplateID != "") {
                var result = true;
                $.ajax({
                    type: 'POST',
                    url: '/ProgramWizard/SaveProgramInstitute',
                    async: true,
                    data: {
                        selectedCourse: selectedCourseTemplateID,
                        Institute: collegeID,
                        ProgramName: programName,
                        ProgramDescription: programDescription
                    },
                    success: function (response) {
                        if (response.split(',')[0] == 'true') {

                            GetProgramSubjects(response.split(',')[1]);
                            result = true;
                        }
                        else {
                            result = false;
                        }
                    }
                });
                return result;
            }
            else {
                if (programName == "") {
                    OpenAlertModal('Please provide name for the Program to be created');
                }
                else if (collegeID == "") {
                    OpenAlertModal('Please select Institute for the Program to be created');
                }
                else if (selectedCourseTemplateID == "") {
                    OpenAlertModal('Please select Course Template for the Program to be created');
                }
                return false;
            }
            break;
        case 1:
            GetProgramQuestionPapers(programID);
            GetQuestionPapersForProgram();
            return true;
            break;
        case 2:
            GetProgramBatches(programID);
            return true;
            break;
        case 3:
            GetBatchStudentDetails(programID);
            return true;
            break;
        case 4:
            FinishWizard();
            return true;
            break;

    }


}

function OpenCourseTemplateModal() {
    if (selectedCourseTemplateID != "") {
        $("#courseDetail").modal({
            show: true,
        });
        CourseChanged();
    }
    else {
        OpenAlertModal('Please select Course Template');
    }

}

function CourseChanged() {
    $(".courseTemplateName").html('');
    $(".courseTemplateDescription").html('');
    $(".tblSubjects").html('');
    $(".tblTestTemplate").html('');
    selectedCourseTemplateID = $(".ddlCourse").val();
    if (selectedCourseTemplateID != "") {
        $.ajax({
            type: 'POST',
            url: '/ProgramWizard/CourseDetails',
            async: true,
            data: {
                selectedCourseTemplateID: selectedCourseTemplateID,
            },
            success: CourseTemplateDetails
        });
    }

}

function CourseTemplateDetails(response) {
    if (response != null && response != "") {
        $(".tblSubjects").html('');
        $(".tblTestTemplate").html('');
        var data = JSON.parse(response);
        $(".courseTemplateName").html(data.Name);
        // $(".courseTemplateDescription").html(data.Description);
        var bodyString = "";
        $.each(data.SubjectViewModelList, function (i, item) {
            bodyString += "<tr><td>" + item.Name + "</td><td>" + item.Description + "</td></tr>";
        });
        bodyString = bodyString.replace('null', '');
        $(".tblSubjects").html(bodyString);
        bodyString = "";
        $.each(data.TestTemplateList, function (i, item) {
            bodyString += "<tr><td>" + item.Name + "</td><td>" + item.Description + "</td></tr>";
        });
        bodyString = bodyString.replace('null', '');
        $(".tblTestTemplate").html(bodyString);

        //$(".courseTemplateName").html(response.split(',')[0]);
        //$(".courseTemplateDescription").html(response.split(',')[1]);
        //var subject = response.split(',')[2];
        //var testTemplate = response.split(',')[3];
        //var testTemplates = testTemplate.split('`');
        //var subjects = subject.split('`');
        //$(".tblSubjects").html('');
        //$(".tblTestTemplate").html('');
        //for (var i = 0; i < subjects.length; i++) {
        //    var tableString = CreateTableString(subjects[i].split('~')[0], subjects[i].split('~')[1]);
        //    $(".tblSubjects").append(tableString);
        //}
        //for (var i = 0; i < testTemplates.length; i++) {
        //    var tableString = CreateTableString(testTemplates[i].split('~')[0], testTemplates[i].split('~')[1]);
        //    $(".tblTestTemplate").append(tableString);

        //}
    }

}


function GetCollegeBatches(collegeID) {
    //$('#dynamic-table').dataTable({
    //    "processing": true,
    //    "serverSide": true,
    //    "ajax": '/ProgramWizard/GetCollegeBatches?collegeID=' + collegeID,
    //    "async": true,
    //    "pagingType": "full_numbers",
    //    "columns": [
    //    { "data": "Name" },
    //    ],
    //    "columnDefs": [{
    //        "targets": [0],
    //        //"data": "download_link",
    //        "render": function (data, type, full) {
    //            return '<a href="Edit/' + full.ID + '">' + data + '</a>';
    //        }
    //    }]
    //});
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/GetCollegeBatches',
        async: true,
        data: {
            collegeID: collegeID,
        },
        success: CollegeBatchesDetailsLoaded,
    });
}

function CreateTableString(Name, Description) {
    var seperateClassForDefinition = "separate" + Name;
    var tableString = "<tr class='" + seperateClassForDefinition + "'> <td class=name" + Name + ">";
    tableString = tableString + Name + "</td><td class=desc" + Description + ">";
    tableString = tableString + Description + "</td></tr>";
    return tableString;
}

function CollegeBatchesDetailsLoaded(response) {
    var data = JSON.parse(response);
    var html = "";
    $.each(data, function (i, item) {
        html += "<tr><td>" + item.Name + "</td></tr>";
    });
    $(".batchTableBody").html(html);
}

function OpenAlertModal(message) {
    $("#AlertModal").modal({
        show: true,
    });
    $(".alertDetailContainer").html('');
    $(".alertDetailContainer").html(message);
}


function GetProgramSubjects(programID) {
    if ($(".programID").val() == 0 || $(".programID").val() == "") {
        $(".programID").val(programID);
    }
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/GetProgramSubjects',
        async: true,
        data: {
            programID: programID,
        },
        success: ProgramSubjectsLoaded,
    });
}

function ProgramSubjectsLoaded(response) {
    var data = JSON.parse(response);
    var html = "";
    $.each(data, function (i, item) {
        html += "<tr><td>" + item.Name + "</td><td value=" + item.ID + " onclick='RemoveSubjectfromProgram(" + item.ID + ")'>Remove</td></tr>";
    });
    $(".subjectTableBody").html(html);
    var programID = $(".programID").val();
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/GetSubjectsForProgram',
        async: true,
        data: {
            programID: programID
        },
        success: SubjectsForProgramLoaded
    });

}

function SubjectsForProgramLoaded(response) {
    var data = JSON.parse(response);
    var options = "";
    $.each(data, function (i, item) {
        if (item.IsSelected == true) {
            options += "<option selected value=" + item.ID + ">" + item.Name + "</option>";
            //$("#e9").val(item.iD);
        }
        else if (item.IsSelected == false) {
            options += "<option  value=" + item.ID + ">" + item.Name + "</option>";
        }
        //if (item.IsSelected == true) {
        //    $('#e9').append($("<option selected></option>").attr("value", item.ID).text(item.Name));
        //}
        //else {
        //    $('#e9').append($("<option></option>").attr("value", item.ID).text(item.Name));
        //}
    });
    $(".select2-search-choice").remove();
    $(".notAssociatedSubjects").html('');
    $(".notAssociatedSubjects").html(options);
    $(".btnCloseAddSubjects").click();
}

function ShowSubjectModal() {
    $("#AddSubjectsModal").modal({
        show: true,
    });
}

function AssignSubjectsForProgram() {
    var subjectIDValues = "";
    var programID = $(".programID").val();
    $('.notAssociatedSubjects :selected').each(function (i, selected) {
        if (subjectIDValues == "") {
            subjectIDValues = $(selected).val();
        }
        else {
            subjectIDValues += "," + $(selected).val();

        }
    });
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/AssignSubjectsForProgram',
        sync: true,
        data: {
            subjectIDString: subjectIDValues,
            programID: programID,

        },
        success: function (response) {
            GetProgramSubjects(response);
        },
    });
    // alert(subjectIDValues);
}

function RemoveSubjectfromProgram(subjectID) {
    // alert(subjectID);
    var programID = $(".programID").val();
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/DeleteSubjectFromProgram',
        sync: true,
        data: {
            subjectID: subjectID,
            programID: programID,
        },
        success: SubjectRemovedFromProgram
    });

}

function SubjectRemovedFromProgram(response) {
    var programID = $(".programID").val();
    if (response == "True") {
        OpenAlertModal('Subject removed from Program');
    }
    else {
        OpenAlertModal('Cannot delete.Please try after sometime');
    }
    GetProgramSubjects(programID);
}

function GetProgramQuestionPapers(programID) {
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/GetProgramQuestionPapers',
        async: true,
        data: {
            programID: programID,
        },
        success: ProgramQuestionPapersLoaded,
    });

}

function ProgramQuestionPapersLoaded(response) {
    var data = JSON.parse(response);
    var html = "";
    $.each(data, function (i, item) {
        if (item.IsTemplate == true) {
            html += "<tr><td>" + item.Name + "</td><td><button type='button' class='btn btn-warning' onclick='CreateQuestionPaperForTemplate(" + item.TestTemplateID + ", 0)'> Create QuestionPaper </button></td></tr>";
        }
        else {
            html += "<tr><td>" + item.Name + "</td><td>QuestionPaper created and added to the program<input type='checkbox' value=" + item.ID + " class='individualQuestionPaper' style='display:none;' checked/></td></tr>";
        }
    });
    $(".assessmentTableBody").html(html);
}

function ShowQuestionPaperModal() {
    $("#QuestionPaperModal").modal({
        show: true,
    });
}

function GetQuestionPapersForProgram() {
    var programID = $(".programID").val();
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/GetQuestionPapersForProgram',
        async: true,
        data: {
            programID: programID,
        },
        success: QuestionPaperForProgramLoaded,
    });
}
function QuestionPaperForProgramLoaded(response) {
    if (response != null) {
        var data = JSON.parse(response);
        $(".tblQuestionPapers").html('');
        $(".tblTestTemplates").html('');
        var bodyString = "";
        $.each(data.QuestionPaperViewModelList, function (i, item) {
            bodyString += "<tr><td>" + item.Name + "</td><td><input type='checkbox' value=" + item.ID + " class='individualQuestionPaper'/></td></tr>";
        });
        bodyString = bodyString.replace('null', '');
        $(".tblQuestionPapers").html(bodyString);
        bodyString = "";
        $.each(data.TestTemplateViewModelList, function (i, item) {
            bodyString += "<tr><td>" + item.Name + "</td><td><input type='checkbox' value=" + item.ID + " class='individualTestTemplate'/></td></tr>";
        });
        bodyString = bodyString.replace('null', '');
        $(".tblTestTemplates").html(bodyString);
    }
}

function AddQuestionPapersForProgram() {
    var programID = $(".programID").val();
    var assessmentIDs = "";
    $(".individualQuestionPaper:checked").each(function () {
        if (assessmentIDs == "")
            assessmentIDs = $(this).val();
        else
            assessmentIDs = assessmentIDs + "," + $(this).val();
    });
    if (assessmentIDs != "") {
        $.ajax({
            type: 'POST',
            url: '/ProgramWizard/AddQuestionPaperToProgram',
            data: {
                assessmentIDs: assessmentIDs,
                programID: programID
            },
            success: QuestionPaperAddedForProgram,

        });
    }
    else {
        OpenAlertModal('Please select alteast one QuestionPaper');
    }

}

function QuestionPaperAddedForProgram(response) {
    $(".btnCloseAsssessmentModal").click();
    if (response == "True") {
        OpenAlertModal('QuestionPapers added to the Program');
    }
    else {
        OpenAlertModal('Something went wrong.Please try after sometime');
    }
    var programID = $(".programID").val();
    GetProgramQuestionPapers(programID);
    GetQuestionPapersForProgram();
}

function AddQuestionPaperByTestTemplateForProgram() {
    var testTemplateID = "";
    var programID = $(".programID").val();
    $(".individualTestTemplate:checked").each(function () {
        testTemplateID = $(this).val();
        CreateQuestionPaperForTemplate(testTemplateID, programID);
    });
    GetProgramQuestionPapers(programID);
    $(".btnCloseAsssessmentModal").click();
    GetQuestionPapersForProgram();
}

function CreateQuestionPaperForTemplate(testTemplateID, programID) {
    programID = $(".programID").val();
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/CreateQuestionPaperForTemplate',
        async: false,
        data: {
            programID: programID,
            testTemplateID: testTemplateID,
        },
        success: function (response) {
            if (response == "True") {
                GetProgramQuestionPapers(programID);
            }
        },
    });
}



function GetProgramBatches(programID) {
    programID = $(".programID").val();
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/GetProgramBatches',
        async: true,
        data: {
            programID: programID,
        },
        success: ProgramBatchesLoaded,
    });
}

function ProgramBatchesLoaded(response) {
    var data = JSON.parse(response);
    var html = "";
    $.each(data, function (i, item) {
        html += "<tr><td>" + item.Name + "</td><td value=" + item.ID + " onclick='RemoveBatchfromProgram(" + item.ID + ")'>Remove</td></tr>";
    });
    $(".batchTableBody").html(html);
    collegeID = $(".ddlInstitute").val();
    var programID = $(".programID").val();
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/GetBatchesForProgram',
        async: true,
        data: {
            programID: programID,
            collegeID: collegeID
        },
        success: BatchesForProgramLoaded
    });

}

function BatchesForProgramLoaded(response) {
    var data = JSON.parse(response);
    var options = "";
    $.each(data, function (i, item) {
        if (item.IsSelected == true) {
            options += "<option selected value=" + item.ID + ">" + item.Name + "</option>";
        }
        else if (item.IsSelected == false) {
            options += "<option  value=" + item.ID + ">" + item.Name + "</option>";
        }
    });
    $(".select2-search-choice").remove();
    $(".notAssociatedBatches").html('');
    $(".notAssociatedBatches").html(options);
    $(".btnCloseAddBatches").click();
}

function ShowBatchModal() {
    $("#AddBatchesModal").modal({
        show: true,
    });
}

function AssignBatchesForProgram() {
    var batchesIDValues = "";
    var programID = $(".programID").val();
    $('.notAssociatedBatches :selected').each(function (i, selected) {
        if (batchesIDValues == "") {
            batchesIDValues = $(selected).val();
        }
        else {
            batchesIDValues += "," + $(selected).val();

        }
    });
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/AssignBatchesForProgram',
        sync: true,
        data: {
            batchIDString: batchesIDValues,
            programID: programID,

        },
        success: function (response) {
            GetProgramBatches(programID);
        },
    });
}

function RemoveBatchfromProgram(batchID) {
    // alert(subjectID);
    var programID = $(".programID").val();
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/DeleteBatchFromProgram',
        sync: true,
        data: {
            batchID: batchID,
            programID: programID,
        },
        success: BatchRemovedFromProgram
    });

}

function BatchRemovedFromProgram(response) {
    var programID = $(".programID").val();
    if (response == "True") {
        OpenAlertModal('Batch removed from Program');
    }
    else {
        OpenAlertModal('Cannot delete.Please try after sometime');
    }
    GetProgramBatches(programID);
}


function GetBatchStudentDetails(programID) {
    programID = $(".programID").val();
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/GetProgramBatches',
        async: true,
        data: {
            programID: programID,
        },
        success: BatchStudentDetailsLoaded,
    });

}

function BatchStudentDetailsLoaded(response) {
    var data = JSON.parse(response);
    var html = "";
    $.each(data, function (i, item) {
        html += "<tr><td>" + item.Name + "</td><td>" + item.StudentCount + "</td><td><button type='button' class='btn btn-success' onclick='ShowAddStudentsForBatchModal(" + item.ID + ")'><i class='fa fa-plus'></i> Add Students </button></td></tr>";
    });
    $(".batchStudentTableBody").html(html);

}

function ShowAddStudentsForBatchModal(batchID) {
    $(".batchID").val(batchID);
    var programID = $(".programID").val();
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/GetBatchStudentDetails',
        data: {
            batchID: batchID,
            programID: programID,
        },
        success: BatchStudentsDetailLoaded,
    });

}

function BatchStudentsDetailLoaded(response) {
    var data = JSON.parse(response);
    $(".tblStudents").html('');
    var bodyString = "";
    $.each(data, function (i, item) {
        bodyString += "<tr><td>" + item.Name + "</td><td><input type='checkbox' value=" + item.ID + " class='individualStudent'/></td></tr>";
    });
    bodyString = bodyString.replace('null', '');
    $(".tblStudents").html(bodyString);
    $("#AddBatchStudentModal").modal({
        show: true,
    });
}

function AddStudentsToBatch() {
    var batchID = $(".batchID").val();
    var studentIDs = "";
    $(".individualStudent:checked").each(function () {
        if (studentIDs == "")
            studentIDs = $(this).val();
        else
            studentIDs = studentIDs + "," + $(this).val();
    });
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/AddStudentsToBatch',
        data: {
            batchID: batchID,
            studentIDs: studentIDs,
        },
        success: AddStudentsToBatchSuccess,
    });
}

function AddStudentsToBatchSuccess(response) {
    if (response == "True") {
        OpenAlertModal('Students added to the Batch');
        //var batchID = response.split(',')[1];
        var programID = $(".programID").val();
        GetBatchStudentDetails(programID);
    }
    else {
        OpenAlertModal('Something went wrong.Please try after sometime');
    }
    $(".btnCloseAddBatchStudentModal").click();
    $(".batchID").val(0);
}

function UploadStudentsForBatch() {
    var batchID = $(".batchID").val();
    var programID = $(".programID").val();
    var form = $(".studentForm");
    var formData = new FormData(form);
    var file = $(".studentFile").get(0).files;
    formData.append("StudentDetails", file[0]);
    formData.append("BatchID", batchID);
    formData.append("ProgramID", programID);
    $.ajax({
        type: 'POST',
        url: '/ProgramWizard/UploadStudentsForBatch',
        processData: false,
        contentType: false,
        data: formData,
        success: function (response) {
            $(".btnCloseAddBatchStudentModal").click();
            $(".batchID").val(0);
            OpenAlertModal(response);
            GetBatchStudentDetails(programID);
        },
    });
}

function FinishWizard() {
    window.location.href = '/Program/Index';
}


function TabChanged(tabIndex) {
    collegeID = $(".ddlInstitute").val();
    var programName = $(".progName").val();
    var programDescription = $(".progDesc").val();
    var programID = $(".programID").val();
    switch (tabIndex) {
        case 0:
            break;
        case 1:
            //if (programName != "" && collegeID != "" && selectedCourseTemplateID != "") {
            //    var result = true;
            //    $.ajax({
            //        type: 'POST',
            //        url: '/ProgramWizard/EditProgramInformation',
            //        async: true,
            //        data: {
            //            CourseTemplateID: selectedCourseTemplateID,
            //            InstituteID: collegeID,
            //            ProgramName: programName,
            //            ProgramDescription: programDescription,
            //            ProgramID: programID,
            //        },
            //        success: function (response) {
            //            if (response.split(',')[0] == 'true') {
            //                $('.ProgramPage').find('.active').removeClass('active');
            //                $('.ProgramPage').find('#' + response + '').addClass('active');
            //                GetProgramSubjects(response.split(',')[2]);
            //            }
            //            else {
            //                OpenAlertModal('Something went wrong.Please try after sometime');
            //            }
            //        }
            //    });
            //    return result;
            //}
            //else {
            //    if (programName == "") {
            //        OpenAlertModal('Please provide name for the Program to be created');
            //    }
            //    else if (collegeID == "") {
            //        OpenAlertModal('Please select Institute for the Program to be created');
            //    }
            //    else if (selectedCourseTemplateID == "") {
            //        OpenAlertModal('Please select Course Template for the Program to be created');
            //    }
            //    return false;
            //}
            break;
        case 2:
            alert("Test");
            break;
        case 3:
            alert("Batches");
            break;
        case 4:
            alert("Students");
            break;
    }
}

function SaveProgramInformation() {
    collegeID = $(".ddlInstitute").val();
    var programName = $(".progName").val();
    var programDescription = $(".progDesc").val();
    var programID = $(".programID").val();
    if (programName != "" && collegeID != "" && selectedCourseTemplateID != "") {
        var result = true;
        $.ajax({
            type: 'POST',
            url: '/ProgramWizard/EditProgramInformation',
            async: true,
            data: {
                CourseTemplateID: selectedCourseTemplateID,
                InstituteID: collegeID,
                ProgramName: programName,
                ProgramDescription: programDescription,
                ProgramID: programID,
            },
            success: function (response) {
                if (response.split(',')[0] == 'true') {
                    $('.ProgramPage').find('.active').removeClass('active');
                    $('.ProgramPage').find('#' + response + '').addClass('active');
                    GetProgramSubjects(response.split(',')[2]);
                }
                else {
                    OpenAlertModal('Something went wrong.Please try after sometime');
                }
            }
        });
    }
    else {
        if (programName == "") {
            OpenAlertModal('Please provide name for the Program to be created');
        }
        else if (collegeID == "") {
            OpenAlertModal('Please select Institute for the Program to be created');
        }
        else if (selectedCourseTemplateID == "") {
            OpenAlertModal('Please select Course Template for the Program to be created');
        }
    }
}
