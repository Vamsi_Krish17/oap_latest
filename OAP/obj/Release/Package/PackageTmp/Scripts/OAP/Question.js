﻿$(function () {
    //RemoveScriptForThisPage("bootstrap-switch.js", "js");
    //RemoveScriptForThisPage("toggle-init.js", "js");
    //$(".btnSaveAnswer").click(function () {
    //    SaveAnswer();
    //});

    $(".btnCloseAnswer").click(function () {
        CKEDITOR.instances.txtAnswer.setData('');
        $(".chkIsCorrectAnswer").prop("checked", false);
    });
    $(".btnAddAnswer").click(function () {
        ChangeOnclickForAnswerUpdate("SaveAnswer", "");
    });
    $(".btnAddTechnicalAnswer").click(function () {
        ChangeOnclickForTechnicalAnswerUpdate("SaveTechnicalAnswer", "");
    });

    //window.onload = function () {
    //    document.onkeydown = function (e) {
    //        if (e.which == 116 || e.keyCode == 116) {
    //            alert("Do not refresh the page");
    //            return false;
    //        }
    //        else
    //            return;
    //        //return (e.which || e.keyCode) != 116;
    //    };
    //}

    $(".chkAll").change(function () {
        if ($(this).prop("checked")) {
            $(".chkSeperate").prop("checked", true);
        }
        else {
            $(".chkSeperate").prop("checked", false);
        }
    });

});

function RemoveScriptForThisPage(filename, filetype) {
    //var targetelement = (filetype == "js") ? "script" : (filetype == "css") ? "link" : "none" //determine element type to create nodelist from
    //var targetattr = (filetype == "js") ? "src" : (filetype == "css") ? "href" : "none" //determine corresponding attribute to test for

    var targetelement = "script";
    var targetattr = "src";
    var allsuspects = document.getElementsByTagName(targetelement)
    for (var i = allsuspects.length; i >= 0; i--) { //search backwards within nodelist for matching elements to remove
        if (allsuspects[i] && allsuspects[i].getAttribute(targetattr) != null && allsuspects[i].getAttribute(targetattr).indexOf(filename) != -1) {
            allsuspects[i].parentNode.removeChild(allsuspects[i]) //remove element by calling parentNode.removeChild()
        }
    }
}

function CreateTableString(answerID, answerDescription, isCorrectAnswer) {
    var yesNo;
    if (isCorrectAnswer == "true" || isCorrectAnswer == "True" || isCorrectAnswer) {
        yesNo = "Yes";
    }
    else {
        yesNo = "No";
    }
    var seperateClassForAnswer = "separate" + answerID;
    var tableString = "<tr class='" + seperateClassForAnswer + "'><td>";
    tableString = tableString + answerDescription + "</td><td>";
    tableString = tableString + yesNo + "</td>";
    tableString = tableString + "<td onclick='EditAnswer(" + answerID + ",this)' style='cursor:pointer'>Edit</td>";
    tableString = tableString + "<td onclick='OpenDeleteAnswerModal(" + answerID + ",this)' style='cursor:pointer'>Delete</td>";
    return tableString;
}

function CreateTechnicalTableString(answerID, inputParam, outputParam) {
    
    var seperateClassForAnswer = "separate" + answerID;
    var tableString = "<tr class='" + seperateClassForAnswer + "'><td>";
    tableString = tableString + inputParam + "</td><td>";
    tableString = tableString + outputParam + "</td>";
    tableString = tableString + "<td onclick='EditAnswer(" + answerID + ",this)' style='cursor:pointer'>Edit</td>";
    tableString = tableString + "<td onclick='OpenDeleteAnswerModal(" + answerID + ",this)' style='cursor:pointer'>Delete</td>";
    return tableString;
}

function SaveAnswer(questionID) {
    var answerContent = CKEDITOR.instances.txtAnswer.getData();
    var isCorrectAnswer = $(".chkIsCorrectAnswer").prop("checked");
    //var questionID = questionID;
    var questionIDString = questionID;
    if (answerContent != "-1") {        
        DisableControlsForSavingAnswer();
        OpenModal("loadingModal");
        $.ajax({
            type: 'POST',
            url: '/Question/SaveAnswer',
            data: {
                answerContent: answerContent,
                isCorrectString: isCorrectAnswer,
                questionIDString: questionID
            },
            success: AnswerSaved
        });
    }
    else {
        ShowAlert("Answer should not be empty");
        //alert("Answer should not be empty");
    }
}

function SaveTechnicalAnswer(questionID) {
    var inputParam = $(".inputParam").val();
    var outputParam = $(".outputParam").val();
    //var questionID = questionID;
    var questionIDString = questionID;
    if (inputParam.length != 0) {
        DisableControlsForSavingAnswer();
        OpenModal("loadingModal");
        $.ajax({
            type: 'POST',
            url: '/Question/SaveTechnicalAnswer',
            data: {
                inputParam: inputParam,
                outputParam: outputParam,
                questionIDString: questionID
            },
            success: TechnicalAnswerSaved
        });
    }
    else {
        ShowAlert("Answer should not be empty");
        //alert("Answer should not be empty");
    }
}

function SaveQuestion(questionID) {
    var answerContent = CKEDITOR.instances.txtAnswer.getData();
    var isCorrectAnswer = $(".chkIsCorrectAnswer").prop("checked");
    //var questionID = questionID;
    var questionIDString = questionID;
    if (answerContent != "-1") {
        DisableControlsForSavingAnswer();
        OpenModal("loadingModal");
        $.ajax({
            type: 'POST',
            url: '/Question/SaveQuestion',
            data: {
                answerContent: answerContent,
                isCorrectString: isCorrectAnswer,
                questionIDString: questionID
            },
            success: AnswerSaved
        });
    }
    else {
        ShowAlert("Answer should not be empty");
        //alert("Answer should not be empty");
    }
}


function AnswerSaved(response) {

    response = JSON.parse(response);

    var answerID = response.AnswerID;
    var isCorrectAnswer = response.IsCorrectAnswer;
    var answerDescription = response.AnswerDescription;

    var tableString = CreateTableString(answerID, answerDescription, isCorrectAnswer);
    $(".tblBody").append(tableString);
    $(".chkIsCorrectAnswer").prop("checked", false);
    var savedAnswers = $(".hdnAnswerIDs").val();

    if (savedAnswers == "") {
        $(".hdnAnswerIDs").val(answerID)
    }
    else {
        savedAnswers = savedAnswers + "," + answerID;
        $(".hdnAnswerIDs").val(savedAnswers)
    }
    CKEDITOR.instances.txtAnswer.setData('');
    EnableControlsAfterSavingAnswer();
    $(".btnCloseAnswer").click();
    ChangeOnclickForAnswerUpdate("SaveAnswer", "");
    $(".modal").modal('hide');
}

function TechnicalAnswerSaved(response) {

    response = JSON.parse(response);

    var answerID = response.AnswerID;
    var outputParam = response.OutputParam;
    var inputParam = response.InputParam;

    var tableString = CreateTechnicalTableString(answerID, inputParam, outputParam);
    $(".tblBody").append(tableString);
    var savedAnswers = $(".hdnAnswerIDs").val();

    if (savedAnswers == "") {
        $(".hdnAnswerIDs").val(answerID)
    }
    else {
        savedAnswers = savedAnswers + "," + answerID;
        $(".hdnAnswerIDs").val(savedAnswers)
    }
    //CKEDITOR.instances.txtAnswer.setData('');
    EnableControlsAfterSavingAnswer();
    $(".btnCloseAnswer").click();
    ChangeOnclickForTechnicalAnswerUpdate("SaveTechnicalAnswer", "");
    $(".modal").modal('hide');
}

function AddAnswer(questionID) {
    var answerContent = CKEDITOR.instances.txtAnswer.getData();
    var isCorrectAnswer = $(".chkIsCorrectAnswer").prop("checked");
    if (answerContent != "") {
        DisableControlsForSavingAnswer();
        $.ajax({
            type: 'POST',
            url: '/Question/AddAnswer',
            data: {
                questionIDString: questionID,
                answerContent: answerContent,
                isCorrectString: isCorrectAnswer,
            },
            success: AnswerAdded
        });
    }
    else {
        alert("Answer should not be empty");
    }
}
function AnswerAdded(response) {
    response = JSON.parse(response);
    EnableControlsAfterSavingAnswer();
    $(".btnCloseAnswer").click();
    var answerID = response.AnswerID;
    var answerDescription = response.AnswerDescription;
    var isCorrectAnswer = response.IsCorrectAnswer;
    var tableString = CreateTableStringForAnswerEditPage(answerID, answerDescription, isCorrectAnswer);
    $(".tblBody").append(tableString);
}

function DisableControlsForSavingAnswer() {
    $(".btnCloseAnswer").attr("disabled", "disabled");
    $(".btnSaveAnswer").attr("disabled", "disabled");
    $(".close").attr("disabled", "disabled");
}

function EnableControlsAfterSavingAnswer() {
    $(".btnCloseAnswer").removeAttr("disabled");
    $(".btnSaveAnswer").removeAttr("disabled");
    $(".close").removeAttr("disabled");
}



function EditAnswer(answerID, obj) {
    //alert(answerID);
    //HideCheckBoxAppropriateToAnswer();
    if ($(".chkIsCorrectAnswer").prop("checked", true)) {
        $("#hdnCorrectAnswerSelected").val("false");
        //$(".chkIsCorrectAnswer").removeAttr("checked");
    }
    ChangeOnclickForAnswerUpdate("UpdateAnswer", answerID);
    $.ajax({
        type: "POST",
        url: "/Question/GetAnswerByID",
        data: {
            answerIDString: answerID
        },
        success: AnswerLoaded
    });
}

function AnswerLoaded(response) {
    response = JSON.parse(response);
    var answerID = response.AnswerID;
    var answerDescription = response.AnswerDescription;
    var isCorrectAnswer = response.IsCorrectAnswer;
    if (isCorrectAnswer == "true" || isCorrectAnswer == "True" || isCorrectAnswer) {
        //$("#hdnCorrectAnswerSelected").val("true");
        $(".chkIsCorrectAnswer").prop("checked", true);
        //$(".correctAnswerContainer").show();
    }
    else {
        $(".chkIsCorrectAnswer").prop("checked", false);
    }
    CKEDITOR.instances.txtAnswer.setData(answerDescription);
   $(".btnAddAnswer").click();
}

function OpenDeleteAnswerModal(answerID)
{
    $(".deleteAnswer").attr("onclick", "DeleteAnswer(" + answerID + ")");
    OpenModal("deleteAnswerModal");
}
function OpenPassageModal(passageID) {

    OpenModal("PassageModal");
    $.ajax({
        type: "POST",
        url: "/Passage/PassageEdit",
        data: {
            passageid: passageID,
        },
        success: function (response) {
            $(".passageEdit").html(response);
        }
    });
}

//function DeleteAnswer(answerID, obj) {
    function DeleteAnswer(answerID) {
    $.ajax({
        type: "POST",
        url: "/Question/DeleteAnswerByID",
        data: {
            answerIDString: answerID

        },
        success: function (response) {
            //if (response == "CorrectAnswerDeleted") {
            //    $("#hdnCorrectAnswerSelected").val("false");
            //    $(".chkIsCorrectAnswer").removeAttr("checked");
            //    $(".correctAnswerContainer").show();
            //}
            var seperateClass = "separate" + answerID;
            $("." + seperateClass).remove();
            ShowAlert("Answer deleted successfully.");
        }
    });
}

function UpdateAnswer(answerID) {
    //HideCheckBoxAppropriateToAnswer();
    var answerDescription = CKEDITOR.instances.txtAnswer.getData();
    var isCorrectAnswer = $(".chkIsCorrectAnswer").prop("checked");
    if (answerDescription != "") {
        $.ajax({
            type: "POST",
            url: "/Question/UpdateAnswer",
            data: {
                answerIDString: answerID,
                answerDescription: answerDescription,
                isCorrectString: isCorrectAnswer,
            },
            success: function (response) {

                response = JSON.parse(response);
                var answerID = response.AnswerID;
                var answerDescription = response.AnswerDescription;
                var isCorrectAnswer = response.IsCorrectAnswer;
                $(".chkIsCorrectAnswer").prop("checked", false);
                //if (isCorrectAnswer == "true" || isCorrectAnswer == "True" || isCorrectAnswer) {
                //    $(".chkIsCorrectAnswer").prop("checked", true);
                //    //$("#hdnCorrectAnswerSelected").val("true");
                //    //$(".correctAnswerContainer").hide();
                //    //$(".chkIsCorrectAnswer").removeAttr("checked");
                //}
                //else {
                //    $(".chkIsCorrectAnswer").prop("checked", false);
                //}
                var tableString = CreateTableString(answerID, answerDescription, isCorrectAnswer);
                var seperateClass = "separate" + answerID;
                $("." + seperateClass).replaceWith(tableString);
                $(".btnCloseAnswer").click();
            }
        });
    }
}

function ChangeOnclickForAnswerUpdate(functionName, answerID) {
    if (functionName == "UpdateAnswer" || functionName == "UpdateAnswerForQuestionEditPage") {
        $(".saveChange").attr("onclick", functionName + "(" + answerID + ")");
    }
    else
        $(".saveChange").attr("onclick", functionName + "()");
    //$(".saveChange").replaceWith("<div class='saveChange' onclick='"+functionName+"("+answerID+")'> Save changes</div>");
}

function ChangeOnclickForTechnicalAnswerUpdate(functionName, answerID) {
    if (functionName == "UpdateTechnicalAnswer" || functionName == "UpdateTechnicalAnswerForQuestionEditPage") {
        $(".saveChange").attr("onclick", functionName + "(" + answerID + ")");
    }
    else
        $(".saveChange").attr("onclick", functionName + "()");
    //$(".saveChange").replaceWith("<div class='saveChange' onclick='"+functionName+"("+answerID+")'> Save changes</div>");
}

function SaveTag() {
    var tagName = $(".tagName").val();
    if (tagName != "") {
        OpenModal("loadingModal");
        $.ajax({
            type: 'POST',
            url: '/Question/CreateTag',
            data:
                {
                    tagName: tagName,
                },
            success: TagSaved
        });
    }
    else {
        ShowAlert("Tag name should not be empty");
    }
}

function TagSaved(response) {
    if (response.toLowerCase() == "false") {
        ShowAlert("Tag not saved.Please try after sometime");
    }
    else {
        var tagID = response.split(',')[0];
        var tagName = response.split(',')[1];
        $(".associatedTags").append("<option value=" + tagID + ">" + tagName + "</option> ");
        ShowAlert("Tag saved successfully");
    }
    $(".tagName").val('');
    //$(".modal").modal('hide');
    //$(".btnCloseTag").click();
}

//Question Edit Page

function UpdateAnswerForQuestionEditPage(answerID) {
    var answerDescription = CKEDITOR.instances.txtAnswer.getData();
    var isCorrectAnswer = $(".chkIsCorrectAnswer").prop("checked");
    if (answerDescription != "") {
        $.ajax({
            type: "POST",
            url: "/Question/UpdateAnswer",
            data: {
                answerIDString: answerID,
                answerDescription: answerDescription,
                isCorrectString: isCorrectAnswer,
            },
            success: function (response) {

                response = JSON.parse(response);
                var answerID = response.AnswerID;
                var answerDescription = response.AnswerDescription;
                var isCorrectAnswer = response.IsCorrectAnswer;
                var questionID = response.QuestionID;
                $(".chkIsCorrectAnswer").prop("checked", false);

                var tableString = CreateTableStringForAnswerEditPage(answerID, answerDescription, isCorrectAnswer);
                var seperateClass = "separate" + answerID;
                $("." + seperateClass).replaceWith(tableString);
                $(".btnCloseAnswer").click();
                ChangeOnclickForAnswerUpdate("AddAnswer", "");
            }
        });
    }
}

function CreateTableStringForAnswerEditPage(answerID, answerDescription, isCorrectAnswer) {
    var yesNo;
    if (isCorrectAnswer == "true" || isCorrectAnswer == "True" || isCorrectAnswer) {
        yesNo = "Yes";
    }
    else {
        yesNo = "No";
    }
    var seperateClassForAnswer = "separate" + answerID;
    var tableString = "<tr class='" + seperateClassForAnswer + "'><td>";
    tableString = tableString + answerDescription + "</td><td>";
    tableString = tableString + yesNo + "</td>";
    tableString = tableString + "<td onclick='EditAnswer(" + answerID + ",this)' data-toggle='modal' href='#answerModal'>Edit</td>";
    tableString = tableString + "<td onclick='DeleteAnswer(" + answerID + ",this)' style='cursor:pointer'>Delete</td>";
    return tableString;
}

function EditAnswerForAnswerEditPage(answerID, obj) {
    if ($(".chkIsCorrectAnswer").prop("checked", true)) {
        $("#hdnCorrectAnswerSelected").val("false");
    }
    $.ajax({
        type: "POST",
        url: "/Question/GetAnswerByID",
        data: {
            answerIDString: answerID
        },
        success: AnswerLoadedForAnswerEditPage
    });
    //ChangeOnclickForAnswerUpdate("UpdateAnswerForQuestionEditPage", answerID);
}

function AnswerLoadedForAnswerEditPage(response) {
    response = JSON.parse(response);
    var answerID = response.AnswerID;

    var answerDescription = response.AnswerDescription;
    var isCorrectAnswer = response.IsCorrectAnswer;
    if (isCorrectAnswer == "true" || isCorrectAnswer == "True" || isCorrectAnswer) {
        $(".chkIsCorrectAnswer").prop("checked", true);
    }
    else {
        $(".chkIsCorrectAnswer").prop("checked", false);
    }
    CKEDITOR.instances.txtAnswer.setData(answerDescription);
    ChangeOnclickForAnswerUpdate("UpdateAnswerForQuestionEditPage", answerID);
    //$(".btnAddAnswer").click();
}

//function AddAnswer(questionID) {
//    var answerContent = CKEDITOR.instances.txtAnswer.getData();
//    var isCorrectAnswer = $(".chkIsCorrectAnswer").prop("checked");
//    if (answerContent != "") {
//        DisableControlsForSavingAnswer();
//        $.ajax({
//            type: 'POST',
//            url: '/Question/AddAnswer',
//            data: {
//                questionIDString: questionID,
//                answerContent: answerContent,
//                isCorrectString: isCorrectAnswer,
//            },
//            success: AnswerAdded
//        });
//    }
//    else {
//        alert("Answer should not be empty");
//    }
//}
//End Question Edit Page



//Correction page

function BindBatch() {
    selectedColleges = $(".ddlCollege").val();
    selectedQuestionPapers = $(".ddlQuestionPaper").val();
    if (selectedColleges != "" && selectedColleges != null && selectedQuestionPapers != "" && selectedQuestionPapers != null) {
        selectedColleges = selectedColleges.toString();
        selectedQuestionPapers = selectedQuestionPapers.toString();
        DisableControls();
        $.ajax({
            url: "/Question/GetBatchUnderCollegeAndQuestionPaper",
            type: "POST",
            data: {
                collegeIDString: selectedColleges,
                assessmentIDString: selectedQuestionPapers,
            },
            success: BatchLoaded
        });
    }
    else {
        $(".ddlBatch").html('');
        BindDataTable();
    }
}

function BatchLoaded(response) {
    MakeTheControlsAvailable();

    var items = [];
    //items.push("<option value='0'>Select</option>");
    items.push(ConverDataToArray(response));
    $(".ddlBatch").html(items.join(''));

    BindDataTable();
}

function ConverDataToArray(data) {
    var items = [];
    data = JSON.parse(data);
    $.each(data, function () {
        items.push("<option value=" + this.ID + ">" + this.Name + "</option>");
    });

    return items;
}

function DisableControls() {
    $(".ddl").attr("disabled", "disabled");
}

function MakeTheControlsAvailable() {
    $(".ddl").removeAttr("disabled");
}

function OpenStaffAssignModal() {
    var selectedQuestionPaperSnapshots = "";

    $(".chkSeperate:checked").each(function () {
        if (selectedQuestionPaperSnapshots == "")
            selectedQuestionPaperSnapshots = $(this).val();
        else
            selectedQuestionPaperSnapshots = selectedQuestionPaperSnapshots + "," + $(this).val();
    });
    if (selectedQuestionPaperSnapshots == "") {
        ShowAlert("Please select atleast one question to assign");
    }
    else {
        $("#assignStaffModal").modal(
            {
                show: true,
                backdrop: 'static',
            });
    }

}

function AssignToStaff() {
    var selectedStaff = $(".ddlStaff").val();
    if (selectedStaff == "") {
        $(".errorHolder").html("Please select the staff");
    }
    else {
        var selectedQuestionPaperSnapshots = "";

        $(".chkSeperate:checked").each(function () {
            if (selectedQuestionPaperSnapshots == "")
                selectedQuestionPaperSnapshots = $(this).val();
            else
                selectedQuestionPaperSnapshots = selectedQuestionPaperSnapshots + "," + $(this).val();
        });
        if (selectedQuestionPaperSnapshots != "") {
            $.ajax({
                url: "/Question/AssignQuestionPaperSnapshotToStaff",
                type: "POST",
                data: {
                    selectedStaffIDString: selectedStaff,
                    selectedQuestionPaperSnapshots: selectedQuestionPaperSnapshots,
                },
                success: AssignToStaffSuccessful
            });
        }
        else {
            ShowAlert("Please select atleast one question to assign");
        }
    }
}

function AssignToStaffSuccessful(response) {
    ShowAlert("Questions successfully assigned to selected staff");
    BindDataTable();
}

function StaffChanged() {
    $(".errorHolder").html("");
}

function BatchChanged(obj) {
    selectedBatches = $(".ddlBatch").val();
    BindDataTable();
}

//function ShowAlert(alertContent) {
//    $(".modal").modal('hide');
//    $(".alertContent").html(alertContent);
//    $("#alertModal").modal({
//        show: true,
//        backdrop: 'static',
//    });
//}

//End Correction Page

//Evaluate Answer

function OpenCorrectByQuestionModal() {
    OpenModal("correctByQuestionModal");
}

function OpenCorrectByStudentModal() {
    OpenModal("correctByStudentModal");
}

function OpenModal(modalName) {
    $(".modal").modal('hide');
    $("#" + modalName).modal({
        show: true,
        backdrop: 'static',
    });
}

function SortByStudent() {
    var selectedStudentID = $(".ddlStudent").val();
    if (selectedStudentID != 0 && selectedStudentID != "") {
        window.location.href = "/Question/EvaluateAnswer?sort=student&sortID=" + selectedStudentID;
    }
    else {
        window.location.href = "/Question/EvaluateAnswer?sort=student";
    }
}

function SortByQuestion() {
    var selectedQuestionID = $(".ddlQuestion").val();
    if (selectedQuestionID != 0 && selectedQuestionID != "") {
        window.location.href = "/Question/EvaluateAnswer?sort=question&sortID=" + selectedQuestionID;
    }
    else {
        window.location.href = "/Question/EvaluateAnswer?sort=question";
    }
}

//End Evaluate Answer

//Delete question

function OpenDeleteModal() {
    var selectedQuestions = "";
    $(".chkSeperate:checked").each(function () {
        if (selectedQuestions == "") {
            selectedQuestions = $(this).val();
        }
        else {
            selectedQuestions = selectedQuestions + "," + $(this).val();
        }
    });
    if (selectedQuestions == "") {
        ShowAlert("Please select ateast one question to delete");
    }
    else {
        OpenModal("deleteModal");
    }
}

function DeleteQuestion()
{
    var selectedQuestions = "";
    $(".chkSeperate:checked").each(function () {
        if (selectedQuestions == "") {
            selectedQuestions = $(this).val();
        }
        else {
            selectedQuestions = selectedQuestions + "," + $(this).val();
        }
    });
    if (selectedQuestions == "") {
        ShowAlert("Please select ateast one question to delete");
    }
    else {
        OpenModal("loadingModal");
        $.ajax({
            url: "/Question/Delete",
            type: "POST",
            data: {
                questionIDString: selectedQuestions,
            },
            success:QuestionsDeleted
        });
    }
}

function QuestionsDeleted(response)
{
    if (response.toLowerCase(response) == "true") {
        ShowAlert("Questions deleted successfully");
        $(".chkAll").prop("checked", false);
        BindDataTable();
    }
    else {
        ShowAlert("Some error occured while deleting the questions. Please try again");
    }

}

//End delete question