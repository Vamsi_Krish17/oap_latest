﻿$(function () {
    $(".chkAll").change(function () {
        if ($(this).prop("checked")) {
            $(".chkSeperate").prop("checked", true);
        }
        else {
            $(".chkSeperate").prop("checked", false);
        }
    });
});


function SaveQuestionModal(passageID) {

    OpenModal("SaveQustionsModal");
    $.ajax({
        type: "POST",
        url: "/Passage/PassageEditPost",
        data: {
            save: passageID,
        },
        success: function (response) {
            $(".savepassage").html(response);
        }
    });
}

function SaveNewQuestionModal(passageID) {

    OpenModal("PassageModal");
    $.ajax({
        type: "POST",
        url: "/Passage/PassageEdit",
        data: {
            passageid: passageID,
        },
        success: function (response) {
            $(".passageEdit").html(response);
        }
    });
}

function AddExistQuestionModal(passageID) {

    OpenModal("PassageModal");
    $.ajax({
        type: "POST",
        url: "/Passage/PassageEdit",
        data: {
            passageid: passageID,
        },
        success: function (response) {
            $(".passageEdit").html(response);
        }
    });
}


function AssignQuestionsToPassage(passageID) {
    var selectedQuestionIDs = "";
    $(".chkSeperate:checked").each(function () {
        if (selectedQuestionIDs == "")
            selectedQuestionIDs = $(this).attr("id");
        else
            selectedQuestionIDs = selectedQuestionIDs + "," + $(this).attr("id");
    });
    if (selectedQuestionIDs == "")
        ShowAlert("Please select atleast one question");
    else {
        $.ajax({
            url: "/Passage/AssignQuestionsToPassage",
            type: "POST",
            data: {
                passageIDString: passageID,
                selectedQuestionIDString: selectedQuestionIDs,
            },
            success: QuestionAssigned
        });
    }
}

function QuestionAssigned() {
    window.location.reload();
}

//Index

function OpenDeleteModal() {
    var selectedPassage = "";
    $(".chkSeperate:checked").each(function () {
        if (selectedPassage == "") {
            selectedPassage = $(this).val();
        }
        else {
            selectedPassage = selectedPassage + "," + $(this).val();
        }
    });
    if (selectedPassage == "") {                           //No passage selected
        ShowAlert("Please select atleast one record to delete");
    }
    else {                                              //Passage selected
        OpenModal("deleteModal");
    }

}

function DeletePassage() {
    var selectedPassages = "";
    $(".chkSeperate:checked").each(function () {
        if (selectedPassages == "") {
            selectedPassages = $(this).val();
        }
        else {
            selectedPassages = selectedPassages + "," + $(this).val();
        }
    });
    if (selectedPassages == "") {                           //No passage selected
        ShowAlert("Please select atleast one record to delete");
    }
    else {                                              //Passage selected
        $.ajax({
            url: "/Passage/DeletePassages",
            type: "POST",
            data: {
                passageIDString:selectedPassages
            },
            success:DeletePassageSuccess

        });
    }
}

function DeletePassageSuccess(response)
{
    if (response.toLowerCase() == "true") {
        ShowAlert("Passage deleted successfully");
    }
    else {
        ShowAlert("Some error occured while deleting the passage. Please try again.");
    }
    $(".chkAll").prop("checked", false);
    BindPassageTable();
}

//function OpenModal(modalName) {
//    $(".modal").modal('hide');
//    $("#" + modalName).modal({
//        show: true,
//        backdrop: 'static',
//    });
//}

//function ShowAlert(alertContent) {
//    $(".modal").modal('hide');
//    $(".alertContent").html(alertContent);
//    $("#alertModal").modal({
//        show: true,
//        backdrop: 'static',
//    });
//}


//End Index