﻿function ShowAlert(alertContent) {
    $(".modal").modal('hide');
    $(".alertContent").html(alertContent);
    $("#alertModal").modal({
        show: true,
        backdrop: 'static',
    });
}

function OpenModal(modalName) {
    $(".modal").modal('hide');
    $("#" + modalName).modal({
        show: true,
        backdrop: 'static',
    });
}