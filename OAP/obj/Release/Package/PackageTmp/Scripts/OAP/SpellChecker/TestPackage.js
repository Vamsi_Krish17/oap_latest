﻿
function DeleteTestPackage() {
    var selectedTestPackage = "";
    $(".chkSeperate:checked").each(function () {
        if (selectedTestPackage == "") {
            selectedTestPackage = $(this).val();
        }
        else {
            selectedTestPackage = selectedTestPackage + "," + $(this).val();
        }
    });

    if (selectedTestPackage == "") {
        ShowAlert("Please select atleast one record to delete");
    }
    else {
        OpenModal("loadingModal");
        $.ajax({
            url: "/TestPackage/Delete",
            type: "POST",
            data: {
                testPackageIDString : selectedTestPackage,
            },
            success: DeleteTestPackageSuccess
        });
    }
}

function DeleteTestPackageSuccess(response) {
    if (response.toLowerCase() == "true") {
        ShowAlert("TestPackage successfully deleted");
    }
    else {
        ShowAlert("Some error occured. Please try again.");
    }
    $(".chkAll").prop("checked", false);
    BindTestPackageTable();
}

function OpenDeleteModal() {
    if ($(".chkSeperate:checked").length <= 0) {
        ShowAlert("Please select atleast one record to delete");
    }
    else {
        OpenModal("deleteModal");
    }
}
