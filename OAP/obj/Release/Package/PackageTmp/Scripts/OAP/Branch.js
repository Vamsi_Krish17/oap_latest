﻿//Branch Index


function OpenBranchDeleteModal() {

    if ($(".chkSeperate:checked").length > 0) {                                           //Branches selected
        OpenModal("deleteModal");
    }
    else {                                                                                  //No Branches selected
        ShowAlert("Please select atleast one record to delete");
    }
}

function DeleteBranch() {

    var selectedBranches = $(".chkSeperate:checked").map(function () {
        return $(this).val();
    }).get();
    if (selectedBranches != "") {                                                       //Branches selected
        OpenModal("loadingModal");
        $.ajax({
            url: "/Branch/DeleteBranches",
            type: "POST",
            data: {
                branchIDString: selectedBranches.toString(),
            },
            success: DeleteBranchSuccess
        });
    }
    else {                                                                              //No branches selected
        ShowAlert("Please select atleast one record to delete");
    }
}

function DeleteBranchSuccess(response) {
    if (response.toLowerCase() == "true") {
        ShowAlert("Branch delete successfully");
    }
    else {
        ShowAlert("Branch not deleted. Please try again later.");
    }
    $(".chkAll").prop("checked", false);
    BindBranchDataTable();
}


//End Branch Index