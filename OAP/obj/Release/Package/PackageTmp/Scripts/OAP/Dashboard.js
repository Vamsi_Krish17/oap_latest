﻿function LoadAdminDashboardContents() {
    $.ajax({
        url: "/Dashboard/LoadMiniStatisticsForAdmin",
        type: "POST",
        success: DashboardContentsLoaded
    });
}

function DashboardContentsLoaded(data) {
    data = JSON.parse(data);

    var totalStudentsCount = data.TotalStudents;
    var createdTests = data.CreatedTests;
    var activeUsers = data.ActiveUsers;
    var activeTests = data.ActiveTests;
    var completedTestsToday = data.CompletedTestsToday;
    var completedTestsLastSeven = data.CompletedTestsLastSeven;
    var completedTestsLastThirty = data.CompletedTestsLastThirty;
    var completedTestsAll = data.CompletedTestsAll;


    $(".totalStudentsCount").html(totalStudentsCount);
    $(".createdTests").html(createdTests);
    $(".activeUsers").html(activeUsers);
    $(".activeTests").html(activeTests);
    $(".completedTestsToday").html(completedTestsToday);
    $(".completedTestsLastSeven").html(completedTestsLastSeven);
    $(".completedTestsLastThirty").html(completedTestsLastThirty);
    $(".completedTestsAll").html(completedTestsAll);

}

///Test List

function OpenMarksScoredDetails(assessmentID)
{

    OpenModal("loadingModal");
    $.ajax({
        url: "/QuestionPaper/GetMarScoredByQuestionPaperID",
        type: "POST",
        data: {
            assessmentIDString:assessmentID,
        },
        success:MarksScoredDetailsLoaded
    });
}

function MarksScoredDetailsLoaded(data)
{
    data = Json.parse(data);
    var assessmentName = data.Name;
    $.each(data.QuestionPaperSectionViewModelList, function () {
        alert(this.Name);
        //items.push("<option value=" + this.ID + ">" + this.Name + "</option>");
    });
}

///End Test List



function LoadAdminProgramDetails() {
    $.ajax({
        url: "/Dashboard/LoadProgramsForAdmin",
        type: "POST",
        success: ProgramDetailsLoaded
    });

}
