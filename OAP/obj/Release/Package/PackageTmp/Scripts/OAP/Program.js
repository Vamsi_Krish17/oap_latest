﻿$(function () {
    $(".chkAll").change(function () {
        if ($(this).prop("checked")) {
            $(".chkSeperate").prop("checked", true);
        }
        else {
            $(".chkSeperate").prop("checked", false);
        }
    });
});


function OpenDeleteModal() {
    if ($(".chkSeperate:checked").length <= 0) {
        ShowAlert("Please select atleast one record to delete.");
    }
    else {
        OpenModal("deleteModal");
    }
}

function DeleteProgram() {
    var selectedPrograms = "";
    $(".chkSeperate:checked").each(function () {
        if (selectedPrograms == "")
            selectedPrograms = $(this).val();
        else
            selectedPrograms = selectedPrograms + "," + $(this).val();
    });

    if (selectedPrograms != "") {                                                   //Students selected
        OpenModal("loadingModal");
        $.ajax({
            url: "/ProgramWizard/Delete",
            type: "POST",
            data: {
                selectedPrograms: selectedPrograms,
            },
            success: DeleteProgramSuccess
        });
    }
    else {                                                                          //Students not selected
        ShowAlert("Please select atleast one record to delete.");
    }
}

function DeleteProgramSuccess(response)
{
    if (response.toLowerCase() == "true") {
        ShowAlert("Program delete successfully");
    }
    else {
        ShowAlert("Program not delete please try again later.");
    }
    $(".chkAll").prop("checked", false);
    BindProgramDataTable();
}