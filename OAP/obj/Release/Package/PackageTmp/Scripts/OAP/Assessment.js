﻿$(function () {
    if ($(".ckhHasInstructionsCheckBox").prop('checked')) {
        $(".instructionsContainer").slideDown("slow");
    }
    else {
        $(".instructionsContainer").hide();
    }
    $(".ckhHasInstructionsCheckBox").change(function () {
        if ($(this).prop("checked")) {
            $(".instructionsContainer").slideDown("slow");
        }
        else {

            $(".instructionsContainer").slideUp();
        }
    });

   
});
function ShowDeleteModal(obj, sectionID) {
    var sectionName = $(obj).parent().children(".tdSectionName").html();
    $(".sectionNameContainer").html(sectionName);
    $('#DeleteSectionModal').modal({
        show: true,
    });
    $(".deleteSection").attr('onclick', 'DeleteSection(' + sectionID + ')');
}
function OpenAlertModal(message) {
    $("#AlertModal").modal({
        show: true,
    });
    $(".alertDetailContainer").html('');
    $(".alertDetailContainer").html(message);
}
function DeleteSection(sectionID) {
    $.ajax({
        url: "/QuestionPaperSection/Delete",
        type: "POST",
        data: {
            idString: sectionID,
        },
        success: DeleteSectionSuccess
    });
}

function DeleteSectionSuccess(response) {
    $(".alertDetailContainer").html(response);
    $('#DeleteSectionModal').modal('hide')
    $('#AlertModal').modal({
        show: true,
    });
}

function ReloadPage() {
    window.location.reload();
}

//QuestionPaper Index

function OpenQuestionPaperDeleteModal() {

    if ($(".chkSeperate:checked").length > 0) {                                           //Sections selected
        OpenModal("deleteModal");
    }
    else {                                                                                  //No Sections selected
        ShowAlert("Please select atleast one record to delete");
    }
}

function DeleteQuestionPaper() {

    var selectedQuestionPapers = $(".chkSeperate:checked").map(function () {
        return $(this).val();
    }).get();
    if (selectedQuestionPapers != "") {                                                       //Sections selected
        OpenModal("loadingModal");
        $.ajax({
            url: "/QuestionPaper/DeleteQuestionPapers",
            type: "POST",
            data: {
                assessmentIDString: selectedQuestionPapers.toString(),
            },
            success: DeleteQuestionPaperSuccess
        });
    }
    else {                                                                              //No branches selected
        ShowAlert("Please select atleast one record to delete");
    }
}

function DeleteQuestionPaperSuccess(response) {
    if (response.toLowerCase() == "true") {
        ShowAlert("Question paper deleted successfully");
    }
    else {
        ShowAlert("Question paper not deleted. Please try again later.");
    }
    BindQuestionPaperDataTable();
}

function OpenPopupModal(ID) {
    $(".questionPaperDownloadID").val(ID);
                                            //Sections selected
        OpenModal("popupModal");
    
  
}

//End QuestionPaper Index
