﻿$(function () {
    //Excel file

    $(".xl").change(function () {
        var fileExtension = ['xlsx', 'xls'];
        var fsize = $('.massUploadFile')[0].files[0].size;
        if (fsize > 4194304) {
            $(".uploadFileSizeError").html("Upload a file less than 4mb");
            $('.massUploadFile').val('');
            return;
        }
        else if ($.inArray($(this).val() != "")) {
            var result = true;
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                result = false;
            }
            else {
                result = true;
            }

            if (!result) {
                $('.ImageError').html("Only 'xlsx or xls' formats are allowed.");
                $('.massUploadFile').val('');
            }
            else {
                $('.ImageError').html('');
            }
        }
        else {
            $('.ImageError').html('');
            $(".uploadFileSizeError").html("");
        }
        $('.titleError').html('');
    });

    //End Excel file

    //Image file

    $(".imageFile").change(function () {
        var fileExtension = ['jpg', 'jpeg','png'];
        var fsize = $('.imageFile')[0].files[0].size;
        if (fsize > 4194304) {
            $(".ImageError").html("Upload a file less than 4mb");
            //$('.imageFile').val('');
            return;
        }
        else if ($.inArray($(this).val() != "")) {
            var result = true;
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                result = false;
            }
            else {
                result = true;
            }

            if (!result) {
                $('.ImageError').html("Only 'jpg or jpeg' or 'png' formats are allowed.");
                $('.imageFile').val('');
            }
            else {
                $('.ImageError').html('');
            }
        }
        else {
            $('.ImageError').html('');
            //$(".uploadFileSizeError").html("");
        }
        //$('.titleError').html('');
    });

    //End Image File



    //Document File

    $(".doc").change(function () {
        var fileExtension = ['doc', 'docx'];
        var fsize = $('.doc')[0].files[0].size;
        if (fsize > 4194304) {
            $(".uploadFileSizeError").html("Upload a file less than 4mb");
            $('.doc').val('');
            return;
        }
        else if ($.inArray($(this).val() != "")) {
            var result = true;
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                result = false;
            }
            else {
                result = true;
            }
            if (!result) {
                $('.ImageError').html("Only 'doc or docx' formats are allowed.");
                $('.doc').val('');
            }
            else {
                $('.ImageError').html('');
            }
        }
        else {
            $('.ImageError').html('');
        }
        $('.titleError').html('');
    });

    //End document file

    //Excel File
    $(".xlsx").change(function () {
        var fileExtension = ['xlsx', 'csv'];
        var fsize = $('.xlsx')[0].files[0].size;
        if (fsize > 4194304) {
            $(".uploadFileSizeError").html("Upload a file less than 4mb");
            $('.xlsx').val('');
            return;
        }
        else if ($.inArray($(this).val() != "")) {
            var result = true;
            if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                result = false;
            }
            else {
                result = true;
            }
            if (!result) {
                $('.ImageError').html("Only 'xlsx or xls' formats are allowed.");
                $('.xlsx').val('');
            }
            else {
                $('.ImageError').html('');
            }
        }
        else {
            $('.ImageError').html('');
        }
        $('.titleError').html('');
    });
    //End Excel File

});