﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Models.ViewModels
{
    public class AnswerViewModel
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public string InputParam { get; set; }
        public Nullable<int> QuestionID { get; set; }
        public string OutputParam { get; set; }
    }
}
