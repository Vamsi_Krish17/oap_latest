﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OAP.Models.ViewModels
{
    public class CodingLanguageModel
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}