﻿using OAP.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OAP.Models.ViewModels
{
    public class QuestionPaperViewModel
    {
        public int ID { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public int TimeLimit { get; set; }
        public bool DisplayResult { get; set; }
        public bool DisplayMarks { get; set; }
        public bool TestComplete { get; set; }
        public bool NoPermission { get; set; }
        public bool IsSectionJump { get; set; }
        public TimeSpan TimeLimitTimeSpan { get; set; }
        public SectionViewModel CurrentSection { get; set; }
        public List<SectionViewModel> SectionList { get; set; }
        public List<ColorCodeViewModel> ColorCodeViewModelList { get; set; }
        public double RemainingSeconds { get; set; }

        public DateTime StartTime { get; set; }

        public string SavedAnswers { get; set; }

        public int CurrentSectionIndex { get; set; }

        public string Instructions { get; set; }

        public bool ShowInstruction { get; set; }

        public int CurrentQuestionNumber { get; set; }
        //public List<string> SectionList { get; set; }

        public bool CanReview { get; set; }

        public bool IsShowQuestionNumber { get; set; }

        public bool IsShowSection { get; set; }
        public string ShowQuestionNumber { get; set; }
        public string ShowSection { get; set; }
    }
}