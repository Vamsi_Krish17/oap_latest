﻿using OAP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Models.ViewModels
{
    public class QuestionViewModel
    {
        public int ID { get; set; }
        public int? PassageID { get; set; }
        public string Description { get; set; }
        public bool IsDescriptive { get; set; }

        public List<AnswerViewModel> AnswerList { get; set; }

        public PassageViewModel PassageViewModel { get; set; }

        public string QuestionType { get; set; }

        public int QuestionTypeID { get; set; }
    }
}
