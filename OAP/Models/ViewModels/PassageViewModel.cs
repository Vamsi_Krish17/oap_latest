﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OAP.Models.ViewModels
{
    public class PassageViewModel
    {

        public int ID { get; set; }

        public string Description { get; set; }

        public DateTime LastUpdated { get; set; }

        public List<QuestionViewModel> QuestionViewModelList { get; set; }

        public bool CanShuffle { get; set; }

        public string AnswerOrder { get; set; }

    }
}