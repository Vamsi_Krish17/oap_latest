﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OAP.Models.ViewModels
{
    public class TechnicalQuestionModel
    {
        public List<CodingLanguageModel> ListOfLanguages { get;set;}

        public List<QuestionsModel> questionsModel { get; set; }
    }
}