﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OAP.Models.ViewModels
{
    public class QuestionPaperMinimalViewModel
    {
        public long QuestionPaperID { get; set; }
        public string QuestionPaperName { get; set; }
    }
}