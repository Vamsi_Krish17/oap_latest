﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OAP.Models.ViewModels
{
    public class SectionViewModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int TimeLimit { get; set; }

        public int QuestionPaperID { get; set; }
        public string QuestionPaperCode { get; set; }
        public string QuestionPaperName { get; set; }

        public List<QuestionAnswerViewModel> QuestionList { get; set; }
    }
}