﻿using OAP.Domain.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OAP.Models.ViewModels
{
    public class QuestionAnswerViewModel
    {
        public int ID { get; set; }
        public string Description { get; set; }
        public QuestionViewModel Question { get; set; }
        public OAP.Domain.ViewModel.TechnicalQuestionViewModel TechnicalQuestion { get; set; }
    }
}