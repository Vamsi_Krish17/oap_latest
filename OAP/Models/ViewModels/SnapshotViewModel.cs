﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OAP.Models.ViewModels
{
    public class SnapshotViewModel
    {
        public int QuestionID { get; set; }
        public int AnswerID { get; set; }
    }
}