﻿using OAP.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OAP.Models.ViewModels
{
    public class TechnicalQuestionViewModel
    {
        public int ID { get; set; }
        public int? PassageID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Nullable<int> QuestionTypeID { get; set; }
        public string Languages { get; set; }
        public string SolutionType { get; set; }

        public List<TechnicalAnswerViewModel> AnswerList { get; set; }

        public PassageViewModel PassageViewModel { get; set; }

        public string QuestionType { get; set; }
    }
}