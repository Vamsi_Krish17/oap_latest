﻿function MappedCollegeSelectionChanged(obj) {
    selectedMappedColleges = "";
    $(".mappedBranches").html("");
    $(".mappedSections").html("");
    $("#mappedBranchList").select2({
        placeholder: "Select Branch",
        allowClear: true
    });
    $("#mappedSectionList").select2({
        placeholder: "Select Sections",
        allowClear: true
    });

    $('.optMappedCollege:selected').each(function () {
        if (selectedMappedColleges == "") {
            selectedMappedColleges = $(this).val();
        }
        else {
            selectedMappedColleges = selectedMappedColleges + "," + $(this).val();
        }
    });

    //if (selectedMappedColleges != "") {
        $.ajax({
            url: "/Batch/GetSectionsYearByCollege",
            type: "POST",
            data:
                {
                    collegeIDString: selectedMappedColleges
                },
            success: SectionYearLoaded
        });
    //}
}

function MappedSectionSelectionChanged(obj) {
    selectedMappedSections = "";
    $('.optMappedSection:selected').each(function () {
        if (selectedMappedSections == "") {
            selectedMappedSections = $(this).val();
        }
        else {
            selectedMappedSections = selectedMappedSections + "," + $(this).val();
        }
    });

    BindSelectedStudents();
}

function MappedBranchSelectionChanged(obj) {
    selectedMappedBranches = "";
    $('.optMappedBranch:selected').each(function () {
        if (selectedMappedBranches == "") {
            selectedMappedBranches = $(this).val();
        }
        else {
            selectedMappedBranches = selectedMappedBranches + "," + $(this).val();
        }
    });
    BindSelectedStudents();
}

function UnMappedSectionSelectionChanged(obj) {
    selectedUnMappedSections = "";
    $('.optUnMappedSection:selected').each(function () {
        if (selectedUnMappedSections == "") {
            selectedUnMappedSections = $(this).val();
        }
        else {
            selectedUnMappedSections = selectedUnMappedSections + "," + $(this).val();
        }
    });
    BindAvailableStudents();
}

function UnMappedBranchSelectionChanged(obj) {
    selectedUnMappedBranches = "";
    $('.optUnMappedBranch:selected').each(function () {
        if (selectedUnMappedBranches == "") {
            selectedUnMappedBranches = $(this).val();
        }
        else {
            selectedUnMappedBranches = selectedUnMappedBranches + "," + $(this).val();
        }
    });
    BindAvailableStudents();
}

function SectionYearLoaded(data) {
    var items = [];
    items.push(ConverDataToArrayForMappedBranch(data));
    $(".mappedBranches").html(items.join(''));

    items = [];
    items.push(ConverDataToArrayForMappedSection(data));
    $(".mappedSections").html(items.join(''));
    BindSelectedStudents();
}

function ConverDataToArrayForMappedBranch(data) {
    var items = [];
    data = JSON.parse(data);
    $.each(data.BranchViewModelList, function () {
        items.push("<option class=optMappedBranch value=" + this.ID + ">" + this.Name + "</option>");
    });
    return items;
}

function ConverDataToArrayForMappedSection(data) {
    var items = [];
    data = JSON.parse(data);
    $.each(data.SectionViewModelList, function () {
        items.push("<option class=optMappedSection  value=" + this.ID + ">" + this.Name + "</option>");
    });

    return items;
}

function UnMappedCollegeSelectionChanged() {
    selectedUnMappedColleges = "";
    $(".unMappedBranches").html("");
    $(".unMappedSections").html("");
    $("#unMappedBranchList").select2({
        placeholder: "Select Branch",
        allowClear: true
    });
    $("#unMappedSectionList").select2({
        placeholder: "Select Sections",
        allowClear: true
    });

    $('.optUnMappedCollege:selected').each(function () {
        if (selectedUnMappedColleges == "") {
            selectedUnMappedColleges = $(this).val();
        }
        else {
            selectedUnMappedColleges = selectedUnMappedColleges + "," + $(this).val();
        }
    });

    //if (selectedUnMappedColleges != "") {
        $.ajax({
            url: "/Batch/GetSectionsYearByCollege",
            type: "POST",
            data:
                {
                    collegeIDString: selectedUnMappedColleges
                },
            success: UnMappedSectionYearLoaded
        });
   // }
}

function UnMappedSectionYearLoaded(data) {
    var items = [];
    items.push(ConverDataToArrayForUnMappedBranch(data));
    $(".unMappedBranches").html(items.join(''));

    items = [];
    items.push(ConverDataToArrayForUnMappedSection(data));
    $(".unMappedSections").html(items.join(''));
    BindAvailableStudents();
}

function ConverDataToArrayForUnMappedBranch(data) {
    var items = [];
    data = JSON.parse(data);
    $.each(data.BranchViewModelList, function () {
        items.push("<option class=optUnMappedBranch value=" + this.ID + ">" + this.Name + "</option>");
    });
    return items;
}

function ConverDataToArrayForUnMappedSection(data) {
    var items = [];
    data = JSON.parse(data);
    $.each(data.SectionViewModelList, function () {
        items.push("<option class=optUnMappedSection  value=" + this.ID + ">" + this.Name + "</option>");
    });

    return items;
}

function test() {
    //$.each(data.SectionViewModelList, function () {
    //    items.push("<option value=" + this.ID + ">" + this.Name + "</option>");
    //});
}

//Unmapping students

function UnMapSelectedStudents(batchID) {
    var selectedStudent = "";
    $(".chkMapped:checked").each(function () {
        if (selectedStudent == "") {
            selectedStudent = $(this).attr("id");
        }
        else {
            selectedStudent = selectedStudent + "," + $(this).attr("id");
        }
    });
    if (batchID != "" && selectedStudent != "") {
        $.ajax({
            url: "/Batch/UnMapStudents",
            type: "POST",
            data: {
                batchIDString: batchID,
                selectedStudentsString: selectedStudent,
            },
            success: StudentsMapped
        });
    }
    else {
        alert("Please select atleast one student");
    }
}

function StudentsMapped(response) {
    $(".chkUnMapped").prop("checked", false);
    $(".chkMapped").prop("checked", false);
    BindSelectedStudents();
    BindAvailableStudents();
}

//Mapping students

function MapSelectedStudents(batchID) {
    var selectedStudent = "";
    $(".chkUnMapped:checked").each(function () {
        if (selectedStudent == "") {
            selectedStudent = $(this).attr("id");
        }
        else {
            selectedStudent = selectedStudent + "," + $(this).attr("id");
        }
    });
    if (selectedStudent != "" && batchID != "") {
        $.ajax({
            url: "/Batch/MapStudents",
            type: "POST",
            data: {
                batchIDString: batchID,
                selectedStudentsString: selectedStudent,
            },
            success: StudentsUnMapped
        });
    }
    else {

        alert("Please select the dates");
    }
}

function StudentsUnMapped() {
    $(".chkUnMapped").prop("checked", false);
    $(".chkMapped").prop("checked", false);
    BindSelectedStudents();
    BindAvailableStudents();
}

//Batch QuestionPapers

//UnMapSelectedQuestionPapers

function UnMapSelectedQuestionPapers(batchID) {
    var selectedQuestionPapers = "";
    $(".chkMappedQuestionPapers:checked").each(function () {
        if (selectedQuestionPapers == "")
            selectedQuestionPapers = $(this).attr("id");
        else
            selectedQuestionPapers = selectedQuestionPapers + "," + $(this).attr("id");
    });

    if (selectedQuestionPapers != "") {
        $.ajax({
            url: "/Batch/UnMapAssessments",
            type: "POST",
            data: {
                batchIDString: batchID,
                selectedAssessments: selectedQuestionPapers,
            },
            success: QuestionPapersUnmappedSuccessfully
        });
    }
    else {
        alert("Please select atleast one assessment");
    }
}

function QuestionPapersUnmappedSuccessfully(response) {
    if (response == "False") {
        ShowAlert('Something went wrong.Please try after sometime');
    }
    else {
        ShowAlert('Question papers successfully unmapped.');

        BindSelectedQuestionPapers();
        BindAvailableQuestionPapers();
        $(".chkUnMappedQuestionPapers").prop("checked", false);
        $(".chkMappedQuestionPapers").prop("checked", false);
    }
}

//MapSelectedQuestionPapers

function MapSelectedQuestionPapers(batchID) {
    var selectedQuestionPapers = "";
    var startDateString = $(".txtStartDateString").val();
    var endDateString = $(".txtEndDateString").val();

    startDateString = startDateString.replace("-", "").replace("  ", " ");
    endDateString = endDateString.replace("-", "").replace("  ", " ");

    $(".chkUnMappedQuestionPapers:checked").each(function () {
        if (selectedQuestionPapers == "")
            selectedQuestionPapers = $(this).attr("id");
        else
            selectedQuestionPapers = selectedQuestionPapers + "," + $(this).attr("id");
    });
    if (selectedQuestionPapers != "" && batchID != "" && startDateString != "" && endDateString != "") {
        $.ajax({
            url: "/Batch/MapAssessments",
            type: "POST",
            data: {
                batchIDString: batchID,
                selectedAssessments: selectedQuestionPapers,
                startDateString: startDateString,
                endDateString: endDateString,
            },
            success: QuestionPapersmappedSuccessfully
        });
    }
    else {
        if (selectedQuestionPapers == "" || batchID == "") {
            alert("Please select atleast one assessment");
        }
        else if (startDateString == "" || endDateString == "") {
            alert("Please select the dates");
        }
    }
}

function QuestionPapersmappedSuccessfully(response) {
    if (response == "False") {
        ShowAlert('Something went wrong.Please try after sometime');
    }
    else {
        ShowAlert('Question papers successfully mapped.');

        BindSelectedQuestionPapers();
        BindAvailableQuestionPapers();
        $(".chkUnMappedQuestionPapers").prop("checked", false);
        $(".chkMappedQuestionPapers").prop("checked", false);
    }
}

//Batch Index

function OpenBatchDeleteModal() {

    //var selectedBatches = $(".chkSeperate:checked").map(function () {
    //    return $(this).val();
    //}).get();

    if ($(".chkSeperate:checked").length > 0) {                                           //Batches selected
        OpenModal("deleteModal");
    }
    else {                                                                                  //No batches selected
        ShowAlert("Please select atleast one record to delete");
    }
}

function DeleteBatch() {

    var selectedBatches = $(".chkSeperate:checked").map(function () {
        return $(this).val();
    }).get();
    if (selectedBatches != "") {                                                       //Branches selected
        OpenModal("loadingModal");

        $.ajax({
            url: "/Batch/DeleteBatches",
            type: "POST",
            data: {
                batchIDString: selectedBatches.toString(),
            },
            success:DeleteBatchSuccess
        });
    }
    else {                                                                              //No branches selected
        ShowAlert("Please select atleast one record to delete");
    }
}

function DeleteBatchSuccess(response)
{
    if (response.toLowerCase() == "true") {
        ShowAlert("Batch delete successfully");
    }
    else {
        ShowAlert("Batch not delete please try again later.");
    }
    $(".chkAll").prop("checked", false);
    BindBatchDataTable();
}


//End Batch Index
