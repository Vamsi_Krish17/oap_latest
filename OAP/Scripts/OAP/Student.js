﻿//function CollegeChanged()
//{
//    var collegeID = $(".ddlCollege").val();
//    window.location.href = "/Student/Create/"+collegeID;
//}

$(function () {
    $(".chkAll").change(function () {
        if ($(this).prop("checked")) {
            $(".chkSeperate").prop("checked", true);
        }
        else {
            $(".chkSeperate").prop("checked", false);
        }
    });
});

function CollegeChanged() {
    var collegeID = $(".ddlCollege").val();
    if (collegeID != "" && collegeID != null) {
        AllowMassUpload();
        $.ajax({
            type: 'POST',
            url: "/College/GetSectionsBatchesUnderCollege",
            data: {
                collegeIDString: collegeID,
            },
            success: SectionsBatchesLoaded
        });
    }
    else {
        var items = [];
        items.push("<option value='0'>Select</option>");
        $(".ddlSection").html(items.join(''));
        $(".ddlBranch").html(items.join(''));
    }
}

function SectionsBatchesLoaded(data) {
    //alert(showAllOption);
    var items = [];
    items.push("<option value='0'>Select</option>");
    items.push(ConverDataToArrayForSection(data));
    $(".ddlSection").html(items.join(''));
    items = [];
    items.push("<option value='0'>Select</option>");
    items.push(ConverDataToArrayForBranch(data));
    $(".ddlBranch").html(items.join(''));
    items = [];
    items.push("<option value=''>Select</option>");
    items.push(ConverDataToArrayForBatch(data));
    items.push("<option value='M'>Multiple</option>");
    $(".ddlBatch").html(items.join(''));
}

function ConverDataToArrayForSection(data) {
    var items = [];
    data = JSON.parse(data);
    $.each(data.SectionViewModelList, function () {
        items.push("<option value=" + this.ID + ">" + this.Name + "</option>");
    });

    return items;
}

function ConverDataToArrayForBranch(data) {
    var items = [];
    data = JSON.parse(data);
    $.each(data.BranchViewModelList, function () {
        items.push("<option value=" + this.ID + ">" + this.Name + "</option>");
    });

    return items;
}

function ConverDataToArrayForBatch(data) {
    var items = [];
    data = JSON.parse(data);
    $.each(data.BatchViewModelList, function () {
        items.push("<option value=" + this.ID + ">" + this.Name + "</option>");
    });

    return items;
}

function GetEndYear() {
    var branchID = $(".ddlBranch").val();
    var startYearString = $(".ddlStartYear").val();
    if (branchID == "" || branchID == null) {
        OpenAlertModal('Please Select any one Branch');
    }
   else if (startYearString == "" || startYearString == null) {
        OpenAlertModal('Please Select start year');
    }
    else {
        if (branchID != "" && branchID != null && startYearString != null && startYearString != "") {
            $.ajax({
                type: 'POST',
                url: "/Branch/GetEndYearRangeByBranch",
                data: {
                    branchIDString: branchID,
                    startYearString: startYearString,
                },
                success: EndYearLoaded
            });
        }
    }
    //else {
    //    //if (branchID == "" || branchID == null)
    //    //{
            
    //    //}
    //}
}

function EndYearLoaded(response) {
    var items = [];
    items.push("<option value='0'>Select</option>");
    //items.push(ConverDataToArray(response));

    var selected = "";

    var data = JSON.parse(response);
    $.each(data, function () {
        //alert(this.Selected);
        if (this.Selected || this.Selected == "true") {
            selected = this.Value;
        }
        items.push("<option value=" + this.Value + ">" + this.Text + "</option>");
    });
    $(".ddlEndYear").html(items.join(''));
    $(".ddlEndYear").val(selected);
}

function ConverDataToArray(data) {
    var items = [];
    data = JSON.parse(data);
    $.each(data, function () {
        if (this.Selected == "true") {
            selected = this.Value;
        }
        items.push("<option value=" + this.Value + ">" + this.Text + "</option>");
    });
    return items;
}

function AllowMassUpload() {
    //if ($("#CollegeID").val() != "" && $("#SectionID").val() != "" && $("#BranchID").val() != "0" ) {
    if ($("#CollegeID").val() != "" && $("#CollegeID").val() != "0") {
        $("#file").prop('disabled', false);
    }
    else {
        $("#file").prop('disabled', true);
    }
}

function OpenAlertModal(message) {
    $("#AlertModal").modal({
        show: true,
    });
    $(".alertDetailContainer").html('');
    $(".alertDetailContainer").html(message);
}
/// Student Index start

function OpenDeleteModal()
{
    if ($(".chkSeperate:checked").length <= 0) {
        ShowAlert("Please select atleast one record to delete.");
    }
    else {
        OpenModal("deleteModal");
    }
}

function OpenPasswordResetModal() {
    if ($(".chkSeperate:checked").length <= 0) {
        ShowAlert("Please select atleast one student.");
    }
    else {
        OpenModal("resetPasswordModal");
    }
}

function OpenResumeDownloadModal() {
    if ($(".chkSeperate:checked").length <= 0) {
        ShowAlert("Please select atleast one student.");
    }
    else {
        // OpenModal("resetPasswordModal");
        DownloadStudentResumes();
    }
}
function DownloadStudentResumes() {
    var selectedStudents = "";
    $(".chkSeperate:checked").each(function () {
        if (selectedStudents == "")
            selectedStudents = $(this).val();
        else
            selectedStudents = selectedStudents + "," + $(this).val();
    });
    if (selectedStudents != "" ) {                                                   //Students selected
        OpenModal("loadingModal");
        $.ajax({
            url: "/Student/DownloadStudentResumes",
            type: "POST",
            data: {
                selectedStudents: selectedStudents,
            },
            success: ResetStudentPasswordSuccess
        });
    }
    else {                                                                          //Students not selected
        ShowAlert("Please select atleast one student.");
    }
}


function MatchPassword() {
    if ($(".password").val() != "" && $(".confirmPassword").val() != "") {
        if ($(".password").val() != $(".confirmPassword").val()) {
            ShowAlert("Password does not match");
        }
    }
    else if ($(".password").val() == "") {
        ShowAlert("Password shouldn't be empty");
    }
    //else {
    //    ShowAlert("Confirm Password shouldn't be empty");
    //}
}


function DeleteStudent()
{
    var selectedStudents = "";
    $(".chkSeperate:checked").each(function () {
        if (selectedStudents == "")
            selectedStudents = $(this).val();
        else
            selectedStudents =selectedStudents +","+ $(this).val();
    });

    if (selectedStudents != "") {                                                   //Students selected
        OpenModal("loadingModal");
        $.ajax({
            url: "/Student/Delete",
            type: "POST",
            data: {
                selectedStudents:selectedStudents,
            },
            success: DeleteStudentSuccess
        });
    }
    else {                                                                          //Students not selected
        ShowAlert("Please select atleast one record to delete.");
    }
}

function DeleteStudentSuccess(response)
{
    if (response.toLowerCase() == "true") {
        ShowAlert("Student deleted successfully");
    }
    else {
        ShowAlert("Could not delete Student. Please try again later.");
    }
    $(".chkAll").prop("checked", false);
    BindStudentDataTable();
}

function FilterChanged()
{

    selectedBranch = $(".ddlBranch").val();

    BindStudentDataTable();
}

function OpenModal(modalName) {
    $(".modal").modal('hide');
    $("#" + modalName).modal({
        show: true,
        backdrop: 'static',
    });
}

function ShowAlert(alertContent) {
    $(".modal").modal('hide');
    $(".alertContent").html(alertContent);
    $("#alertModal").modal({
        show: true,
        backdrop: 'static',
    });
}

//End Student Index Start
function CheckPassword(inputtxt) {
    var passw = /^[A-Za-z]\w{7,14}$/;
    if (inputtxt.value.match(passw)) {
        alert('Correct, try another...')
        return true;
    }
    else {
        alert('Please enter a valid password.')
        return false;
    }
}

function ValidateEmail(inputText) {
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (inputText.value.match(mailformat)) {
        document.form1.text1.focus();
        return true;
    }
    else {
        alert("You have entered an invalid email address!");
        document.form1.text1.focus();
        return false;
    }
}

function ResetStudentPassword() {
    var selectedStudents = "";
    $(".chkSeperate:checked").each(function () {
        if (selectedStudents == "")
            selectedStudents = $(this).val();
        else
            selectedStudents = selectedStudents + "," + $(this).val();
    });
    var password = $(".password").val();
    if (selectedStudents != "" && $(".password").val() != "" && $(".confirmPassword").val() != "") {                                                   //Students selected
        OpenModal("loadingModal");
        $.ajax({
            url: "/Student/ResetStudentPassword",
            type: "POST",
            data: {
                selectedStudents: selectedStudents,
                password: password,
            },
            success: ResetStudentPasswordSuccess
        });
    }
    else {                                                                          //Students not selected
        ShowAlert("Please select atleast one student.");
    }
}

function ResetStudentPasswordSuccess(response) {
    if (response.toLowerCase() == "true") {
        ShowAlert("Students password reseted successfully");
    }
    else {
        ShowAlert("Could not reset password. Please try again later.");
    }
    $(".chkAll").prop("checked", false);
    BindStudentDataTable();
}
