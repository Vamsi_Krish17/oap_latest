﻿

$(function () {
    $(".chkAll").change(function () {
        if ($(this).prop("checked")) {
            $(".chkSeperate").prop("checked", true);
        }
        else {
            $(".chkSeperate").prop("checked", false);
        }
    });
});

//Subject Index

function OpenDeleteModal()
{
    if ($(".chkSeperate:checked").length <= 0) {
        ShowAlert("Please select atleast one record to delete");
    }
    else
    {
        OpenModal("deleteModal");
    }
}

function DeleteMember()
{
    var selectedMembers = "";
    $(".chkSeperate:checked").each(function () {
        if (selectedMembers == "") {
            selectedMembers = $(this).val();
        }
        else {
            selectedMembers =selectedMembers +","+ $(this).val();
        }
    });

    if (selectedMembers == "") {
        ShowAlert("Please select atleast one record to delete");
    }
    else {
        OpenModal("loadingModal");
        $.ajax({
            url: "/Member/Delete",
            type: "POST",
            data: {
                memberIDString:selectedMembers,
            },
            success:DeleteMemberSuccess
        });
    }
}

function DeleteMemberSuccess(response)
{
    if (response.toLowerCase() == "true") {
        ShowAlert("Member successfully deleted");
    }
    else {
        ShowAlert("Some error occured. Please try again.");
    }
    $(".chkAll").prop("checked", false);
    BindMemberTable();
}


//End Member Index
    function OpenModal(modalName) {
        $(".modal").modal('hide');
        $("#" + modalName).modal({
            show: true,
            backdrop: 'static',
        });
    }

    function ShowAlert(alertContent) {
        $(".modal").modal('hide');
        $(".alertContent").html(alertContent);
        $("#alertModal").modal({
            show: true,
            backdrop: 'static',
        });
    }