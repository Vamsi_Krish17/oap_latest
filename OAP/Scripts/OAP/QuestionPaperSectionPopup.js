﻿var selectedQuestionsChecked = new Array();
var availableQuestionsChecked = new Array();

var selectedTags = '';
var selectedTagsForMappedQuestions = new Array();
var selectedTagsForUnMappedQuestions = new Array();

var message = "";

$(function () {

    $(".collapsible").click();

    $(".chkAllMapped").change(function () {
        if ($(this).prop("checked")) {
            $(".chkMappedQuestions").prop("checked", true);
        }
        else {
            $(".chkMappedQuestions").prop("checked", false);
        }
    });

    $(".chkAllUnMapped").change(function () {
        if ($(this).prop("checked")) {
            $(".chkUnMappedQuestions").prop("checked", true);
        }
        else {
            $(".chkUnMappedQuestions").prop("checked", false);
        }
    });



    //Tags all clicked for mappedtable

    $(".chkAllTagsMappedTable").change(function () {
        //selectedTags = '1';
        if ($(this).prop("checked")) {
            $(".chkTagsMappedTable").prop("checked", true);
            $(".chkTagsMappedTable:checked").each(function () {
                selectedTagsForMappedQuestions.push($(this).attr("id"));
            });
        }
        else {
            $(".chkTagsMappedTable").prop("checked", false);
            selectedTagsForMappedQuestions = new Array();
        }
        BindSelectedQuestions();
    });

    //Seperate tag clicked for mappedtable

    $(".chkTagsMappedTable").change(function () {
        if ($(this).prop("checked")) {
            selectedTagsForMappedQuestions.push($(this).attr("id"));
        }
        else {
            RemoveTagIDFromMappedList($(this).attr("id"));
        }
        BindSelectedQuestions();
    });

    //Tags all clicked for Un Mappedtable

    $(".chkAllTagsUnMappedTable").change(function () {
        //selectedTags = '1';
        if ($(this).prop("checked")) {
            $(".chkTagsUnMappedTable").prop("checked", true);
            $(".chkTagsUnMappedTable:checked").each(function () {
                selectedTagsForUnMappedQuestions.push($(this).attr("id"));
            });
        }
        else {
            $(".chkTagsUnMappedTable").prop("checked", false);
            selectedTagsForUnMappedQuestions = new Array();
        }
        BindAvailableQuestions();
    });

    //Seperate tag clicked for Un Mappedtable

    $(".chkTagsUnMappedTable").change(function () {
        if ($(this).prop("checked")) {
            selectedTagsForUnMappedQuestions.push($(this).attr("id"));
        }
        else {
            RemoveTagIDFromUnMappedList($(this).attr("id"));
        }
        BindAvailableQuestions();
    });


    BindSelectedQuestions();
    BindAvailableQuestions();

    if (message != undefined && message != "") {
        OpenAlertModal(message);
        message = "";
    }

});


function SelectedFunctionQuestionsChanged() {

}

function UnMapSelectedQuestions(assessmentSectionID) {
    var selectedQuestionsIDs = "";
    $(".chkMappedQuestions:checked").each(function () {
        if (selectedQuestionsIDs == "") {
            selectedQuestionsIDs = $(this).attr("id");
        }
        else {
            selectedQuestionsIDs = selectedQuestionsIDs + "," + $(this).attr("id");
        }
    });
    if (selectedQuestionsIDs == "") {
        alert("Please select atleast one question");
    }
    else {
        //OpenAlertModal('Question removed from the section');
        $.ajax({
            url: "/QuestionPaperSection/UnMapQuestions",
            type: "POST",
            data: {
                questionIDString: selectedQuestionsIDs,
                assessmentSectionIDString: assessmentSectionID
            },
            success: function (response) {
                QuestionPaperSectionUnMapped(response);
            },
        });
    }
}
function QuestionPaperSectionUnMapped(response) {
    OpenAlertModal('Question removed from the section');
    setTimeout(function () {
        window.location.reload();

    }, 1000);

}

function MapSelectedQuestions(assessmentSectionID) {
    var selectedQuestionsIDs = "";
    $(".chkUnMappedQuestions:checked").each(function () {
        if (selectedQuestionsIDs == "") {
            selectedQuestionsIDs = $(this).attr("id");
        }
        else {
            selectedQuestionsIDs = selectedQuestionsIDs + "," + $(this).attr("id");
        }
    });
    if (selectedQuestionsIDs == "") {
        alert("Please select atleast one question");
    }
    else {

        $.ajax({
            url: "/QuestionPaperSection/MapQuestions",
            type: "POST",
            data: {
                questionIDString: selectedQuestionsIDs,
                assessmentSectionIDString: assessmentSectionID
            },
            success: function (response) {

                QuestionPaperSectionMapped(response);
            },
        });
    }
}

function QuestionPaperSectionMapped(response) {
    OpenAlertModal('Question added to the section');
    setTimeout(function () {
        window.location.reload();
    }, 1000);
}

//View answer Detail

function ViewAnswerDetail(questionID) {
    $.ajax({
        url: "/Question/GetQuestionDetail",
        data: {
            id: questionID
        },
        success: QuestionDetailsLoaded
    });
}

function QuestionDetailsLoaded(response) {
    $(".questionContainer").html(response);
}

function RemoveTagIDFromMappedList(tagID) {
    if ($.inArray(tagID, selectedTagsForMappedQuestions) > -1) {
        var i = selectedTagsForMappedQuestions.indexOf(tagID);
        selectedTagsForMappedQuestions = jQuery.grep(selectedTagsForMappedQuestions, function (value) {
            return value != tagID;
        });
    }
}

function RemoveTagIDFromUnMappedList(tagID) {
    if ($.inArray(tagID, selectedTagsForUnMappedQuestions) > -1) {
        var i = selectedTagsForUnMappedQuestions.indexOf(tagID);
        selectedTagsForUnMappedQuestions = jQuery.grep(selectedTagsForUnMappedQuestions, function (value) {
            return value != tagID;
        });
    }
}



//End View Answer Detail


function BindSelectedQuestions() {
    //if ($.fn.dataTable.isDataTable('#tblSelectedQuestions')) {
    //}
    $('#tblSelectedQuestions').dataTable({
        "destroy": true,
        "processing": true,
        "serverSide": true,
        //'ajax': '/QuestionPaperSection/GetSelectedQuestions?id=@Model.ID&tags=' + selectedTags,
        'ajax': '/QuestionPaperSection/GetSelectedQuestions?id=@Model.ID&tags=' + selectedTagsForMappedQuestions.toString(),
        "pagingType": "full_numbers",
        "columns": [
        { "data": "Description" },
        { "data": "" },
        ],
        "columnDefs": [
             {
                 "targets": [0],
                 "data": "download_link",
                 "render": function (data, type, full) {
                     return '<a href="#questionDetailModal" data-toggle="modal" onclick="ViewAnswerDetail(' + full.ID + ')" class="modalOpen ' + full.ID + '" id=' + full.ID + ' >' + data + '</a>';
                 }
             },
            {
                "targets": [1],
                "data": "download_link",
                "bSortable": false,
                "render": function (data, type, full) {
                    return '<input type="checkbox"  class="chkSelected chkMappedQuestions chk' + full.ID + '" id=' + full.ID + ' />';
                }
            },
        ]
    });
}
function BindAvailableQuestions() {
    //if ($.fn.dataTable.isDataTable('#tblAvailableQuestions')) {
    //}
    $('#tblAvailableQuestions').dataTable({
        "destroy": true,
        "processing": true,
        "serverSide": true,
        //'ajax': '/Question/List',
        'ajax': '/QuestionPaperSection/GetOtherQuestions?id=@Model.ID&tags=' + selectedTagsForUnMappedQuestions.toString(),
        "pagingType": "full_numbers",
        "columns": [
        { "data": "Description" },
        { "data": "" },
        ],
        "columnDefs": [
              {
                  "targets": [0],
                  "data": "download_link",
                  "render": function (data, type, full) {
                      return '<a href="#questionDetailModal" data-toggle="modal" onclick="ViewAnswerDetail(' + full.ID + ')" class="modalOpen ' + full.ID + '" id=' + full.ID + ' >' + data + '</a>';
                  }
              },
            {
                "targets": [1],
                "data": "download_link",
                "bSortable": false,
                "render": function (data, type, full) {
                    //return '<a href="Edit/' + full.ID + '">' + data + '</a>';
                    return '<input type="checkbox" class="chkAvailable chkUnMappedQuestions chk' + full.ID + '" id=' + full.ID + ' />';
                }
            }]
    });
}
function OpenAlertModal(message) {
    $("#AlertModal").modal({
        show: true,
    });
    $(".messageContainer").html('');
    $(".messageContainer").html(message);
}