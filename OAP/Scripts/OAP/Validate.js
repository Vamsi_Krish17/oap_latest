﻿function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;
    return true;
}

function isDecimal(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode!=46)
        return false;    
    return true;
}

function isPhonenumber(classname, evt) {
    var Str = $("." + classname).val();
    var len = Str.length;
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    else {
        if (len <= 9) {
            return true;
        }
        else {
            return false;
        }
    }
}

