﻿function DeleteTags()
{
    var selectedTags = "";
    $(".chkSeperate:checked").each(function () {
        if (selectedTags == "") {
            selectedTags = $(this).attr("id");
        }
        else {
            selectedTags =selectedTags +","+ $(this).attr("id");
        }

     
    });
    if (selectedTags != "") {
        $.ajax({
            url: "/Tag/Delete",
            type: "POST",
            data: {
                idString: selectedTags,
            },
            success: TagsDeleted
        });
    }
    else {
        ShowAlert("Please try after sometime");
    }
}

function TagsDeleted(response)
{
    ShowModal("alertModal");
    $(".alertContent").html("Tag deleted successfully");
    $(".chkAll").prop("checked", false);
    BindDataTable();
}

function OpenDeletePopup()
{
    var selectedTags = "";
    $(".chkSeperate:checked").each(function () {
        if (selectedTags == "") {
            selectedTags = $(this).attr("id");
        }
        else {
            selectedTags = selectedTags + "," + $(this).attr("id");
        }
    });
    if (selectedTags != "") {
        $(".alertContent").html("Are you sure you want to delete these records? Deleting these tags will also delete questions under these tags.");
        ShowModal("deleteConfirmation");

    }
    else {
        $(".alertContent").html("Please select atleast one record to delete");
        ShowModal("alertModal");
    }
}

function ShowModal(modalName)
{
    $("#deleteConfirmation").modal('hide');
    $("#alertModal").modal("hide");

    $("#" + modalName).modal({
        show: true,
        backdrop: 'static',
    });

}