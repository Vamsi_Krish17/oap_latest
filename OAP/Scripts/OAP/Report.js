﻿function OpenQuestionPaperReport(assessmentID) {
    window.open("/Report/StudentQuestionPaperReport/" + assessmentID + "", "_blank");
}
function OpenSectionReport(assessmentID) {
    window.open("/Report/SectionWiseBarGraph/" + assessmentID + "", "_blank");
}
function PerformancePieReport(assessmentID) {
    window.open("/Report/PerformancePieReport/" + assessmentID + "", "_blank");
}
function OpenQuestionWiseAnalysis(assessmentID) {
    window.open("/Report/QuestionWiseAnalysis/" + assessmentID + "", "_blank");
}
//BatchQuestionPaperReport

function LoadQuestionPapersUnderBatch() {
    var batchID = $(".ddlBatch").val();
    if (batchID != "" && batchID != "0") {
        $.ajax({
            url: "/QuestionPaper/GetQuestionPaperByBatch",
            type: "POST",
            data: {
                batchIDString: batchID,
            },
            success: GotQuestionPapers
        });
    }
    else {
        var items = [];
        items.push("<option value='0'>All</option>");
        $(".ddlQuestionPaper").html(items.join(''));
    }
}

function GotQuestionPapers(response) {
    var items = [];
    items.push("<option value='0'>All</option>");
    items.push(ConverDataToArrayForSection(response));
    $(".ddlQuestionPaper").html(items.join(''));
}

function ConverDataToArrayForSection(data) {
    var items = [];
    data = JSON.parse(data);
    $.each(data, function () {
        items.push("<option value=" + this.ID + ">" + this.Name + "</option>");
    });
    return items;
}

function GetQuestionPaperReport() {
    var batchIds = [];
    $('.ddlBatch option').each(function () {
        if ($(this).attr('value') != "") {
            batchIds.push($(this).attr('value'));
        }
    });
    var assessmentIds = [];
    $('.ddlQuestionPaper option').each(function () {
        if ($(this).attr('value') != "") {
            assessmentIds.push($(this).attr('value'));
        }
    });

    var batchID = $(".ddlBatch").val();
    var QuestionIdString = "";
    $('.AssociatedQuestionPaper :selected').each(function (i, selected) {
        if (QuestionIdString == "") {
            QuestionIdString = $(selected).val();
        }
        else {
            QuestionIdString += "," + $(selected).val();
        }
    });
    var assessmentID = QuestionIdString;
    alert(assessmentID);
    if (batchID != "") {
        window.open("/Report/GenerateAssessmentReport?batchID=" + batchID + "&assessmentID=" + assessmentID, "new")
    }
    else {
        window.open("/Report/GenerateAssessmentReport", "new")
    }
    //open new window with report
    //$.ajax({
    //    type: 'POST',
    //    url: "/Report/GenerateQuestionPaperReport",
    //    dataType: 'html',
    //    data: { idString: 1 },
    //    success: function (result) {
    //        var w = window.open();
    //        $(w.document.body).html(result);
    //    }
    //});
}

//End BatchQuestionPaperReport

//Batch Pass percentage report

function LoadBatchesUnderQuestionPapers(obj) {
    var assessmentID = $(obj).val();
    if (assessmentID != "") {
        $.ajax({
            url: "/Batch/GetBatchUnderQuestionPaper",
            type: "POST",
            data: {
                assessmentID: assessmentID,
            },
            success: BatchesUnderQuestionPapersLoaded
        });
    }
    else {
        var items = [];
        items.push("<option value=''>All</option>");
        $(".ddlBatch").html(items.join(''));
    }
}

function BatchesUnderQuestionPapersLoaded(response) {
    var items = [];
    items.push("<option value=''>All</option>");
    items.push(ConverDataToArrayForSection(response));
    $(".ddlBatch").html(items.join(''));
}

function GetBatchPassPercentageReport() {
    var assessmentID = $(".ddlQuestionPaper").val();
    var batchID = $(".ddlBatch").val();
    var selectedBatchString = $(".ddlBatch option:selected").text();

    var selMulti = $.map($(".ddlBatch option:selected"), function (el, i) {
        if ($(el).text().toLowerCase() == "all") {
            selectedBatchString = "all";
            batchID = "all";
        }
        return $(el).text();
    });

    if (assessmentID != "" && assessmentID != undefined && batchID != null) {
        window.open("/Report/GenerateBatchPercentageReport?assessmentID=" + assessmentID + "&selectedBatchesString=" + batchID.toString());

        //$.ajax({
        //    url: "/Report/GenerateBatchPercentageReport",
        //    type: "POST",
        //    data: {
        //        assessmentID: assessmentID,
        //        selectedBatchesString: batchID.toString(),
        //    },
        //    success: GotBatchPassPercentageReport
        //});
    }
    else {                                              //QuestionPaper or batch not selected 
        if (assessmentID == "" ) {
            ShowAlert("Please select the assessment");
        }
        else {
            ShowAlert("Please select the batch");
        }
    }

}

function GotBatchPassPercentageReport(response) {
   
    var win = window.open();
    win.document.write(response);
}
//function GetBatchRadarReport() {
   
//    var college = $(".ddlCollage").val();
//    var batch = $(".ddlBatches").val();

//    //var Batchvalue = "";
//    //$('.AssociatedBatch :selected').each(function (i, selected) {
//    //    if (Batchvalue == "") {
//    //        Batchvalue = $(selected).val();
//    //    }
//    //    else {
//    //        Batchvalue += "," + $(selected).val();

//    //    }
//    //});
//    //$(".batchIDString").val(Batchvalue);


//    //var selectedBatchString = $(".ddlBatches option:selected").text();
//    var selectedCollegeString = $(".ddlCollage option:selected").text();
//    var selMulti = $.map($(".ddlBatches option:selected"), function (el, i) {
//        if ($(el).text().toLowerCase() == "all") {
//            selectedBatchString = "all";
//            batch = "all";
//        }
//        else {
//            if (batch == "") {
//                batch = $(el).text();
//            }
//            else {
//                batch += "," + $(el).text();
//            }
//        }
//    });
//    if (college != "" && college != undefined && batch != null) {
//        window.open("/Report/BatchPassPercentageRadarReport?college=" + college + "&selectedCollegeString=" + batch);
//    }
//    else {                                              //QuestionPaper or batch not selected 
//        if (college == "") {
//            ShowAlert("Please select the assessment");
//        }
//        else {
//            ShowAlert("Please select the batch");
//        }
//    }
//    //new Chart(ctx).Radar(data, { pointDot: false });

//}

//function GotBatchRadarReport() {
//    var win = window.open();
//    win.document.write(response);
//}

function GetBatchRadarReport() {
    var college = $(".ddlCollage").val();    var batch = $(".ddlBatches").val();    var Batchvalue = "";
    $('.ddlBatches :selected').each(function (i, selected) {
        if (Batchvalue == "") {
            Batchvalue = $(selected).val();
        } else {
            Batchvalue += "," + $(selected).val(); }
    });
    // $(".batchIDString").val(Batchvalue);    //var selectedBatchString = $(".ddlBatches option:selected").text();    //var selectedCollegeString = $(".ddlCollage option:selected").text();
    //var selMulti = $.map($(".ddlBatches option:selected"), function (el, i) {
    //    if ($(el).text().toLowerCase() == "all") {
    //        selectedBatchString = "all";
    //        batch = "all";
    //    } else {
    //        if (batch == "") {
    //            batch = $(el).text(); } 
    //      else {
    //            batch += "," + $(el).text();
    //        }
    //    }
    //});
    if (college != "" && college != undefined && batch != null) {
        window.open("/Report/BatchPassPercentageRadarReport?college=" + college + "&batch=" + Batchvalue);
    } else {                                              //QuestionPaper or batch not selected 
        if (college == "") {
            ShowAlert("Please select the assessment");
        } else {
            ShowAlert("Please select the batch");
        }
    }
    //new Chart(ctx).Radar(data, { pointDot: false });

}


function OpenInNewWindow() {
    var w = window.open();
    var html = $("#toNewWindow").html();

    $(w.document.body).html(html);
}

function ShowAlert(alertContent) {
    $(".modal").modal('hide');
    $(".alertContent").html(alertContent);
    $("#alertModal").modal({
        show: true,
        backdrop: 'static',
    });
}

function OpenModal(modalName) {
    $(".modal").modal('hide');
    $("#" + modalName).modal({
        show: true,
        backdrop: 'static',
    });
}


//End batch pass percentage report

//Branch Pass percentage report

function LoadBranchesUnderQuestionPapers(obj) {
    var assessmentID = $(obj).val();
    if (assessmentID != "") {
        $.ajax({
            url: "/Branch/GetBranchUnderQuestionPaper",
            type: "POST",
            data: {
                assessmentID: assessmentID,
            },
            success: BranchesUnderQuestionPapersLoaded
        });
    }
    else {
        var items = [];
        items.push("<option value=''>All</option>");
        $(".ddlBranch").html(items.join(''));
    }
}

function BranchesUnderQuestionPapersLoaded(response) {
    var items = [];
    items.push("<option value=''>All</option>");
    items.push(ConverDataToArrayForSection(response));
    $(".ddlBranch").html(items.join(''));
}

function GetBranchPassPercentageReport() {
    var assessmentID = $(".ddlQuestionPaper").val();
    var branchID = $(".ddlBranch").val();
    var selectedBranchString = $(".ddlBranch option:selected").text();

    var selMulti = $.map($(".ddlBranch option:selected"), function (el, i) {
        if ($(el).text().toLowerCase() == "all") {
            selectedBranchString = "all";
            branchID = "all";
        }
        return $(el).text();
    });

    if (assessmentID != "" && assessmentID != undefined && branchID != null) {
        window.open("/Report/GenerateBranchPercentageReport?assessmentID=" + assessmentID + "&selectedBranchesString=" + branchID.toString());
        //$.ajax({
        //    url: "/Report/GenerateBranchPercentageReport",
        //    type: "POST",
        //    data: {
        //        assessmentID: assessmentID,
        //        selectedBranchesString: branchID.toString(),
        //    },
        //    success: GotBranchPassPercentageReport
        //});
    }
    else {                                              //QuestionPaper or branch not selected 
        if (assessmentID != "" && assessmentID) {
            ShowAlert("Please select the assessment");
        }
        else {
            ShowAlert("Please select the branch");
        }
    }

}

function GotBranchPassPercentageReport(response) {
    var win = window.open();
    win.document.write(response);
}

//End branch pass percentage report

//QuestionPaper pass percentage report

//function LoadQuestionPapersUnderBatch(obj) {
//    var batchID = $(obj).val();
//    if (batchID != "") {
//        $.ajax({
//            url: "/QuestionPaper/GetQuestionPaperUnderBatch",
//            type: "POST",
//            data: {
//                batchID: batchID,
//            },
//            success: QuestionPapersUnderBatchLoaded
//        });
//    }
//    else {
//        var items = [];
//        items.push("<option value=''>All</option>");
//        $(".ddlQuestionPaper").html(items.join(''));
//    }
//}

function QuestionPapersUnderBatchLoaded(response) {
    var items = [];
    items.push("<option value=''>All</option>");
    items.push(ConverDataToArrayForSection(response));
    $(".ddlQuestionPaper").html(items.join(''));
}

//function LoadQuestionPapersUnderBranch(obj) {
//    var branchID = $(obj).val();
//    if (branchID != "") {
//        $.ajax({
//            url: "/QuestionPaper/GetQuestionPaperUnderBranch",
//            type: "POST",
//            data: {
//                branchIDString: branchID.toString(),
//            },
//            success: QuestionPapersUnderBranchLoaded
//        });
//    }
//    else {
//        var items = [];
//        items.push("<option value=''>All</option>");
//        $(".ddlQuestionPaper").html(items.join(''));
//    }
//}

//function QuestionPapersUnderBranchLoaded(response) {
//    var items = [];
//    items.push("<option value=''>All</option>");
//    items.push(ConverDataToArrayForSection(response));
//    $(".ddlQuestionPaper").html(items.join(''));
//}

function LoadQuestionPapersUnderBranch() {
    var branchID = $(".ddlBranch").val();
    //$("#e9").select2();
    if (branchID != "") {
        $.ajax({
            url: "/QuestionPaper/GetQuestionPaperUnderBranch",
            type: "POST",
            data: {
                branchIDString: branchID.toString(),
            },
            success: QuestionPapersUnderBranchLoaded
        });
    }
    else {
        var items = [];
        items.push("<option value=''>All</option>");
        $(".ddlQuestionPaperBranches").html(items.join(''));
    }
}

function QuestionPapersUnderBranchLoaded(response) {
    //var items = [];
    //items.push("<option value=''>All</option>");
    //items.push(ConverDataToArrayForSection(response));
    //$(".ddlQuestionPaper").html(items.join(''));
    var data = JSON.parse(response);
    var options = "";
    $.each(data, function (i, item) {
        options += "<option  value=" + item.ID + ">" + item.Name + "</option>";
    });
    $(".select2-search-choice").remove();
    $(".AssociatedQuestionPaper").html('');
    $(".AssociatedQuestionPaper").html(options);
}
function QuestionPaperFilterUnderBatch() {
    batchID = $(".ddlBatch").val();
    //$("#e9").select2();
    //selectedBatch = $(".ddlBranch").val();
    if (batchID != "") {
        $.ajax({
            type: 'POST',
            url: '/QuestionPaper/GetQuestionPaperUnderBatch',
            //GetfilterQuestionPapersUnderBatches',
            data: {
                batchIDString: batchID.toString(),
            },
            success: QuestionPaperLoad
        });
    }
}
function QuestionPaperLoad(response) {
    var data = JSON.parse(response);
    var options = "";
    $.each(data, function (i, item) {
        options += "<option  value=" + item.ID + ">" + item.Name + "</option>";
    });
    $(".select2-search-choice").remove();
    $(".AssociatedQuestionPaper").html('');
    $(".AssociatedQuestionPaper").html(options);
}

function GetQuestionPaperPassPercentageReport() {
    var selectedType = $(".radSelectType:checked").val();
    if (selectedType.toLowerCase() == "bybatch") {              //Batch radio button selected
        var assessmentID = $(".ddlQuestionPapers").val();
        var assessmentIDList = "";
        var batchID = $(".ddlBatch").val();
        var selectedQuestionPaperString = $(".ddlQuestionPapers option:selected").text();

        $('.ddlQuestionPapers :selected').each(function (i, selected) {
            if (assessmentIDList == "") {
                assessmentIDList = $(selected).val();
            } else {
                assessmentIDList += "," + $(selected).val();
            }
        });

        //var selMulti = $.map($(".ddlQuestionPaper option:selected"), function (el, i) {
        //    if ($(el).text().toLowerCase() == "all") {
        //        selectedQuestionPaperString = "all";
        //        assessmentID = "all";
        //    }
        //    return $(el).text();
        //});
        if (batchID != "" && batchID != undefined && assessmentID != null) {
            window.open("/Report/GenerateAssessmentPercentageReportByBatch?batchID=" + batchID + "&selectedQuestionPapersString=" + assessmentIDList.toString());

            //$.ajax({
            //    url: "/Report/GenerateQuestionPaperPercentageReportByBatch",
            //    type: "POST",
            //    data: {
            //        batchID: batchID,
            //        selectedQuestionPapersString: assessmentID.toString(),
            //    },
            //    success: GotQuestionPaperPassPercentageReport
            //});
        }
        else {                                              //QuestionPaper or branch not selected 
            if (batchID == "" || batchID == undefined) {
                ShowAlert("Please select the Batch");
            }
            else if(assessmentIDList==undefined || assessmentIDList==""){
                ShowAlert("Please select the assessment");
            }
        }
    }
    else if (selectedType.toLowerCase() == "bybranch") {         //Branch radio button selected
        var assessmentID = $(".ddlQuestionPaperBranches").val();
        var branchID = $(".ddlBranch").val();
        var selectedQuestionPaperString = $(".ddlQuestionPaperBranches option:selected").text();
        var assessmentIDBranchList = "";
        $('.ddlQuestionPaperBranches :selected').each(function (i, selected) {
            if (assessmentIDBranchList == "") {
                assessmentIDBranchList = $(selected).val();
            } else {
                assessmentIDBranchList += "," + $(selected).val();
            }
        });  // anu
        //var selMulti = $.map($(".ddlQuestionPaper option:selected"), function (el, i) {
        //    if ($(el).text().toLowerCase() == "all") {
        //        selectedQuestionPaperString = "all";
        //        assessmentID = "all";
        //    }
        //    return $(el).text();
        //});
        if (branchID != "" && branchID != undefined && assessmentID != null) {
            window.open("/Report/GenerateAssessmentPercentageReportByBranch?branchID=" + branchID + "&selectedQuestionPapersString=" + assessmentIDBranchList.toString());

            //$.ajax({
            //    url: "/Report/GenerateQuestionPaperPercentageReportByBranch",
            //    type: "POST",
            //    data: {
            //        branchID: branchID,
            //        selectedQuestionPapersString: assessmentID.toString(),
            //    },
            //    success: GotQuestionPaperPassPercentageReport
            //});
        }
        else {                                              //QuestionPaper or branch not selected 
            if (branchID == "" || branchID == undefined) {
                ShowAlert("Please select the Branch");
            }
            else if (assessmentIDBranchList == "" || assessmentIDBranchList==undefined) {
                ShowAlert("Please select the assessment");
            }
        }
    }
}

function GotQuestionPaperPassPercentageReport(response) {
    var win = window.open();
    win.document.write(response);
}

//End assessment pass percentage report