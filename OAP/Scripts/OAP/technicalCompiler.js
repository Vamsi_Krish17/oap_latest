﻿$('#execute').click(function () {

    var requestData = {
        "source_code": $("textarea#inputCode").val(),
        "language_id": $('#languageDropdown :selected').val(),
        "number_of_runs": "1",
        "stdin": "Judge0",
        "expected_output": "hello, Judge0",
        "cpu_time_limit": "2",
        "cpu_extra_time": "0.5",
        "wall_time_limit": "5",
        "memory_limit": "128000",
        "stack_limit": "64000",
        "max_processes_and_or_threads": "30",
        "enable_per_process_and_thread_time_limit": false,
        "enable_per_process_and_thread_memory_limit": true,
        "max_file_size": "1024"
    };

    $.ajax({
        url: 'https://api.judge0.com/submissions?wait=true',
        type: 'POST',
        data: JSON.stringify(requestData),
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        error: function (xhr) {
            alert('Error: ' + xhr.statusText);
        },
        success: function (result) {
            $("#TestCase1").empty();
            $("#TestCase2").empty();
            $("#TestCase3").empty();

            if (result.time == null) {
                result.time = "Compile Error";
            }
            var compileTime = result.time;

            if (result.time == "Compiler Error") {
                $("#compilerTime").text(compileTime)
            }
            else {
                $("#compilerTime").text("Compile Time : " + compileTime + " secs")
            }

            if (result.compile_output == null) {
                $("#TestCase1").css("color", "Green")
                $("#TestCase1").text("Success")
                $("#TestCase2").css("color", "Green")
                $("#TestCase2").text("Success")
                $("#TestCase3").css("color", "Green")
                $("#TestCase3").text("Success")
            }
            else {
                $("#TestCase1").css("color", "Red")
                $("#TestCase1").text("Fail ... Error : " + result.compile_output)
                $("#TestCase2").css("color", "Red")
                $("#TestCase2").text("Fail ... Error : " + result.compile_output)
                $("#TestCase3").css("color", "Red")
                $("#TestCase3").text("Fail ... Error : " + result.compile_output)
            }
        },
        async: true,
        processData: false
    });

});