﻿$(function () {
    $(".validation-summary-errors").removeClass("validation-summary-errors");
    $(".input-validation-error").removeClass("input-validation-error").parent().addClass("has-error");

    $(".chkMassUpload").change(function () {
        if ($(this).prop('checked')) {
            //$(".massUploadContainer").show();
            $(".has-error").removeClass("has-error");
            $(".massUploadFile").removeAttr("disabled");
            $(".txt").attr("disabled", "disabled");
        }
        else {
            //$(".massUploadContainer").hide();
            $(".massUploadFile").attr("disabled", "disabled");
            $(".txt").removeAttr("disabled");
            $(".uploadFileSizeError").html("");
            $(".ImageError").html("");
            $('.massUploadFile').val('');
        }
    })
});

function EditSection(obj)
{
    var name = $(obj).parent().parent().children('.name').html();
    var htmlResult = "<div class='nameContainer '> <input type='text' class='txtName txt form-control' value='" + name + "'> <input type='button' onclick='UpdateSection(this)' value='Update'>";
    $(obj).parent().parent().children('.name').append(htmlResult);
}

function UpdateSection(obj)
{
    var updatedName = $(obj).parent().children(".txtName").val();
    var id=$(obj).parent().parent().parent().attr('id');
    $.ajax({
        type: 'POST',
        url: '/Section/Update',
        data: {
            idString: id,
            name:updatedName,
        },
        success:SectionUpdated,
    });
}

function SectionUpdated(response)
{
    if (response == "True") {
        alert("Updated successfully");
        $(".nameContainer").remove();
    }
    else {
        alert("Some error occured");
    }
}

//College Index



function OpenCollegeDeleteModal() {
    var selectedColleges = "";
    $(".chkSeperate:checked").each(function () {
        if (selectedColleges == "") {
            selectedColleges = $(this).val();
        }
        else {
            selectedColleges = selectedColleges + "," + $(this).val();
        }
    });
    if (selectedColleges == "") {
        ShowAlert("Please select ateast one college to delete");
    }
    else {
        OpenModal("deleteModal");
    }
    // anu
    //if ($(".chkSeperate:checked").length > 0) {                                           //Branches selected
    //    OpenModal("deleteModal");
    //}
    //else {                                                                                  //No Branches selected
    //    ShowAlert("Please select atleast one record to delete");
    //}
}

function DeleteCollege() {

    var selectedColleges = $(".chkSeperate:checked").map(function () {
        return $(this).val();
    }).get();
    if (selectedColleges != "") {                                                       //Branches selected
        OpenModal("loadingModal");
        $.ajax({
            url: "/College/DeleteColleges",
            type: "POST",
            data: {
                collegeIDString: selectedColleges.toString(),
            },
            success: DeleteCollegeSuccess
        });
    }
    else {                                                                              //No branches selected
        ShowAlert("Please select atleast one record to delete");
    }
}

function DeleteCollegeSuccess(response) {
    if (response.toLowerCase() == "true") {
        ShowAlert("College delete successfully");
    }
    else {
        ShowAlert("College not deleted. Please try again later.");
    }
    $(".chkAll").prop("checked", false);
    BindColleteDataTable();
}

function OpenModal(modalName) {
    $(".modal").modal('hide');
    $("#" + modalName).modal({
        show: true,
        backdrop: 'static',
    });
}

function ShowAlert(alertContent) {
    $(".modal").modal('hide');
    $(".alertContent").html(alertContent);
    $("#alertModal").modal({
        show: true,
        backdrop: 'static',
    });
}


//End College Index

