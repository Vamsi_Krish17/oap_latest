﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OAP.Domain.ViewModel;
using OAP.Domain;
using System.Globalization;


namespace HelperClasses
{
    public class MapperHelper
    {


        #region Branch


        public static List<BranchViewModel> MapToViewModelList(IEnumerable<Branch> branches, int rowNumber = 1)
        {
            List<BranchViewModel> branchViewModelList = (from b in branches
                                                         select new BranchViewModel()
                                                         {
                                                             ID = b.ID,
                                                             Name = b.Name,
                                                             Years = b.Years ?? 0,
                                                             RowNumber = rowNumber++,
                                                         }).ToList();

            return branchViewModelList;
        }

        public static Branch MapToDomainObject(OAPEntities db, BranchViewModel branchViewModel)
        {
            return MapToDomainObject(db, branchViewModel, new Branch());
        }

        public static Branch MapToDomainObject(OAPEntities db, BranchViewModel branchViewModel, Branch branch)
        {
            branch.ID = branchViewModel.ID;
            branch.Name = branchViewModel.Name;
            //branch.College = db.College.Find(branchViewModel.CollegeID);
            branch.ModifiedDate = DateTime.Now;
            branch.Years = branchViewModel.Years;
            return branch;
        }

        public static BranchViewModel MapToViewModel(Branch branch)
        {
            BranchViewModel branchViewModel = new BranchViewModel();
            branchViewModel.ID = branch.ID;
            branchViewModel.Name = branch.Name;
            branchViewModel.Years = branch.Years ?? 0;
            return branchViewModel;
        }

        #endregion

        #region Section

        public static List<SectionViewModel> MapToViewModelList(IEnumerable<Section> sections, int rowNumber = 1)
        {
            //if (!sections.Any())
            //{
            List<SectionViewModel> sectionViewModelList = (from s in sections
                                                           select new SectionViewModel()
                                                           {
                                                               ID = s.ID,
                                                               Name = s.Name,
                                                               RowNumber = rowNumber++,
                                                           }).ToList();
            return sectionViewModelList;
            //}
            //else
            //{
            //    return new List<SectionViewModel>();
            //}

        }

        public static Section MapToDomainObject(OAPEntities db, SectionViewModel sectionViewModel)
        {
            return MapToDomainObject(db, sectionViewModel, new Section());
        }

        public static Section MapToDomainObject(OAPEntities db, SectionViewModel sectionViewModel, Section section)
        {
            section.ID = sectionViewModel.ID;
            section.Name = sectionViewModel.Name;
            //section.College = db.College.Find(sectionViewModel.CollegeID);           
            return section;
        }

        public static SectionViewModel MapToViewModel(Section section)
        {
            SectionViewModel sectionViewModel = new SectionViewModel();
            sectionViewModel.ID = section.ID;
            sectionViewModel.Name = section.Name;
            return sectionViewModel;
        }

        #endregion

        #region College

        public static List<CollegeViewModel> MapToViewModelList(IEnumerable<College> collegeList, int rowNumber = 1)
        {
            List<CollegeViewModel> collegeViewModelList = (from c in collegeList
                                                           select new CollegeViewModel()
                                                           {
                                                               ID = c.ID,
                                                               Name = c.Name,
                                                               Code = c.Code,
                                                               Address1 = c.Address1,
                                                               Address2 = c.Address2,
                                                               City = c.City,
                                                               Pincode = c.Pincode,
                                                           }).ToList();
            return collegeViewModelList;
        }

        public static CollegeViewModel MapToViewModel(College college)
        {
            CollegeViewModel collegeViewModel = new CollegeViewModel();
            if (college != null)
            {
                collegeViewModel.ID = college.ID;
                collegeViewModel.Name = college.Name;
                collegeViewModel.Code = college.Code;
                collegeViewModel.Address1 = college.Address1;
                collegeViewModel.Address2 = college.Address2;
                collegeViewModel.City = college.City;
                collegeViewModel.Pincode = college.Pincode;
            }
            return collegeViewModel;
        }

        public static College MapToDomainObject(OAPEntities db, CollegeViewModel collegeViewModel)
        {
            return MapToDomainObject(db, collegeViewModel, new College());
        }

        public static College MapToDomainObject(OAPEntities db, CollegeViewModel collegeViewModel, College college)
        {
            college.ID = collegeViewModel.ID;
            college.Code = collegeViewModel.Code;
            college.Name = collegeViewModel.Name;
            college.Address1 = collegeViewModel.Address1;
            college.Address2 = collegeViewModel.Address2;
            college.City = collegeViewModel.City;
            college.Pincode = collegeViewModel.Pincode;

            return college;
        }

        #endregion

        #region Student

        public static List<StudentViewModel> MapToViewModelList(IQueryable<Student> studentList, int rowNumber = 1)
        {
            List<StudentViewModel> studentViewModelList = (from s in studentList
                                                           select new StudentViewModel()
                                                           {
                                                               ID = s.ID,
                                                               FullName = s.Name,
                                                               UniversityRegistryNumber = s.UniversityRegisterNumber,
                                                               BranchID = s.Branch != null ? s.Branch.ID : 0,
                                                               EmailId = s.EmailId,
                                                               BatchName = s.BatchStudent.Select(x => x.Batch.Name).FirstOrDefault(),
                                                               UserProfileID = s.UserProfile.Select(x => x.UserId).FirstOrDefault(),
                                                               ResumePath = s.StudentResume.Count > 0 ? s.StudentResume.FirstOrDefault() != null ? s.StudentResume.FirstOrDefault().FilePath : "" : "",
                                                           }).ToList();
            return studentViewModelList;
        }

        public static StudentViewModel MapToViewModel(Student student)
        {
            StudentViewModel studentViewModel = new StudentViewModel();
            studentViewModel.ID = student.ID;
            studentViewModel.UserProfileID = student.UserProfile.FirstOrDefault().UserId;
            studentViewModel.FullName = student.Name;
            studentViewModel.UniversityRegistryNumber = student.UniversityRegisterNumber;
            studentViewModel.BranchID = student.Branch != null ? student.Branch.ID : 0;
            studentViewModel.EmailId = student.EmailId;
            return studentViewModel;
        }

        public static Student MapToDomainObject(OAPEntities db, StudentViewModel studentViewModel)
        {
            return MapToDomainObject(db, studentViewModel, new Student());
        }

        public static Student MapToDomainObject(OAPEntities db, StudentViewModel studentViewModel, Student student)
        {
            student.ID = studentViewModel.ID;
            student.Name = studentViewModel.FullName;
            student.UserProfile.FirstOrDefault().UserName = studentViewModel.UniversityRegistryNumber;
            student.Branch = db.Branch.Find(studentViewModel.BranchID);
            student.EmailId = studentViewModel.EmailId;
            return student;
        }

        #endregion



        #region Question
        public static List<QuestionViewModel> MapToViewModelList(IQueryable<Question> questions)
        {
            // int questionNumber = 1;
            List<QuestionViewModel> questionViewModelList = (from q in questions
                                                             select new QuestionViewModel()
                                                             {
                                                                 ID = q.ID,
                                                                 Description = q.Description,


                                                                 // QuestionTypeID = q.QuestionType != null ? q.QuestionType.ID : 0,
                                                                 // QuestionCategoryID = q.QuestionCategory != null ? q.QuestionCategory.ID : 0,
                                                                 LastUpdatedTime = q.LastUpdated != null ? q.LastUpdated.Value : new DateTime(),
                                                                 Solution = q.Solution,
                                                                 DownloadLink = q.DownloadLink,
                                                                 Passage = q.Passage != null ? q.Passage.Description : "",
                                                                 PassageID = q.PassageID ?? 0,
                                                             }).ToList();
            return questionViewModelList;
        }

        public static List<TechnicalQuestionViewModel> MapToTechnicalViewModelList(IQueryable<TechnicalQuestion> questions)
        {
            // int questionNumber = 1;
            List<TechnicalQuestionViewModel> questionViewModelList = (from q in questions
                                                                      select new TechnicalQuestionViewModel()
                                                                      {
                                                                          ID = q.ID,
                                                                          Description = q.Description,
                                                                          Title = q.Title,
                                                                          LastUpdatedTime = q.LastUpdated != null ? q.LastUpdated.Value : new DateTime(),
                                                                          Solution = q.Solution,
                                                                          DownloadLink = q.DownloadLink,
                                                                          Passage = q.Passage != null ? q.Passage.Description : "",
                                                                          PassageID = q.PassageID ?? 0,
                                                                      }).ToList();
            return questionViewModelList;
        }


        #region Uploaded Question Preview
        public static List<QuestionViewModel> MapToViewModelListToShowFullQuestion(IQueryable<Question> questions)
        {
            List<QuestionViewModel> questionViewModelList = (from q in questions
                                                             select new QuestionViewModel()
                                                             {
                                                                 ID = q.ID,
                                                                 Description = q.Description != null ? q.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
                                                                                                                    .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ") : "",
                                                                 QuestionTypeID = q.QuestionType != null ? q.QuestionType.ID : 0,
                                                                 //  QuestionCategoryID = q.QuestionCategory != null ? q.QuestionCategory.ID : 0,
                                                                 LastUpdatedTime = q.LastUpdated != null ? q.LastUpdated.Value : new DateTime(),
                                                                 Solution = q.Solution,
                                                                 AnswerViewModelList = q.Answer.Count() > 0 ? MapToViewModelList(q.Answer.AsQueryable()) : new List<AnswerViewModel>(),
                                                                 QuestionPassage = q.Passage != null ? MapToViewModel(q.Passage) : new PassageViewModel(),
                                                                 QuestionType = q.QuestionType != null ? q.QuestionType.Name : "",
                                                                 Marks = q.Marks ?? 0,
                                                             }).ToList();
            return questionViewModelList;
        }



        public static QuestionViewModel MapToViewModelToShowFullQuestion(Question question)
        {
            QuestionViewModel questionViewModel = new QuestionViewModel();
            questionViewModel.ID = question.ID;
            questionViewModel.Description = question.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
                                                                                                                    .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
            questionViewModel.LastUpdatedTime = question.LastUpdated != null ? question.LastUpdated.Value : new DateTime();
            questionViewModel.QuestionCategoryID = question.QuestionCategory != null ? question.QuestionCategory.ID : 0;
            questionViewModel.QuestionTypeID = question.QuestionType != null ? question.QuestionType.ID : 0;
            questionViewModel.Solution = question.Solution;
            questionViewModel.AnswerViewModelList = question.Answer.Count() > 0 ? MapToViewModelList(question.Answer.AsQueryable()) : new List<AnswerViewModel>();
            questionViewModel.QuestionPassage = question.Passage != null ? MapToViewModel(question.Passage) : new PassageViewModel();
            questionViewModel.QuestionType = question.QuestionType != null ? question.QuestionType.Name : "";
            questionViewModel.Marks = question.Marks ?? 0;
            return questionViewModel;
        }


        #endregion

        public static QuestionViewModel MapToViewModel(Question question)
        {
            QuestionViewModel questionViewModel = new QuestionViewModel();
            questionViewModel.ID = question.ID;
            questionViewModel.Description = question.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η").Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
            questionViewModel.LastUpdatedTime = question.LastUpdated != null ? question.LastUpdated.Value : new DateTime();
            // questionViewModel.QuestionCategoryID = question.QuestionCategory != null ? question.QuestionCategory.ID : 0;
            //questionViewModel.QuestionTypeID = question.QuestionType != null ? question.QuestionType.ID : 0;
            questionViewModel.Solution = question.Solution;
            questionViewModel.QuestionTypeID = question.QuestionTypeID ?? 0;
            questionViewModel.SubjectID = question.SubjectID ?? 0;
            questionViewModel.ComplexityID = question.ComplexityID ?? 0;
            // questionViewModel.QuestionType = question.QuestionType != null ? question.QuestionType.Name : "";
            return questionViewModel;
        }



        public static Question MapToDomainObject(OAPEntities db, QuestionViewModel questionViewModel)
        {
            return MapToDomainObject(db, questionViewModel, new Question());
        }

        public static Question MapToDomainObject(OAPEntities db, QuestionViewModel questionViewModel, Question question)
        {
            if (question == null)
            {
                question = new Question();
            }
            question.ID = questionViewModel.ID;
            question.Description = questionViewModel.Description;
            question.QuestionType = db.QuestionType.Find(questionViewModel.QuestionTypeID);
            //  question.QuestionCategory = db.QuestionCategory.Find(questionViewModel.QuestionCategoryID);
            question.LastUpdated = DateTime.Now;
            question.Solution = questionViewModel.Solution;
            //question.SubjectID = questionViewModel.SubjectID;
            //question.ComplexityID = questionViewModel.ComplexityID;
            return question;
        }


        public static TechnicalQuestion MapToDomainObject(OAPEntities db, TechnicalQuestionViewModel questionViewModel)
        {
            return MapToDomainObject(db, questionViewModel, new TechnicalQuestion());
        }

        public static TechnicalQuestion MapToDomainObject(OAPEntities db, TechnicalQuestionViewModel questionViewModel, TechnicalQuestion question)
        {
            if (question == null)
            {
                question = new TechnicalQuestion();
            }
            question.ID = questionViewModel.ID;
            question.Title = questionViewModel.Title;
            question.Description = questionViewModel.Description;
            question.QuestionType = db.QuestionType.Find(questionViewModel.QuestionTypeID);
            //  question.QuestionCategory = db.QuestionCategory.Find(questionViewModel.QuestionCategoryID);
            question.LastUpdated = DateTime.Now;
            question.Solution = questionViewModel.Solution;
            question.Languages = questionViewModel.LanguagesString;
            question.SolutionType = questionViewModel.SolutionType;
            //question.SubjectID = questionViewModel.SubjectID;
            //question.ComplexityID = questionViewModel.ComplexityID;
            return question;
        }






        #endregion


        #region UserQuestionPaper

        public static List<UserQuestionPaperViewModel> MapToViewModelList(IQueryable<UserAssessment> userQuestionPapers)
        {
            List<UserQuestionPaperViewModel> userQuestionPaperViewModelList = (from ua in userQuestionPapers
                                                                               select new UserQuestionPaperViewModel
                                                                               {
                                                                                   ID = ua.ID,
                                                                                   UserID = ua.UserID ?? 0,
                                                                                   UserName = ua.UserProfile.UserName,
                                                                                   UniversityRegisterNumber = ua.UserProfile.Student.UniversityRegisterNumber,
                                                                                   QuestionPaperID = ua.AssessmentID ?? 0,
                                                                                   SessionID = ua.SessionID,
                                                                                   StartDateTime = ua.StartTime ?? new DateTime(),
                                                                                   FinishDateTime = ua.FinishTime ?? new DateTime(),
                                                                                   MobileNumber = ua.MobileNo,
                                                                                   Marks = ua.Marks ?? 0,
                                                                                   Result = ua.Result ?? false,
                                                                                   IsActive = ua.IsActive ?? false,
                                                                                   CurrentSectionIndex = ua.CurrentSectionIndex ?? 0,

                                                                               }).ToList();
            return userQuestionPaperViewModelList;
        }

        #endregion

        #region Upload Questions
        public static List<UploadQuestionViewModel> MapToViewModelList(IEnumerable<Uploads> uploads)
        {
            List<UploadQuestionViewModel> uploadQuestionViewModelList = (from u in uploads
                                                                         select new UploadQuestionViewModel()
                                                                         {

                                                                             ID = u.ID,
                                                                             Title = u.Title,
                                                                             UploadedDate = u.UploadedDate,
                                                                             UploadedDateTimeString = u.UploadedDate.ToString("dd-MM-yyy HH:mm "),
                                                                             UploadedUserID = u.UploadedUserID ?? 0,
                                                                             QuestionCount = u.Question.Count(),
                                                                         }).ToList();
            return uploadQuestionViewModelList;
        }


        #endregion


        #region BatchQuestionPaper

        public static List<BatchQuestionPaperViewModel> MapToViewModelList(IQueryable<BatchAssessment> batchQuestionPapers)
        {
            List<BatchQuestionPaperViewModel> batchQuestionPaperViewModelList = (from ba in batchQuestionPapers
                                                                                 select new BatchQuestionPaperViewModel()
                                                                                 {
                                                                                     ID = ba.ID,
                                                                                     BatchID = ba.BatchID,
                                                                                     Name = ba.Name != null ? ba.Name : "[No Name]",
                                                                                     QuestionPaperID = ba.AssessmentID,
                                                                                     QuestionPaperName = ba.QuestionPaper != null ? ba.QuestionPaper.Name : "",
                                                                                     CollegeName = ba.Batch != null ? ba.Batch.College != null ? ba.Batch.College.Name : "" : "",
                                                                                     //StartDateString = ba.StartDateTime.ToString("dd-MM-yyy"),
                                                                                     //EndDateString = ba.EndDateTime.ToString("dd-MM-yyy"),
                                                                                     //BatchViewModel = MapToViewModel(ba.Batch),
                                                                                     BatchName = ba.Batch.Name,
                                                                                     //QuestionPaperViewModel = MapToViewModel(ba.QuestionPaper),
                                                                                     TotalStudent = ba.Batch.BatchStudent.Select(x => x.Student).Count(),
                                                                                     WritingStudent = ba.Batch.BatchStudent.SelectMany(x => x.Student.UserProfile.SelectMany(y => y.UserAssessment.Where(z => z.FinishTime == null && z.AssessmentID == ba.AssessmentID))).Count(),
                                                                                     NotTakenStudent = ba.Batch.BatchStudent.Select(x => x.Student).Count() - (ba.Batch.BatchStudent.SelectMany(x => x.Student.UserProfile.SelectMany(y => y.UserAssessment.Where(z => z.FinishTime == null && z.AssessmentID == ba.AssessmentID))).Count() + ba.Batch.BatchStudent.SelectMany(x => x.Student.UserProfile.SelectMany(y => y.UserAssessment.Where(z => z.FinishTime != null && z.AssessmentID == ba.AssessmentID))).Count()),
                                                                                     CompletedStudent = ba.Batch.BatchStudent.SelectMany(x => x.Student.UserProfile.SelectMany(y => y.UserAssessment.Where(z => z.FinishTime != null && z.AssessmentID == ba.AssessmentID))).Count(),

                                                                                 }).ToList();
            return batchQuestionPaperViewModelList;
        }

        //public static List<BatchQuestionPaperViewModel> MapToViewModelList(IQueryable<FormAssess> formQuestionPaper)
        //{
        //    List<BatchQuestionPaperViewModel> batchQuestionPaperViewModelList = (from ba in formQuestionPaper
        //                                                                         select new BatchQuestionPaperViewModel()
        //                                                                         {
        //                                                                             ID = ba.ID,
        //                                                                             // BatchID = ba.BatchID,
        //                                                                             Name = ba.Name != null ? ba.Name : "[No Name]",
        //                                                                             QuestionPaperID = ba.QuestionPaperID,
        //                                                                             QuestionPaperName = ba.QuestionPaper != null ? ba.QuestionPaper.Name : "",
        //                                                                             // CollegeName = ba.Batch != null ? ba.Batch.College != null ? ba.Batch.College.Name : "" : "",
        //                                                                             // StartDateString = ba.StartDateTime.ToString("dd-MM-yyy"),
        //                                                                             //EndDateString = ba.EndDateTime.ToString("dd-MM-yyy"),
        //                                                                             //BatchViewModel = MapToViewModel(ba.Batch),
        //                                                                             // BatchName = ba.Batch.Name,
        //                                                                             //QuestionPaperViewModel = MapToViewModel(ba.QuestionPaper),
        //                                                                             // TotalStudent = ba.Batch.BatchStudent.Select(x => x.Student).Count(),
        //                                                                             // WritingStudent = ba.Batch.BatchStudent.SelectMany(x => x.Student.UserProfile.SelectMany(y => y.UserQuestionPaper.Where(z => z.FinishTime == null && z.QuestionPaperID == ba.QuestionPaperID))).Count(),
        //                                                                             // NotTakenStudent = ba.Batch.BatchStudent.Select(x => x.Student).Count() - (ba.Batch.BatchStudent.SelectMany(x => x.Student.UserProfile.SelectMany(y => y.UserQuestionPaper.Where(z => z.FinishTime == null && z.QuestionPaperID == ba.QuestionPaperID))).Count() + ba.Batch.BatchStudent.SelectMany(x => x.Student.UserProfile.SelectMany(y => y.UserQuestionPaper.Where(z => z.FinishTime != null && z.QuestionPaperID == ba.QuestionPaperID))).Count()),
        //                                                                             //CompletedStudent = ba.Batch.BatchStudent.SelectMany(x => x.Student.UserProfile.SelectMany(y => y.UserQuestionPaper.Where(z => z.FinishTime != null && z.QuestionPaperID == ba.QuestionPaperID))).Count(),

        //                                                                         }).ToList();
        //    return batchQuestionPaperViewModelList;
        //}

        public static BatchQuestionPaperViewModel MapToViewModel(BatchAssessment batchQuestionPaper)
        {
            BatchQuestionPaperViewModel batchQuestionPaperViewModel = new BatchQuestionPaperViewModel();
            batchQuestionPaperViewModel.ID = batchQuestionPaper.ID;
            batchQuestionPaperViewModel.Name = batchQuestionPaper.Name;
            batchQuestionPaperViewModel.BatchID = batchQuestionPaper.BatchID;
            batchQuestionPaperViewModel.QuestionPaperID = batchQuestionPaper.AssessmentID;
            //batchQuestionPaperViewModel.StartDateString = batchQuestionPaper.StartDateTime.ToString("dd-MM-yyyy HH:MM");
            batchQuestionPaperViewModel.StartDateString = batchQuestionPaper.StartDateTime.ToString("dd-MM-yyyy HH:mm");
            batchQuestionPaperViewModel.EndDateString = batchQuestionPaper.EndDateTime.ToString("dd-MM-yyyy HH:mm");
            batchQuestionPaperViewModel.CollegeID = batchQuestionPaper.Batch.CollegeID != null ? batchQuestionPaper.Batch.CollegeID.Value : 0;
            return batchQuestionPaperViewModel;
        }


        //public static BatchQuestionPaperViewModel MapToViewModel(FormAss formQuestionPaper)
        //{
        //    BatchQuestionPaperViewModel batchQuestionPaperViewModel = new BatchQuestionPaperViewModel();
        //    batchQuestionPaperViewModel.ID = formQuestionPaper.ID;
        //    batchQuestionPaperViewModel.Name = formQuestionPaper.Name;
        //    // batchQuestionPaperViewModel.BatchID = batchQuestionPaper.BatchID;
        //    batchQuestionPaperViewModel.QuestionPaperID = formQuestionPaper.QuestionPaperID;
        //    //batchQuestionPaperViewModel.StartDateString = batchQuestionPaper.StartDateTime.ToString("dd-MM-yyyy HH:MM");
        //    batchQuestionPaperViewModel.StartDateString = formQuestionPaper.StartDateTime.ToString("dd-MM-yyyy HH:mm");
        //    batchQuestionPaperViewModel.EndDateString = formQuestionPaper.EndDateTime.ToString("dd-MM-yyyy HH:mm");
        //    //batchQuestionPaperViewModel.CollegeID = batchQuestionPaper.Batch.CollegeID != null ? batchQuestionPaper.Batch.CollegeID.Value : 0;
        //    return batchQuestionPaperViewModel;
        //}
        #endregion



        //public static List<char> PhoneNumber2 { get; set; }

        public static QuestionPaperViewModel MapToViewModelForProgram(QuestionPaper questionPaper)
        {
            QuestionPaperViewModel questionPaperViewModel = new QuestionPaperViewModel();
            questionPaperViewModel.ID = questionPaper.ID;
            questionPaperViewModel.Name = questionPaper.Name;
            //questionPaperViewModel.Description = questionPaper.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
            //                                                                                                        .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
            //                                                                                                        .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
            //                                                                                                        .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
            //                                                                                                        .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
            //                                                                                                        .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
            //                                                                                                        .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
            //                                                                                                        .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
            //                                                                                                        .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
            //                                                                                                        .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
            questionPaperViewModel.Marks = questionPaper.Marks ?? 0;
            questionPaperViewModel.NegativeMarks = questionPaper.NegativeMarks ?? 0;

            questionPaperViewModel.questionPaperSectionViewModelList = MapToViewModelList(questionPaper.QuestionPaperSection.AsQueryable());
            return questionPaperViewModel;
        }

        public static List<QuestionPaperSectionQuestionViewModel> MapToViewModelList(List<QuestionPaperSectionQuestion> sectionQuestionList)
        {
            List<QuestionPaperSectionQuestionViewModel> questionPaperSectionQuestionViewModelList = (from ba in sectionQuestionList
                                                                                                     select new QuestionPaperSectionQuestionViewModel()
                                                                                                     {
                                                                                                         ID = ba.ID,
                                                                                                         QuestionPaperSectionID = ba.AssessmentSectionID,
                                                                                                         QuestionID = ba.QuestionID,
                                                                                                         QuestionSubject = ba.Question != null ? ba.Question.Subject != null ? ba.Question.Subject.Name : "" : "",
                                                                                                         QuestionComplexityLevel = ba.Question != null ? ba.Question.Complexity != null ? ba.Question.Complexity.ComplexityLevel ?? 0 : 0 : 0,
                                                                                                         questionTags = ba.Question.QuestionTags.Select(q => q.Tag != null ? q.Tag.Name : "").ToList(),
                                                                                                     }
                                                                              ).ToList();

            return questionPaperSectionQuestionViewModelList;
        }

        //public static QuestionTemplateWizardViewModel MapToViewModel(QuestionPaperTemplate questionPaperTemplate)
        //{
        //    QuestionTemplateWizardViewModel questionTemplateWizardViewModel = new QuestionTemplateWizardViewModel();
        //    questionTemplateWizardViewModel.ID = questionPaperTemplate.ID;
        //    questionTemplateWizardViewModel.QuestionCode = questionPaperTemplate.Code;
        //    questionTemplateWizardViewModel.QuestionName = questionPaperTemplate.Name;
        //    questionTemplateWizardViewModel.TimeLimit = questionPaperTemplate.TimeLimit ?? 0;
        //    questionTemplateWizardViewModel.Marks = questionPaperTemplate.Marks ?? 0;
        //    questionTemplateWizardViewModel.NegativeMarks = questionPaperTemplate.NegativeMarks ?? 0;
        //    questionTemplateWizardViewModel.CutOffMark = questionPaperTemplate.CutOffMark ?? 0;
        //    questionTemplateWizardViewModel.Description = questionPaperTemplate.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                                                                            .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                                                                            .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                                                                            .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                                                                            .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                                                                            .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                                                                            .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                                                                            .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
        //                                                                                                            .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η")
        //                                                                                                            .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
        //    questionTemplateWizardViewModel.DisplayMarks = questionPaperTemplate.DisplayMarks ?? false;
        //    questionTemplateWizardViewModel.DisplayResult = questionPaperTemplate.DisplayResult ?? false;
        //    questionTemplateWizardViewModel.TimeLimitTimeSpan = questionPaperTemplate.TimeLimit != null ? TimeSpan.FromMinutes(questionPaperTemplate.TimeLimit.Value) : TimeSpan.Zero;
        //    questionTemplateWizardViewModel.CanJumpSections = questionPaperTemplate.CanJumpSections ?? false;
        //    questionTemplateWizardViewModel.Instructions = questionPaperTemplate.Instructions;
        //    questionTemplateWizardViewModel.ShowInstructions = questionPaperTemplate.ShowInstruction ?? false;
        //    questionTemplateWizardViewModel.ShowReport = questionPaperTemplate.ShowReport ?? false;

        //    return questionTemplateWizardViewModel;
        //}


        //#region Question Paper Section Rule

        //public static List<QuestionPaperSectionTemplateRuleViewModel> MapToViewModelList(IQueryable<QuestionPaperSectionTemplateRule> questionPaperSectionTemplateRules, int currentIndex)
        //{
        //    List<QuestionPaperSectionTemplateRuleViewModel> questionPaperSectionTemplateRuleViewModelList = (from qpST in questionPaperSectionTemplateRules
        //                                                                                                     select new QuestionPaperSectionTemplateRuleViewModel()
        //                                                                                                     {
        //                                                                                                         ID = qpST.ID,
        //                                                                                                         Count = qpST.Count ?? 0,
        //                                                                                                         Name = qpST.Name,
        //                                                                                                         Complexity = qpST.QuestionPaperSectionTemplateRuleComplexity.FirstOrDefault() != null ? qpST.QuestionPaperSectionTemplateRuleComplexity.FirstOrDefault().Complexity != null ? qpST.QuestionPaperSectionTemplateRuleComplexity.FirstOrDefault().Complexity.ComplexityLevel ?? 0 : 0 : 0,
        //                                                                                                         SubjectNames = string.Join(",", qpST.QuestionPaperSectionTemplateRuleSubject.Select(x => x.Subject.Name)),
        //                                                                                                         TagNames = string.Join(",", qpST.QuestionPaperSectionTemplateRuleTag.Select(x => x.Tag.Name)),
        //                                                                                                         QuestionPaperSectionTemplateID = qpST.QuestionPaperSectionTemplateID ?? 0,
        //                                                                                                         CurrentIndex = currentIndex,
        //                                                                                                     }).ToList();

        //    return questionPaperSectionTemplateRuleViewModelList;
        //}

        //#endregion

        //public static List<char> TagNames { get; set; }

        //#region Question paper Templates
        //public static List<QuestionTemplateWizardViewModel> MapToViewModelList(IQueryable<QuestionPaperTemplate> questionPaperTemplates)
        //{
        //    //OAPEntities db = new OAPEntities();
        //    List<QuestionTemplateWizardViewModel> questionTemplateWizardViewModelList = (from qpt in questionPaperTemplates
        //                                                                                 select new QuestionTemplateWizardViewModel()
        //                                                                                 {
        //                                                                                     ID = qpt.ID,
        //                                                                                     QuestionName = qpt.Name,
        //                                                                                     QuestionCode = qpt.Code,
        //                                                                                     CutOffMark = qpt.CutOffMark != null ? qpt.CutOffMark.Value : 0,
        //                                                                                     Description = qpt.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                                                                            .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                                                                            .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                                                                            .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                                                                            .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                                                                            .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                                                                            .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                                                                            .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
        //                                                                                                            .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η")
        //                                                                                                            .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ"),
        //                                                                                     DisplayMarks = qpt.DisplayMarks != null ? qpt.DisplayMarks.Value : false,
        //                                                                                     DisplayResult = qpt.DisplayResult != null ? qpt.DisplayResult.Value : false,
        //                                                                                     // EndDate = qpt.EndDateTime != null ? qpt.EndDateTime.Value : new DateTime(),
        //                                                                                     // StartDate = qpt.StatDateTime != null ? qpt.StatDateTime.Value : new DateTime(),
        //                                                                                     // Instructions = qpt.Instructions,
        //                                                                                     Marks = qpt.Marks != null ? qpt.Marks.Value : 0,
        //                                                                                     NegativeMarks = qpt.NegativeMarks != null ? qpt.NegativeMarks.Value : 0,
        //                                                                                     //  IsNegativePercentage = qpt.IsNegativePercentage != null ? qpt.IsNegativePercentage.Value : false,
        //                                                                                     // ReleaseKey = qpt.ReleaseKey != null ? qpt.ReleaseKey.Value : false,
        //                                                                                     TimeLimit = qpt.TimeLimit != null ? qpt.TimeLimit.Value : 0,
        //                                                                                     // ShowInstructions = qpt.ShowInstruction ?? false,
        //                                                                                     // ShowReport = qpt.ShowReport ?? false,
        //                                                                                     createdDate = qpt.CreatedDate,
        //                                                                                     //QuestionPaperCountThisTemplate = 
        //                                                                                 }).ToList();

        //    return questionTemplateWizardViewModelList;
        //}

        //public static QuestionPaperTemplate MapToDomainObject(OAPEntities db, QuestionTemplateWizardViewModel questionTemplateWizardViewModel)
        //{
        //    return MapToDomainObject(db, questionTemplateWizardViewModel, new QuestionPaperTemplate());
        //}

        //public static QuestionPaperTemplate MapToDomainObject(OAPEntities db, QuestionTemplateWizardViewModel questionTemplateWizardViewModel, QuestionPaperTemplate questionPaperTemplate)
        //{
        //    questionPaperTemplate.ID = Convert.ToInt32(questionTemplateWizardViewModel.ID);
        //    questionPaperTemplate.Name = questionTemplateWizardViewModel.QuestionName;
        //    questionPaperTemplate.Code = questionTemplateWizardViewModel.QuestionCode;
        //    questionPaperTemplate.CutOffMark = questionTemplateWizardViewModel.CutOffMark;
        //    questionPaperTemplate.Description = questionTemplateWizardViewModel.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                                                                            .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                                                                            .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                                                                            .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                                                                            .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                                                                            .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                                                                            .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                                                                            .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
        //                                                                                                            .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η")
        //                                                                                                            .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
        //    questionPaperTemplate.DisplayMarks = questionTemplateWizardViewModel.DisplayMarks;
        //    questionPaperTemplate.DisplayResult = questionTemplateWizardViewModel.DisplayResult;
        //    // questionPaperTemplate.EndDateTime = questionTemplateWizardViewModel.EndDate;
        //    questionPaperTemplate.Instructions = questionTemplateWizardViewModel.Instructions;
        //    //   questionPaperTemplate.IsNegativePercentage = questionTemplateWizardViewModel.IsNegativePercentage;
        //    questionPaperTemplate.Marks = questionTemplateWizardViewModel.Marks;
        //    questionPaperTemplate.NegativeMarks = questionTemplateWizardViewModel.NegativeMarks;
        //    // questionPaperTemplate.ReleaseKey = questionTemplateWizardViewModel.ReleaseKey;
        //    //   questionPaperTemplate.StatDateTime = questionTemplateWizardViewModel.StartDate;
        //    questionPaperTemplate.TimeLimit = questionTemplateWizardViewModel.TimeLimit;
        //    //assessment.College = db.College.Find(questionPaperViewModel.CollegeID);
        //    questionPaperTemplate.ModifiedDate = DateTime.Now;
        //    questionPaperTemplate.CanJumpSections = questionTemplateWizardViewModel.CanJumpSections;
        //    questionPaperTemplate.ShowInstruction = questionTemplateWizardViewModel.ShowInstructions;
        //    questionPaperTemplate.ShowReport = questionTemplateWizardViewModel.ShowReport;
        //    return questionPaperTemplate;
        //}
        //#endregion

        //public static List<QuestionPaperSectionTemplateViewModel> MapToViewModelList(ICollection<QuestionPaperSectionTemplate> questionPaperSectionTemplate)
        //{
        //    List<QuestionPaperSectionTemplateViewModel> questionPaperSectionTemplateViewModelList = (from qpsT in questionPaperSectionTemplate
        //                                                                                             select new QuestionPaperSectionTemplateViewModel()
        //                                                                                             {
        //                                                                                                 ID = qpsT.ID,
        //                                                                                                 SectionName = qpsT.Name,
        //                                                                                                 SectionCutOfMarks = qpsT.CutOffMark != null ? qpsT.CutOffMark.Value : 0,
        //                                                                                                 TimeLimit = qpsT.TimeLimit != null ? qpsT.TimeLimit.Value : 0,
        //                                                                                                 //QuestionPaperID = qpsT.QuestionPaper != null ? qpsT.QuestionPaper.ID : 0,
        //                                                                                                 SectionDescription = qpsT.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                                                                            .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                                                                            .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                                                                            .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                                                                            .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                                                                            .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                                                                            .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                                                                            .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
        //                                                                                                            .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η")
        //                                                                                                            .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ"),
        //                                                                                                 // Mark = qpsT.Mark != null ? qpsT.Mark.Value : 0,
        //                                                                                                 SectionNegativeMark = qpsT.NegativeMark != null ? qpsT.NegativeMark.Value : 0,
        //                                                                                                 SectionCount = qpsT.QuestionPaperSectionTemplateRule.Sum(x => x.Count ?? 0),
        //                                                                                             }
        //                                                                       ).ToList();

        //    return questionPaperSectionTemplateViewModelList;
        //}

        //public static List<QuestionPaperSectionTemplateViewModel> MapToViewModelList(IQueryable<QuestionPaperSectionTemplate> questionPaperSectionTemplate)
        //{
        //    List<QuestionPaperSectionTemplateViewModel> questionPaperSectionTemplateViewModelList = (from qpsT in questionPaperSectionTemplate
        //                                                                                             select new QuestionPaperSectionTemplateViewModel()
        //                                                                                             {
        //                                                                                                 ID = qpsT.ID,
        //                                                                                                 SectionName = qpsT.Name,
        //                                                                                                 SectionCutOfMarks = qpsT.CutOffMark != null ? qpsT.CutOffMark.Value : 0,
        //                                                                                                 TimeLimit = qpsT.TimeLimit != null ? qpsT.TimeLimit.Value : 0,
        //                                                                                                 //QuestionPaperID = qpsT.QuestionPaper != null ? qpsT.QuestionPaper.ID : 0,
        //                                                                                                 SectionDescription = qpsT.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                                                                            .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                                                                            .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                                                                            .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                                                                            .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                                                                            .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                                                                            .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                                                                            .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
        //                                                                                                            .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η")
        //                                                                                                            .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ"),
        //                                                                                                 // Mark = qpsT.Mark != null ? qpsT.Mark.Value : 0,
        //                                                                                                 SectionNegativeMark = qpsT.NegativeMark != null ? qpsT.NegativeMark.Value : 0,
        //                                                                                                 SectionCount = qpsT.QuestionPaperSectionTemplateRule.Sum(x => x.Count ?? 0),
        //                                                                                             }
        //                                                                       ).ToList();

        //    return questionPaperSectionTemplateViewModelList;
        //}

        //public static QuestionPaperSectionTemplateViewModel MapToViewModel(QuestionPaperSectionTemplate questionPaperSectionTemplate)
        //{
        //    QuestionPaperSectionTemplateViewModel questionPaperSectionTemplateViewModel = new QuestionPaperSectionTemplateViewModel();
        //    questionPaperSectionTemplateViewModel.ID = questionPaperSectionTemplate.ID;
        //    questionPaperSectionTemplateViewModel.SectionName = questionPaperSectionTemplate.Name;
        //    questionPaperSectionTemplateViewModel.SectionDescription = questionPaperSectionTemplate.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                                                                            .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                                                                            .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                                                                            .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                                                                            .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                                                                            .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                                                                            .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                                                                            .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
        //                                                                                                            .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η")
        //                                                                                                            .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
        //    questionPaperSectionTemplateViewModel.SectionCutOfMarks = questionPaperSectionTemplate.CutOffMark ?? 0;
        //    questionPaperSectionTemplateViewModel.SectionNegativeMark = questionPaperSectionTemplate.NegativeMark ?? 0;
        //    questionPaperSectionTemplateViewModel.SectionCount = questionPaperSectionTemplate.ID;
        //    questionPaperSectionTemplateViewModel.TimeLimit = questionPaperSectionTemplate.TimeLimit ?? 0;
        //    questionPaperSectionTemplateViewModel.QuestionPaperTemplateID = questionPaperSectionTemplate.QuestionPaperTemplateID ?? 0;
        //    questionPaperSectionTemplateViewModel.TimeLimitTimeSpan = questionPaperSectionTemplate.TimeLimit != null ? TimeSpan.FromMinutes(questionPaperSectionTemplate.TimeLimit.Value) : TimeSpan.Zero;
        //    //questionPaperSectionTemplateViewModel.questionPaperSectionRuleList = MapToViewModelList(questionPaperSectionTemplate.QuestionPaperSectionTemplateRule.AsQueryable(),0);

        //    return questionPaperSectionTemplateViewModel;
        //}



        //public static QuestionPaperSectionTemplate MapToDomainObject(OAPEntities db, QuestionPaperSectionTemplateViewModel questionPaperSectionTemplateViewModel, QuestionPaperSectionTemplate questionPaperSectionTemplate)
        //{
        //    questionPaperSectionTemplate.ID = Convert.ToInt32(questionPaperSectionTemplateViewModel.ID);
        //    questionPaperSectionTemplate.Name = questionPaperSectionTemplateViewModel.SectionName;
        //    questionPaperSectionTemplate.NegativeMark = questionPaperSectionTemplateViewModel.SectionNegativeMark;
        //    questionPaperSectionTemplate.CutOffMark = questionPaperSectionTemplateViewModel.SectionCutOfMarks;
        //    questionPaperSectionTemplate.Description = questionPaperSectionTemplateViewModel.SectionDescription.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                                                                            .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                                                                            .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                                                                            .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                                                                            .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                                                                            .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                                                                            .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                                                                            .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
        //                                                                                                            .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η")
        //                                                                                                            .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
        //    questionPaperSectionTemplate.ModifiedDate = DateTime.Now;
        //    questionPaperSectionTemplate.TimeLimit = questionPaperSectionTemplateViewModel.TimeLimit;

        //    return questionPaperSectionTemplate;
        //}

        //public static List<StudentViewModel> MapToViewModelList(List<Student> studentList)
        //{
        //    List<StudentViewModel> StudentViewModellList = (from stL in studentList
        //                                                    select new StudentViewModel()
        //                                                    {
        //                                                        UniversityRegistryNumber = stL.UniversityRegisterNumber,
        //                                                        Name = stL.Name,

        //                                                    }).ToList();
        //    return StudentViewModellList;
        //}



        //public static TestPackage MapToDomainObject(OAPEntities db, TestPackageViewModel testPackageViewModel)
        //{
        //    return MapToDomainObject(db, testPackageViewModel, new TestPackage());
        //}

        //public static TestPackage MapToDomainObject(OAPEntities db, TestPackageViewModel testPackageViewModel, TestPackage testPackage)
        //{
        //    //TestPackage testPackage = new TestPackage();

        //    testPackage.Name = testPackageViewModel.Name;
        //    testPackage.Description = testPackageViewModel.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                                                                            .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                                                                            .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                                                                            .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                                                                            .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                                                                            .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                                                                            .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                                                                            .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
        //                                                                                                            .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η")
        //                                                                                                            .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
        //    testPackage.IsActive = testPackageViewModel.IsActive;
        //    testPackage.Validity = testPackageViewModel.Validity;
        //    testPackage.Cost = testPackageViewModel.Cost;
        //    testPackage.DispalyOrder = testPackageViewModel.DisplayOrder;
        //    //testPackage.Paper = testPackageViewModel.QuestionPaperTemplateID;
        //    //testPackage.Count = testPackageViewModel.Count;
        //    return testPackage;
        //}

        //public static TestPackageViewModel MapToViewModel(TestPackage testPackage)
        //{
        //    TestPackageViewModel testPackageViewModel = new TestPackageViewModel();
        //    testPackageViewModel.ID = testPackage.ID;
        //    testPackageViewModel.Name = testPackage.Name;
        //    testPackageViewModel.Description = testPackage.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                                                                            .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                                                                            .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                                                                            .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                                                                            .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                                                                            .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                                                                            .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                                                                            .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
        //                                                                                                            .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η")
        //                                                                                                            .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
        //    testPackageViewModel.IsActive = testPackage.IsActive ?? false;

        //    testPackageViewModel.Validity = testPackage.Validity ?? 0;
        //    testPackageViewModel.Cost = testPackage.Cost ?? 0.0;
        //    //testPackageViewModel.QuestionPaperTemplateID = testPackage.Paper??0;
        //    //testPackageViewModel.Count = testPackage.Count??0;

        //    return testPackageViewModel;
        //}

        //public static List<TestPackageViewModel> MapToViewModelList(IQueryable<TestPackage> testPackage)
        //{
        //    List<TestPackageViewModel> testPackageViewModelList = (from s in testPackage
        //                                                           select new TestPackageViewModel()
        //                                                           {
        //                                                               ID = s.ID,
        //                                                               Name = s.Name,
        //                                                               Description = s.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                                                                            .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                                                                            .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                                                                            .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                                                                            .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                                                                            .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                                                                            .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                                                                            .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
        //                                                                                                            .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η")
        //                                                                                                            .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ"),
        //                                                               IsActive = s.IsActive ?? false,
        //                                                               Validity = s.Validity ?? 0,
        //                                                               //QuestionPaperTemplateID = stL.Paper ?? 0,
        //                                                               Cost = s.Cost ?? 0.0,
        //                                                               DisplayOrder = s.DispalyOrder ?? 0,
        //                                                               //Count = s.TestPackageQuestionPaperTemplate.Sum(x => x.Count??0),
        //                                                               //testPackageQuestionPaperTempleteViewModelList=MapToViewModelList(s.TestPackageQuestionPaperTemplate.AsQueryable()),
        //                                                               //Hours = s.Hours,
        //                                                           }).ToList();

        //    return testPackageViewModelList;
        //}


        //public static List<DocumentTemplateViewModel> MapToViewModelList(IQueryable<DocumentTemplate> documentTemplate)
        //{
        //    List<DocumentTemplateViewModel> DocumentTemplateViewModelList = (from s in documentTemplate
        //                                                                     select new DocumentTemplateViewModel()
        //                                                                     {
        //                                                                         ID = s.ID,
        //                                                                         TemplateName = s.TemplateName,
        //                                                                         FilePath = s.FilePath,
        //                                                                         //Hours = s.Hours,
        //                                                                     }).ToList();

        //    return DocumentTemplateViewModelList;
        //}

        //public static DocumentTemplateViewModel MapToViewModel(DocumentTemplate documentTemplate)
        //{
        //    DocumentTemplateViewModel documentTemplateViewModel = new DocumentTemplateViewModel();
        //    documentTemplateViewModel.ID = documentTemplate.ID;
        //    documentTemplateViewModel.TemplateName = documentTemplate.TemplateName;
        //    documentTemplateViewModel.FilePath = documentTemplate.FilePath;


        //    return documentTemplateViewModel;
        //}

        //public static List<TestPackageQuestionPaperTemplateViewModel> MapToViewModelList(IQueryable<TestPackageQuestionPaper> testPackageQuestionPaperTemplate)
        //{
        //    List<TestPackageQuestionPaperTemplateViewModel> testPackageQuestionPaperTemplateViewModelList = (from s in testPackageQuestionPaperTemplate
        //                                                                                                     select new TestPackageQuestionPaperTemplateViewModel()
        //                                                                                                     {

        //                                                                                                         ID = s.ID,
        //                                                                                                         TestPackageID = s.TestPackageID ?? 0,
        //                                                                                                         //Count = s.
        //                                                                                                         QuestionPapertemplateID = s.QuestionPaperTemplateID ?? 0,
        //                                                                                                         QuestionPaperID = s.QuestionPaperID ?? 0,
        //                                                                                                         //Hours = s.Hours,
        //                                                                                                     }).ToList();

        //    return testPackageQuestionPaperTemplateViewModelList;
        //}


        //public static TestPackageQuestionPaperViewModel MapToViewModelForTestPackageTest(QuestionPaper questionPaperCreated)
        //{
        //    TestPackageQuestionPaperViewModel testPackageQuestionPaperViewModel = new TestPackageQuestionPaperViewModel();
        //    testPackageQuestionPaperViewModel.ID = questionPaperCreated.ID;
        //    testPackageQuestionPaperViewModel.Name = questionPaperCreated.Name;
        //    testPackageQuestionPaperViewModel.Code = questionPaperCreated.Code;
        //    testPackageQuestionPaperViewModel.CutOffMark = questionPaperCreated.CutOffMark != null ? questionPaperCreated.CutOffMark.Value : 0;
        //    testPackageQuestionPaperViewModel.Description = questionPaperCreated.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                                                                            .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                                                                            .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                                                                            .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                                                                            .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                                                                            .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                                                                            .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                                                                            .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")

        //                                                                                                            .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η")
        //                                                                                                            .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
        //    testPackageQuestionPaperViewModel.DisplayMarks = questionPaperCreated.DisplayMarks != null ? questionPaperCreated.DisplayMarks.Value : false;
        //    testPackageQuestionPaperViewModel.DisplayResult = questionPaperCreated.DisplayResult != null ? questionPaperCreated.DisplayResult.Value : false;
        //    testPackageQuestionPaperViewModel.EndDate = questionPaperCreated.EndDateTime != null ? questionPaperCreated.EndDateTime.Value : new DateTime();
        //    testPackageQuestionPaperViewModel.ID = questionPaperCreated.ID;
        //    testPackageQuestionPaperViewModel.Instructions = questionPaperCreated.Instructions;
        //    testPackageQuestionPaperViewModel.IsNegativePercentage = questionPaperCreated.IsNegativePercentage != null ? questionPaperCreated.IsNegativePercentage.Value : false;
        //    testPackageQuestionPaperViewModel.Marks = questionPaperCreated.Marks != null ? questionPaperCreated.Marks.Value : 0;
        //    testPackageQuestionPaperViewModel.NegativeMarks = questionPaperCreated.NegativeMarks != null ? questionPaperCreated.NegativeMarks.Value : 0;
        //    testPackageQuestionPaperViewModel.ReleaseKey = questionPaperCreated.ReleaseKey != null ? questionPaperCreated.ReleaseKey.Value : false;
        //    testPackageQuestionPaperViewModel.StartDate = questionPaperCreated.StatDateTime != null ? questionPaperCreated.StatDateTime.Value : new DateTime();
        //    testPackageQuestionPaperViewModel.TimeLimit = questionPaperCreated.TimeLimit != null ? questionPaperCreated.TimeLimit.Value : 0;
        //    testPackageQuestionPaperViewModel.TimeLimitTimeSpan = questionPaperCreated.TimeLimit != null ? TimeSpan.FromMinutes(questionPaperCreated.TimeLimit.Value) : TimeSpan.Zero;

        //    testPackageQuestionPaperViewModel.ShowInstructions = questionPaperCreated.ShowInstruction ?? false;
        //    testPackageQuestionPaperViewModel.ShowReport = questionPaperCreated.ShowReport ?? false;

        //    testPackageQuestionPaperViewModel.questionPaperSectionViewModelList = MapperHelper.MapToViewModelList(questionPaperCreated.QuestionPaperSection);
        //    return testPackageQuestionPaperViewModel;
        //}

        public static long CreateQuestionPaper(long questionID, string questionName, int UserID)
        {
            OAPEntities db = new OAPEntities();
            long returnResult;
            //QuestionPaperViewModel questionPaperViewModel;
            //try
            //{
            long QuestionPaperID = Convert.ToInt64(questionID);
            QuestionPaperTemplate questionPapertemplate = db.QuestionPaperTemplate.Find(QuestionPaperID);
            List<QuestionPaperSectionTemplate> questionPaperSectionTemplateList = db.QuestionPaperSectionTemplate.Where(q => q.QuestionPaperTemplateID == QuestionPaperID).ToList();
            int questionCount = 0;
            int ruleCount = 0;
            foreach (QuestionPaperSectionTemplate questionPaperSectionTemplate in questionPaperSectionTemplateList)
            {
                List<QuestionPaperSectionTemplateRule> QuestionPaperSectionTemplateRuleList = db.QuestionPaperSectionTemplateRule.Where(q => q.QuestionPaperSectionTemplateID == questionPaperSectionTemplate.ID).ToList();
                foreach (QuestionPaperSectionTemplateRule sectionTemplateRule in QuestionPaperSectionTemplateRuleList)
                {
                    ruleCount += sectionTemplateRule.Count ?? 0;
                    List<Tag> tags = sectionTemplateRule.QuestionPaperSectionTemplateRuleTag.Select(q => q.Tag).ToList();
                    List<Subject> Subjects = sectionTemplateRule.QuestionPaperSectionTemplateRuleSubject.Select(q => q.Subject).ToList();
                    List<Complexity> Complexities = sectionTemplateRule.QuestionPaperSectionTemplateRuleComplexity.Select(q => q.Complexity).ToList();
                    List<Question> questionCheckList = new List<Question>();
                    foreach (Tag tag in tags)
                    {
                        questionCheckList = db.QuestionTags.Where(q => q.TagID == tag.ID).Select(q => q.Question).ToList();
                        foreach (Subject subj in Subjects)
                        {
                            questionCheckList = questionCheckList.Where(q => q.SubjectID == subj.ID).ToList();
                            foreach (Complexity complexity in Complexities)
                            {
                                questionCheckList = questionCheckList.Where(q => q.ComplexityID == complexity.ID).ToList();
                            }
                        }
                    }
                    if (questionCheckList != null)
                    {
                        questionCount += questionCheckList.Count();
                    }
                    else
                    {
                        questionCount += 0;
                    }
                }
            }
            if (questionCount >= ruleCount)
            {
                QuestionPaper questionPaper = new QuestionPaper();
                questionPaper.Name = questionName;
                questionPaper.Code = questionPapertemplate.Code;
                questionPaper.Description = questionPapertemplate.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
                                                                                                                    .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
                questionPaper.CutOffMark = questionPapertemplate.CutOffMark;
                questionPaper.DisplayMarks = questionPapertemplate.DisplayMarks;
                questionPaper.DisplayResult = questionPapertemplate.DisplayResult;
                questionPaper.TimeLimit = questionPapertemplate.TimeLimit;
                questionPaper.Marks = questionPapertemplate.Marks;
                questionPaper.NegativeMarks = questionPapertemplate.NegativeMarks;
                questionPaper.IsNegativePercentage = questionPapertemplate.IsNegativePercentage;
                questionPaper.Instructions = questionPapertemplate.Instructions;
                questionPaper.ReleaseKey = questionPapertemplate.ReleaseKey;
                questionPaper.CreatedBy = UserID;
                questionPaper.CreatedDate = DateTime.Now;
                questionPaper.AutoCreatedFromTemplate = true;
                questionPaper.QuestionPaperTemplateID = QuestionPaperID;
                db.QuestionPaper.Add(questionPaper);
                db.SaveChanges();
                List<QuestionPaperSectionTemplateViewModel> questionPaperSectionViewModelList = new List<QuestionPaperSectionTemplateViewModel>();
                foreach (QuestionPaperSectionTemplate questionPaperSectionTemplate in questionPaperSectionTemplateList)
                {
                    QuestionPaperSectionTemplateViewModel questionPaperSectionTemplateViewModel = new QuestionPaperSectionTemplateViewModel();
                    questionPaperSectionTemplateViewModel.ID = questionPaperSectionTemplate.ID;
                    questionPaperSectionTemplateViewModel.QuestionPaperSectionName = questionPaperSectionTemplate.Name;
                    questionPaperSectionTemplateViewModel.TimeLimit = questionPaperSectionTemplate.TimeLimit ?? 0;
                    questionPaperSectionTemplateViewModel.SectionCount = questionPaperSectionTemplate.QuestionPaperSectionTemplateRule.Select(q => q.Count).Sum() ?? 0;
                    questionPaperSectionViewModelList.Add(questionPaperSectionTemplateViewModel);
                    List<QuestionPaperSectionTemplateRule> QuestionPaperSectionTemplateRuleList = db.QuestionPaperSectionTemplateRule.Where(q => q.QuestionPaperSectionTemplateID == questionPaperSectionTemplate.ID).ToList();
                    questionPaperSectionTemplateViewModel.questionPaperSectionRuleList = new List<QuestionPaperSectionTemplateRuleViewModel>();

                    QuestionPaperSection questionPaperSection = new QuestionPaperSection();
                    questionPaperSection.AssessmentID = questionPaper.ID;
                    questionPaperSection.CreatedBy = UserID;
                    questionPaperSection.CreatedDate = DateTime.Now;
                    questionPaperSection.CutOffMark = questionPaperSectionTemplate.CutOffMark;
                    questionPaperSection.Description = questionPaperSectionTemplate.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
                                                                                                                    .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
                    questionPaperSection.IsNegativeMarkPercentage = questionPaperSectionTemplate.IsNegativeMarkPercentage;
                    questionPaperSection.Mark = questionPaperSectionTemplate.Mark;
                    questionPaperSection.Name = questionPaperSectionTemplate.Name;
                    questionPaperSection.NegativeMark = questionPaperSectionTemplate.NegativeMark;
                    db.QuestionPaperSection.Add(questionPaperSection);
                    db.SaveChanges();

                    foreach (QuestionPaperSectionTemplateRule sectionTemplateRule in QuestionPaperSectionTemplateRuleList)
                    {
                        List<Tag> tags = sectionTemplateRule.QuestionPaperSectionTemplateRuleTag.Select(q => q.Tag).ToList();
                        List<Subject> Subjects = sectionTemplateRule.QuestionPaperSectionTemplateRuleSubject.Select(q => q.Subject).ToList();
                        List<Complexity> Complexities = sectionTemplateRule.QuestionPaperSectionTemplateRuleComplexity.Select(q => q.Complexity).ToList();
                        for (int i = 0; i < sectionTemplateRule.Count; i++)
                        {
                            var random = new Random();
                            var Tagitems = new Tag();
                            var Subjectitems = new Subject();
                            var Complexityitems = new Complexity();
                            if (tags.Count() != 0)
                            {
                                var Tag = new List<Tag>();
                                var query = from item in tags
                                            orderby random.Next()
                                            select item;

                                Tagitems = query.FirstOrDefault();
                            }
                            if (Subjects.Count() != 0)
                            {
                                var subjects = new List<Subject>();
                                var Subjectquery = from item in Subjects
                                                   orderby random.Next()
                                                   select item;
                                Subjectitems = Subjectquery.FirstOrDefault();
                            }
                            if (Complexities.Count() != 0)
                            {
                                var complexities = new List<Complexity>();
                                var Complexityquery = from item in Complexities
                                                      orderby random.Next()
                                                      select item;
                                Complexityitems = Complexityquery.FirstOrDefault();
                            }
                            long complexdityID = (Complexityitems != null ? Complexityitems.ID : 0);
                            List<Question> questionList = new List<Question>();

                            if (Tagitems.ID != 0 && Subjectitems.ID != 0 && complexdityID != 0)
                            {
                                questionList = db.Question.Where(q => q.QuestionTags.Where(qt => qt.TagID == Tagitems.ID).Count() > 0 && q.SubjectID == Subjectitems.ID && q.ComplexityID == complexdityID).ToList();
                            }
                            else if (Tagitems.ID != 0 && Subjectitems.ID == 0 && complexdityID == 0)
                            {
                                questionList = db.Question.Where(q => q.QuestionTags.Where(qt => qt.TagID == Tagitems.ID).Count() > 0).ToList();
                            }
                            else if (Tagitems.ID != 0 && Subjectitems.ID != 0 && complexdityID == 0)
                            {
                                questionList = db.Question.Where(q => q.QuestionTags.Where(qt => qt.TagID == Tagitems.ID).Count() > 0 && q.SubjectID == Subjectitems.ID).ToList();
                            }
                            else if (Tagitems.ID != 0 && Subjectitems.ID == 0 && complexdityID != 0)
                            {
                                questionList = db.Question.Where(q => q.QuestionTags.Where(qt => qt.TagID == Tagitems.ID).Count() > 0 && q.ComplexityID == complexdityID).ToList();
                            }

                            if (questionList.Count() > 0)
                            {
                                var result = new List<Question>();
                                var Questionquery = from item in questionList
                                                    orderby random.Next()
                                                    select item;
                                var question = Questionquery.FirstOrDefault();
                                QuestionPaperSectionQuestion questionPaperSectionQuestion = new QuestionPaperSectionQuestion();
                                questionPaperSectionQuestion.AssessmentSectionID = questionPaperSection.ID;
                                questionPaperSectionQuestion.QuestionID = question.ID;
                                questionPaperSectionQuestion.Mark = question.Marks;
                                questionPaperSectionQuestion.NegativeMark = question.NegativeMarks;
                                db.QuestionPaperSectionQuestion.Add(questionPaperSectionQuestion);
                                db.SaveChanges();
                            }
                        }
                    }
                }

                UserProfile userProfile = db.UserProfile.Find(UserID);
                //Student student = db.Student.Where(d => d.ID == userProfile.StudentID).FirstOrDefault();
                StudentAssessment studentQuestionPaper = new StudentAssessment();
                studentQuestionPaper.StudentID = userProfile.StudentID;
                studentQuestionPaper.QuestionPaperID = questionPaper.ID;
                db.StudentAssessment.Add(studentQuestionPaper);
                db.SaveChanges();

                return returnResult = questionPaper.ID;
            }
            else
            {
                return returnResult = 0;
            }
            //}
            //catch
            //{
            //    Response.Write(false);

            //}
        }

        //public static int TimeTaken { get; set; }

        //public static string AnswerDescription { get; set; }

        //public static List<UploadQuestionViewModel> MapToViewModelList(IQueryable<TempUploads> tempUploads)
        //{
        //    List<UploadQuestionViewModel> uploadQuestionViewModelList = (from u in tempUploads
        //                                                                 select new UploadQuestionViewModel()
        //                                                                 {

        //                                                                     ID = u.ID,
        //                                                                     Title = u.Title,
        //                                                                     UploadedDate = u.UploadedDate,
        //                                                                     // UploadedDateTimeString = u.UploadedDate.ToString("dd-MM-yyy HH:mm "),
        //                                                                     UploadedUserID = u.UploadedUserID ?? 0,
        //                                                                     QuestionCount = u.TempQuestion.Count(),
        //                                                                 }).ToList();
        //    return uploadQuestionViewModelList;
        //}



        //public static PassageViewModel MapToViewModel(TempPassage passage)
        //{
        //    PassageViewModel passageViewModel = new PassageViewModel();
        //    passageViewModel.ID = passage.ID;
        //    passageViewModel.Description = passage.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                                                                            .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                                                                            .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                                                                            .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                                                                            .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                                                                            .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                                                                            .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                                                                            .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
        //                                                                                                            .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η")
        //                                                                                                            .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
        //    passageViewModel.LastUpdated = passage.LastUpdated != null ? passage.LastUpdated.Value : DateTime.Now;
        //    passageViewModel.CanShuffle = passage.CanShuffle != null ? passage.CanShuffle.Value : false;
        //    return passageViewModel;
        //}

        //public static List<AnswerViewModel> MapToViewModelList(IQueryable<TempAnswer> answers)
        //{
        //    List<AnswerViewModel> anwerViewModelList = (from a in answers
        //                                                select new AnswerViewModel()
        //                                                {
        //                                                    ID = a.ID,
        //                                                    Description = a.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
        //                                                                                                            .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
        //                                                                                                            .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
        //                                                                                                            .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
        //                                                                                                            .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
        //                                                                                                            .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
        //                                                                                                            .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
        //                                                                                                            .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
        //                                                                                                            .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]","¶").Replace("[eta]","η")
        //                                                                                                            .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ"),
        //                                                    isCorrect = a.IsCorrect != null ? a.IsCorrect.Value : false,
        //                                                    LastUpdated = a.LastUpdated != null ? a.LastUpdated.Value : new DateTime(),
        //                                                    QuestionID = a.TempQuestion != null ? a.TempQuestion.ID : 0,
        //                                                }).ToList();

        //    return anwerViewModelList;
        //}


        //public static List<TagViewModel> MapToViewModelList(IQueryable<TempQuestionTags> tagsList)
        //{
        //    List<TagViewModel> tagViewModelList = (from t in tagsList
        //                                           select new TagViewModel()
        //                                           {
        //                                               ID = t.ID,
        //                                               Name = t.TempTag.Name,

        //                                           }).ToList();
        //    return tagViewModelList;
        //}




        #region Member


        public static List<MemberViewModel> MapToViewModelList(IQueryable<UserProfile> members)
        {

            List<MemberViewModel> memberViewModelList = (from m in members
                                                         select new MemberViewModel()
                                                         {
                                                             UserName = m.UserName,
                                                             EmailID = m.EmailID,
                                                             ID = m.UserId,
                                                             OrganisationName = m.UserOrganisation.FirstOrDefault() != null ? m.UserOrganisation.FirstOrDefault().OrganisationName : "",
                                                             OrganisationAddress1 = m.UserOrganisation.FirstOrDefault() != null ? m.UserOrganisation.FirstOrDefault().OrganisationAddress1 : "",
                                                             OrganisationAddress2 = m.UserOrganisation.FirstOrDefault() != null ? m.UserOrganisation.FirstOrDefault().OrganisationAddress2 : "",
                                                             ContactNumber1 = m.UserOrganisation.FirstOrDefault() != null ? m.UserOrganisation.FirstOrDefault().ContactNumber1 : "",
                                                             ContactNumber2 = m.UserOrganisation.FirstOrDefault() != null ? m.UserOrganisation.FirstOrDefault().ContactNumber2 : "",
                                                             //  UserOrganisationViewModel = MapToViewModel(m.UserOrganisation.FirstOrDefault()),
                                                         }).ToList();

            return memberViewModelList;

        }


        public static MemberViewModel MapToViewModel(UserProfile userProfile)
        {
            MemberViewModel memberViewModel = new MemberViewModel();
            memberViewModel.ID = userProfile.UserId;
            memberViewModel.UserName = userProfile.UserName;
            memberViewModel.EmailID = userProfile.EmailID;
            memberViewModel.OrganisationName = userProfile.UserOrganisation.FirstOrDefault() != null ? userProfile.UserOrganisation.FirstOrDefault().OrganisationName : "";
            memberViewModel.OrganisationAddress1 = userProfile.UserOrganisation.FirstOrDefault() != null ? userProfile.UserOrganisation.FirstOrDefault().OrganisationAddress1 : "";
            memberViewModel.OrganisationAddress2 = userProfile.UserOrganisation.FirstOrDefault() != null ? userProfile.UserOrganisation.FirstOrDefault().OrganisationAddress2 : "";
            memberViewModel.ContactNumber1 = userProfile.UserOrganisation.FirstOrDefault() != null ? userProfile.UserOrganisation.FirstOrDefault().ContactNumber1 : "";
            memberViewModel.ContactNumber2 = userProfile.UserOrganisation.FirstOrDefault() != null ? userProfile.UserOrganisation.FirstOrDefault().ContactNumber2 : "";
            memberViewModel.UserOrganisationViewModel = MapToViewModel(userProfile.UserOrganisation.FirstOrDefault());
            return memberViewModel;
        }



        #endregion



        #region UserOrganisation



        public static UserOrganisationViewModel MapToViewModel(UserOrganisation userOrganisation)
        {
            UserOrganisationViewModel userOrganisationViewModel = new UserOrganisationViewModel();
            if (userOrganisation != null)
            {
                userOrganisationViewModel.ID = userOrganisation.ID;
                userOrganisationViewModel.UserId = userOrganisation.UserId ?? 0;
                userOrganisationViewModel.OrganisationName = userOrganisation.OrganisationName;
                userOrganisationViewModel.OrgLogo = userOrganisation.OrgLogo;
                userOrganisationViewModel.OrganisationAddress1 = userOrganisation.OrganisationAddress1;
                userOrganisationViewModel.OrganisationAddress2 = userOrganisation.OrganisationAddress2;
                userOrganisationViewModel.ContactNumber1 = userOrganisation.ContactNumber1;
                userOrganisationViewModel.ContactNumber2 = userOrganisation.ContactNumber2;
            }
            return userOrganisationViewModel;
        }

        public static UserOrganisation MapToDomainObject(OAPEntities db, UserOrganisationViewModel userOrganisationViewModel)
        {
            return MapToDomainObject(db, userOrganisationViewModel, new UserOrganisation());
        }

        public static UserOrganisation MapToDomainObject(OAPEntities db, UserOrganisationViewModel userOrganisationViewModel, UserOrganisation userOrganisation)
        {
            userOrganisation.ID = userOrganisationViewModel.ID;
            userOrganisation.UserId = userOrganisationViewModel.UserId;
            userOrganisation.OrganisationName = userOrganisationViewModel.OrganisationName;
            userOrganisation.OrgLogo = userOrganisationViewModel.OrgLogo;
            userOrganisation.OrganisationAddress1 = userOrganisationViewModel.OrganisationAddress1;
            userOrganisation.OrganisationAddress2 = userOrganisationViewModel.OrganisationAddress2;
            userOrganisation.ContactNumber1 = userOrganisationViewModel.ContactNumber1;
            userOrganisation.ContactNumber2 = userOrganisationViewModel.ContactNumber2;
            return userOrganisation;
        }



        #endregion


        #region Tag

        public static List<TagViewModel> MapToViewModelList(IQueryable<Tag> tagsList)
        {
            List<TagViewModel> tagViewModelList = (from t in tagsList
                                                   select new TagViewModel()
                                                   {
                                                       ID = t.ID,
                                                       Name = t.Name,
                                                   }).ToList();
            return tagViewModelList;
        }

        public static TagViewModel MapToViewModel(Tag tags)
        {
            TagViewModel tagViewModel = new TagViewModel();
            tagViewModel.ID = tags.ID;
            tagViewModel.Name = tags.Name;
            return tagViewModel;
        }

        public static Tag MapToDomainObject(OAPEntities db, TagViewModel tagViewModel)
        {
            return MapToDomainObject(db, tagViewModel, new Tag());
        }

        public static Tag MapToDomainObject(OAPEntities db, TagViewModel tagViewModel, Tag tags)
        {
            tags.ID = tagViewModel.ID;
            tags.Name = tagViewModel.Name;

            return tags;
        }

        #endregion




        #region Answer

        public static List<AnswerViewModel> MapToViewModelList(IQueryable<Answer> answers)
        {
            List<AnswerViewModel> anwerViewModelList = (from a in answers
                                                        select new AnswerViewModel()
                                                        {
                                                            ID = a.ID,
                                                            Description = a.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
                                                                                                                    .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ"),
                                                            isCorrect = a.IsCorrect != null ? a.IsCorrect.Value : false,
                                                            LastUpdated = a.LastUpdated != null ? a.LastUpdated.Value : new DateTime(),
                                                            QuestionID = a.Question != null ? a.Question.ID : 0,
                                                        }).ToList();

            return anwerViewModelList;
        }

        public static List<AnswerViewModel> MapToViewModelListTemp(IQueryable<TempAnswer> answers)
        {
            List<AnswerViewModel> anwerViewModelList = (from a in answers
                                                        select new AnswerViewModel()
                                                        {
                                                            ID = a.ID,
                                                            Description = a.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ")
                                                                                                                    .Replace("[drachma]", "₯").Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ")
                                                                                                                    .Replace("[para]", "¶").Replace("[eta]", "η").Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ"),
                                                            isCorrect = a.IsCorrect != null ? a.IsCorrect.Value : false,
                                                            LastUpdated = a.LastUpdated != null ? a.LastUpdated.Value : new DateTime(),
                                                            QuestionID = a.TempQuestion != null ? a.TempQuestion.ID : 0,
                                                        }).ToList();

            return anwerViewModelList;
        }


        public static AnswerViewModel MapToViewModel(Answer answer)
        {
            AnswerViewModel answerViewModel = new AnswerViewModel();
            answerViewModel.ID = answer.ID;
            answerViewModel.Description = answer.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
                                                                                                                    .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
            answerViewModel.isCorrect = answer.IsCorrect != null ? answer.IsCorrect.Value : false;
            answerViewModel.QuestionID = answer.Question != null ? answer.Question.ID : 0;
            return answerViewModel;
        }

        public static AnswerViewModel MapToViewModelTemp(TempAnswer answer)
        {
            AnswerViewModel answerViewModel = new AnswerViewModel();
            answerViewModel.ID = answer.ID;
            answerViewModel.Description = answer.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
                                                                                                                    .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
            answerViewModel.isCorrect = answer.IsCorrect != null ? answer.IsCorrect.Value : false;
            answerViewModel.QuestionID = answer.TempQuestion != null ? answer.TempQuestion.ID : 0;
            return answerViewModel;
        }
        #endregion


        #region QuestionPaper


        public static List<QuestionPaperViewModel> MapToViewModelList(IEnumerable<QuestionPaper> questionpapers)
        {
            List<QuestionPaperViewModel> assessmentViewModelList = (from a in questionpapers
                                                                    select new QuestionPaperViewModel()
                                                                    {
                                                                        ID = a.ID,
                                                                        Name = a.Name,
                                                                        Code = a.Code,
                                                                        CutOffMark = a.CutOffMark != null ? a.CutOffMark.Value : 0,
                                                                        Description = a.Description != null ? a.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
                                                                                                                    .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ") : "",
                                                                        DisplayMarks = a.DisplayMarks != null ? a.DisplayMarks.Value : false,
                                                                        DisplayResult = a.DisplayResult != null ? a.DisplayResult.Value : false,
                                                                        EndDate = a.EndDateTime != null ? a.EndDateTime.Value : new DateTime(),
                                                                        StartDate = a.StatDateTime != null ? a.StatDateTime.Value : new DateTime(),
                                                                        Instructions = a.Instructions,
                                                                        Marks = a.Marks != null ? a.Marks.Value : 0,
                                                                        NegativeMarks = a.NegativeMarks != null ? a.NegativeMarks.Value : 0,
                                                                        IsNegativePercentage = a.IsNegativePercentage != null ? a.IsNegativePercentage.Value : false,
                                                                        ReleaseKey = a.ReleaseKey != null ? a.ReleaseKey.Value : false,
                                                                        TimeLimit = a.TimeLimit != null ? a.TimeLimit.Value : 0,
                                                                        ShowInstructions = a.ShowInstruction ?? false,
                                                                        ShowReport = a.ShowReport ?? false,
                                                                        createdDate = a.CreatedDate,
                                                                        CanJumpSections = a.CanJumpSections ?? false,
                                                                        IsHaveRetestRequest = a.IsHaveRetestRequest ?? false,
                                                                        CanShuffle = a.CanShuffle ?? false,

                                                                    }).ToList();

            return assessmentViewModelList;
        }

        public static QuestionPaper MapToDomainObject(OAPEntities db, QuestionPaperViewModel questionPaperViewModel)
        {
            return MapToDomainObject(db, questionPaperViewModel, new QuestionPaper());
        }

        public static QuestionPaper MapToDomainObject(OAPEntities db, QuestionPaperViewModel questionPaperViewModel, QuestionPaper questionPaper)
        {
            questionPaper.ID = questionPaperViewModel.ID;
            questionPaper.Name = questionPaperViewModel.Name;
            questionPaper.Code = questionPaperViewModel.Code;
            questionPaper.CutOffMark = questionPaperViewModel.CutOffMark;
            //questionPaper.Description = questionPaperViewModel.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
            //                                                                                                        .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
            //                                                                                                        .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
            //                                                                                                        .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
            //                                                                                                        .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
            //                                                                                                        .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
            //                                                                                                        .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
            //                                                                                                        .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
            //                                                                                                        .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
            //                                                                                                        .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
            questionPaper.Description = questionPaperViewModel.Description;
            questionPaper.DisplayMarks = questionPaperViewModel.DisplayMarks;
            questionPaper.DisplayResult = questionPaperViewModel.DisplayResult;
            questionPaper.EndDateTime = questionPaperViewModel.EndDate;
            questionPaper.Instructions = questionPaperViewModel.Instructions;
            questionPaper.CanJumpSections = questionPaperViewModel.CanJumpSections;
            questionPaper.IsNegativePercentage = questionPaperViewModel.IsNegativePercentage;
            questionPaper.Marks = questionPaperViewModel.Marks;
            questionPaper.NegativeMarks = questionPaperViewModel.NegativeMarks;
            questionPaper.ReleaseKey = questionPaperViewModel.ReleaseKey;
            questionPaper.StatDateTime = questionPaperViewModel.StartDate;
            questionPaper.TimeLimit = questionPaperViewModel.TimeLimit;
            questionPaperViewModel.TimeLimitTimeSpan = questionPaperViewModel.TimeLimit != 0 ? TimeSpan.FromMinutes(questionPaper.TimeLimit.Value) : TimeSpan.Zero;
            //assessment.College = db.College.Find(questionPaperViewModel.CollegeID);
            questionPaper.ModifiedDate = DateTime.Now;

            questionPaper.ShowInstruction = questionPaperViewModel.ShowInstructions;
            questionPaper.ShowReport = questionPaperViewModel.ShowReport;
            questionPaper.IsHaveRetestRequest = questionPaperViewModel.IsHaveRetestRequest;
            questionPaper.CanShuffle = questionPaperViewModel.CanShuffle;
            return questionPaper;
        }

        public static QuestionPaperViewModel MapToViewModel(QuestionPaper questionPaper)
        {
            QuestionPaperViewModel questionPaperViewModel = new QuestionPaperViewModel();
            questionPaperViewModel.ID = questionPaper.ID;
            questionPaperViewModel.Name = questionPaper.Name;
            questionPaperViewModel.Code = questionPaper.Code;
            questionPaperViewModel.CutOffMark = questionPaper.CutOffMark != null ? questionPaper.CutOffMark.Value : 0;
            questionPaperViewModel.Description = questionPaper.Description;
            questionPaperViewModel.DisplayMarks = questionPaper.DisplayMarks != null ? questionPaper.DisplayMarks.Value : false;
            questionPaperViewModel.DisplayResult = questionPaper.DisplayResult != null ? questionPaper.DisplayResult.Value : false;
            questionPaperViewModel.EndDate = questionPaper.EndDateTime != null ? questionPaper.EndDateTime.Value : new DateTime();
            questionPaperViewModel.ID = questionPaper.ID;
            questionPaperViewModel.Instructions = questionPaper.Instructions;
            questionPaperViewModel.IsNegativePercentage = questionPaper.IsNegativePercentage != null ? questionPaper.IsNegativePercentage.Value : false;
            questionPaperViewModel.Marks = questionPaper.Marks != null ? questionPaper.Marks.Value : 0;
            questionPaperViewModel.NegativeMarks = questionPaper.NegativeMarks != null ? questionPaper.NegativeMarks.Value : 0;
            questionPaperViewModel.ReleaseKey = questionPaper.ReleaseKey != null ? questionPaper.ReleaseKey.Value : false;
            questionPaperViewModel.StartDate = questionPaper.StatDateTime != null ? questionPaper.StatDateTime.Value : new DateTime();
            questionPaperViewModel.TimeLimit = questionPaper.TimeLimit != null ? questionPaper.TimeLimit.Value : 0;
            questionPaperViewModel.TimeLimitTimeSpan = questionPaper.TimeLimit != null ? TimeSpan.FromMinutes(questionPaper.TimeLimit.Value) : TimeSpan.Zero;

            questionPaperViewModel.ShowInstructions = questionPaper.ShowInstruction ?? false;
            questionPaperViewModel.ShowReport = questionPaper.ShowReport ?? false;
            questionPaperViewModel.CanJumpSections = questionPaper.CanJumpSections ?? false;

            questionPaperViewModel.questionPaperSectionViewModelList = MapperHelper.MapToViewModelList(questionPaper.QuestionPaperSection);

            questionPaperViewModel.IsHaveRetestRequest = questionPaper.IsHaveRetestRequest ?? false;
            questionPaperViewModel.CanShuffle = questionPaper.CanShuffle ?? false;
            return questionPaperViewModel;
        }


        public static QuestionPaperViewModel MapToViewModelForTestList(QuestionPaper questionPaper, BatchAssessment batchAssessment)
        {
            QuestionPaperViewModel questionPaperViewModel = new QuestionPaperViewModel();
            questionPaperViewModel.ID = questionPaper.ID;
            questionPaperViewModel.Name = questionPaper.Name;
            questionPaperViewModel.Code = questionPaper.Code;
            questionPaperViewModel.Instructions = questionPaper.Instructions;
            questionPaperViewModel.Marks = questionPaper.Marks != null ? questionPaper.Marks.Value : 0;
            questionPaperViewModel.NegativeMarks = questionPaper.NegativeMarks != null ? questionPaper.NegativeMarks.Value : 0;
            questionPaperViewModel.StartDate = batchAssessment.StartDateTime;
            questionPaperViewModel.EndDate = batchAssessment.EndDateTime;
            questionPaperViewModel.TimeLimit = questionPaper.TimeLimit != null ? questionPaper.TimeLimit.Value : 0;
            questionPaperViewModel.TimeLimitTimeSpan = questionPaper.TimeLimit != null ? TimeSpan.FromMinutes(questionPaper.TimeLimit.Value) : TimeSpan.Zero;
            questionPaperViewModel.ShowInstructions = questionPaper.ShowInstruction ?? false;
            questionPaperViewModel.ShowReport = questionPaper.ShowReport ?? false;
            questionPaperViewModel.CanJumpSections = questionPaper.CanJumpSections ?? false;
            questionPaperViewModel.questionPaperSectionViewModelList = MapperHelper.MapToViewModelList(questionPaper.QuestionPaperSection);
            questionPaperViewModel.IsHaveRetestRequest = questionPaper.IsHaveRetestRequest ?? false;
            questionPaperViewModel.CanShuffle = questionPaper.CanShuffle ?? false;
            return questionPaperViewModel;
        }


        #endregion


        #region QuestionPaper Section

        public static List<QuestionPaperSectionViewModel> MapToViewModelList(IEnumerable<QuestionPaperSection> questionPaperSections)
        {
            List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList = (from s in questionPaperSections
                                                                                     select new QuestionPaperSectionViewModel()
                                                                                     {
                                                                                         ID = s.ID,
                                                                                         Name = s.Name,
                                                                                         CutOffMark = s.CutOffMark != null ? s.CutOffMark.Value : 0,
                                                                                         TimeLimit = s.TimeLimit != null ? s.TimeLimit.Value : 0,
                                                                                         QuestionPaperID = s.QuestionPaper != null ? s.QuestionPaper.ID : 0,
                                                                                         Description = s.Description != null ? s.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
                                                                                                                    .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ") : "",
                                                                                         Mark = s.Mark != null ? s.Mark.Value : 0,
                                                                                         NegativeMark = s.NegativeMark != null ? s.NegativeMark.Value : 0,
                                                                                         TotalQuestionCount = s.QuestionPaperSectionQuestion.Count() == 0 ? s.TechnicalQuestionPaperSectionQuestion.Count() : s.QuestionPaperSectionQuestion.Count(),
                                                                                         TimeLimitTimeSpan = s.TimeLimit != null ? TimeSpan.FromMinutes(s.TimeLimit != null ? s.TimeLimit.Value : 0) : TimeSpan.Zero,

                                                                                     }).ToList();

            return questionPaperSectionViewModelList;
        }

        public static QuestionPaperSection MapToDomainObject(OAPEntities db, QuestionPaperSectionViewModel questionPaperSectionViewModel, QuestionPaperSection questionPaperSection)
        {
            questionPaperSection.ID = questionPaperSectionViewModel.ID;
            questionPaperSection.Mark = questionPaperSectionViewModel.Mark;
            questionPaperSection.Name = questionPaperSectionViewModel.Name;
            questionPaperSection.NegativeMark = questionPaperSectionViewModel.NegativeMark;
            questionPaperSection.TimeLimit = questionPaperSectionViewModel.TimeLimit;
            questionPaperSection.QuestionPaper = db.QuestionPaper.Find(questionPaperSectionViewModel.QuestionPaperID);
            questionPaperSection.CutOffMark = questionPaperSectionViewModel.CutOffMark;
            //questionPaperSection.Description = questionPaperSectionViewModel.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
            //                                                                                                        .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
            //                                                                                                        .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
            //                                                                                                        .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
            //                                                                                                        .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
            //                                                                                                        .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
            //                                                                                                        .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
            //                                                                                                        .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
            //                                                                                                        .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
            //                                                                                                        .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
            questionPaperSection.Description = questionPaperSectionViewModel.Description;

            questionPaperSection.ModifiedDate = DateTime.Now;
            return questionPaperSection;

        }

        #endregion

        #region Passage

        public static List<PassageViewModel> MapToViewModelList(IQueryable<Passage> passages)
        {
            List<PassageViewModel> passageViewModelList = (from p in passages
                                                           select new PassageViewModel()
                                                           {
                                                               ID = p.ID,
                                                               Description = p.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
                                                                                                                    .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ"),
                                                               LastUpdated = p.LastUpdated != null ? p.LastUpdated.Value : DateTime.Now,
                                                               CanShuffle = p.CanShuffle != null ? p.CanShuffle.Value : false,
                                                           }).ToList();
            return passageViewModelList;
        }

        public static PassageViewModel MapToViewModel(Passage passage)
        {
            PassageViewModel passageViewModel = new PassageViewModel();
            passageViewModel.ID = passage.ID;
            passageViewModel.Description = passage.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
                                                                                                                    .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
            passageViewModel.LastUpdated = passage.LastUpdated != null ? passage.LastUpdated.Value : DateTime.Now;
            passageViewModel.CanShuffle = passage.CanShuffle != null ? passage.CanShuffle.Value : false;
            return passageViewModel;
        }

        public static PassageViewModel MapToViewModelTemp(TempPassage passage)
        {
            PassageViewModel passageViewModel = new PassageViewModel();
            passageViewModel.ID = passage.ID;
            passageViewModel.Description = passage.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
                                                                                                                    .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
            passageViewModel.LastUpdated = passage.LastUpdated != null ? passage.LastUpdated.Value : DateTime.Now;
            passageViewModel.CanShuffle = passage.CanShuffle != null ? passage.CanShuffle.Value : false;
            return passageViewModel;
        }

        public static Passage MapToDomainObject(OAPEntities db, PassageViewModel passageViewModel)
        {
            return MapToDomainObject(db, passageViewModel, new Passage());
        }

        public static Passage MapToDomainObject(OAPEntities db, PassageViewModel passageViewModel, Passage passage)
        {
            passage.ID = passageViewModel.ID;
            passage.Description = passageViewModel.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
                                                                                                                    .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
            passage.LastUpdated = passageViewModel.LastUpdated;
            passage.CanShuffle = passageViewModel.CanShuffle;
            passage.ModifiedDate = DateTime.Now;
            return passage;
        }

        public static TempPassage MapToDomainObjectTemp(OAPEntities db, PassageViewModel passageViewModel, TempPassage passage)
        {
            passage.ID = passageViewModel.ID;
            passage.Description = passageViewModel.Description.Replace("[infin]", "∞").Replace("[prod]", "∏").Replace("[sqrt]", "√").Replace("[int]", "∫").Replace("[isin]", "∈").Replace("[notin]", "∉")
                                                                                                                    .Replace("[ni]", "∋").Replace("[forall]", "∀").Replace("[prop]", "∝").Replace("[part]", "∂").Replace("[larr]", "←").Replace("[pi]", "π").Replace("[Pi]", "Π")
                                                                                                                    .Replace("[frasl]", "⁄").Replace("[Sigma]", "Σ").Replace("[plusmn]", "±").Replace("[yen]", "¥").Replace("[circ]", "ˆ").Replace("[nabla]", "∇")
                                                                                                                    .Replace("[Delta]", "Δ").Replace("[ordm]", "º").Replace("[alpha]", "α").Replace("[pound]", "£").Replace("[hellip]", "…").Replace("[beta]", "β")
                                                                                                                    .Replace("[gamma]", "γ").Replace("[theta]", "θ").Replace("[phi]", "φ").Replace("[thetasym]", "ϑ").Replace("[rho]", "ρ").Replace("[equiv]", "≡")
                                                                                                                    .Replace("[epsilon]", "ε").Replace("[lambda]", "λ").Replace("[fnof]", "ƒ").Replace("[mu]", "μ").Replace("hArr", "⇔").Replace("[harr]", "↔")
                                                                                                                    .Replace("[ang]", "∠").Replace("[and]", "∧").Replace("[or]", "∨").Replace("[cap]", "∩").Replace("[cup]", "∪").Replace("[sim]", "∼").Replace("[psi]", "ψ")
                                                                                                                    .Replace("[cong]", "≅").Replace("[rarr]", "→").Replace("[ne]", "≠").Replace("[omega]", "ω").Replace("[diffepsilon]", "ϵ").Replace("[drachma]", "₯")
                                                                                                                    .Replace("[Graphemica]", "⪔").Replace("[sum]", "∑").Replace("[delta]", "δ").Replace("[micro]", "µ").Replace("[para]", "¶").Replace("[eta]", "η")
                                                                                                                    .Replace("[sub]", "⊂").Replace("[sup]", "⊃").Replace("[nsub]", "⊄").Replace("[sube]", "⊆").Replace("[supe]", "⊇").Replace("[Psi]", "Ψ");
            passage.LastUpdated = passageViewModel.LastUpdated;
            passage.CanShuffle = passageViewModel.CanShuffle;
            passage.ModifiedDate = DateTime.Now;
            return passage;
        }

        #endregion


        #region Batch

        public static List<BatchViewModel> MapToViewModelList(IQueryable<Batch> batches)
        {
            List<BatchViewModel> batchViewModelList = (from b in batches
                                                       select new BatchViewModel
                                                       {
                                                           ID = b.ID,
                                                           Name = b.Name,
                                                           StudentCount = b.BatchStudent.Select(x => x.Student).Count(),
                                                       }).ToList();
            return batchViewModelList;
        }

        public static BatchViewModel MapToViewModel(Batch batch)
        {
            BatchViewModel batchViewModel = new BatchViewModel();
            batchViewModel.ID = batch.ID;
            batchViewModel.Name = batch.Name;
            return batchViewModel;
        }

        public static Batch MapToDomainObject(OAPEntities db, BatchViewModel batchViewModel)
        {
            return MapToDomainObject(db, batchViewModel, new Batch());
        }

        public static Batch MapToDomainObject(OAPEntities db, BatchViewModel batchViewModel, Batch batch)
        {
            batch.ID = batchViewModel.ID;
            batch.Name = batchViewModel.Name;
            return batch;
        }

        #endregion

        #region QuestionWiseAnalysis
        public static List<QuestionViewModel> MapToViewModelListforReport(IQueryable<Question> questions, long questionPaperID = 0, long userAssessmentID = 0)
        {
            List<QuestionViewModel> questionViewModelList = (from q in questions
                                                             select new QuestionViewModel()
                                                             {
                                                                 ID = q.ID,
                                                                 TagNames = string.Join(",", q.QuestionTags.Select(x => x.Tag.Name)),
                                                                 SectionName = q.QuestionPaperSectionQuestion.Where(x => x.QuestionPaperSection.AssessmentID == questionPaperID).FirstOrDefault().QuestionPaperSection.Name,
                                                                 TimeTaken = q.AssessmentSnapshot.Where(asp => asp.UserAssessment.ID == userAssessmentID).Count() > 0 ? q.AssessmentSnapshot.Where(asp => asp.UserAssessment.ID == userAssessmentID).FirstOrDefault().TimeTaken != null ? q.AssessmentSnapshot.Where(asp => asp.UserAssessment.ID == userAssessmentID).FirstOrDefault().TimeTaken.Value : 0 : 0,
                                                                 Status = q.AssessmentSnapshot.Where(asp => asp.UserAssessment.ID == userAssessmentID).Count() > 0 ? q.AssessmentSnapshot.Where(asp => asp.UserAssessment.ID == userAssessmentID).FirstOrDefault().Answer.IsCorrect : false,
                                                                 //Status = q.AssessmentSnapshot.Where(x => x.UserAssessment.ID == UserAssessmentID).Select(z=>z.)
                                                                 //StudentAnsweredViewModelList =MapToViewModelList(q.AssessmentSnapshot.Where(asp => asp.QuestionID == q.ID && asp.UserAssessmentID == userAssessmentID && asp.UserAssessment.AssessmentID == questionPaperID).Where(x => x.Answer != null).Select(x => x.Answer).AsQueryable()),
                                                                 // AnswerViewModelList=MapToViewModelList(q.Answer.AsQueryable()),
                                                             }).ToList();
            return questionViewModelList;
        }
        #endregion


        #region Student Resume

        public static StudentResumeViewModel MapToViewModel(StudentResume studentResume)
        {

            StudentResumeViewModel studentResumeViewModel = new StudentResumeViewModel();
            studentResumeViewModel.ID = studentResume.ID;
            studentResumeViewModel.FilePath = studentResume.FilePath;
            studentResumeViewModel.ModifiedDateTime = studentResume.ModifiedDate ?? new DateTime(1900, 01, 01);
            studentResumeViewModel.UploadedDateTime = studentResume.UploadedDate;
            studentResumeViewModel.StudentID = studentResume.StudentID;
            studentResumeViewModel.StudentName = studentResume.Student != null ? studentResume.Student.Name : "";

            return studentResumeViewModel;
        }

        #endregion

        #region Practice School
        public static PracticeSchool MapToDomainObject(PracticeSchoolViewModel practiceSchoolViewModel)
        {
            return MapToDomainObject(practiceSchoolViewModel, new PracticeSchool());
        }

        public static PracticeSchool MapToDomainObject(PracticeSchoolViewModel practiceSchoolViewModel, PracticeSchool practiceSchool)
        {
            if (practiceSchool == null)
            {
                practiceSchool = new PracticeSchool();
            }
            practiceSchool.ID = practiceSchoolViewModel.ID;
            practiceSchool.SessionName = practiceSchoolViewModel.SessionName;
            practiceSchool.Section1 = practiceSchoolViewModel.Section1;
            practiceSchool.Section2 = practiceSchoolViewModel.Section2;
            practiceSchool.Section3 = practiceSchoolViewModel.Section3;
            practiceSchool.Section4 = practiceSchoolViewModel.Section4;
            practiceSchool.IsTechnicalSession = practiceSchoolViewModel.IsTechnicalSession;
            return practiceSchool;
        }
        #endregion 


        public static List<TechnicalAnswerViewModel> MapToTechnicalAnswerList(List<TechnicalAnswer> answers)
        {
            var resultList = new List<TechnicalAnswerViewModel>();
            foreach (var answer in answers)
            {
                var result = new TechnicalAnswerViewModel()
                {
                    ID = answer.ID,
                    InputParam = answer.InputParam,
                    OutputParam = answer.OutputParam,
                    QuestionID = answer.QuestionID,
                };

                resultList.Add(result);
            }

            return resultList;
        }
    }
}
