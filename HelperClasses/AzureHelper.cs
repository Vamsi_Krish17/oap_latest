﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OAT.Domain;

namespace HelperClasses
{
    public class AzureHelper
    {
        private static OatEntities db = new OatEntities();
        private static TimeZoneInfo INDIAN_ZONE = TimeZoneInfo.FindSystemTimeZoneById("India Standard Time");

        private static StorageCredentials storageCredentials;
        private static CloudStorageAccount storageAccount;
        private static CloudBlobClient blobClient;
        private static CloudBlobContainer container;

        
        static AzureHelper()
        {
            //storageCredentials = new StorageCredentials("sunsigns", "HLZzdISF6RKxTZf6eBRlZR7HoTuGSre253wscdFno4DpF/kYCNu2C+MCctc61J8RyHF7Y7RxH2Opu5/l2gj+TA==");
            //storageAccount = new CloudStorageAccount(storageCredentials, false);
            //blobClient = storageAccount.CreateCloudBlobClient();
            //container = blobClient.GetContainerReference("cdn");

            storageCredentials = new StorageCredentials("pencil", "CTAGabY5HGvRa3TM3sTRrkkwCMti4AAnSkphc1PqTJhHbdsBNga9Pm1IMwFbmXZtmKGTdDjxaCiY/50Ifo1V6w==");
            storageAccount = new CloudStorageAccount(storageCredentials, false);
            blobClient = storageAccount.CreateCloudBlobClient();
            container = blobClient.GetContainerReference("cdn");
        }

        public static string UploadToAzureStorage(string filePath, string destinationPath, bool deleteSourceAfterUpload)
        {
            try
            {
                string filename = Path.GetFileName(filePath);
                string blobReference = (destinationPath +"/"+ filename).Trim('/');
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobReference);
                //if (blockBlob.Exists())
                //{
                //    return "";
                //}
                using (var fileStream = System.IO.File.OpenRead(filePath))
                {
                    blockBlob.UploadFromStream(fileStream);
                }
                return blockBlob.Uri.AbsoluteUri;
            }
            catch (Exception ex)
            {
                //UploadFailureLog errorLog = new UploadFailureLog();
                //errorLog.Message = ex.Message;
                //errorLog.Description = "Upload Azure Error" + ex.StackTrace + ex.Source;
                //errorLog.Date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, INDIAN_ZONE);
                //db.UploadFailureLog.Add(errorLog);
                //db.SaveChanges();
                return "";
            }

        }

        public static bool Download(string blobReference, string destinationPath)
        {
            try
            {
                CloudBlockBlob blockBlob = container.GetBlockBlobReference(blobReference);
                if (!blockBlob.Exists())
                {
                    return false;
                }
                blockBlob.DownloadToFile(destinationPath, System.IO.FileMode.CreateNew);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        //public static async Task<ICloudBlob> GetAzureBlobAsync(string uri)
        //{
        //}
        //public static string CdnRoot { get { return "http://sunsigns.blob.core.windows.net/cdn"; } }
        //public static string CdnRoot { get { return "http://pencil.blob.core.windows.net/cdn"; } }
    }
}
