﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace HelperClasses
{
    public class HtmlFormatter
    {

        public string FormatFileContent(string fileContent)
        {
            int index;
            if (fileContent != null)
            {
                //index = fileContent.IndexOf("<table class=MsoNormalTable");
                index = fileContent.IndexOf("<table class=Mso");
                if (index >= 0)
                    fileContent = fileContent.Substring(index);
                index = fileContent.LastIndexOf("</div>");
                if (index >= 0)
                    fileContent = fileContent.Remove(index);
                index = fileContent.LastIndexOf("</div>");
                if (index >= 0)
                    fileContent = fileContent.Remove(index);

                fileContent = fileContent.Replace("border=1 cellspacing=0 cellpadding=0", "");
                fileContent = fileContent.Replace("border=0 cellspacing=0 cellpadding=0", "");

                fileContent = fileContent.Replace("valign=top", "");
                fileContent = fileContent.Replace("valign=bottom", "");
                fileContent = fileContent.Replace("align=center", "");
                fileContent = fileContent.Replace("align=left", "");

                fileContent = fileContent.Replace("<br>", "<br/>");
                fileContent = fileContent.Replace("<o:p>", "<span>");
                fileContent = fileContent.Replace("</o:p>", "</span>");

                fileContent = fileContent.Replace("apple-converted-space", "MsoNormal");
                fileContent = fileContent.Replace("rowspan=3", "rowspan='3'");
                fileContent = fileContent.Replace("<p class='MsoNormal'> </p>", "");
                fileContent = fileContent.Replace("<p class='MsoNormal'>&nbsp;</p>", "");
                fileContent = fileContent.Replace("lang=EN-IN", "");
                fileContent = fileContent.Replace("lang=EN-GB", "");
                fileContent = fileContent.Replace("CxSpMiddle", "");
                fileContent = fileContent.Replace("lang=EN-US", "");
                fileContent = fileContent.Replace("∞", "[infin]").Replace("∏", "[prod]").Replace("√", "[sqrt]").Replace("∫", "[int]").Replace("∈", "[isin]").Replace("∉", "[notin]")
                                         .Replace("∋", "[ni]").Replace("∀", "[forall]").Replace("∝", "[prop]").Replace("∂", "[part]").Replace("←", "[larr]").Replace("π", "[pi]").Replace("Π","[Pi]")
                                         .Replace("⁄", "[frasl]").Replace("Σ", "[Sigma]").Replace("±", "[plusmn]").Replace("¥", "[yen]").Replace("ˆ", "[circ]").Replace("∇", "[nabla]")
                                         .Replace("Δ", "[Delta]").Replace("º", "[ordm]").Replace("α","[alpha]").Replace("£","[pound]").Replace("…","[hellip]").Replace("β","[beta]")
                                         .Replace("γ","[gamma]").Replace("θ","[theta]").Replace("φ","[phi]").Replace("ϑ","[thetasym]").Replace("ρ","[rho]").Replace("≡","[equiv]")
                                         .Replace("ε", "[epsilon]").Replace("λ", "[lambda]").Replace("ƒ", "[fnof]").Replace("μ", "[mu]").Replace("↔", "[harr]").Replace("⇔", "hArr")
                                         .Replace("∠","[ang]").Replace("∧","[and]").Replace("∨","[or]").Replace("∩","[cap]").Replace("∪","[cup]").Replace("∼","[sim]").Replace("ψ","[psi]")
                                         .Replace("≅","[cong]").Replace("⊂","[sub]").Replace("⊃","[sup]").Replace("⊄","[nsub]").Replace("⊆","[sube]").Replace("⊇","[supe]").Replace("Ψ","[Psi]")
                                         .Replace("→", "[rarr]").Replace("≠", "[ne]").Replace("ω", "[omega]").Replace("ϵ", "[diffepsilon]").Replace("₯", "[drachma]").Replace("⪔", "[Graphemica]")
                                         .Replace("∑", "[sum]").Replace("δ", "[delta]").Replace("µ", "[micro]").Replace("¶", "[para]").Replace("η", "[eta]");
                fileContent = fileContent.Replace("&nbsp;", " ");

                string pattern = "(?<width>width)=(?<num>\\d+)";
                string replace = "${width}='${num}'";
                fileContent = Regex.Replace(fileContent, pattern, replace, RegexOptions.IgnoreCase);

                pattern = "(?<start>start)=(?<num>\\d+)";
                replace = "${start}='${num}'";
                fileContent = Regex.Replace(fileContent, pattern, replace, RegexOptions.IgnoreCase);

                pattern = "(?<type>type)=(?<num>\\d+)";
                replace = "${type}='${num}'";
                fileContent = Regex.Replace(fileContent, pattern, replace, RegexOptions.IgnoreCase);

                pattern = "(?<type>type)=(?<txt>\\w+)";
                replace = "${type}='${txt}'";
                fileContent = Regex.Replace(fileContent, pattern, replace, RegexOptions.IgnoreCase);


                pattern = "(?<colspan>colspan)=(?<num>\\d+)";
                replace = "${colspan}='${num}'";
                fileContent = Regex.Replace(fileContent, pattern, replace, RegexOptions.IgnoreCase);

                pattern = "(?<height>height)=(?<num>\\d+)";
                replace = "${height}='${num}'";
                fileContent = Regex.Replace(fileContent, pattern, replace, RegexOptions.IgnoreCase);

                //pattern = "(?<alt>alt)=(?<txt>[a-zA-Z0-9//:\\.\\-\"]*)";
                //replace = "${alt}='${txt}'";
                //fileContent = Regex.Replace(fileContent, pattern, replace, RegexOptions.IgnoreCase);

                pattern = "(?<class>class)=(?<txt>[a-zA-Z0-9\\-]*)";
                //replace = "${class}='${txt}'";
                replace = "${class}=${txt}";
                fileContent = Regex.Replace(fileContent, pattern, replace, RegexOptions.IgnoreCase);

                fileContent = fileContent.Replace("JPG\"'></", "JPG\"'/></");


                pattern = "(?<img>img)(?<txt>(.*\n*.*))(?<brack>'>)";
                replace = "${img} ${txt}'/>";
                fileContent = Regex.Replace(fileContent, pattern, replace, RegexOptions.IgnoreCase);

                pattern = ".gif\">";
                replace = ".gif\"/>";
                fileContent = fileContent.Replace(pattern, replace);

                pattern = ".jpg\">";
                replace = ".jpg\"/>";
                fileContent = fileContent.Replace(pattern, replace);

                pattern = ".png\">";
                replace = ".png\"/>";
                fileContent = fileContent.Replace(pattern, replace);

                //fileContent = fileContent.Replace("&rsquo;","");
                //fileContent = fileContent.Replace("&ldquo;", "");
                //fileContent = fileContent.Replace("&rdquo;", "");
                //fileContent = fileContent.Replace("&lsquo;", "");
                //fileContent = fileContent.Replace("&ndash;", "");
                //fileContent = fileContent.Replace("&bull;", "");
                //fileContent = fileContent.Replace("&frac", "");
                //fileContent = fileContent.Replace("&deg;", "");
                //fileContent = fileContent.Replace("&frasl;", "");
                //fileContent = fileContent.Replace("&pi;", "");
                //fileContent = fileContent.Replace("&ang;", "");
                //fileContent = fileContent.Replace("&times;", "");

                //Replace for alt text in image
                //fileContent = fileContent.Replace("'\"", "\"").Replace("' "," ");
            }
            else
            {
                fileContent = "";
            }
            return "<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"yes\"?><div id=\"root\">" + fileContent + "</div>";
            //return  fileContent + "</div>";
        }

        public string ConvertHtmlToString(string newPath)
        {
            string fileContent = "";
            using (FileStream fileStream = System.IO.File.Open(newPath, FileMode.Open))
            {
                using (StreamReader reader = new StreamReader(fileStream, Encoding.UTF8))
                {
                    fileContent = reader.ReadToEnd();
                }
            }
            return fileContent;
        }


        #region Get Content From Html Content

        //public static string StripTagsRegex(string source)
        //{
        //    return Regex.Replace(source, "<.*?>", string.Empty);
        //}

        //static Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        //public static string StripTagsRegexCompiled(string source)
        //{
        //    return _htmlRegex.Replace(source, string.Empty);
        //}

        public string GetContentFromHtml(string source)
        {
            char[] array = new char[source.Length];
            int arrayIndex = 0;
            bool inside = false;

            for (int i = 0; i < source.Length; i++)
            {
                char let = source[i];
                if (let == '<')
                {
                    inside = true;
                    continue;
                }
                if (let == '>')
                {
                    inside = false;
                    continue;
                }
                if (!inside)
                {
                    array[arrayIndex] = let;
                    arrayIndex++;
                }
            }
            string result = new string(array, 0, arrayIndex);
            return result = result.Replace("\n", "").Replace("\r", "").Replace(" ", "");
            //return new string(array, 0, arrayIndex);
        }

        public string GetContentFromHtmlWithOutReplaceSpace(string source)
        {
            char[] array = new char[0];
            int arrayIndex = 0;
            bool inside = false;
            if (source != null)
            {
                array = new char[source.Length];
                for (int i = 0; i < source.Length; i++)
                {
                    char let = source[i];
                    if (let == '<')
                    {
                        inside = true;
                        continue;
                    }
                    if (let == '>')
                    {
                        inside = false;
                        continue;
                    }
                    if (!inside)
                    {
                        array[arrayIndex] = let;
                        arrayIndex++;
                    }
                }
            }
            string result = new string(array, 0, arrayIndex);
            result = result.Replace("\n", "").Replace("\r", "");
            if (result.Length > 200)
                return result.Substring(0, 200);
            else
                return result;


            //return new string(array, 0, arrayIndex);
        }



        #endregion
    }
}
