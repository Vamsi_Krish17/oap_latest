﻿using DocumentFormat.OpenXml.Packaging;
using OpenXmlPowerTools;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace HelperClasses
{
    public class Converter
    {
        public void ConvertDocxToHtml(string fileName)
        {
            //byte[] byteArray = File.ReadAllBytes(fileName);
            //using (MemoryStream memoryStream = new MemoryStream())
            //{
            //    memoryStream.Write(byteArray, 0, byteArray.Length);
            //    using (WordprocessingDocument doc = WordprocessingDocument.Open(memoryStream, true))
            //    {
            //        HtmlConverterSettings settings = new HtmlConverterSettings()
            //        {
            //            PageTitle = "My Page Title"
            //        };
            //        XElement html = HtmlConverter.ConvertToHtml(doc, settings);
            //        //File.WriteAllText(@"E:\Test.html", html.ToStringNewLineOnAttributes());
            //        File.WriteAllText(@"D:\OAP\AGE PROBLEMS.html", html.ToString());

            //    }
            //}


            // This example shows conversion of images. A cascading style sheet is not used.
//            string css = @"
//        p.PtNormal
//            {margin-bottom:10.0pt;
//            font-size:11.0pt;
//            font-family:""Times"";}
//        h1.PtHeading1
//            {margin-top:24.0pt;
//            font-size:14.0pt;
//            font-family:""Helvetica"";
//            color:blue;}
//        h2.PtHeading2
//            {margin-top:10.0pt;
//            font-size:13.0pt;
//            font-family:""Helvetica"";
//            color:blue;}";

            FileInfo fileInfo = new FileInfo(fileName);
            string imageDirectoryName =fileInfo.Directory.ToString()+"\\"+ fileInfo.Name.Substring(0,
                fileInfo.Name.Length - fileInfo.Extension.Length) + "_files";
            DirectoryInfo dirInfo = new DirectoryInfo(imageDirectoryName);
            if (dirInfo.Exists)
            {
                // Delete the directory and files.
                foreach (var f in dirInfo.GetFiles())
                    f.Delete();
                dirInfo.Delete();
            }
            int imageCounter = 0;
            if (File.Exists(fileName))
            { 
                //File exists
            }
            byte[] byteArray = File.ReadAllBytes(fileName);
            using (MemoryStream memoryStream = new MemoryStream())
            {
                memoryStream.Write(byteArray, 0, byteArray.Length);
                using (WordprocessingDocument doc =
                    WordprocessingDocument.Open(memoryStream, true))
                {
                    HtmlConverterSettings settings = new HtmlConverterSettings()
                    {
                        PageTitle = "Questions",
                        CssClassPrefix = "Pt",
                        //Css = css,
                        ConvertFormatting = false,
                    };
                    ImageInfo ext;
                    XElement html = HtmlConverter.ConvertToHtml(doc, settings,
                        imageInfo =>
                        {
                            DirectoryInfo localDirInfo = new DirectoryInfo(imageDirectoryName);
                            if (!localDirInfo.Exists)
                                localDirInfo.Create();
                            ++imageCounter;
                            string extension = imageInfo.ContentType.Split('/')[1].ToLower();
                            ext = imageInfo;
                            ImageFormat imageFormat = null;
                            if (extension == "png")
                            {
                                // Convert the .png file to a .jpeg file.
                                extension = "jpeg";
                                imageFormat = ImageFormat.Jpeg;
                            }
                            else if (extension == "bmp")
                                imageFormat = ImageFormat.Bmp;
                            else if (extension == "jpeg")
                                imageFormat = ImageFormat.Jpeg;
                            else if (extension == "tiff")
                                imageFormat = ImageFormat.Tiff;
                            

                            // If the image format is not one that you expect, ignore it,
                            // and do not return markup for the link.
                            if (imageFormat == null)
                                return null;

                            string imageFileName = imageDirectoryName + "/image" +
                                imageCounter.ToString() + "." + extension;
                            try
                            {
                                imageInfo.Bitmap.Save(imageFileName, imageFormat);
                            }
                            catch (System.Runtime.InteropServices.ExternalException)
                            {
                                return null;
                            }
                            XElement img = new XElement(Xhtml.img,
                                new XAttribute(NoNamespace.src, imageFileName),
                                imageInfo.ImgStyleAttribute,
                                imageInfo.AltText != null ?
                                    new XAttribute(NoNamespace.alt, imageInfo.AltText) : null);
                            return img;
                            
                        });

                    // Note: The XHTML returned by the ConvertToHtmlTransform method contains objects of type
                    // XEntity. PtOpenXmlUtil.cs define the XEntity class. For more information, see
                    // http://blogs.msdn.com/ericwhite/archive/2010/01/21/writing-entity-references-using-linq-to-xml.aspx.

                    //
                    // If you transform the XML tree returned by the ConvertToHtmlTransform method further, you
                    // must do it correctly, or entities do not serialize correctly.

                    File.WriteAllText(fileInfo.Directory.FullName + "/" + fileInfo.Name.Substring(0,
                        fileInfo.Name.Length - fileInfo.Extension.Length) + ".html",
                        html.ToStringNewLineOnAttributes());
                }
            }

        }
    }
}
