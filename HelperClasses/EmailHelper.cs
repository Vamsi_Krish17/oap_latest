﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Configuration;

namespace HelperClasses
{
    public class EmailHelper
    {

        static string USERNAME;
        static string API_KEY;
        static string FromEmail;
        static string FromName;

        static EmailHelper()
        {
            USERNAME = ConfigurationManager.AppSettings["ElasticEmail_Username"];
            API_KEY = ConfigurationManager.AppSettings["ElasticEmail_API_KEY"];

            FromEmail = ConfigurationManager.AppSettings["FromEmail"];
            FromName = ConfigurationManager.AppSettings["FromName"];
        }

        public static void SendResetPasswordEmail(string UserName, string MailId, string PasswordResetToken, string forgotPasswordTemplateFile)
        {
            string forgotPasswordTemplate = File.ReadAllText(forgotPasswordTemplateFile);
            forgotPasswordTemplate = forgotPasswordTemplate.Replace("#PASSWORD_RESET_LINK#", "http://pencil.azurewebsites.net/Home/PasswordReset/" + PasswordResetToken);
            SendEmail(MailId, "Reset Password", "", forgotPasswordTemplate);
        }

        public static string SendEmail(string to, string subject, string bodyText, string bodyHtml)
        {
            WebClient client = new WebClient();
            NameValueCollection values = new NameValueCollection();
            values.Add("username", USERNAME);
            values.Add("api_key", API_KEY);
            values.Add("from", FromEmail);
            values.Add("from_name", FromName);
            values.Add("subject", subject);
            if (bodyHtml != null)
                values.Add("body_html", bodyHtml);
            if (bodyText != null)
                values.Add("body_text", bodyText);
            values.Add("to", to);

            byte[] response = client.UploadValues("https://api.elasticemail.com/mailer/send", values);
            return Encoding.UTF8.GetString(response);
        }

    }
}
