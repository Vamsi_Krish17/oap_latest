//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OAP.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class TempPassage
    {
        public TempPassage()
        {
            this.TempPassageQuestionOrder = new HashSet<TempPassageQuestionOrder>();
            this.TempQuestion = new HashSet<TempQuestion>();
        }
    
        public int ID { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public byte[] Timestamp { get; set; }
        public Nullable<bool> CanShuffle { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
    
        public virtual UserProfile UserProfile { get; set; }
        public virtual UserProfile UserProfile1 { get; set; }
        public virtual ICollection<TempPassageQuestionOrder> TempPassageQuestionOrder { get; set; }
        public virtual ICollection<TempQuestion> TempQuestion { get; set; }
    }
}
