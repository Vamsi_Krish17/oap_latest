﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain
{
    public partial class UserSectionStatus
    {
        private OAPEntities db = new OAPEntities();

        public int MarkComplete()
        {

            this.FinishTime = DateTime.Now;
            int result = this.Evaluate();
            this.Marks = result;
            return result;
        }

        public int Evaluate()
        {
            //int correctAnswers = this.UserQuestionPaper.QuestionPaperSnapshot.Where(snap => snap.Answer.IsCorrect ?? false).Count();
            int correctAnswers = this.UserAssessment.AssessmentSnapshot.Where(snap => snap.Answer!=null? (snap.Answer.IsCorrect ?? false) :false).Count();
            int totalAnswered = this.UserAssessment.AssessmentSnapshot.Count();

            if (this.UserAssessment.UserSectionStatus.Count() == this.UserAssessment.QuestionPaper.QuestionPaperSection.Count()) //All sections complete
            {
                this.UserAssessment.FinishTime = this.FinishTime;
                this.UserAssessment.IsActive = false;
                this.UserAssessment.Marks = this.UserAssessment.UserSectionStatus.Sum(ss => ss.Marks).Value;
            }
            return correctAnswers;
        }
    }
}
