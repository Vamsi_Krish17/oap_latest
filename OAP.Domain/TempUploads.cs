//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OAP.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class TempUploads
    {
        public TempUploads()
        {
            this.TempQuestion = new HashSet<TempQuestion>();
        }
    
        public long ID { get; set; }
        public string Title { get; set; }
        public System.DateTime UploadedDate { get; set; }
        public byte[] Timestamp { get; set; }
        public Nullable<int> UploadedUserID { get; set; }
    
        public virtual ICollection<TempQuestion> TempQuestion { get; set; }
        public virtual UserProfile UserProfile { get; set; }
    }
}
