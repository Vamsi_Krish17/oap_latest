﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class QuestionPaperSectionTemplateRuleViewModel
    {

        public long ID { get; set; }

        public string Name { get; set; }

        public int Count { get; set; }

        public int QuestionPaperSectionTemplateID { get; set; }

        public string TagIDString { get; set; }

        public string SubjectIDString { get; set; }

        public int Complexity { get; set; }


        public string SubjectNames { get; set; }

        public string TagNames { get; set; }

        public int CurrentIndex { get; set; }

        public string ComplexityString { get; set; }

        public string SubjectString { get; set; }

        public string TagString { get; set; }
    }
}
