﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OAP.Domain.ViewModel
{
    public class UserProfileViewModel
    {

        public long ID { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }

        public string OldPassword { get; set; }

        [Required]
        [MaxLength(50)]
        public string UniversityRegistryNumber { get; set; }

        public long CollegeID { get; set; }

        public long BranchID { get; set; }

        public long YearID { get; set; }

        public long SectionID { get; set; }

        [MaxLength(200)]
        public string Address { get; set; }

        public int RowNumber { get; set; }

        public string CollegeName { get; set; }

        public string BranchName { get; set; }

        public string SectionName { get; set; }

        public string EmailId { get; set; }

        public string PhoneNumber { get; set; }

        public string ImageUrl { get; set; }

        public string LogoUrl { get; set; }

        public string Theme { get; set; }

        public string RoleName { get; set; }

        public string StudentName { get; set; }
    }
}
