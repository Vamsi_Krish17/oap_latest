﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
   public  class SectionViewModel
    {
        public long ID { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name = "Section")]
       [MaxLength(10)]
        public string Name { get; set; }

        public int RowNumber { get; set; }

        public long CollegeID { get; set; }
    }
}
