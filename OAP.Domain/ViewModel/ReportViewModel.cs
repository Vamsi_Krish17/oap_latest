﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OAP.Domain.ViewModel
{
    public class ReportViewModel
    {

        public List<TimeReportModel> TimeReport { get; set; }

        public bool IsFirst { get; set; }

        public bool IsLast { get; set; }

        public bool IsNext { get; set; }

        public bool IsPrev { get; set; }

        public long QuestionPaperID { get; set; }

        public string QuestionPaperName { get; set; }
        public int TotalTimeTaken { get; set; }

        public QuestionViewModel CurrentQuestion { get; set; }

        public AnswerViewModel StudentAnswer { get; set; }

        public List<AnswerViewModel> StudentAnsweredList { get; set; }

        public QuestionPaperViewModel questionPaperViewModel { get; set; }

        public List<BatchViewModel> BatchViewModelList { get; set; }

        public QuestionPaperReportModel assessmentReportModel { get; set; }
        public SectionReportModel sectionReportModel { get; set; }

        public ReportViewModel reportModel { get; set; }
        public ReportViewModel reportViewModel { get; set; }
        public class TimeReportModel
        {
            public int BestTime { get; set; }

            public int StudentTime { get; set; }

            public int AverageTime { get; set; }

            public decimal AverageRatio { get; set; }

            public decimal BestRatio { get; set; }

            // public bool IsAverageHigher { get; set; }

            //public bool IsBestHigher { get; set; }

            public int AverageStatus { get; set; }

            public int BestStatus { get; set; }

            public string AverageMessage { get; set; }

            public string BestMessage { get; set; }

            public long StudentAnswerID { get; set; }

            public List<long> StudentAnswerIDList { get; set; }


        }

        public class QuestionPaperReportModel
        {

            public string StudentName { get; set; }

            public string RegisterNumber { get; set; }

            public string QuestionPaperName { get; set; }

            public int TotalQuestions { get; set; }

            public int AttentedQuestions { get; set; }

            public int CorrectAnswers { get; set; }

            public decimal MarksPercentage { get; set; }

            public decimal HitPercentage { get; set; }

            public int TimeTakenForCorrectAnswer { get; set; }

            public float AverageTimeTakenForCorrectAnswer { get; set; }

            public string Tags { get; set; }

            public List<TagReportModel> TagReportModelList { get; set; }


            public String SectionName { get; set; }

            public int Tag { get; set; }

            public int TimeTaken { get; set; }

            public int Status { get; set; }

            public int userAnswered { get; set; }
        }

        public class TagReportModel
        {

            public string Name { get; set; }

            public int TotalQuestions { get; set; }

            public int AttendedQuestions { get; set; }

            public int CorrectAnswers { get; set; }

            public int CorrectAnswerPercentage { get; set; }

            public int StudentAnswerID { get; set; }
        }
        public class SectionReportModel
        {

            public string StudentName { get; set; }

            public string RegisterNumber { get; set; }

            public string QuestionPaperName { get; set; }

            public int TotalQuestions { get; set; }

            public int TotalSections { get; set; }


            public int AttendedQuestions { get; set; }

            public int UnattendedQuestions { get; set; }

            public int CorrectAnswers { get; set; }

            public int WrongAnswers { get; set; }

            public float MarksPercentage { get; set; }

            public float HitPercentage { get; set; }

            public decimal CorrectAnswerPercentage { get; set; }

            public List<SectionReportModel> SectionReportModelList { get; set; }


            public string Sections { get; set; }

            public string SectionName { get; set; }
        }
        public List<QuestionViewModel> QuestionViewModelList { get; set; }

        public string ResultJsonString { get; set; }

        public int AttentedStudentsCount { get; set; }

        public List<BranchViewModel> BranchViewModelList { get; set; }

        public BatchViewModel BatchViewModel { get; set; }

        public List<QuestionPaperViewModel> QuestionPaperViewModelList { get; set; }

        public BranchViewModel BranchViewModel { get; set; }

        public string Type { get; set; }


        public string CollegeString { get; set; }

        public string BatchString { get; set; }


        public List<QuestionPaperSectionViewModel> QuestionPaperSectionViewModellList { get; set; }

        public int TestDuration { get; set; }

        public int TotalSections { get; set; }

        public string Sections { get; set; }

        public int Totalquestions { get; set; }

        public int TotalQuestionCount { get; set; }

        public int YourScore { get; set; }

        public bool CanReview { get; set; }

        public int Qno { get; set; }

        //public string totalTimeTakenString { get; set; }

        //public string TestDurationString { get; set; }

        public long CollegeID { get; set; }

        public List<string> BatchListString { get; set; }

        public string BatchIDString { get; set; }

        //public string QuestionPaperName { get; set; }

        public IQueryable<Question> questionPaperSectionQuestionList { get; set; }

        public string collegeName { get; set; }

        public string batchName { get; set; }

        public List<StudentViewModel> StudentViewModelList { get; set; }

        public List<string> branchNames { get; set; }
        public string branchName { get; set; }

        public string DateConductedString { get; set; }

        public string StudentCount { get; set; }

        public long batchID { get; set; }

        public List<MarkViewModel> MarkViewModelList { get; set; }

        public List<TagViewModel> TagViewModelList { get; set; }

        // VM Objects for Consolidated Report
        public ReportViewModel BarChartViewModel { get; set; }
        public ReportViewModel StreamWiseBarChartViewModel { get; set; }
        public ReportViewModel SectionWisePieChartViewModel { get; set; }
        public ReportViewModel GetDeptWiseAvgHorzBarChartModel { get; set; }
        public List<ReportViewModel> TagWiseBarChartViewModelList { get; set; }
        public List<ReportViewModel> DeptWisePieChartViewModelList { get; set; }
        public List<ReportViewModel> GetDeptWiseBarChartViewModelList { get; set; }
        public static List<ReportViewModel> subDeptWiseBarChartViewModelList { get; set; }
        
        public long branchID { get; set; }

        public List<long> branchIDs { get; set; }

        public List<ReportViewModel> GetDeptWiseAvgHorzBarChartModelList { get; set; }

        public List<ReportViewModel> getDeptWisepieChartList { get; set; }

        public ReportViewModel DeptWisePieChartViewModel { get; set; }
    }
}
