﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class TestPackageQuestionPaperViewModel
    {
        public long ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        [StringLength(50)]
        public string Code { get; set; }

        [Display(Name = "Cut Off Mark")]
        public decimal CutOffMark { get; set; }

        public bool DisplayResult { get; set; }

        public bool DisplayMarks { get; set; }

        [Display(Name = "Time Limit")]
        public int TimeLimit { get; set; }

        public decimal Marks { get; set; }

        public decimal NegativeMarks { get; set; }


        public bool ReleaseKey { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Instructions { get; set; }

        public bool IsNegativePercentage { get; set; }

        //public QuestionPaperSectionViewModel questionPaperSection { get; set; }

        public int SectionCount { get; set; }

        public bool IsQuestionPaperTaken { get; set; }

        //[AdminID] [int] NULL,


        public string Status { get; set; }

        public bool IsShowSectionTimeLimit { get; set; }

        public bool ShowInstructions { get; set; }

        public bool ShowReport { get; set; }

        public List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList { get; set; }

        public int TotalStudentsTaken { get; set; }

        public StudentViewModel FirstStudentViewModel { get; set; }

        public StudentViewModel LastStudentViewModel { get; set; }

        public string UploadName { get; set; }

        public int PassCount { get; set; }

        public int FailedCount { get; set; }

        public decimal PassPercentage { get; set; }

        public int AttentedStudentsCount { get; set; }

        public bool IsCutOffMarkSpecified { get; set; }

        public bool IsShowRetestRequest { get; set; }

        public bool IsHaveRetestRequest { get; set; }

        //public bool IsShowRetestRequest { get; set; }

        public bool IsRetestRequestRejected { get; set; }

        public string RejectReason { get; set; }

        public bool IsQuestionPaperBatchDateNotInRange { get; set; }

        CultureInfo ci = new CultureInfo("en-GB");

        [Required]
        public string _StartDateString;
        public string StartDateString
        {
            get { return StartDate.ToString("dd-MM-yyyy"); }
            set
            {
                _StartDateString = value;
                DateTime _start;
                if (DateTime.TryParse(value, ci, DateTimeStyles.None, out _start))
                //if (DateTime.TryParse(value, ci, DateTimeStyles.None, out _start))
                {
                    StartDate = _start;
                }
            }
        }

        [Required]
        public string _EndDateString;
        public string EndDateString
        {
            get { return EndDate.ToString("dd-MM-yyyy"); }
            set
            {
                _EndDateString = value;
                DateTime _end;
                if (DateTime.TryParse(value, ci, DateTimeStyles.None, out _end))
                {
                    EndDate = _end;
                }
            }
        }


        public bool IsTemplate { get; set; }

        public long TestTemplateID { get; set; }

        public DateTime? createdDate { get; set; }

        public string CreatedDateString
        {
            get
            {
                return createdDate != null ? createdDate.Value.ToString("dd/MM/yyyy") : "";
            }
        }

        public List<QuestionPaperSectionViewModel> QuestionPaperSectionViewModelList { get; set; }

        public QuestionPaperSectionViewModel QuestionPaperSection { get; set; }

        public int NoOfSections { get; set; }

        public List<TagViewModel> tagViewModelList { get; set; }

        public TimeSpan TimeLimitTimeSpan { get; set; }
    }
}
