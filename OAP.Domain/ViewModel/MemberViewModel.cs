﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAP.Domain.ViewModel
{
    public class MemberViewModel
    {
        public int ID { get; set; }


        //[Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        //  [Required]
        //  [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        // [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        // [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        //  [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [EmailAddress]
        public string EmailID { get; set; }

        [Display(Name = "Organisation Name")]
        public string OrganisationName { get; set; }

        public string OrgLogo { get; set; }


        [Display(Name = "Address Line 1")]
        public string OrganisationAddress1 { get; set; }

        [Display(Name = "Address Line 2")]
        public string OrganisationAddress2 { get; set; }

        [Display(Name = "Contact Number 1")]
        public string ContactNumber1 { get; set; }

        [Display(Name = "Contact Number 2")]
        public string ContactNumber2 { get; set; }

        public UserOrganisationViewModel UserOrganisationViewModel { get; set; }
    }
}
