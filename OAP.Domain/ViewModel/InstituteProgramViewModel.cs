﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
   public class InstituteProgramViewModel
    {
        public long ID { get; set; }

        public ProgramViewModel program { get; set; }

        public CollegeViewModel Institute { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }

        public string ProgramName { get; set; }

        public string InstituteName { get; set; }
    }
}
