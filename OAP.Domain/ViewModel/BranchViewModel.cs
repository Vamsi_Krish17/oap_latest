﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OAP.Domain.ViewModel
{
   public class BranchViewModel
    {

        public long ID { get; set; }

        [Required]
        [StringLength(100)]
        [Display(Name="Branch")]
        public string Name { get; set; }

        public int RowNumber { get; set; }

        public long CollegeID { get; set; }

        public int PassCount { get; set; }

        public int FailedCount { get; set; }

        public decimal PassPercentage { get; set; }

        public int AttentedStudentsCount { get; set; }

        public bool IsCutOffMarkSpecified { get; set; }

        public int Years { get; set; }


        public List<BranchViewModel> BranchInstituteViewModelList { get; set; }

        public List<StudentViewModel> studentViewModelList { get; set; }

        public List<SectionViewModel> sectionList { get; set; }

        public List<BatchViewModel> batchList { get; set; }

        public List<ProgramViewModel> programViewModelList { get; set; }

        public string programIDlist { get; set; }

        public List<NoteViewModel> notesViewModelList { get; set; }

        public List<BatchQuestionPaperViewModel> BatchQuestionPaperList { get; set; }

        public decimal PassPercentageAverage { get; set; }

        public string ParentBranch { get; set; }
    }
}
