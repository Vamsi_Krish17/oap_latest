﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class NotificationViewModel
    {

        public int Count { get; set; }

        public string Name{ get; set; }

        public string Url { get; set; }

    }
}
