﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class QuestionPaperSectionTemplateViewModel
    {
        public long ID { get; set; }
        public string SectionName { get; set; }
        public string SectionDescription { get; set; }
        public int SectionTimLimit { get; set; }
        public decimal SectionCutOfMarks { get; set; }
        public decimal SectionNegativeMark { get; set; }

        public string QuestionPaperSectionName { get; set; }

        public int TimeLimit { get; set; }

        public int SectionCount { get; set; }

        public List<QuestionPaperSectionTemplateRuleViewModel> questionPaperSectionRuleList { get; set; }

        public int? SectionTemplateID { get; set; }

        public QuestionPaperSectionTemplateRuleViewModel QuestionPaperSectionTemplateRuleViewModel { get; set; }


        public long QuestionPaperTemplateID { get; set; }

        public List<SubjectViewModel> SubjectViewModelList { get; set; }

        public List<TagViewModel> TagViewModelList { get; set; }

        public TimeSpan TimeLimitTimeSpan { get; set; }

        //public TimeSpan TimeLimitTimeSpan { get; set; }
    }
}
