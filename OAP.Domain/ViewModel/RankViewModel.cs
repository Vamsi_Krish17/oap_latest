﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class RankViewModel
    {
        public long ID { get; set; }
        public long QuestionPaperID { get; set; }
        public int UserID { get; set; }
        public int BatchRank { get; set; }
        public int CollegeRank { get; set; }
        public long NationalRank { get; set; }
        public long InternationalRank { get; set; }
        public int DepartmentRank { get; set; }
        public DateTime LastUpdatedDate { get; set; }


        public string QuestionPaperName { get; set; }

        public DateTime? Date { get; set; }

        public decimal? Mark { get; set; }

        public string DateString { get; set; }

        public string LastUpdatedDateString { get; set; }

        public int OverallRank { get; set; }

        public string College { get; set; }

        public string RegisterNumber { get; set; }

        public string StudentName { get; set; }
    }
}
