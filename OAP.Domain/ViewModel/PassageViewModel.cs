﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OAP.Domain.ViewModel
{
    public class PassageViewModel
    {

        public int ID { get; set; }

        [Required]
        public string Description { get; set; }

        public DateTime LastUpdated { get; set; }

        public List<QuestionViewModel> QuestionViewModelList { get; set; }

        public bool CanShuffle { get; set; }

        public string AnswerOrder { get; set; }


    }
}
