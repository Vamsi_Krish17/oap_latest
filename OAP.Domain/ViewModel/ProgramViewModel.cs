﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OAP.Domain.ViewModel
{
    public class ProgramViewModel
    {

        public long ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }


        public string Description { get; set; }

        public string LeftSelectedIds { get; set; }

        public string RightSelectedIds { get; set; }

        public List<OptionModel> CourseTemplateDetails { get; set; }



        public int StartDateDay
        {
            get
            {
                return StartDate.Day;
            }
        }
        public int StartDateMonth
        {
            get
            {
                return StartDate.Month;
            }
        }
        public int StartDateYear
        {
            get
            {
                return StartDate.Year;
            }
        }
        public int EndDateDay
        {
            get
            {
                return EndDate.Day;
            }
        }
        public int EndDateMonth
        {
            get
            {
                return EndDate.Month;
            }
        }
        public int EndDateYear
        {
            get
            {
                return EndDate.Year;
            }
        }

        public bool IsSelected { get; set; }


        public List<BatchViewModel> batchQuestionPapers { get; set; }

        public long CollegeID { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public long ProgramID { get; set; }

        public string collegeName { get; set; }

        public string ProgramName { get; set; }

        public List<ProgramViewModel> programDetails { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string CreatedDateString
        {
            get
            {
                return CreatedDate != null ? CreatedDate.Value.ToString("dd/MM/yyyy") : "";
            }
        }

    }
}
