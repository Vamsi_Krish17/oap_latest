﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OAP.Domain.ViewModel
{
    public class BatchViewModel
    {

        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

       
        public long BranchID { get; set; }

        public long SectionID { get; set; }

        public int StartYear { get; set; }

        public int EndYear { get; set; }

        public List<StudentViewModel> StudentViewModelList { get; set; }

        public List<CollegeViewModel> CollegeViewModelList { get; set; }

        public int SelectedBatchID { get; set; }

        public List<QuestionPaperViewModel> QuestionPaperViewModelList { get; set; }

        public int FailedCount { get; set; }

        public int PassCount { get; set; }

        public int QuestionPaperID { get; set; }

        public decimal PassPercentage { get; set; }

        public int AttentedStudentsCount { get; set; }

        public bool IsCutOffMarkSpecified { get; set; }


        public bool IsSelected { get; set; }

        public int StudentCount { get; set; }

        public string collegeName { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string BatchName { get; set; }

        public int BatchID { get; set; }

        public int BatchIDString { get; set; }

        public string QuestionPaperName { get; set; }
        public int StartDateDay
        {
            get
            {
                return StartDate.Day;
            }
        }
        public int StartDateMonth
        {
            get
            {
                return StartDate.Month;
            }
        }
        public int StartDateYear
        {
            get
            {
                return StartDate.Year;
            }
        }
        public int EndDateDay
        {
            get
            {
                return EndDate.Day;
            }
        }
        public int EndDateMonth
        {
            get
            {
                return EndDate.Month;
            }
        }
        public int EndDateYear
        {
            get
            {
                return EndDate.Year;
            }
        }


        public List<BatchQuestionPaperViewModel> BatchQuestionPaperViewModelList { get; set; }


        public List<BatchViewModel> BatchViewModelList { get; set; }

        public int TotalStudentCount { get; set; }


        public object branchNames { get; set; }
    }

    public class BatchQuestionPaperViewModel
    {
        public long ID { get; set; }

          [Required]
        public long BatchID { get; set; }
          public string BatchIDString { get; set; }
          [Required]
        public long QuestionPaperID { get; set; }

        public BatchViewModel BatchViewModel { get; set; }

        public QuestionPaperViewModel QuestionPaperViewModel { get; set; }

        public string Name { get; set; }

        [Required]
        public string StartDateString { get; set; }

        [Required]
        public string EndDateString { get; set; }

        public List<BatchQuestionPaperViewModel> BatchQuestionPaperList { get; set; }

        public List<BatchQuestionPaperViewModel> batchList { get; set; }

        public string BatchName { get; set; }

        public string QuestionPaperName { get; set; }

        public long CollegeID { get; set; }

        public string CollegeName { get; set; }

        public string Description { get; set; }

        public long TotalStudent { get; set; }
        public long CompletedStudent { get; set; }
        public long NotTakenStudent { get; set; }
        public long WritingStudent { get; set; }
        public long SectionCounts {get; set;}
        public List<StudentViewModel> studentViewModelList { get; set; }
        public List<QuestionPaperSectionViewModel> QuestionPaperSectionList { get; set; }
        public List<UserQuestionPaperViewModel> userQuestionPaperViewModelLiat { get; set; }

        public bool CanReview { get; set; }

        public bool IsHaveRetestRequest { get; set; }

        public bool ShowReport { get; set; }

        public DateTime StartDate { get; set; }

        public bool IsQuestionPaperTaken { get; set; }

        public string Status { get; set; }

        public bool IsShowRetestRequest { get; set; }

        public bool IsRetestRequestRejected { get; set; }

        public string RejectReason { get; set; }

        public bool IsQuestionPaperBatchDateNotInRange { get; set; }
    }
}
