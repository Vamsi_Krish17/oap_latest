﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OAP.Domain.ViewModel
{
    public class CollegeViewModel
    {
        
        public long ID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Code { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string City { get; set; }

        public string Pincode { get; set; }

        public int RowNumber { get; set; }
        [Required]
        public string BatchIDString { get; set; }
        [Required]
        public string QuestionPaperIDString { get; set; }
        //[Required]
        //public File file { get; set; }

        public List<SectionViewModel> SectionViewModelList { get; set; }

        public List<YearViewModel> YearViewModelList { get; set; }

        public List<BranchViewModel> BranchViewModelList { get; set; }

        public List<BatchViewModel> BatchViewModelList { get; set; }

        //public string DT_RowId { get; set; }

        //public RowData DT_RowData { get; set; }

        public List<SectionViewModel> SelectedSectionViewModelList { get; set; }

        public long SectionID { get; set; }

        public string LeftSelectedIds  { get; set; }

        public string RightSelectedIds  { get; set; }

        public List<OptionModel> CollegeBranchDetails { get; set; }

        public List<OptionModel> OptionModelList { get; set; }

        public bool IsSelected { get; set; }

        public string errormessage { get; set; }
        public string CurrentTab { get; set; }

        public string LeftSubSelectedIds { get; set; }

        public string RightSubSelectedIds { get; set; }

        public List<ProgramViewModel> ProgramList { get; set; }

        public bool Status { get; set; }

        //public object BatchIDString { get; set; }
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        [Required]
        public string ReasonID { get; set; }
        [Required]
        public string Comments { get; set; }

        public int TotalStudents { get; set; }
    }

    public class TableResultViewModel
    {
        List<CollegeViewModel> CollegeViewModelList { get; set; }
    }

    public class RowData
    {
        public int pkey { get; set; }

        public RowData(int no)
        {
            pkey = no;
        }
    }

    public class OptionModel
    {
        public long ID { get; set; }

        public string Name { get; set; }

        public bool IsSelected { get; set; }
    }
}
