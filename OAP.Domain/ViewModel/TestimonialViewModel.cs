﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class TestimonialViewModel
    {
        public long ID { get; set; }

        [Display(Name="Comments")]
        [Required]
        public string Description { get; set; }

        [Display(Name="User Name")]
        [Required]
        public string UserName { get; set; }

         [Required]
        public string Designation { get; set; }

        [Display(Name="User Image")]
        public string UserImage { get; set; }

        public int UserID { get; set; }

        [Display(Name="College Name")]
        [Required]
        public string CollegeName { get; set; }

        [Display(Name="Show in Website")]
        public bool IsShow { get; set; }

        public DateTime CreatedDate { get; set; }

    }
}
