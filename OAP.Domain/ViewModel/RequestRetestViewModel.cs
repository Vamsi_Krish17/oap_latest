﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OAP.Domain.ViewModel
{
    public class RequestRetestViewModel
    {

        public int ID { get; set; }

        public long QuestionPaperID { get; set; }

        public int UserProfileID { get; set; }

        public bool IsApproved { get; set; }

        [Required]
        public int ReasonID { get; set; }

        [Required]
        public string Comments { get; set; }

        public string StudentName { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Status { get; set; }

        public bool IsAlreadyRequested { get; set; }

        public string QuestionPaperName { get; set; }

        public string CreatedDateString {
            get { return CreatedDate.ToString("dd-MM-yyyy"); }
            set{}
        }

    }
}
