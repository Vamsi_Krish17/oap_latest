﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAP.Domain.ViewModel
{
    public class CommonQuestionViewModel
    {
        public int ID { get; set; }

        [Required]
        [Display(Name = "Question Description")]
        public string Description { get; set; }


        public int QuestionCategoryID { get; set; }

        [Required]
        public int QuestionTypeID { get; set; }

        public DateTime LastUpdatedTime { get; set; }

        public string AnswerIDString { get; set; }

        public int Sno { get; set; }

        public string LastUpdated
        {
            get
            {
                return LastUpdatedTime.ToString("dd-MM-yyy");
            }
        }

        public string AnswerDescription { get; set; }

        public List<TagViewModel> TagsAssociated { get; set; }

        public List<TagViewModel> TagsNotAssociated { get; set; }

        public string TagIDString { get; set; }

        public bool IsCorrectAnswer { get; set; }

        public string TagName { get; set; }

        public List<AnswerViewModel> AnswerViewModelList { get; set; }
        public int CorrectAnswerID { get; set; }

        public string Solution { get; set; }

        public int PassageID { get; set; }

        public PassageViewModel QuestionPassage { get; set; }

        public List<Domain.ViewModel.ReportViewModel.TimeReportModel> TimeReports { get; set; }

        public string DownloadLink { get; set; }

        public string Passage { get; set; }

        public string QuestionType { get; set; }

        public int QuestionNumber { get; set; }

        public decimal Marks { get; set; }

        public decimal NegativeMarks { get; set; }

        public List<AnswerViewModel> StudentAnsweredViewModelList { get; set; }

        public String SectionName { get; set; }

        public String TagNames { get; set; }

        public int TimeTaken { get; set; }

        public bool? Status { get; set; }

        public long questionPaperID { get; set; }

        public long CollegeID { get; set; }

        public int batchID { get; set; }

        [Required]
        public long ComplexityID { get; set; }

        [Required]
        public long SubjectID { get; set; }

        public int QuestionID { get; set; }

        public List<TagViewModel> TagViewModelList { get; set; }

        [Required]
        public string Title { get; set; }

        public string LanguageID { get; set; }

        public List<CodingLanguageModel> Languages { get; set; }

        public string LanguagesString { get; set; }

        [Required]
        public string SolutionType { get; set; }

        public class CodingLanguageModel
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }
    }
}
