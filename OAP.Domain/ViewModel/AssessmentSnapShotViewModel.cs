﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class QuestionPaperSnapShotViewModel
    {

        public int ID { get; set; }

        public QuestionViewModel QuestionViewModel { get; set; }

        public AnswerViewModel AnswerViewModel { get; set; }

        public string Question { get; set; }

        public string Student { get; set; }
        
        public decimal Marks { get; set; }

        public decimal NegativeMarks { get; set; }

        public string StudentName { get; set; }

        public string StudentRegistrationNumber { get; set; }

    }
}
