﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAP.Domain.ViewModel
{
    public class UserOrganisationViewModel
    {

        public long ID { get; set; }

        public int UserId { get; set; }

        [Display(Name = "Organisation Name")]
        public string OrganisationName { get; set; }

        public string OrgLogo { get; set; }

        [Display(Name = "Address Line 1")]
        public string OrganisationAddress1 { get; set; }

        [Display(Name = "Address Line 2")]
        public string OrganisationAddress2 { get; set; }

        [Display(Name = "Contact Number 1")]
        public string ContactNumber1 { get; set; }

        [Display(Name = "Contact Number 2")]
        public string ContactNumber2 { get; set; }


    }
}
