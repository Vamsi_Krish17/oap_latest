﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class ColorCodeViewModel
    {
        public long ID { get; set; }
        public string Name { get; set; }
        public string HexCode { get; set; }
    }
}
