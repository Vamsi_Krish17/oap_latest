﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class MarkViewModel
    {
        public string LabelName { get; set; }
        public List<UserProfileViewModel> StudentViewModelList { get; set; }
        public int studentCount { get; set;}

    }
}
