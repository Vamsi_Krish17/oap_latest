﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OAP.Domain.ViewModel
{
    public class CourseTemplateViewModel
    {

        public long ID { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public List<SubjectViewModel> SubjectViewModelList { get; set; }

        public string LeftSubSelectedIds { get; set; }

        public string RightSubSelectedIds { get; set; }


        public List<TestTemplateViewModel> TestTemplateList { get; set; }
    }
}
