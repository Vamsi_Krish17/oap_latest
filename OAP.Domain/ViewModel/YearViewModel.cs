﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
   public  class YearViewModel
    {

       public long ID { get; set; }

       [Required]
       [Display(Name="Start Year")]      
       public int StartYear { get; set; }

       [Required]
       [Display(Name="End Year")]
       public int EndYear { get; set; }    
       
       public long CollegeID { get; set; }

       public string Years { get; set; }
       

       public int RowNumber { get; set; }
    }
}
