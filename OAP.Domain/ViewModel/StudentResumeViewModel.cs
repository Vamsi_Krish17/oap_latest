﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAP.Domain.ViewModel
{
    public class StudentResumeViewModel
    {
        public long ID { get; set; }

        public long StudentID { get; set; }

        public string StudentName { get; set; }

        public string FilePath { get; set; }

        public DateTime UploadedDateTime { get; set; }

        [Display(Name = "Uploaded Date")]
        public string UploadedDate
        {
            get
            {
                return UploadedDateTime!=new DateTime()?UploadedDateTime!=new DateTime(1900,01,01)? UploadedDateTime.ToString("dd-MM-yyyy"):"":"";
            }
        }

        public DateTime ModifiedDateTime { get; set; }

         [Display(Name = "Modified Date")]
        public string ModifiedDate
        {
            get
            {
                return ModifiedDateTime != new DateTime() ? ModifiedDateTime != new DateTime(1900, 01, 01) ? ModifiedDateTime.ToString("dd-MM-yyyy") : "" : "";
            }
        }

    }
}
