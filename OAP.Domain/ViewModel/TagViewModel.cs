﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OAP.Domain.ViewModel
{
    public class TagViewModel
    {

        public long ID { get; set; }

        [Required]
        public string Name { get; set; }

        public long ParentTagID { get; set; }

        public int AvailableQuestionsCount { get; set; }

        public int SelectedQuestionsCount { get; set; }


        public bool IsSelected { get; set; }

        public decimal PreTrainingValue { get; set; }

        public decimal PostTrainingValue { get; set; }
    }
}
