﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data;
using System.Data.Entity;

namespace OAP.Domain.ViewModel
{
    public class ScoreCardViewModel
    {
        public long ID { get; set; }

        public string CollegeName { get; set; }

        public string BatchName { get; set; }

        public string QuestionPaperName { get; set; }

        public string QuestionPaper { get; set; }

        public string BatchesNotAssociated { get; set; }

        public List<BatchViewModel> batchList { get; set; }

        public string BatchAssociated { get; set; }

        public List<QuestionPaperViewModel> questionList { get; set; }

        public List<CollegeViewModel> collegeViewModelList { get; set; }

    }
    public class Export_Word : DbContext
    {
        public DbSet<StudentDetail> Studentrecord { get; set; }
    }

    public class StudentDetail
    {
        [Key]
        public int id { get; set; }
        [Required]
        public String BatchName { get; set; }
        public String Address { get; set; }
        public String Marks { get; set; }
    }
}
