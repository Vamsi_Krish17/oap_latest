﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class HomeViewModel
    {
        public long selectedTestPackageID { get; set; }
        public List<TestPackageViewModel> testPackageViewModelList { get; set; }
    }
}
