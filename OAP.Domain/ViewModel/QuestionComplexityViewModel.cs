﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class QuestionComplexityViewModel
    {

        public long ID { get; set; }

        public long UploadID { get; set; }

        public int QuestionNumber { get; set; }

        public string Title { get; set; }

        public string Subject { get; set; }

        public string Area { get; set; }

        public long Topic_TagID { get; set; }

        public string Topic_TagName { get; set; }

        public long SubTopic_TagID { get; set; }

        public string SubTopic_TagName { get; set; }

        public int DifficultyID { get; set; }

        public string DifficultyName { get; set; }

        public string AnswerKey { get; set; }

        //public string AnswerKey { get; set; }
        public decimal Mark { get; set; }

        public decimal NegativeMark { get; set; }

        public int Complexity { get; set; }

        public string UploadedUrl { get; set; }

        public string Topic_Subtopic{get;set;}

        public string Result { get; set; }

        public string Reason { get; set; }

    }
}
