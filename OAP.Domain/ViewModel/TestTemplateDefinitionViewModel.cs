﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class TestTemplateDefinitionViewModel
    {
        public long ID { get; set; }

        public long? ParentTagID { get; set; }

        public int? QuestionCount { get; set; }

        public long? TagID { get; set; }

        public long? TestTemplateID { get; set; }

        public string Complexity { get; set; }
        public string ParentTagName { get; set; }
        public string ChildTagName { get; set; }
    }
}
