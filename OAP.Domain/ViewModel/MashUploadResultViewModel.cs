﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class MashUploadResultViewModel
    {

        public string Name { get; set; }

        public bool IsSuccess { get; set; }

        public string Message { get; set; }

        public string Reason { get; set; }

        public List<QuestionViewModel> questionViewModelList { get; set; }

        public string UploadedQuestionIDs { get; set; }

        public QuestionViewModel CurrentQuestion { get; set; }

        public int CurrentIndex { get; set; }

        public long UploadedID { get; set; }
    }
}
