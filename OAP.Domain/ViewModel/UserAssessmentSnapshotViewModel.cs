﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public  class UserQuestionPaperSnapshotViewModel
    {

        public int ID { get; set; }

        public int UserID { get; set; }

        public int QuestionPaperSnapshotID { get; set; }

        public int QuestionID { get; set; }

        public string Question { get; set; }

        public int AnswerID { get; set; }

        public string Answer { get; set; }

        public string AnswerSolution { get; set; }

        public string UserAnswered { get; set; }

        public decimal MarksScored { get; set; }

        public int KeywordCount{ get; set; }

        public int MatchedKeywordCount { get; set; }

        public decimal ScoredAssumptionMarks { get; set; }

        public bool IsEvaluated { get; set; }

        public int CurrentIndex { get; set; }

        public int TotalRecords { get; set; }

        public bool IsCorrectByQuestion { get; set; }

        public bool IsCorrectByStudent { get; set; }

        public int StudentID { get; set; }

        public int SortID { get; set; }

        public decimal Marks { get; set; }

    }
}
