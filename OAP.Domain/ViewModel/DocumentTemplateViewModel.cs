﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OAP.Domain.ViewModel
{
    public class DocumentTemplateViewModel
    {
        public long ID { get; set; }

        public string FilePath { get; set; }

        public List<KeywordListViewModel> keyWordList { get; set; }


        [Required(ErrorMessage="Mandatory")]
       
        public string TemplateName { get; set; }
    }
    public class KeywordListViewModel
    {
        public string KeywordName { get; set; }
        public int Counting { get; set; }
    }
}
