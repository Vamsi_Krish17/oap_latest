﻿using System;
using System.Collections.Generic;

namespace OAP.Domain.ViewModel
{

    public class TechnicalAnswerViewModel
    {
        public int ID { get; set; }
        public string InputParam { get; set; }
        public string OutputParam { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public virtual Question Question { get; set; }
        public Nullable<int> QuestionID { get; set; }
    }
}
