﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OAP.Domain.ViewModel
{
   public class SubjectViewModel
    {

       public long ID { get; set; }

       [Required]
       public string Name { get; set; }

       public string Description { get; set; }

       [Required]
       public decimal Hours { get; set; }

       public bool IsSelected { get; set; }

    }
}
