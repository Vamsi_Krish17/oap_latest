﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OAP.Domain.ViewModel
{
    public class QuestionPaperSectionViewModel
    {

        public int ID { get; set; }

        public long? QuestionPaperID { get; set; }

        [Required]
        public string Name { get; set; }

        public string SectionName { get; set; }

        [StringLength(50)]
        public string Description { get; set; }

        [Display(Name = "Cut Off Mark")]
        public decimal CutOffMark { get; set; }

        public decimal Mark { get; set; }

        public decimal NegativeMark { get; set; }

        public bool IsNegativeMarkPercentage { get; set; }

        [Display(Name = "Time Limit")]
        public int TimeLimit { get; set; }

        public int SectionCount { get; set; }

        //public string SectionName { get; set; }

        public List<QuestionPaperSectionViewModel> questionPaperSections { get; set; }

        public List<TagViewModel> TagViewModelList { get; set; }

        public int AvailableQuestionsCount { get; set; }

        public int SelectedQuestionsCount { get; set; }

        public bool IsShowTimeLimit { get; set; }

        public TimeSpan TimeLimitTimeSpan { get; set; }

        public decimal TotalMarksScored { get; set; }

        public int UserAnsweredCount { get; set; }

        public int UsertotalTimeTaken { get; set; }

        public int CorrectAnswerCount { get; set; }

        public int TotalQuestionCount { get; set; }

        public int UnAnswerQuestions { get; set; }

        public int InCorrectAnswer { get; set; }

        public decimal ScorePercentage { get; set; }







        //public decimal mark { get; set; }
        public int Answered { get; set; }
        public int UnAnswer { get; set; }
        public int Total { get; set; }
        public int CorrectAnswer { get; set; }
        public int InCorrectAnswers { get; set; }
        public decimal MarksPercentage { get; set; }


        //public int[] QuestionPaperSections { get; set; }

        public List<QuestionPaperSectionQuestionViewModel> questionPaperSectionQuestionList { get; set; }

        public List<SubjectViewModel> SubjectViewModeList { get; set; }
        
        public List<QuestionViewModel> QuestionViewModeList { get; set; }

        public void Add(QuestionPaperSectionViewModel qpsvm)
        {
            throw new NotImplementedException();
        }

        public int TotalQuestions { get; set; }
        //public int Totalquestions { get; set; }
        public int YourScore { get; set; }
        
        public int AnsweredCount { get; set; }

        public int UnAttemptedQuestions { get; set; }

        public int WrongAnswerCount { get; set; }

        public decimal Percentage { get; set; }

        //public decimal ScorePercentage { get; set; }

        public decimal fromTimeString { get; set; }
    }
}
