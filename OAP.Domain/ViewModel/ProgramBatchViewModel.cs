﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class ProgramBatchViewModel
    {
        public long ID { get; set; }

        public int BatchID { get; set; }

        public long ProgramID { get; set; }
    }
}
