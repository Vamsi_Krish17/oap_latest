﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class FormQuestionPaperViewModel
    {
        [Required]
        public long QuestionPaperID { get; set; }
        public string QuestionPaperName { get; set; }
        public QuestionPaperViewModel QuestionPaperViewModel { get; set; }
        public List<BatchQuestionPaperViewModel> BatchQuestionPaperList { get; set; }
        [Required]
        public long BatchID { get; set; }
        public string BatchIDString { get; set; }
        public List<BatchQuestionPaperViewModel> batchList { get; set; }
        public string BatchName { get; set; }
        public BatchViewModel BatchViewModel { get; set; }
        public bool CanReview { get; set; }
        public long CollegeID { get; set; }
        public string CollegeName { get; set; }
        public long CompletedStudent { get; set; }
        public string Description { get; set; }
        [Required]
        public string EndDateString { get; set; }
        public long ID { get; set; }
        public bool IsQuestionPaperBatchDateNotInRange { get; set; }
        public bool IsQuestionPaperTaken { get; set; }
        public bool IsHaveRetestRequest { get; set; }
        public bool IsRetestRequestRejected { get; set; }
        public bool IsShowRetestRequest { get; set; }
        public string Name { get; set; }
        public long NotTakenStudent { get; set; }
        public List<QuestionPaperSectionViewModel> QuestionPaperSectionList { get; set; }
        public string RejectReason { get; set; }
        public long SectionCounts { get; set; }
        public bool ShowReport { get; set; }
        public DateTime StartDate { get; set; }
        [Required]
        public string StartDateString { get; set; }
        public string Status { get; set; }
        public List<StudentViewModel> studentViewModelList { get; set; }
        public long TotalStudent { get; set; }
        public List<UserQuestionPaperViewModel> userQuestionPaperViewModelLiat { get; set; }
        public long WritingStudent { get; set; }
    }
}
