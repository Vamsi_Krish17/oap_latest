﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
   public class AnswerViewModel
    {

        public string Description { get; set; }

        public int ID { get; set; }

        public bool isCorrect { get; set; }

        public DateTime LastUpdated { get; set; }

        public int QuestionID { get; set; }

       


    }
}
