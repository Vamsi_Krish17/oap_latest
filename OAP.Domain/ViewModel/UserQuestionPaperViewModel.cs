﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class UserQuestionPaperViewModel
    {

        public int ID { get; set; }

        public int UserID { get; set; }

        public string UserName { get; set; }

        public string UniversityRegisterNumber { get; set; }

        public long QuestionPaperID { get; set; }

        public string SessionID { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime FinishDateTime { get; set; }

        public string MobileNumber { get; set; }

        public decimal Marks { get; set; }

        public bool Result { get; set; }

        public bool IsActive { get; set; }

        public int CurrentSectionIndex { get; set; }

        public int TotalQuestions { get; set; }

        public decimal Average { get; set; }

        public int CorrectAnswerCount { get; set; }

        public int AttendedQuestionsCount { get; set; }

        public double AverageTimeTaken { get; set; }

        public double AverageTimeTakenForCorrectAnswer{ get; set; }

        public double AverageTimeTakenForWrongAnswer { get; set; }

    }
}
