﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class TestPackageQuestionPaperTemplateViewModel
    {
        public long ID { get; set; }
        public long QuestionPapertemplateID { get; set; }
        public string QuestionPaperName { get; set; }
        public long TestPackageID { get; set; }
        public int Count { get; set; }
        public bool IsQuestionPaperTaken { get; set; }
        public long QuestionPaperID { get; set; }
        public string TemplateName { get; set; }

        public List<QuestionPaperSectionTemplateViewModel> QuestionPaperSectionTemplateViewModellist { get; set; }
    }
}
