﻿namespace OAP.Domain.ViewModel
{
    public class PracticeSchoolLevelModel
    {
        public int ID { get; set; }

        public int PracticeSchoolID { get; set; }

        public string LevelName { get; set; }

        public string LevelDescription { get; set; }
    }
}