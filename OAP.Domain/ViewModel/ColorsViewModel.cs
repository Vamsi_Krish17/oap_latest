﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class ColorsViewModel
    {
        public List<ColorCodeViewModel> colorCodeViewModelList { get; set; }
    }
}
