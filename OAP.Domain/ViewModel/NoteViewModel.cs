﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class NoteViewModel
    {
        public long InstituteID { get; set; }

        public DateTime CreatedDate { get; set; }

        public long CreatedBy { get; set; }

        public string Description { get; set; }

        public string Name { get; set; }

        public long ID { get; set; }

        public string CreatedUser { get; set; }

        public string CreatedDateString { get; set; }
    }
}
