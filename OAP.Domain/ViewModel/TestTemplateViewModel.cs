﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
   public class TestTemplateViewModel
    {
        public long ID { get; set; }

        [Required]
        public string Name { get; set; }

        public string  Description { get; set; }

        public List<TagViewModel> TagsNotAssociated { get; set; }

        public List<SubjectViewModel> SubjectsNotAssociated { get; set; }

        public List<TagViewModel> TagsAssociated { get; set; }

        public List<SubjectViewModel> SubjectsAssociated { get; set; }

        public string TagIDString { get; set; }

        public string SubjectIDString { get; set; }

        public string Complexity { get; set; }

        public decimal Marks { get; set; }

        public decimal NegativeMarks { get; set; }

        public int Count { get; set; }

        public long ParentTagID { get; set; }

        public long ChildTagID { get; set; }

        public string TestTemplateDefinitionsIDString { get; set; }
        public string QuestionPaperName { get; set; }

        public long TestTemplateDefinitionID { get; set; }


        public List<TestTemplateDefinitionViewModel> TestTemplateValues { get; set; }
    }

}
