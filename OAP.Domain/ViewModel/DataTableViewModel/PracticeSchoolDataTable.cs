﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class PracticeSchoolDataTable : DataTableViewModel
    {
        public List<PracticeSchoolViewModel> data { get; set; }

        public PracticeSchoolDataTable(List<PracticeSchoolViewModel> practiceSchoolViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = practiceSchoolViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }

    public class PracticeSchoolLevelDataTable : DataTableViewModel
    {
        public List<PracticeSchoolLevelModel> data { get; set; }

        public PracticeSchoolLevelDataTable(List<PracticeSchoolLevelModel> practiceSchoolLevelModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = practiceSchoolLevelModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}
