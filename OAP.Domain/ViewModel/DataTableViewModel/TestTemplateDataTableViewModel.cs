﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
   public class TestTemplateDataTableViewModel:DataTableViewModel
    {
       public List<TestTemplateViewModel> data { get; set; }
        public TestTemplateDataTableViewModel(List<TestTemplateViewModel> testTemplateViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = testTemplateViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }

    
}
