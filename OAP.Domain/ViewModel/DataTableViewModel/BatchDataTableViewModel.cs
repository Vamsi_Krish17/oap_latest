﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OAP.Domain.ViewModel;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class BatchDataTableViewModel:DataTableViewModel
    {

         public List<BatchViewModel> data { get; set; }
         public BatchDataTableViewModel(List<BatchViewModel> batchViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = batchViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }

    }
}
