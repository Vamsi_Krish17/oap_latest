﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class QuestionComplexityDataTableviewModel : DataTableViewModel
    {

         public List<QuestionComplexityViewModel> data { get; set; }

         public QuestionComplexityDataTableviewModel(List<QuestionComplexityViewModel> questionComplexityViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = questionComplexityViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }

    }
}
