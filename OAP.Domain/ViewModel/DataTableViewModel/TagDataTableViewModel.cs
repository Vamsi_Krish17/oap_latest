﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class TagDataTableViewModel:DataTableViewModel
    {

        public List<TagViewModel> data { get; set; }
        public TagDataTableViewModel(List<TagViewModel> tagViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = tagViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }

    }
}
