﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class RequestRetestDataTableViewModel : DataTableViewModel
    {
         public List<RequestRetestViewModel> data { get; set; }

         public RequestRetestDataTableViewModel(List<RequestRetestViewModel> requestRetestViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = requestRetestViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }

    }
}
