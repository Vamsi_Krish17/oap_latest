﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class ReportDataTableViewModel: DataTableViewModel
    {
        public List<ReportViewModel> data { get; set; }
        public ReportDataTableViewModel(List<ReportViewModel> reportViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = reportViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}
