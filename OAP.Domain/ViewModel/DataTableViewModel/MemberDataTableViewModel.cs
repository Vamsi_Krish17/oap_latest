﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAP.Domain.ViewModel.DataTableViewModel
{

    public class MemberDataTableViewModel : DataTableViewModel
    {
        public List<MemberViewModel> data { get; set; }
        public MemberDataTableViewModel(List<MemberViewModel> memberViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = memberViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}
