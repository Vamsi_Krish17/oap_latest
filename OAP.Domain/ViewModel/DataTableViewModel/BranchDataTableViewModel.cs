﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class BranchDataTableViewModel:DataTableViewModel
    {

        public List<BranchViewModel> data { get; set; }
         public BranchDataTableViewModel(List<BranchViewModel> branchViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = branchViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }

    }
}
