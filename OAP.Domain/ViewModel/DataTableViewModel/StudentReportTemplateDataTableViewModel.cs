﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class StudentReportTemplateDataTableViewModel : DataTableViewModel
    {

        public List<StudentReportTemplateViewModel> data { get; set; }
        public StudentReportTemplateDataTableViewModel(List<StudentReportTemplateViewModel> templateViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = templateViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }

    }
}
