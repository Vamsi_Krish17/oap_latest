﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class TemplateDataTableViewModel : DataTableViewModel
    {

        public List<TemplateViewModel> data { get; set; }
        public TemplateDataTableViewModel(List<TemplateViewModel> templateViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = templateViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }

    }
}
