﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
   public class QuestionPaperDataTableViewModel:DataTableViewModel
    {
         public List<QuestionPaperViewModel> data { get; set; }
         public QuestionPaperDataTableViewModel(List<QuestionPaperViewModel> questionPaperViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = questionPaperViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}
