﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class StudentDataTableViewModel : DataTableViewModel
    {
        public List<StudentViewModel> data { get; set; }
        public StudentDataTableViewModel(List<StudentViewModel> studentViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = studentViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}
