﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class UserProfileDataTableViewModel : DataTableViewModel
    {
         public List<UserProfileViewModel> data { get; set; }

         public UserProfileDataTableViewModel(List<UserProfileViewModel> userProfileViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = userProfileViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}
