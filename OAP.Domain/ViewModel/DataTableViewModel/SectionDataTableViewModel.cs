﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class SectionDataTableViewModel :DataTableViewModel
    {

          public List< SectionViewModel> data { get; set; }
          public SectionDataTableViewModel(List<SectionViewModel> sectionViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = sectionViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }

    }
}
