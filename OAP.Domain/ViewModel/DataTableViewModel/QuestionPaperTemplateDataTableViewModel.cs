﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
   public class QuestionPaperTemplateDataTableViewModel:DataTableViewModel
    {
         public List<QuestionTemplateWizardViewModel> data { get; set; }
         public QuestionPaperTemplateDataTableViewModel(List<QuestionTemplateWizardViewModel> questionPaperTemplateViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = questionPaperTemplateViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}
