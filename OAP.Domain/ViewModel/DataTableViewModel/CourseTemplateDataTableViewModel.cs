﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class CourseTemplateDataTableViewModel : DataTableViewModel
    {
        public List<CourseTemplateViewModel> data { get; set; }
        public CourseTemplateDataTableViewModel(List<CourseTemplateViewModel> collegeViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = collegeViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }

    }
}
