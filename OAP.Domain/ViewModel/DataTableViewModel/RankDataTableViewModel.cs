﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class RankDataTableViewModel : DataTableViewModel
    {
        public List<RankViewModel> data { get; set; }
        public RankDataTableViewModel(List<RankViewModel> rankViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = rankViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}
