﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class TestimonialDataTableViewModel : DataTableViewModel
    {

        public List<TestimonialViewModel> data { get; set; }
        public TestimonialDataTableViewModel(List<TestimonialViewModel> testimonialViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = testimonialViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }

}
