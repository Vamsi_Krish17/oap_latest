﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
   public class QuestionPaperSectionTemplateDataTableViewModel:DataTableViewModel
    {
       public List<QuestionPaperSectionTemplateViewModel> data { get; set; }
       public QuestionPaperSectionTemplateDataTableViewModel(List<QuestionPaperSectionTemplateViewModel> questionPaperSectionTemplateViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = questionPaperSectionTemplateViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}

 