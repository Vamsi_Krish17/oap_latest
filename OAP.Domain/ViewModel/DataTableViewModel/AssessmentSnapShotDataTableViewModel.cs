﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class QuestionPaperSnapShotDataTableViewModel : DataTableViewModel
    {

        public List<QuestionPaperSnapShotViewModel> data { get; set; }

        public QuestionPaperSnapShotDataTableViewModel(List<QuestionPaperSnapShotViewModel> assessmentSnapShotViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = assessmentSnapShotViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }

    }
}
