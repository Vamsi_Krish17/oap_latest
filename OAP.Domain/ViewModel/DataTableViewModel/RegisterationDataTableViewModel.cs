﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class RegisterationDataTableViewModel : DataTableViewModel
    {
        public List<RegisterationViewModel> data { get; set; }
        public RegisterationDataTableViewModel(List<RegisterationViewModel> registerationViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = registerationViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }

    }
}
