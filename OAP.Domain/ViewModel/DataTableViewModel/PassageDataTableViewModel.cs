﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class PassageDataTableViewModel : DataTableViewModel
    {

        public List<PassageViewModel> data { get; set; }

        public PassageDataTableViewModel(List<PassageViewModel> passageViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = passageViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }

    }
}
