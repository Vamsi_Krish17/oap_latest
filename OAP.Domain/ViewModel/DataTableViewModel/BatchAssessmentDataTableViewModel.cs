﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
   public class BatchQuestionPaperDataTableViewModel:DataTableViewModel
    {
         public List<BatchQuestionPaperViewModel> data { get; set; }
         public BatchQuestionPaperDataTableViewModel(List<BatchQuestionPaperViewModel> batchQuestionPaperViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = batchQuestionPaperViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}
