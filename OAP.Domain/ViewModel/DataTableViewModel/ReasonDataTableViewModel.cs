﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class ReasonDataTableViewModel : DataTableViewModel
    {
        public List<ReasonViewModel> data { get; set; }
        public ReasonDataTableViewModel(List<ReasonViewModel> reasonViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = reasonViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}
