﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
   public class DocumentTemplateDataTableViewModel:DataTableViewModel
    {
         public List<DocumentTemplateViewModel> data { get; set; }
         public DocumentTemplateDataTableViewModel(List<DocumentTemplateViewModel> documentTemplateViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = documentTemplateViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}

