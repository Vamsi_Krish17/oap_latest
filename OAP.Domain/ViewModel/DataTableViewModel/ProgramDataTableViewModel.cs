﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class ProgramDataTableViewModel : DataTableViewModel
    {
        public List<ProgramViewModel> data { get; set; }
        public ProgramDataTableViewModel(List<ProgramViewModel> programViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = programViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}





