﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public  class QuestionPaperSectionTemplateRuleDataTableViewModel:DataTableViewModel
    {
       public List<QuestionPaperSectionTemplateRuleViewModel> data { get; set; }
       public QuestionPaperSectionTemplateRuleDataTableViewModel(List<QuestionPaperSectionTemplateRuleViewModel> questionPaperSectionTemplateRuleViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = questionPaperSectionTemplateRuleViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}
