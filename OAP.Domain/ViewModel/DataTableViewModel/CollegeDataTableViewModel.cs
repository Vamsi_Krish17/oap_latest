﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class CollegeDataTableViewModel : DataTableViewModel
    {
        public List<CollegeViewModel> data { get; set; }
        public CollegeDataTableViewModel(List<CollegeViewModel> collegeViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = collegeViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}
