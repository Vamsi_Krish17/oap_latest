﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class QuestionPaperSectionDataTableViewModel:DataTableViewModel
    {
        public List<QuestionPaperSectionViewModel> data { get; set; }
         public QuestionPaperSectionDataTableViewModel(List<QuestionPaperSectionViewModel> assessmentSectionViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = assessmentSectionViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
   
}
