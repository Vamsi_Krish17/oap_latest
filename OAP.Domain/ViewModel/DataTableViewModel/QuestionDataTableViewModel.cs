﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class QuestionDataTableViewModel : DataTableViewModel
    {

        public List<CommonQuestionViewModel> data { get; set; }
        public List<TechnicalQuestionViewModel> technicalData { get; set; }

        public QuestionDataTableViewModel(List<QuestionViewModel> questionViewModelList, List<TechnicalQuestionViewModel> technicalQuestionViewModelList, int draw, int totalRecords, int filteredCount)
        {

            this.data = ConvertToCommonQuestionViewModel(questionViewModelList, technicalQuestionViewModelList);
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }

        public class TechnicalQuestionDataTableViewModel : DataTableViewModel
        {

            public List<TechnicalQuestionViewModel> data { get; set; }
            public TechnicalQuestionDataTableViewModel(List<TechnicalQuestionViewModel> questionViewModelList, int draw, int totalRecords, int filteredCount)
            {
                this.data = questionViewModelList;
                this.draw = draw;
                this.recordsTotal = totalRecords;
                this.recordsFiltered = filteredCount;
            }

        }

        private List<CommonQuestionViewModel> ConvertToCommonQuestionViewModel(List<QuestionViewModel> questionViewModelList, List<TechnicalQuestionViewModel> technicalQuestionViewModelList)
        {
            var datas = new List<CommonQuestionViewModel>();

            foreach (var q in questionViewModelList)
            {
                var data = new CommonQuestionViewModel()
                {
                    ID = q.ID,
                    Description = q.Description,
                    LastUpdatedTime = q.LastUpdatedTime,
                    Solution = q.Solution,
                    DownloadLink = q.DownloadLink,
                    Passage = q.Passage,
                    PassageID = q.PassageID,
                };

                datas.Add(data);
            }

            foreach (var q in technicalQuestionViewModelList)
            {
                var data = new CommonQuestionViewModel()
                {
                    ID = q.ID,
                    Description = q.Title,
                    LastUpdatedTime = q.LastUpdatedTime,
                    Solution = q.Solution,
                    DownloadLink = q.DownloadLink,
                    Passage = q.Passage,
                    PassageID = q.PassageID,
                };
                datas.Add(data);
            }

            return datas;
        }

    }
}
