﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class TestPackageDataTableViewModel : DataTableViewModel
    {
        public List<TestPackageViewModel> data { get; set; }
        public TestPackageDataTableViewModel(List<TestPackageViewModel> testPackageViewModel, int draw, int totalRecords, int filteredCount)
        {
            this.data = testPackageViewModel;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}
