﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class SubjectDataTableViewModel : DataTableViewModel
    {
        public List<SubjectViewModel> data { get; set; }
        public SubjectDataTableViewModel(List<SubjectViewModel> studentViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = studentViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }


    }
}
