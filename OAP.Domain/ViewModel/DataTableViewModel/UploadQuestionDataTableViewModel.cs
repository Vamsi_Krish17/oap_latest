﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class UploadQuestionDataTableViewModel : DataTableViewModel
    {
        public List<UploadQuestionViewModel> data { get; set; }

        public UploadQuestionDataTableViewModel(List<UploadQuestionViewModel> uploadQuestionViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = uploadQuestionViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}
