﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel.DataTableViewModel
{
    public class InstituteProgramDataTableViewModel : DataTableViewModel
    {
        public List<InstituteProgramViewModel> data { get; set; }
        public InstituteProgramDataTableViewModel(List<InstituteProgramViewModel> instituteprogramViewModelList, int draw, int totalRecords, int filteredCount)
        {
            this.data = instituteprogramViewModelList;
            this.draw = draw;
            this.recordsTotal = totalRecords;
            this.recordsFiltered = filteredCount;
        }
    }
}





