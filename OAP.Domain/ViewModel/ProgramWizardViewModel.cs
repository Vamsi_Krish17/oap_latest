﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
   public class ProgramWizardViewModel
    {
        public string LeftSelectedIds { get; set; }

        public string RightSelectedIds { get; set; }

        public string ProgramName { get; set; }

        public string ProgramDescription { get; set; }

        public List<OptionModel> CourseTemplateDetails { get; set; }


        public List<OptionModel> InstituesList { get; set; }

        public string selectedCourse { get; set; }

        public string Institute { get; set; }

        public long ProgramID { get; set; }

        public List<QuestionPaperViewModel> QuestionPaperViewModelList { get; set; }

        public List<TestTemplateViewModel> TestTemplateViewModelList { get; set; }

        public long InstituteID { get; set; }

        public long CourseTemplateID { get; set; }

        public List<SubjectViewModel> SubjectViewModelList { get; set; }

        public List<BatchViewModel> BatchViewModelList { get; set; }

        public string StartDateString { get; set; }

        public string EndDateString { get; set; }

    }
}
