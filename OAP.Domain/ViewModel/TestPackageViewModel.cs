﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
   public class TestPackageViewModel
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public bool  IsActive { get; set; }

        public int Validity { get; set; }

        public string Validitys { get; set; }

        public long QuestionPaperTemplateID { get; set; }

        public List<QuestionTemplateWizardViewModel> QuestionPaperTemplateList { get; set; }

        public string JonDataString { get; set; }

        public string TestPackageQPTJonDataString { get; set; }

        public double Cost { get; set; }

        public int Count { get; set; }

        public int DisplayOrder { get; set; }

        public string DisplayOrderString { get; set; }

        public string DisplayOrderList { get; set; }

        public long ID { get; set; }

       public string CountStrings { get; set; }

       public string TemplateIDString { get; set; }

       public List<TestPackageQuestionPaperTemplateViewModel> testPackageQuestionPaperTempleteViewModelList { get; set; }


       public List<QuestionTemplateWizardViewModel> questionTemplateWizardViewModelList { get; set; }

       public string Code { get; set; }

       public decimal CutOffMark { get; set; }

       public bool DisplayMarks { get; set; }

       public bool DisplayResult { get; set; }

       public DateTime EndDate { get; set; }

       public string Instructions { get; set; }

       public bool IsNegativePercentage { get; set; }

       public decimal Marks { get; set; }

       public decimal NegativeMarks { get; set; }

       public bool ReleaseKey { get; set; }

       public DateTime StartDate { get; set; }

       public int TimeLimit { get; set; }

       public TimeSpan TimeLimitTimeSpan { get; set; }

       public bool ShowInstructions { get; set; }

       public bool ShowReport { get; set; }

       public List<QuestionPaperSectionViewModel> questionPaperSectionViewModelList { get; set; }
    }

   
}
