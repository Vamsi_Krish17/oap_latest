﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace OAP.Domain.ViewModel
{
    public class StudentReportTemplateViewModel
    {

        public int ID { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Template { get; set; }

        public DateTime CreatedDate { get; set; }

        public DateTime ModifiedDate { get; set; }

        public int CreatedByID { get; set; }

        public int ModifiedByID { get; set; }

    }
}
