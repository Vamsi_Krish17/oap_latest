﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class QuestionTemplateWizardViewModel
    {
        public long ID { get; set; }
        public string QuestionName { get; set; }
        public string QuestionCode { get; set; }
        public string Description { get; set; }
        public int NoOfSections { get; set; }
        public int Count { get; set; }
        public int Complexity { get; set; }
        public int TimeLimit { get; set; }
        public decimal CutOffMark { get; set; }
        public bool DisplayResult { get; set; }
        public bool DisplayMarks { get; set; }
        public decimal Marks { get; set; }
        public decimal NegativeMarks { get; set; }

        public int QuestionPaperCountThisTemplate { get; set; }

        public TimeSpan TimeLimitTimeSpan { get; set; }
        //QuestionPaper Section Template values below        
        public List<QuestionPaperSectionTemplateViewModel> questionPaperSectionTemplateViewmodelList { get; set; }

        public List<TagViewModel> Tagslist { get; set; }
        public List<SubjectViewModel> SubjectList { get; set; }

        public QuestionPaperSectionTemplateViewModel QuestionPaperSectionTemplateViewModel { get; set; }

        public List<QuestionPaperViewModel> questionPaperViewModellist { get; set; }


        public DateTime? createdDate { get; set; }

        public string CreatedDateString
        {
            get
            {
                return createdDate != null ? createdDate.Value.ToString("dd/MM/yyyy") : "";
            }
        }

        public bool IsShowSectionTimeLimit { get; set; }

        public string TemplateName { get; set; }

        public bool CanJumpSections { get; set; }
        public string Instructions { get; set; }
        public bool ShowInstructions { get; set; }

        public bool ShowReport { get; set; }
    }
}
