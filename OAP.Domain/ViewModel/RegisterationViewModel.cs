﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
   public class RegisterationViewModel
    {
        public long ID { get; set; }

       [Required]
        public string Name { get; set; }
       [Required]
        public string RegisterNumber { get; set; }
       [Required]
        public string EducationalQualification { get; set; }
       [Required]
        public string Specialization { get; set; }
         [Required]

        public string YearofPassing { get; set; }
         [Required]
        public string CollegeName { get; set; }
         [Required]
        public string CGPA10 { get; set; }
         [Required]
        public string CGPA12 { get; set; }

         public string Diploma { get; set; }

         [Required]
        public string UGCGPA { get; set; } 
      
        public string PGCGPA { get; set; }
         [Required]
        public string ContactNumber1 { get; set; }

        public string ContactNumber2 { get; set; }
         [Required]
        public string EMailId { get; set; }
         [Required]
        public string Location { get; set; }

        public string OtherCertificates { get; set; }
         [Required]
        public string Resume { get; set; }

        public DateTime? CreatedDate { get; set; }

        public string CreatedDateString
        {
            get
            {
                return CreatedDate != null ? CreatedDate.Value.ToString("dd/MM/yyyy") : "";
            }
        }



    }
}
