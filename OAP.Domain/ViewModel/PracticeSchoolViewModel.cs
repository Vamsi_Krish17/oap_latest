﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAP.Domain.ViewModel
{
    public class PracticeSchoolViewModel
    {
        public PracticeSchoolViewModel()
        {
            this.PracticeSchoolLevel = new HashSet<PracticeSchoolLevel>();
        }

        public int ID { get; set; }
        public string SessionName { get; set; }
        public string Section1 { get; set; }
        public string Section2 { get; set; }
        public string Section3 { get; set; }
        public string Section4 { get; set; }
        public Nullable<System.DateTime> CreatedDate { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> ModifiedDate { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<bool> IsTechnicalSession { get; set; }

        public virtual ICollection<PracticeSchoolLevel> PracticeSchoolLevel { get; set; }
    }
}
