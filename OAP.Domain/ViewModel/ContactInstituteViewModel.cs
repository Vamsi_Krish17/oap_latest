﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class ContactInstituteViewModel
    {
        public DateTime? EffectiveDate { get; set; }

         public string EffectiveDatestring
        {
            get
            {
                string dateString = EffectiveDate != null ? EffectiveDate.Value.ToString("dd-MM-yyy") : "";
                return dateString;
            }
            //set;
        }
         [Required]
        public string EmailID1 { get; set; }

         [Required]
        public string EmailID2 { get; set; }

         [Required]
        public string PhoneNumber1 { get; set; }
       
         [Required]
        public string PhoneNumber2 { get; set; }

        //public string Password1 { get; set; }
        //[Compare("Password1")]
        //public string Password2 { get; set; }

        //public string Address { get; set; }
         [Required]
        public string Designation { get; set; }
         [Required]
        public string Name { get; set; }

        public long ID { get; set; }
         [Required]
        public string Status { get; set; }

        public long InstituteID { get; set; }

        public string errormessage { get; set; }

        public List<ContactInstituteViewModel> contactInstituteViewModelList { get; set; }

    }
}
