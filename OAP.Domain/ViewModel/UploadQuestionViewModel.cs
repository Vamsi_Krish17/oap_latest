﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace OAP.Domain.ViewModel
{
    public class UploadQuestionViewModel
    {
        CultureInfo ci = new CultureInfo("en-GB");
        public long ID { get; set; }

        public DateTime UploadedDate { get; set; }

        public string _UploadedDateTimeString;
        public string UploadedDateTimeString
        {
            get { return UploadedDate.ToString("dd-MM-yyyy"); }
            set
            {
                _UploadedDateTimeString = value;
                DateTime _start;
                if (DateTime.TryParse(value, ci, DateTimeStyles.None, out _start))
                //if (DateTime.TryParse(value, ci, DateTimeStyles.None, out _start))
                {
                    UploadedDate = _start;
                }
            }
        }

        public string Title { get; set; }

        public int UploadedUserID { get; set; }

        public List<QuestionViewModel> UploadedQuestions { get; set; }

        public QuestionViewModel QuestionViewModel { get; set; }

        public int CurrentIndex { get; set; }

        public int SelectedIndex { get; set; }

        public int TotalQuestions { get; set; }

        public bool IsCorrectAnswer { get; set; }

        public int QuestionCount { get; set; }

        public int Marks { get; set; }

        public decimal NegativeMarks { get; set; }
    }

}

