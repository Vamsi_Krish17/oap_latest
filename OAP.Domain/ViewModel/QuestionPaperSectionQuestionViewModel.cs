﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{
    public class QuestionPaperSectionQuestionViewModel
    {
        public int QuestionComplexityLevel { get; set; }

        public string QuestionSubject { get; set; }

        public int QuestionID { get; set; }

        public int QuestionPaperSectionID { get; set; }

        public int ID { get; set; }

        public List<string> questionTags { get; set; }
    }
}
