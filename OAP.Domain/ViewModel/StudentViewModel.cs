﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;


namespace OAP.Domain.ViewModel
{
    public class StudentViewModel
    {

        public long ID { get; set; }
     
        [Required]
        public string FullName { get; set; }

        //[Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        //[Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [MaxLength(50)]
        public string UniversityRegistryNumber { get; set; }
        
        public string BatchName { get; set; }

        [Display(Name = "Email Id")]
        [RegularExpression(@"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                            @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                            @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$",
                            ErrorMessage = "Email is not valid")]
        public string EmailId { get; set; }

        public long BranchID { get; set; }

        public int UserProfileID { get; set; }

        public List<StudentSectionMarksModel> SectionMarksDetails { get; set; }

        public QuestionPaperSectionViewModel QuestionPaperSectionViewModel { get; set; }


        public string UploadName { get; set; }

        public int ScheduledTestCount { get; set; }

        public int TakenTestCount { get; set; }

        public int RemainingTestCount { get; set; }

        public List<QuestionPaperViewModel> AssessmentViewModelList { get; set; }

        public List<BatchQuestionPaperViewModel> batchAssessmentViewModelList { get; set; }

        public string ResumePath { get; set; }
    }
    public class StudentSectionMarksModel
    {
        public long StudentID { get; set; }

        public int SectionIndex { get; set; }

        public decimal Marks { get; set; }

        public string Name { get; set; }

        public decimal Total { get; set; }

        public string UniversityRegNo { get; set; }


    }
}
