﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OAP.Domain.ViewModel
{
    public class LevelTechnicalQuestionViewModel
    {
        public int SessionLevelId { get; set; }
        public List<TagViewModel> TagViewModelList { get; set; }
    }
}
