﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OAP.Domain.ViewModel
{

    public class PasswordResetViewModel
    {

        public string PasswordResetToken { get; set; }

        public string NewPassword { get; set; }

    }

}
