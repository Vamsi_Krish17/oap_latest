//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OAP.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class Answer
    {
        public Answer()
        {
            this.AssessmentSnapshot = new HashSet<AssessmentSnapshot>();
            this.UserAssessmentResponse = new HashSet<UserAssessmentResponse>();
        }
    
        public int ID { get; set; }
        public string Description { get; set; }
        public Nullable<int> QuestionID { get; set; }
        public Nullable<bool> IsCorrect { get; set; }
        public Nullable<System.DateTime> LastUpdated { get; set; }
        public byte[] Timestamp { get; set; }
    
        public virtual Question Question { get; set; }
        public virtual ICollection<AssessmentSnapshot> AssessmentSnapshot { get; set; }
        public virtual ICollection<UserAssessmentResponse> UserAssessmentResponse { get; set; }
    }
}
