//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OAP.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class PracticeResult
    {
        public int ID { get; set; }
        public int PracticeID { get; set; }
        public int QuestionCategoryID { get; set; }
        public int QuestionCount { get; set; }
        public int CorrectAnswerCount { get; set; }
        public int WrongAnswerCount { get; set; }
        public byte[] Timestamp { get; set; }
    }
}
