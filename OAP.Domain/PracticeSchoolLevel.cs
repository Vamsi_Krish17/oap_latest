//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OAP.Domain
{
    using System;
    using System.Collections.Generic;
    
    public partial class PracticeSchoolLevel
    {
        public PracticeSchoolLevel()
        {
            this.PracticeSchoolLevelQuestion = new HashSet<PracticeSchoolLevelQuestion>();
        }
    
        public int ID { get; set; }
        public int PracticeSchoolId { get; set; }
        public string LevelName { get; set; }
        public string LevelDescription { get; set; }
    
        public virtual PracticeSchool PracticeSchool { get; set; }
        public virtual ICollection<PracticeSchoolLevelQuestion> PracticeSchoolLevelQuestion { get; set; }
    }
}
